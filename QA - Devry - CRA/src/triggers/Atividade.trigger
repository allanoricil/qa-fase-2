trigger Atividade on Atividade__c (after insert) {

        List<Task> listTaskToAdd = new List<Task>();

        for(Atividade__c atv : trigger.new){

            Task taskToAdd = new Task();

            Atividade__c atividade = [Select Id, User__c, Descri_o__c, Status__c, StatusContato__c, Canal__c, Oportunidade__r.Opportunity__c from Atividade__c where id=:atv.Id];


            taskToAdd.Subject = 'Primeiro Contato com Candidato';

            taskToAdd.Priority = 'Alta';
             
            taskToAdd.Description = atividade.Descri_o__c;
                 
            taskToAdd.Status = atividade.Status__c; 
             
            taskToAdd.Conseguiu_Contato__c = atividade.StatusContato__c;    

            taskToAdd.Canal_de_contato__c = atividade.Canal__c; 

            taskToAdd.WhatId = atividade.Oportunidade__r.Opportunity__c;

            listTaskToAdd.add(taskToAdd);
    }


    insert listTaskToAdd;
}