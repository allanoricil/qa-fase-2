trigger UsuarioProprietarioDaOportunidade on User (before update) {    
    for(User user : Trigger.new){
        
        //Lista todas as Inscrições/Oportunidades do usuário que está sendo atualizado.
        List<Opportunity> ownerOpp = [SELECT o.Id, o.OwnerId, o.Description FROM Opportunity o WHERE o.OwnerId =: user.Id];
        
        //Usuário Ativo.
        if(user.IsActive){
            
            //Usuário proprietário de oportunidade(s).
            if(!ownerOpp.isEmpty())
            {
                user.Opportunity_Owner__c = true;
            }
            
            //Usuário não proprietário de oportunidade(s).
            else
            {
                user.Opportunity_Owner__c = false;
            }
        }
        
        //Usuário Inativo.
        else if(!user.IsActive)
        {
            //Usuário proprietário de oportunidade(s).
            if(!ownerOpp.isEmpty())
            {                
                try
                {
                    //Ids das oportunidades que serão atribuidos ao Data Loader.
                    List<Id> Ids = new List<Id>();
                    
                    //Iterações
                    integer tamanho = Integer.valueOf(Math.Ceil(ownerOpp.size()/20.0));                    
                    
                    for(integer i = 0; i<tamanho; i++)
                    {
                        //Lista 20 oportunidades por vez pertencentes ao usuário que está sendo inativado e que ainda não foi atribuido ao Data Loader
                        List<Opportunity> ownerOppLimit = [SELECT o.Id FROM Opportunity o WHERE o.OwnerId =: user.Id AND o.Id NOT IN: Ids LIMIT 20];
                        
                        for(Opportunity opp : ownerOppLimit)
                        {
                            //Id da oportunidade que será atribuída ao Data Loader
                            Ids.add(opp.Id);
                        }
                        //Chamada do Batch, passando as oportunidades que seram atribuídaa ao Data Loader.
                        Batch_UpdateOwnerOpportunity up = new Batch_UpdateOwnerOpportunity(ownerOppLimit);                            
                        Database.executeBatch(up);
                    }
                    user.Opportunity_Owner__c = false;
                }
                catch ( DmlException ex )
                {
                    user.addError('Ops! Ocorreu um erro ao desacoplar os registros de Inscrições/Oportunidades do usuário que está sendo inativado, favor entrar em contato com o Administrados do Sistema.' + ex.getMessage());
                    CreateErrorLog.createErrorRecord(ex.getMessage(), 'UsuarioProprietarioDaOportunidade'); 
                }
            }
            
            //Usuário não proprietário de oportunidade(s)
            else
            {
                user.Opportunity_Owner__c = false;
            }
        }
    }
}