trigger AcademicoEAD on Academico__c (after insert, after update) {
	List<Opportunity> opp = [Select Id, EAD__c from Opportunity where Id =: Trigger.new[0].Oportunidade__c limit 1];  // Seleciona a oportunidade do RA

	if(opp.size() > 0 && opp[0].Ead__c == 'Sim'){  // Verifica se é EAD
		List<Oportunidade_ead__c> ead = [Select Id from Oportunidade_ead__c where Opportunity__c =: opp[0].Id limit 1];
		if(Trigger.new[0].SituacaoMatricula__c == null && ead[0].SituacaoMatricula__c == null )
			ead[0].SituacaoMatricula__c = '-';
		else
			ead[0].SituacaoMatricula__c = Trigger.new[0].SituacaoMatricula__c;  // Popula a oportunidade EAD vinculada a Oportunidade padrão com o valor do RA respectivo
		update ead[0];
	}

	opp[0].RA__c = Trigger.new[0].Id; // Preenche a oportunidade com seu respectivo RA

	update opp;

}