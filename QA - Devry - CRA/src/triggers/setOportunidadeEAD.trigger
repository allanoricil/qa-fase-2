trigger setOportunidadeEAD on Opportunity (after insert) {

        
        List<Oportunidade_EAD__c> oppToInsert = new List<Oportunidade_EAD__c>();

        for ( Opportunity opp : trigger.new ){
            
            Oportunidade_EAD__c ead = new Oportunidade_EAD__c();

            Opportunity ac = [SELECT id, Account.Name, Account.PersonMobilePhone, Account.CPF_2__c, Account.Phone, QualNotaMediaENEM__c,
                                AnoProvaENEM__c, Processo_Seletivo__r.Tipo_de_Matr_cula__c, CodigoInscricao__c, Oferta__r.Name, Email__c,
                                Account.E_portador_de_alguma_aten_o_especial__c, Account.PrecisaAtendimentoEspecial__c, EAD__c, StageName, Polo__c,
                                Account.DataNascimento__c, Account.Sexo__c, Account.Cidade__c, Account.Estado__c, Account.CPFdoResponsavel__c,
                                Processo_Seletivo__r.Nota_M_nima__c, X1_OpcaoCurso__r.Name, Polo__r.Email__c
                              From Opportunity where id=: opp.id];

            if(ac.EAD__c == 'Sim'){
                

                if(opp.Id != null) {ead.Opportunity__c = opp.Id;}
     
                if(ac.Account.Name != null) {ead.Name = ac.Account.Name;}
     
                if(ac.Account.PersonMobilePhone != null) {ead.Celular__c = ac.Account.PersonMobilePhone;}
          
                if(ac.Account.CPF_2__c != null) {ead.CPF__c = ac.Account.CPF_2__c;}
          
                if(ac.Account.Phone != null) {ead.Telefone__c = ac.Account.Phone;}

                if(ac.AnoProvaENEM__c != null){ead.AnoProvaENEM__c = ac.AnoProvaENEM__c;}
                 
                if(ac.QualNotaMediaENEM__c != null) {ead.QualNotaMediaENEM__c = ac.QualNotaMediaENEM__c;}

                if(ac.Processo_Seletivo__r.Tipo_de_Matr_cula__c != null ){ ead.TipoProcesso__c = ac.Processo_Seletivo__r.Tipo_de_Matr_cula__c;}

                if(ac.X1_OpcaoCurso__r.Name != null ){ead.Curso__c = ac.X1_OpcaoCurso__r.Name;}

                if(ac.Account.E_portador_de_alguma_aten_o_especial__c != null ){ead.CandidatoPNE__c = ac.Account.E_portador_de_alguma_aten_o_especial__c; ead.RecursoProva__c = ac.Account.PrecisaAtendimentoEspecial__c;}
                else{ead.CandidatoPNE__c = 'Não'; ead.RecursoProva__c = 'Nenhum';}

                if(ac.Email__c != null ){ ead.Email__c = ac.Email__c;}

                if(ac.Polo__c != null ){ead.Polo__c = ac.Polo__c;}

                if(ac.Account.DataNascimento__c != null) {ead.Data_de_Nascimento__c = ac.Account.DataNascimento__c;}

                if(ac.Account.Sexo__c != null) {ead.Sexo__c = ac.Account.Sexo__c;}

                if(ac.Account.Cidade__c != null) {ead.Cidade__c = ac.Account.Cidade__c;}

                if(ac.Account.Estado__c != null) {ead.Estado__c = ac.Account.Estado__c;}

                if(ac.Account.CPFdoResponsavel__c != null) {ead.Fiador__c = ac.Account.CPFdoResponsavel__c;}

                if(ac.Processo_Seletivo__r.Tipo_de_Matr_cula__c == 'ENEM'){
                    
                    if(ac.Processo_Seletivo__r.Nota_M_nima__c != null){
                        if(ac.QualNotaMediaENEM__c >= ac.Processo_Seletivo__r.Nota_M_nima__c){
                            ead.StageName__c = 'Inscrito';
                            
                            List<String> idOpp = new List<String>();
                            
                            idOpp.add(ac.Id);
                            
                            SRM_CadastroCandidato.insertQueue(ac.Id);
                                                        
                        }else{
                            ead.StageName__c = 'Inscrito';
                        }
                    }
                    else{
                        ead.StageName__c = 'Inscrito';
                    }

                }else{

                    if(ac.StageName != null){ ead.StageName__c = ac.StageName;}
                
                }

                if(ac.Polo__r.Email__c != null){ead.Email_Polo__c = ac.Polo__r.Email__c;}

                ead.StatusContato__c = 'Não';

                ead.Data_de_Inscri_o__c = system.Today();

                ead.Agendamentos__c = 0;

                ead.SituacaoMatricula__c = '-';

                
                oppToInsert.add(ead);
        }
    
    }

    insert oppToInsert;

}