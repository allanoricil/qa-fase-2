trigger CampusSistemaTrigger on CampusSistema__c (before insert, before update) {
    
    List<String> juctionIDsCampus = new List<String>();
    List<String> juctionIDsSistema = new List<String>();
    
     for (CampusSistema__c campusSistema: Trigger.New){
        juctionIDsCampus.add(campusSistema.Campus__c);
        juctionIDsSistema.add(campusSistema.Sistema__C);
    }
    
    List<CampusSistema__c> campusSistemas = [Select ID,campus__c, sistema__c from CampusSistema__c];
    
    for(Integer i = 0 ; i< Trigger.New.size(); i++){
        
        for (CampusSistema__c csc : campusSistemas){
            if(csc.campus__c.equals(Trigger.New[i].campus__c )
               && csc.sistema__c.equals( Trigger.New[i].sistema__c)){
                    Trigger.New[i].addError('Record Duplicate');   
               }
                
        }
            
    }
}