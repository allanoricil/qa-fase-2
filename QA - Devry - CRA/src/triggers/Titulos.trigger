/*
    @author Diego Moreira
    @trigger Trigger do objeto titulos
*/
trigger Titulos on Titulos__c ( after update, after insert ) {

	if( trigger.isInsert){
		if(trigger.isAfter){
			TituloBO.getInstance().createQueueToInsertTitles( trigger.new );
		}
	}

	if( trigger.isUpdate ) {
		if( trigger.isAfter ) {
			  for (Titulos__c a : trigger.new){
				  system.debug('entrada no método');

                  if(a.CodigoAutorizacao__c !='' && (a.CodigoSap__c == null || a.CodigoSap__c == '' || a.CodigoSap__c == '$')){
                      system.debug('enviou para fila de criação após update');
                      TituloBO.getInstance().createQueueToInsertTitles( trigger.new ); 
                      }

                  if(!a.firstSap__c)
                     TituloBO.getInstance().createQueueToUpdateTitles(trigger.new);
                  
                 
	       }
			
			/*

			//trecho comentado 
			if( !ProcessorControl.inFutureContext ) {
				if(ProcessorControl.sincronoContext == false){
					system.debug('future on');
					//TituloBO.getInstance().createQueueToInsertTitles( trigger.new );
					TituloBO.getInstance().createQueueToUpdateTitles( trigger.new );
				}
			}
			*/
			// if(ProcessorControl.sincronoContext){
				//system.debug('future off');
				//TituloBO.getInstance().createQueueToInsertTitlesSyncrono( trigger.new );
			
			TituloBO.getInstance().atualizaFaseOportunidadeTituloPago( trigger.new );
		}	
	}

}