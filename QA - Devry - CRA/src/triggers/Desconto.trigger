trigger Desconto on Desconto__c (before insert, before update) {
    for (Desconto__c desconto : trigger.new){
        
        if(trigger.isInsert)
        {
            List<Desconto__c> lstDesconto = new List<Desconto__c>();
            lstDesconto = [SELECT Id FROM Desconto__c
                           WHERE Institui_o__c =: desconto.Institui_o__c AND Campus__c =: desconto.Campus__c AND N_vel_de_Ensino__c =: desconto.N_vel_de_Ensino__c LIMIT 1];
            if(!lstDesconto.isEmpty()){
                desconto.addError('Este Desconto já existe para o Nível de Ensino associado a esta Instituição/Campus.');
                break;
            }
        }
        
        else if(trigger.isUpdate)
        {
            Desconto__c oldDesconto = Trigger.oldMap.get(desconto.ID);                                                                  
            if(oldDesconto.Institui_o__c != desconto.Institui_o__c || oldDesconto.Campus__c != desconto.Campus__c || oldDesconto.N_vel_de_Ensino__c != desconto.N_vel_de_Ensino__c)
            {
                desconto.addError('Não é permitido modificar os campos: Instituição / Campus / Nível de Ensino');
                break;
            }
        }
        
        desconto.Name = InstituicaoDAO.getInstance().getInstituicaoById(desconto.Institui_o__c)[0].Name + ' / ' + CampusDAO.getInstance().getCampusById(desconto.Campus__c)[0].Name + ' / ' + desconto.N_vel_de_Ensino__c;
    }
}