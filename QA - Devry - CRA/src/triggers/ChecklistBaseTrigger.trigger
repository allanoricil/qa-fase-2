trigger ChecklistBaseTrigger on Opportunity (after insert, after update) {
    ChecklistBase__c objBase = New ChecklistBase__c() ;

    for (Opportunity op : Trigger.New) {
        if ((op.Tipo_de_Matr_cula__c == 'PROUNI') || (op.Tipo_de_Matr_cula__c == 'FIES')) {     
            if (Trigger.isinsert) {  
                Account acx = [Select Name, CPF_2__c From Account Where Id =: op.AccountId limit 1];
                
                objBase.OportunidadeID__c = op.Id;
                objBase.AccountName__c = acx.Name;
                objBase.CPF__c = acx.CPF_2__c;

                DateTime dtCriacao = op.CreatedDate;
                Date novaDtCriacao = date.newinstance(dtCriacao.year(), dtCriacao.month(), dtCriacao.day());

                objBase.CreatedDate__c = novaDtCriacao;
                objBase.ProcessoSeletivoIES__c = op.IES__c;
                objBase.ProcessoSeletivoName__c = op.Processo_seletivo_Nome__c;
                objBase.TipoMatricula__c = op.Tipo_de_Matr_cula__c;

                try {
                    insert objBase;
                } catch (DMLException e){
                    op.addError('Ocorreu um erro ao inserir o Checklist. Detalhes do erro: ' + e.getMessage());
                    CreateErrorLog.createErrorRecord(e.getMessage(), 'ChecklistBaseTrigger');
                }
            }

            if (Trigger.isUpdate) {
                List<ChecklistBase__c> ckb = [select OportunidadeID__c from ChecklistBase__c where OportunidadeID__c =: op.Id limit 1];
                If (ckb.isEmpty()) {
                    Account acx = [Select Name, CPF_2__c From Account Where Id =: op.AccountId limit 1];
                    objBase.OportunidadeID__c = op.Id;
                    objBase.AccountName__c = acx.Name;
                    objBase.CPF__c = acx.CPF_2__c;

                    DateTime dtCriacao = op.CreatedDate;
                    Date novaDtCriacao = date.newinstance(dtCriacao.year(), dtCriacao.month(), dtCriacao.day());

                    objBase.CreatedDate__c = novaDtCriacao;
                    objBase.ProcessoSeletivoIES__c = op.IES__c;
                    objBase.ProcessoSeletivoName__c = op.Processo_seletivo_Nome__c;
                    objBase.TipoMatricula__c = op.Tipo_de_Matr_cula__c;

                    try {
                        insert objBase;
                    } catch (DMLException e){
                        op.addError('Ocorreu um erro ao atualizar o Checklist. Detalhes do erro: ' + e.getMessage());
                        CreateErrorLog.createErrorRecord(e.getMessage(), 'ChecklistBaseTrigger');
                    }
                }
            }           
        }
    }
}