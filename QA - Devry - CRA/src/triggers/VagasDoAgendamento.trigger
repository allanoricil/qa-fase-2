/*******************************************************************
Author    :   Adílio Santos
Date      :   Outubro 2016
Purpose   :   Trigger
*******************************************************************/
trigger VagasDoAgendamento on Opportunity (before insert, before update) {    
    
    for(Opportunity opp : Trigger.new){
              
        if(opp.TipoProcesso__c == 'Vestibular Agendado' || opp.TipoProcesso__c == 'Vestibular Especial' || opp.Tipo_de_Matr_cula__c == 'Pós-Graduação IBMEC' || opp.Tipo_de_Matr_cula__c == 'Pós-Graduação'){
                                 
            set<ID>cObjectID = new set<ID>();
            cObjectID.add(opp.Agendamento__c);
            
            List<Agendamento__c> lstAgendamento = [Select ID, Name, Institui_o__c, Campus__c, Vagas__c From Agendamento__c a Where a.ID =: cObjectID limit 1];
            
            string identificador = null;            
            string instituicao = null;
            string campus = null;
            
            if (!lstAgendamento.isEmpty()){
                identificador = lstAgendamento[0].ID;            
                instituicao = lstAgendamento[0].Institui_o__c;
                campus = lstAgendamento[0].Campus__c;
                
                identificador = identificador.substring(0,15);
                instituicao = instituicao.substring(0,15);
                campus = campus.substring(0,15);
                
                system.debug('Select da opp0: '+lstAgendamento[0].ID);
                system.debug('Select da opp1: '+lstAgendamento[0].Name);
                system.debug('Select da opp2: '+lstAgendamento[0].Institui_o__c);
                system.debug('Select da opp3: '+lstAgendamento[0].Campus__c);
                system.debug('Select da opp4: '+lstAgendamento[0].Vagas__c);            
                system.debug('Select da opp5: '+identificador);
                system.debug('Select da opp6: '+instituicao);
                system.debug('Select da opp7: '+campus);
            }  
            
            if( trigger.isInsert){
                if(!lstAgendamento.isEmpty()){
                    integer qtdInscricoesNoAgendadmento = [Select Count() From Opportunity o Where
                                                           o.IdInstituicao__c =: instituicao and
                                                           o.Id_Campus__c =: campus and
                                                           o.Agendamento__c =: identificador];
                            
                    if(lstAgendamento[0].Vagas__c>qtdInscricoesNoAgendadmento){
                        lstAgendamento[0].Vagas__c = lstAgendamento[0].Vagas__c - 1;
                        update lstAgendamento;
                        break;
                    }
                }
            }                              
                
            if( trigger.isUpdate ){
                
                Opportunity oldOpp = Trigger.oldMap.get(opp.Id);                                                        
                system.debug('oldOpp: '+oldOpp.Agendamento__c);
                system.debug('newOpp' + opp.Agendamento__c);  
                
                List<Agendamento__c> lstOldAgendamento = [Select ID, Vagas__c From Agendamento__c a Where a.ID =: oldOpp.Agendamento__c limit 1];                    
                
                integer cadastradoNoAgendamento = [Select Count() From Opportunity o Where
                                                   o.IdInstituicao__c =: instituicao and
                                                   o.Id_Campus__c =: campus and
                                                   o.Agendamento__c =: identificador and
                                                   o.Id =: opp.Id limit 1];
                
                system.debug('oppID: '+opp.Id);
                system.debug('cadastradoNoAgendamento: '+cadastradoNoAgendamento);
                
                //Se a Opp não estiver associada ao Agendamento
                if(cadastradoNoAgendamento<1){
                    if(!lstAgendamento.isEmpty()){
                        integer qtdInscricoesNoAgendadmento = [Select Count() From Opportunity o Where
                                                               o.IdInstituicao__c =: instituicao and
                                                               o.Id_Campus__c =: campus and
                                                               o.Agendamento__c =: identificador];
                        
                        system.debug('qtdInscricoesNoAgendadmento: '+qtdInscricoesNoAgendadmento);                        
                        
                        if(lstAgendamento[0].Vagas__c>qtdInscricoesNoAgendadmento){
                            lstAgendamento[0].Vagas__c = lstAgendamento[0].Vagas__c - 1;
                            
                            if(!lstOldAgendamento.isEmpty()){
                                lstOldAgendamento[0].Vagas__c = lstOldAgendamento[0].Vagas__c + 1;
                                update lstOldAgendamento;
                            }
                            
                            update lstAgendamento;
                            break;
                        }
                    }                   
                
                    if(!lstOldAgendamento.isEmpty()){
                        lstOldAgendamento[0].Vagas__c = lstOldAgendamento[0].Vagas__c + 1;
                        update lstOldAgendamento;
                        break;
                    }
                }
            }
        }
    }
}