trigger CampanhasAcompanhamentos on Configura_es_do_Per_odo_Letivo__c (after update) {

    String conf = trigger.new[0].id;
    Batch_EmailCampanhasCASA controller = new Batch_EmailCampanhasCASA(conf) ;  
    Integer batchSize = 1;  
    database.executebatch(controller , batchSize);
      
}