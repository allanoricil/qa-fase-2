/*
	@author Diego Moreira
	@class trigger do processo seletivo
*/
trigger Processo_seletivo on Processo_seletivo__c ( before insert, before update ) {

	if( trigger.isBefore ) {
		Processo_seletivoBO.getInstance().atualizaProcessoLetivo( trigger.new );
	}

}