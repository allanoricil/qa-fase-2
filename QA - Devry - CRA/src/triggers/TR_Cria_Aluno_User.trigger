trigger TR_Cria_Aluno_User on Academico__c (after insert, after update) {

    if (trigger.new.size() < 50)
    {
        User admin = [Select id From User Where alias = 'D_Loader' and isactive = true limit 1];
         
        for (Academico__c ac : trigger.new)
        {   
            Account acc = [Select Id, PersonContactId,FirstNAme, Lastname, Sexo__c, PersonMobilePhone, PersonEmail, RG__c,
            				CPF_2__c, DataNascimento__c, Nome_da_Mae__c, Nome_do_Pai__c, Rua__c, Cidade__c, Estado__c, N_mero__c, Bairro__c,CEP__c From Account Where id =: ac.Aluno__c limit 1];
            
            //verificar se aluno existe antes de inserir
            if(acc!=null &&[select count() from Aluno__c where R_A_do_Aluno__c =: ac.Name] == 0){
            try{
            Aluno__c aluno = new Aluno__c();
            
            aluno.Name = acc.Lastname;
            aluno.OwnerId = admin.id;//String.valueOf(admin);
            aluno.Sexo__c = acc.Sexo__c;
            aluno.Telefone__c = acc.PersonMobilePhone;
            aluno.Celular__c = acc.Sexo__c;
            aluno.R_A_do_Aluno__c = ac.Name;
            aluno.e_mail__c = acc.PersonEmail;
            aluno.RG__c = acc.RG__c;
            aluno.CPF__c = acc.CPF_2__c.replace('.','').replace('-','');
            aluno.Data_de_Nascimento__c = acc.DataNascimento__c;
            aluno.Nome_da_M_e__c = acc.Nome_da_Mae__c;
            aluno.Nome_do_Pai__c = acc.Nome_do_Pai__c;
            aluno.Curso__c = ac.Curso__c;
            aluno.Status_de_Pagamento__c = '';
            aluno.Turno__c = ac.Turno__c;
            aluno.Institui_o_n__c = ac.IES__c;
            aluno.Campus_n__c = ac.Campus__c;
            aluno.Situa_o_da_Matr_cula__c = ac.SituacaoMatricula__c;
            aluno.Data_Situa_o__c = Date.valueOf(ac.DataSituacao__c);
            aluno.Rua__c = acc.Rua__c;
            aluno.Cidade__c = acc.Cidade__c;
            aluno.Estado__c = acc.Estado__c;
            aluno.Numero__c = acc.N_mero__c;
            aluno.Bairro__c = acc.Bairro__c;
            aluno.CEP__c = acc.CEP__c;
            
            insert aluno;
            }catch(Exception e){
                  Error_Log__c e1 = new Error_Log__c(Source_Code__c = ac.id, Details__c =Datetime.now()+'Error in aluno' + String.valueOf(e));
                  insert e1;     
            }
            }
            
            if(acc != null || string.valueOf(acc.id) != ''){
            try{
            acc.OwnerId = admin.id;
            update acc;
            } catch(Exception e){
                   Error_Log__c e1 = new Error_Log__c(Source_Code__c = ac.id, Details__c =Datetime.now()+'Error in Account :'+ String.valueOf(e));
                  insert e1;   
            }
            }
            
            //verificar se conta existe existe antes de inserir
            if([select count() from User where Username =: ac.Name + '@com.br'] == 0){
            try{      
            User cra = new User();
            cra.Username= ac.Name + '@com.br';
            cra.Email = acc.PersonEmail;
            cra.Lastname = acc.LastName;
            cra.Firstname = acc.FirstNAme;
            cra.Alias = 'test1';
            cra.CommunityNickname =ac.AutoNick__c;// ac.Name+'testeNickName';
                System.debug ('Random=>>>'+ ac.AutoNick__c );
            //cra.UserRole = [ select id from userrole where id ='00Ee0000000LhpB' ];
            cra.ProfileId = [ select id from profile where Name = 'CRA - Aluno' ].id;
            cra.ContactId = acc.PersonContactId;
            //cra.CurrencyIsoCode = 'USD';
            cra.TimeZoneSidKey = 'GMT';
            cra.LocaleSidKey = 'pt_BR';
            cra.EmailEncodingKey = 'ISO-8859-1';
            cra.LanguageLocaleKey = 'pt_BR'; 
            cra.UserPermissionsMobileUser = false;
                //a propriedade que esta sendo setada como ra é o name e nao o RA__c
            cra.DBNumber__c = ac.Name;
           System.debug ('RA=>>>'+ ac.RA__c );
            
            insert cra;
            } catch(Exception e){
                 Error_Log__c e1 = new Error_Log__c(Source_Code__c = ac.id, Details__c = Datetime.now()+' error in User '+ String.valueOf(e));
                 insert e1;   
            }
            }
       }
    }
}