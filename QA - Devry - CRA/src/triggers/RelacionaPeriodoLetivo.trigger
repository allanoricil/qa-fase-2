trigger RelacionaPeriodoLetivo on Atendimento_CASA__c (before insert, before update, after update, after insert) {

    String emtId, userId;

    if (trigger.new.size() < 20 && trigger.isbefore){
        
        // If it's in Production Org, otherwise...
        if(UserInfo.getOrganizationId() == '00DA0000000b61vMAA'){
            //User u = [Select Id From User Where Name =: 'Servico Data Loader' limit 1];
            userId = '005A0000000sMPiIAM';        
            //EmailTemplate emt = [Select id From EmailTemplate Where Name =: 'Apresentacao CASA - Oficial' limit 1];        
            emtId = '00XA0000001duKGMAY';    
        }
        else{
            //User u = [Select Id From User Where Name =: 'Servico Data Loader' limit 1];
            userId = '005A0000000sMPiIAM';
            //EmailTemplate emt = [Select id From EmailTemplate Where Name =: 'Apresentacao CASA - Oficial' limit 1];        
            emtId = '00XK0000000DhmeMAC';    
        }
        List<Atendimento_CASA__c> acx = new List<Atendimento_CASA__c>();    
        for (Atendimento_CASA__c ac : trigger.new){
            if (ac.Configura_es_do_Per_odo_Letivo__c == null){
                if ([Select count() From Configura_es_do_Per_odo_Letivo__c Where Per_odo_Letivo__c =: ac.Per_odo_Letivo__c] > 0){
                    Configura_es_do_Per_odo_Letivo__c cpl = [Select id From Configura_es_do_Per_odo_Letivo__c Where Per_odo_Letivo__c =: ac.Per_odo_Letivo__c limit 1];
                    ac.Configura_es_do_Per_odo_Letivo__c = cpl.id;
                }
            }
            if([SELECT count() FROM ALUNO__C WHERE IDUSUARIO__C =: ac.IDUSUARIO__C] > 0 && ac.Aluno__c == null){
                Aluno__c a = [SELECT ID FROM ALUNO__C WHERE IDUSUARIO__C =: ac.IDUSUARIO__C limit 1];
                ac.Aluno__c = a.id;
            }
            if (ac.CodPeriodo__c != null && ac.Per_odo_Letivo__c == null){
                Per_odo_Letivo__c a = [SELECT ID FROM Per_odo_Letivo__c WHERE CodPeriodo__c =: ac.CodPeriodo__c limit 1];
                ac.Per_odo_Letivo__c = a.id;
            }            
        }
    }
    List<String> emails = new List<String>();        
    List<ID> ids = new List<ID>();
    String NomeOrientador, EmailOrientador;
/*
    for (Atendimento_CASA__c acx : trigger.new){
        if (acx.OwnerId != userId && acx.Grupo_de_Risco__c != null && acx.Status_do_Per_odo_Letivo__c == 'Ativo' && 
            acx.Data_de_Envio_de_Email_de_Apresenta_o__c == null && acx.Id_do_Contato__c != null){            
            if (trigger.isbefore){
                acx.Data_de_Envio_de_Email_de_Apresenta_o__c = system.today(); 
            }
            emails.add(acx.Email_Aluno__c);
            System.debug(acx.Email_Aluno__c);
            ids.add(acx.Id);
            NomeOrientador = acx.Owner.Name;
            EmailOrientador = acx.Owner.Email;
            //emailHelper.sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, String ReplyTo, String SenderDisplayName, String[] setToAddresses, String Subject, String PlainTextBody) {            
            EmailHelper.sendEmail(acx.Id_do_Contato__c, emtId, acx.Id, true, false, EmailOrientador, NomeOrientador, emails, null, null);                                            
        }
    }
*/
}