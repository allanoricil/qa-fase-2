trigger CaseTrigger on Case (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

	if (Trigger.isBefore)
        if (Trigger.isUpdate)
            CaseTriggerHandler.getInstance().beforeUpdate();
        else if (Trigger.isInsert)
            CaseTriggerHandler.getInstance().beforeInsert();
        else
            CaseTriggerHandler.getInstance().beforeDelete();
    else
        if (Trigger.isUpdate)
            CaseTriggerHandler.getInstance().afterUpdate();
        else if (Trigger.isInsert)
            CaseTriggerHandler.getInstance().afterInsert();
        else
            CaseTriggerHandler.getInstance().afterDelete();
}