trigger Cobranca on Cobranca__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isBefore)
	        if (Trigger.isUpdate)
	            CobrancaTriggerHandler.getInstance().beforeUpdate();
	        else if (Trigger.isInsert)
	            CobrancaTriggerHandler.getInstance().beforeInsert();
	        else
	            CobrancaTriggerHandler.getInstance().beforeDelete();
	    else
	        if (Trigger.isUpdate)
	            CobrancaTriggerHandler.getInstance().afterUpdate();
	        else if (Trigger.isInsert)
	            CobrancaTriggerHandler.getInstance().afterInsert();
	        else
	            CobrancaTriggerHandler.getInstance().afterDelete();
}