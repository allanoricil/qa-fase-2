//trigger Task on Task (before update, before insert, before delete, after update, after insert, after delete) {
//  if( trigger.isAfter ) {
//      if( trigger.isUpdate ){
//          if(trigger.new[0].WhatId != null){
//              TaskBO.getInstance().taskToOpportunity( trigger.new);
//          }else{
//              TaskBO.getInstance().taskToLead( trigger.new);
//          }   
//      }
//  }
//}
trigger Task on Task(before update, before insert, before delete, after update, after insert, after delete) {
    if (Trigger.isBefore)
        if (Trigger.isUpdate)
            TaskTriggerHandler.getInstance().beforeUpdate();
        else if (Trigger.isInsert)
        TaskTriggerHandler.getInstance().beforeInsert();
    else
        TaskTriggerHandler.getInstance().beforeDelete();
    else
    if (Trigger.isUpdate) {
        TaskTriggerHandler.getInstance().afterUpdate();
        if (trigger.new[0].WhatId != null) {
            TaskBO.getInstance().taskToOpportunity(trigger.new);
        } else {
            TaskBO.getInstance().taskToLead(trigger.new);
        }
    } else if (Trigger.isInsert)
        TaskTriggerHandler.getInstance().afterInsert();
    else
        TaskTriggerHandler.getInstance().afterDelete();

    if (trigger.isAfter && !Trigger.isDelete) {

        Task t;
        for (Task a: trigger.new) {
            t = [select id, subject, Lista_de_A_es__c from Task where id =: a.id limit 1];
        }

        if (t.Lista_de_A_es__c!=null && t.subject != t.Lista_de_A_es__c) {
            t.subject = t.Lista_de_A_es__c;
            update t;
        }
    }
}