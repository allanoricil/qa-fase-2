/*
    @author Diego Moreira
    @class trigger do objeto oportunidade

    @alterado por Adriano Ferreira em agosto/2017
*/

trigger Opportunity on Opportunity (before update, before insert, before delete, after update, after insert, after delete) {
     System.debug('Entrada na Trigger');
    if (Trigger.isBefore)
        if (Trigger.isUpdate){
            OpportunityTriggerHandler.getInstance().beforeUpdate();
            if(ApproveOpportunityHandler.oneTime()) // Evita recursividade no código
                ApproveOpportunityHandler.getInstance().beforeUpdate(); // Aprovar Candidato RM - CCD
        }
        else if (Trigger.isInsert)
            OpportunityTriggerHandler.getInstance().beforeInsert();
        else
            OpportunityTriggerHandler.getInstance().beforeDelete();
    else
        if (Trigger.isUpdate)
            OpportunityTriggerHandler.getInstance().afterUpdate();
        else if (Trigger.isInsert)
            OpportunityTriggerHandler.getInstance().afterInsert();
        else
            OpportunityTriggerHandler.getInstance().afterDelete(); 
}