trigger ItemDoCaso on Item_do_Caso__c (before insert, before update, before delete, after insert, after update, after delete) {

    if (Trigger.isBefore && Trigger.isInsert)
		ItemDoCasoTriggerHandler.handleBeforeInsert(Trigger.new);
	
	else if (Trigger.isBefore && Trigger.isUpdate)
		ItemDoCasoTriggerHandler.handleBeforeUpdate(Trigger.new, Trigger.old);

	else if (Trigger.isBefore && Trigger.isDelete)
		ItemDoCasoTriggerHandler.handleBeforeDelete(Trigger.old);

    if (Trigger.isAfter && Trigger.isInsert)
		ItemDoCasoTriggerHandler.handleAfterInsert(Trigger.new);
	
	else if (Trigger.isAfter && Trigger.isUpdate)
		ItemDoCasoTriggerHandler.handleAfterUpdate(Trigger.new, Trigger.old);

	else if (Trigger.isAfter && Trigger.isDelete)
		ItemDoCasoTriggerHandler.handleAfterDelete(Trigger.old);

}