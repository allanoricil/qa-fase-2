trigger SetConta on Aluno__c (before insert, before update) {

    if (trigger.new.size() < 50){
        RecordType rec = [Select id From RecordType Where name =: 'Conta Pessoal' limit 1];
        User admin = [Select id From User Where Name = 'Weengo Consultoria' limit 1];
        List<String> sup = new List<String>{'rafael@weengo.com'};        
        for (Aluno__c a : trigger.new){
            String cpf = a.CPF__c;

            if(a.Conta__c != null){
              Account act=[select id,PersonEmail from Account where id=:a.Conta__c limit 1] ;
               act.PersonEmail=a.e_mail__c;
               update act;
           }
            
            
            if (a.CPF__c != null && a.Conta__c == null){
                if (cpf.length() <= 11){
                    cpf = cpf.substring(0,3) + '.' + cpf.substring(3,6) + '.' + cpf.substring(6,9) + '-' + cpf.substring(9,11);
                    a.CPF__c = cpf;
                }        
                if ([Select count() From Account Where CPF_2__c =: a.CPF__c] > 0){
                    Account acx = [Select Id, PersonContactId From Account Where CPF_2__c =: a.CPF__c limit 1];
                    a.Conta__c = acx.Id;
                    a.Id_do_Contato__c = acx.PersonContactId;
                }
                else {
                    Account ac = new Account();
                    ac.recordtypeId = rec.Id;
                    if (a.Name.indexof(' ') > 0){
                        ac.FirstName = a.Name.substring(0, a.Name.indexof(' '));
                        ac.LastName = a.Name.substring(a.Name.indexof(' '), a.Name.length());
                    }
                    else {
                        ac.FirstName = a.Name;
                        ac.LastName = '-';
                    }
                    
                    ac.PersonEmail = a.E_mail__c;
                    ac.PersonMobilePhone = a.Celular__c;
                    ac.Phone = a.Telefone__c;
                    ac.RG__c = a.RG__c;
                    ac.Nome_da_Mae__c = a.Nome_da_M_e__c;
                    ac.Nome_do_Pai__c = a.Nome_do_Pai__c;
                    ac.Rua__c = a.Rua__c;
                    ac.Cidade__c = a.Cidade__c;
                    ac.Estado__c = a.Estado__c;
                    ac.N_mero__c = a.Numero__c;
                    ac.CEP__c = a.CEP__c;
                    ac.Complemento__c = a.Complemento__c;
                    ac.Bairro__c = a.Bairro__c;
                    ac.Sexo__c = a.Sexo__c;
                    ac.PersonBirthDate = a.Data_de_Nascimento__c;
                    ac.R_A_do_aluno__c = a.R_A_do_aluno__c;
                    ac.CPF_2__c = a.CPF__c;                

                    try{                    
                        insert ac;                    
                        a.Conta__c = ac.Id;
                        Account x = [Select id, PersonContactId From Account Where id =: ac.id limit 1];
                        a.Id_do_Contato__c = x.PersonContactId;           
                    }
                    catch (DMLException e){
                        a.addError('Ocorreu um erro inserindo registros de Contas/Candidatos, através de Alunos. Verifique se estiver duplicado. O CPF é : ' + a.CPF__c + '. Detalhes do erro: ' + e.getMessage());
                        CreateErrorLog.createErrorRecord(e.getMessage(), 'SetConta');                
                        emailHelper.sendEmail(admin.Id, null, null, false, true, null, 'DeVry Weengo Suporte', sup, 'Erro na trigger SetConta', e.getMessage());
                        //emailHelper.sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, String ReplyTo, String SenderDisplayName, String[] setToAddresses, String Subject, String PlainTextBody) {
                    }                        
                }
            }
        }
    }
}