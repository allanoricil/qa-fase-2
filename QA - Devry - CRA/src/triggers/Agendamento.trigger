/*******************************************************************
Author    :   Adílio Santos
Date      :   Outubro 2016
Purpose   :   Trigger
*******************************************************************/
trigger Agendamento on Agendamento__c (before insert, before update) {
    for (Agendamento__c a : trigger.new){
        if(trigger.isInsert){
            a.Vagas__c = a.Capacidade__c;
            break;
        }
        
        if(trigger.isUpdate){
            Agendamento__c oldAgendamento = Trigger.oldMap.get(a.ID);                                                        
            system.debug('oldAgendamento: '+ oldAgendamento.Capacidade__c);
                    
            if(oldAgendamento.Capacidade__c != a.Capacidade__c){                
                a.Vagas__c = a.Capacidade__c - (oldAgendamento.Capacidade__c - oldAgendamento.Vagas__c);                
                break;
            }
        }
    }
}