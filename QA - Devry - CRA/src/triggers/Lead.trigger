/*
	@author Diego Moreira
	@class trigger de lead
*/
trigger Lead on Lead ( before insert, before update, after insert, after update ) {
	if( trigger.isBefore ) {
		if( trigger.isInsert ) {
			LeadBO.getInstance().autalizaInstituicao( trigger.new );
            CRA_LeadUtil.setPoloLeadFicha((List<Lead>)Trigger.new);
      		CRA_LeadUtil.setCampusLeadFicha((List<Lead>)Trigger.new);
            CRA_OwnerFranquia.setOwnerFranquiaLead((List<Lead>)Trigger.new);
			CRA_PhoneUtil.fixLeadPhone((List<Lead>)Trigger.new);
		}
		if( trigger.isUpdate ) {
			LeadBO.getInstance().autalizaInstituicao( trigger.new );
			CRA_PhoneUtil.fixLeadPhone((List<Lead>)Trigger.new);
		}
	} else if( trigger.isAfter ) {
		if( trigger.isInsert ) {
			LeadBO.getInstance().atribuiRegraAtiva( trigger.new );
			//LeadBO.getInstance().createCheckPhoneQueue( trigger.new );
		} if( trigger.isUpdate ) {
			//LeadBO.getInstance().createCheckPhoneQueue( trigger.new, trigger.oldMap );
		}
	} 

}