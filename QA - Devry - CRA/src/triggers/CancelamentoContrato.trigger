trigger CancelamentoContrato on Cancelamento__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isBefore) {
	    	//call your handler.before method
	    	if(Trigger.isInsert){
	    		for(Cancelamento__c canc: Trigger.new){
	    			string jsonBodySap = Cancelamentoutil.createClientToJson(canc.Aluno__c);
	    			String queueidSap = QueueBO.getInstance().createQueueNotSchedule(QueueEventNames.CRIA_CLIENTE_SAP.name(), jsonBodySap);
	    			canc.Queue_ClienteSap__c = queueidSap;

	    			canc = Cancelamentoutil.preencheDadosRm(canc);
	    			String queueidRm = QueueBO.getInstance().createQueueNotSchedule(QueueEventNames.RETIRA_FATURAMENTO_RM.name(), 'retira do faturamento');
	    			canc.Queue_RetiraFaturamentoRM__c = queueidRm;
	    			
	    		}
	    	}
		} else if (Trigger.isAfter) {
	    	//call handler.after method
	    	
		}
}