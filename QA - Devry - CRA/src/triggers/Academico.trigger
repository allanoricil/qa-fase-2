/*
    @author Adílio Santos
    @class trigger do objeto Informações academicas
*/
trigger Academico on Academico__c ( after insert ) {

    if( trigger.isAfter ) {
        if( trigger.isInsert )
            AcademicoBO.getInstance().atualizaFaseInscricao( trigger.new );
    }

}