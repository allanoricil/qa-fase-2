trigger PeriodoLetivo on Per_odo_Letivo__c (before update,before insert) {
    
     for(Per_odo_Letivo__c myper :(List<Per_odo_Letivo__c>)Trigger.new ){
          List<Per_odo_Letivo__c> perlets;
           perlets= [select id,name from  Per_odo_Letivo__c where  Status_CRA__c='Ativo'];
         if(perlets.size()>0 && myper.Status_CRA__c=='Ativo'){
             for(Per_odo_Letivo__c per :perlets){
                 if(myper.Id!=per.id){
                      myper.adderror('Só pode haver um período letivo com o StatusCRA como :Ativo, Por favor verifique o período: '+per.name+' ');
                     break;
                 }
                    
             }
             
         }
             
         
     }
    
      
}