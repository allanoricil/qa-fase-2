trigger Aluno on Aluno__c (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {

		if (Trigger.isBefore)
	        if (Trigger.isUpdate)
	            AlunoTriggerHandler.getInstance().beforeUpdate();
	        else if (Trigger.isInsert)
	            AlunoTriggerHandler.getInstance().beforeInsert();
	        else
	            AlunoTriggerHandler.getInstance().beforeDelete();
	    else
	        if (Trigger.isUpdate)
	            AlunoTriggerHandler.getInstance().afterUpdate();
	        else if (Trigger.isInsert)
	            AlunoTriggerHandler.getInstance().afterInsert();
	        else
	            AlunoTriggerHandler.getInstance().afterDelete();
}