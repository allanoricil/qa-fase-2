trigger ReagendamentoTrigger on Reagendamento__c (after delete) {
    
    System.debug('ANTES DE DELETAR UM REAGENDAMENTO');
    Set<Id> oppIds = new Set<Id>();
    for(Reagendamento__c reagendamento: Trigger.Old){
        //por algum motivo sem sentido essa variavel com esse nome é um lookup de Opportunity
        oppIds.add(reagendamento.Reagendamento_del__c);
    }

    List<Opportunity> opportunitiesToUpdate = [SELECT Id, 
                                                      numero_de_agendamentos__c
                                                 FROM Opportunity
                                                WHERE Id IN: oppIds];

    for(Opportunity opp : opportunitiesToUpdate){
        if(opp.numero_de_agendamentos__c > 0){
            opp.numero_de_agendamentos__c--;
        }   
    }
    
    update opportunitiesToUpdate;
    
}