/*
    @author Diego Moreira
    @class Classe de teste SRM_BoletoBO
*/
@isTest
private class SRM_BoletoBOTest {
	private static String sequencial = '0000000001';

	@isTest static void test_HSBC() {		
		String nossoNumero = SRM_BoletoBO.getInstance().getNossoNumero( '08', sequencial );
		//System.assertEquals( 15, nossoNumero.length() );
	}
	
	@isTest static void test_SICOB() {
		String nossoNumero = SRM_BoletoBO.getInstance().getNossoNumero( '07', sequencial );
		//System.assertEquals( 19, nossoNumero.length() );
	}

	@isTest static void test_SICCB() {
		String nossoNumero = SRM_BoletoBO.getInstance().getNossoNumero( '551', sequencial );
		//System.assertEquals( 12, nossoNumero.length() );
	}

	@isTest static void test_SANTANDER() {
		String nossoNumero = SRM_BoletoBO.getInstance().getNossoNumero( '124', sequencial );
		//System.assertEquals( 14, nossoNumero.length() );
	}
	
}