/*
    @author Diego Moreira
    @class Classe de negocio de geração de boleto
*/
public class SRM_BoletoBO {
	/*
        Singleton
    */
    private static final SRM_BoletoBO instance = new SRM_BoletoBO();    
    private SRM_BoletoBO(){}
    
    public static SRM_BoletoBO getInstance() {
        return instance;
    }

    private static Map<String, String> mapBanco;
    Static{
        mapBanco = new Map<String, String>();
        mapBanco.put( '08', 'HSBC' );
        mapBanco.put( '07', 'CAIXA-SICOB' );
        mapBanco.put( '551', 'CAIXA-SICCB' );
        mapBanco.put( '124', 'SANTANDER' );
    }

    /*
        HSBC (FANOR, ÁREA1, RUY, FBV e FMF) - Devry São Luiz está dentro da estrutura da FANOR.
        CAIXA - SICOB (UNIFAVIP) - MO67119007
        CAIXA - SICCB (FACID) - MO67021007
        SANTANDER (DeVry JP)

        Obs.: Vale ressaltar que todas as nossas cobranças são "SEM REGISTRO"
    */
	public String getNossoNumero( String codBanco, String sequencial ) {
        String nossoNumero = '';       
        Date dtToday = Date.today();

        if( mapBanco.get( codBanco ) == null ) return 'N/A';

        if( mapBanco.get( codBanco ).equals( 'CAIXA-SICCB' ) ) {
        	nossoNumero = '240' + dtToday.year() + sequencial;
        	nossoNumero += '-' + getDigitoNossoNumero( nossoNumero );
        } else if( mapBanco.get( codBanco ).equals( 'CAIXA-SICOB' ) ) {
        	nossoNumero = '82' + sequencial.right(8);
        	nossoNumero += '-' + getDigitoNossoNumero( nossoNumero );
        } else if( mapBanco.get( codBanco ).equals( 'SANTANDER' ) ) {
        	nossoNumero = sequencial;
        	nossoNumero += getDigitoNossoNumeroSantander( nossoNumero );
        } else if( mapBanco.get( codBanco ).equals( 'HSBC' ) ) {
        	nossoNumero = '900' + sequencial;
        	nossoNumero += getDigitoNossoNumeroInvertido( nossoNumero );
            nossoNumero += '4';
            nossoNumero += getDigitoNossoNumeroInvertido( nossoNumero );
        }

        return nossoNumero;
    }

    /*

    */
	private String getDigitoNossoNumero( String nossoNumero ) {
        Integer soma = 0;
        Integer mult = 2;
        Integer indice = nossoNumero.length();

        for( Integer a = 0; a < nossoNumero.length(); a++ ) {    
            soma += Integer.valueof(nossoNumero.subString(indice - 1, indice)) * mult;  
            System.debug('>>> ' + nossoNumero.subString(indice - 1, indice) + ' X ' + mult + ' = ' + soma);    
            if ( mult == 9 )
                mult = 2;
            else
                mult++;
            indice--;
        }

        soma = math.mod( soma, 11 );
        System.debug('>>> ' + soma);
        soma = 11 - soma;
        System.debug('>>> ' + soma);

        if ( soma > 9 ) 
            return '0';
        else
            return String.valueOf(soma);
    }

    private String getDigitoNossoNumeroSantander( String nossoNumero ) {
        Integer soma = 0;
        Integer mult = 2;
        Integer indice = nossoNumero.length();

        for( Integer a = 0; a < nossoNumero.length(); a++ ) {    
            soma += Integer.valueof(nossoNumero.subString(indice - 1, indice)) * mult;  
            System.debug('>>> ' + nossoNumero.subString(indice - 1, indice) + ' X ' + mult + ' = ' + soma);    
            if ( mult == 9 )
                mult = 2;
            else
                mult++;
            indice--;
        }

        soma = math.mod( soma, 11 );
        System.debug('>>> ' + soma);

        if(soma == 10)
            return '1';
        if(soma == 1 || soma == 0)
            return '0';

        soma = 11 - soma;
        return String.valueOf(soma);
   
            
    }


        /*

    */
    private String getDigitoNossoNumeroInvertido( String nossoNumero ) {
        Integer soma = 0;
        Integer mult = 9;
        Integer indice = nossoNumero.length();

        for( Integer a = 0; a < nossoNumero.length(); a++ ) {    

            soma += Integer.valueof(nossoNumero.subString(indice - 1, indice)) * mult;  
            System.debug('>>> ' + nossoNumero.subString(indice - 1, indice) + ' X ' + mult + ' = ' + soma);  
            if ( mult == 2 )
                mult = 9;
            else
                mult--;
            indice--;
        }

        soma = math.mod( soma, 11 );

        if ( soma > 9 ) 
            return '0';
        else
            return String.valueOf(soma);
    }
}