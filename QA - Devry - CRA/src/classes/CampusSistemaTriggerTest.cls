@IsTest(seeAllData=false)
public class CampusSistemaTriggerTest {
    static Campus__c campus;
    static Sistema__C sistema;
    @IsTest
    public static void shouldCreateCampusSistema(){
        init();
		CampusSistema__C css = new CampusSistema__C(Campus__C = campus.id, Sistema__C = sistema.id);
        
        insert css;
        
        System.assert(css.id != null);
        
    }
    @IsTest
    public static void shouldCreateOneCampusSistemaAndRefuseSecond(){
    	init();
        CampusSistema__C css = new CampusSistema__C(Campus__C = campus.id, Sistema__C = sistema.id);
        insert css;
        System.assert(css.id != null);
        css = new CampusSistema__C(Campus__C = campus.id, Sistema__C = sistema.id);
        try{
        	insert css;    
        }catch (DmlException e){
            System.assert(true);
        }
    }
    public static void init(){
        campus = new Campus__C(Name = 'CampusTest');
        campus.Id_Campus__c = '9999';
        insert campus;
        sistema = new Sistema__C(Name = 'SistemaTest');
        insert sistema;
    }

}