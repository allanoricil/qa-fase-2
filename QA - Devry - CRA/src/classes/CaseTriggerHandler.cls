public class CaseTriggerHandler implements TriggerHandlerInterface{
    private static final CaseTriggerHandler instance = new CaseTriggerHandler();
    /** Private constructor to prevent the creation of instances of this class.*/
    private CaseTriggerHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    **/

    public static CaseTriggerHandler getInstance() {
        return instance;
    }    
    
    /**
    * Method to handle trigger before update operations. 
    */
    public void beforeUpdate() {
        CRA_CaseUtil.ProcessCases((List<Case>)Trigger.new, (Map<Id,Case>)Trigger.oldMap);
        CRA_CaseUtil.setRecordType((List<Case>)Trigger.new, (Map<Id,Case>)Trigger.oldMap);
        CRA_CaseUtil.setQueueMilestone((List<Case>)Trigger.new, (Map<Id,Case>)Trigger.oldMap);
    }
    
    /**
    * Method to handle trigger before insert operations. 
    */ 
    public void beforeInsert() {
        CRA_CaseUtil.podeCancelar((List<Case>)Trigger.new);
        CRA_CaseUtil.barraCancEmAndamento((List<Case>)Trigger.new);
        CRA_CaseUtil.setRecordType((List<Case>)Trigger.new);
    }
    
    /**
    * Method to handle trigger before delete operations. 
    */
    public void beforeDelete() {

    }
    
    /**
    * Method to handle trigger after update operations. 
    */
    public void afterUpdate() {
        CRA_CaseUtil.processCasesAfter((Map<Id,Case>)Trigger.newMap, (Map<Id,Case>)Trigger.oldMap);
        CRA_CaseUtil.sendEmail((List<Case>)Trigger.new, (Map<Id,Case>)Trigger.oldMap);
        CRA_CaseUtil.assignmentRuleInlineEdit((Map<Id,Case>)Trigger.newMap, (Map<Id,Case>)Trigger.oldMap);
    }
    
    /**
    * Method to handle trigger after insert operations. 
    */
    public void afterInsert()  {
        CRA_CaseUtil.createCancelamento((List<Case>)Trigger.new, (Map<Id,Case>)Trigger.oldMap);
    }
    /**
    * Method to handle trigger after delete operations. 
    */
    public void afterDelete() {

    }

}