global class CRA_DocumentCloseCaseSchedule implements Schedulable{

	global void execute(SchedulableContext sc){
		
		CRA_DocumentCloseCaseBatch documentCloseCaseBatch = new CRA_DocumentCloseCaseBatch(); 
		Database.executeBatch(documentCloseCaseBatch, 10);

	}
	
}