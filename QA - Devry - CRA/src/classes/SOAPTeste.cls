public with sharing class SOAPTeste {
	public SOAPTeste() {
		CRA_soapSforceCom200509Outbound.Notification stub = new CRA_soapSforceCom200509Outbound.Notification();
		List<CRA_soapSforceCom200509Outbound.Oferta_xcNotification> notificacoesParaEnvio = new List<CRA_soapSforceCom200509Outbound.Oferta_xcNotification>();
	       
		List<Oferta__c> listOferta = [Select id, 
								  Ag_ncia__c,
							      C_digo_da_Area__c,
							      C_digo_da_Coligada__c,
							      C_digo_da_Oferta__c,
							      C_digo_do_Campus__c,
							      Conta__c,
							      Contrato__c,
							      Contrato_n__c,
							      Curso__c,
							      D_gito__c,
							      Filial__c,
							      ID_Academus_Oferta__c,
							      ID_Habilitacao_Filial__c,
							      Id_Campus__c,
							      Id_Institui_o__c,
							      Institui_o__c,
							      Institui_o_n__c,
							      IsDeleted,
							      Name,
							      Nome_do_Contrato__c,
							      Observa_o_do_Boleto__c,
							      Processo_seletivo__r.Id,
							      Status_de_Cria_o_Altera_o__c,
							      Turno_da_Oferta__c,
							      Vagas__c,
							      Valor_da_Inscri_o__c,
							      Valor_da_Matr_cula__c,
							      rea_Curso__c
					    	 FROM Oferta__c 
					  	    WHERE Processo_seletivo__c = 'a06A000001FjIvc' 
					          AND ID_Habilitacao_Filial__c != :'' 
					          AND Envia_para_Academus_RM__c = :true 
					          AND ID_Academus_Oferta__c != :'' 
					          AND C_digo_da_Oferta__c != :'' 
					   	    ];
        for(Oferta__c oferta : listOferta){
			CRA_soapSforceCom200509Outbound.Oferta_xcNotification notificacao = new CRA_soapSforceCom200509Outbound.Oferta_xcNotification();			   	    
            notificacao.Id = oferta.Id;
            notificacao.sObject_x = new CRA_sobjectEnterpriseSoapSforceCom.Oferta_xc();		   	    
            notificacao.sObject_x.Ag_ncia_xc = oferta.Ag_ncia__c;
            notificacao.sObject_x.C_digo_da_Area_xc = oferta.C_digo_da_Area__c;
            notificacao.sObject_x.C_digo_da_Coligada_xc = oferta.C_digo_da_Coligada__c;
            notificacao.sObject_x.C_digo_da_Oferta_xc = oferta.C_digo_da_Oferta__c;
            notificacao.sObject_x.C_digo_do_Campus_xc = oferta.C_digo_do_Campus__c;
            notificacao.sObject_x.Conta_xc = oferta.Conta__c;
            notificacao.sObject_x.Contrato_xc = oferta.Contrato__c;
            notificacao.sObject_x.Contrato_n_xc = oferta.Contrato_n__c;
            notificacao.sObject_x.Curso_xc = oferta.Curso__c;
            notificacao.sObject_x.D_gito_xc = oferta.D_gito__c;
            notificacao.sObject_x.Filial_xc = oferta.Filial__c;
            notificacao.sObject_x.ID_Academus_Oferta_xc = oferta.ID_Academus_Oferta__c;
            notificacao.sObject_x.ID_Habilitacao_Filial_xc = oferta.ID_Habilitacao_Filial__c;
            notificacao.sObject_x.Id_Campus_xc = oferta.Id_Campus__c;
            notificacao.sObject_x.Id_Institui_o_xc = oferta.Id_Institui_o__c;
            notificacao.sObject_x.Institui_o_xc = oferta.Institui_o__c;
            notificacao.sObject_x.Institui_o_n_xc = oferta.Institui_o_n__c;
            notificacao.sObject_x.IsDeleted = oferta.IsDeleted;
            notificacao.sObject_x.Name = oferta.Name;
            notificacao.sObject_x.Nome_do_Contrato_xc = oferta.Nome_do_Contrato__c;
            notificacao.sObject_x.Observa_o_do_Boleto_xc = oferta.Observa_o_do_Boleto__c;
            notificacao.sObject_x.Processo_seletivo_xc = oferta.Processo_seletivo__r.Id;
            notificacao.sObject_x.Status_de_Cria_o_Altera_o_xc = oferta.Status_de_Cria_o_Altera_o__c;
            notificacao.sObject_x.Turno_da_Oferta_xc = oferta.Turno_da_Oferta__c;
            notificacao.sObject_x.Vagas_xc = oferta.Vagas__c;
            notificacao.sObject_x.Valor_da_Inscri_o_xc = oferta.Valor_da_Inscri_o__c;
            notificacao.sObject_x.Valor_da_Matr_cula_xc = oferta.Valor_da_Matr_cula__c;
            notificacao.sObject_x.rea_Curso_xc = oferta.rea_Curso__c;
    
            notificacoesParaEnvio.add(notificacao);
        }		

		System.debug(notificacoesParaEnvio);
		
		
        Boolean resultado = stub.notifications(UserInfo.getOrganizationId(), 
												  '', 
												  UserInfo.getSessionId(), 
												  'https://cs96.salesforce.com/services/Soap/u/28.0/00D1g0000004bQ2EAI', 
												  'https://cs96.salesforce.com/services/Soap/c/28.0/00D1g0000004bQ2EAI',
												  notificacoesParaEnvio);
        
      
		System.debug('Resultado: ' + resultado);
	}
}