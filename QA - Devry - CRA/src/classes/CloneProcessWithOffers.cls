public class CloneProcessWithOffers{
    Processo_seletivo__c process = new Processo_seletivo__c();
    Processo_seletivo__c newProcess = new Processo_seletivo__c();
    List<Oferta__c> os = new List<Oferta__c>(); 
    List<Oferta__c> osx = new List<Oferta__c>();     
	String processId;
    
    public CloneProcessWithOffers(ApexPages.StandardController controller){
        this();
    }
    
    public CloneProcessWithOffers(){
        processId = ApexPages.currentPage().getParameters().get('id');
        if (processId != null){
            process = [Select id, Name, RecordTypeId, Institui_o_n__c, Campus__c, Tipo_de_Processo_n__c, Tipo_de_Matr_cula__c, Niveis_de_Ensino__c,
						ndice_do_Processo__c, PeriodoLetivo__c, Status_Processo_Seletivo__c,Vis_vel_na_Ficha_de_Inscri_o__c, Integra_o_Candidatos__c,
						Resultado__c, Data_de_in_cio__c, Data_Validade_Processo__c, Data_do_Vestibular__c, Data_de_Inicio_das_Aulas__c, Data_de_encerramento__c,
						Data_Aprova_Classificaveis__c, CobrarTaxaInscricao__c, Valor_Taxa_Inscricao__c, Vencimento_Boleto_dias__c, Instrucoes_Boleto__c                       
                       From Processo_seletivo__c Where id =: processId limit 1];

            newProcess.RecordTypeId = process.RecordTypeId;
            newProcess = process.clone();
        }
    }
    
    public Processo_seletivo__c getNewProcess(){
        return newProcess;
    }
    
    public void setNewProcess(Processo_seletivo__c newProcess){
        this.newProcess = newProcess;
    }
    

    public Pagereference Clonar(){        
        //Savepoint sp = Database.setSavepoint();        
        
            if ([Select count() From Oferta__c Where Processo_seletivo__c = :processId AND ID_Habilitacao_Filial__c != :'' AND Envia_para_Academus_RM__c = :true AND ID_Academus_Oferta__c != :'' AND C_digo_da_Oferta__c != :''] > 0){                
                if(newProcess.id == null){
                    insert newProcess;                    
                }
                else{
                    update newProcess;
                }
                
                system.debug('new processID: '  + newProcess.Id);
                
                os = [Select id, Name, Tipo_do_Curso__c, Institui_o_n__c, Campus__c, Processo_Seletivo__c, rea_Curso__c, Curso__c, Vagas__c, Contrato_n__c, Envia_para_Academus_RM__c, Formul_rio_Salesforce__c From Oferta__c Where Processo_seletivo__c =: processId AND ID_Habilitacao_Filial__c != :'' AND Envia_para_Academus_RM__c = :true AND ID_Academus_Oferta__c != :'' AND C_digo_da_Oferta__c != :''];
        
                /*
                List<List<Oferta__c>> groups = new List<List<Oferta__c>>();
                Integer indexPosition = 0;
                Integer groupSize = 0;
                List<Oferta__c> ids = new List<Oferta__c>();
                for(Integer i = 0; i < os.size(); i++){  
                    while(groupSize < 2 && indexPosition + groupSize < os.size()){
                        ids.add(os.get(indexPosition + groupSize));
                        groupSize++;
                        if(groupSize == 2 || indexPosition + groupSize == os.size()){
                            groups.add(ids);
                            System.debug(ids);
                            ids.clear();
                        }
                    }
                    indexPosition+=groupSize;
                    groupSize = 0;
                }

                for(List<Oferta__c> offersGroup: groups){
                    insereOfertas(offersGroup, process.Id, newProcess.Id);
                }*/
                /*
                List<Oferta__c> offersToInsert = new List<Oferta__c>();
                for(Oferta__c o: os){
                    Oferta__c offerAux = new Oferta__c();
                    offerAux.Name = o.Name;
                    offerAux.Tipo_do_Curso__c = o.Tipo_do_Curso__c;
                    offerAux.Institui_o_n__c = o.Institui_o_n__c;
                    offerAux.Campus__c = o.Campus__c;
                    offerAux.Processo_Seletivo__c = newProcess.Id;
                    offerAux.rea_Curso__c = o.rea_Curso__c;
                    offerAux.Curso__c = o.Curso__c;
                    offerAux.Vagas__c = o.Vagas__c;
                    offerAux.Contrato_n__c = o.Contrato_n__c;
                    offerAux.Envia_para_Academus_RM__c = o.Envia_para_Academus_RM__c;
                    offerAux.Formul_rio_Salesforce__c = o.Formul_rio_Salesforce__c;
                    offersToInsert.add(offerAux);
                }

                Integer count = 0;
                for(Oferta__c o: offersToInsert){
                    System.debug('CONTADOR: ' + count);
                    Integer start = System.Now().millisecond();
                    System.debug('INICIO' + start);
                    while(System.Now().millisecond() < start + 10){
                    }
                    System.debug('ESPEROU 10 ms');
                    count++;
                    insert o;
                }
                */
                for (Oferta__c o : os){
                    Oferta__c ox = new Oferta__c();
                    ox.Name = o.Name;
                    ox.Tipo_do_Curso__c = o.Tipo_do_Curso__c;
                    ox.Institui_o_n__c = o.Institui_o_n__c;
                    ox.Campus__c = o.Campus__c;
                    ox.Processo_Seletivo__c = newProcess.Id;
                    ox.rea_Curso__c = o.rea_Curso__c;
                    ox.Curso__c = o.Curso__c;
                    ox.Vagas__c = o.Vagas__c;
                    ox.Contrato_n__c = o.Contrato_n__c;
                    ox.Envia_para_Academus_RM__c = false;
                    ox.Formul_rio_Salesforce__c = o.Formul_rio_Salesforce__c;
                    osx.add(ox);
                }                
                insert osx;
                
                System.debug(osx);
                
                enviaOfertasParaRM(newProcess.Id);
                
                System.debug('CONSEGUIU CLONAR');
                PageReference p = new PageReference('/' + newProcess.Id);
                p.setRedirect(true);  
                return p;                  
            }
            else{   
                System.debug('NÃO PRECISA CLONAR ');                 
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.error, 'O processo clonado não possui Ofertas integradas com Academus/RM.');
                Apexpages.addMessage(msg);
                return null;
            }
        
        //Database.rollback(sp);    
        //return null;
    }

    private static void insereOfertas(List<Oferta__c> offers, String processId, String newProcessId){
        List<Oferta__c> offersToInsert = new List<Oferta__c>();
        for(Oferta__c o: offers){
            Oferta__c offerAux = new Oferta__c();
            offerAux.Name = o.Name;
            offerAux.Tipo_do_Curso__c = o.Tipo_do_Curso__c;
            offerAux.Institui_o_n__c = o.Institui_o_n__c;
            offerAux.Campus__c = o.Campus__c;
            offerAux.Processo_Seletivo__c = newProcessId;
            offerAux.rea_Curso__c = o.rea_Curso__c;
            offerAux.Curso__c = o.Curso__c;
            offerAux.Vagas__c = o.Vagas__c;
            offerAux.Contrato_n__c = o.Contrato_n__c;
            offerAux.Envia_para_Academus_RM__c = o.Envia_para_Academus_RM__c;
            offerAux.Formul_rio_Salesforce__c = o.Formul_rio_Salesforce__c;
            offersToInsert.add(offerAux);
        }
        Integer start = System.Now().millisecond();
        System.debug(start);
        while(System.Now().millisecond() < start + 50){
            System.debug(System.Now().millisecond());
        }
        insert offersToInsert;
    }

    /*
    @future(Callout = true)
    private static void enviaOfertasParaRM(String bodyRequest){
        try{
            HttpResponse httpResponse = OfertaRMAPI.sendOffers(bodyRequest);
            Integer statusCode = httpResponse.getStatusCode();
            if(statusCode == 200){
                System.debug('CloneProcessWithOffers -> 200 Sucess');
            }else{
                System.debug('CloneProcessWithOffers -> ' + statusCode + ' Error');
            }
        }catch(CalloutException ex){
            System.debug('CloneProcessWithOffers -> CalloutException');
        }
    }*/
    
    @future(Callout = true)
    private static void enviaOfertasParaRM(Id processoID){
        system.debug('processo Id future: ' + processoID);
        CRA_soapSforceCom200509Outbound.Notification stub = new CRA_soapSforceCom200509Outbound.Notification();
		List<CRA_soapSforceCom200509Outbound.Oferta_xcNotification> notificacoesParaEnvio = new List<CRA_soapSforceCom200509Outbound.Oferta_xcNotification>();
	       
		List<Oferta__c> listOferta = [Select id, 
                                      Ag_ncia__c,
                                      C_digo_da_Area__c,
                                      C_digo_da_Coligada__c,
                                      C_digo_da_Oferta__c,
                                      C_digo_do_Campus__c,
                                      Conta__c,
                                      Contrato__c,
                                      Contrato_n__c,
                                      Curso__c,
                                      D_gito__c,
                                      Filial__c,
                                      ID_Academus_Oferta__c,
                                      ID_Habilitacao_Filial__c,
                                      Id_Campus__c,
                                      Id_Institui_o__c,
                                      Institui_o__c,
                                      Institui_o_n__c,
                                      IsDeleted,
                                      Name,
                                      Nome_do_Contrato__c,
                                      Observa_o_do_Boleto__c,
                                      Processo_seletivo__r.Id,
                                      Status_de_Cria_o_Altera_o__c,
                                      Turno_da_Oferta__c,
                                      Vagas__c,
                                      Valor_da_Inscri_o__c,
                                      Valor_da_Matr_cula__c,
                                      rea_Curso__c
                                 FROM Oferta__c 
                                WHERE Processo_seletivo__c =: processoID 
                                  //AND ID_Habilitacao_Filial__c != '' 
                                  //AND Envia_para_Academus_RM__c = true 
                                  //AND ID_Academus_Oferta__c != '' 
                                  //AND C_digo_da_Oferta__c != '' 
                                ];
        system.debug('listOferta '+ listOferta);
        
        for(Oferta__c oferta : listOferta){
			CRA_soapSforceCom200509Outbound.Oferta_xcNotification notificacao = new CRA_soapSforceCom200509Outbound.Oferta_xcNotification();			   	    
            notificacao.Id = oferta.Id;
            notificacao.sObject_x = new CRA_sobjectEnterpriseSoapSforceCom.Oferta_xc();	
            notificacao.sObject_x.Id = oferta.Id;
            notificacao.sObject_x.Ag_ncia_xc = oferta.Ag_ncia__c;
            notificacao.sObject_x.C_digo_da_Area_xc = oferta.C_digo_da_Area__c;
            notificacao.sObject_x.C_digo_da_Coligada_xc = oferta.C_digo_da_Coligada__c;
            notificacao.sObject_x.C_digo_da_Oferta_xc = oferta.C_digo_da_Oferta__c;
            notificacao.sObject_x.C_digo_do_Campus_xc = oferta.C_digo_do_Campus__c;
            notificacao.sObject_x.Conta_xc = oferta.Conta__c;
            notificacao.sObject_x.Contrato_xc = oferta.Contrato__c;
            notificacao.sObject_x.Contrato_n_xc = oferta.Contrato_n__c;
            notificacao.sObject_x.Curso_xc = oferta.Curso__c;
            notificacao.sObject_x.D_gito_xc = oferta.D_gito__c;
            notificacao.sObject_x.Filial_xc = oferta.Filial__c;
            notificacao.sObject_x.ID_Academus_Oferta_xc = oferta.ID_Academus_Oferta__c;
            notificacao.sObject_x.ID_Habilitacao_Filial_xc = oferta.ID_Habilitacao_Filial__c;
            notificacao.sObject_x.Id_Campus_xc = oferta.Id_Campus__c;
            notificacao.sObject_x.Id_Institui_o_xc = oferta.Id_Institui_o__c;
            notificacao.sObject_x.Institui_o_xc = oferta.Institui_o__c;
            notificacao.sObject_x.Institui_o_n_xc = oferta.Institui_o_n__c;
            notificacao.sObject_x.IsDeleted = oferta.IsDeleted;
            notificacao.sObject_x.Name = oferta.Name;
            notificacao.sObject_x.Nome_do_Contrato_xc = oferta.Nome_do_Contrato__c;
            notificacao.sObject_x.Observa_o_do_Boleto_xc = oferta.Observa_o_do_Boleto__c;
            notificacao.sObject_x.Processo_seletivo_xc = oferta.Processo_seletivo__r.Id;
            notificacao.sObject_x.Status_de_Cria_o_Altera_o_xc = oferta.Status_de_Cria_o_Altera_o__c;
            notificacao.sObject_x.Turno_da_Oferta_xc = oferta.Turno_da_Oferta__c;
            notificacao.sObject_x.Vagas_xc = oferta.Vagas__c;
            notificacao.sObject_x.Valor_da_Inscri_o_xc = oferta.Valor_da_Inscri_o__c;
            notificacao.sObject_x.Valor_da_Matr_cula_xc = oferta.Valor_da_Matr_cula__c;
            notificacao.sObject_x.rea_Curso_xc = oferta.rea_Curso__c;
    
            notificacoesParaEnvio.add(notificacao);
        }		

		System.debug(notificacoesParaEnvio);
		
		
        Boolean resultado = stub.notifications(UserInfo.getOrganizationId(), 
												  '', 
												  UserInfo.getSessionId(), 
												  'https://cs96.salesforce.com/services/Soap/u/28.0/00D1g0000004bQ2EAI', 
												  'https://cs96.salesforce.com/services/Soap/c/28.0/00D1g0000004bQ2EAI',
												  notificacoesParaEnvio);
    }
    
    public Pagereference Cancelar(){
        PageReference p = new PageReference('/' + processId);
        p.setRedirect(true);  
        return p;      
    }
    
    @isTest(SeeAllData=true)
    static void UnitTest1(){
        Oferta__c oft = [Select id, Processo_seletivo__r.Id From Oferta__c Where Processo_seletivo__r.Ativo__c =: true AND Campus__c != null AND Institui_o_n__c != null AND Tipo_do_Curso__c != null AND Curso__r.Status__c =: 'Ativo' AND Curso__c != null limit 1];
        oft.Envia_para_Academus_RM__c = true;
        update oft;
        Processo_seletivo__c process = [Select id From Processo_seletivo__c Where id =: oft.Processo_seletivo__r.Id limit 1];
        PageReference testPage = Page.CloneProcessWithOffers;
        testPage.getParameters().put('id',(String) process.Id);
        testPage.setRedirect(true);
        ApexPages.currentPage().getParameters().put('id', (String)process.id);
        CloneProcessWithOffers ext = new CloneProcessWithOffers();
        ext.setNewProcess(process);
        ext.getNewProcess();    
        PageReference result1 = ext.Clonar();
        PageReference result2 = ext.Cancelar();        
    }
}