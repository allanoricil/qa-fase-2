/*
    @author Diego Moreira
    @class Schedule de atualização de status do condidato
*/
global class SRM_ScheduleConsultaResultadoVestibular implements Schedulable {
	global void execute(SchedulableContext sc) { 
		SRM_ConsultaResultadoVestibularBO.execute();
	}
}