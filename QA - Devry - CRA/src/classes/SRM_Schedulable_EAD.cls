global class SRM_Schedulable_EAD implements Schedulable {

	public SRM_Schedulable_EAD(){}
	global void execute(SchedulableContext sc) {

		for( Queue__c queue : getQueueByEventNameAndStatus( 'APROVA_CANDIDATO_2', 'CREATED', 50 ) ) {
			SRM_CadastroCandidato.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
		}

	}
	public List<Queue__c> getQueueByEventNameAndStatus( String eventName, String queueStatus, Integer numLimit ) {
		return [SELECT Id, Name, EventName__c, Status__c, Payload__c, createdDate, ProcessBySchedule__c, PayloadSAP__c
					FROM Queue__c
					WHERE EventName__c = :eventName
					AND Status__c = :queueStatus
					ORDER BY createdDate ASC
					Limit :numLimit];
	}


}