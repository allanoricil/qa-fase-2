/*
    @author Diego Moreira
    @class Classe de negocio de processo seletivo
*/
public with sharing class Processo_seletivoBO {
	/*
        Singleton
    */
    private static final Processo_seletivoBO instance = new Processo_seletivoBO();    
    private Processo_seletivoBO(){}
    
    public static Processo_seletivoBO getInstance() {
        return instance;
    }

    /*
		Atualiza o nome do periodo letivo do processo seletivo
		@param processosSeletivos processos seletivos criados ou editados
    */
    public void atualizaProcessoLetivo( List<Processo_seletivo__c> processosSeletivos ) {
    	List<String> periodosLetivos = new List<String>();
    	Map<String, Per_odo_Letivo__c> mapPeriodoLetivo = new Map<String, Per_odo_Letivo__c>(); 

    	for( Processo_seletivo__c processoSeletivo : processosSeletivos ) {
    		periodosLetivos.add( processoSeletivo.PeriodoLetivo__c );
    	}
 
    	for( Per_odo_Letivo__c periodoLetivo : Periodo_LetivoDAO.getInstance().getPeriodoLetivoById( periodosLetivos ) )
    		mapPeriodoLetivo.put( periodoLetivo.Id, periodoLetivo );
 
    	for( Processo_seletivo__c processoSeletivo : processosSeletivos ) {
    		if( mapPeriodoLetivo.get( processoSeletivo.PeriodoLetivo__c ) != null ) { 
    			processoSeletivo.Periodo_Letivo__c = mapPeriodoLetivo.get( processoSeletivo.PeriodoLetivo__c ).Name;
    		}
    	}	

    }
}