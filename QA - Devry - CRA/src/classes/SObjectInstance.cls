/*
	@author Diego Moreira
	@class Classe de cria��o de instancias de objetos para classes de teste
*/
public with sharing class SObjectInstance {
	/* 
    	Return an Account Object
    	@param recordTypeId id do tipo da conta, cpf__2 cpf da conta
    */
    public static Account getAccount( String recordTypeId, String cpf_2 ) {      
        Account account 					= new Account();
        account.RecordTypeId 				= recordTypeId;
        account.FirstName 					= 'Test Firstname';
        account.LastName 					= 'Test Lastname';
        account.PersonMobilePhone		    = '11 9999-9999';
        account.CPF__c						=  cpf_2;
        account.PersonEmail					= 'email@email.com';
        account.RG__c						= '12345678';
        account.PersonBirthdate			 	= date.parse('10/10/1990');
        account.Nacionalidade__c	 		= 'Brasileira';
        account.Naturalidade__c				= 'Paulista';
        account.Rua__c						= 'Rua Albano Almeida Lima';
        account.Complemento__c				= 'casa';
        account.N_mero__c					= '416';
        account.Cidade__c					= 'Campinas';
        account.CEP__c 						= '13073-131';
        account.Estado__c					= 'SP';
        account.Pa_s__c						= 'BR';
        account.MoraComPais__c				= 'N�o';
        account.JaPossuiGraduacao__c		= 'N�o';
        account.Tipo_escola_cursa_ou_cursou_ensino_medio__c 		='Estabelecimento privado';
  
        
        
        return account;
    }

    /*
		Return an Opportunity object
		@param recordTypeId id do tipo de registro, accountId id da conta, processoSeletivo id do processo seletivo
    */
    public Static Opportunity getOpportunity( String recordTypeId, String accountId, String processoSeletivo ) {
    	Opportunity opportunity 			= new Opportunity();
    	opportunity.RecordTypeId 			= recordTypeId;
    	opportunity.AccountId 				= accountId;
    	opportunity.Name 					= 'Teste oportunidade';
    	opportunity.Processo_seletivo__c	= processoSeletivo;
    	opportunity.CloseDate 				= Date.today();
    	opportunity.StageName 				= 'Pr�-Inscrito';

    	return opportunity;
    }
    
    /*
    	Monta objeto de Institui��o
    	@param idInstituicao id da institui��o, codigo c�digo da coligada
    */
    public Static Institui_o__c getInstituicao( String codigo, String idInstituicao ) {
    	Institui_o__c  instituicao			= new Institui_o__c();
    	instituicao.Name					= 'FANOR';
    	instituicao.EnderecoInstituicao__c  = 'Endereco';
    	instituicao.CNPJ__c					= '48.326.681/0001-09';
    	instituicao.Id_Institui_o__c		= idInstituicao;
    	instituicao.C_digo_da_Coligada__c	= codigo;
    	
    	return instituicao;
    }
    /*
    	Monta objeto de meios de pagamento da institui��o
    	@param idInstituicao id da institui��o 
    */
    //ALTER: MERCHANTID
    /*public Static MeioPagamentoIES__c getMeioPagamentoIES( String idIstituicao ) {
    	MeioPagamentoIES__c meiopagamentoies = new MeioPagamentoIES__c();
    	meiopagamentoies.Instituicao__c		 = idIstituicao;
    	
    	return meiopagamentoies;
    }*/
    
    public Static Campus_Payment_Type__c getCampusPaymentType( String idCampus ) {
    	Campus_Payment_Type__c campusPaymentType = new Campus_Payment_Type__c();
    	campusPaymentType.Campus__c = idCampus;
    	
    	return campusPaymentType;
    }
    
    /*
    	Monta objeto de meios de pagamento braspag
    	@param codigo c�digo do meio de pagamento braspag
    */
    
    public Static MeioPagamentoBraspag__c getMeioPagamentoBraspag(String Codigo){
    	MeioPagamentoBraspag__c braspag 	= new MeioPagamentoBraspag__c();
    	braspag.Name						= 'Cielo Visa';
    	braspag.Codigo__c					= Codigo;
    	braspag.TipoMeioPagamento__c		= 'Cart�o';
    	
    	return braspag;
    }
    
     /*
    	Monta objeto de Processo Seletivo
    	@param recordTypeId id do tipo da conta, instituicaoId id da instituic�o, campusId Id do Campus
    */
    
    public Static Processo_seletivo__c getProcessoSeletivo(String recordTypeId, String instituicaoId, String campusId){
    	Processo_seletivo__c pseletivo		= new Processo_seletivo__c();
    	pseletivo.RecordTypeId 			    = recordTypeId;
    	pseletivo.Institui_o_n__c			= instituicaoId;    	
    	pseletivo.Name 						= 'Vestibular 2015';
    	pseletivo.Tipo_de_Processo_n__c		= 'Vestibular';
    	pseletivo.Tipo_de_Matr_cula__c		= 'Vestibular Tradicional';
    	pseletivo.Periodo_Letivo__c			= '2015.1';
    	pseletivo.Campus__c					= campusId;
    	pseletivo.ndice_do_Processo__c		= 'A';
		pseletivo.Status_Processo_Seletivo__c = 'Aberto';    	
		pseletivo.Data_de_Inicio_das_Aulas__c = date.today();
		pseletivo.Data_Validade_Processo__c = date.today();
		pseletivo.Data_de_in_cio__c			= date.today();
		pseletivo.Data_do_Vestibular__c		= date.today();
		pseletivo.Data_de_encerramento__c	= date.today();
		pseletivo.Vencimento_Boleto_dias__c = 3;
		pseletivo.Valor_Taxa_Inscricao__c	= 100;
		pseletivo.Instrucoes_Boleto__c		= 'N�o receber ap�s vencimento';
		
		
		return pseletivo;
    }
    
    
    public Static Campus__c getCampus(String campusId){
    	Campus__c campus 					= new Campus__c();
    	campus.Name							= 'Dunas';
    	campus.Id_Campus__c					= campusId;
    	campus.CodigoSAP__c					= '001';
    	
    	return campus;	
    	
    } 

    public Static CodigoPromocional__c getCodPromocional(){
        CodigoPromocional__c codpromocional             = new CodigoPromocional__c();
        codpromocional.DescricaoCodigoPromocional__c    = 'test';
        codpromocional.Desconto__c                      = 10;
        codpromocional.Expira__c                        = date.today();
        codpromocional.Tipo_de_C_digo__c                = 'Coletivo';
        codpromocional.TipoDesconto__c                  = 'Parcial';
        codpromocional.Name                             = '1234';

        return codpromocional;
    }
}