@isTest 
public class CraAllClientTest {
    static testMethod List<CraDirectorCords.DirectorCord> GetCraDirectorCord(){
        
       List<CraDirectorCords.DirectorCord> drs = CraDirectorCords.getDirectorCords('1','07100713','1');
      
        if(drs!=null){
           return drs;
        }
        else
            return null; 
    }
    static testMethod List<CraOferta.CraOfertasResult> CraOfertasResult(){
        
        List<CraOferta.CraOfertasResult> drs1 = CraOferta.getOfertas('1','07100713','1','INCLUSÃO');
        if(drs1!=null){
           return drs1;
        }
        else
            return null; 
    }
    static testMethod List<CRA_TurmasEproList.TurmasEPro> GetTurmasEPro(){
        
       List<CRA_TurmasEproList.TurmasEPro> turmas = CRA_TurmasEproList.getTurmasEpro('1','161I010490','1');
        if(turmas!=null){
           return turmas;
        }
        else
            return null; 
    }
     static testMethod List<CraEstudoDirigido.EstudoDirigido> GetEstudoDirigido(){
        
      List<CraEstudoDirigido.EstudoDirigido> ets= CraEstudoDirigido.getEstudoDirigidos('1','07100713','1');
        if(ets!=null){
           return ets;
        }
        else
            return null; 
    }
      static testMethod List<CRA_Empresas.Empresas> GetEmpresas(){
        
      List<CRA_Empresas.Empresas> emps= CRA_Empresas.getEmpresas('1','07100713','1');
        if(emps!=null){
           return emps;
        }
        else
            return null; 
    }
     static testMethod List<Cra_MatrizesAtivas.MatrizesAtivas> GetMatrizesAtivas(){
        
     List<Cra_MatrizesAtivas.MatrizesAtivas> mats= Cra_MatrizesAtivas.getMatrizesAtivas('1','07100713','1');
        if(mats!=null){
           return mats;
        }
        else
            return null; 
    }
}