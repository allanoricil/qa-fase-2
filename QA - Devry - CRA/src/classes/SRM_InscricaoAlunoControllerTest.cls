@isTest
private class SRM_InscricaoAlunoControllerTest {

    static testMethod void myUnitTest() {
         
        Account acc = InstanceToClassCoverage.createAccount();
        insert acc;
         
        Lead lead1 = InstanceToClassCoverage.createLead();
        insert lead1;
        
        Institui_o__c instituicao = InstanceToClassCoverage.createInstituicao();
        insert instituicao;
        
        Campus__c campus = InstanceToClassCoverage.createCampus(instituicao);
        insert campus;
        
        Per_odo_Letivo__c periodoLetivo = InstanceToClassCoverage.createPeriodoLetivo();
        insert periodoLetivo;
        
        Processo_seletivo__c processoSeletivo = InstanceToClassCoverage.createProcessoSeletivo(instituicao, campus, periodoLetivo);
        insert processoSeletivo;
         
         Opportunity opp = InstanceToClassCoverage.createOpportunity(processoSeletivo);
         opp.AccountId = acc.Id;
         insert opp;
         
         Processo_seletivo__c ps1 = InstanceToClassCoverage.createProcess();
         insert ps1;     
         
         test.StartTest();
         /* PageReference pageref                                   = Page.SRM_InscricaoAluno;
            Test.setCurrentPage(pageRef);
                        
            pageRef.getParameters().put('origem',lead1.Id);
            pageRef.getParameters().put('processoSeletivoId',ps1.Id);
            
            SRM_InscricaoAlunoController cls = new SRM_InscricaoAlunoController();
            cls.createInscricao();
            //cls.atualizaEndereco();
            
            //cls.execute();
         */ 
         test.StopTest();
        
    }
}