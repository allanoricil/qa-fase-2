/*************************************************************************
Author   : Appirio Offshore(Sunil Gupta)
Date     : Dec 24, 2012
Usage    : Provides Test Coverage for GraduateInterviewHomeControllerMestrado 

//Modifications
**************************************************************************/
@isTest(SeeAllData=true)
private class GraduateInterviewHomeControllerMes_Test {
    
    static testMethod void myMethod1() {
        //Insert test data
        User u1 = TestUtils.GetUser();
        //Opportunity objOpportunity = mc_TestUtility.createOpportunity(u1.Id);
        //mc_TestUtility.InsertPortalRecord(u1,objOpportunity);
        
        System.RunAs(u1){
            Test.setCurrentPage(Page.GraduateInterviewHome);
            Test.StartTest(); //Start Test
            GraduateInterviewHomeControllerMestrado  objClass = new GraduateInterviewHomeControllerMestrado ();
            
            //Verify Countries list
            List<SelectOption> lstOptions = objClass.getCountriesSelectList(); 
            System.assert(lstOptions.size() >0);
            
            //Verify Page Language
            String language = objClass.pageLanguage;
            System.assertEquals(language.equalsIgnoreCase('pt_BR'),true);
            
            ApexPages.currentPage().getParameters().put('lang','en');
            String language2 = objClass.pageLanguage;
            System.assertEquals(language2.equalsIgnoreCase('en'),true);
                        
            //Verify Selection process items                
            List<SelectOption> lstOptions2 = objClass.getAllSelectionProcessItems();
            //@Sunil ToDo: Insert test records for selection process
            
            //Verify On change of selection process
            Pagereference pr = objClass.OnchangeSelectionProcess();
            System.assertEquals(pr,null);
            
            //Verify On change of interview date
            pr = objClass.OnchangeInterviewDate();
            System.assertEquals(pr,null);
            
            //Verify On change of programs
            pr = objClass.OnchangePrograms();
            System.assertEquals(pr,null);
            
            
            //Verify opportunity creation
            objClass.newOpportunity.Primeiro_Nome__c = 'TestTestExecution';
            objClass.newOpportunity.Sobrenome__c = 'Test';
            objClass.newOpportunity.Passaporte__c = 'TestPassport';
            PageReference pgSave = objClass.save();
            
            List<Opportunity> lst = [select id from Opportunity where Primeiro_Nome__c = 'TestTestExecution' limit 1];
            //System.assert(lst.size() > 0);
            //System.assert(pgSave == null);
            
               
            Test.StopTest(); //Stop Test
            
         }
    }
}