public class CRA_LeadUtil {
    public static void setPoloLeadFicha(List<Lead> newLeadList){
		Set<String> poloNameList = new Set<String>();
        List<Lead> leadPoloList = new List<Lead>();
        for(Lead newLead : newLeadList){
            if(String.isBlank(String.valueOf(newLead.Polo__c)) && !String.isBlank(String.valueOf(newLead.Polo_W2L__c))){
                leadPoloList.add(newLead);
                poloNameList.add(newLead.Polo_W2L__c);
            }			
        }
        
        if(!leadPoloList.isEmpty()){
            List<Polo__c> poloList = [SELECT Id, Name FROM Polo__c WHERE Name IN: poloNameList];
            Map<String, Id> poloNameIdMap = new Map<String, Id>();
            for(Polo__c polo : poloList){
                poloNameIdMap.put(polo.Name, polo.Id);
            }
            for(Lead setPoloLead : leadPoloList){
                if(poloNameIdMap.get(setPoloLead.Polo_W2L__c) != null)
                    setPoloLead.Polo__c = poloNameIdMap.get(setPoloLead.Polo_W2L__c);
            }
        }
    }
    
    public static void setCampusLeadFicha(List<Lead> newLeadList){
		Set<String> campusNameList = new Set<String>();
        List<Lead> leadCampusList = new List<Lead>();
        for(Lead newLead : newLeadList){
            if(String.isBlank(String.valueOf(newLead.Campus_LookUp__c)) && !String.isBlank(String.valueOf(newLead.Campus__c))){
                leadCampusList.add(newLead);
                campusNameList.add(newLead.Campus__c);
            }			
        }
        
        if(!leadCampusList.isEmpty()){
            List<Campus__c> campusList = [SELECT Id, Name FROM Campus__c WHERE Name IN: campusNameList];
            Map<String, Id> campusNameIdMap = new Map<String, Id>();
            for(Campus__c campus : campusList){
                campusNameIdMap.put(campus.Name, campus.Id);
            }
            for(Lead setCampusLead : leadCampusList){
                if(campusNameIdMap.get(setCampusLead.Campus__c) != null)
                    setCampusLead.Campus_LookUp__c = campusNameIdMap.get(setCampusLead.Campus__c);
            }
        }
    }
}