public with sharing class CandidateController {

    public String idpolo   { get{ return [Select Polo__c From User Where Id =:UserInfo.getUserId() limit 1].Polo__c ;} set; }
    public String polo     { get{ return [Select Name from Polo__c where id=: idpolo limit 1].Name;} set;}
    // Paginação
    
    public EadIterator obj {get; set;}
    
    public List<EadWrapper> lstWrapper {get;set;}
    public List<EadWrapper> lstSetController{get;set;}
    public List<EadWrapper> lstSetControllerP{get;set;}
    public String query {get; set; }
    
    //Listas de Filtro

    public List<String> fases {get; set;}
    public List<String> processos {get; set;} 
    public List<String> contatados {get; set;}

    //Campos de Filtro
    public String valorPesquisa { get; set; }
    public String dataInicial   { get; set; }
    public String dataFinal     { get; set; }
    public String data          { get; set; }
    public String processo      { get; set; }


    //Atividade
    public Atividade__c task { get; set; }
    public String contato  { get; set; }
    public String canal  { get; set; }
    public String description   { get; set;}

    //Bolsa
    public String bolsa     {get; set;}


    //Alerta Pendentes
    public String alert { get; set; }
    public Integer pendentes { get; set; }

    public Oportunidade_ead__c opp { get; set; }
    public CandidateController(ApexPages.StandardController controller) {
        //controller.addFields(new String[]{'Visualizacoes__c'});
        this.opp = (Oportunidade_ead__c)controller.getRecord();
    }

    public String getSenha(){
        return generate(opp.Cpf__c);
    }

    public void createTask(){
        task = new Atividade__c();
        String opead = ApexPages.currentPage().getParameters().get('optask');
        Oportunidade_EAD__c op  = [Select Id, Opportunity__c from Oportunidade_EAD__c where id =: opead];

        op.StatusContato__c = 'Sim';

        task.Name = Userinfo.getName() + ' - Atividade';
        task.Status__c = 'Concluída';
 
        task.Oportunidade__c = op.Id;
        task.StatusContato__c = contato;
        task.Canal__c = canal;
        task.Descri_o__c = description;
        task.User__c = UserInfo.getUserId();
        insert task;
        update op;
    }


    //Criar fila - Evolucional

    public void insertQueue(){
        String oportunidade = ApexPages.currentPage().getParameters().get('oportunidade');
        Oportunidade_EAD__c oead = [select id, Opportunity__c, Email__c, Name, Celular__c, Cpf__c from Oportunidade_EAD__c where Id =: oportunidade limit 1];

        Queue__c fila = new Queue__c();
        fila.EventName__c = QueueEventNames.EVOLUCIONAL_SERVICES.name();
        fila.Status__c ='CREATED';
        fila.Payload__c    = createJson(oead);
        fila.Oportunidade__c = oead.Opportunity__c;
        fila.ProcessBySchedule__c = false;
        fila.Oportunidade_EAD__c = oead.Id;

        oead.Presente__c = true;

        update oead;

        QueueDAO.getInstance().insertData( fila ); 

        Evolucional_Create.processingQueue(fila.Id, fila.Payload__c);
    }


    public String createJson(Oportunidade_EAD__c opp) {
    
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'token','756C85B5-71AC-469A-979D-D8AD020D3154');
        if(opp.Email__c == null)
            gen.writeNullField('email');
        else
            gen.writeStringField( 'email', opp.Email__c);

            gen.writeNullField('cityId');   
        
        if(opp.Name == null)
            gen.writeNullField('cityId');
        else
            gen.writeStringField( 'name', opp.Name);
        
        if(opp.Celular__c == null)
            gen.writeNullField('mobile');
        else
            gen.writeStringField( 'mobile', opp.Celular__c.replace(' ','').replace('-',''));

        if(opp.Cpf__c == null)
            gen.writeNullField('password');
        else
            gen.writeStringField( 'password', generate(opp.Cpf__c));        
        
        
        gen.writeStringField( 'degreeId', '7');
        gen.writeNullField( 'schoolId');
        gen.writeNullField( 'schoolCityId');
        gen.writeNullField( 'schoolName');
        gen.writeStringField( 'courseId', '1');
        gen.writeNullField( 'universityResponse');

        gen.writeEndObject();
        return gen.getAsString();

    }


    public void gerarProva(){
        String oportunidade = ApexPages.currentPage().getParameters().get('oportunidade');
        Oportunidade_EAD__c oead = [select id, Opportunity__c, Email__c, Name, Celular__c, Cpf__c from Oportunidade_EAD__c where Id =: oportunidade];

        Queue__c fila = [SELECT ID, Payload__c, PayloadSAP__c, Oportunidade__c, Oportunidade_EAD__c from Queue__c WHere Oportunidade_EAD__c =: oportunidade limit 1];

        Evolucional_Simulado.processingQueue(fila.Id, fila.PayloadSAP__c);
    }

    //Fim Evolucional


    //Métodos de auxílio

    public Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(0)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }

    public String generate(String cpf){
            return 'UNIFAVIP@' + cpf.replace('.','').substring(0,4);
    }

   //Metódos de Fase / Nota

   public void atualizaNota(){
        String opead = ApexPages.currentPage().getParameters().get('opnota');
        Oportunidade_EAD__c ead = [SELECT Id, TipoProcesso__c, Bolsa__c, QualNotaMediaENEM__c ,StageName__c, Nota__c, 
        Nota_de_Corte__c from Oportunidade_EAD__c where id=: opead];
        

        ead.Bolsa__c = bolsa;


        if(ead.TipoProcesso__c == 'ENEM' && ead.Bolsa__c != null){
                    
            if(ead.QualNotaMediaENEM__c >= ead.Nota_de_Corte__c){
                ead.StageName__c = 'Aprovado';                    
            }
            else{
                ead.StageName__c = 'Reprovado';
            }
        }
        if(ead.TipoProcesso__c == 'Vestibular Especial' && ead.Bolsa__c != null && ead.Nota__c != null){
            if(ead.Nota__c >= ead.Nota_de_Corte__c){
                ead.StageName__c = 'Aprovado';                    
            }
            else{
                ead.StageName__c = 'Reprovado';
            }

            if((ead.Nota__c == null || ead.Nota__c == 0) && ead.StageName__c == 'Inscrito'){
                ead.StageName__c = 'Inscrito';
            }
        }

        

        update ead;

    }


    public void atualizaNotaEnem(){
        String opead = ApexPages.currentPage().getParameters().get('opnota');
        Oportunidade_EAD__c ead = [SELECT Id, TipoProcesso__c, Bolsa__c, QualNotaMediaENEM__c ,StageName__c, Nota__c, 
        Nota_de_Corte__c from Oportunidade_EAD__c where id=: opead];
        

        ead.Bolsa__c = bolsa;
        if(ead.Bolsa__c != null){        
            if(ead.QualNotaMediaENEM__c >= ead.Nota_de_Corte__c)
                    ead.StageName__c = 'Aprovado';                    
            else
                ead.StageName__c = 'Reprovado';
        }
    }

    public void atualizaNotaAgendado(){
        String opead = ApexPages.currentPage().getParameters().get('opnota');
        Oportunidade_EAD__c ead = [SELECT Id, TipoProcesso__c, Bolsa__c, QualNotaMediaENEM__c ,StageName__c, Nota__c, 
        Nota_de_Corte__c from Oportunidade_EAD__c where id=: opead];
        

        ead.Bolsa__c = bolsa;
        if(ead.Bolsa__c != null){        
            if(ead.Nota__c >= ead.Nota_de_Corte__c)
                    ead.StageName__c = 'Aprovado';                    
            else
                ead.StageName__c = 'Reprovado';
        }
    }

}