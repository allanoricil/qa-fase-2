public class CRA_OwnerFranquia {
    
    public static void setOwnerFranquiaOpp(List<Opportunity> listNewOpp){        
        Map<Id, List<Opportunity>> mapPoloOpps = new Map<Id, List<Opportunity>>();
        if(!listNewOpp.isEmpty()){
          	for(Opportunity newOpp : listNewOpp){
                if(!String.isBlank(newOpp.Polo__c)){
                    if(mapPoloOpps.get(newOpp.Polo__c) == null)
                        mapPoloOpps.put(newOpp.Polo__c, new List<Opportunity>{newOpp});
                    else
                        mapPoloOpps.get(newOpp.Polo__c).add(newOpp);
                }
        	}  
        }    
        
        if(!mapPoloOpps.isEmpty()){
            List<Account> partnerAccountList = new List<Account>([SELECT Name, Polo__c, Polo__r.Email__c, (SELECT ContactId, IsPrimary FROM AccountContactRoles WHERE IsPrimary = TRUE), (SELECT Id FROM Contacts) FROM Account WHERE Polo__c IN: mapPoloOpps.keySet() AND isPartner = true]);
            Map<Id, Account> mapPoloAccount = new Map<Id, Account>();
            for(Account acc : partnerAccountList){
                mapPoloAccount.put(acc.Polo__c, acc);
            }
			Map<Id, Id> mapPoloUser = searchUsers(partnerAccountList);       
            for(Id idPolo : mapPoloOpps.keySet()){
                if(mapPoloUser.get(idPolo) != null){
                    for(Opportunity opp : mapPoloOpps.get(idPolo)){
                        opp.OwnerId = mapPoloUser.get(idPolo);
                        opp.Email_do_Polo__c = mapPoloAccount.get(idPolo).Polo__r.Email__c;
                    }
                }
        	}
        }        
    }   
    
    public static void setOwnerFranquiaAluno(List<Aluno__c> listNewAluno){        
        Map<Id, List<Aluno__c>> mapPoloAlunos = new Map<Id, List<Aluno__c>>();
        if(!listNewAluno.isEmpty()){
          	for(Aluno__c newAluno : listNewAluno){
                if(!String.isBlank(newAluno.Polo__c)){
                    if(mapPoloAlunos.get(newAluno.Polo__c) == null)
                        mapPoloAlunos.put(newAluno.Polo__c, new List<Aluno__c>{newAluno});
                    else
                        mapPoloAlunos.get(newAluno.Polo__c).add(newAluno);
                }
        	}  
        }    
        
        if(!mapPoloAlunos.isEmpty()){
            List<Account> partnerAccountList = new List<Account>([SELECT Name, Polo__c, Polo__r.Email__c, (SELECT ContactId, IsPrimary FROM AccountContactRoles WHERE IsPrimary = TRUE), (SELECT Id FROM Contacts) FROM Account WHERE Polo__c IN: mapPoloAlunos.keySet() AND isPartner = true]);
			Map<Id, Id> mapPoloUser = searchUsers(partnerAccountList);       
            for(Id idPolo : mapPoloAlunos.keySet()){
                if(mapPoloUser.get(idPolo) != null){
                    for(Aluno__c aluno : mapPoloAlunos.get(idPolo)){
                        aluno.OwnerId = mapPoloUser.get(idPolo);
                    }
                }
        	}
        }        
    }
    
    public static void setOwnerFranquiaLead(List<Lead> listNewLead){        
        Map<Id, List<Lead>> mapPoloLeads = new Map<Id, List<Lead>>();
        if(!listNewLead.isEmpty()){
          	for(Lead newLead : listNewLead){
                if(!String.isBlank(newLead.Polo__c)){
                    if(mapPoloLeads.get(newLead.Polo__c) == null)
                        mapPoloLeads.put(newLead.Polo__c, new List<Lead>{newLead});
                    else
                        mapPoloLeads.get(newLead.Polo__c).add(newLead);
                }
        	}  
        }    
        
        if(!mapPoloLeads.isEmpty()){
            List<Account> partnerAccountList = new List<Account>([SELECT Name, Polo__c, Polo__r.Email__c, (SELECT ContactId, IsPrimary FROM AccountContactRoles WHERE IsPrimary = TRUE), (SELECT Id FROM Contacts) FROM Account WHERE Polo__c IN: mapPoloLeads.keySet() AND isPartner = true]);
			Map<Id, Id> mapPoloUser = searchUsers(partnerAccountList);       
            for(Id idPolo : mapPoloLeads.keySet()){
                if(mapPoloUser.get(idPolo) != null){
                    for(Lead Lead : mapPoloLeads.get(idPolo)){
                        Lead.OwnerId = mapPoloUser.get(idPolo);
                    }
                }
        	}
        }        
    }
    
    public static Map<Id, Id> searchUsers(List<Account> partnerAccountList){
        Map<Id, Id> mapContactPolo = new Map<Id, Id>(); 
        Map<Id, Id> mapPoloUser = new Map<Id, Id>();
        for(Account acc : partnerAccountList){
            if(acc.AccountContactRoles.get(0) != null)
                mapContactPolo.put(acc.AccountContactRoles.get(0).ContactId, acc.Polo__c);
            else if(acc.Contacts.get(0) != null)
                mapContactPolo.put(acc.Contacts.get(0).Id, acc.Polo__c);            
        }
        List<User> partnerUserList = new List<User>([SELECT Id, ContactId FROM User WHERE ContactId IN: mapContactPolo.keySet() AND isActive = true]);
        for(User partnerUser : partnerUserList){
            if(mapContactPolo.get(partnerUser.ContactId) != null)
            	mapPoloUser.put(mapContactPolo.get(partnerUser.ContactId), partnerUser.Id);
        }
        return mapPoloUser;
    }
}