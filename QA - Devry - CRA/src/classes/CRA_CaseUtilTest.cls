@isTest
public class CRA_CaseUtilTest {
	public static testMethod void testOverallDuplicate(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Anexo__c = 'Obrigatório';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Status = 'Em andamento';
        caso.Assunto__c = assunto.Id;
        caso.Apex_context__c = true;
        insert caso;
        
        system.runAs(usuario){
            Test.startTest();
            Case casoNew = CRA_DataFactoryTest.newCase(aluno);
            casoNew.Status = 'Novo';
            casoNew.Assunto__c = assunto.Id;
            insert casoNew;
            casoNew.Status = 'Em andamento';
            try{
               update casoNew; 
            }
            catch(Exception e){
                system.debug('exceção: ' + e.getMessage());
            }
            Test.stopTest();
        }
    }
    
    public static testMethod void testPayment(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Aprovacao_Coordenador__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Restringe_duplicidade__c = true;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Pagamento__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
            Case casoNew = CRA_DataFactoryTest.newCase(aluno);
            casoNew.Status = 'Novo';
            casoNew.Assunto__c = assunto.Id;
            insert casoNew;
            myCaseId = casoNew.Id;
            Item_do_caso__c item = new Item_do_caso__c(
            	Caso__c = casoNew.Id
            );
            insert item;
            casoNew.Status = 'Em andamento';
            update casoNew;
            Test.stopTest();
        }
		Cobranca__c cobranca = [SELECT Id, Status__c, Caso__c FROM Cobranca__c WHERE Caso__c =: myCaseId LIMIT 1];
        cobranca.Status__c = 'Pago';
        update cobranca;      
        
    }
    
    public static testMethod void testApproval(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        usuario.ManagerId = userInfo.getUserId();
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Supervisor__c = true;
        assunto.Pagamento__c = false;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
            Case casoNew = CRA_DataFactoryTest.newCase(aluno);
            casoNew.Status = 'Novo';
            casoNew.Assunto__c = assunto.Id;
            insert casoNew;
            casoNew.Status = 'Em andamento';
            update casoNew;
            myCaseId = casoNew.Id;
            Test.stopTest();
        }
        
        ProcessInstance pi = [Select ID, Status From ProcessInstance Where TargetObjectID =: myCaseId AND Status = 'Pending' LIMIT 1];
        ProcessInstanceWorkitem piwi = [select Id, OriginalActorId from ProcessInstanceWorkitem where ProcessInstanceId= :pi.Id LIMIT 1];
        Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
        prWkItem.setWorkItemID(piwi.id);
        //prWkItem.setComments(comment);
        prWkItem.setAction('Approve');
        try{
            Approval.ProcessResult appResult = Approval.process(prWkItem);
        }
        catch(Exception e){
            system.debug('exception' + e.getMessage());
        }
        
    }
    
    public static testMethod void testPaymentClienteSAP(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.Codigo_SAP__c = '';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Aprovacao_Coordenador__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Restringe_duplicidade__c = true;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Pagamento__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
            Case casoNew = CRA_DataFactoryTest.newCase(aluno);
            casoNew.Status = 'Novo';
            casoNew.Assunto__c = assunto.Id;
            insert casoNew;
            myCaseId = casoNew.Id;
            Item_do_caso__c item = new Item_do_caso__c(
            	Caso__c = casoNew.Id
            );
            insert item;
            Case newCase = new Case(Id = casoNew.Id,
                                    Status = 'Em andamento');
            casoNew.Status = 'Em andamento';
            try{
            	update newCase;  
            }
            catch(Exception e){
                system.debug('error' + e.getMessage());
            }
            Test.stopTest();
        }     
   }
}