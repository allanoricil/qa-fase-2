global class AggregateResultIterable implements Iterable<AggregateResult> {

    String q;

    global AggregateResultIterable(String query){
        q = query;
    }
    global Iterator<AggregateResult> Iterator(){
        return new AggregateResultIterator(q);
    }
}