public without sharing class CRA_MatrizConsoleController {

	public Case myCase{get;set;}
    public Boolean error{get;set;}
    public String matriz{get;set;}
    public List<SelectOption> matrizList{set;}
    public List<Cra_MatrizesAtivas.MatrizesAtivas> matrizesAtivas{get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CRA_MatrizConsoleController(ApexPages.StandardController stdController) {
        this.error = false;
        this.matrizesAtivas = new List<Cra_MatrizesAtivas.MatrizesAtivas>();
        List<String> myFields = new List<String>();
        myFields.add('Assunto__r.Tipo_de_Registro_do_Caso__c');
        stdController.addFields(myFields); 
        this.myCase = (Case)stdController.getRecord();
        if(myCase.Assunto__r.Tipo_de_Registro_do_Caso__c == 'CRA - Mudança de Grade'){ 
            matrizesAtivas = Cra_MatrizesAtivas.getMatrizesAtivas('1','07100713','1');
            if(matrizesAtivas == null){
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não foi possível encontrar matrizes ativas para a mudança solicitada.'));
            }
            else if(matrizesAtivas.isEmpty()){
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não foi possível encontrar matrizes ativas para a mudança solicitada.'));
            }
        }
        else{
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não é possível selecionar a grade para esse tipo de solicitação.'));
        }
    }

    public PageReference selectMatriz() {
        myCase.Grade__c = matriz;
        update myCase;
        return null;
    }
    public List<SelectOption> getMatrizList(){
        List<SelectOption> matrizList = new List<SelectOption>();
        for(Cra_MatrizesAtivas.MatrizesAtivas matriz : matrizesAtivas){
            matrizList.add(new SelectOption(matriz.Descricao,matriz.Descricao));
        }
        return matrizList;
    }

}