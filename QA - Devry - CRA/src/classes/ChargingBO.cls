public with sharing class ChargingBO {
	private static final ChargingBO instance = new ChargingBO();    
    private ChargingBO(){}
    
    public static ChargingBO getInstance() {
        return instance;
    }

    public Cobranca__c updateCreateSapCharging( String id, String codSAP, String empresa, String exercicio, String response ) {
        Cobranca__c cobranca =  new Cobranca__c();
        cobranca.Id = id;
        cobranca.Status__c = 'Em Aberto';
        cobranca.CodigoSAP__c = codSAP;
        cobranca.EmpresaSAP__c = empresa;
        cobranca.ExercicioSAP__c = exercicio; 
        cobranca.Log_Integracao__c = response;
        return cobranca;
    }
}