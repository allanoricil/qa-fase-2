@isTest
public class CRA_NewCasePortalControllerTest {
	    
    public static testMethod void testPadrao(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Coordenador__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            System.currentPageReference().getParameters().put('assuntoInput', 'cartao');
           	controller.runSearch();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.goBack();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.objAttach.Body = Blob.valueOf('1234');
            controller.objAttach.Name = 'Atachment';
            controller.newAttach();
            controller.deleteSelectedAttachment();
            controller.SelectedAttachments.get(0).selected = true;
            controller.deleteSelectedAttachment();
            controller.submit();
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            Decimal valor = controller.valorTotal;
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }
        Cobranca__c cobranca = [SELECT Id, Status__c, Caso__c FROM Cobranca__c WHERE Caso__c =: myCaseId LIMIT 1];
        cobranca.Status__c = 'Pago';
        update cobranca;             
    }
    
	public static testMethod void testOverallDuplicate(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Coordenador__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Status = 'Em andamento';
        caso.Assunto__c = assunto.Id;
        caso.Apex_context__c = true;
        insert caso;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            Test.stopTest();
        }
    }
    
    public static testMethod void testBolsa(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id);
        usuario.Username = '3142A3423@com.br';
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.R_A_do_Aluno__c = '3142A3423';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Bolsa e desconto';
        assunto.Restringe_duplicidade__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Coordenador__c = true;
        assunto.Tipo_Disciplinas__c = 'PENDENTES';
        assunto.Anexo__c = 'Obrigatório';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        motivo.Ativo__c = true;
        insert motivo;
        
        Id myCaseId;
        
        Test.setMock(HttpCalloutMock.class, new CRA_SubjectsQueryMockTest());
        
        insert new WSSetup__c(Endpoint__c = 'login', Name ='Login');
        insert new WSSetup__c(Endpoint__c = 'disciplina', Name ='HistoricoDisciplinas');
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            Boolean hasMotivo = controller.hasMotivo;
            controller.getMotivosList();
            controller.submit();
            controller.myCase.Tipo_de_bolsa__c = 'Corpore';
            controller.submit();
            Test.stopTest();
        }            
    }
    
    public static testMethod void testEstagio(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id);
        usuario.Username = 'T9330232@com.br';
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.R_A_do_Aluno__c = 'T9330232';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Compromisso de Estágio';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        CRA_Empresas.Empresas empresa = new CRA_Empresas.Empresas();
        empresa.codcoligada = '1';
        empresa.codinterno = '1';
        empresa.descricao = 'Nome da empresa';
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();            
            controller.EmpresasList = new List<CRA_Empresas.Empresas>();
            controller.EmpresasList.add(empresa);            
            controller.submit();
            controller.newEmpresa();
            controller.myCase.Nome_da_Empresa_Estagio__c = 'Empresa';
            controller.submit();
            System.currentPageReference().getParameters().put('empresaInput', 'Nome');
            controller.runSearchCompany();            
            controller.empresaCodigo = empresa.codinterno;
            controller.empresaDescricao = empresa.descricao; 
            controller.selectEmpresa();           
            controller.submit();
            controller.myCase.Nome_da_Empresa_Estagio__c = '';
            controller.empresaCodigo = empresa.codinterno;
            controller.empresaDescricao = empresa.descricao; 
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'descrição';
            controller.MyCase.Tipo_de_Termo__c = 'Novo';
            controller.myCase.Numero_da_apolice_do_seguro__c = '3232';
            controller.myCase.Seguradora__c = 'Mapfre';
            controller.myCase.Inicio_da_Vigencia__c = Date.today();
            controller.myCase.Fim_da_Vigencia__c = Date.today();
            controller.submit();
            Test.stopTest();
        }            
    }
    
    public static testMethod void testRessarcimento(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        usuario.Username = '32442I@com.br';
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.R_A_do_Aluno__c = '32442I';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Ressarcimento';
        assunto.Restringe_duplicidade__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Coordenador__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        motivo.Ativo__c = true;
        insert motivo;
        
        Banco__c banco = new Banco__c();
        banco.Codigo__c = '123';
        banco.Name = 'Banco Didical';
        insert banco;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.getMotivosList();
            controller.submit();
            controller.myCase.Tipo_de_Ressarcimento__c = 'Depósito em conta';
            controller.submit();
            controller.myCase.CPF_do_titular__c = '47585748567';
            controller.submit();
            controller.myCase.Tipo_de_Ressarcimento__c = 'Crédito na próxima mensalidade';
            controller.bancoId = banco.Id;
            controller.submit();
            Test.stopTest();
        }            
    }
	
    public static testMethod void testTurmaEPro(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        usuario.Username = '3242M3@com.br';
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.R_A_do_Aluno__c = '3242M3';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Turma - EnglishPro';
        assunto.Restringe_duplicidade__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.Aprovacao_Coordenador__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        motivo.Ativo__c = true;
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController();
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.getTurmasEProSelect();
            controller.submit();
            Test.stopTest();
        }            
    }
    
    public static testMethod void testSubstitutiva(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id);
        usuario.Username = '343432E@com.br';
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        aluno.R_A_do_Aluno__c = '343432E';
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Tipo_Disciplinas__c = 'CURSANDO';
        assunto.Conclusao_apos_pagamento__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        motivo.Ativo__c = true;
        insert motivo;
        
        CRA_NewCasePortalController.SubjectWrapper subject = new CRA_NewCasePortalController.SubjectWrapper();
        subject.Codigo = '3321';
        subject.Name = 'Álgebra Linear';
        subject.Periodo = '2017.2';
        subject.Tipo = 'CURSANDO';
        subject.Selected = true;
        
        Id myCaseId;
        
        insert new WSSetup__c(Endpoint__c = 'login', Name ='Login');
        insert new WSSetup__c(Endpoint__c = 'disciplina', Name ='HistoricoDisciplinas');
        
        Test.setMock(HttpCalloutMock.class, new CRA_SubjectsQueryMockTest());
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
			CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.SubjectWrapperList = new List<CRA_NewCasePortalController.SubjectWrapper>();
            controller.SubjectWrapperList.add(subject);
            controller.orderSubjects();
            controller.SubjectWrapperList.get(0).toDelete = true;
            controller.deleteSelectedSubject();
            controller.SubjectWrapperList.get(0).selected = true;
            controller.orderSubjects();
            controller.motivo = motivo.Id;
            controller.matriz = 'a';
            Decimal valor = controller.valorTotal;
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }
        Cobranca__c cobranca = [SELECT Id, Status__c, Caso__c FROM Cobranca__c WHERE Caso__c =: myCaseId LIMIT 1];
        cobranca.Status__c = 'Pago';
        update cobranca;             
    }
    
    public static testMethod void testCertEnglishPro(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Pagamento__c = false;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Certificado English Pro';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            Boolean testBoolean = controller.hasMotivo;
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }             
    }
    
    public static testMethod void testMudaGrade(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Pagamento__c = false;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Mudança de Grade';
        assunto.Id_externo__c = 'Alteração do responsável financeiro';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.getMatrizesAtivasList();
            Boolean testBoolean = controller.hasMotivo;
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }             
    }
    
    public static testMethod void testEstudoDir(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Pagamento__c = false;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Faturamento de Parcelas';
        assunto.Name = 'Inclusão de Disciplina não Ofertada em Semestre';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }       
    }
    
    public static testMethod void testColacao(){
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aluno'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Pagamento__c = false;
        assunto.Tipo_de_Registro_do_Caso__c = 'CRA - Colação';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
        
        Id myCaseId;
        
        system.runAs(usuario){
            Test.startTest();
           	PageReference pageRef = Page.CRA_NewCasePortal ;
            Test.setCurrentPage(pageRef);      
            CRA_NewCasePortalController controller = new CRA_NewCasePortalController(); 
            controller.idAssunto = assunto.Id;
            controller.selectType();
            controller.confirm();
            controller.myCase.Li_e_Aceito__c = true;
            controller.myCase.Description = 'Abrindo o caso';
            controller.submit();
            myCaseId = controller.myCase.Id;
            Test.stopTest();
        }       
    }
}