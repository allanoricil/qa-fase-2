public with sharing class CRA_BotaoCobrancaController {

	public Case caso;
    public String outputMessage {get; set;}
    public String severity {get; set;}

    public CRA_BotaoCobrancaController(ApexPages.StandardController stdController) {
        
        if(!Test.isRunningTest())
        	stdController.addFields(new List<String>{'Id','Status','Vencimento_do_boleto__c','Valor__c'});
        
        caso = (Case)stdController.getRecord();

    }

    public void GeraCobranca(){


        if(caso.Status!='Em andamento'){
            this.outputMessage = 'A cobrança só pode ser gerada quando Status do caso for \'Em andamento\'.';
            this.severity = 'warning';  
            return; 
        }

        if(caso.Vencimento_do_boleto__c!=null){
            this.outputMessage = 'Uma cobrança já foi gerada para esse Caso.';
            this.severity = 'warning'; 
            return;
        }

        if(caso.Valor__c<=0){
            this.outputMessage = 'O valor da cobrança deve ser maior que zero.';
            this.severity = 'warning'; 
            return;   
        }

        try{
            List<Case> casos = CRA_CobrancaUtil.CreateChargingfromCase(new Set<Id>{caso.Id});
            Database.update(casos);
            this.outputMessage = 'Cobrança gerada com sucesso. Clique no botão abaixo para retornar ao Caso.';
            this.severity = 'confirm';

        }catch(Exception e){
            this.outputMessage = 'Erro ao gerar a cobrança: '+e.getMessage();
            this.severity = 'error';            
        }

    }
    
    public PageReference return2Case(){

        return new PageReference('/'+caso.Id);
    
    }

}