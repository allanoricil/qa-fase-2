/*
    @author Diego Moreira
    @class Classe controladora da pagina de atualização de pagamento inscrição
*/
public with sharing class SRM_BraspagAtualizaStatusController {
    /*
        Construtor
    */
    public SRM_BraspagAtualizaStatusController() {}

    /*
        Metodo principal de execução
    */
    public void execute() {
        String merchantId = Braspag__c.getValues( 'MERCHANTID' ).Valor__c;
        String crypt = ApexPages.currentPage().getParameters().get('crypt');
        Map<String, String> mapDecryptResult = SRM_BraspagCryptographyService.getInstance().braspagDecryptMapResult( merchantId, crypt );

    	QueueBO.getInstance().createQueue( QueueEventNames.UPDATE_BRASPAG_STATUS.name(), String.valueOf( mapDecryptResult ) );            
    }
}