public abstract class SortUtil {
	
    public static void sortStringList(List<String> elements) {
        if (elements == null || elements.size() == 0) {
            return;
        }
        quickSort(elements, 0, elements.size() - 1);
    }

    private static void quickSort(List<String> elements, Integer lowerIndex, Integer higherIndex) {
        Integer i = lowerIndex;
        Integer j = higherIndex;
        String pivot = elements.get(lowerIndex + (higherIndex - lowerIndex) / 2);

        while (i <= j) {
            while (elements.get(i).compareTo(pivot) < 0) {
                i++;
            }

            while (elements.get(j).compareTo(pivot) > 0) {
                j--;
            }

            if (i <= j) {
                exchangeNames(elements, i, j);
                i++;
                j--;
            }
        }
 
        if (lowerIndex < j) {
            quickSort(elements, lowerIndex, j);
        }
        if (i < higherIndex) {
            quickSort(elements, i, higherIndex);
        }
    }

    private static void exchangeNames(List<String> elements, Integer i, Integer j) {
        String temp = elements.get(i);
        elements.set(i, elements.get(j));
        elements.set(j, temp);
    }

    
}