global class FindDuplicateAlunoBatch implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator('SELECT R_A_do_Aluno__c FROM Aluno__c WHERE R_A_do_Aluno__c != null AND Conta__c != null AND IsDeleted = false');

	}
    
    global void execute(Database.BatchableContext BC, List<Aluno__c> alunoList) {       
        List<String> RAList = new List<String>();
        for(Aluno__c aluno : alunoList){
            RAList.add(String.valueOf(aluno.R_A_do_Aluno__c));
        }
        
        List<AggregateResult> alunoAggregateResult = [SELECT R_A_do_Aluno__c, COUNT(Id) FROM Aluno__c WHERE R_A_do_Aluno__c != null AND IsDeleted = false AND R_A_do_Aluno__c IN: RAList GROUP BY R_A_do_Aluno__c HAVING COUNT(Id) > 1 ];
        List<String> RADupList = new List<String>();
        for(AggregateResult alunoAggregate : alunoAggregateResult){
            RADupList.add(String.valueOf(alunoAggregate.get('R_A_do_Aluno__c')));
        }
        
        Map<Id, Aluno__c> alunos = new Map<Id, Aluno__c>([SELECT Id, Conta__c, R_A_do_Aluno__c, IdUsuario__c, CreatedDate FROM Aluno__c WHERE isDeleted = false AND R_A_do_Aluno__c IN: RADupList ORDER BY Conta__c ASC, R_A_do_Aluno__c ASC, CreatedDate DESC]);        
        
        Map<String, Id> idAccountAluno = new Map<String, Id>();
        for(Aluno__c aluno : alunos.values()){
        	if(idAccountAluno.get(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c)) == null){
            	 idAccountAluno.put(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c), aluno.Id);
            }
            else{
                Id idAlunoDestino = idAccountAluno.get(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c));
                Aluno__c alunoDestino = alunos.get(idAlunoDestino);
               	if(String.isBlank(alunoDestino.IdUsuario__c)){
                    if(!String.isBlank(aluno.IdUsuario__c))
                        idAccountAluno.put(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c), aluno.Id);
                }
            }
        }
        
        system.debug(' ' + idAccountAluno);
        
        List<Aluno__c> alunosToUpdate = new List<Aluno__c>();
        for(Aluno__c aluno: alunos.values()){
            if(idAccountAluno.get(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c)) != aluno.Id){
                aluno.Aluno_de_destino__c = String.valueOf(idAccountAluno.get(String.valueOf(aluno.Conta__c) + String.valueOf(aluno.R_A_do_Aluno__c)));
                alunosToUpdate.add(aluno);
            }        
        }
        
        if(!alunosToUpdate.isEmpty())
            update alunosToUpdate;
		
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
}