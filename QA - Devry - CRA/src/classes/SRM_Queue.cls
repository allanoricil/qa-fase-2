public class SRM_Queue {
  @InvocableMethod(label='Get SRM Cria Fila' description='Returns the list of account names corresponding to the specified account IDs.')
  public static void createQueue(List<String> opportunity){
	try {
	Opportunity ac = [SELECT Processo_seletivo__r.C_digo_da_Coligada__c,Processo_seletivo__r.C_digo_Processo_Seletivo__c,
Numeroinscrprocsel__c, Processo_seletivo__r.Sequencial_Processo_Seletivo__c,Account.Name,Account.Sexo__c,Data_Nascimento__c,
Account.Nacionalidade__c,Estado_Civil__c,Account.Nome_do_Pai__c,Account.Nome_da_Mae__c,Account.RG__c,
Account.CPF_2__c,Account.Rua__c,Account.N_mero__c,Account.Bairro__c,Account.Cidade__c,Account.Estado__c,Account.CEP__c,
Account.Phone,Account.Complemento__c,Account.PersonEmail,Processo_seletivo__r.IdPeriodoLetivo__c,Nota__c,
ClassificaProcessoSeletivo__c, X1_OpcaoCurso__r.C_digo_da_Oferta__c 
	 FROM opportunity WHERE id =: opportunity[0]];

  Queue__c queue = new Queue__c();



		
  				// inicio
		        queue.EventName__c          = 'APROVA_CANDIDATO_2';
        		queue.Payload__c            = getJsonRequest(ac);
		        queue.Status__c             = 'CREATED';
		        queue.ProcessBySchedule__c  = false;
		        insert queue;
		        // fim
		        //QueueDAO.getInstance().insertData( queue );
} catch (Exception e) {

} 

  }

  public static String getJsonRequest( Opportunity opportunity ){
    List<Opportunity> lastOpp = [Select CodigoInscricao__c from Opportunity where Processo_seletivo__c =: opportunity.Processo_seletivo__c order by CreatedDate desc limit 1];
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('polo', opportunity.Polo__r.Name != null ? opportunity.Polo__r.Name : '');
        gen.writeFieldName('CandidatoProcSel');
        gen.writeStartObject();

        gen.writeNumberField( 'CODCOLIGADA', 9 );
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        //gen.writeStringField( 'NUMEROINSCPROCSEL',opportunity.CodigoInscricao__c != null ? opportunity.CodigoInscricao__c : '' );
        //gen.writeStringField( 'NUMEROINSCPROCSEL', lastOpp.size() > 0 ? String.valueof(Integer.valueof(lastOpp[0].CodigoInscricao__c)+1) : '' );
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c));
        gen.writeStringField( 'NOME',opportunity.Account.Name.toUpperCase() ); 
        gen.writeStringField( 'SEXO',opportunity.Account.Sexo__c != null ? opportunity.Account.Sexo__c.substring(0,1).toUpperCase() : '' );
        if(opportunity.Data_Nascimento__c == null){
            gen.writeNullField('DATANASC');
        }
        else{
            gen.writeStringField( 'DATANASC',opportunity.Data_Nascimento__c != null ? String.valueOf( opportunity.Data_Nascimento__c) + ' 16:00:00.200' : '' ); 
        }
        gen.writeStringField( 'CIDADENASC',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '');
        gen.writeStringField( 'UFNASC',opportunity.Account.Estado__c != null ? SRM_CadastroCandidato.getUF(opportunity.Account.Estado__c) : '' );      
        gen.writeStringField( 'NACIONALIDADE',opportunity.Account.Nacionalidade__c != null ? opportunity.Account.Nacionalidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADOCIVIL',opportunity.Estado_Civil__c != null ? opportunity.Estado_Civil__c.toUpperCase() : '' );
        gen.writeStringField( 'PAI',opportunity.Account.Nome_do_Pai__c != null ? opportunity.Account.Nome_do_Pai__c.toUpperCase() : '');
        gen.writeStringField( 'MAE',opportunity.Account.Nome_da_Mae__c != null ? opportunity.Account.Nome_da_Mae__c.toUpperCase() : '');
        gen.writeStringField( 'CPFFIADOR','' );
        gen.writeStringField( 'IDENT',opportunity.Account.RG__c !=null ? opportunity.Account.RG__c.replace( '.', '').replace( '-', '' ) : '' );
        gen.writeStringField( 'ORGAOEMISSOR','' );
        gen.writeStringField( 'CPFALUNO',opportunity.Account.CPF_2__c.replace( '.', '').replace( '-', '' )  ); 
        gen.writeStringField( 'TIPOCURSOORIGEM','' );
        gen.writeStringField( 'LOGRADOURO',opportunity.Account.Rua__c != null ? opportunity.Account.Rua__c.toUpperCase() : '' ); 
        gen.writeStringField( 'NUMEROIMOVEL',opportunity.Account.N_mero__c != null ? opportunity.Account.N_mero__c : '' ); 
        gen.writeStringField( 'BAIRRO',opportunity.Account.Bairro__c != null ? opportunity.Account.Bairro__c.toUpperCase() : '' ); 
        gen.writeStringField( 'CIDADE',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADO',opportunity.Account.Estado__c != null ? SRM_CadastroCandidato.getUF(opportunity.Account.Estado__c) : '' ); 
        gen.writeStringField( 'CEP',opportunity.Account.CEP__c != null ? opportunity.Account.CEP__c : '' ); 
        gen.writeStringField( 'TELEFONE',opportunity.Account.Phone != null ? opportunity.Account.Phone : '' ); 
        gen.writeStringField( 'COMPLEMENTO',opportunity.Account.Complemento__c != null ? opportunity.Account.Complemento__c.toUpperCase() : '' ); 
        gen.writeStringField( 'EMAIL',opportunity.Account.PersonEmail != null ? opportunity.Account.PersonEmail.toUpperCase() : '' ); 
        gen.writeNullField  ( 'DATANASCPAI'); 
        gen.writeStringField( 'CIDADENASCPAI','' ); 
        gen.writeStringField( 'UFNASCPAI','' ); 
        gen.writeNullField('DATANASCMAE');
        gen.writeStringField( 'CIDADENASCMAE','' ); 
        gen.writeStringField( 'UFNASCMAE','' ); 
        gen.writeStringField( 'CPFMAE','' );
        gen.writeEndObject();
        gen.writeFieldName('OpcaoCandidato');
        gen.writeStartObject();
        
        //gen.writeNumberField( 'CODCOLIGADA',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_da_Coligada__c ));
        gen.writeNumberField( 'CODCOLIGADA', 9 );
        //gen.writeNumberField( 'IDPERLET',Integer.ValueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) != null ? Integer.valueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) : 0 );
        gen.writeNumberField( 'IDPERLET', 2061 );  
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c) );
        gen.writeStringField( 'CODCURSO', opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c != null ?  opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c : '');
        gen.writeNumberField( 'OPCAO', 1 );
        gen.writeNumberField( 'PONTUACAO',opportunity.Nota__c != null ? opportunity.Nota__c : 0 );
        gen.writeNumberField( 'CLASSIFICACAO', opportunity.ClassificaProcessoSeletivo__c != null ? Integer.valueOf(opportunity.ClassificaProcessoSeletivo__c) : 0 ); 
        gen.writeNumberField( 'IDHABILITACAOFILIAL', 3 ); 
        gen.writeEndObject();
        





    
    gen.writeEndObject(); 
        
        return gen.getAsString(); 
    }
	
	public static String getUF(String estado){
    estado = estado.toUpperCase();
    String retorno = '';
    if(estado =='ACRE')retorno = 'AC';
    if(estado =='ALAGOAS')retorno = 'AL';
    if(estado =='AMAPÁ')retorno = 'AP';
    if(estado =='AMAZONAS')retorno = 'AM';
    if(estado =='BAHIA')retorno = 'BA';
    if(estado =='CEARÁ')retorno = 'CE';
    if(estado =='DISTRITO FEDERAL')retorno = 'DF';
    if(estado =='ESPÍRITO SANTO')retorno = 'ES';
    if(estado =='GOIÁS')retorno = 'GO';
    if(estado =='MARANHÃO')retorno = 'MA';
    if(estado =='MATO GROSSO')retorno = 'MT';
    if(estado =='MATO GROSSO DO SUL')retorno = 'MS';  
    if(estado =='MINAS GERAIS')retorno = 'MG';
    if(estado =='PARÁ')retorno = 'PA';
    if(estado =='PARAÍBA')retorno = 'PB';
    if(estado =='PARANÁ')retorno = 'PR';
    if(estado =='PERNAMBUCO')retorno = 'PE';
    if(estado =='PIAUÍ')retorno = 'PI';
    if(estado =='RIO DE JANEIRO')retorno = 'RJ';
    if(estado =='RIO GRANDE DO NORTE')retorno = 'RN';
    if(estado =='RIO GRANDE DO SUL')retorno = 'RS';
    if(estado =='RONDÔNIA')retorno = 'RO';
    if(estado =='RORAIMA')retorno = 'RR';
    if(estado =='SANTA CATARINA')retorno = 'SC';
    if(estado =='SÃO PAULO')retorno = 'SP';
    if(estado =='SERGIPE')retorno = 'SE';
    if(estado =='TOCANTINS')retorno = 'TO';

    return retorno;
}
		

}