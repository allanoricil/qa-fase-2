@isTest
private class SRM_RestCodigoPromocionalTest {
	
	static testMethod void testDoGetFalse(){

	  		//do request
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();

	    req.requestURI = '/services/apexrest/v1/codigopromocional/';  

	    req.httpMethod = 'GET';

	    RestContext.request = req;
	    RestContext.response = res;

        String results = SRM_RestCodigoPromocional.doGet();
	}



	static testMethod void testDoGetTrue(){

		CodigoPromocional__c codigopromocional = SObjectInstance.getCodPromocional();
		insert codigopromocional;
		String name = codigopromocional.name;

	  		//do request
	    RestRequest req = new RestRequest(); 
	    RestResponse res = new RestResponse();

	    req.requestURI = '/services/apexrest/v1/codigopromocional/' + name;  

		System.debug('-------> ' + req.requestURI);

	    req.httpMethod = 'GET';


	    RestContext.request = req;
	    RestContext.response = res;

        String results = SRM_RestCodigoPromocional.doGet();
	}
	

}