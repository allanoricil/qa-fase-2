public class CRA_ConvertEADPartnerController {

	public String cpf								  { get; set; }
	public String nome								  { get; set; }	
	public String sobrenome							  { get; set; }
	public String email								  { get; set; }	
	public String phone								  { get; set; }	
    public String utm_sfuser						  { get; set; }	
		
    public Lead myLead								  { get; set; }
    public Boolean isEAD							  { get; set; }
    public Boolean hasError							  { get; set; }
    
    
	public CRA_ConvertEADPartnerController(ApexPages.StandardController stdController) {
        
        List<String> myFields = new List<String>();
        myFields.add('Id');
		myFields.add('CPF__c');
		myFields.add('FirstName');
		myFields.add('LastName'); 
        myFields.add('Email'); 
        myFields.add('MobilePhone');
        myFields.add('EAD__c');  
        stdController.addFields(myFields);
        this.myLead = (Lead)stdController.getRecord();
        system.debug('myLead' + myLead);
        this.isEAD = myLead.EAD__c;
        if(!myLead.EAD__c){
            this.hasError = true;
        }
        else{
            this.cpf = myLead.CPF__c ;
        	this.nome = myLead.FirstName;
        	this.sobrenome = myLead.LastName;
        	this.email = myLead.Email;
        	this.phone = myLead.MobilePhone ;
            this.utm_sfuser = String.valueOf(UserInfo.getUserId());
        }
	}
    
    public PageReference redirectOpportunity(){
        String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
        PageReference pageRef = new pageReference('');
        if(System.Network.getNetworkId() != null )
        	pageRef = new pageReference(baseUrl + '/franquias/s/recordlist/Opportunity/Recent');
        else
            pageRef = new pageReference(baseUrl + '/006');
        pageRef.setRedirect(true);
        return pageRef;
    }
    
    public PageReference goBackLead(){
        PageReference pageRef = new pageReference('/'+ myLead.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

}