/*
    @author Luiz Prandini
    @class Classe de cobertura
*/
public with sharing class InstanceToClassCoverage {
    
    public static Account createAccount(){
        return new Account(
            //Name =   'teste123',
            firstName = 'teste',
            lastName = '123',
            CPF_2__c = '412.142.888-98',
            PrecisaAtendimentoEspecial__c = 'Teste'
        ); 
    }
    
    public static Account createAccount(string nome, string cpf, string atendimentoEspecial){
        return new Account(
            Name = nome,
            CPF_2__c = cpf,
            PrecisaAtendimentoEspecial__c = atendimentoEspecial
        ); 
    }
    
    public static Opportunity createOpportunity(){
        return new Opportunity(
            RecordTypeId = OpportunityDAO.RECORDTYPE_GRADUACAO,
            Name        =   'teste123',
            StageName   =   'Inscrito',
            CloseDate   =   System.Today(),
            FezProvaENEM__c = 'Não',
            Telefone__c = '85 3456-6543',
            Celular__c = '85 9876-6789'            
        ); 
    }
    
    public static Opportunity createOpportunity(Processo_seletivo__c processoSeletivo){
        Opportunity opp = createOpportunity();
        opp.name = 'teste123';
        opp.CodigoInscricao__c = '12345678';
        opp.Processo_seletivo__c = processoSeletivo.Id;
        return opp;
    }
    
    public static Opportunity createOpportunity(string nome, string stage, Date closeDate, string fone, string celular, Processo_seletivo__c processoSeletivo){
        return new Opportunity(
            RecordTypeId = OpportunityDAO.RECORDTYPE_GRADUACAO,
            Name        =   nome,
            StageName   =   stage,
            CloseDate   =  closeDate,
            Telefone__c = fone,
            Celular__c = celular,
        	CodigoInscricao__c = '12345678',
			Processo_seletivo__c = processoSeletivo.Id            
        ); 
    }
    
    public static Campus__c createCampus(Institui_o__c instituicao){
        Campus__c result = new Campus__c(
            Name = 'Campus',
            Id_Campus__c = '1',
            CodigoSAP__c = '0001',
            Institui_o__c = instituicao.Id
        );
        
        return result;
    }

	public static Campus__c createCampus(Institui_o__c instituicao, string nome, string idCampus, string codSap){
        Campus__c result = new Campus__c(
            Name = nome,
            Id_Campus__c = idCampus,
            CodigoSAP__c = codSap,
            Institui_o__c = instituicao.Id
        );
        
        return result;
    }
    
    public static Processo_seletivo__c createProcessoSeletivo(Institui_o__c instituicao, Campus__c campus, Per_odo_Letivo__c periodoLetivo){
        Processo_seletivo__c result = new Processo_seletivo__c(
            RecordTypeId = Processo_SeletivoDAO.RECORDTYPE_GRADUACAO,
            Institui_o_n__c = instituicao.Id,
            Campus__c = campus.Id,
            Periodo_Letivo__c = periodoLetivo.Id,
            Name = 'Processo Seletivo Teste',
            Tipo_de_Processo_n__c = 'Vestibular',
            Niveis_de_Ensino__c = 'Graduação',
            Tipo_de_Matr_cula__c = 'Vestibular Tradicional',
            ndice_do_Processo__c = 'A',
            Status_Processo_Seletivo__c = 'Planejado',
            Data_de_in_cio__c = System.today(),
            Data_do_Vestibular__c = System.today() + 30,
            Data_de_encerramento__c = system.today() + 25,
            Data_Validade_Processo__c = System.today(),
            Data_de_Inicio_das_Aulas__c = System.today() + 60,
            Instrucoes_Boleto__c = 'Boleto',
            ID_Academus_ProcSel__c = '12345678',
            Valor_Taxa_Inscricao__c = 50,
            Vencimento_Boleto_dias__c = 5
        );
        
        return result;
    }
    
    public static Lead createLead(){
        return new Lead(
            FirstName   = 'Luiz',
            LastName    = 'Prandini',
            Phone       = '11 8888-8888',
            MobilePhone = '11 98888-8888',
            CEP__c      = '60150-190',
            IES_de_interesse__c = 'Fanor'
        ); 
    }
    
    public static Task createTask(){
        return new Task(
            Conseguiu_Contato__c    = 'Sim',
            Priority                = 'normal',
            Status                  = 'Concluída'
        ); 
    }
    
    public static Processo_seletivo__c createProcess(){
        return new Processo_seletivo__c(
            Name                            =   'teste123',
            ID_Academus_ProcSel__c 			= 	'teste123',
            Data_Validade_Processo__c       =   System.Today()
        ); 
    } 
    
    //ALTER: MERCHANTID
    /*public static MeioPagamentoIES__c createMeioPgto(){
        return new MeioPagamentoIES__c();
    }*/
    
    public static Campus_Payment_Type__c createPaymentType(){
        return new Campus_Payment_Type__c();
    }
    
    public static Titulos__c createTitulo(){
        return new Titulos__c(
            
            
        );
    }
    
    public static Per_odo_Letivo__c createPeriodoLetivo(){
        return new Per_odo_Letivo__c(
            Name = '2015.2',
            Status_do_Per_odo_Letivo__c = 'Ativo',
            CodPeriodo__c = '2015.2',
            IdPeriodo__c = '31'
        );
    }
        
    public static Configura_es_do_Per_odo_Letivo__c createConf(){
        return new Configura_es_do_Per_odo_Letivo__c(

        );
    }
    
    public static CodigoPromocional__c createCodPromo(){
        return new CodigoPromocional__c(
            Name = 'teste'          
        );
    }
    
    public static Institui_o__c createInstituicao(){
        return new Institui_o__c(
            Name = 'teste'  ,
            C_digo_da_Coligada__c = '42332',
            Id_Institui_o__c        = '344',
            CodigoSAP__c            = '3',
            Banco__c                = '3',
            CentroLucro__c          = '4'
        );
        
        /*
		public static Institui_o__c createInstituicao() {
            Institui_o__c result = new Institui_o__c(
                Name = 'Instituição-Apex',
                CentroLucro__c = 'Centro de lucro-Apex',
                EnderecoInstituicao__c = 'Endereço instituição-Apex',
                CNPJ__c = '89.782.746/0001-08',
                Lead_Map__c = 'Fanor',
                C_digo_da_Coligada__c = 'Colig-Apex'            
            );
                
            return result;
		}        
		*/
    }
    
    
    public static Processo_seletivo__c createProcSele(){
        return new Processo_seletivo__c(
            ID_Academus_ProcSel__c = '12345678',
            Name = 'teste'  
        );
    }
    
    public static Oferta__c createOferta(){
        return new Oferta__c(
            Name = 'teste'  
        );
    }
    
    public static Academico__c getAcademico( String accountId, String oppId ) {
        Academico__c academico = new Academico__c();
        academico.Name                  = '0000001';
        academico.RA__c                 = '0000001';
        academico.Curso__c              = 'Tecnologia da informação';
        academico.Semestre__c           = '2/2015';
        academico.SituacaoMatricula__c  = 'Ativa';
        academico.Turno__c              = 'Matutino';       
        academico.Aluno__c              = accountId;
        academico.Oportunidade__c       = oppId;
        
        return academico;
    }   
    
    public static Agendamento__c createAgendamento(Institui_o__c instituicao, Campus__c campus){
        Agendamento__c agendamento = new Agendamento__c();
        agendamento.Institui_o__c        	= instituicao.Id;
        agendamento.Campus__c            	= campus.Id;
        agendamento.Data_Hora_Inicial__c    = System.Today();
        agendamento.Data_Hora_T_rmino__c    = System.Today() + 1;
        agendamento.Capacidade__c		    = 50;
        agendamento.Status__c  				= 'Aberta';
        return agendamento;
    }
    public static Atividade__c createAtividade(){
        Atividade__c task = new Atividade__c();
            task.Name = ' - Atividade';
            task.Status__c = 'Concluída';
            task.StatusContato__c = 'Sim';
            task.Canal__c = 'Phone';
            task.Descri_o__c = 'Atendeu';
        return task;
    }
    
    
}