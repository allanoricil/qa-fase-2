public without sharing class ChecklistBase {
	public static void checklistBaseUpdate(List<Opportunity> allOpps){
		List<Id> oppsId = new List<Id>();
		List<Id> accsId	= new List<Id>();
		List<ChecklistBase__c> checklistBase2Insert = new List<ChecklistBase__c>();

		for(Opportunity op:allOpps){
			Id auxId = op.Id;
			Id auxId2 = op.AccountId;
			oppsId.add(auxId);
			accsId.add(auxId2);
		}

		List<ChecklistBase__c> ckb = [select OportunidadeID__c from ChecklistBase__c where OportunidadeID__c =: oppsId];
		List<Account> allAcc = [SELECT Id, Name, CPF_2__c FROM Account WHERE Id =: accsId];
		system.debug('lista de contas do select: '+allAcc);

		Map<Id,ChecklistBase__c> mapCKB = new Map<Id,ChecklistBase__c>();
		for(ChecklistBase__c cb: ckb){
			if(mapCKB.get(cb.OportunidadeID__c) == null){
				mapCKB.put(cb.OportunidadeID__c,cb);
			}
		}

		Map<Id,Account> mapAcct = new Map<Id,Account>();
		for(Account acc: allAcc){
			for(Opportunity op: allOpps){
				if(op.AccountId == acc.Id){
					if(mapAcct.get(op.Id) == null){
						mapAcct.put(op.Id,acc);
					}
				}
			}
		}
		system.debug('map após select: '+mapAcct);

		for (Opportunity op : allOpps) {
			system.debug('tipo de matrícula: '+op.Tipo_de_Matr_cula__c);
			if ((op.Tipo_de_Matr_cula__c == 'PROUNI') || (op.Tipo_de_Matr_cula__c == 'FIES')) {
				if(mapCKB.get(op.Id) == null) {
					ChecklistBase__c objBase = New ChecklistBase__c();
					Account acx = mapAcct.get(op.Id);
					system.debug('conta aqui: '+acx);
					objBase.OportunidadeID__c = op.Id;
					system.debug('Erro aqui: '+acx.Name);
					objBase.AccountName__c = acx.Name;
					objBase.CPF__c = acx.CPF_2__c;

					DateTime dtCriacao = op.CreatedDate;
					Date novaDtCriacao = date.newinstance(dtCriacao.year(), dtCriacao.month(), dtCriacao.day());

					objBase.CreatedDate__c = novaDtCriacao;
					objBase.ProcessoSeletivoIES__c = op.IES__c;
					objBase.ProcessoSeletivoName__c = op.Processo_seletivo_Nome__c;
					objBase.TipoMatricula__c = op.Tipo_de_Matr_cula__c;

					checklistBase2Insert.add(objBase);
				}
			}			
		}

		if(!checklistBase2Insert.isEmpty()){
			try {
				insert checklistBase2Insert;
			} catch (DMLException e){
				CreateErrorLog.createErrorRecord(e.getMessage(), 'ChecklistBaseTrigger');
			}
		}
	}

	public static void ChecklistBaseInsert(List<Opportunity> allOpps) {
		List<Id> oppsId = new List<Id>();
		List<Id> accsId	= new List<Id>();
		List<ChecklistBase__c> checklistBase2Insert = new List<ChecklistBase__c>();

		for(Opportunity op:allOpps){
			Id auxId = op.Id;
			Id auxId2 = op.AccountId;
			oppsId.add(auxId);
			accsId.add(auxId2);
		}
		List<ChecklistBase__c> ckb = [select OportunidadeID__c from ChecklistBase__c where OportunidadeID__c =: oppsId];
		List<Account> allAcc = [SELECT Id, Name, CPF_2__c FROM Account WHERE Id =: accsId];

		Map<Id,ChecklistBase__c> mapCKB = new Map<Id,ChecklistBase__c>();
		for(ChecklistBase__c cb: ckb){
			if(mapCKB.get(cb.OportunidadeID__c) == null){
				mapCKB.put(cb.OportunidadeID__c,cb);
			}
		}

		Map<Id,Account> mapAcct = new Map<Id,Account>();
		for(Account acc: allAcc){
			for(Opportunity op: allOpps){
				if(op.AccountId == acc.Id){
					if(mapAcct.get(op.Id) == null){
						mapAcct.put(op.Id,acc);
					}
				}
			}
		}

		for (Opportunity op : allOpps) {
			if ((op.Tipo_de_Matr_cula__c == 'PROUNI') || (op.Tipo_de_Matr_cula__c == 'FIES')) {
				ChecklistBase__c objBase = New ChecklistBase__c() ;
				Account acx = mapAcct.get(op.Id);
				
				objBase.OportunidadeID__c = op.Id;
				objBase.AccountName__c = acx.Name;
				objBase.CPF__c = acx.CPF_2__c;

				DateTime dtCriacao = op.CreatedDate;
				Date novaDtCriacao = date.newinstance(dtCriacao.year(), dtCriacao.month(), dtCriacao.day());

				objBase.CreatedDate__c = novaDtCriacao;
				objBase.ProcessoSeletivoIES__c = op.IES__c;
				objBase.ProcessoSeletivoName__c = op.Processo_seletivo_Nome__c;
				objBase.TipoMatricula__c = op.Tipo_de_Matr_cula__c;
			}
		}

		if(!checklistBase2Insert.isEmpty()){
			try {
				insert checklistBase2Insert;
			} catch (DMLException e){
				CreateErrorLog.createErrorRecord(e.getMessage(), 'ChecklistBaseTrigger');
			}
		}
	}
}