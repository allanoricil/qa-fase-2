/*
    @author Diego Moreira
    @class Classe de negocio para o objeto queue
*/
public class QueueBO {  
    /*
        Singleton
    */
    private static final QueueBO instance = new QueueBO();    
    private QueueBO(){}
    
    public static QueueBO getInstance() {
        return instance;
    }

    public static String queueId;
    /*
        @enum para representação dos status da fila
    */
    public enum QueueStatus {
        CREATED,
        SUCCESS,
        ERROR
    }
    
    /* 
        Map de execução dos eventos
    */
    private static map<String, IProcessingQueue> mapToExecute;     
    static {
        mapToExecute = new map<String, IProcessingQueue>();
        mapToExecute.put( QueueEventNames.INSERT_REGISTRATION_TITLES.name(), new SRM_CreateRegistrationTitlesBO() );
        mapToExecute.put( QueueEventNames.UPDATE_REGISTRATION_TITLES.name(), new SRM_UpdateRegistrationTitlesBO() );
        mapToExecute.put( QueueEventNames.UPDATE_BRASPAG_STATUS.name(), new SRM_BraspagAtualizaStatusBO() );
        mapToExecute.put( QueueEventNames.NEW_CANDIDATE.name(), new SRM_CadastroCandidatoBO() );
        mapToExecute.put( QueueEventNames.UPDATE_CANDIDATE.name(), new SRM_AtualizaCandidatoBO() );
        mapToExecute.put( QueueEventNames.SEARCH_STATUS_INSCRIPTION.name(), new SRM_ConsultaStatusInscricaoBO() );
        mapToExecute.put( QueueEventNames.SEARCH_VESTIBULAR_RESULTS.name(), new SRM_ConsultaResultadoVestibularBO() );
        mapToExecute.put( QueueEventNames.ASTERIX_VALIDATE_PHONE_NUMBER.name(), new SRM_ValidaNumeroTelefoneBO() );
        mapToExecute.put( QueueEventNames.SEARCH_BRASPAG_ORDER.name(), new SRM_BraspagConsultaPedidoBO() );
        mapToExecute.put( QueueEventNames.UPDATE_BRASPAG_ORDER_STATUS.name(), new SRM_BraspagAtualizaStatusPedidoBO() );
        mapToExecute.put( QueueEventNames.FICHA_INSCRICAO.name(), new SRM_FichaInscricaoBO() );
        mapToExecute.put( QueueEventNames.FICHA_CAPTACAO.name(), new SRM_FichaInscricaoBO() );
        mapToExecute.put( QueueEventNames.APROVA_CANDIDATO.name(), new SRM_CadastroCandidato() );
        mapToExecute.put( QueueEventNames.APROVA_CANDIDATO_2.name(), new SRM_CadastroCandidato() );
        mapToExecute.put( QueueEventNames.CANCELAMENTO_CONTRATO_SAP.name(), new CancelamentoQueuesProccess() );
        mapToExecute.put( QueueEventNames.CANCELAMENTO_CONTRATO_RM.name(), new CancelamentoQueuesProccess() );
        mapToExecute.put( QueueEventNames.RETORNA_FATURAMENTO_RM.name(), new CancelamentoQueuesProccess() );
        mapToExecute.put( QueueEventNames.RETIRA_FATURAMENTO_RM.name(), new CancelamentoQueuesProccess() );
        mapToExecute.put( QueueEventNames.CRIA_CLIENTE_SAP.name(), new CancelamentoQueuesProccess() );
        mapToExecute.put( QueueEventNames.RETORNA_CLIENTE_SAP.name(), new EstornaTaxasRequerimentoBO() );
        mapToExecute.put( QueueEventNames.ESTORNO_TAXAS_REQUERIMENTO.name(), new EstornaTaxasRequerimentoBO() );
        mapToExecute.put( QueueEventNames.CRIAR_ALUNO_CCD.name(), new CCD_CriarAluno() );
    }
 
    /*
        Cria fila de processamento
        @param eventName string com o nome do evento a ser processado
        @param payload string com os JSON do objeto a ser processado
    */ 
    public void createQueue( String eventName, String payLoad ) {
        Queue__c queue      = new Queue__c();
        queue.EventName__c  = eventName;
        queue.Payload__c    = payLoad;
        queue.Status__c     = QueueStatus.CREATED.name();
        
        QueueDAO.getInstance().insertData( queue ); 
    }

    public String createQueueNotSchedule( String eventName, String payLoad ) {
        Queue__c queue      = new Queue__c();
        queue.EventName__c  = eventName;
        queue.Payload__c    = payLoad;
        queue.Status__c     = QueueStatus.CREATED.name();
        
        QueueDAO.getInstance().insertData( queue ); 
        return queue.Id; 
    }

    public String createQueueWithObjectId( String eventName, String payLoad, String objectId ) {
        Queue__c queue      = new Queue__c();
        queue.EventName__c  = eventName;
        queue.Payload__c    = payLoad;
        queue.Status__c     = QueueStatus.CREATED.name();
        queue.ObjectId__c     = objectId;
        
        QueueDAO.getInstance().insertData( queue ); 
        return queue.Id; 
    }

    /*
        Cria fila de processamento para agendamento apex
        @param eventName string com o nome do evento a ser processado
        @param payload string com os JSON do objeto a ser processado
    */ 
    public String createQueueByScheduleProcess( String eventName, String payLoad ) {
        Queue__c queue              = new Queue__c();
        queue.EventName__c          = eventName;
        queue.Payload__c            = payLoad;
        queue.Status__c             = QueueStatus.CREATED.name();
        queue.ProcessBySchedule__c  = true;
        
        QueueDAO.getInstance().insertData( queue );
        return queue.Id; 
    }

    public String createQueueByScheduleProcess( String eventName, String payLoad, String payloadSAP, String titulo ) {
        Queue__c queue              = new Queue__c();
        queue.EventName__c          = eventName;
        queue.Payload__c            = payLoad;
        queue.PayloadSAP__c         = payLoadSAP;
        queue.Status__c             = QueueStatus.CREATED.name();
        queue.ProcessBySchedule__c  = true;
        
        QueueDAO.getInstance().insertData( queue );
        queueId = queue.id;
        return queue.Id; 
    }
    
    /*
        Atualiza fila de processamento
        @param queueId string com o id da fila para atualização
        @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueue( String queueId, String dmlExceptionStackTrace ) {
        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;
        queue.Status__c                 = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() : QueueStatus.ERROR.name();
        queue.ExceptionStackTrace__c    = dmlExceptionStackTrace;       
        
        QueueDAO.getInstance().updateData( queue );
    }

    public void updateQueue( String queueId, String dmlExceptionStackTrace, String response ) {
        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;
        queue.Status__c                 = dmlExceptionStackTrace.equals('') ? QueueStatus.SUCCESS.name() : QueueStatus.ERROR.name();
        queue.ExceptionStackTrace__c    = dmlExceptionStackTrace;
        queue.PayloadSAP__c             = response;     
        
        QueueDAO.getInstance().updateData( queue );
    }
    
    /*
        Atualiza fila de processamento
        @param queueId string com o id da fila para atualização
        @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueueWithNoPayload( String queueId, String payload ) {
        Queue__c queue                  = new Queue__c();
        queue.Id                        = queueId;
        queue.Status__c                 = payload.equals('') ?QueueStatus.ERROR.name()  :  QueueStatus.SUCCESS.name();
        queue.Payload__c                = payload;       
        
        QueueDAO.getInstance().updateData( queue );
    }

     /*
        Atualiza fila de processamento
        @param queueId string com o id da fila para atualização
        @param dmlExceptionStackTrace string com possivel erro de processamento da fila
    */
    public void updateQueueTemp(String queueId, String payLoad) {
        Queue__c queue              = new Queue__c();
        queue.Payload__c            = payLoad;
        queue.Status__c             = QueueStatus.CREATED.name();
        queue.ProcessBySchedule__c  = false;
        
        QueueDAO.getInstance().updateData( queue );
    }
    /*
        Repocessa os dados da fila
    */ 
    public void reprocessQueue() {
        List<Queue__c> queuesResult = QueueDAO.getInstance().getQueueByStatus( new List<String>{ QueueStatus.CREATED.name() } );
        executeProcessingQueue( queuesResult );
    }    

    /*
        Executa o processamento da fila
        @param queueToProcessing filas para processamento
    */
    public void executeProcessingQueue( List<Queue__c> queueToProcessing ) {
        for( Queue__c queue : queueToProcessing ) {
            if( Test.isRunningTest() ) return;

            if( queue.ProcessBySchedule__c ) return;
            
            if( (!queue.EventName__c.equals( 'INSERT_REGISTRATION_TITLES' )) && (!queue.EventName__c.equals('APROVA_CANDIDATO_2')) && (!queue.EventName__c.equals('EVOLUCIONAL_SERVICES')) ) {
                
                mapToExecute.get( queue.EventName__c ).processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
                system.debug('Entrou ali');
            }else{
                if(queue.EventName__c.equals('APROVA_CANDIDATO_2') || queue.EventName__c.equals('EVOLUCIONAL_SERVICES'))
                    system.debug('Aprova Cand');
                else
                SRM_CreateRegistrationTitlesBO.processingQueueSap( queue.Id, queue.EventName__c,queue.Payload__c, queue.PayloadSap__c);
                
            }
                       
        }
    }

}