public with sharing class CRA_AtualizaFaseOportunidade {
	public static  void atualizaFaseConversao(List<Opportunity> allOpps) {
		//método chamado na conversão de Lead em Oportunidade
		system.debug('minha lista de Oportunidade: '+allOpps);
        Id corporativoId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Corporativo Opp').getRecordTypeId();
        Id empresarialId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Processo empresarial').getRecordTypeId();
        Id sCorporativasId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Soluções Corporativas').getRecordTypeId();
		Set<Id> processId = new Set<Id>();        
		for(Opportunity o: allOpps){
			system.debug('meus Ids de processos seltivos: '+o.Processo_seletivo__c);
            if(String.isBlank(String.valueOf(o.Polo__c))){
                processId.add(o.Processo_seletivo__c);
				o.Apex_Context__c = true;
            }			
		}
		Map<Id,Processo_seletivo__c> allProcess = new Map<Id,Processo_seletivo__c>([SELECT CobrarTaxaInscricao__c,Id, Cobrar_Taxa_de_Inscri_o__c, Precisa_de_prova__c, Precisa_de_entrevista__c FROM Processo_seletivo__c WHERE Id =: processId]);
		system.debug('minha lista de processos com esse ID: '+allProcess);
		for(Opportunity o: allOpps){
            if(o.RecordTypeId != corporativoId && o.RecordTypeId != empresarialId && o.RecordTypeId != sCorporativasId && String.isBlank(String.valueOf(o.Polo__c))){
                system.debug('Cobra taxa de inscrição? '+allProcess.get(o.Processo_seletivo__c).Cobrar_Taxa_de_Inscri_o__c);
                if(allProcess.get(o.Processo_seletivo__c).Cobrar_Taxa_de_Inscri_o__c)
                    o.StageName = 'Pré-Inscrito';
                else
                    o.StageName = 'Inscrito';
            }			
		}
		//try{
		//	update allOpps;
		//} catch(DmlException e){
		//	system.debug('Erro da mudança de fase: '+e);
		//}
	}
}