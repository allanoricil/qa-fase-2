public with sharing class NewCaseController {
    
    public Case newCase {get{
        if(newCase == null){newCase = new Case(); }
        return newCase;
    }set;}
    
    public List<SelectOption> categoriaMotivos { get; set; }
    public List<SelectOption> motivos { get; set; }
    public List<SelectOption> campusInst { get; set; }
    
    public String userName  {get;private set;}
    
    
    List<Motivos__c> sMotivos {get;set;}
    
    public NewCaseController(){
        carregarCategoriasMotivosDB();
        getCampusInstituicoes();
     }
    
    private boolean validaCampos(){
        boolean ok = true;
        if(newCase.Unidade__c == null || newCase.Unidade__c.trim() == ''){
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, 'Unidade: Insira um valor!' );
            Apexpages.addMessage( msg );
            ok = false;
        }
        if(newCase.Categoria_do_Atendimento__c == null || newCase.Categoria_do_Atendimento__c.trim() == ''){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Categoria Atendimento: Insira um valor!' );
            Apexpages.addMessage( msg );
            ok = false;
        }
        if(newCase.Motivo_Atendimento__c ==null || newCase.Motivo_Atendimento__c.trim() == ''){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,'Motivo Atendimento: Insira um valor!');
            Apexpages.addMessage( msg );
            ok = false;
        }
        return ok;
    }
    
    public PageReference save(){
        if(newCase != null && validaCampos()){    
            newCase.Atendente__c =  getLoggedUserName();
            String[] parts = newCase.Unidade__c.split('-');
            newCase.IES__c = parts[0].trim();
            newCase.Campus__c = parts[1].trim();
           
            insert newCase;
            Case found = [SELECT Id, CaseNumber  FROM Case WHERE id =: newCase.Id];
            String caseNumber = found.CaseNumber;
            if(caseNumber  != null){
                ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Caso Adicionado com Sucesso! \n protocolo: ' + caseNumber )); 
                 newCase = new Case();
            }
        }
        return null;
    }
    
    public PageReference carregarMotivos(){
         sMotivos = [SELECT name, categoria__c FROM motivos__c WHERE categoria__c =: newCase.Categoria_do_Atendimento__c];
          motivos = new List<SelectOption>();
         for(Motivos__c mot : sMotivos){
            motivos.add(new SelectOption(mot.name, mot.name));
          }
        
        return null;
    }
    
    private void carregarCategoriasMotivosDB(){
        sMotivos = [SELECT name, categoria__c FROM motivos__c];
        categoriaMotivos = new List<SelectOption>();
        List<String> distinctCategories = new List<String>();
        
        for(Motivos__c mot : sMotivos){
            Boolean found = false;
            for(Integer i=0; i< distinctCategories.size(); i++){
                if((mot.Categoria__c).equalsIgnoreCase(distinctCategories[i])){
                    found = true;
                    break;                    
                }
            }
            if(!found)
            {
        		distinctCategories.add(mot.Categoria__c);     
            	categoriaMotivos.add(new SelectOption(mot.Categoria__c, mot.Categoria__c)); 
        	}
        }
    }
    
    
    public PageReference buscarNomeDoAlunoPorRA(){
        List<Aluno__c> alunos = [SELECT name, e_mail__c, Celular__c, Telefone__c ,Curso__c FROM Aluno__c WHERE R_A_do_Aluno__c =: newCase.R_A_do_Aluno__c];
        if(alunos.size() > 0){
            Aluno__c a = alunos[0];
            String name = a.name;
            if(name != null){   
                Integer index = a.name.indexOf('-'); 
                if(index > 0){
                    newCase.Nome_do_Aluno__c = name.subString(0,index -1).trim();
                }
            }
            newCase.Email_do_Aluno__c = a.e_mail__c;
            newCase.Curso__c = a.Curso__c;
            String telefone = a.Celular__c;
            if(telefone  == null){
                 telefone = a.Telefone__c;  
            }
            newCase.Telefone_do_Aluno__c = telefone;
        }
        return null;
    }
    
    private String getLoggedUserName(){
        String userId = UserInfo.getUserId();
        User u = [SELECT name FROM User WHERE id =: userId];
        return u.name;
    }
    
    private void getCampusInstituicoes(){
        List<Campus__c> campus = [SELECT Institui_o__r.name, name FROM Campus__c ORDER BY Institui_o__r.name];
        campusInst =  new List<SelectOption>();
        for(Campus__c c : campus){
            String campusName = c.Name;
            String instituicaoName = c.Institui_o__r.name;
            if(!campusName.contains(instituicaoName)){
                campusName =  instituicaoName + ' - ' + campusName;
            }
            campusInst.add(new SelectOption(campusName,campusName));     
        }   
    }
    
}