public class importDataFromCSVController {

  public String erro { get; set; }


public List<SelectOption> estadoOptions     {get;set;}

public String[] erros               { get; set; }

public List<SelectOption> poloOptions     {get;set;}

public Blob csvFileBody                     {get;set;}

public string csvAsString                   {get;set;}

public String[] csvFileLines                {get;set;}

public List<Agendamento__c> reagendamentos  {get;set;}

public String estado                        {get;set;}

public Boolean isCSV { get; set; }

public List<Polo__c> estados { get { return [Select Estado__c from Polo__c];} set;}

public Map<String, Polo__c> polos = new Map<String, Polo__c> ();

  public importDataFromCSVController(){
    csvFileLines = new String[]{};
    reagendamentos = New List<Agendamento__c>();
    //getEstados();
    populaMap();
  }


  public void populaMap(){
    for(Polo__c p : [Select id, Name from Polo__c])
      polos.put(p.Name, p);
  }
  
  public void importCSVFile(){


    

    List<Agendamento__c> reagendamentosToInsert = new List<Agendamento__c>();
       try{
           csvAsString = csvFileBody.toString(); 

           csvFileLines = csvAsString.split('\n');

           if(csvFileLines.size() < 1001){

             for(Integer i=1;i< csvFileLines.size();i++){
                 Agendamento__c obj = new Agendamento__c() ;
                 string[] csvRecordData = csvFileLines[i].replace('"','').split(',');
                 String idpolo = polos.get(csvRecordData[0]).Id;
                 obj.Institui_o__c = 'a0SA0000009zd1e';
                system.debug('aqui ' + csvRecordData);
                 obj.Polo__c = idpolo;

                 erros = csvRecordData;

                 obj.Vagas__c = Integer.valueof(csvRecordData[1]);
                 obj.Capacidade__c = Integer.valueof(csvRecordData[1]);
                 obj.Dia__c = GetDateByString(csvRecordData[2]);
                 obj.Hor_rio__c = csvRecordData[3];
                                                                           
                 reagendamentosToInsert.add(obj);
             }
            insert reagendamentosToInsert;
            reagendamentos.addAll(reagendamentosToInsert);
          }
          else{
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'CSV grande demais, limite de 1000. ( '+ String.valueof((csvFileLines.size() - 1)) + ' registros encontrados )');
            ApexPages.addMessage(errorMessage);
          }
        }
        catch (Exception e)
        {
            ApexPages.Message errorMessage = new ApexPages.Message(ApexPages.severity.ERROR, 'Problemas encontrados no CSV.' + ' '+ e.getCause() +
              ' '+ e.getMessage() + ' '+ e.getLineNumber() + ' '+ e.getTypeName() );

        }  
  }

  public Date GetDateByString(String s){
        String[] myDateOnly = s.replace(' ','').split('-');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(0)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(2)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }

    public void getEstados(){
      estadoOptions = new List<SelectOption> ();
      for(Polo__c p : estados)
        estadoOptions.add( new SelectOption(p.Estado__c, p.Estado__c));
    }
    public void getPolos(){
      poloOptions = new List<SelectOption> ();
      for(Polo__c p : [Select Id, Name from Polo__c where Estado__c =: estado])
        poloOptions.add( new SelectOption(p.Id, p.Name));
    }
}