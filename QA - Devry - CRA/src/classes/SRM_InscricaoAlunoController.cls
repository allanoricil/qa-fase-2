/*
    @author Diego Moreira
    @class Classe controladora da pagina de inscrição de aluno
*/
public without sharing class SRM_InscricaoAlunoController {
    public SRM_PesquisaInscricaoController pesquisa         { get; set; }
    public List<Processo_seletivo__c> processoSeletivoList  { get; set; }
    public List<SelectOption> opcaoCurso1List               { get; set; }
    public List<SelectOption> opcaoCurso2List               { get; set; }
    public Account accountToInsert                          { get; set; }
    public Opportunity opportunityToInsert                  { get; set; } 
    public String aviso                                     { get; set; }
    public String formInsc                                  { get; set; }
    public String codigoPromocional                         { get; set; }
    public String nomeDaInstituicao                         { get; set; }
    //public String transferencia                             { get; set; }
    private String idContaDuplicada;
    public String matricula                                 { get; set; }
    public String nomeColegio { get; set; }
    public String idColegio { get; set; }

    private Map<String, Processo_seletivo__c> mapProcessosSeletivos = new Map<String, Processo_seletivo__c>();

    private static String RECORDTYPE_FICHA_SONHOS = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DNA do Aluno').getRecordTypeId();
    private String leadOrigemId = ApexPages.currentPage().getParameters().get('origem');
    private Id processoSeletivoId = ApexPages.currentPage().getParameters().get('processoSeletivoId');
    private Id instituicaoId = ApexPages.currentPage().getParameters().get('instId');
    private String consulta = ApexPages.currentPage().getParameters().get('consulta');

    /*
        Construtor
    */
    public SRM_InscricaoAlunoController() {
        pesquisa = new SRM_PesquisaInscricaoController(); 
        accountToInsert = new Account();
        opportunityToInsert = new Opportunity();
        this.formInsc = 'processoSeletivo';
        
        if( instituicaoId != null )
            this.getInstituicao();
        
        this.getProcessosSeletivos();        
        
        if( leadOrigemId != null )
            this.getLeadData();

        if( processoSeletivoId != null &&
                instituicaoId != null)
            createInscricao();      
    }

    /*
        Busca o nome da Instituição
    */
    private void getInstituicao() {        
        List<Institui_o__c> instituicao = new List<Institui_o__c>();
        instituicao = [SELECT Id, Name FROM Institui_o__c WHERE Id = :instituicaoId];
        this.nomeDaInstituicao = instituicao[0].Name;
    }
    public void getTipoMatricula(){
        List<Processo_seletivo__c> processo = new List<Processo_seletivo__c>();
        processo = [Select Tipo_de_Matr_cula__c from Processo_seletivo__c where Id=: processoSeletivoId];
        this.matricula = processo[0].Tipo_de_Matr_cula__c;
    }
    
    /*
        Busca os processos seletivos
    */
    private void getProcessosSeletivos() {
        
        if(consulta == null){
            //this.processoSeletivoList = null;//new List<Processo_seletivo__c>();
            this.processoSeletivoList = Processo_SeletivoDAO.getInstance().getProcessoSeletivosAtivosByInstituicaoId( ApexPages.currentPage().getParameters().get('instId') );
            
            if(processoSeletivoList != null){
                for( Processo_seletivo__c processo : processoSeletivoList )
                    mapProcessosSeletivos.put( processo.Id, processo );
            }
            else
            {
                this.aviso = 'Nenhum Processo Seletivo';
            }
        }
    }

   
    /*
        Preenche os dados do lead
    */
    private void getLeadData() {
        Lead lead = LeadDAO.getInstance().getLeadById( leadOrigemId )[0];
        
        this.accountToInsert = new Account(
                FirstName = lead.FirstName,
                LastName = lead.LastName,
                Nacionalidade__c = lead.Nacionalidade__c,
                RG__c = lead.RG__c,
                CPF_2__c = lead.CPF__c,
                Passaporte__c = lead.Passaporte__c,
                VoceEstrangeiro__c = lead.VoceEstrangeiro__c,
                VoceTreineiro__c = lead.VoceTreineiro__c,
                Codigo_Especial__c = lead.C_digo_Especial__c,
                Sexo__c = lead.Sexo__c, 
                Phone = lead.Phone,
                PersonMobilePhone = lead.MobilePhone,
                PersonEmail = lead.Email,
                DataNascimento__c = lead.Data_de_Nascimento__c,
                Rua__c = lead.Rua__c,
                N_mero__c = lead.N_mero__c,
                Complemento__c = lead.Complemento__c,
                Bairro__c = lead.Bairro__c,
                CEP__c = lead.CEP__c,
                Cidade__c = lead.Cidade__c,
                Estado__c = lead.Estado__c,
                OutroColegio__c = lead.OutroColegio__c, 
                Naturalidade__c = lead.Naturalidade__c,
                NomeCompletoResponsavel__c = lead.NomeCompletoResponsavel__c,  
                CPFdoResponsavel__c = lead.CPFdoResponsavel__c, 
                Tipo_escola_cursa_ou_cursou_ensino_medio__c = lead.Tipo_de_Escola_cursa_cursou_ensino_medio__c,
                E_portador_de_alguma_aten_o_especial__c = lead.portador_de_necessidades_especiais__c
            );

        this.opportunityToInsert = new Opportunity( CanalInscricao__c = lead.CanalInscricao__c );    
    } 
 
    /*
        Ativa formulario de inscrição
        @action botão Inscrição
    */
    public void createInscricao() {
        this.processoSeletivoId = ApexPages.currentPage().getParameters().get('processoSeletivoId');
        this.formInsc = 'formulario';
        this.createCourseOptions();
        //this.createColleges();
        getTipoMatricula();
    }

    /*
        Cria as opções de cursos disponiveis 
    */
    private void createCourseOptions() {
        this.opcaoCurso1List = new List<SelectOption>();
        this.opcaoCurso2List = new List<SelectOption>();

        for( Oferta__c oferta : OfertaDAO.getInstance().getOfertasByProcessoSeletivo( this.processoSeletivoId ) ) {
            this.opcaoCurso1List.add( new SelectOption( oferta.Id, oferta.Name ) );
            this.opcaoCurso2List.add( new SelectOption( oferta.Id, oferta.Name ) );
        }
    }

    /*
        Cria as opções de colegios
    */
    @RemoteAction
    public static List<Account> createColleges( String nomeColegio ) {
        return Database.query('Select Id, Name, Estado__c from Account where Name like \'%' + String.escapeSingleQuotes(nomeColegio) + '%\' AND RecordTypeId = \'' + AccountDAO.RECORDTYPE_ESCOLA + '\' ');
    }

    /*
        Consulta e atuliza os campos com o retorno dos dados de CEP
    */
    public void atualizaEndereco() {
        Map<String, Object> mapResult = SRM_BuscaCEP.getInstance().getCepByNumber( accountToInsert.CEP__c );

        if( mapResult != null ) { 
            accountToInsert.Rua__c      = (String) mapResult.get( 'logradouro' );
            accountToInsert.Bairro__c   = (String) mapResult.get( 'bairro' );
            accountToInsert.Cidade__c   = (String) mapResult.get( 'cidade' );
            accountToInsert.Estado__c   = (String) mapResult.get( 'estado' );
        }
    } 

    /*
        Executa os metodos para salvamento
        @action botão inscrever
    */
    public PageReference execute() {
        system.debug('entrou aqui');
        if( validaCampos() ) {
            system.debug('passou aqui');
            if( !contaDuplicada() ) {
                system.debug('chegou aqui');
                return createAccount();
            } else if( !oportunidadeDuplicada() ) {
                system.debug('veio aqui');
                return createOpportunity( idContaDuplicada );        
            } else {
                system.debug('alcançou aqui');
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Já existe a inscrição para este processo seletivo!' );
                Apexpages.addMessage( msg );
            }
        }
        return null;   
    }

    /*
        Cria a inscrição do aluno
    */
    private PageReference createAccount() {        
        Savepoint sp = Database.setSavepoint();
        accountToInsert.LeadOrigem__c   = leadOrigemId;
        accountToInsert.RecordTypeId    = RECORDTYPE_FICHA_SONHOS;
        accountToInsert.OwnerId         = userinfo.getUserId();
        accountToInsert.Colegio__c      = idColegio.length() > 0 ? idColegio : null;
        accountToInsert.OutroColegio__c = nomeColegio.length() > 0 && idColegio.length() == 0 ? nomeColegio: '';
      
       
     /*  if (nomeColegio != null && idColegio == null) {
               accountToInsert.OutroColegio__c      = nomeColegio;          
        } 
      */ 
        

        try {
            insert accountToInsert; 
            return createOpportunity( accountToInsert.Id );            
        } catch ( DmlException ex ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, ex.getDmlMessage(0) ); 
            Apexpages.addMessage( msg );
            Database.rollback(sp);
            /*
            Uma ve que a o telefone e celular estão sendo validados na oportunidade é necessário limpar os Ids dos objetos
            porque o SF gera os identificadores e os atribui antes do submit. 
            (by Anderson Souto) */ 
            opportunityToInsert.Id = null;
            accountToInsert.Id = null;
            return null;   
        }
    }

    /*
        Cria a oportunidade da inscrição e processo seletivo
        @param accountId id de relacionamento com a conta/candidato
        @action botão inscrever
    */ 
    private PageReference createOpportunity( String accountId ) {
        Boolean isIntegral = validaCodigoPromocional();
        Processo_seletivo__c processoSeletivo = [Select Id, Name, CobrarTaxaInscricao__c, Data_Validade_Processo__c, Campus__c, Institui_o_n__c, Tipo_do_curso_oferecido__c from Processo_seletivo__c where id=: processoSeletivoId];
        opportunityToInsert.RecordTypeId            = OpportunityDAO.RECORDTYPE_GRADUACAO;
        opportunityToInsert.AccountId               = accountId;
        opportunityToInsert.OwnerId                 = userinfo.getUserId();
        //opportunityToInsert.Name                    = mapProcessosSeletivos.get( this.processoSeletivoId ).Name + ' - ' + mapProcessosSeletivos.get( this.processoSeletivoId ).Data_Validade_Processo__c;
        //opportunityToInsert.CloseDate               = mapProcessosSeletivos.get( this.processoSeletivoId ).Data_Validade_Processo__c;
        //opportunityToInsert.Processo_seletivo__c    = this.processoSeletivoId;
        //opportunityToInsert.StageName               = mapProcessosSeletivos.get( this.processoSeletivoId ).CobrarTaxaInscricao__c == true && isIntegral == false ? 'Pré-Inscrito' : 'Inscrito';
        opportunityToInsert.Name                    = processoSeletivo.Name + ' - ' + processoSeletivo.Data_Validade_Processo__c;
        opportunityToInsert.CloseDate               = processoSeletivo.Data_Validade_Processo__c;
        opportunityToInsert.Processo_seletivo__c    = processoSeletivo.Id;
        opportunityToInsert.StageName               = processoSeletivo.CobrarTaxaInscricao__c == true && isIntegral == false? 'Pré-Inscrito' : 'Inscrito';
        opportunityToInsert.X1_OpcaoCurso__c        = accountToInsert.Z1_opcao_curso__c;
        opportunityToInsert.X2_Op_o_de_curso__c     = accountToInsert.Z2_opcao_curso__c;
        opportunityToInsert.Email__c                = accountToInsert.PersonEmail;
        opportunityToInsert.Telefone__c             = accountToInsert.Phone;
        opportunityToInsert.Celular__c              = accountToInsert.PersonMobilePhone;
        opportunityToInsert.Data_Nascimento__c      = accountToInsert.DataNascimento__c;
        opportunityToInsert.Conta_Empresa_Escola__c = accountToInsert.Colegio__c;
        opportunityToInsert.M_dia_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('utm_source'));
        opportunityToInsert.CPF__c                  = accountToInsert.CPF_2__c;
        opportunityToInsert.Instituicao__c                      = processoSeletivo.Institui_o_n__c;
        opportunityToInsert.Campus_Unidade__c           = processoSeletivo.Campus__c;
        opportunityToInsert.Tipo_do_curso__c            = processoSeletivo.Tipo_do_curso_oferecido__c;
        opportunityToInsert.Amount                  = opportunityToInsert.Processo_seletivo__r.Valor_Taxa_Inscricao__c;
        if( leadOrigemId == null )
            opportunityToInsert.CanalInscricao__c = 'Site - Ficha de inscrição';
        insert opportunityToInsert;
        //return mapProcessosSeletivos.get( this.processoSeletivoId ).CobrarTaxaInscricao__c == true ? paymentRegistration( opportunityToInsert.Id ) : openConfirmacaoInscricao();
        return processoSeletivo.CobrarTaxaInscricao__c == true ? paymentRegistration( opportunityToInsert.Id ) : openConfirmacaoInscricao(); 
    }

    /*
        Valida se a conta ja existe com o mesmo CPF
    */
    private Boolean contaDuplicada() {
        List<Account> accountDuplicate = AccountDAO.getInstance().getAccountByCpf( accountToInsert.CPF_2__c );
        if( accountDuplicate.size() > 0 ) {
            idContaDuplicada = accountDuplicate[0].Id;
            return true;
        }
        return false;  
    }

    /*
        Valida se ja existe a mesma conta e processo seletivivo criados
    */
    private Boolean oportunidadeDuplicada() {
        List<Opportunity> oppDuplicate = OpportunityDAO.getInstance().getOpportunityByAccountIdAndProcessoSeletivo( idContaDuplicada, processoSeletivoId );
        if( oppDuplicate.size() > 0 ) return true;
        return false;    
    }

    /*
        Procura o codigo promocional para relacionamento
    */
    private Boolean validaCodigoPromocional() {
        Boolean isIntegral = false;
        List<CodigoPromocional__c> codigosPromocionais = CodigoPromocionalDAO.getInstance().getCodigoPromocionalByCodigo( codigoPromocional );
        if( !codigosPromocionais.isEmpty() ) {
            List<Opportunity> opportunityList = OpportunityDAO.getInstance().getOpportunityByPromotionalCode( codigosPromocionais[0].Id );
            if( opportunityList.isEmpty() || codigosPromocionais[0].Tipo_de_C_digo__c == 'Coletivo' )
                opportunityToInsert.CodigoPromocional__c = codigosPromocionais[0].Id;
                isIntegral = codigosPromocionais[0].TipoDesconto__c.equals( 'Integral' ) ? true : false;
        }
        return isIntegral;
    }  

    /*  
        Valida campos antes de salvar
    */
    private Boolean validaCampos() {
        Boolean isValid = true;

        if( opportunityToInsert.FezProvaENEM__c.equals( 'Sim' ) &&
                ( opportunityToInsert.QualNotaMediaENEM__c == null ||
                opportunityToInsert.AnoProvaENEM__c == null ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Quando marcado SIM na prova do ENEM é obrigatório o preenchimento dos campos. Qual a Nota Média do ENEM? e Ano da Prova do ENEM!' );
            Apexpages.addMessage( msg );
            isValid = false;
        }
      /*  if( nomeColegio != null && 
                nomeColegio.equals( 'Outros' ) && 
                accountToInsert.OutroColegio__c == null ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Quando selecionado Outros na escola. É obrigatório descrever a escola!' );
            Apexpages.addMessage( msg );
            isValid = false;
        } 
      */
     
     
        if( String.isEmpty( accountToInsert.Z1_opcao_curso__c ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, '1ª Opção de curso: É necessário inserir um valor' );
            Apexpages.addMessage( msg );
            isValid = false;
        }
        if( String.isEmpty( accountToInsert.LastName ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Nome Completo: É necessário inserir um valor' );
            Apexpages.addMessage( msg );
            isValid = false;
        }
        if( String.isEmpty( accountToInsert.Phone ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Telefone: É necessário inserir um valor' );
            Apexpages.addMessage( msg );
            isValid = false;
        }

        System.debug( '>>> codigoPromocional ' + codigoPromocional );
        if( !String.isEmpty( codigoPromocional ) ) {
            List<CodigoPromocional__c> codigosPromocionais = CodigoPromocionalDAO.getInstance().getCodigoPromocionalByCodigo( codigoPromocional );
            if( !codigosPromocionais.isEmpty() ) {
                List<Opportunity> opportunityList = OpportunityDAO.getInstance().getOpportunityByPromotionalCode( codigosPromocionais[0].Id );
                if(!opportunityList.isEmpty() &&  codigosPromocionais[0].Tipo_de_C_digo__c == 'Individual') {
                    ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, Label.SRM_CODIGO_PROMOCIONAL_JA_UTILIZADO );
                    Apexpages.addMessage( msg );
                    isValid = false;
                }
            } else {
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, Label.SRM_CODIGO_PROMOCIONAL_INVALIDO );
                Apexpages.addMessage( msg );
                isValid = false;
            }
        }
        return isValid;        
    }

    /*
        Redireciona para pagina de escolha do tipo de pagamento
        @action botão inscrever
    */
    private PageReference paymentRegistration( String opportunityId ) {
        PageReference paymentPage = Page.SRM_PagamentoInscricao;
         
        Map<String,String> parameters = paymentPage.getParameters();
        parameters.put( 'id', opportunityId );

        return paymentPage;
    }

    /*
        Abre o formulario de confirmação de inscrição
        @action botão inscrever
    */
    public PageReference openConfirmacaoInscricao() {
        PageReference confirmPage = Page.SRM_InscricaoConfirmada;
        Map<String,String> parameters = confirmPage.getParameters();
        parameters.put( 'id', ApexPages.currentPage().getParameters().get('instId') );
        Processo_Seletivo__C processo = [Select Tipo_de_Matr_cula__c, Campus__c from Processo_Seletivo__C where id =: processoSeletivoId];
        Campus__c campus = [Select Name from Campus__c where id =: processo.Campus__c];
        confirmPage.getParameters().put('processo', processo.Tipo_de_Matr_cula__c);
        confirmPage.getParameters().put('campus', campus.Name);
        return confirmPage;
    }
    public PageReference openReagendamento(){
        String oportunidadeId = ApexPages.currentPage().getParameters().get('oportunidadeId');
        
        PageReference reagendamento = Page.ReagendamentoAntigo;
        Map<String, String> parameters = reagendamento.getParameters();

        parameters.put('id', oportunidadeId );
        return reagendamento;
    }
}