/********************************************************************
Author    :   Adílio Santos
Date      :   May 2017
Descr     :   Classe para consumo de Serviços do RM [Usuário]
*********************************************************************/
public class WS_RMSituacaoUsuario {
    
	//Singleton
    private static final WS_RMSituacaoUsuario instance = new WS_RMSituacaoUsuario();
    
    private WS_RMSituacaoUsuario(){}
    
    public static WS_RMSituacaoUsuario getInstance() {
        return instance;
    }
    
    public class Usuario {
        public String TIPODEUSUARIO;
        public String ID;
		public String NOME;
        public String IDCOLIGADA;
		public String IDFILIAL;
		public String NIVELDEENSINO;
        public String CURSO;
        public String DESCRICAO;
        public String IDTIPOCURSO;
    }
	
    @future( Callout=true )
    public Static void syncToServer(String ofertaId, String opportunityID, String cpf) {
        Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
        system.debug('mapTOKEN:::'+mapToken);
        if( mapToken.get('200') != null ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( WSSetup__c.getValues( 'SituacaoUsuario' ).Endpoint__c + cpf);
            req.setMethod('GET');
            req.setHeader( 'content-type', 'application/json' );
            req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SituacaoUsuario' ).API_Token__c );
            req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );            
            try {
                Http h = new Http();
                HttpResponse res = h.send( req );
                if( res.getStatusCode() == 200 ){
                    setDescontos(ofertaId, opportunityID, res.getBody());
                } 
            } catch ( CalloutException ex ) {
                throw ex;
            } 
        } 
    }
    
    public static Decimal setDescontos(String ofertaId, String opportunityID, String json) {
        Decimal retorno,valor;
        json = json.Substring( json.indexOf('['),json.indexOf(']')+1);
        Opportunity oportunity = [Select Id, Tipo_de_Desconto_Aplicado__c, Amount from Opportunity where Id = :opportunityID limit 1];                            
        Oferta__c oferta = [Select Id, C_digo_da_Coligada__c, C_digo_do_Campus__c, Valor_da_Matr_cula__c, X15_de_Desconto_at__c, X10_de_Desconto_at__c, Tipo_do_Curso__c, M_todo_do_Curso__c from Oferta__c where Id = :ofertaId limit 1];
        List<Usuario> u = (List<Usuario>)System.JSON.deserialize(json, List<Usuario>.class);  
        if(oferta.Valor_da_Matr_cula__c > 0)
        {
            for(integer i = 0; i < u.Size(); i++)
            {   
                //Desconto 80% Colaborador
                if(u[i].TIPODEUSUARIO == 'COLABORADOR'){                
                    List<Desconto__c> percentual = [SELECT ID, Percentual_de_Desconto_Presencial__c, Percentual_de_Desconto_Online__c FROM Desconto__c
                                                    WHERE RecordTypeID =: Schema.SObjectType.Desconto__c.getRecordTypeInfosByName().get(u[i].TIPODEUSUARIO).getRecordTypeId()
                                                    AND IDCOLIGADA__c =: oferta.C_digo_da_Coligada__c AND IDFILIAL__c =: oferta.C_digo_do_Campus__c AND Ativo__c =: true LIMIT 1];
                    
                    if (!percentual.isEmpty()) {
                        if(oferta.M_todo_do_Curso__c=='Presencial')
                        {
                            oportunity.Tipo_de_Desconto_Aplicado__c = 'COLABORADOR -> COLIGADA: ' + u[i].IDCOLIGADA + ' | FILIAL: ' + u[i].IDFILIAL + ' | Desconto - Presencial: ' + percentual[0].Percentual_de_Desconto_Presencial__c;
                            valor = oferta.Valor_da_Matr_cula__c - ((oferta.Valor_da_Matr_cula__c * percentual[0].Percentual_de_Desconto_Presencial__c)/100);
                            if(valor != null && (valor < retorno || retorno == null))
                                retorno = valor;
                        }
                        else if(oferta.M_todo_do_Curso__c=='Online')
                        {
                            oportunity.Tipo_de_Desconto_Aplicado__c = 'COLABORADOR -> COLIGADA: ' + u[i].IDCOLIGADA + ' | FILIAL: ' + u[i].IDFILIAL + ' | Desconto - Online: ' + percentual[0].Percentual_de_Desconto_Online__c;
                            valor = oferta.Valor_da_Matr_cula__c - ((oferta.Valor_da_Matr_cula__c * percentual[0].Percentual_de_Desconto_Online__c)/100);
                            if(valor != null && (valor < retorno || retorno == null))
                                retorno = valor;
                        }
                    }
                }
                
                else if(u[i].TIPODEUSUARIO == 'EX-ALUNO'){
                    List<Desconto__c> percentual = [SELECT ID, Percentual_de_Desconto_Presencial__c, Percentual_de_Desconto_Online__c FROM Desconto__c
                                                    WHERE RecordTypeID =: Schema.SObjectType.Desconto__c.getRecordTypeInfosByName().get(u[i].TIPODEUSUARIO).getRecordTypeId()
                                                    AND N_vel_de_Ensino__c =: oferta.Tipo_do_Curso__c AND IDCOLIGADA__c =: oferta.C_digo_da_Coligada__c AND IDFILIAL__c =: oferta.C_digo_do_Campus__c AND Ativo__c =: true LIMIT 1];
                    if (!percentual.isEmpty()) {
                        if(oferta.M_todo_do_Curso__c=='Presencial')
                        {
                            oportunity.Tipo_de_Desconto_Aplicado__c = 'EX-ALUNO -> COLIGADA: ' + u[i].IDCOLIGADA + ' | FILIAL: ' + u[i].IDFILIAL + ' | Desconto - Presencial: ' + percentual[0].Percentual_de_Desconto_Presencial__c;
                            valor = oferta.Valor_da_Matr_cula__c - ((oferta.Valor_da_Matr_cula__c * percentual[0].Percentual_de_Desconto_Presencial__c)/100);    
                            if(valor != null && (valor < retorno || retorno == null))
                                retorno = valor;
                        }
                        else if(oferta.M_todo_do_Curso__c=='Online')
                        {
                            oportunity.Tipo_de_Desconto_Aplicado__c = 'EX-ALUNO -> COLIGADA: ' + u[i].IDCOLIGADA + ' | FILIAL: ' + u[i].IDFILIAL + ' | Desconto - Online: ' + percentual[0].Percentual_de_Desconto_Online__c;
                            valor = oferta.Valor_da_Matr_cula__c - ((oferta.Valor_da_Matr_cula__c * percentual[0].Percentual_de_Desconto_Online__c)/100);    
                            if(valor != null && (valor < retorno || retorno == null))
                                retorno = valor;
                        }
                    }               
                }
                
                if(retorno != null)
                {
                    oportunity.Amount = retorno;
                    update oportunity;
                }
            }
        }
        return retorno;
    }
}