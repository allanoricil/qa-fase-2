public without sharing class ExtensionController
{


    public String nome_sf { get; set; }
    public String sobrenome_sf { get; set; }
    public String telefone_sf { get; set; }
    public String celular_sf { get; set; }
    public String cpf_sf { get; set; }
    public String email_sf { get; set; }

/*
    public void getParametersSF(){
        if(ApexPages.currentpage().getparameters().get('l') != null)
        {
            List<Lead> lead = [Select Name, FirstName, LastName, Phone, Celular__c, CPF__c, Email
            from Lead where id =: ApexPages.currentpage().getparameters().get('l')];
            nome_sf = lead[0].FirstName;
            sobrenome_sf = lead[0].LastName;
            telefone_sf = lead[0].Phone;
            celular_sf = lead[0].Celular__c;
            cpf_sf = lead[0].CPF__c;
            email_sf = lead[0].Email;
        }
    }
*/

    public SRM_PesquisaInscricaoController pesquisa         { get; set; }
    public List<SelectOption> campusEstadoOptions { get; set; }
    public List<SelectOption> EstadoOptions { get; set; }
    public List<SelectOption> ofertaProcessoSeletivoInternacionalOptions { get; set; }
    public List<SelectOption> ofertaProcessoSeletivoOptions { get; set; }
    public List<SelectOption> ofertaProcessoSeletivo1Options { get; set; }
    public String courseTypeSelected { get; set; }
    public String campusEstadoId { get; set; }
    public String campOnline { get; set; }
    public String nomeIES { get; set; }
    public String cpf { get; set; }
    public String nomeCursoInternacional { get; set; }
    public String nomeCurso { get; set; }
    public String nomeCurso1 { get; set; }
    public String processoSeletivoInternacionalID { get; set; }
    public String processoSeletivoID { get; set; }
    public String IESID { get; set; }
    public String processoSeletivo1ID { get; set; }
    public String qtdOfertas { get; set; }
    public String ofertaProcessoSeletivoInternacionalId { get; set; }
    public String ofertaProcessoSeletivoId { get; set; }
    public String ofertaProcessoSeletivo1Id { get; set; }
    public integer existeLeadCPF { get; set; }
    public String opportunityId { get; set; }
    public Opportunity opp { get; set; }
    public String formInsc { get; set; }
    public String leadNome { get; set; }
    public String leadSobreNome { get; set; }
    public String leadEmail { get; set; }
    public String leadCpf { get; set; }
    public String leadCelular { get; set; }
    public String leadCampus { get; set; }
    public String leadOferta { get; set; }
    public String oppNomeCandidato { get; set; }
    public String oppCpf { get; set; }
    public String oppEmail { get; set; }
    public String oppCelular { get; set; }
    public String oppIES { get; set; }
    public String oppCampus { get; set; }
    public String oppCurso { get; set; }
    public String cRegistro { get; set; }
    public String cSexo { get; set; }
    public String cDataNascimento { get; set; }
    public String cComoChegou { get; set; }
    public String cNacionalidade { get; set; }
    public String cNaturalidade { get; set; }
    public String cCep { get; set; }
    public String cNumero { get; set; }
    public String cEndereco { get; set; }
    public String cBairro { get; set; }
    public String cCidade { get; set; }
    public String cEstado { get; set; }
    public String cComplemento { get; set; }
    public String cExpProfissional { get; set; }
    public String cFormacaoAcademica { get; set; }
    public String cUltInstituicao { get; set; } 
    //public String codigoPromocional {get; set;}
    public Account accountToInsert { get; set; }
    public List<SelectOption> countOfertas { get; set; }
    public String aluno { get; set; }
    public PageReference finish;
    private Map<String, Oferta__c> mapOferta = new Map<String, Oferta__c>();
    
    public ExtensionController(ApexPages.StandardController controller) {
        pesquisa = new SRM_PesquisaInscricaoController();
        this.formInsc = 'IbmecStep1';
        campusEstadoOptions = new List<SelectOption>();
        getCampus();
        //finishExtension();
      //  getParametersSF();
    }
     
    public void getCampus() {
        campusEstadoOptions = new List<SelectOption>();
        for( Campus__c campus : getCampusLista() ) {
            campusEstadoOptions.add( new SelectOption( campus.Id, campus.Name ) );
        }
    }
    

    public List<Campus__c> getCampusLista() {
        return [Select Name from campus__C where id in (select Campus__C from Processo_seletivo__C where Campus__C != '' and Ativo__C = true and Status_Processo_Seletivo__C = 'Aberto' and Data_Validade_Processo__c >= TODAY and Matr_cula_Online__c = 'Sim' ) and id in (select Campus__C from Oferta__C where Processo_seletivo__C != '' and M_todo_do_curso__C = 'Presencial')];
    }
    
    public void getOfertaProcessoSeletivoInternacional() {
        
        ofertaProcessoSeletivoInternacionalOptions = new List<SelectOption>();
        for( Oferta__c o : getOfertaProcessoSeletivoInternacionalLista() ) {
            ofertaProcessoSeletivoInternacionalOptions.add( new SelectOption( o.Id, o.Name ) );
        }
    }

    public void getOfertaProcessoSeletivo() {
        nomeIES = getNomeIES(campusEstadoId);
        //IESID = getIdIES(campusEstadoId);
        String campusEstadoId = apexpages.currentpage().getparameters().get('campusEstado');

        ofertaProcessoSeletivoOptions = new List<SelectOption>();
        for( Oferta__c os : getOfertaProcessoSeletivoLista(campusEstadoId, getIdCampus(campusEstadoId)) ) {
            ofertaProcessoSeletivoOptions.add( new SelectOption( os.Id, os.Name ) );
        }

        qtdOfertas = '10';
    }
    
    public void getOfertaProcessoSeletivo1() {
        
        ofertaProcessoSeletivo1Options = new List<SelectOption>();
        for( Oferta__c os1 : getOfertaProcessoSeletivo1Lista() ) {
            System.debug(' nome da oferta '+os1.Name);
            ofertaProcessoSeletivo1Options.add( new SelectOption( os1.Id, os1.Name ) );
        }
    }

    public void getInformacoesAdicionaisInternacional() {
        nomeCursoInternacional = getOfertaCurso(ofertaProcessoSeletivoInternacionalId);
        processoSeletivoInternacionalID = getOfertaProcessoSeletivoId(ofertaProcessoSeletivoInternacionalId);
        this.ofertaProcessoSeletivoInternacionalId = ofertaProcessoSeletivoInternacionalId;//processoSeletivoInternacionalID;
        IESID = getIdIES(ofertaProcessoSeletivoInternacionalId);
    }
    
    public void getInformacoesAdicionais() {
            
        nomeCurso = getOfertaCurso(ofertaProcessoSeletivoId);
        processoSeletivoID = getOfertaProcessoSeletivoId(ofertaProcessoSeletivoId);
        
        
        this.ofertaProcessoSeletivoId = ofertaProcessoSeletivoId;
        List<Processo_seletivo__c> processoSeletivob = new List<Processo_seletivo__c>();
        //processoSeletivob = [SELECT Id, Institui_o_n__c,Campus__c,Tipo_do_curso_oferecido__c FROM Processo_seletivo__c WHERE id =: processoSeletivoID limit 1];
        IESID = getIdIES(ofertaProcessoSeletivoId);
    }
    
    
    public void getInformacoesAdicionais1() {
        
        nomeCurso1 = getOfertaCurso(ofertaProcessoSeletivo1Id);
        processoSeletivoID = getOfertaProcessoSeletivoId(ofertaProcessoSeletivo1Id);
        campOnline = [Select Id,campus__c from Oferta__c where Processo_seletivo__c = :processoSeletivoID limit 1].campus__c;
        this.ofertaProcessoSeletivo1Id = ofertaProcessoSeletivo1Id;
        IESID = getIdIES(ofertaProcessoSeletivo1Id);
    }

    public void getLeadExistencia() {
        System.debug('chegou aqui');
        List<Lead> leads = [SELECT Id, FirstName, LastName, CPF__c, MobilePhone, Email
                    FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];
    
        existeLeadCPF = leads.size();
            
        leadCpf = leads[0].CPF__c;
        leadNome = leads[0].FirstName;
        leadSobreNome = leads[0].LastName;
        leadEmail = leads[0].Email;
        leadCelular = leads[0].MobilePhone;
        this.formInsc = 'IbmecStep2';
    }

    public void getLeadDetalhes() {
        this.formInsc = 'IbmecStep2';
    }
    
	public String getLeadIdByCPF( String cpf ) {
        String retorno = '';
        if (cpf != null) {
            retorno = [SELECT Id, Name FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1].Id;
            
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;                    
    }

    public String getIdCampus( String campusEstadoId){
        return [Select Id_Campus__c from Campus__c where id=:campusEstadoId].Id_Campus__c;
    }

    
    public void setConfirmarDados() {
        // Esta atualização do lead é fundamental, pois armazenamos as informações do segundo step.
        atualizaLead(getLeadIdByCPF(cpf));
      
        if (!contaDuplicada(cpf)) {
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(getLeadIdByCPF(cpf));

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true  and MasterLabel='Convertido' LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            if(lcr.isSuccess()){
                system.debug('converteu!');
                opportunityId = lcr.getOpportunityId();
                atualizaOportunidade(opportunityId);
            }
            System.assert(lcr.isSuccess());
        } else {
             
            // Coleta informações do lead
            Lead leadPopulado = [SELECT Id, Name, FirstName, LastName, CPF__c, Phone, MobilePhone, Email, RG__c, Sexo__c, Data_de_Nascimento__c, Como__c, Nacionalidade__c,
                    Naturalidade__c, CEP__c, N_mero__c, Rua__c, Bairro__c, Cidade__c, Estado__c, Complemento__c, Oferta__c, Nome_do_Curso__c, Processo_seletivo__c
                    FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];

            // Atualiza a conta
            Account contaAtualiza = [SELECT Id 
                FROM Account
                WHERE CPF_2__c = :cpf  LIMIT 1];
            
            // Informações coletadas no step1
            contaAtualiza.FirstName = leadPopulado.FirstName;
            contaAtualiza.LastName = leadPopulado.LastName;
            contaAtualiza.PersonEmail = leadPopulado.Email;
            contaAtualiza.Phone = leadPopulado.Phone;
            contaAtualiza.PersonMobilePhone = leadPopulado.MobilePhone;

            // Informações coletadas no step2
            contaAtualiza.RG__c = leadPopulado.RG__c;
            contaAtualiza.Sexo__c = leadPopulado.Sexo__c;
            contaAtualiza.DataNascimento__c = GetDateByString(cDataNascimento);
            contaAtualiza.Como__c = leadPopulado.Como__c;
            contaAtualiza.Nacionalidade__c = leadPopulado.Nacionalidade__c;
            contaAtualiza.Naturalidade__c = leadPopulado.Naturalidade__c;
            contaAtualiza.CEP__c = leadPopulado.CEP__c;
            contaAtualiza.N_mero__c = leadPopulado.N_mero__c;
            contaAtualiza.Rua__c = leadPopulado.Rua__c;
            contaAtualiza.Bairro__c = leadPopulado.Bairro__c;
            contaAtualiza.Cidade__c = leadPopulado.Cidade__c;
            contaAtualiza.Estado__c = leadPopulado.Estado__c;
            contaAtualiza.Complemento__c = leadPopulado.Complemento__c;

            update contaAtualiza;
            
            // Cria oportunidade
            String tipoProcessoSeletivo = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Cursos de Extensão').getRecordTypeId();
            String aprovacaoDaInscricao = getProcessoSeletivoNecessitaDeAprovacao(leadPopulado.Processo_seletivo__c);
                      
            List<Processo_seletivo__c> processoSeletivoa = new List<Processo_seletivo__c>();       
            
            processoSeletivoa = [SELECT Id, Institui_o_n__c,Campus__c,Tipo_do_curso_oferecido__c FROM Processo_seletivo__c WHERE id =: processoSeletivoID limit 1];
            //IESID = processoSeletivoa[0].Institui_o_n__c;
            List<Opportunity> o = OpportunityDAO.getInstance().getDuplicateOpportunityByCPF(leadPopulado.Processo_seletivo__c, leadPopulado.Oferta__c, cpf);
            system.debug('opp duplicada '+o.size());
            if( o.size() > 0 ) {
                
                o[0].AccountId            = contaAtualiza.Id;
                o[0].Name                 = leadPopulado.Name;
                o[0].Email__c             = leadPopulado.Email;
                o[0].RecordTypeId         = tipoProcessoSeletivo;                
                o[0].StageName            = aprovacaoDaInscricao == 'Sim' ? 'Pré-Inscrito' : 'Inscrito';
                //o[0].StageName            = 'Inscrito';
                o[0].CloseDate            = System.Today();
                o[0].CPF__c               = leadPopulado.CPF__c;
                o[0].FezProvaENEM__c      = 'Não';
                o[0].Telefone__c          = leadPopulado.Phone;
                o[0].Celular__c           = leadPopulado.MobilePhone;
                o[0].Oferta__c            = leadPopulado.Oferta__c;
                o[0].X1_OpcaoCurso__c	  = leadPopulado.Oferta__c;
                o[0].Processo_seletivo__c = leadPopulado.Processo_seletivo__c;
                
                //update opp[0];
                opportunityId = o[0].Id;
                
            }
            
            else{
                system.debug('cria opp do zero');
                Opportunity opp          = new Opportunity (
                    AccountId            = contaAtualiza.Id,
                    Name                 = leadPopulado.Name,
                    Email__c             = leadPopulado.Email,
                    RecordTypeId         = tipoProcessoSeletivo,
                    StageName            = aprovacaoDaInscricao == 'Sim' ? 'Pré-Inscrito' : 'Inscrito',
                    //StageName            = 'Inscrito',
                    CloseDate            = System.Today(),
                    CPF__c               = leadPopulado.CPF__c,
                    FezProvaENEM__c      = 'Não',
                    Telefone__c          = leadPopulado.Phone,
                    Celular__c           = leadPopulado.MobilePhone,
                    Oferta__c            = leadPopulado.Oferta__c,
                    // estou usando essa variável para burlar a atualização de campo:Preenche Unique Name v2
                    // essa atualização visa:garantir que não haverá duas oportunidades com mesmo processo seletivo pra uma conta
                    // contudo essa oportunidade não se encaixa nesse caso,pois é trata-se de um processo de curso de extensão. 
                    Qual_atencao__c='Cursos de Extensão',
                    isClone__c=false,
                    X1_OpcaoCurso__c	 = leadPopulado.Oferta__c,
                    Processo_seletivo__c = processoSeletivoa[0].id,
                    Instituicao__c      = processoSeletivoa[0].Institui_o_n__c,
           			Campus_Unidade__c    = processoSeletivoa[0].Campus__c,
           			Tipo_do_curso__c    = processoSeletivoa[0].Tipo_do_curso_oferecido__c

                );
                  system.debug('recordtypeId  opp =>>>'+opp.RecordTypeId);
                insert opp;
            	opportunityId = opp.Id;
            }
            // Atualiza a oportunidade
            atualizaOportunidade(opportunityId);

            // Muda status do Lead para Convertido e deixa uma observação no mesmo informando que está convertido, pois já existia uma conta criada para o mesmo.
            leadPopulado.Status = 'Convertido';
            System.debug('>>>Antes de atualizar');
            update leadPopulado;


        }
        this.formInsc = 'IbmecStep3';
          System.debug('>>>id opp '+opportunityId);
        getOpportunidadeCampos(opportunityId);
    }



public PageReference efetuarPagamento(){
       
        String actCont = apexpages.currentpage().getparameters().get('actCont');
        opt =  getOpportunityById(opportunityId );
        system.debug('actCont'+actCont);
        opt.aceite_de_Contrato__c=Boolean.valueOf(actCont); 
   
         system.debug('actCont'+opt.aceite_de_Contrato__c);
        update opt;
        if(aluno == 'Sim'){
                   
            finish  =  finishExtension( opportunityId );
            
        }else{

            finish  =   paymentRegistration( opportunityId );
        
        }

        return finish;
    }
    public PageReference finishExtension( String opportunityId){

        Opportunity oportunidade = [ SELECT Id, Processo_seletivo__c FROM Opportunity where Id=: opportunityId ];
        Processo_seletivo__c processo = [Select Id, Tipo_de_Matr_cula__c, Campus__c from Processo_seletivo__c  where Id=: oportunidade.Processo_seletivo__c];
        Campus__c campus = [Select Id, Name from Campus__c where id=: processo.Campus__c];

        PageReference finishPage = Page.FinishExtension;

        Map<String,String> parameters = finishPage.getParameters();
        parameters.put( 'id', opportunityId );
        parameters.put( 'processo', processo.Tipo_de_Matr_cula__c);
        parameters.put( 'campus', campus.Name);

        return finishPage;
    }

    private PageReference paymentRegistration( String opportunityId ) {

        PageReference paymentPage = Page.SRM_PagamentoInscricao;
         
        Map<String,String> parameters = paymentPage.getParameters();
        parameters.put( 'id', opportunityId );

        return paymentPage;
    }
    

    /*
        Valida se a conta ja existe com o mesmo CPF
    */
    private Boolean contaDuplicada(String cpf) {
        List<Account> accountDuplicate = AccountDAO.getInstance().getAccountByCpf(cpf);
        if( accountDuplicate.size() > 0 ) {
            return true;
        }
        return false;  
    }
    
    public void atualizaLead(String leadId) {
        try {
            Lead mLead = [SELECT Id, Name, RG__c, Data_de_Nascimento__c
                                      FROM Lead 
                                      WHERE Id = :leadId
                                      LIMIT 1];

            mLead.RG__c = cRegistro;
            mLead.Sexo__c = cSexo;
            mLead.Como__c = cComoChegou;
            mLead.Nacionalidade__c = cNacionalidade;
            mLead.Naturalidade__c = cNaturalidade;
            mLead.CEP__c = cCep;
            mLead.N_mero__c = cNumero;
            mLead.Rua__c = cEndereco;
            mLead.Bairro__c = cBairro;
            mLead.Cidade__c = cCidade;
            mLead.Estado__c = cEstado;
            mLead.Complemento__c = cComplemento;
           
            update mLead;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
    }
    
    /*
        Redireciona para pagina de escolha do tipo de pagamento
        @action botão inscrever
    */
    
public Opportunity getOpportunityById( String id ) {
        Opportunity opp;

        try {
            opp = [SELECT Id, Nome_do_Candidato__c, CPF__c, Account.PersonMobilePhone,aceite_de_Contrato__c, Account.PersonEmail,Processo_seletivo__r.Institui_o_n__c, NomeCampus__c, Oferta__r.Curso__r.Name, Processo_seletivo__c, IES__c, Sede__c
                    FROM Opportunity
                    WHERE Id = :id];
        } catch (Exception e) {
            opp = null;
        }

        return opp;
    }     
    
    
    public Opportunity opt {get;set;}
    public string attachmentId{get;set;}
    
  public void getOpportunidadeCampos(String oppId) {
        opt =  getOpportunityById(oppId);
        oppNomeCandidato = (opt == null) ? null : opt.Nome_do_Candidato__c;
        oppCpf = (opt == null) ? null : opt.CPF__c;
        oppEmail = (opt == null) ? null : opt.Account.PersonEmail;
        oppCelular = (opt == null) ? null : opt.Account.PersonMobilePhone;
        oppIES = (opt == null) ? null : opt.IES__c;
		//oppCampus = (opp == null) ? null : opp.NomeCampus__c == 'RJ Centro' ? 'Ibmec Online' : opp.NomeCampus__c;
        oppCampus = (opt == null) ? null : opt.NomeCampus__c;
        oppCurso = (opt == null) ? null : opt.Oferta__r.Curso__r.Name;
       attachmentId=[SELECT id FROM Attachment where parentId=:opt.Processo_seletivo__r.Institui_o_n__c limit 1].id;

    }
    
    
    public List<Oferta__c> getOfertaProcessoSeletivoInternacionalLista() {
        String tipoProcessoSeletivo = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Extensão').getRecordTypeId();
        return [Select Id, Name from Oferta__c where Processo_seletivo__r.Campus__c != null and Processo_seletivo__r.Necessita_de_aprova_o_para_Inscri_o__c = 'Sim' and Processo_seletivo__r.Status_Processo_Seletivo__c = 'Aberto'
                and Processo_seletivo__r.Data_Validade_Processo__c >= TODAY and Inativa__c=false and Envia_para_Academus_RM__c=true and Processo_seletivo__r.RecordTypeId = :tipoProcessoSeletivo
                and ID_Academus_Oferta__c != null and C_digo_da_Oferta__c != null and Processo_seletivo__r.Matr_cula_Online__c = 'Sim' and Processo_seletivo__r.Ativo__c = true
                and Processo_seletivo__r.ID_Academus_ProcSel__c != null and Processo_seletivo__r.C_digo_Processo_Seletivo__c != null and M_todo_do_Curso__c = : 'Internacional'];
    }
    // Select CAMPUS
    public List<Oferta__c> getOfertaProcessoSeletivoLista(String campusId, String idcampus) {
        String tipoProcessoSeletivo = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Extensão').getRecordTypeId();
        return [Select Id, Name from Oferta__c where Processo_seletivo__r.Campus__c != null 
                and Processo_seletivo__r.Status_Processo_Seletivo__c = 'Aberto'
                and Processo_seletivo__r.Data_Validade_Processo__c >= TODAY 
                and Inativa__c=false 
                and Envia_para_Academus_RM__c=true 
                and Processo_seletivo__r.RecordTypeId = :tipoProcessoSeletivo
                and ID_Academus_Oferta__c != null 
                and C_digo_da_Oferta__c != null 
                and Processo_seletivo__r.Matr_cula_Online__c = 'Sim' 
                and Processo_seletivo__r.Ativo__c = true
                and Processo_seletivo__r.ID_Academus_ProcSel__c != null 
                and Processo_seletivo__r.C_digo_Processo_Seletivo__c != null 
                and Id_Campus__c =: idcampus
                and M_todo_do_Curso__c =: 'Presencial'];
    }
    
    public List<Oferta__c> getOfertaProcessoSeletivo1Lista() {
        
        String tipoProcessoSeletivo = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Extensão').getRecordTypeId();
         system.debug('tipoProcessoSeletivo '+ tipoProcessoSeletivo);
        return [Select Id, Name from Oferta__c where Processo_seletivo__r.Campus__c != null
         and Processo_seletivo__r.Status_Processo_Seletivo__c = 'Aberto'
                and Processo_seletivo__r.Data_Validade_Processo__c >= TODAY
                and Inativa__c=false 
                and Envia_para_Academus_RM__c=true
                and Processo_seletivo__r.RecordTypeId = :tipoProcessoSeletivo
                and ID_Academus_Oferta__c != null 
                and C_digo_da_Oferta__c != null
                and Processo_seletivo__r.Matr_cula_Online__c = 'Sim'
                and Processo_seletivo__r.Ativo__c = true
                and Processo_seletivo__r.ID_Academus_ProcSel__c != null
                and Processo_seletivo__r.C_digo_Processo_Seletivo__c != null
                and M_todo_do_Curso__c = : 'Online'];
    }

    public String getNomeIES(String campusId) {
        String retorno = '';
        if (campusId != null) {
            retorno = [Select Id, Institui_o__r.Name from Campus__c where Id_Campus__c = :campusId limit 1].Institui_o__r.Name;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;
    }

    public String getIdIES(String ofertaId) {
        String retorno = '';
        if (ofertaId != null) {
            retorno = [Select Id,Processo_seletivo__c, Institui_o_n__c from Oferta__c where Id = :ofertaId limit 1].Institui_o_n__c;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;
    }
    
    public String getOfertaCurso(String ofertaId) {
        String retorno = '';
        if (ofertaId != null) {
            retorno = [Select Id,Name,Curso__r.Name,Inativa__c from Oferta__c where Id = :ofertaId limit 1].Curso__r.Name;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;
    }
    
    public Decimal getOfertaValor(String ofertaId) {
        Decimal retorno;
        if (ofertaId != null) {
            retorno = [Select Id, Valor_da_Matr_cula__c from Oferta__c where Id = :ofertaId limit 1].Valor_da_Matr_cula__c;          
        } 
        return retorno;
    } 
    
    public void getDescontos(String ofertaId, Opportunity opportunity) {
        system.debug('DEGUG do Valor');
        if (ofertaId != null && opportunity.ID != null) {
            WS_RMSituacaoUsuario.syncToServer(ofertaId, opportunity.ID, opportunity.CPF__c.Replace('.','').Replace('-','').Replace(' ',''));
        }      
    }

    public String getOfertaProcessoSeletivoId(String ofertaId) {
        String retorno = '';
        if (ofertaId != null) {
            retorno = [Select Id,Processo_seletivo__c, Institui_o_n__c from Oferta__c where Id = :ofertaId limit 1].Processo_seletivo__c;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        } 
        return retorno;
    }
    
    public String getProcessoSeletivoNecessitaDeAprovacao(String processoId) {
        String necessitaDeAprovacao = '';
        if (processoId != null) {
            necessitaDeAprovacao = [Select Necessita_de_aprova_o_para_Inscri_o__c from Processo_seletivo__c where Id = :processoId limit 1].Necessita_de_aprova_o_para_Inscri_o__c;
        }
        return necessitaDeAprovacao;         
    }

    public static List<Lead> getLeadByCPF( String cpf ) {
        return [SELECT Id, FirstName, LastName, CPF__c, MobilePhone, Email
                FROM Lead
                WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];
    }
    
    public void atualizaOportunidade(String opportunityId) {
        try {
            Opportunity mOpp = [Select Id, CPF__c, Amount, StageName, Account.FirstName, Account.LastName,Estado__c,Data_Nascimento__c, Account.PersonEmail, Processo_seletivo__c, Oferta__c from Opportunity Where Id = : opportunityId ];
            String offProcSelID = ofertaProcessoSeletivoId != null ? ofertaProcessoSeletivoId : ofertaProcessoSeletivo1Id != null ? ofertaProcessoSeletivo1Id : ofertaProcessoSeletivoInternacionalId != null ? ofertaProcessoSeletivoInternacionalId : null;
          
            //mOpp.Amount =  getOfertaValor(ofertaProcessoSeletivoId);
            mOpp.Amount =  getOfertaValor(offProcSelID);
            mOpp.Primeiro_Nome__c = mOpp.Account.FirstName;
            mOpp.Sobrenome__c = mOpp.Account.LastName;
            mOpp.Email__c = mOpp.Account.PersonEmail;
            mOpp.X1_OpcaoCurso__c = mOpp.Oferta__c;
            //mOpp.C_digo_Promocional__c = codigoPromocional;
            String aprovacaoDaInscricao = getProcessoSeletivoNecessitaDeAprovacao(mOpp.Processo_seletivo__c);
            if(aprovacaoDaInscricao=='Sim')
            mOpp.StageName = 'Pré-Inscrito';
            mOpp.Aluno_Ex__c = aluno;
            mOpp.Data_Nascimento__c=GetDateByString(cDataNascimento);
            mOpp.Estado__c=cEstado;
            update mOpp;
            
			//Aplica DESCONTO se for 'COLABORADOR' e 'EX-ALUNO'
            //getDescontos(ofertaProcessoSeletivoId, mOpp);
            getDescontos(offProcSelID, mOpp);
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: erro ao criar opp' + e.getMessage());
        }
    }
    public Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(0)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }


}