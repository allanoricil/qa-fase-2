public with sharing class Executa_Batch_AtualizaProprietarioOpp {
	public static void executabatch(Integer vezes, String ownerid){
		List<Opportunity> opps = [SELECT o.Id FROM Opportunity o WHERE o.OwnerId =: ownerid];
		if(opps.size() > 0){
			String hour = String.valueOf(Datetime.now().hour());
	        Integer min = Datetime.now().minute() + 1;
	        String ss = String.valueOf(Datetime.now().second());

			for(integer i = 1; i <= vezes; i++){
				String sch = ss + ' ' + min + ' ' + hour  + ' * * ?';

				//Passe o Id do usuário entre aspas que terá oportunidades transferidas para o data loader . Exemplo: '005A0000007asu2'        
				Batch_AtualizaProprietarioOpp s = new Batch_AtualizaProprietarioOpp(ownerid);
				system.debug('Tranferência de oportunidades do usuário: ' + ownerid + ' ' +  i + sch + s);
				system.schedule('Tranferência de oportunidades do usuário: ' + ownerid + ' ' +  i, sch, s);
				min++;
			}
		}else{
			system.debug('Usuário não tem oportunidades associadas.');
		}
	}
}