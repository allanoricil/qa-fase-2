/*
@author Willian Matheus
@class Classe para CRA_TurmasEproList
*/
global class CRA_TurmasEproList{

        public static List<TurmasEPro> getTurmasEpro( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
            List<TurmasEPro> turmasEPro = new List<TurmasEPro>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                req.setEndpoint( WSSetup__c.getValues( 'CraTurmasEPROList' ).Endpoint__c + '?CODCOLIGADA=' + codcoligada+'&RA='+ ra +'&CODFILIAL='+ codfilial);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraTurmasEPROList' ).API_Token__c );
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );

                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                    system.debug('serverres.getBody(): ' +  res.getBody());
                    if( res.getStatusCode() == 200 ){
                        turmasEPro = processJsonBody(res.getBody() );
                        return turmasEPro;
                    }
                    else{
                        system.debug( 'CraTurmasEPROList getStatusCode / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraTurmasEPROList CalloutException / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }


        public static List<TurmasEPro> processJsonBody( String jsonBody ) {

            List<TurmasEPro> TurmasEPro = new List<TurmasEPro>();

            //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe TurmasEPro
            //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
            ServicosResponse SR = ( ServicosResponse ) JSON.deserializeStrict( jsonBody, ServicosResponse.class );

            system.debug(SR + ' destaque1 ');
            if( !SR.CraTurmasEPROResult.isEmpty() ){
                for( TurmasEPro item : SR.CraTurmasEPROResult ){
                    TurmasEPro.add(item);
                }
                system.debug('TurmasEPro' + TurmasEPro);
                return TurmasEPro;
                }else{
                    system.debug('lista de response vazia');
                    return null;
                }

        }

        public Class ServicosResponse {
            public List<TurmasEPro> CraTurmasEPROResult;
        }

        global Class TurmasEPro {
            public String codcoligada;
            public String coddisc;
            public String codturma;
            public String nome;

        }

    }