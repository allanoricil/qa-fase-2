public with sharing class selecaoController{

    public string selectedOferta { get; set; }
    public string selectedCampus { get; set; }
    public string selectedEstado { get; set; }
    public string selectedCampusSede { get; set; }
    public string selectedSelectionProcess { get; set; }
    public List<SelectOption> Estados { get; set; }
    public List<SelectOption> options { get; set; }
    public List<SelectOption> programsItems { get; set; }
    public List<SelectOption> campusSede { get; set; }
    public String codPromocional {get; set;}

    // the soql without the order and limit
    private String soql {get;set;}
    private Boolean dadosCand {get;set;}
    public  Opportunity newOpportunity{get;set;}

    // the collection of Accounts to display
    public List<Account> accounts {get;set;}
    public List<opportunity> opp {get;set;}

    public selecaoController(){
        newOpportunity = new Opportunity();
        getEstado();
        getprocessosSeletivos();
        getOfertas();
        getCampusSede();
    }

    /*public Boolean getDadosCand() {
          return dadosCand;
    }*/
    
    public PageReference runSearch() {
        String cpf = Apexpages.currentPage().getParameters().get('cpf');
        soql = 'select id, Name,CPF_2__c, Cidade__c, Estado__c, PersonBirthdate, PersonMobilePhone, PersonEmail from account where account.CPF_2__c = \'' + cpf + '\' limit 1';
        runQuery();    
        return null;
    }
    
    public PageReference createLad() {
        
        if(String.valueOf(accounts[0].CPF_2__c) != null) 
        {
        insert New Lead(lastname = String.valueOf(accounts[0].name),
                        IES_de_interesse__c = ' ',
                        Status = 'Não inscrito', 
                        Phone = String.valueOf(accounts[0].PersonMobilePhone),
                        Cidade__c = String.valueOf(accounts[0].Cidade__c),
                        Estado__c = String.valueOf(accounts[0].Estado__c),
                        CPF__c = String.valueOf(accounts[0].CPF_2__c));

        }
        return null;
    } 
    public PageReference limparForm() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.setRedirect(true);
        return newpage;
	}
    
    public void getprocessosSeletivos(){
        options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        //String selectedCampus = apexpages.currentpage().getparameters().get('parmSelectedCampus');
        String selectedCampusSede = apexpages.currentpage().getparameters().get('parmSelectedCampusSede');
        List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Institui_o_n__c,p.name, p.Campus__c, p.Periodo_Letivo__c,p.Data_de_encerramento__c, Nome_do_Campus__c
                                                 FROM Processo_seletivo__c p
                                                 WHERE p.Campus__c = :selectedCampusSede and
                                                    //p.Institui_o_n__c = :selectedCampus and
                                                 	p.Periodo_Letivo__c != null and
                                                 	p.Formul_rio_Salesforce__c = true and
                                                 	p.Ativo__c = true and
                                                 	p.Inscri_o__c = true and
                                                 	(NOT p.Name like '%english%') and
                                                 	p.Data_de_encerramento__c >= :system.today() and
                                                    p.Periodo_Letivo__c IN ('SOLCORP','EXT') and
                                                    p.Matr_cula_Online__c != 'Sim'
                                                 ORDER BY p.Campus__c ASC];
        for (Processo_seletivo__c proc : lstProcess)  {
            String nome = proc.Name;
            String campus = proc.Nome_do_Campus__c;
            String institution = proc.Institui_o_n__c;
            String session = proc.Data_de_encerramento__c.format();
            options.add(new SelectOption(proc.Id,nome+' - '+ campus));
        }
    }
    
    public List<SelectOption> getCampus(){
        List<SelectOption> Campus = new List<SelectOption>();
        Campus.add(new SelectOption('','Selecionar uma Opção'));
        List<Institui_o__c> lstCampus = [SELECT c.Id,c.Name
                                         FROM Institui_o__c c
                                         ORDER BY c.Name ASC];
        for (Institui_o__c ies : lstCampus)  {
            String nome = ies.Name;
            Campus.add(new SelectOption(ies.id,nome));
        }
        return Campus;
    }
    
    public void getCampusSede()
    {
        campusSede = new List<SelectOption>();
        campusSede.add(new SelectOption('', 'Selecionar uma Opção'));
        String selectedUF = apexpages.currentpage().getparameters().get('parmSelectedEstado');
        if(selectedUF != null && selectedUF != '')
        {
        	List<Campus__c> lstCampusSede = [SELECT Id,Name,Institui_o__c 
                                             FROM Campus__c 
                                             where Estado__c = :selectedUF
                                             ORDER BY Name ASC];
       		for(Campus__c campus : lstCampusSede)
            {
            	String nome = campus.Name;
                String id_instituicao = campus.Institui_o__c;
                String nome_instituicao = '';
                List<Institui_o__c> lstinstituicao = [SELECT Name FROM Institui_o__c where id = :id_instituicao];
                for(Institui_o__c instituicao : lstinstituicao)
                {
                    nome_instituicao = instituicao.Name;                    
                }

				if((campus.Id == 'a0PA000000BXUYVMA5') || (campus.Id == 'a0PA000000F3EIGMA3'))
                {                
                    campusSede.add(new SelectOption(campus.Id, nome));
                }
                else
                {
                    campusSede.add(new SelectOption(campus.Id, nome_instituicao+' - '+nome));    
                }
            	
        	}                    
        }
    }
    
    public List<SelectOption> getEstado(){
       
       
        Estados = new list<SelectOption>();
        Estados.add(new SelectOption('', 'Selecionar uma Opção'));
      
        List<Estados__c> lstEstado = [SELECT Name FROM Estados__c ORDER BY Name ASC];
        
        for(Estados__c estado : lstEstado){
            String nome = estado.Name;
            List<Campus__c> lstEstado_campus = [SELECT id FROM Campus__c WHERE Estado__c != '' and Estado__c = :nome ORDER BY Estado__c ASC];
            if (lstEstado_campus.size() > 0)
            {                
                Estados.add(new SelectOption(nome, nome));
            }           
            
        }
     

        return Estados;
    }
    
     public String getcodPromocionalId(String codPromocional){
        String idCodPromo;
        List<CodigoPromocional__c> codigos = [SELECT Id, Name FROM Codigopromocional__c WHERE Name =: codPromocional];
        if(codigos.size() > 0){
            idCodPromo = codigos[0].id;
        }else{
            return null;
        }
        return idCodPromo;
    }

    public void getOfertas(){
        programsItems = new List<SelectOption>();
        programsItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedProcessoSeletivo');
        if(selectedProcess != null && selectedProcess != ''){
            List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where  o.Processo_seletivo__c = :selectedProcess and Formul_rio_Salesforce__c = true];
            for(Oferta__c o : lstOffers){
                programsItems.add(new SelectOption(o.Id, o.Name));
            }
        }
    }
    
    public Pagereference OnchangeCampus(){
        getprocessosSeletivos();
        return null;
    }

	public Pagereference OnchangeEstado(){
        getCampusSede();
        getprocessosSeletivos();
        getOfertas();
        return null;
    }
    
    public Pagereference OnChangeSelectionCampusSede(){
        getprocessosSeletivos();
        getOfertas();
        return null;
    }
    
    public Pagereference OnchangeSelectionProcess(){
        getOfertas();
        return null;
    }    
    public PageReference OnchangeOferta() {
        return null;
    }    
    public PageReference salvar() {
        try{
            //newOpportunity.Processo_seletivo__c = 'a06K0000004IzQp';
			//newOpportunity.oferta__c = 'a0LK0000003e0Bt';
            newOpportunity.CodigoPromocional__c = getcodPromocionalId(codPromocional);
            newOpportunity.C_digo_Promocional__c = codPromocional; 
            
			if(selectedSelectionProcess != null && selectedSelectionProcess != '') {
                newOpportunity.Processo_seletivo__c = selectedSelectionProcess;
                
                List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Niveis_de_Ensino__c, Institui_o_n__c, Campus__c, Tipo_do_curso_oferecido__c
                                                 FROM Processo_seletivo__c p
                                                      WHERE p.Id = :selectedSelectionProcess LIMIT 1];
            
                newOpportunity.Instituicao__c          = lstProcess[0].Institui_o_n__c;
        		newOpportunity.Campus_Unidade__c       = lstProcess[0].Campus__c;
        		newOpportunity.Tipo_do_curso__c        = lstProcess[0].Tipo_do_curso_oferecido__c;
            
                
                
                
                
                if(lstProcess[0].Niveis_de_Ensino__c == 'Soluções Corporativas')
                {
                    //Id do tipo de registro 'Soluções Corporativas'
                   newOpportunity.RecordTypeId = '012A0000000GpNE';
                    
                }
            }
            if(selectedOferta != null && selectedOferta != '') {
                newOpportunity.oferta__c = selectedOferta;
                newOpportunity.Name = 'opp_Name';
                newOpportunity.StageName = 'Inscrito';
                newOpportunity.CloseDate = date.today();
            }
            newOpportunity.Name = 'opp_Name';
            
            insert newOpportunity;
            
            Integer counter = [ Select count() from opportunity Where Processo_seletivo__c = :selectedSelectionProcess And oferta__c = :selectedOferta];
            Integer counter2 = [ Select count() from opportunity Where Processo_seletivo__c = :selectedSelectionProcess];
			//String Oppname = [ Select Nome_do_Candidato__c from opportunity Where id = newOpportunity.id];
               
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Dados Salvos com Sucesso!'));
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Seu número de inscrição é ' + counter2 + '.' ));
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Parabéns! Você é o ' + counter + '° inscrito para esse curso.'));

            newOpportunity.Posi_o_de_Inscri_o_Curso__c = String.valueOf(counter);
			//newOpportunity.Name = Oppname;
                
            upsert newOpportunity;
            newOpportunity = new Opportunity();
            //getCampus();
            //
            
        // Estados = new list<SelectOption>();
       // Estados.add(new SelectOption('', 'Selecionar uma Opção'));
       
            /*
            getEstado();
      
            getCampusSede();
            getprocessosSeletivos();
            getOfertas();
*/

            
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        return null;
    }
    
    public void runQuery() {
        try {
            accounts = Database.query(soql);
        } catch (Exception e) {
            //soql = '1';
            soql = string.valueof(e);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Dados Não Encontrados'));
        }
    }
    
    public String debugSoql {
        get { return soql;}
        set;
    }
}