global class StatusCandidato implements Schedulable {


    global StatusCandidato(){
    	updateStage();
    }

	global static void execute(SchedulableContext sc) {
		String sch = '0 30 23 ? * MON-FRI *';
		StatusCandidato s = new StatusCandidato();
		System.Schedule('Status EAD', sch, s);
		updateStage();

	}

	public Static void updateStage(){
		List<Oportunidade_EAD__c> opptoupdate = new List<Oportunidade_EAD__c>();
		List<Oportunidade_EAD__c> ausentes = [Select Id, Nota__c, StageName__c, 
		DiaProva__c, Agendamentos__c From Oportunidade_EAD__c where DiaProva__c <: system.today() and StageName__c != 'Aprovado' and StageName__c != 'Reprovado'];
	
		for(Oportunidade_EAD__c ead : ausentes){
			ead.StageName__c = 'Ausente';
			opptoupdate.add(ead);
		}
		update opptoupdate;
	}


	/* public void sendMail(String address, String subject, String body) {
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {address};
        mail.setToAddresses(toAddresses);
        mail.setSubject(subject);
        mail.setPlainTextBody(body);
        Messaging.SendEmailResult[] results = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
	}
	sendmail('emaik', 'assunto', 'corpo!');*/

	





}