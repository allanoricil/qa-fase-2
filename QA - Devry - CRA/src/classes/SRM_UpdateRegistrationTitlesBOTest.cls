@isTest
private class SRM_UpdateRegistrationTitlesBOTest {

    static testMethod void myUnitTest() {
       
       Account acc = InstanceToClassCoverage.createAccount();
       insert acc;
        
        Institui_o__c instituicao = InstanceToClassCoverage.createInstituicao();
        insert instituicao;
        
        Campus__c campus = InstanceToClassCoverage.createCampus(instituicao);
        insert campus;
        
        Per_odo_Letivo__c periodoLetivo = InstanceToClassCoverage.createPeriodoLetivo();
        insert periodoLetivo;
        
        Processo_seletivo__c processoSeletivo = InstanceToClassCoverage.createProcessoSeletivo(instituicao, campus, periodoLetivo);
		insert processoSeletivo;
       
       Opportunity opp = InstanceToClassCoverage.createOpportunity(processoSeletivo);
       opp.CloseDate = Date.today();
       opp.Name = 'Teste Opportunity';
       opp.AccountId = acc.Id;
       insert opp;
       
       Titulos__c tit = InstanceToClassCoverage.createTitulo();
       tit.Inscricao__c         = opp.Id;
       tit.EmpresaSAP__c        = '2';
       tit.CodigoSAP__c         = '3';
       tit.ExercicioSAP__c      = '3';
       tit.CodigoAutorizacao__c = '4';
       tit.Bandeira__c          = '5';
       tit.Forma__c             = '6';
       tit.Parcelado__c         = 1;
       
       insert tit;
       
       test.StartTest();
       
       SRM_UpdateRegistrationTitlesBO.createTitlesToJson(tit);
       
       test.StopTest();
       
        
    }
}