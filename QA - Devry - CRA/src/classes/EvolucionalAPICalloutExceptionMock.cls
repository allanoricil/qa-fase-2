@isTest
public class EvolucionalAPICalloutExceptionMock implements HttpCalloutMock {

    protected Integer code;
    protected String status;
    protected String body;
    protected Map<String, String> responseHeaders;

    public EvolucionalAPICalloutExceptionMock(Integer code, 
                                              String status, 
                                              String body, 
                                              Map<String, String> responseHeaders) {
        this.code = code;
        this.status = status;
        this.body = body;
        this.responseHeaders = responseHeaders;
    }

    public HTTPResponse respond(HTTPRequest req) {
        CalloutException e = (CalloutException)CalloutException.class.newInstance();
        e.setMessage('Callout Exception!');
        throw e;
    }

}