/*
	@author Diego Moreira
	@class Classe de serviço para codigo promocional
*/
@RestResource (urlMapping='/v1/codigopromocional/*')
global class SRM_RestCodigoPromocional {
	/*
		Serviço de consulta de dados para codigo promocional
    */
	@HttpGet
	global static String doGet() {
		String jsonResult;

		try {
			String codigoPromocional = RestContext.request.requestURI.substring( RestContext.request.requestURI.lastIndexOf('/')+1 );
			List<CodigoPromocional__c> codigosPromocionais = CodigoPromocionalDAO.getInstance().getCodigoPromocionalByCodigo( codigoPromocional );
			if( codigosPromocionais.size() > 0 )
				jsonResult = jsonResponse( codigosPromocionais );
			else
				jsonResult = jsonResponse( 'NOK', 'Código invalido!' );	
		} catch( Exception ex ) {
			jsonResult = jsonResponse( 'NOK', ex.getMessage() );
		}		

		return jsonResult;
	}

	/*
		JSON de retorno de serviço
		@action GET -> Dados da consulta
    */
    private static String jsonResponse( List<CodigoPromocional__c> codigosPromocionais ) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'status', 'OK' );
        gen.writeStringField( 'codigo', codigosPromocionais[0].Name );
        gen.writeStringField( 'tipoDesconto', codigosPromocionais[0].TipoDesconto__c ); 
        gen.writeEndObject(); 

    	return gen.getAsString(); 
    }

	/*
		JSON de retorno de serviço
		@action GET -> ERROR na consulta
    */
    private static String jsonResponse( String status, String mensagem ) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'status', status ); 
        gen.writeStringField( 'mensagem', mensagem ); 
        gen.writeEndObject(); 

    	return gen.getAsString(); 
    }
}