/*********************************************
+Class to test updateOppOwnerPosTestClass
+for graduate program
**********************************************/
@isTest (SeeAllData = true)
private class updateOppOwnerPosTestClass{
    
    static testMethod void createOpp(){
        
        Account AccId;
        Processo_seletivo__c ProcSelId;
        Oferta__c OfeId;


        AccId = [select id from Account WHERE CPF_2__c != null limit 1];
        ProcSelId = [select id, Institui_o_n__c, Campus__c, Tipo_do_curso_oferecido__c  from Processo_seletivo__c limit 1];
        OfeId = [select id from Oferta__c where Processo_seletivo__c =: ProcSelId.Id limit 1 ];

        Opportunity Opp1 = new Opportunity(
                                            Name = 'Test12454',
                                            AccountId = AccId.Id,
                                            CloseDate = system.today(),
                                            StageName = 'Inscrito',
                                            Processo_seletivo__c = ProcSelId.id,
                                            Oferta__c = OfeId.id,
            								CPF__c = '613.638.878-23',
             								Instituicao__c          = ProcSelId.Institui_o_n__c,
        									Campus_Unidade__c       = ProcSelId.Campus__c,
        									Tipo_do_curso__c        = ProcSelId.Tipo_do_curso_oferecido__c
                                          );
        
        insert Opp1;
        update Opp1;
    }
}