@IsTest
public class ObjectFactoryTest {
	@IsTest    
    public static void shouldCreateBasicAccount(){
        
        System.assertNotEquals(createAccount().id, '');
        
    }
    @IsTest
    public static void shouldCreateBasicOpportunity(){
        Account acc = createAccount(); 
        Opportunity opp = new Opportunity();
        
        Opportunity oppNew = ObjectFactory.CREATE_AND_GET_OPPORTUNITY(acc,opp);
        
        System.assertNotEquals(oppNew.RecordTypeId,'');
    }
    
    private static Account createAccount(){
        Account acc = new Account (LastName = 'Joao Teste', CPF_2__C = '723.631.738-13' );
        Account basicAccount = ObjectFactory.CREATE_AND_GET_ACCOUNT(acc);
        
        insert basicAccount;
        
        return basicAccount;
    }

}