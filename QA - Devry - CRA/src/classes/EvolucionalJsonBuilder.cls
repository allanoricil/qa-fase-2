public with sharing class EvolucionalJsonBuilder {
	
	public static String buildRequestForUserCreationService(Opportunity opp){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('token','756C85B5-71AC-469A-979D-D8AD020D3154');
        
        if(opp.Email_form__c == null){
            gen.writeNullField('email');
        }else{
            gen.writeStringField('email', opp.Email_form__c);
        }
        gen.writeNullField('cityId');   

        if(opp.Name == null){
            gen.writeNullField('cityId');
        }else{
            gen.writeStringField('name', opp.Name);
        }
        
        if(opp.Celular__c == null){
            gen.writeNullField('mobile');
        }else{
            gen.writeStringField('mobile', opp.Celular__c.trim().replace('-',''));
        }

        if(opp.Senha__c == null){
            gen.writeNullField('password');
        }else{
            gen.writeStringField('password', opp.Senha__c);        
        }
        
        gen.writeStringField('degreeId', '7');
        gen.writeNullField('schoolId');
        gen.writeNullField('schoolCityId');
        gen.writeNullField('schoolName');
        gen.writeStringField('courseId', '1');
        gen.writeNullField('universityResponse');
        gen.writeEndObject();
        return gen.getAsString();
    }


    public static String buildRequestForListExamsService( Opportunity opportunity ){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('polo', opportunity.Polo__r.Name != null ? opportunity.Polo__r.CodPolo__c+'-'+opportunity.Polo__r.Name : '');
        gen.writeStringField('IdProcessoACD', opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c != null ? opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c : '');
        gen.writeFieldName('CandidatoProcSel');
        gen.writeStartObject();

        gen.writeNumberField( 'CODCOLIGADA', 9 );
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        //gen.writeStringField( 'NUMEROINSCPROCSEL',opportunity.CodigoInscricao__c != null ? opportunity.CodigoInscricao__c : '' );
        //gen.writeStringField( 'NUMEROINSCPROCSEL', lastOpp.size() > 0 ? String.valueof(Integer.valueof(lastOpp[0].CodigoInscricao__c)+1) : '' );
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c));
        gen.writeStringField( 'NOME',opportunity.Account.Name.toUpperCase() ); 
        gen.writeStringField( 'SEXO',opportunity.Account.Sexo__c != null ? opportunity.Account.Sexo__c.substring(0,1).toUpperCase() : '' );
        if(opportunity.Data_Nascimento__c == null){
            gen.writeNullField('DATANASC');
        }else{
            gen.writeStringField( 'DATANASC', String.valueOf( opportunity.Data_Nascimento__c) + ' 16:00:00.200'); 
        }
        gen.writeStringField( 'CIDADENASC',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '');
        gen.writeStringField( 'UFNASC',opportunity.Account.Estado__c != null ? opportunity.Account.Estado__c : '' );      
        gen.writeStringField( 'NACIONALIDADE',opportunity.Account.Nacionalidade__c != null ? opportunity.Account.Nacionalidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADOCIVIL',opportunity.Estado_Civil__c != null ? opportunity.Estado_Civil__c.toUpperCase() : '' );
        gen.writeStringField( 'PAI',opportunity.Account.Nome_do_Pai__c != null ? opportunity.Account.Nome_do_Pai__c.toUpperCase() : '');
        gen.writeStringField( 'MAE',opportunity.Account.Nome_da_Mae__c != null ? opportunity.Account.Nome_da_Mae__c.toUpperCase() : '');
        gen.writeStringField( 'CPFFIADOR','' );
        gen.writeStringField( 'IDENT',opportunity.Account.RG__c !=null ? opportunity.Account.RG__c.replace( '.', '').replace( '-', '' ) : '' );
        gen.writeStringField( 'ORGAOEMISSOR','' );
        gen.writeStringField( 'CPFALUNO',opportunity.Account.CPF_2__c.replace( '.', '').replace( '-', '' )  ); 
        gen.writeStringField( 'TIPOCURSOORIGEM','' );
        gen.writeStringField( 'LOGRADOURO',opportunity.Account.Rua__c != null ? opportunity.Account.Rua__c.toUpperCase() : '' ); 
        gen.writeStringField( 'NUMEROIMOVEL',opportunity.Account.N_mero__c != null ? opportunity.Account.N_mero__c : '' ); 
        gen.writeStringField( 'BAIRRO',opportunity.Account.Bairro__c != null ? opportunity.Account.Bairro__c.toUpperCase() : '' ); 
        gen.writeStringField( 'CIDADE',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADO',opportunity.Account.Estado__c != null ? opportunity.Account.Estado__c : '' ); 
        gen.writeStringField( 'CEP',opportunity.Account.CEP__c != null ? opportunity.Account.CEP__c : '' ); 
        gen.writeStringField( 'TELEFONE',opportunity.Account.Phone != null ? opportunity.Account.Phone : '' ); 
        gen.writeStringField( 'COMPLEMENTO',opportunity.Account.Complemento__c != null ? opportunity.Account.Complemento__c.toUpperCase() : '' ); 
        gen.writeStringField( 'EMAIL',opportunity.Account.PersonEmail != null ? opportunity.Account.PersonEmail.toUpperCase() : '' ); 
        gen.writeNullField  ( 'DATANASCPAI'); 
        gen.writeStringField( 'CIDADENASCPAI','' ); 
        gen.writeStringField( 'UFNASCPAI','' ); 
        gen.writeNullField('DATANASCMAE');
        gen.writeStringField( 'CIDADENASCMAE','' ); 
        gen.writeStringField( 'UFNASCMAE','' ); 
        gen.writeStringField( 'CPFMAE','' );
        gen.writeEndObject();
        gen.writeFieldName('OpcaoCandidato');
        gen.writeStartObject();
        
        //gen.writeNumberField( 'CODCOLIGADA',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_da_Coligada__c ));
        gen.writeNumberField( 'CODCOLIGADA', 9 );
        //gen.writeNumberField( 'IDPERLET',Integer.ValueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) != null ? Integer.valueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) : 0 );
        gen.writeNumberField( 'IDPERLET', 2061 );  
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c) );
        gen.writeStringField( 'CODCURSO', opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c != null ?  opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c : '');
        gen.writeNumberField( 'OPCAO', 1 );
        gen.writeNumberField( 'PONTUACAO',opportunity.Nota__c != null ? opportunity.Nota__c : 0 );
        gen.writeNumberField( 'CLASSIFICACAO', opportunity.ClassificaProcessoSeletivo__c != null ? Integer.valueOf(opportunity.ClassificaProcessoSeletivo__c) : 0 ); 
        gen.writeNumberField( 'IDHABILITACAOFILIAL', 3 ); 
        gen.writeEndObject();
    
    	gen.writeEndObject(); 
        
        return gen.getAsString(); 
    }

    public Static String buildPayloadSAP(Opportunity opp, String userId){
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'token','18B257B8-9280-4B3C-B14E-674AD4EB0148');
        
        if((opp.Account.PersonEmail == null) && (userId != null)){
            gen.writeNullField('email');
            gen.writeStringField( 'userId', userId);
        }

        if((opp.Account.PersonEmail != null) && (userId == null)){
            gen.writeStringField( 'email', opp.Email__c);
            gen.writeNullField('userId');
        }

        if((opp.Account.PersonEmail != null) && (userId != null)){
            gen.writeNullField('email');
            gen.writeStringField( 'userId', userId);
        }
        gen.writeEndObject();
        return gen.getAsString();
    }
}