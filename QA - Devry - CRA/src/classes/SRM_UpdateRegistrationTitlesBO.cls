/*
    @author Diego Moreira
    @class Classe de negocio para atualização de titulos na integração
*/
public class SRM_UpdateRegistrationTitlesBO implements IProcessingQueue {
	/* 
        Processa a fila de atualização de atualização de titulos no SAP
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, eventName, payload );     
    }

    /*
		Processa a fila de atualização de atualização de titulos no SAP
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String eventName, String payload ) {
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null ) {
    		HttpRequest req = new HttpRequest();
	        req.setEndpoint( WSSetup__c.getValues( 'SAP-FrenteCaixa' ).Endpoint__c );
	        req.setMethod( 'POST' );
	        req.setHeader( 'content-type', 'application/json' );
	        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-FrenteCaixa' ).API_Token__c );
	        req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
	        req.setBody( payload );
	 		
	        try {
	            Http h = new Http();
	            HttpResponse res = h.send( req );

	            if( res.getStatusCode() == 200 ) {
	            	Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
			    	Map<String, Object> mapTitulos = ( Map<String, Object> ) JSON.deserializeUntyped( payload );
                       system.debug('resultado do payload'+payload);
                     try{
                     string EmpresaSAP='';
                      string CodigoSAP='';
                      string ExercicioSAP='';
			    	   for(string cp : mapTitulos.keySet())
                       {
                          if(cp=='BUKRS')
                            EmpresaSAP=(string) mapTitulos.get(cp);
                          if(cp=='BELNR')
                            CodigoSAP=(string)mapTitulos.get(cp);
                          if(cp=='GJAHR')
                             ExercicioSAP=(string)mapTitulos.get(cp);
                       }
                    List<Titulos__c> titus=[select id from Titulos__c where EmpresaSAP__c=: EmpresaSAP and CodigoSAP__c =:CodigoSAP and ExercicioSAP__c=:ExercicioSAP limit 1];
                    System.debug(titus);  
                    titus[0].firstSap__c=true;
                    update titus[0];
                     }catch(Exception e)
                     {
                        System.debug('erro ao setar firstSap__c '+e.getMessage());   
                     }
                    processJsonBody( queueId, res.getBody() );							               					          
	            }
	            else
	                QueueBO.getInstance().updateQueue( queueId, 'FrenteCaixa / ' + res.getStatusCode() + ' / ' + res.getStatus() );
	        } catch ( CalloutException ex ) {
	            QueueBO.getInstance().updateQueue( queueId, 'FrenteCaixa / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
	        }
        } else {
        	QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }	
    }


    /*
        Processa o retorno do serviço
        @param queueId Id da fila que sera atualizada
        @param jsonBody JSON de retorno do serviço
    */
    private static void processJsonBody( String queueId, String jsonBody ) {
        Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( jsonBody );
          
        String errorMessage = '';
        if(postData.get( 'OutBoundSapResult' ) == null ) {
            errorMessage =  jsonBody;
        } else {
            for( Object result : ( List<Object> )postData.get( 'OutBoundSapResult' ) ) {
                Map<String, Object> resultSuccessData = ( Map<String, Object> ) result;
      
                if(!String.valueOf( resultSuccessData.get('tIPOField')).equals('S') ){
                    errorMessage = String.valueOf(resultSuccessData.get( 'mSGField' ));    
                }
            } 
        }
        QueueBO.getInstance().updateQueue( queueId, errorMessage );
    }

    /*
        Cria o JSON do titulo criado no Salesforce 
    */
    public static String createTitlesToJson( Titulos__c titulo ) {
        date myDate = date.today();
    String dataAtual = String.valueOf(myDate.year());
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        if(titulo.EmpresaSAP__c != null)
            gen.writeStringField( 'BUKRS', titulo.EmpresaSAP__c );
        else
            gen.writeStringField( 'BUKRS', ' ' );
        
        if(titulo.CodigoSAP__c != null){
            gen.writeStringField( 'BELNR', titulo.CodigoSAP__c );
        }
        else{
            gen.writeStringField( 'BELNR', ' ' );
        }
        if(titulo.EmpresaSAP__c != null){
            gen.writeStringField( 'GJAHR', titulo.ExercicioSAP__c );
        }
        else{
            gen.writeStringField( 'GJAHR', '2017');
        }

        gen.writeStringField( 'BUZEI', '001' );
        gen.writeFieldName('_DadosCartao');
        gen.writeStartObject();
        //gen.writeStringField( 'PARCELAS', '1' );
        gen.writeStringField( 'PARCELAS', String.valueOf(titulo.Parcelado__c) );
        gen.writeStringField( 'TID', titulo.CodigoAutorizacao__c );
        gen.writeStringField( 'CHV_BANDEIRA', titulo.Bandeira__c );
        gen.writeStringField( 'CHV_FORMA', titulo.Forma__c );
        gen.writeEndObject();  
        gen.writeEndObject(); 

    	return gen.getAsString(); 
    }
}