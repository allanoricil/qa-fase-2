/* 
    @author Diego Moreira
    @class Classe DAO do objeto Account
*/
public with sharing class CodigoPromocionalDAO extends SObjectDAO {
    /*
        Singleton
    */
    private static final CodigoPromocionalDAO instance = new CodigoPromocionalDAO();    
    private CodigoPromocionalDAO(){}
    
    public static CodigoPromocionalDAO getInstance() {
        return instance; 
    }

    /*
        Busca o codigo promocional pelo codigo
        @param codigoPromocional codigo para filtro
    */
    public List<CodigoPromocional__c> getCodigoPromocionalByCodigo( String codigoPromocional ) {
        return [SELECT Id, Name, TipoDesconto__c, Tipo_de_C_digo__c
                FROM CodigoPromocional__c
                WHERE Name = :codigoPromocional
                AND Expira__c >= :Date.today()];
    }
    
     public CodigoPromocional__c getFirstCodigoPromocionalByCodigo( String codigoPromocional ) {
        List<CodigoPromocional__c> result = this.getCodigoPromocionalByCodigo(codigoPromocional);

        if (result.isEmpty() || result.size() > 1)
            return null;

        return result[0];
    }
    
}