public class Evolucional_Simulado {

    public static void processingQueue( String queueId, String payload ) {
        syncToServer( queueId, payload );         
    }

    
    @future(Callout = true)
    public Static void syncToServer(String queueId, String payload){

        HttpRequest req = new HttpRequest();

        req.setEndpoint('http://api.evolucional.com.br/simulado-online/usuario/listar-simulados');

        req.setMethod( 'POST' );

        req.setHeader( 'Content-Type', 'application/json' );

        req.setTimeout( 120000 );

        req.setBody( payload );

        try {
            Http h = new Http();
            HttpResponse res = h.send( req );
            if( res.getStatusCode() == 200 )
            processJsonBody( queueId, res.getBody() );                                                                                        
            else
            QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + res.getStatusCode() + ' / ' + res.getStatus() );
            } catch ( CalloutException ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
        }

        private static void processJsonBody( String queueId, String jsonBody) {
            system.debug(jsonBody);

            String errorMessage = '';

            Queue__c fila = [Select Oportunidade__c  From Queue__c where Id =: queueId limit 1];

            Opportunity opp = [SELECT Id, Processo_Seletivo__r.Nota_M_nima__c FROM Opportunity where Id =: fila.Oportunidade__c limit 1];

            Oportunidade_EAD__c ead = [Select Id, Bolsa__c from Oportunidade_EAD__c where Opportunity__c =: opp.Id limit 1];

            Map<String, Object> m =(Map<String, Object>) JSON.deserializeUntyped(jsonBody);


            List<Object> a =(List<Object>)m.get('AssessmentList');

            Map<String, Object> assment1 = (Map<String, Object>)a[0];
            Map<String, Object> assment2 = (Map<String, Object>)a[1];
            Map<String, Object> assment3 = (Map<String, Object>)a[2];

            List<Object> evalutions1 = (List<Object>)assment1.get('Evaluations');
            List<Object> evalutions2 = (List<Object>)assment2.get('Evaluations');
            List<Object> evalutions3 = (List<Object>)assment3.get('Evaluations');
            

            Map<String, Object> evalutionsmap1 = (Map<String, Object>)evalutions1[0];
            List<Object> areas1 = (List<Object>)evalutionsmap1.get('Areas');
            Map<String, Object> nota1area1 = (Map<String, Object>)areas1[0];
            Map<String, Object> nota2area1 = (Map<String, Object>)areas1[1];

            Map<String, Object> evalutionsmap2 = (Map<String, Object>)evalutions2[0];
            List<Object> areas2 = (List<Object>)evalutionsmap2.get('Areas');
            Map<String, Object> nota1area2 = (Map<String, Object>)areas2[0];
            Map<String, Object> nota2area2 = (Map<String, Object>)areas2[1];

            Map<String, Object> evalutionsmap3 = (Map<String, Object>)evalutions3[0];
            List<Object> areas3 = (List<Object>)evalutionsmap3.get('Areas');
            Map<String, Object> nota1area3 = (Map<String, Object>)areas3[0];
            Map<String, Object> nota2area3 = (Map<String, Object>)areas3[1];
  
            Integer notaLC1, notaLC2, notaLC3;
            Integer notaMT1, notaMT2, notaMT3;
            Integer notaFinal,nota1,nota2,nota3;

        


            notaLC1 = nota1area1.get('Value') != null ? Integer.valueOf(nota1area1.get('Value')) : 0;

            notaMT1 = nota2area1.get('Value') != null ? Integer.valueOf(nota2area1.get('Value')) : 0;

            notaLC2 = nota1area2.get('Value') != null ? Integer.valueOf(nota1area2.get('Value')) : 0;

            notaMT2 = nota2area2.get('Value') != null ? Integer.valueOf(nota2area2.get('Value')) : 0;

            notaLC3 = nota1area3.get('Value') != null ? Integer.valueOf(nota1area3.get('Value')) : 0;

            notaMT3 = nota2area3.get('Value') != null ? Integer.valueOf(nota2area3.get('Value')) : 0;



                //Medias
            nota1 = (notaLC1 + notaMT1)/2;
            nota2 = (notaLC2 + notaMT2)/2;
            nota3 = (notaLC3 + notaMT3)/2;       
                

                //Nota final
            notaFinal = returnHighest(nota1, nota2, nota3);
        
            

    

            if(notaFinal  >=  opp.Processo_Seletivo__r.Nota_M_nima__c){
                if(ead.Bolsa__c != null && ead.Bolsa__c != '')
                	ead.StageName__c = 'Aprovado';
               	else
                    ead.StageName__c = 'Inscrito';
                List<String> idOpp = new List<String>();
                idOpp.add(opp.id);
                createQueue(idOpp);
                    }else{
                        
                    	if(ead.Bolsa__c != null && ead.Bolsa__c != '')
                			ead.StageName__c = 'Reprovado';
               			else
                            ead.StageName__c = 'Inscrito';
                    }

                    if(notaFinal == 0)
                        ead.StageName__c = 'Inscrito';
    
                    ead.Nota_1__c = nota1;
                    ead.Nota_2__c = nota2;
                    ead.Nota_3__c = nota3;
                    ead.Nota__c = notaFinal;
                    update ead;
                    

                QueueBO.getInstance().updateQueue( queueId, errorMessage ); 

            }

            public static Integer returnHighest(Integer n1, Integer n2, Integer n3){
                Integer nota;
                if (n1 >= n2) 
                    nota  = n1;
                else
                    nota  = n2;

                if (nota <= n3)

                    nota = n3;

                return nota;
            }


            public static void createQueue(List<String> opportunity){
                try {
                    Opportunity ac = [SELECT Processo_seletivo__r.C_digo_da_Coligada__c,Processo_seletivo__r.C_digo_Processo_Seletivo__c,
                    Numeroinscrprocsel__c, Processo_seletivo__r.Sequencial_Processo_Seletivo__c,Account.Name,Account.Sexo__c,Data_Nascimento__c,
                    Account.Nacionalidade__c,Estado_Civil__c,Account.Nome_do_Pai__c,Account.Nome_da_Mae__c,Account.RG__c,
                    Account.CPF_2__c,Account.Rua__c,Account.N_mero__c,Account.Bairro__c,Account.Cidade__c,Account.Estado__c,Account.CEP__c,
                    Account.Phone,Account.Complemento__c,Account.PersonEmail,Processo_seletivo__r.IdPeriodoLetivo__c,Nota__c,
                    ClassificaProcessoSeletivo__c, X1_OpcaoCurso__r.C_digo_da_Oferta__c ,Polo__r.Name, Processo_seletivo__r.ID_Academus_ProcSel__c, Polo__r.CodPolo__c
                    FROM opportunity WHERE id =: opportunity[0]];

                    Queue__c queue = new Queue__c();




                // inicio
                queue.EventName__c          = 'APROVA_CANDIDATO_2';
                queue.Payload__c            = getJsonRequest(ac);
                system.debug('Xoxanna '+ queue.Payload__c);
                queue.Status__c             = 'CREATED';
                queue.ProcessBySchedule__c  = false;
                //insert queue;
                // fim
                ProcessorControl.inFutureContext = true;
                QueueDAO.getInstance().insertData( queue );
                } catch (Exception e) {

                } 

            }

            public static String getJsonRequest( Opportunity opportunity ){

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField('polo', opportunity.Polo__r.Name != null ? opportunity.Polo__r.CodPolo__c+'-'+opportunity.Polo__r.Name : '');
        gen.writeStringField('IdProcessoACD', opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c != null ? opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c : '');
        gen.writeFieldName('CandidatoProcSel');
        gen.writeStartObject();

        gen.writeNumberField( 'CODCOLIGADA', 9 );
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        //gen.writeStringField( 'NUMEROINSCPROCSEL',opportunity.CodigoInscricao__c != null ? opportunity.CodigoInscricao__c : '' );
        //gen.writeStringField( 'NUMEROINSCPROCSEL', lastOpp.size() > 0 ? String.valueof(Integer.valueof(lastOpp[0].CodigoInscricao__c)+1) : '' );
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c));
        gen.writeStringField( 'NOME',opportunity.Account.Name.toUpperCase() ); 
        gen.writeStringField( 'SEXO',opportunity.Account.Sexo__c != null ? opportunity.Account.Sexo__c.substring(0,1).toUpperCase() : '' );
        if(opportunity.Data_Nascimento__c == null){
            gen.writeNullField('DATANASC');
        }
        else{
            gen.writeStringField( 'DATANASC',opportunity.Data_Nascimento__c != null ? String.valueOf( opportunity.Data_Nascimento__c) + ' 16:00:00.200' : '' ); 
        }
        gen.writeStringField( 'CIDADENASC',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '');
        gen.writeStringField( 'UFNASC',opportunity.Account.Estado__c != null ? opportunity.Account.Estado__c : '' );      
        gen.writeStringField( 'NACIONALIDADE',opportunity.Account.Nacionalidade__c != null ? opportunity.Account.Nacionalidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADOCIVIL',opportunity.Estado_Civil__c != null ? opportunity.Estado_Civil__c.toUpperCase() : '' );
        gen.writeStringField( 'PAI',opportunity.Account.Nome_do_Pai__c != null ? opportunity.Account.Nome_do_Pai__c.toUpperCase() : '');
        gen.writeStringField( 'MAE',opportunity.Account.Nome_da_Mae__c != null ? opportunity.Account.Nome_da_Mae__c.toUpperCase() : '');
        gen.writeStringField( 'CPFFIADOR','' );
        gen.writeStringField( 'IDENT',opportunity.Account.RG__c !=null ? opportunity.Account.RG__c.replace( '.', '').replace( '-', '' ) : '' );
        gen.writeStringField( 'ORGAOEMISSOR','' );
        gen.writeStringField( 'CPFALUNO',opportunity.Account.CPF_2__c.replace( '.', '').replace( '-', '' )  ); 
        gen.writeStringField( 'TIPOCURSOORIGEM','' );
        gen.writeStringField( 'LOGRADOURO',opportunity.Account.Rua__c != null ? opportunity.Account.Rua__c.toUpperCase() : '' ); 
        gen.writeStringField( 'NUMEROIMOVEL',opportunity.Account.N_mero__c != null ? opportunity.Account.N_mero__c : '' ); 
        gen.writeStringField( 'BAIRRO',opportunity.Account.Bairro__c != null ? opportunity.Account.Bairro__c.toUpperCase() : '' ); 
        gen.writeStringField( 'CIDADE',opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c.toUpperCase() : '' ); 
        gen.writeStringField( 'ESTADO',opportunity.Account.Estado__c != null ? opportunity.Account.Estado__c : '' ); 
        gen.writeStringField( 'CEP',opportunity.Account.CEP__c != null ? opportunity.Account.CEP__c : '' ); 
        gen.writeStringField( 'TELEFONE',opportunity.Account.Phone != null ? opportunity.Account.Phone : '' ); 
        gen.writeStringField( 'COMPLEMENTO',opportunity.Account.Complemento__c != null ? opportunity.Account.Complemento__c.toUpperCase() : '' ); 
        gen.writeStringField( 'EMAIL',opportunity.Account.PersonEmail != null ? opportunity.Account.PersonEmail.toUpperCase() : '' ); 
        gen.writeNullField  ( 'DATANASCPAI'); 
        gen.writeStringField( 'CIDADENASCPAI','' ); 
        gen.writeStringField( 'UFNASCPAI','' ); 
        gen.writeNullField('DATANASCMAE');
        gen.writeStringField( 'CIDADENASCMAE','' ); 
        gen.writeStringField( 'UFNASCMAE','' ); 
        gen.writeStringField( 'CPFMAE','' );
        gen.writeEndObject();
        gen.writeFieldName('OpcaoCandidato');
        gen.writeStartObject();
        
        //gen.writeNumberField( 'CODCOLIGADA',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_da_Coligada__c ));
        gen.writeNumberField( 'CODCOLIGADA', 9 );
        //gen.writeNumberField( 'IDPERLET',Integer.ValueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) != null ? Integer.valueOf(opportunity.Processo_seletivo__r.IdPeriodoLetivo__c) : 0 );
        gen.writeNumberField( 'IDPERLET', 2061 );  
        gen.writeNumberField( 'IDPROCSEL',Integer.ValueOf(opportunity.Processo_seletivo__r.C_digo_Processo_Seletivo__c));
        gen.writeStringField( 'NUMEROINSCPROCSEL', String.valueOf(opportunity.Numeroinscrprocsel__c) );
        gen.writeStringField( 'CODCURSO', opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c != null ?  opportunity.X1_OpcaoCurso__r.C_digo_da_Oferta__c : '');
        gen.writeNumberField( 'OPCAO', 1 );
        gen.writeNumberField( 'PONTUACAO',opportunity.Nota__c != null ? opportunity.Nota__c : 0 );
        gen.writeNumberField( 'CLASSIFICACAO', opportunity.ClassificaProcessoSeletivo__c != null ? Integer.valueOf(opportunity.ClassificaProcessoSeletivo__c) : 0 ); 
        gen.writeNumberField( 'IDHABILITACAOFILIAL', 3 ); 
        gen.writeEndObject();
        





    
    gen.writeEndObject(); 
        
        return gen.getAsString(); 
    }
    
    public static String getUF(String estado){
        estado = estado.toUpperCase();
        String retorno = '';
        if(estado =='ACRE')retorno = 'AC';
        if(estado =='ALAGOAS')retorno = 'AL';
        if(estado =='AMAPÁ')retorno = 'AP';
        if(estado =='AMAZONAS')retorno = 'AM';
        if(estado =='BAHIA')retorno = 'BA';
        if(estado =='CEARÁ')retorno = 'CE';
        if(estado =='DISTRITO FEDERAL')retorno = 'DF';
        if(estado =='ESPÍRITO SANTO')retorno = 'ES';
        if(estado =='GOIÁS')retorno = 'GO';
        if(estado =='MARANHÃO')retorno = 'MA';
        if(estado =='MATO GROSSO')retorno = 'MT';
        if(estado =='MATO GROSSO DO SUL')retorno = 'MS';  
        if(estado =='MINAS GERAIS')retorno = 'MG';
        if(estado =='PARÁ')retorno = 'PA';
        if(estado =='PARAÍBA')retorno = 'PB';
        if(estado =='PARANÁ')retorno = 'PR';
        if(estado =='PERNAMBUCO')retorno = 'PE';
        if(estado =='PIAUÍ')retorno = 'PI';
        if(estado =='RIO DE JANEIRO')retorno = 'RJ';
        if(estado =='RIO GRANDE DO NORTE')retorno = 'RN';
        if(estado =='RIO GRANDE DO SUL')retorno = 'RS';
        if(estado =='RONDÔNIA')retorno = 'RO';
        if(estado =='RORAIMA')retorno = 'RR';
        if(estado =='SANTA CATARINA')retorno = 'SC';
        if(estado =='SÃO PAULO')retorno = 'SP';
        if(estado =='SERGIPE')retorno = 'SE';
        if(estado =='TOCANTINS')retorno = 'TO';

        return retorno;
    }

}