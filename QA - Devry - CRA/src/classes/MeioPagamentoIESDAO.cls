/*
    @author Diego Moreira
    @class Classe DAO do objeto Meios de pagamento(MeioPagamentoIES__c)
*/
public with sharing class MeioPagamentoIESDAO extends SObjectDAO {
	/*
        Singleton
    */
    private static final MeioPagamentoIESDAO instance = new MeioPagamentoIESDAO();    
    private MeioPagamentoIESDAO(){}
    
    public static MeioPagamentoIESDAO getInstance() {
        return instance; 
    }

    /*
		Busca os meios de pagamento da instituição
		@param instituicaoId Id da instituição do aluno
    */
    public List<MeioPagamentoIES__c> getMeioPagamentoByInstituicaoId( String instituicaoId ) {
    	return [SELECT Id, Instituicao__c, MeioPagamentoBraspag__c, MeioPagamentoBraspag__r.Name, 
                MeioPagamentoBraspag__r.Codigo__c, MeioPagamentoBraspag__r.TipoMeioPagamento__c,
                MeioPagamentoBraspag__r.Bandeira__c, MeioPagamentoBraspag__r.Chave_Bandeira__c,
                MeioPagamentoBraspag__r.Forma__c
    				FROM MeioPagamentoIES__c
    				WHERE Instituicao__c = :instituicaoId];
    }
}