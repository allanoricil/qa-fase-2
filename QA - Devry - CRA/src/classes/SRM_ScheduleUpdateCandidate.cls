/*
    @author Diego Moreira
    @class Schedule de processamento fila de integração Academus
*/
global class SRM_ScheduleUpdateCandidate implements Schedulable {
	
	global void execute(SchedulableContext sc) { 

		for( Queue__c queue : QueueDAO.getInstance().getQueueByEventNameAndStatus( QueueEventNames.UPDATE_CANDIDATE.name(), 'CREATED', 50 ) ) {
			SRM_AtualizaCandidatoBO.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
		}
		for( Queue__c queue : QueueDAO.getInstance().getQueueByEventNameAndStatus( QueueEventNames.APROVA_CANDIDATO.name(), 'CREATED', 50 ) ) {
			SRM_AtualizaCandidatoBO.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
		}
	}
}