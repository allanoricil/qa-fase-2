/*
    @author Diego Moreira
    @class Classe de pesquisa de CEP
*/
public class SRM_BuscaCEP {
	/*
        Singleton 
    */
    private static final SRM_BuscaCEP instance = new SRM_BuscaCEP();    
    private SRM_BuscaCEP(){}
    
    public static SRM_BuscaCEP getInstance() {
        return instance;
    }

    /*
		Busca os dados do CEP no serviço
		@param cepNumber numero do CEP de pesquisa
    */
    public Map<String, Object> getCepByNumber( String cepNumber ) {
    	HttpRequest req = new HttpRequest();
		req.setEndpoint( 'http://api.postmon.com.br/v1/cep/' + cepNumber );
		req.setMethod('GET');
		req.setTimeout( 20000 );

		try {
			Http h = new Http();
			HttpResponse res = h.send( req );
			
			if( res.getStatusCode() == 404 ) return null;
			return ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
		} catch( CalloutException ex ) {
			return null;
		}		
    }
}