@isTest
public class CRA_CaseItemConsoleControllerTest {
	public static testMethod void CRA_CaseItemConsoleControllerTest() {
		Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        //Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Contact Center'];
        //Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        //User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        //insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        List<Assunto__c> allAssuntos = new List<Assunto__c>();
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Tipo_Disciplinas__c = 'CURSANDO';
        assunto.Pagamento__c = false;
        assunto.Anexo__c = 'Sem anexo';
        assunto.Contact_Center__c = true;
        //insert assunto;

        Assunto__c assunto2 = CRA_DataFactoryTest.newAssunto(produto);
        assunto2.Name = 'Inclusão de Disciplina não Ofertada em Semestre';
		assunto2.Portal__c = true;
		assunto2.ID_externo__c = 'Estudo Dirigido';
		assunto2.Graduacao__c = true;
		assunto2.Servico__c = produto.Id;
		//assunto2.Tipo_de_Registro_do_caso__c = 'CRA - Padrão';
		assunto2.SLA__c = direito.Id;
		assunto2.Restringe_duplicidade__c = true;
		assunto2.Pagamento__c = false;
		assunto2.Anexo__c = 'Sem anexo';
		assunto2.Contact_Center__c = true;
        //insert assunto2;

        Assunto__c assunto3 = new Assunto__c(
        	Name = 'Assinatura do Termo de Estágio',
            Portal__c = true,
            ID_externo__c = 'Termo de Compromisso de Estágio',
            Graduacao__c = true,
            Servico__c = produto.Id,
            //Tipo_de_Registro_do_caso__c = 'CRA - Compromisso de Estágio',
            Tipo_de_Registro_do_caso__c = 'CRA - Padrão',
            SLA__c = direito.Id,
	        Restringe_duplicidade__c = true,
	        Pagamento__c = false,
	        Anexo__c = 'Sem anexo',
	        Contact_Center__c = true
        );

        Assunto__c assunto4 = new Assunto__c(
        	Name = 'Alteração de Turma EnglishPro',        	
            Portal__c = true,
            ID_externo__c = 'Mudança de Turma (EnglishPro)',
            Graduacao__c = true,
            Servico__c = produto.Id,
            //Tipo_de_Registro_do_caso__c = 'CRA - Compromisso de Estágio',
            Tipo_de_Registro_do_caso__c = 'CRA - Turma - EnglishPro',
            //Tipo_de_Registro_do_caso__c = 'CRA - Padrão',
            SLA__c = direito.Id,
	        Restringe_duplicidade__c = true,
	        Pagamento__c = false,
	        Anexo__c = 'Sem anexo',
	        Contact_Center__c = true
        );

        allAssuntos.add(assunto);
        allAssuntos.add(assunto2);
        allAssuntos.add(assunto3);
        allAssuntos.add(assunto4);

        system.debug('antes dos assuntos');
        insert allAssuntos;
        system.debug('depois dos assuntos');
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;

        List<Case> allCases = new List<Case>();
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Assunto__c = assunto.Id;
        caso.Status = 'Novo';
        caso.Description = 'Testanto classe teste';
        //insert caso;

        Case caso2 = CRA_DataFactoryTest.newCase(aluno);
        caso2.Assunto__c = assunto2.Id;
        caso2.Status = 'Novo';
        caso2.Description = 'Testanto classe teste';
        //insert caso2;

        Case caso3 = CRA_DataFactoryTest.newCase(aluno);
        caso3.Assunto__c = assunto3.Id;
        caso3.Status = 'Novo';
        caso3.Description = 'Testanto classe teste';

        Case caso4 = CRA_DataFactoryTest.newCase(aluno);
        caso4.Assunto__c = assunto4.Id;
        caso4.Status = 'Novo';
        caso4.Description = 'Testanto classe teste';

        allCases.add(caso);
        allCases.add(caso2);
        allCases.add(caso3);
        allCases.add(caso4);
        system.debug('antes dos casos');
        insert allCases;
        system.debug('depois dos casos');

        Set<Id> casoIds = new Set<Id>();
        casoIds.add(caso.Id);
        casoIds.add(caso2.Id);
        casoIds.add(caso3.Id);
        casoIds.add(caso4.Id);

        List<Case> myCase = [SELECT Id,Status,
        						Assunto__c,Assunto__r.Tipo_Disciplinas__c,
        						Assunto__r.Name,Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c,
        						Aluno__r.Campus_n__r.C_digo_do_Campus__c,
        						Aluno__r.R_A_do_Aluno__c,Assunto__r.Tipo_de_Registro_do_Caso__c 
        						FROM Case WHERE Id =: casoIds LIMIT 4];

		system.debug('myCase: '+myCase);

        Profile perfil2 = [SELECT Id FROM Profile WHERE Name = 'CRA - Contact Center'];
        User usuario3 = new User(
            LastName = 'Contac Center',
            Alias = 'ccenter',
            Username = 'ccenter@adtalem.com.br.qa',
            Email = 'contact.center@harpiacloud.com.br.qa',
            ProfileId = perfil2.Id,
            DBNumber__c = '13243238',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR'
        );
        insert usuario3;

        insert new WSSetup__c(Endpoint__c = 'login', Name ='Login');
        insert new WSSetup__c(Endpoint__c = 'disciplina', Name ='HistoricoDisciplinas');
        insert new WSSetup__c(Endpoint__c = 'token', Name ='Token Test');
        insert new WSSetup__c(Endpoint__c = 'english', Name ='CraTurmasEPROList');
        

        //Test.setMock(HttpCalloutMock.class, new CRA_LoginMockTest());
        Test.setMock(HttpCalloutMock.class, new CRA_SubjectsQueryMockTest());

        //system.debug('tipo disciplina: '+myCase.Assunto__r.Tipo_Disciplinas__c);

        Test.startTest();

        system.runAs(usuario3){
           	ApexPages.StandardController sc = new ApexPages.StandardController(myCase[0]); 
	        PageReference pageRef = Page.CRA_CaseItemConsole ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseItemConsoleController controller = new CRA_CaseItemConsoleController(sc);
	        system.debug('debug controller: '+controller.myCase.Assunto__r.Name);
	        if(myCase[0].Id == caso.Id){
    	        controller.SubjectWrapperList[0].Selected = true;
    	        controller.selectSubjects();
    	    }
        }
        system.runAs(usuario3){
           	ApexPages.StandardController sc = new ApexPages.StandardController(myCase[1]); 
	        PageReference pageRef = Page.CRA_CaseItemConsole ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseItemConsoleController controller = new CRA_CaseItemConsoleController(sc);
	        system.debug('debug controller2: '+controller.myCase.Assunto__r.Name);
	        if(myCase[1].Id == caso.Id){
    	        controller.SubjectWrapperList[0].Selected = true;
    	        controller.selectSubjects();
    	    }
        }
        system.runAs(usuario3){
           	ApexPages.StandardController sc = new ApexPages.StandardController(myCase[2]); 
	        PageReference pageRef = Page.CRA_CaseItemConsole ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseItemConsoleController controller = new CRA_CaseItemConsoleController(sc);
	        system.debug('debug controller3: '+controller.myCase.Assunto__r.Name);
	        if(myCase[2].Id == caso.Id){
    	        controller.SubjectWrapperList[0].Selected = true;
    	        controller.selectSubjects();
    	    }
        }
		system.runAs(usuario3){
           	ApexPages.StandardController sc = new ApexPages.StandardController(myCase[3]); 
	        PageReference pageRef = Page.CRA_CaseItemConsole ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseItemConsoleController controller = new CRA_CaseItemConsoleController(sc);
	        system.debug('debug controller3: '+controller.myCase.Assunto__r.Name);
	        if(myCase[3].Id == caso.Id){
    	        controller.SubjectWrapperList[0].Selected = true;
    	        controller.selectSubjects();
    	    }
        }

        Test.stopTest();
	}
}