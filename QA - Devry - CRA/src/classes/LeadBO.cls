/*
    @author Diego Moreira
    @class Classe de negocio do objeto Lead
*/
public with sharing class LeadBO {
    /*
        Singleton
    */
    private static final LeadBO instance = new LeadBO();    
    private LeadBO(){}
    
    public static LeadBO getInstance() {
        return instance;
    } 
 
    /*
        Metodo principal para conversão de candidatos
    */
    public void convertWithOriginCandidate() {
        List<Account> accounts = AccountDAO.getInstance().getAllAccountsWithLeadOrigin();
        System.debug('>>> ' + accounts);
        updateLeadOrigin( accounts );

        for( Account account : accounts )
            doConvertWithAccount( account.LeadOrigem__c, account.Id );          
    }
 
    /*
        Atualiza os dados do Lead
    */
    private void updateLeadOrigin( List<Account> accounts ) {
        List<Lead> leadsToUpdate = new List<Lead>();

        for( Account account : accounts ) {
            leadsToUpdate.add( new Lead(
                Id                                          = account.LeadOrigem__c, 
                FirstName                                   = account.FirstName,
                LastName                                    = account.LastName,
                Nacionalidade__c                            = account.Nacionalidade__c,
                RG__c                                       = account.RG__c,
                CPF__c                                      = account.CPF_2__c,
                Passaporte__c                               = account.Passaporte__c,
                VoceEstrangeiro__c                          = account.VoceEstrangeiro__c,
                VoceTreineiro__c                            = account.VoceTreineiro__c,
                C_digo_Especial__c                          = account.Codigo_Especial__c, 
                Sexo__c                                     = account.Sexo__c, 
                Phone                                       = account.Phone,
                MobilePhone                                 = account.PersonMobilePhone, 
                Email                                       = account.PersonEmail, 
                Data_de_Nascimento__c                       = account.DataNascimento__c, 
                Rua__c                                      = account.Rua__c,
                N_mero__c                                   = account.N_mero__c,
                Complemento__c                              = account.Complemento__c,
                Bairro__c                                   = account.Bairro__c,
                CEP__c                                      = account.CEP__c,
                Cidade__c                                   = account.Cidade__c,
                Estado__c                                   = account.Estado__c,
                OutroColegio__c                             = account.OutroColegio__c, 
                Naturalidade__c                             = account.Naturalidade__c,
                NomeCompletoResponsavel__c                  = account.NomeCompletoResponsavel__c,  
                CPFdoResponsavel__c                         = account.CPFdoResponsavel__c, 
                Tipo_de_Escola_cursa_cursou_ensino_medio__c = account.Tipo_escola_cursa_ou_cursou_ensino_medio__c, 
                portador_de_necessidades_especiais__c       = account.E_portador_de_alguma_aten_o_especial__c,
                X1a_Op_o_de_Curso__c                        = account.Z1_opcao_curso__c
                ) );    
        }
        update leadsToUpdate;        
    } 

    /*
        Metodo de conversão do Lead
    */
    private void doConvertWithAccount( String leadId, String accountId ) {
        Database.LeadConvert leadToConvert;
        Database.LeadConvertResult leadConvertedResult;
        
        leadToConvert = new database.LeadConvert();     
        leadToConvert.setLeadId( leadId );
        leadToConvert.setAccountId( accountId );
        leadToConvert.setConvertedStatus( 'Pré-inscrito' );    
        leadToConvert.setDoNotCreateOpportunity(true);         

        try {
            leadConvertedResult = Database.convertLead( leadToConvert );
        } catch( Exception ex ) {
            System.debug( '>>> ' + ex.getMessage() );
        }
    }

    /*
        Cria fila de verificação do telefone
    */
    public void createCheckPhoneQueue( List<Lead> leads ) {
        for( Lead lead : leads ) {
            if( lead.Phone != null && lead.RecordTypeId == LeadDAO.RECORDTYPE_CAPTACAO ) {
                String phoneNumber = '0' + lead.Phone;
                phoneNumber = phoneNumber.replace('(', '').replace( ')', '' ).replace( '-', '' );
                phoneNumber = phoneNumber.deleteWhitespace();
                    
                String jsonResult = SRM_ValidaNumeroTelefoneBO.getJsonRequest( lead.Id, phoneNumber );
                System.debug('>>> ' + jsonResult);
                QueueBO.getInstance().createQueue( QueueEventNames.ASTERIX_VALIDATE_PHONE_NUMBER.name(), jsonResult );
            }
        }
    }

    /*
        Cria fila de verificação do telefone
    */
    public void createCheckPhoneQueue( List<Lead> leads, Map<Id, Lead> oldMapLead ) {
        for( Lead lead : leads ) {
            if( lead.Phone != null && 
                    lead.RecordTypeId == LeadDAO.RECORDTYPE_CAPTACAO &&
                    Lead.Phone != oldMapLead.get( lead.Id ).Phone ) {
                String phoneNumber = '0' + lead.Phone;
                phoneNumber = phoneNumber.replace('(', '').replace( ')', '' ).replace( '-', '' );
                phoneNumber = phoneNumber.deleteWhitespace();

                String jsonResult = SRM_ValidaNumeroTelefoneBO.getJsonRequest( lead.Id, phoneNumber );
                System.debug('>>> ' + jsonResult);
                QueueBO.getInstance().createQueue( QueueEventNames.ASTERIX_VALIDATE_PHONE_NUMBER.name(), jsonResult );
            }
        }
    }

    /*  
        Atualiza o lookup de instituição de ensino via web-to-lead
        @param leads lista de leads criados
    */
    public void autalizaInstituicao( List<Lead> leads ) {
        List<String> instituicaoNameList = new List<String>();  
        Map<String, Institui_o__c> mapInstituicao = new Map<String, Institui_o__c>();

        for( Lead lead : leads ) {
            if( lead.IES_de_interesse__c != null )
                instituicaoNameList.add( lead.IES_de_interesse__c );
        }
        
        for( Institui_o__c instituicao : InstituicaoDAO.getInstance().getInstituicaoByName( instituicaoNameList ) )
            mapInstituicao.put( instituicao.Lead_Map__c, instituicao );
        
        for( Lead lead : leads ) {
            if( lead.IES_de_interesse__c != null && !mapInstituicao.isEmpty() && lead.Instituicao__c == null) {
                lead.Instituicao__c = mapInstituicao.get( lead.IES_de_interesse__c ).Id;
            }
        }
    }

    /*
        Atribui o lead na criação a regra de atribuição
    */
    public void atribuiRegraAtiva( List<Lead> leads ) {
        List<Lead> leadToUpdate = new List<Lead>();
        Database.DMLOptions options = new Database.DMLOptions();
        options.assignmentRuleHeader.useDefaultRule= true;

        for( Lead lead : leads ) {
            Lead lead2 = new Lead();
            lead2.Id = lead.Id;
            lead2.setOptions( options );

            leadToUpdate.add( lead2 );
        }
        LeadDAO.getInstance().updateData( leadToUpdate );
    }

}