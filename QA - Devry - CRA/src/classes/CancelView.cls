/**
 * Author: Willian Matheus 
 * Date: 01/02/2017
 * email: wbatista@moldsoft.com.br
 */
public with sharing class CancelView {

    public ApexPages.StandardController controllerobj;

    public List < SelectOption > statusItens {get;set;}
    public String statusSelecionado {get;set;}
    public Boolean rnewCancel {get;set;}

    public List < Cancelamento__c > cancs {get;set;}
    public string tipoOperacaoaberta {get;set;}
    public Boolean emAtendimento {get;set;}
    public Boolean podeAtender {get;set;}
    public Boolean podeCriar {get;set;}
    public Boolean semOportunidade {get;set;}
    public Boolean semOferta {get;set;}

    public Boolean emNAAF {get;set;}
    public Boolean emFaturamento {get;set;}
    public Boolean emCasa {get;set;}
    public Boolean emCoordenacao {get;set;}
    public Boolean emBiblioteca {get;set;}

    public Boolean erroRAWithStatusEmAberto {get;set;}
    public Boolean success {get;set;}
    public Boolean erro {get;set;}
    public Boolean aprovado {get;set;}
    public Boolean erroAprovado {get;set;}
    public Boolean finalizado {get;set;}
    public Boolean errofinalizado {get;set;}

    public Boolean alerta {get;set;}
    public Boolean closeWindow {get;set;}

    public Boolean obrigatorioAnexar {get;set;}

    public string ra {get;set;}
    public Boolean view {
        get {
            if (ApexPages.currentPage().getParameters().get('id') != null) return true;
            else return false;
        }
        set;
    }
    public Cancelamento__c c {get;set;}
    List < CategoryWrapper > lista {get;set;}

    public CancelView(ApexPages.StandardController controller) {
        this.controllerobj = controller;
        this.c = new Cancelamento__c();
        this.rnewCancel = false;
        this.success = false;
        this.erro = false;

        List < string > lista = new List < String > ();
        lista.add('Usu_rio_em_Atendimento__c');
        lista.add('Casa_Admiss_es_Confirma__c');
        lista.add('Coordena_o_Confirma__c');
        lista.add('Biblioteca_Confirma__c');
        lista.add('Usu_rio_em_Atendimento__c');
        lista.add('Faturamento_Aprova__c');
        lista.add('Finalizado__c');
        lista.add('Aluno__r.Name');
        lista.add('Solicita_o__r.CaseNumber');
        lista.add('LastModifiedBy.Name');
        lista.add('LastModifiedBy.Email');
        controller.addFields(lista);

        this.c = (Cancelamento__c) controller.getRecord();
        
    }

    // Instancia o StandardSetController para uma query locator
    public ApexPages.StandardSetController controller {
        get {
            if (controller == null) {
                if (ra != null) {
                    controller = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, StageQueueRM__c,Solicita_o__r.CaseNumber,NAAF_Confirma__c, Usu_rio_em_Atendimento__c, Casa_Admiss_es_Confirma__c, Cod_Operac_o__c, Usu_rio_Delegado_NAAF__c, Name, RA__c, Oportunidade__c, StageQueue__c, Faturamento_Aprova__c, Aluno__c FROM Cancelamento__c WHERE RA__c =: ra Order By CreatedDate asc]));

                } else {

                    controller = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, StageQueueRM__c,Solicita_o__r.CaseNumber,NAAF_Confirma__c, Usu_rio_em_Atendimento__c, Casa_Admiss_es_Confirma__c, Cod_Operac_o__c, Usu_rio_Delegado_NAAF__c, Name, RA__c, Oportunidade__c, StageQueue__c, Faturamento_Aprova__c, Aluno__c FROM Cancelamento__c Order By CreatedDate asc]));
                }
                // Números de itens por página.
                controller.setPageSize(30);
            }
            return controller;
        }
        set;
    }

    // Retorna uma lista de Objetos #Class CategoryWrapper
    public List < CategoryWrapper > getCategories() {
        lista = new List < CategoryWrapper > ();
        for (Cancelamento__c category: (List < Cancelamento__c > ) controller.getRecords())
            lista.add(new CategoryWrapper(category));

        return lista;
    }

    // Retorna verdadeiro se há mais registros depois da página atual.
    public Boolean hasNext {
        get {
            return controller.getHasNext();
        }
        set;
    }

    // Retorna verdadeira se há mais registros antes da página atual.
    public Boolean hasPrevious {
        get {
            return controller.getHasPrevious();
        }
        set;
    }

    // Retorna o número da página
    public Integer pageNumber {
        get {
            return controller.getPageNumber();
        }
        set;
    }
    // aaeeee krl
    public Long pageSize {
        get {
            return Decimal.valueOf(controller.getResultSize()).divide(30, 1).round(System.RoundingMode.CEILING);
            // Decimal - divide - valor - escala - arredonda pro próximo inteiro
        }
        set;
    }

    // Primeira página
    public void first() {
        controller.first();
    }

    // Última Página
    public void last() {
        controller.last();
    }

    // Página Anterior
    public void previous() {
        controller.previous();
    }

    // Próxima Página
    public void next() {
        controller.next();
    }

    public class CategoryWrapper {

        public Cancelamento__c fila {
            get;
            set;
        }

        public CategoryWrapper() {
            fila = new Cancelamento__c();
        }

        public CategoryWrapper(Cancelamento__c q) {
            fila = q;
        }


    }

    public Boolean isAtendimento(string email) {
        if (email == UserInfo.getUserEmail()) {
            return true;
        } else {
            return false;
        }
    }

    public PageReference newCancel() {
        this.rnewcancel = true;
        return null;
    }

    public PageReference cancelar() {
        this.rnewcancel = false;
        return reload();
    }  

    public PageReference reset() {
        this.c = null;
        this.rnewCancel = false;
        this.success = false;
        return reload();
    }

    public PageReference reloadWithRA() {
        consultaCancelamento();
        return null;
    }

    public PageReference reloadWithoutRA() {
        return null;
    }

    public PageReference reload() {
        PageReference p = Page.CancelView;
        p.setRedirect(true);
        return p;

    }

    public void consultaCancelamento() {

        controller = new ApexPages.StandardSetController(Database.getQueryLocator([Select Id, StageQueueRM__c,Solicita_o__r.CaseNumber,NAAF_Confirma__c, Usu_rio_em_Atendimento__c, Casa_Admiss_es_Confirma__c, Cod_Operac_o__c, Usu_rio_Delegado_NAAF__c, Name, RA__c, Oportunidade__c, StageQueue__c, Faturamento_Aprova__c, Aluno__c FROM Cancelamento__c WHERE RA__c =: ra Order By CreatedDate asc]));
        if (controller.getResultSize() == 0) {
            erro = true;
        } else {
            erro = false;
        }
    }

        

}