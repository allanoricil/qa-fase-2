public with sharing class ReagendamentoController {

    private Static final Integer QUANTIDADE_MAX_OPCOES_DE_DATA = 25;
    private Static final Integer QUANTIDADE_MAX_REAGENDAMENTOS = (Integer)ControleDeAgendamentos__c.getAll().values()[0].Maximo__c;

    public Opportunity opp                 {get; private set;}
    public List<SelectOption> horaOptions  {get; set;}
    public List<SelectOption> diaOptions   {get; set;}
    public Agendamento__c age              {get; set;}
    public Boolean excedeReagendamento     {get; set;} 
    public Boolean agendamentoConcluido    {get; set;}   
    public Date    diaAgendado             {get; set;}
    public String  horarioAgendado         {get; set;}
    public String  nome                    {get; set;}
    public String  cpf                     {get; set;}
    public String  polo                    {get; set;}
    public String  idpolo                  {get; set;}
    public String  nInscricao              {get; set;}
    public String  agendId                 {get; set;}
    public string  callfunc                {get; set;}
    public String  generate                {get; set;}
    public String  userAlias               {get; private set;}
    public Id userId {get;set;}
    public PageReference oppReturn{get;set;}
    
    public ReagendamentoController(ApexPages.StandardController controller){
    	opp = (Opportunity)controller.getRecord();
    	opp = [SELECT id, 
                      Account.Name,
                      Account.CPF_2__c, 
                      Polo__c, 
                      Numeroinscrprocsel__c, 
                      Polo__r.Name,
                      Name,
                      StageName, 
                      Numero_de_Agendamentos__c,
                      RecordType.Name,
                      Dia_Prova__c,
                      Hora_Prova__c,
                      Page_Auto_Refresh__c,
                      Data_e_hora_da_prova_agendada__c
                 FROM Opportunity 
                WHERE Id=:opp.Id];

        //Controla a visibilidade do botão "Voltar para Oportunidade"
        //Caso o usuário seja visitante, o bottão não irá aparecer
        //Caso o usuário seja um operador do salesforce,o botão irá aparecer
        userId = UserInfo.getUserId();
        userAlias = [SELECT Id,
                            alias
                       FROM User
                      WHERE Id=:userId
                      LIMIT 1].alias;
        
        setCandidateViewInformation();
        oppReturn = new PageReference('/' + opp.Id);
        agendamentoConcluido = false;
        diaOptions = new List<SelectOption>();
        horaOptions = new List<SelectOption>();
        if(isUserAbleToReschedule()){    
            getAvailableDates();
        }

        //isso aqui tambem deve ser refatorado...
        generate = 'function generate(type, text) {var n = noty({text: text,type: type,dismissQueue: true,progressBar: true,timeout: 3000,layout: \'center\',closeWith: [\'click\'],theme: \'relax\',maxVisible: 10,animation: {open: \'animated bounceInLeft\',close: \'animated bounceOutRight\',easing: \'swing\',speed: 500}}); return n;}';    
    }

    //isso aqui tambem deve ser refatorado...
    public void calljavascript_cls(){
        callfunc='<script> '+ generate +' generate(\'error\', \'<div class="activity-item" style="text-align: left"><div class="activity">Não há vagas Disponíveis.</div></div>\');</script>';
    }
   
    public void getAvailableDates(){
        List<Agendamento__c> agendamentos = fetchAvailableScheduleDates();
        for(Agendamento__c agendamento : agendamentos){
            Date diaDoAgendamento  = agendamento.Dia__c;
            String diaDoAgendamentoText = diaDoAgendamento.format();
            if(diaDoAgendamento >= Date.TODAY() && 
               diaOptions.size() < QUANTIDADE_MAX_OPCOES_DE_DATA){
           		 diaOptions.add(new SelectOption(diaDoAgendamentoText,diaDoAgendamentoText));
            }
        }
    }

    public Boolean getIsThereAvailableScheduledDate(){
        return diaOptions.size() > 0;
    }

    public void getListAvailableScheduleTimes(){
        if(diaAgendado != null){
          horaOptions = new List<SelectOption>();
        	List<Agendamento__c> agendamentos = fetchAvailableScheduleTimes(diaAgendado);
          for(Agendamento__c  agendamento : agendamentos){
            List<String> horas = agendamento.Hor_rio__c.split(';');
            List<String> datasEHorariosDisponiveis = descobreOsHorariosEDiasQueAindaEPossivelAgendar(agendamento.Dia__c, horas);
            for(String horario : datasEHorariosDisponiveis){
          	   horaOptions.add(new SelectOption(horario, horario));
            }           
          }
        }else{
            horaOptions.add(new SelectOption('Dia indisponível', '')); 
        }
    }

    public void confirmaAgendamento(){
        System.debug('DIA AGENDADO: '+diaAgendado);
        if(existeVagaParaOAgendamento(diaAgendado, horarioAgendado)){
            Agendamento__c agendamento = fetchAgendamento(diaAgendado);
            List<Reagendamento__c> reagendamentosNoMesmoDia = fetchReagendamentosNoMesmoDia(diaAgendado);

            Reagendamento__c novoReagendamento = new Reagendamento__c();
    		    novoReagendamento.Data__c = diaAgendado;
            novoReagendamento.Hora__c = horarioAgendado;
            novoReagendamento.Reagendamento_del__c = opp.Id;
            novoReagendamento.Agendamento__c = agendamento.Id;

            opp = fetchNumeroDeAgendamentos(opp.Id);

            if(reagendamentosNoMesmoDia.size() > 0){
                novoReagendamento.Id = reagendamentosNoMesmoDia.get(0).Id;
                System.debug('ATUALIZOU REAGENDAMENTO');
                update novoReagendamento;
            }else if(opp.Numero_de_Agendamentos__c < QUANTIDADE_MAX_REAGENDAMENTOS){
                System.debug('ANTIGO NUMERO DE AGENDAMENTOS: ' + opp.Numero_de_Agendamentos__c);
                opp.Numero_de_Agendamentos__c++;
                System.debug('NOVO NUMERO DE AGENDAMENTOS: ' + opp.Numero_de_Agendamentos__c);
                insert novoReagendamento;
            }

            updateOpportunityWithNewScheduledDate(diaAgendado, horarioAgendado);
            agendamentoConcluido = true;
        }
    }

     private void setCandidateViewInformation(){
        nome = opp.Account.Name;
        cpf = opp.Account.CPF_2__c;
        polo = opp.Polo__r.Name;
        nInscricao = opp.Numeroinscrprocsel__c;
        idpolo = opp.Polo__c;
    }

    private Boolean isUserAbleToReschedule(){
        if(opp.Numero_de_Agendamentos__c >= QUANTIDADE_MAX_REAGENDAMENTOS){
            return false;
        }else{
            return true;
        }
    }

    private void updateOpportunityWithNewScheduledDate(Date dia, String hora){
        opp.Data_e_hora_da_prova_agendada__c = datetime.newInstance(dia,convertString2Time(hora));
        opp.Dia_Prova__c = dia;
        opp.Hora_Prova__c = hora;   
        opp.Presente__c = false;
        opp.StageName = 'Inscrito';
        opp.Page_Auto_Refresh__c = true;
        update opp;
        System.debug('Atualizou dia da Prova');
    } 

    private Boolean existeVagaParaOAgendamento(Date dia, String hora){
        Agendamento__c agendamento = fetchAgendamento(dia);

        List<Reagendamento__c> reagendamentos = [SELECT Id 
                                                   FROM Reagendamento__c 
                                                  WHERE Agendamento__c =:agendamento.id 
                                                        AND Hora__c=:hora];

        if(reagendamentos.size() > agendamento.Capacidade__c){
            calljavascript_cls();
            return false;
        }else{
            return true;
        }   
    }

    private List<String> descobreOsHorariosEDiasQueAindaEPossivelAgendar(Date dia , List<String> horarios){
        Agendamento__c agendamento = fetchAgendamento(dia);

        List<Reagendamento__c> reagendamentos = [SELECT Id,
                                                        Hora__c
                                                   FROM Reagendamento__c 
                                                  WHERE Agendamento__c =:agendamento.id 
                                                        AND Hora__c IN: horarios];

        List<String> horariosQueAindaEstaoDisponiveis = new List<String>();
        for(String horario:horarios){
          Integer quantity = 0;

          for(Reagendamento__c reagendamento : reagendamentos){
            if(reagendamento.Hora__c.equals(horario)){
              quantity++;
            }
          }

          if(quantity < agendamento.Capacidade__c){
            horariosQueAindaEstaoDisponiveis.add(horario);
          }

        }

        return horariosQueAindaEstaoDisponiveis;
    }

    //FAZER DAOS PARA OS OBJETOS
    private Opportunity fetchNumeroDeAgendamentos(Id oppId){
      return [SELECT id, 
                     Numero_de_Agendamentos__c
                FROM Opportunity 
               WHERE Id=:oppId];
    }

    private Agendamento__c fetchAgendamento(Date dia){
        return [SELECT Id, 
                       Dia__c, 
                       Capacidade__c
                  FROM Agendamento__c 
                 WHERE Dia__c =:dia 
                       AND Polo__c =:idpolo 
                 LIMIT 1];
    }

    private List<Agendamento__c> fetchAvailableScheduleDates(){
        return [SELECT Id, 
                       Name, 
                       Polo__c,
                       Dia__c
                  FROM Agendamento__c 
                 WHERE Polo__r.Name=:polo
              ORDER BY Dia__c ASC];
    }

    private List<Agendamento__c> fetchAvailableScheduleTimes(Date data){
        return [SELECT Id, 
                       Name, 
                       Polo__c, 
                       Hor_rio__c,
                       Dia__c 
                  FROM Agendamento__c 
                 WHERE Dia__c=:data 
                       AND Polo__c =:idpolo];
    }

    private List<Reagendamento__c> fetchReagendamentosNoMesmoDia(Date dia){
        return [SELECT Id, 
                       Name, 
                       Data__c, 
                       Agendamento__c, 
                       Candidato__c,
                       Hora__c
                  FROM Reagendamento__c 
                 WHERE Reagendamento_del__c =:opp.Id 
                       AND Data__c =:dia];
    }


    //JOGAR EM UM UTILS/HELPER
    private Time convertString2Time(String timeText){
        String[] timeValues = retrieveUpperLimitOfScheduledTime(timeText).split(':');
        Time examScheduledTime = null;
        Integer hour = Integer.valueOf(timeValues.get(0));
        Integer minutes = Integer.valueOf(timeValues.get(1));
        try{
          examScheduledTime = Time.newInstance(hour, minutes,0,0);
        }catch(exception ex){
          throw ex;
        }
        return examScheduledTime;
    }

    private String retrieveUpperLimitOfScheduledTime(String timeText){
        String timeUpperLimit = timeText.substring(timeText.length()-5);
        return timeUpperLimit;
    }
}