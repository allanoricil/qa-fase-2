/*
    @author Diego Moreira
    @class Classe de negocio para serviço de ficha de inscrição
*/
public with sharing class SRM_FichaInscricaoBO implements IProcessingQueue {
    /* 
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
        if( eventName.equals( QueueEventNames.FICHA_INSCRICAO.name() ) )
            processaFichaInscricao( queueId, payload );
        else
            processaFichaCaptacao( queueId, payload );
    }

    /*
        Processa fila de ficha de inscrição
        @param queueId Id da fila de processamento
        @param payload JSON com o item da fila para processamento
    */
    public static String processaFichaInscricao( String queueId, String payload ) {     
        Savepoint sp = Database.setSavepoint();

        try {
            Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );         
            List<Opportunity> opportunityToUpsert = new List<Opportunity>();

            for( Object objInscricao : ( List<Object> ) mapResult.get( 'FichaInscricao' ) ) {
                Map<String, Object> mapInscricao = ( Map<String, Object> ) objInscricao;

                if( (String) mapInscricao.get( 'IdInscricao' ) == '' ) {
                    if( inscricaoDuplicada( (String) mapInscricao.get( 'CPF' ), (String) mapInscricao.get( 'IdProcessoSeletivo' ) ) ) {
                        QueueBO.getInstance().updateQueue( queueId, QueueEventNames.FICHA_INSCRICAO.name() + ' / Inscrição duplicada CPF: ' + (String) mapInscricao.get( 'CPF' ) + ' Id processo seletivo: ' + (String) mapInscricao.get( 'IdProcessoSeletivo' ) );
                        return jsonResponse( 'NOK', 'Já existe uma inscrição com este CPF' );
                    }
                }
            }

            for( Object objInscricao : ( List<Object> ) mapResult.get( 'FichaInscricao' ) ) {
                Map<String, Object> mapInscricao = ( Map<String, Object> ) objInscricao;

                Account account = dadosConta( mapInscricao );
                upsert account;

                opportunityToUpsert.add( dadosOportunidade( mapInscricao, account.Id ) );
            }

            upsert opportunityToUpsert;
            QueueBO.getInstance().updateQueue( queueId, '' );
            return jsonResponse( 'OK', opportunityToUpsert[0].Id, '' );
        } catch( DmlException ex ) {
            Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, QueueEventNames.FICHA_INSCRICAO.name() + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            return jsonResponse( 'NOK', ex.getMessage() );
        } catch( Exception ex ) {
            Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, QueueEventNames.FICHA_INSCRICAO.name() + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            return jsonResponse( 'NOK', ex.getMessage() );
        }
    }

    /*
        Processa fila de ficha de captação
        @param queueId Id da fila de processamento
        @param payload JSON com o item da fila para processamento
    */
    public static String processaFichaCaptacao( String queueId, String payload ) {
        Savepoint sp = Database.setSavepoint();

        try {
            List<Lead> leadToInsert = new List<Lead>();
            Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( payload );         
            
            for( Object objInscricao : ( List<Object> ) mapResult.get( 'FichaInscricao' ) ) {
                Map<String, Object> mapInscricao = ( Map<String, Object> ) objInscricao;

                leadToInsert.add( dadosLead( mapInscricao ) );              
            }

            insert leadToInsert;
            QueueBO.getInstance().updateQueue( queueId, '' );
            return jsonResponse( 'OK', '' );
        } catch( DmlException ex ) {
            Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, QueueEventNames.FICHA_CAPTACAO.name() + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            return jsonResponse( 'NOK', ex.getMessage() );    
        } catch( Exception ex ) {
            Database.rollback(sp);
            QueueBO.getInstance().updateQueue( queueId, QueueEventNames.FICHA_CAPTACAO.name() + ' / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            return jsonResponse( 'NOK', ex.getMessage() );
        }
    }

    /*
        Monta os dados do objeto
        @param mapInscricao Mapa com os dados de inscrição
    */
    private static Lead dadosLead( Map<String, Object> mapInscricao ) {
        Lead lead = new Lead();
        lead.LastName                                       = (String) mapInscricao.get( 'LastName' );
        lead.Nacionalidade__c                               = (String) mapInscricao.get( 'Nacionalidade' );
        lead.CPF__c                                         = (String) mapInscricao.get( 'CPF' );
        lead.VoceTreineiro__c                               = (String) mapInscricao.get( 'Treineiro' );
        lead.Sexo__c                                        = (String) mapInscricao.get( 'Sexo' ); 
        lead.Phone                                          = (String) mapInscricao.get( 'Telefone' );
        lead.MobilePhone                                    = (String) mapInscricao.get( 'Celular' );
        lead.Email                                          = (String) mapInscricao.get( 'Email' );
        lead.Data_de_Nascimento__c                          = (String) mapInscricao.get( 'DataNascimento' ) != '' ? Date.valueOf( String.valueOf( mapInscricao.get( 'DataNascimento' ) ) ) : null;
        lead.Rua__c                                         = (String) mapInscricao.get( 'Rua' );
        lead.N_mero__c                                      = (String) mapInscricao.get( 'Numero' );
        lead.Complemento__c                                 = (String) mapInscricao.get( 'Complemento' );
        lead.Bairro__c                                      = (String) mapInscricao.get( 'Bairro' );
        lead.CEP__c                                         = (String) mapInscricao.get( 'Cep' );
        lead.Cidade__c                                      = (String) mapInscricao.get( 'Cidade' );
        lead.Estado__c                                      = (String) mapInscricao.get( 'Estado' ); 
        lead.Tipo_de_Escola_cursa_cursou_ensino_medio__c    = (String) mapInscricao.get( 'TipoEscolaCursou' );
        lead.Portador_de_necessidades_especiais__c          = (String) mapInscricao.get( 'PrecisaAtendimentoEspecial' );

        return lead;
    }

    /*

    */
    private static Account dadosConta( Map<String, Object> mapInscricao ) {
        Account account                                     = new Account();
        account.Id                                          = (String) mapInscricao.get( 'IdConta' ) != '' ? (String) mapInscricao.get( 'IdConta' ) : null;
        account.RecordTypeId                                = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DNA do Aluno').getRecordTypeId();
        account.OwnerId                                     = Label.SRM_OWNERID;
        account.Z1_opcao_curso__c                           = (String) mapInscricao.get( 'IdOpcaoCurso1' );
        account.Z2_opcao_curso__c                           = (String) mapInscricao.get( 'IdOpcaoCurso2' ) != '' ? (String) mapInscricao.get( 'IdOpcaoCurso2' ) : null;
        account.LastName                                    = (String) mapInscricao.get( 'LastName' );
        account.Nacionalidade__c                            = (String) mapInscricao.get( 'Nacionalidade' );
        account.CPF_2__c                                    = (String) mapInscricao.get( 'CPF' );
        account.Sexo__c                                     = (String) mapInscricao.get( 'Sexo' );
        account.Phone                                       = (String) mapInscricao.get( 'Telefone' );
        account.PersonMobilePhone                           = (String) mapInscricao.get( 'Celular' );
        account.PersonEmail                                 = (String) mapInscricao.get( 'Email' );
        account.DataNascimento__c                           = (String) mapInscricao.get( 'DataNascimento' ) != '' ? Date.valueOf( String.valueOf( mapInscricao.get( 'DataNascimento' ) ) ) : null;
        account.CEP__c                                      = (String) mapInscricao.get( 'Cep' );
        account.Rua__c                                      = (String) mapInscricao.get( 'Rua' );
        account.N_mero__c                                   = (String) mapInscricao.get( 'Numero' );
        account.Complemento__c                              = (String) mapInscricao.get( 'Complemento' );
        account.Bairro__c                                   = (String) mapInscricao.get( 'Bairro' );
        account.Cidade__c                                   = (String) mapInscricao.get( 'Cidade' );
        account.Estado__c                                   = (String) mapInscricao.get( 'Estado' );
        account.Tipo_escola_cursa_ou_cursou_ensino_medio__c = (String) mapInscricao.get( 'TipoEscolaCursou' );
        account.E_portador_de_alguma_aten_o_especial__c     = (String) mapInscricao.get( 'PortadorAtencaoEspecial' );
        account.PrecisaAtendimentoEspecial__c               = (String) mapInscricao.get( 'PrecisaAtendimentoEspecial' );
        account.Colegio__c                                  = (String) mapInscricao.get( 'IdColegio' ) != '' ? (String) mapInscricao.get( 'IdColegio' ) : null;
        account.Cor_Ra_a__c                                 = (String) mapInscricao.get( 'CorRaca' ) != '' ? (String) mapInscricao.get( 'CorRaca' ) : '';

        return account;
    }

    /*

    */
    private static Opportunity dadosOportunidade( Map<String, Object> mapInscricao, String accountId ) {
        Opportunity opportunity             = new Opportunity();
        opportunity.Id                      = (String) mapInscricao.get( 'IdInscricao' ) != '' ? (String) mapInscricao.get( 'IdInscricao' ) : null;
        opportunity.RecordTypeId            = OpportunityDAO.RECORDTYPE_GRADUACAO;
        opportunity.OwnerId                 = Label.SRM_OWNERID;
        opportunity.AccountId               = accountId;
        opportunity.FezProvaENEM__c         = (String) mapInscricao.get( 'FezProvaENEM' );
        opportunity.QualNotaMediaENEM__c    = (String) mapInscricao.get( 'QualNotaMediaENEM' ) != '' ? Double.valueOf( (String) mapInscricao.get( 'QualNotaMediaENEM' ) ) : 0;
        opportunity.AnoProvaENEM__c         = (String) mapInscricao.get( 'AnoProvaENEM' ) != '' ? Double.valueOf( (String) mapInscricao.get( 'AnoProvaENEM' ) ) : 0;
        opportunity.Treineiro__c            = (String) mapInscricao.get( 'Treineiro' );         
        opportunity.Name                    = 'Integração';
        opportunity.CloseDate               = Date.valueOf( String.valueOf( mapInscricao.get( 'DataValidadeProcesso' ) ) );
        opportunity.Processo_seletivo__c    = (String) mapInscricao.get( 'IdProcessoSeletivo' );
        opportunity.StageName               = 'Inscrito';
        opportunity.X1_OpcaoCurso__c        = (String) mapInscricao.get( 'IdOpcaoCurso1' );
        opportunity.X2_Op_o_de_curso__c     = (String) mapInscricao.get( 'IdOpcaoCurso2' ) != '' ? (String) mapInscricao.get( 'IdOpcaoCurso2' ) : null;
        opportunity.Email__c                = (String) mapInscricao.get( 'Email' );
        opportunity.Telefone__c             = (String) mapInscricao.get( 'Telefone' );
        opportunity.Celular__c              = (String) mapInscricao.get( 'Celular' );
        opportunity.Data_Nascimento__c      = (String) mapInscricao.get( 'DataNascimento' ) != '' ? Date.valueOf( String.valueOf( mapInscricao.get( 'DataNascimento' ) ) ) : null;
        opportunity.CanalInscricao__c       = (String) mapInscricao.get( 'CanalInscricao' );
        opportunity.CodigoPromocional__c    = (String) mapInscricao.get( 'IdCodigoPromocional' ) != '' ? (String) mapInscricao.get( 'IdCodigoPromocional' ) : null;
        opportunity.M_dia_Digital__c        = (String) mapInscricao.get( 'MidiaDigital' );
        opportunity.Campanha_Digital__c     = (String) mapInscricao.get( 'CampanhaDigital' );
        opportunity.Agendamento__c          = (String) mapInscricao.get( 'IdAgendamento' ) != '' ? (String) mapInscricao.get( 'IdAgendamento' ) : null;

        //Novos campos para ficha dinâmica
        opportunity.Data_e_hora_da_prova_agendada__c = (String) mapInscricao.get( 'DataHoraProvaAgendada' ) != '' ? DateTime.valueOf(String.valueOf(mapInscricao.get( 'DataHoraProvaAgendada' ))) : null;
        opportunity.Quando_deseja_ingressar__c = (String) mapInscricao.get( 'PeriodoIngresso' ) != '' ? (String) mapInscricao.get( 'PeriodoIngresso' ) : null;
        opportunity.Nome_da_Mae__c          = (String) mapInscricao.get( 'NomeResponsavelMae' ) != '' ? (String) mapInscricao.get( 'NomeResponsavelMae' ) : null;
        opportunity.Telefone_mae__c         = (String) mapInscricao.get( 'TelefoneResponsavelMae' ) != '' ? (String) mapInscricao.get( 'TelefoneResponsavelMae' ) : null;
        opportunity.Email_Mae__c            = (String) mapInscricao.get( 'EmailResponsavelMae' ) != '' ? (String) mapInscricao.get( 'EmailResponsavelMae' ) : null;
        opportunity.Nome_do_pai__c          = (String) mapInscricao.get( 'NomeResponsavelPai' ) != '' ? (String) mapInscricao.get( 'NomeResponsavelPai' ) : null;
        opportunity.Telefone_Pai__c         = (String) mapInscricao.get( 'TelefoneResponsavelPai' ) != '' ? (String) mapInscricao.get( 'TelefoneResponsavelPai' ) : null;
        opportunity.Email_Pai__c            = (String) mapInscricao.get( 'EmailResponsavelPai' ) != '' ? (String) mapInscricao.get( 'EmailResponsavelPai' ) : null;
        
        //NOVOS CAMPOS DATA E HORA 
        /*
        String dataProva = (String)mapInscricao.get('DataProva');
        String horarioProva = (String)mapInscricao.get('HoraProva');
        opportunity.Dia_Prova__c = dataProva != '' ? DateTime.valueOf(dataProva) : null;
        opportunity.Hora_Prova__c = horarioProva != ''? horarioProva : null;
        teste
        */
        
        opportunity.EAD__c                  = (String) mapInscricao.get( 'IsEAD' ) != '' ? (String) mapInscricao.get( 'IsEAD' ) : null; // Sim ou não
        opportunity.Polo__c                 = (String) mapInscricao.get( 'IdPolo' ) != '' ? (String) mapInscricao.get( 'IdPolo' ) : null; // Id do polo
        opportunity.Usuario_conversao_ficha__c = (String) mapInscricao.get( 'UsuarioConversaoFicha' ) != '' ? (String) mapInscricao.get( 'UsuarioConversaoFicha' ) : null;
        opportunity.Email_do_Polo__c        = (String) mapInscricao.get( 'EmailDoPolo' ) != '' ? (String) mapInscricao.get( 'EmailDoPolo' ) : null; 
        //CRA
        
         //Processo_seletivo__c proc = new Processo_seletivo__c();
         String proc = (string) mapInscricao.get( 'IdProcessoSeletivo' );
        
         List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Niveis_de_Ensino__c, Institui_o_n__c, Campus__c, Tipo_do_curso_oferecido__c
                                                 FROM Processo_seletivo__c p WHERE p.id =: proc LIMIT 1];
            
              //  newOpportunity.Instituicao__c          = lstProcess[0].Institui_o_n__c;
            //  newOpportunity.Campus_Unidade__c       = lstProcess[0].Campus__c;
                //newOpportunity.Tipo_do_curso__c        = lstProcess[0].Tipo_do_curso_oferecido__c;
        
        opportunity.Instituicao__c                      = lstProcess[0].Institui_o_n__c;
        opportunity.Campus_Unidade__c                   = lstProcess[0].Campus__c;
        opportunity.Tipo_do_curso__c                    = lstProcess[0].Tipo_do_curso_oferecido__c;
        

        //Se for EAD e tiver cpf duplicado, retorna nulo
        if(opportunity.EAD__c == 'Sim'){
            if(inscricaoDuplicada((String) mapInscricao.get( 'CPF' ), opportunity.Processo_seletivo__c))
                return null;
        }

        return opportunity;
    }

    /*
        Retorna os dados de consulta da fica de inscrição
        @param parameters Parametros para consulta de dados
    */
    public static String getRecords( Map<String, String> parameters ) {
        String sObjectSF = parameters.get( 'object' );
        String fields = parameters.get( 'fields' ) == null || parameters.get( 'fields' ).equals( '' ) ? 'Id,Name' : parameters.get( 'fields' );
        String filter = parameters.get( 'filter' ) == null || parameters.get( 'filter' ).equals( '' ) ? 'Limit 2000' : 'WHERE ' + parameters.get( 'filter' );

        String soql = 'SELECT ' + fields + ' FROM ' + sObjectSF + ' ' + filter;
        System.debug( '>>> SOQL ' + soql );

        List<sObject> dataReultList = Database.query( soql );

        return JSON.serialize( dataReultList );
    }

    /*
        Valida a inscrição
    */
    private static Boolean inscricaoDuplicada( String cpf, String idProcessoSeletivo ) {
        List<Opportunity> oppDuplicate = OpportunityDAO.getInstance().getOpportunityByCPFAndProcessoSeletivo( cpf, idProcessoSeletivo );
        if( oppDuplicate.size() > 0 ) return true;
        return false;
    }

    /*
        JSON de retorno de serviço
        @action GET -> ERROR na consulta
        @action POST -> Mensagem de retorno do sf de recebimento com SUCESSO ou ERRO 
    */
    public static String jsonResponse( String status, String mensagem ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'status', status ); 
        gen.writeStringField( 'mensagem', mensagem ); 
        gen.writeEndObject(); 

        return gen.getAsString(); 
    }

    /*
        JSON de retorno de serviço
        @action GET -> ERROR na consulta
        @action POST -> Mensagem de retorno do sf de recebimento com SUCESSO ou ERRO 
    */
    public static String jsonResponse( String status, String idInscricao, String mensagem ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'status', status ); 
        gen.writeStringField( 'idInscricao', idInscricao ); 
        gen.writeStringField( 'mensagem', mensagem ); 
        gen.writeEndObject(); 

        return gen.getAsString(); 
    }

}