global class  eadIterator implements Iterator<list<EadWrapper>>
{ 
   list<EadWrapper> InnerList 		{ get; set; }
   list<EadWrapper> ListRequested	{ get; set; }

   public static Integer totalPages { get; set;}
   public static Integer currentPageNumber { get; set;}
   Integer i {get; set;} 
   public Integer setPageSize 		{ get; set; } 

   public eadIterator(List<EadWrapper> lst)
   {
       InnerList = new list<EadWrapper>(); 
       ListRequested = new list<EadWrapper>();     
       InnerList = lst;
       setPageSize = 10;
       i = 0; 
   }   

   global boolean hasNext(){ 
       if(i >= InnerList.size()) {
           return false; 
       } else {
           return true; 
       }
   } 
   
   global boolean hasPrevious(){ 
       if(i <= setPageSize) {
           return false; 
       } else {
           return true; 
       }
   }   

   global list<EadWrapper> next(){ 

        ListRequested = new list<EadWrapper>(); 
        integer startNumber;

        integer size = InnerList.size();
        if(hasNext())
        { 
        if(size <= (i + setPageSize))
        {
        startNumber = i;
        i = size;
        currentPageNumber = i/setPageSize + 1;

        }
        else
        {
        currentPageNumber = i/setPageSize + 1;
        i = (i + setPageSize);
        startNumber = (i - setPageSize);
        }

        for(integer start = startNumber; start < i; start++)
        {
        ListRequested.add(InnerList[start]);

        }
        } 
        if(InnerList.size() > 0){
          totalPages = (InnerList.size())/setPageSize;
          if(math.mod(totalPages,setPageSize) != 0) totalPages = totalPages + 1;
        }
        else{
          currentPageNumber = 0;
          totalPages = 0;
        }

        return ListRequested;
    } 
   
   global list<EadWrapper> previous(){ 

      ListRequested = new list<EadWrapper>(); 
      integer size = InnerList.size(); 

      if(i == size)
      {
      if(math.mod(size, setPageSize) > 0)
      { 
      currentPageNumber = i/setPageSize; 
      i = size - math.mod(size, setPageSize);

      }
      else
      {
      currentPageNumber = i/setPageSize - 1;
      i = (size - setPageSize);
      } 

      }
      else
      {
      currentPageNumber = i/setPageSize - 1;
      i = (i - setPageSize);
      }

      for(integer start = (i - setPageSize); start < i; ++start)
      {
      ListRequested.add(InnerList[start]);
      } 

      totalPages = (InnerList.size())/setPageSize;
      if(math.mod(totalPages,setPageSize) != 0) totalPages = totalPages + 1;

      return ListRequested;
  }   
}