public without sharing class SetInscricoesContas {
	public static void setInscricoesContas(List<Opportunity> allOppsNew, List<Opportunity> allOppsOld, Boolean isInsert) {
		system.debug('ENTROU NA TRIGGER OOOOOOOOOOOOOOOO');
    
	    // Process up to 48 opportunities records per call.
	    if (allOppsNew.size() <= 23){
	        RecordType rec = [Select id From RecordType Where Name =: 'Conta Pessoal' limit 1];
	        //User admin = [Select id From User Where Name =: 'Servico Data Loader'];
	        //List<String> sup = new List<String>{'asantos58@devrybrasil.edu.br'};
	        for (Opportunity opp : allOppsNew){
	            if(opp.AccountId == null){
		            if( opp.RecordTypeId != OpportunityDAO.RECORDTYPE_GRADUACAO && opp.RecordTypeId != OpportunityDAO.RECORDTYPE_MESTRADO && opp.RecordTypeId != Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Processo empresarial' ).getRecordTypeId()) {
		                Account acc = new Account();
		                if ([Select count() From Account Where (CPF_2__c =: opp.CPF__c AND CPF_2__c != null) OR (RG__c =: opp.RG__c AND RG__c != null AND CPF_2__c =: null) OR (Passaporte__c =: opp.Passaporte__c AND Passaporte__c != null AND CPF_2__c =: null) limit 1] > 0){
		                    System.debug('first@@Sunil@@'+opp);
		                    acc = [Select Id, Bairro__c, PersonMobilePhone, CEP__c, Cidade__c, Codigo_Especial__c, Complemento__c, firstname,
		                            Confirmar_E_mail__c, Confirmar_Telefone__c, CPF__c, Curso_Aprovado__c, PersonBirthdate, 
		                            E_portador_de_alguma_aten_o_especial__c, PersonEmail, Empresa_Parceira__c, Estado__c, Estado_Civil__c, 
		                            ID_Participacao__c, Local_que_cursa_ou_cursou_ensino_m_dio__c, N_mero__c, Nacionalidade__c, 
		                            Naturalidade__c, Nome_da_Mae__c, Nome_do_Pai__c, Origem_RG__c, Passaporte__c, Processo_seletivo__c, Processo_seletivo_de_Interesse__pc,  Tipo_de_Matr_cula__pc, Empresa_Escola__c,
		                            Qual_atencao__c, Questao_01__c, Questao_02__c, Questao_03__c, 
		                            Questao_04__c, Questao_05__c, Questao_06__c, Questao_07__c, Questao_08__c, Questao_09__c, 
		                            Questao_10__c, RG__c, Rua__c, Sexo__c, LastName, Status_da_Avalia_o__c, Taxa_Paga_Isento__c, 
		                            Phone, Tipo_escola_cursa_ou_cursou_ensino_medio__c, Treineiro__c, Z1_opcao_curso__c, 
		                            Z2_opcao_curso__c  From Account Where (CPF_2__c =: opp.CPF__c AND CPF_2__c != null) OR (RG__c =: opp.RG__c AND RG__c != null AND CPF_2__c =: null) OR (Passaporte__c =: opp.Passaporte__c AND Passaporte__c != null AND CPF_2__c =: null) limit 1];     
		                    acc.ID_Participacao__c = opp.ID_Participa_o__c;
		                    if (opp.CPF__c != null) acc.CPF_2__c = opp.CPF__c;
		                    if (opp.Bairro__c != null) acc.Bairro__c=opp.Bairro__c;
		                    if (opp.Celular__c != null) acc.PersonMobilePhone =opp.Celular__c;
		                    if (opp.CEP__c != null) acc.CEP__c=opp.CEP__c;
		                    if (opp.Cidade__c != null) acc.Cidade__c=opp.Cidade__c;
		                    if (opp.Codigo_Especial__c != null) acc.Codigo_Especial__c=opp.Codigo_Especial__c;
		                    if (opp.Complemento__c != null) acc.Complemento__c=opp.Complemento__c;
		                    if (opp.Confirmar_E_mail__c != null) acc.Confirmar_E_mail__c=opp.Confirmar_E_mail__c;
		                    if (opp.Confirmar_Telefone__c != null) acc.Confirmar_Telefone__c=opp.Confirmar_Telefone__c;
		                    if (opp.Curso_Aprovado__c != null) acc.Curso_Aprovado__c=opp.Curso_Aprovado__c;
		                    if (opp.Data_Nascimento__c != null) acc.PersonBirthdate=opp.Data_Nascimento__c;
		                    if (opp.E_portador_de_alguma_aten_o_especial__c != null) acc.E_portador_de_alguma_aten_o_especial__c=opp.E_portador_de_alguma_aten_o_especial__c;
		                    if (opp.Email__c != null) acc.PersonEmail =opp.Email__c;
		                    if (opp.Empresa_Parceira__c != null) acc.Empresa_Parceira__c=opp.Empresa_Parceira__c;
		                    if (opp.Estado__c != null) acc.Estado__c=opp.Estado__c;
		                    if (opp.Estado_Civil__c != null) acc.Estado_Civil__c=opp.Estado_Civil__c;
		                    if (opp.Local_que_cursa_ou_cursou_ensino_m_dio__c != null) acc.Local_que_cursa_ou_cursou_ensino_m_dio__c=opp.Local_que_cursa_ou_cursou_ensino_m_dio__c;
		                    if (opp.N_mero__c != null) acc.N_mero__c=opp.N_mero__c;
		                    if (opp.Nacionalidade__c != null) acc.Nacionalidade__c=opp.Nacionalidade__c;
		                    if (opp.Naturalidade__c != null) acc.Naturalidade__c=opp.Naturalidade__c;
		                    if (opp.Nome_da_Mae__c != null) acc.Nome_da_Mae__c=opp.Nome_da_Mae__c;
		                    if (opp.Nome_do_Pai__c != null) acc.Nome_do_Pai__c=opp.Nome_do_Pai__c;
		                    if (opp.Origem_RG__c != null) acc.Origem_RG__c=opp.Origem_RG__c;
		                    if (opp.Passaporte__c != null) acc.Passaporte__c=opp.Passaporte__c;
		                    if (opp.Processo_seletivo__c != null) acc.Processo_seletivo__c=opp.Processo_seletivo__c;
		                    if (opp.Qual_atencao__c != null) acc.Qual_atencao__c=opp.Qual_atencao__c;
		                    if (opp.Questao_01__c != null) acc.Questao_01__c=opp.Questao_01__c;
		                    if (opp.Questao_02__c != null) acc.Questao_02__c=opp.Questao_02__c;
		                    if (opp.Questao_03__c != null) acc.Questao_03__c=opp.Questao_03__c;
		                    if (opp.Questao_04__c != null) acc.Questao_04__c=opp.Questao_04__c;
		                    if (opp.Questao_05__c != null) acc.Questao_05__c=opp.Questao_05__c;
		                    if (opp.Questao_06__c != null) acc.Questao_06__c=opp.Questao_06__c;
		                    if (opp.Questao_07__c != null) acc.Questao_07__c=opp.Questao_07__c;
		                    if (opp.Questao_08__c != null) acc.Questao_08__c=opp.Questao_08__c;
		                    if (opp.Questao_09__c != null) acc.Questao_09__c=opp.Questao_09__c;
		                    if (opp.Questao_10__c != null) acc.Questao_10__c=opp.Questao_10__c;
		                    if (opp.RG__c != null) acc.RG__c=opp.RG__c;
		                    if (opp.Rua__c != null) acc.Rua__c=opp.Rua__c;
		                    if (opp.Sexo__c != null) acc.Sexo__c=opp.Sexo__c;
		                    if (opp.Primeiro_Nome__c != null) acc.FirstName =opp.Primeiro_Nome__c;
		                    else
		                    {
		                        opp.Primeiro_Nome__c = acc.FirstName;
		                    }
		                    if (opp.Sobrenome__c != null) acc.LastName =opp.Sobrenome__c;
		                    else                        
		                    {
		                        opp.Sobrenome__c = acc.LastName;
		                    }
		                    if (opp.Status_da_Avalia_o__c != null) acc.Status_da_Avalia_o__c=opp.Status_da_Avalia_o__c;
		                    if (opp.Taxa_Paga_Isento__c != null) acc.Taxa_Paga_Isento__c=opp.Taxa_Paga_Isento__c;
		                    if (opp.Telefone__c != null) acc.Phone =opp.Telefone__c;
		                    if (opp.Tipo_escola_cursa_ou_cursou_ensino_medio__c != null) acc.Tipo_escola_cursa_ou_cursou_ensino_medio__c=opp.Tipo_escola_cursa_ou_cursou_ensino_medio__c;
		                    if (opp.Treineiro__c != null) acc.Treineiro__c=opp.Treineiro__c;       
		                    if (opp.Oferta__c != null) acc.Z1_opcao_curso__c=opp.Oferta__c;
		                    if (opp.X2_Op_o_de_curso__c != null) acc.Z2_opcao_curso__c=opp.X2_Op_o_de_curso__c;
		                    if (opp.Processo_Seletivo__c != null) acc.Processo_seletivo_de_Interesse__pc = opp.Processo_Seletivo__c;
		                    if (opp.Tipo_de_matr_cula__c != null) acc.Tipo_de_Matr_cula__pc = opp.Tipo_de_matr_cula__c; 
		                    
		                    //Below two fields added by sunil
		                    if (opp.Renda_m_dia_familiar_em_Reais__c != null) acc.Renda_m_dia_familiar_em_Reais__c = opp.Renda_m_dia_familiar_em_Reais__c;
		                    if (opp.Possui_conta_no__c != null) acc.Possui_conta_no__c = opp.Possui_conta_no__c;
		                    
		                    try{
		                        if (isInsert){
		                            if (opp.Empresa_Parceira__c != null){
		                                if ([Select Count() From Account Where Name =: opp.Empresa_Parceira__c] > 0){
		                                    Account aux = [Select id From Account Where Name =: opp.Empresa_Parceira__c limit 1];
		                                    acc.Empresa_Escola__c = aux.id;
		                                }
		                                else{
		                                    RecordType rec2 = [Select id From RecordType Where Name =: 'Empresa' limit 1];
		                                    Account aux = new Account(recordTypeId = rec2.id, Name = opp.Empresa_Parceira__c);
		                                    insert aux;
		                                    acc.Empresa_Escola__c = aux.id;
		                                }
		                            }
		                            update acc;
		                        }
		                        else {
		                            for (Opportunity oppx : allOppsOld){
		                                system.debug('OwnerID opp: '+opp.OwnerID);
		                                system.debug('OwnerID oppx: '+oppx.OwnerID);
		                                
		                                
		                                system.debug('OwnerID opp: '+opp.Id);
		                                system.debug('OwnerID oppx: '+oppx.Id);
		                                if (oppx.Id == opp.Id){
		                                    if (opp.OwnerId != null  && opp.OwnerId == oppx.OwnerId){
		                                        if (opp.Empresa_Parceira__c != null){
		                                            if ([Select Count() From Account Where Name =: opp.Empresa_Parceira__c] > 0){
		                                                Account aux = [Select id From Account Where Name =: opp.Empresa_Parceira__c limit 1];
		                                                acc.Empresa_Escola__c = aux.id;
		                                            }
		                                            else{
		                                                RecordType rec2 = [Select id From RecordType Where Name =: 'Empresa' limit 1];
		                                                Account aux = new Account(recordTypeId = rec2.id, Name = opp.Empresa_Parceira__c);
		                                                insert aux;
		                                                acc.Empresa_Escola__c = aux.id;
		                                            }
		                                        }
		                                        update acc;
		                                    }                           
		                                }
		                            }
		                        }
		                        
		                    }
		                    catch (DMLException e){
		                        opp.addError('Ocorreu um erro atualizando registros de Contas/Candidatos. Verifique se estiver duplicado. O CPF é : ' + acc.CPF_2__c + '. Detalhes do erro: ' + e.getMessage());
		                        CreateErrorLog.createErrorRecord(e.getMessage(), 'SetInscricoesContas');                
		                        //emailHelper.sendEmail(admin.Id, null, null, false, true, null, 'DeVry Suporte Salesforce', sup, 'Erro na trigger SetInscricoesContas', e.getMessage());
		                        //emailHelper.sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, String ReplyTo, String SenderDisplayName, String[] setToAddresses, String Subject, String PlainTextBody) 
		                    }    
		        
		                    opp.AccountId = acc.Id;  
		                    if (isInsert) opp.Name = 'temp';//opp.ID_Participa_o__c + ' - ' + acc.Lastname;      
		                    opp.CloseDate = system.today();
		                    if (isInsert) opp.StageName = 'Inscrito';
		                    
		                    if (opp.StageName == 'Inscrito' && opp.Participa_o_no_Processo_Sel_Confirmada__c == 'Confirmado' && opp.Data_programada_Encontro_com_Profiss_o__c == null) 
		                        opp.StageName = 'Inscrição Confirmada';
		                    
		                    if (opp.Status_da_Avalia_o__c != null) opp.StageName = opp.Status_da_Avalia_o__c;
		                    
		                    if (opp.Data_de_Matr_cula__c != null) opp.StageName = 'Matriculado';
		                    
		                    if (opp.Candidato_Iniciado__c) opp.StageName = 'Iniciado';
		                }        
		                else if (opp.CPF__c != null || opp.Treineiro__c != 'Não'){ 
		                    System.debug('Second@@Sunil@@'+opp.CPF__c +' '+opp.Treineiro__c );                          
		                    acc = new Account();
		                    acc.recordTypeId = rec.Id;
		                    acc.ID_Participacao__c = opp.ID_Participa_o__c;
		                    acc.CPF_2__c = opp.CPF__c;
		                    acc.Bairro__c=opp.Bairro__c;
		                    acc.PersonMobilePhone =opp.Celular__c;
		                    acc.CEP__c=opp.CEP__c;
		                    acc.Cidade__c=opp.Cidade__c;
		                    acc.Codigo_Especial__c=opp.Codigo_Especial__c;
		                    acc.Complemento__c=opp.Complemento__c;
		                    acc.Confirmar_E_mail__c=opp.Confirmar_E_mail__c;
		                    acc.Confirmar_Telefone__c=opp.Confirmar_Telefone__c;
		                    acc.Curso_Aprovado__c=opp.Curso_Aprovado__c;
		                    acc.PersonBirthdate=opp.Data_Nascimento__c;
		                    acc.E_portador_de_alguma_aten_o_especial__c=opp.E_portador_de_alguma_aten_o_especial__c;
		                    acc.PersonEmail =opp.Email__c;
		                    acc.Empresa_Parceira__c=opp.Empresa_Parceira__c;
		                    acc.Estado__c=opp.Estado__c;
		                    acc.Estado_Civil__c=opp.Estado_Civil__c;
		                    acc.Local_que_cursa_ou_cursou_ensino_m_dio__c=opp.Local_que_cursa_ou_cursou_ensino_m_dio__c;
		                    acc.N_mero__c=opp.N_mero__c;
		                    acc.Nacionalidade__c=opp.Nacionalidade__c;
		                    acc.Naturalidade__c=opp.Naturalidade__c;
		                    acc.Nome_da_Mae__c=opp.Nome_da_Mae__c;
		                    acc.Nome_do_Pai__c=opp.Nome_do_Pai__c;
		                    acc.Origem_RG__c=opp.Origem_RG__c;
		                    acc.Passaporte__c=opp.Passaporte__c;
		                    acc.Processo_seletivo__c=opp.Processo_seletivo__c;
		                    acc.Qual_atencao__c=opp.Qual_atencao__c;
		                    acc.Questao_01__c=opp.Questao_01__c;
		                    acc.Questao_02__c=opp.Questao_02__c;
		                    acc.Questao_03__c=opp.Questao_03__c;
		                    acc.Questao_04__c=opp.Questao_04__c;
		                    acc.Questao_05__c=opp.Questao_05__c;
		                    acc.Questao_06__c=opp.Questao_06__c;
		                    acc.Questao_07__c=opp.Questao_07__c;
		                    acc.Questao_08__c=opp.Questao_08__c;
		                    acc.Questao_09__c=opp.Questao_09__c;
		                    acc.Questao_10__c=opp.Questao_10__c;
		                    acc.RG__c=opp.RG__c;
		                    acc.Rua__c=opp.Rua__c;
		                    acc.Sexo__c=opp.Sexo__c;
		                    if (opp.Primeiro_Nome__c != null) acc.FirstName =opp.Primeiro_Nome__c;            
		                    if (opp.Sobrenome__c != null) acc.LastName =opp.Sobrenome__c;
		                    else
		                    {
		                        acc.LastName = opp.Name;
		                        opp.Sobrenome__c = opp.Name;
		                    }
		                    acc.Status_da_Avalia_o__c=opp.Status_da_Avalia_o__c;
		                    acc.Taxa_Paga_Isento__c=opp.Taxa_Paga_Isento__c;
		                    acc.Phone =opp.Telefone__c;
		                    acc.Tipo_escola_cursa_ou_cursou_ensino_medio__c=opp.Tipo_escola_cursa_ou_cursou_ensino_medio__c;
		                    acc.Treineiro__c=opp.Treineiro__c;
		                    if (opp.Oferta__c != null) acc.Z1_opcao_curso__c=opp.Oferta__c;
		                    if (opp.X2_Op_o_de_curso__c != null) acc.Z2_opcao_curso__c=opp.X2_Op_o_de_curso__c;
		                    acc.Processo_seletivo_de_Interesse__pc = opp.Processo_Seletivo__c;
		                    acc.Tipo_de_Matr_cula__pc = opp.Tipo_de_matr_cula__c;
		                    //Below two fields added by sunil
		                    acc.Renda_m_dia_familiar_em_Reais__c = opp.Renda_m_dia_familiar_em_Reais__c;
		                    acc.Possui_conta_no__c = opp.Possui_conta_no__c;
		                                     
		                    
		                    if (opp.Empresa_Parceira__c != null){
		                        if ([Select Count() From Account Where Name =: opp.Empresa_Parceira__c] > 0){
		                            Account aux = [Select id From Account Where Name =: opp.Empresa_Parceira__c limit 1];
		                            acc.Empresa_Escola__c = aux.id;
		                        }
		                        else{
		                            RecordType rec2 = [Select id From RecordType Where Name =: 'Empresa' limit 1];
		                            Account aux = new Account(recordTypeId = rec2.id, Name = opp.Empresa_Parceira__c);
		                            insert aux;
		                            acc.Empresa_Escola__c = aux.id;
		                        }
		                    }
		                    
		                    try{
		                        insert acc;
		                    }
		                    catch (DMLException e){
		                        opp.addError('Ocorreu um erro inserindo registros de Contas/Candidatos. Verifique se estiver duplicado. O CPF é : ' + account.CPF_2__c + '. Detalhes do erro: ' + e.getMessage());
		                        CreateErrorLog.createErrorRecord(e.getMessage(), 'SetInscricoesContas');
		                        //emailHelper.sendEmail(admin.Id, null, null, false, true, null, 'DeVry Suporte Salesforce', sup, 'Erro na trigger SetInscricoesContas', e.getMessage());
		                        //emailHelper.sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, String ReplyTo, String SenderDisplayName, String[] setToAddresses, String Subject, String PlainTextBody) {
		                    }  
		                    
		                    opp.AccountId = acc.Id;
		                    if (isInsert) opp.Name = 'temp';//opp.ID_Participa_o__c + ' - ' + acc.Lastname;
		                    opp.CloseDate = system.today();
		                    if (isInsert) opp.StageName = 'Inscrição';                
		                }
		            }
	        	}
	        }
	    }
	}
}