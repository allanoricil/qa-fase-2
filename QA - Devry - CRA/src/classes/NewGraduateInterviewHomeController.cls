/*******************************************************************
Author    :   Adílio Santos
Date      :   Agosto 2016
Purpose   :   Controller Class for Page NewGraduateInterviewHome!
*******************************************************************/

public without sharing class NewGraduateInterviewHomeController {  
    
    /* member variables */
    public Opportunity newOpportunity{get;set;}
    public Id theDean = null;
    public string selectedEstado {get;set;}
    public string selectedCampus {get;set;}
    public string selectedSelectionProcess {get;set;}
    public string selectedPrograms {get;set;}
    public string selectedScheduling {get;set;}
    public string selectedInterviewDate {get;set;}
    public string selectedInterviewTime {get;set;}
    public string formStep {get;set;}
    public String codigoPromocional { get; set; }
    public string getDocument() {return null;} 
    public List<SelectOption> campusItems {get;set;}
    public List<SelectOption> processItems {get;set;}
    public List<SelectOption> programsItems {get;set;}
    public List<SelectOption> schedulingItems {get;set;}
    public List<SelectOption> avaliableDateItems {get;set;}
    public List<SelectOption> interviewTimesOptions {get;set;}
    
    private String recordTypeId = ApexPages.currentPage().getParameters().get('recordTypeId');    
    //private String leadOrigemMidia = ApexPages.currentPage().getParameters().get('origem_midia');
    //private String leadOrigemCampanha = ApexPages.currentPage().getParameters().get('origem_campanha');
    
    public List<SelectOption> getCountriesSelectList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', 'Selecionar uma Opção'));        
        Map<String, Countries__c> countries = Countries__c.getAll();
        List<String> countryNames = new List<String>();
        countryNames.addAll(countries.keySet());
        countryNames.sort();
        for (String countryName : countryNames) {
            Countries__c country = countries.get(countryName);
            options.add(new SelectOption(country.Name, country.Name));
        }
        return options;
    }
    
    public String pageLanguage{
        get{
            String lang = ApexPages.currentPage().getParameters().get('lang');
            if( lang != null && lang.length() > 0){
                return lang;
            }
            return 'pt_BR';
        }
        set;
    }
    
    public String getcodPromocionalId(String codPromocional){
        String idCodPromo;
        List<CodigoPromocional__c> codigos = [SELECT Id, Name FROM Codigopromocional__c WHERE Name =: codPromocional];
        if(codigos.size() > 0){
            idCodPromo = codigos[0].id;
        }else{
            return null;
        }
        return idCodPromo;
    }
    
    /* constructor */
    public NewGraduateInterviewHomeController(){
        this.formStep = 'form';
        newOpportunity = new Opportunity();
        
        String name = ApexPages.currentPage().getParameters().get('name');
        String email = ApexPages.currentPage().getParameters().get('email');
        String phone = ApexPages.currentPage().getParameters().get('phone');
        
        if(name != '' || name != null)
            newOpportunity.Name = name;
        
        if(email != '' || email != null)
            newOpportunity.Email__c = email;
        
        if(phone != '' || phone != null)
            newOpportunity.Celular__c = phone;
        
        fillCampus();
        fillProcess();
        fillPrograms();
        fillInterviewDates();
        fillInterviewTimes();
    }
    
    public Pagereference OnchangeSelectionEstado(){
        fillCampus();
        return null;
    }
    
    public Pagereference OnchangeCampus(){
        fillProcess();
        return null;
    }
    
    public Pagereference OnchangeSelectionProcess(){
        fillPrograms();
        fillScheduling();
        fillInterviewDates();
        fillInterviewTimes();
        return null;
    }
    
    public Pagereference OnchangePrograms(){
        fillInterviewDates();
        fillInterviewTimes();
        return null;
    }
    
    public Pagereference OnchangeInterviewDate(){
        fillInterviewTimes();
        return null;
    }
    
    public Pagereference save(){
        try{
            //validaCodigo();

            newOpportunity.CodigoPromocional__c = getcodPromocionalId(codigoPromocional);
            newOpportunity.C_digo_Promocional__c = codigoPromocional;

            if(selectedSelectionProcess != null && selectedSelectionProcess != '')
                newOpportunity.Processo_seletivo__c = selectedSelectionProcess;
            if(selectedPrograms != null && selectedPrograms != '')
                newOpportunity.oferta__c = selectedPrograms;
            if(selectedScheduling != null && selectedScheduling != '')
                newOpportunity.Agendamento__c = selectedScheduling;
            if (recordTypeId != null && recordTypeId != '')
                newOpportunity.RecordTypeId = recordTypeId;
            
            /* Mapeamento Marketing */
            if (System.currentPageReference().getParameters().get('utm_source') != null) {
                newOpportunity.M_dia_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('utm_source'));
            } else {
                if (System.currentPageReference().getParameters().get('origem_midia') != null) {
                    newOpportunity.M_dia_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('origem_midia'));
                }
            }
            if (System.currentPageReference().getParameters().get('utm_campaign') != null) {
                newOpportunity.Campanha_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('utm_campaign'));
            } else {
                if (System.currentPageReference().getParameters().get('origem_campanha') != null) {
                    newOpportunity.Campanha_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('origem_campanha'));
                }
            }
            
            /*  
if (leadOrigemMidia != null && leadOrigemMidia != '') {
newOpportunity.M_dia_Digital__c = leadOrigemMidia;
}
if (leadOrigemCampanha != null && leadOrigemCampanha != '') {
newOpportunity.Campanha_Digital__c = leadOrigemCampanha;
}
*/
            try{
              /*  codigoPromocional = newOpportunity.CodigoPromocional__c;
                Boolean isIntegral = validaCodigoPromocional();*/
                
           
                  List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Niveis_de_Ensino__c, Institui_o_n__c, Campus__c, Tipo_do_curso_oferecido__c
                                                 FROM Processo_seletivo__c p
                                                      WHERE p.Id = :selectedSelectionProcess LIMIT 1];
            
                newOpportunity.Instituicao__c          = lstProcess[0].Institui_o_n__c;
        		newOpportunity.Campus_Unidade__c       = lstProcess[0].Campus__c;
        		newOpportunity.Tipo_do_curso__c        = lstProcess[0].Tipo_do_curso_oferecido__c;

                // System.debug('RecordTypeId da pagina   '+ApexPages.currentPage().getParameters().get('recordTypeId'));
                 System.debug('newOpportunity.RecordTypeId    '+newOpportunity.RecordTypeId   );  
                insert newOpportunity;
                System.debug('>>>>Opportunity inserted successfully.'+newOpportunity.Id+' Account:'+ +newOpportunity.AccountId);
                return returnFinishPage();
                    } catch(Exception exInner){
                        System.debug('>>>>Exception in inserting opprotunity'+exInner.getMessage());
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, exInner.getMessage()));
                        this.formStep = 'form';
                        return null;
                    }
            
            newOpportunity = new Opportunity();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  System.Label.Interview_Message)); 
            return returnFinishPage();
        } catch(Exception ex){
            System.debug('>>>>Exception in Save'+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            this.formStep = 'form';
            return returnFinishPage();
        }
        
        return returnFinishPage();
    }
    public PageReference returnFinishPage(){
        PageReference  pageRef = new PageReference('/apex/FinishPage');
        pageRef.setRedirect(true);
        Processo_Seletivo__C processo = [Select Tipo_de_Matr_cula__c from Processo_Seletivo__C where id = :selectedSelectionProcess];
        Campus__C campus = [Select Name from Campus__C where id = :selectedCampus];
        pageRef.getParameters().put('processo', processo.Tipo_de_Matr_cula__c);
        pageRef.getParameters().put('campus', campus.Name);
        pageRef.getParameters().put('IdCampus', campus.id);
        return pageRef;
    }
    
    public PageReference resetForm() {
        PageReference newpage = new PageReference(System.currentPageReference().getURL());
        newpage.setRedirect(true);
        return newpage;
    }
    
    public List<SelectOption> getAllSelectionEstadoItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        Map<String, Estados__c> estados = Estados__c.getAll();
        List<String> estadosNome = new List<String>();
        estadosNome.addAll(estados.keySet());
        estadosNome.sort();
        for (String e : estadosNome) {
            Estados__c estado = estados.get(e);
            if(estado.Name == 'BA' || estado.Name == 'CE' || estado.Name == 'PE' || estado.Name == 'RJ' || estado.Name == 'DF' || estado.Name == 'MG'|| estado.Name == 'SP') {
                options.add(new SelectOption(estado.Name, estado.Name));
            }
        }
        return options;
    }
    
    public List<SelectOption> getAllSelectionEstadoItemsSP(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        Map<String, Estados__c> estados = Estados__c.getAll();
        List<String> estadosNome = new List<String>();
        estadosNome.addAll(estados.keySet());
        estadosNome.sort();
        for (String e : estadosNome) {
            Estados__c estado = estados.get(e);
            if(estado.Name == 'SP') {
                options.add(new SelectOption(estado.Name, estado.Name));
            }
        }
        return options;
    }
    
    public List<SelectOption> getAllSelectionCampusItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        List<Campus__c> lstCampus = [Select c.Id, c.Name From Campus__c c where c.Estado__c = :selectedEstado];
        for (Campus__c c : lstCampus){
            String campus = c.Name;
            options.add(new SelectOption('',campus));
        }
        return options;
    } 
    
    public List<SelectOption> getAllSelectionProcessItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Nome_da_IES__c,p.name, p.Nome_do_Campus__c, 
                                                 p.Periodo_Letivo__c,p.Sess_o_Ciclo__c,Grupo_P_s__c, ndice_do_Processo__c, Nome_Instituicao_Pos__c
                                                 FROM Processo_seletivo__c p  
                                                 WHERE p.Campus__c != null and 
                                                 p.Periodo_Letivo__c != null and 
                                                 (p.Tipo_de_Matr_cula__c = 'Pós-Graduação IBMEC' or (p.Tipo_de_Matr_cula__c = 'Pós-Graduação' and p.Campus__c = 'a0PA000000FiNao')) and
                                                 p.Ativo__c = true and
                                                 p.Data_de_encerramento__c >= :system.today()
                                                 ORDER BY p.Campus__c ASC];
        for (Processo_seletivo__c proc : lstProcess)  {
            String campus = proc.Nome_do_Campus__c;
            String institution = proc.Nome_da_IES__c;
            if(proc.Nome_Instituicao_Pos__c != null && proc.Nome_Instituicao_Pos__c.length() > 0){
                institution = proc.Nome_Instituicao_Pos__c;
            }      
            String session;
            String indice;
            String grupo;
            if(String.isNotBlank(proc.Sess_o_Ciclo__c) && String.isNotEmpty(proc.Sess_o_Ciclo__c)){
                session = proc.Sess_o_Ciclo__c;
            }else{
                session = ' ';
            }
            if(String.isNotBlank(proc.ndice_do_Processo__c) && String.isNotEmpty(proc.ndice_do_Processo__c)){
                indice = proc.ndice_do_Processo__c + ' - ';
            }else{
                indice = ' ';
            }
            if(String.isNotBlank(proc.Grupo_P_s__c) && String.isNotEmpty(proc.Grupo_P_s__c)){
                grupo = ' - ' + proc.Grupo_P_s__c;
            }else{
                grupo = ' ';
            }
            options.add(new SelectOption(proc.Id,session + indice + institution + ' - ' + campus + grupo));
        }
        return options;
    }
    
    private void fillCampus(){
        campusItems = new List<SelectOption>();
        campusItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedEstado = apexpages.currentpage().getparameters().get('parmSelectedSelectiionEstado');
        if(selectedEstado != null && selectedEstado != ''){
            List<Campus__c> lstCampus = [Select c.Id, c.Name, c.Institui_o__c From Campus__c c where 
                                         c.Estado__c = :selectedEstado and
                                         c.Vis_vel_na_Ficha_de_Inscri_o__c = true];
          
            for(Campus__c c : lstCampus){
               
                if(c.id != 'a0PA000000BXUYV'){
                    Institui_o__c i = [Select c.Name From Institui_o__c c where c.Id =: c.Institui_o__c limit 1];
                    string instituicao = i.Name;
                    c.Name = 'IBMEC' + ' - ' + c.Name;
                }
                
                if(c.id.equals('a0PA0000007bDQ7')||c.id.equals('a0PA0000007bDQl')||c.id.equals('a0PA0000007bDQR')||c.id.equals('a0PA000000F3OIZ')||c.id.equals('a0PA000000F3OIU')||c.id.equals('a0PA000000F3OGs')||c.id.equals('a0PA000000F3OHM')||c.id.equals('a0PA000000FiNao')||c.id.equals('a0PA000000F3EIG'))
                    campusItems.add(new SelectOption(c.Id, c.Name));
            }
       
        }
    }
    
    private void fillProcess(){
        processItems = new List<SelectOption>();
        processItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedCampus = apexpages.currentpage().getparameters().get('parmSelectedCampus');
        
        if(selectedCampus != null && selectedCampus != ''){
            List<Processo_seletivo__c> lstProcess = [Select p.Id, p.Name From Processo_seletivo__c p where  
                                                     p.Campus__c = :selectedCampus and
                                                     p.Campus__c != null and 
                                                     p.Periodo_Letivo__c != null and 
                                                     (p.Tipo_de_Matr_cula__c = 'Pós-Graduação IBMEC' or (p.Tipo_de_Matr_cula__c = 'Pós-Graduação' and p.Campus__c = 'a0PA000000FiNao')) and
                                                     p.Ativo__c = true and
                                                     p.Data_de_encerramento__c >= :system.today()
                                                     ORDER BY p.Campus__c ASC];
            for(Processo_seletivo__c p : lstProcess){
                processItems.add(new SelectOption(p.Id, p.Name));
            }
        }
    }
    
    private void fillPrograms(){
        programsItems = new List<SelectOption>();
        programsItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
        if(selectedProcess != null && selectedProcess != ''){
            List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where
                                         o.Processo_seletivo__c = :selectedProcess and
                                         o.Vagas_int__c > 0];
            for(Oferta__c o : lstOffers){
                programsItems.add(new SelectOption(o.Id, o.Name));
            }
        }
    }
    
    private void fillScheduling(){
        schedulingItems = new List<SelectOption>();
        schedulingItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
        
        List<Processo_seletivo__c> lstProcess = [Select p.Id, p.Name, p.Tipo_de_Matr_cula__c, p.Institui_o_n__c, p.Campus__c From Processo_seletivo__c p where  
                                                 p.ID = :selectedProcess and
                                                 (p.Tipo_de_Matr_cula__c = 'Pós-Graduação IBMEC' or p.Tipo_de_Matr_cula__c = 'Pós-Graduação') limit 1];      
        
        if((lstProcess[0].Tipo_de_Matr_cula__c == 'Pós-Graduação IBMEC' || lstProcess[0].Tipo_de_Matr_cula__c == 'Pós-Graduação') && selectedProcess != null && selectedProcess != ''){
           //recordtype referente a pós graduação ibmec.
            recordTypeId ='012A0000000nharIAA';
             // System.debug('newOpportunity.RecordTypeId    '+newOpportunity.RecordTypeId   );  
            List<Agendamento__c> lstAgendamentos = [Select a.Id, a.Name, a.Data_Hora_Inicial__c, a.Data_Hora_T_rmino__c From Agendamento__c a where
                                                    a.RecordTypeId = '012A00000012n1t' and //Tipo de registro igual a Pós-Graduação - Produção
                                                    //a.RecordTypeId = '012Z00000005CaB' and //Tipo de registro igual a Pós-Graduação - QA
                                                    a.Institui_o__c =: lstProcess[0].Institui_o_n__c and
                                                    a.Campus__c =: lstProcess[0].Campus__c and
                                                    a.Status__c = 'Aberta' and
                                                    a.Vagas__c > 0 and
                                                    a.Data_Hora_Inicial__c > :system.today()];
            
            for(Agendamento__c a : lstAgendamentos){
                String diaHora = a.Data_Hora_Inicial__c.format('dd/MM/YYYY','America/Sao_Paulo') + ' - ' + a.Data_Hora_Inicial__c.format('HH:mm','America/Sao_Paulo') + ' às ' + a.Data_Hora_T_rmino__c.format('HH:mm','America/Sao_Paulo');
                schedulingItems.add(new SelectOption(a.Id, diaHora));
            }
        }
    }
    
    private void fillInterviewDates(){
        avaliableDateItems = new List<SelectOption>();
        avaliableDateItems.add(new SelectOption('','Selecione a Data'));
        String selectedProgram = apexpages.currentpage().getparameters().get('parmSelectedProgram');
        if (selectedProgram != null && selectedProgram !=''){
            Oferta__c theProgram = [Select ID, Location_Dean__c FROM Oferta__c WHERE id =:selectedProgram];
            theDean = theProgram.Location_Dean__c;
            
            if (theDean != null) {
                AggregateResult[] groupedResults = [select Interview_Date__c from Interview_Times__c  where Interview_Date__c > YESTERDAY and  ownerId = :theDean and opportunity__c = null GROUP BY Interview_Date__c  ORDER BY Interview_Date__c ASC];
                for (AggregateResult ar : groupedResults)  {
                    Date dateObj = Date.valueOf(ar.get('Interview_Date__c'));
                    Datetime dt = datetime.newInstance(dateObj.year(), dateObj.month(),dateObj.day());
                    avaliableDateItems.add(new SelectOption(dt.format('MM/dd/yyyy'),dt.format('MM/dd/yyyy')));
                }
            }
        }
    }
    
    private void fillInterviewTimes(){
        interviewTimesOptions = new List<SelectOption>();
        interviewTimesOptions.add(new SelectOption('','Selecione a Hora'));
        String selectedDate = apexpages.currentpage().getparameters().get('parmSelectedDate');        
        if(theDean != null && selectedDate != null) {
            Date dateObj = GetDateByString(selectedDate);
            List<Interview_Times__c> lstInterviewTimes = [SELECT Start_Time__c
                                                          FROM Interview_Times__c 
                                                          WHERE Interview_Date__c = :dateObj and 
                                                          opportunity__c = null and
                                                          ownerId = :theDean];
            
            for(Interview_Times__c it : lstInterviewTimes){
                interviewTimesOptions.add(new SelectOption(it.Start_Time__c,it.Start_Time__c));
            }
        }
    }
    
    private void insertInterviewTimeRecord(Opportunity opp){
        Date dateObj = GetDateByString(selectedInterviewDate);
        List<Interview_Times__c> lstInterviewTimes = [SELECT Id 
                                                      FROM Interview_Times__c 
                                                      WHERE Interview_Date__c = :dateObj and
                                                      Start_Time__c = :selectedInterviewTime and 
                                                      opportunity__c = null and
                                                      ownerId = :theDean];
        if(lstInterviewTimes.size() > 0){
            Interview_Times__c objInterview = lstInterviewTimes[0];
            objInterview.Opportunity__c = opp.Id;
            List<Opportunity> lst = [Select o.Account.PersonContactID, o.AccountId From Opportunity o where id = :opp.Id];
            if(lst.size() >0 && lst.get(0).Account.PersonContactID != null){
                objInterview.Contact__c = lst.get(0).Account.PersonContactID;
            }
            //update objInterview;
        }                                                               
    }
    
    private Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(0)),Integer.valueOf(myDateOnly.get(1)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }
    public Boolean validaCodigo(){
        Boolean isValid = true;
        System.debug( '>>> codigoPromocional ' + codigoPromocional );
        if( !String.isEmpty( codigoPromocional ) ) {
            List<CodigoPromocional__c> codigosPromocionais = CodigoPromocionalDAO.getInstance().getCodigoPromocionalByCodigo( codigoPromocional );
            if( !codigosPromocionais.isEmpty() ) {
                List<Opportunity> opportunityList = OpportunityDAO.getInstance().getOpportunityByPromotionalCode( codigosPromocionais[0].Id );
                if(!opportunityList.isEmpty() &&  codigosPromocionais[0].Tipo_de_C_digo__c == 'Individual') {
                    ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, Label.SRM_CODIGO_PROMOCIONAL_JA_UTILIZADO );
                    Apexpages.addMessage( msg );
                    isValid = false;
                }
            } else {
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, Label.SRM_CODIGO_PROMOCIONAL_INVALIDO );
                Apexpages.addMessage( msg );
                isValid = false;
            }
        }
        return isValid;
    }
    private Boolean validaCodigoPromocional() {
        Boolean isIntegral = false;
        List<CodigoPromocional__c> codigosPromocionais = CodigoPromocionalDAO.getInstance().getCodigoPromocionalByCodigo( codigoPromocional );
        if( !codigosPromocionais.isEmpty() ) {
            List<Opportunity> opportunityList = OpportunityDAO.getInstance().getOpportunityByPromotionalCode( codigosPromocionais[0].Id );
            if( opportunityList.isEmpty() || codigosPromocionais[0].Tipo_de_C_digo__c == 'Coletivo' )
                newOpportunity.CodigoPromocional__c = codigosPromocionais[0].Id;
                isIntegral = codigosPromocionais[0].TipoDesconto__c.equals( 'Integral' ) ? true : false;
        }
        return isIntegral;
    }
}