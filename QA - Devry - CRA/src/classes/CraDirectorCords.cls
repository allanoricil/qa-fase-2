global class CraDirectorCords {
    
   public static List<DirectorCord> getDirectorCords( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
               
            List<DirectorCord> matrizesAtivas = new List<DirectorCord>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraDiretorCordenadorList').Endpoint__c,new String[]{codcoligada,ra,codFilial});
                System.debug( 'Valor da Url===>'+url);  
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraDiretorCordenadorList' ).API_Token__c );
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );
                System.debug( 'Entrada no metodo');    
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                    system.debug('serverres.getBody(): ' +  res.getBody());
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraDiretorCordenadorList / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraDiretorCordenadorList/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }

     private static List <DirectorCord>  processJsonBody(String jsonBody ) {
      
        CordResponse SR = ( CordResponse ) JSON.deserializeStrict( jsonBody, CordResponse.class );
        if( !SR.CraDiretorCordenadorListResult.isEmpty() ){
           return  SR.CraDiretorCordenadorListResult;
       }
        else{
            return null; 
        }
       
    }
  
     public Class CordResponse {
        public List<DirectorCord> CraDiretorCordenadorListResult;
    }
  
    global Class DirectorCord {
      
        public String CodColigada;
        public String CoodCodigo;
        public String DiretorCodPessoa;
        public String DiretorNome;
        public String Nome;
    }

}