@isTest
public class CRA_DataFactoryTest {
    
    private Static final Id DNA_ALUNO_ACCOUNT_RECORD_TYPE_ID = '012A00000003wENIAY';
    private Static final Id ENEM_OPPORTUNITY_RECORD_TYPE_ID = '012A0000000nhapIAA';
    private Static final Id GRADUACAO_OPPORTUNITY_RECORD_TYPE_ID = '012A0000000oGh0IAE';
	private Static final Id VESTIBULAR_AGENDADO_OPPORTUNITY_RECORD_TYPE_ID = '012A0000000nhauIAA';
    private Static final Id AGENDAMENTO_EAD_RECORD_TYPE_ID = '012A0000000VwlAIAS';
    private Static final String OPPORTUNITIES_INITIAL_STAGE = 'Inscrito';


    public static Opportunity newOpportunityVestibularAgendado(Institui_o__c instituicao,
                                                               Campus__c campus,
                                                               Per_odo_Letivo__c periodoLetivo,
                                                               Processo_Seletivo__c processoSeletivo,
                                                               Curso__c curso,
                                                               Area_Curso__c  areaCurso,
                                                               Oferta__c oferta,
                                                               Account conta){
                                                                   
    	return newOpportunity(VESTIBULAR_AGENDADO_OPPORTUNITY_RECORD_TYPE_ID, 
                              OPPORTUNITIES_INITIAL_STAGE,
                              instituicao, 
                              campus,
                              periodoLetivo,
                              processoSeletivo,
                              curso, 
                              areaCurso, 
                              oferta, 
                              conta);
    }

    public static Opportunity newOpportunityEnem(Institui_o__c instituicao,
                                                 Campus__c campus,
                                                 Per_odo_Letivo__c periodoLetivo,
                                                 Processo_Seletivo__c processoSeletivo,
                                                 Curso__c curso,
                                                 Area_Curso__c  areaCurso,
                                                 Oferta__c oferta,
                                                 Account conta){
                                                                   
        return newOpportunity(ENEM_OPPORTUNITY_RECORD_TYPE_ID, 
                              OPPORTUNITIES_INITIAL_STAGE,
                              instituicao, 
                              campus,
                              periodoLetivo,
                              processoSeletivo,
                              curso, 
                              areaCurso, 
                              oferta, 
                              conta);
    }

    public static Opportunity newOpportunityGraduacao(Institui_o__c instituicao,
                                                      Campus__c campus,
                                                      Per_odo_Letivo__c periodoLetivo,
                                                      Processo_Seletivo__c processoSeletivo,
                                                      Curso__c curso,
                                                      Area_Curso__c  areaCurso,
                                                      Oferta__c oferta,
                                                      Account conta){
                                                                   
        return newOpportunity(GRADUACAO_OPPORTUNITY_RECORD_TYPE_ID, 
                              OPPORTUNITIES_INITIAL_STAGE,
                              instituicao, 
                              campus,
                              periodoLetivo,
                              processoSeletivo,
                              curso, 
                              areaCurso, 
                              oferta, 
                              conta);
    }
    
    private static Opportunity newOpportunity (Id recordTypeId, 
                                               String stage,
                                               Institui_o__c instituicao,
                                               Campus__c campus,
                                               Per_odo_Letivo__c periodoSeletivo,
                                               Processo_Seletivo__c processoSeletivo,
                                               Curso__c curso,
                                               Area_Curso__c  areaCurso,
                                               Oferta__c oferta,
                                               Account conta){   

        Opportunity oportunidade = new Opportunity(
            name = Faker.getName(),
            accountid = conta.Id,
            stagename = stage,
            instituicao__c = instituicao.Id,
            campus_unidade__c = campus.Id,
            processo_seletivo__c = processoSeletivo.Id,
            oferta__c = oferta.Id,
            closedate = Date.newInstance(2018, 12, 21),
            recordtypeid = recordTypeId,
            Email_do_Polo__c = 'allan.oricil@harpiacloud.com.br',
            Presente__c = false
        );

        return oportunidade;
    }

    public static User newCustomerUser(Id contactId, Id ProfileId){
        User usuario = new User(
            LastName = 'Maria Suzuki',
            Alias = 'msuzuki',
            Username = '1234567@adtalem.com.br.qa',
            Email = 'felippe.ramos@harpiacloud.com.br.qa',
            CommunityNickname = 'msuzuki2',
            ProfileId = ProfileId,
            DBNumber__c = '13243236',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR',
            ContactId = contactId
        );
        return usuario;
    }
    
     public static User newStandardUser(Id ProfileId){
        User usuario = new User(
            LastName = 'Maria Suzuki',
            Alias = 'msuzuki',
            Username = '1234567@adtalem.com.br.qa',
            Email = 'felippe.ramos@harpiacloud.com.br.qa',
            CommunityNickname = 'msuzuki2',
            ProfileId = ProfileId,
            DBNumber__c = '13243236',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR'
        );
        return usuario;
    }
    
    
    public static Account newPersonAccount(){
        Id devRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Conta pessoal').getRecordTypeId();
        Account acc = new Account(
        	FirstName = 'Rica',
            LastName = 'Maria Suzuki',
            PersonEmail = 'felippe.ramos@harpiacloud.com.br.qa',
            RecordTypeId = devRecordTypeId            
        );
        return acc;
    }

    public static Account newAccountAluno(){
        Account acc = new Account(
            FirstName = Faker.getFirstName(),
            LastName = Faker.getLastName(),
            PersonEmail = Faker.getEmail(),
            CPF_2__c = '447.829.193-10',
            RecordTypeId = DNA_ALUNO_ACCOUNT_RECORD_TYPE_ID        
        );
        return acc;
    }
    
    public static Case newCase(Aluno__c aluno){
        Id devRecordTypeId = Schema.SObjectType.Case.getRecordTypeInfosByName().get('CRA - Padrão').getRecordTypeId();
        Case caso = new Case(
            Aluno__c = aluno.Id,
            RecordTypeId = devRecordTypeId
        );
        return caso;
    }
    
    public static Aluno__c newAluno(Campus__c campus, Account conta){
        Aluno__c aluno = new Aluno__c(
            Conta__c = conta.Id,
        	Campus_n__c = campus.Id,
            Name = 'Maria Suzuki',
            R_A_do_Aluno__c = '1234567',
            Codigo_SAP__c = '1234567890'
        );
        return aluno;
    }
    
    public static Campus__c newCampus(Institui_o__c instituicao){
        Campus__c campus = new Campus__c(
        	Name = Faker.getName().deleteWhitespace(),
            Institui_o__c = instituicao.Id,
            Id_Campus__c = String.valueOf(Randomizer.getNextInt(100))
        );
        return campus;
    }
    
    public static Institui_o__c newInstituicao(){
        Institui_o__c instituicao = new Institui_o__c(
            Name = 'Instituição',
            C_digo_da_Coligada__c = '1',
            Id_Institui_o__c = '1'
        );
        return instituicao;
    }
    
    public static Assunto__c newAssunto(Product2 servico){
        Assunto__c assunto = new Assunto__c(
        	Name = 'Cartão de Entrada - Emissão 2ª Via',
            Portal__c = true,
            ID_externo__c = 'Cartão de Entrada (2ª via)',
            Graduacao__c = true,
            Mestrado__c = true,
            Pos_Graduacao__c = true,
            Preparatorio__c = true,
            Tecnico__c = true,
            Extensao__c = true,
            Servico__c = servico.Id,
            Pagamento__c = true,
            Front_Office__c = true,
            Tipo_de_Registro_do_caso__c = 'CRA - Padrão'
        );
        return assunto;
    }
    
    public static Detalhe_do_assunto__c newDetalhe(Assunto__c assunto){
        Date inicio = Date.today();
        Date fim = Date.today();
        fim.addYears(1);
        Detalhe_do_assunto__c detalhe = new Detalhe_do_assunto__c(
            Assunto__c = assunto.Id,
            Fim_de_Vigencia__c = inicio,
            Inicio_de_Vigencia__c = fim
        );
        return detalhe;
    }
    
    public static Motivo_de_Abertura__c newMotivo(Assunto__c assunto){
        Motivo_de_Abertura__c motivo = new Motivo_de_Abertura__c(
        	Name = 'Motivo teste',
            Assunto__c = assunto.Id
        );
        return motivo;
    }
    
    public static Product2 newProduct(){
        Product2 produto = new Product2(
        	Name = 'Cartão de Entrada - Emissão 2ª Via',
            ProductCode = 'SF'
        );
        return produto;
    }
    
    public static Pricebook2 newPricebook(){
        Pricebook2 pricebook = new Pricebook2(
        	Name = 'Catálogo 1'
        );
        return pricebook;
    }
    
    public static PricebookEntry newPBEntry(Product2 produto, Id pricebookID){
        PricebookEntry pbentry = new PricebookEntry(
            Pricebook2Id = pricebookID,
            Product2Id = produto.Id,
            UnitPrice = 20,
            isActive = true,
            UseStandardPrice = false
        );
        return pbentry;
    }
    
    public static Entitlement newEntitlement(){
        Entitlement direito = new Entitlement(
            Name = 'Nome do direito',
            IsPerIncident = false            
        );
        return direito;
    }
    
    public static Cobranca__c newCharging(Id caseId){
        Cobranca__c cobranca = new Cobranca__c(
			Caso__c = caseId,
            DataVencimento__c = date.today(),
            Data_Limite_Pagamento__c = date.today(),
            Status__c = 'Em Aberto',
            Valor__c = 20
            
        );
        return cobranca;
    }

    public static Lead newLead(){
        Lead lead = new Lead(
            FirstName = 'Rica',
            LastName = 'Mastrocolla',
            Email = 'email@email.com',
            MobilePhone = '11999999999',
            Status = 'Novo',
            Canal_de_Origem__c = 'E-mail'
        );
        return lead;
    }

    public static Curso__c newCurso(Institui_o__c instituicao, Campus__c campus){
        Curso__c curso =  new Curso__c(
            Name = 'test Curso',
            Institui_o_n__c = instituicao.Id,
            Campus__c = campus.Id,
            Tipo_do_Curso__c = 'Graduação',
            Status__c = 'Ativo',
            Id_Habilitacao_Filial__c = '123456'
        );
        return   curso;
    }

    public static Processo_Seletivo__c newProcesso(Institui_o__c instituicao, Campus__c campus, Per_odo_Letivo__c periodo){
        Processo_Seletivo__c processo = new Processo_Seletivo__c(
            Name = 'processo test',
            Institui_o_n__c = instituicao.Id,
            Campus__c = campus.Id,
            Tipo_de_Processo_n__c = 'Vestibular',
            //Tipo_de_Processo_n__c = 'Seleção',
            Tipo_de_Matr_cula__c = 'Vestibular Tradicional',
            //Tipo_de_Matr_cula__c = 'PROUNI',
            //Tipo_de_Matr_cula__c = 'ENEM',
            //Tipo_de_Matr_cula__c = 'Transferência Externa',
            //Tipo_de_Matr_cula__c = 'Portador de Diploma',
            //Tipo_de_Matr_cula__c = 'Vestibular Especial',
            //Tipo_de_Matr_cula__c = 'Entrevista',
            //Tipo_de_Matr_cula__c = 'EnglishPRO',
            //Tipo_de_Matr_cula__c = 'Retorno MI',
            //Tipo_de_Matr_cula__c = 'Reingresso de novato',
            //Tipo_de_Matr_cula__c = 'FIES',
            //Tipo_de_Matr_cula__c = 'Processos Seletivos Internacionais',
            Niveis_de_Ensino__c = 'Graduação',
            //Niveis_de_Ensino__c = 'Sequencial',
            //Niveis_de_Ensino__c = 'Pós-Graduação',
            //Niveis_de_Ensino__c = 'Extensão',
            //Niveis_de_Ensino__c = 'Técnico',
            //Niveis_de_Ensino__c = 'Mestrado',
            //Niveis_de_Ensino__c = 'Idiomas',
            //Niveis_de_Ensino__c = 'Pós-Graduação IBMEC',
            //Niveis_de_Ensino__c = 'Soluções Corporativas',
            ndice_do_Processo__c = 'A',
            PeriodoLetivo__c = periodo.Id,
            Status_Processo_Seletivo__c = 'Planejado',
            CobrarTaxaInscricao__c = true,
            Tipo_do_curso_oferecido__c = 'Graduação',
            //Tipo_do_curso_oferecido__c = 'Pós Graduação',
            //Tipo_do_curso_oferecido__c = 'Preparatório',
            //Tipo_do_curso_oferecido__c = 'Extensão',
            //Tipo_do_curso_oferecido__c = 'Mestrado',
            Precisa_de_prova__c = true,
            Precisa_de_entrevista__c = false,
            Data_de_in_cio__c = Date.newInstance( 2017, 12, 01),
            Data_Aprova_Classificaveis__c = Date.newInstance( 2018, 12, 01),
            Data_de_Inicio_das_Aulas__c = Date.newInstance( 2018, 11, 30),
            Data_Validade_Processo__c = Date.newInstance( 2018, 11, 25),
            Data_do_Vestibular__c = Date.newInstance( 2018, 11, 26),
            Data_de_encerramento__c = Date.newInstance( 2018, 11, 29),
            Valor_Taxa_Inscricao__c = 50,
            Vencimento_Boleto_dias__c = 2,
            Instrucoes_Boleto__c = 'descrição test',
            Tipo_de_Processo__c = 'Graduação',
            C_digo_Processo_Seletivo__c = '123',
            ID_Academus_ProcSel__c = '1234'
        );
        return processo;
    }

    public static Per_odo_Letivo__c newPeriodo(){
        Per_odo_Letivo__c periodo = new Per_odo_Letivo__c(
            Name = '2018.1',
            Status_do_Per_odo_Letivo__c = 'Ativo',
            In_cio_das_aulas_do_pr_ximo_per_odo__c = Date.newInstance(2018, 01, 01),
            CodPeriodo__c = '123',
            IdPeriodo__c = '1234'
        );
        return periodo;
    }

    public static Oferta__c newOferta(Institui_o__c instituicao, Campus__c campus, Per_odo_Letivo__c periodo, Curso__c curso, Area_Curso__c areaCurso, Processo_Seletivo__c processo){
        Oferta__c oferta = new Oferta__c(
            Name = 'oferta test',
            Institui_o_n__c = instituicao.Id,
            Campus__c = campus.Id,
            Tipo_do_Curso__c = 'Graduação',
            //Tipo_do_Curso__c = 'Sequencial',
            //Tipo_do_Curso__c = 'Pós-Graduação',
            //Tipo_do_Curso__c = 'Extensão',
            //Tipo_do_Curso__c = 'Técnico',
            //Tipo_do_Curso__c = 'Mestrado',
            //Tipo_do_Curso__c = 'Idiomas',
            //Tipo_do_Curso__c = 'Pós-Graduação IBMEC',
            //Tipo_do_Curso__c = 'Soluções Corporativas',
            Processo_seletivo__c = processo.Id,
            M_todo_do_Curso__c = 'Presencial',
            Curso__c = curso.Id,
            Vagas__c = '10',
            rea_Curso__c = areaCurso.Id,
            ID_Academus_Oferta__c = '1234',
            C_digo_da_Oferta__c = '123'      
        );
        return oferta;
    }

    public static Area_Curso__c newAreaCurso(){
        Area_Curso__c area = new Area_Curso__c(
            Name = 'area test'
        );
        return area;
    }

    public static Lead newLead(Institui_o__c instituicao, Campus__c campus, Per_odo_Letivo__c periodo, Processo_Seletivo__c processo){
        Lead lead = new Lead(
            FirstName = 'Lipe',
            LastName = 'Lippinho',
            Email = 'lipelipinho5@email.com',
            CPF__c = '593.861.233-04',
            MobilePhone = '11999999999',
            Phone = '1122222222',
            Data_de_Nascimento__c = Date.newInstance(1990, 01, 01),
            Instituicao__c = instituicao.Id,
            Campus_LookUp__c = campus.Id,
            Processo_seletivo__c = processo.Id,
            Produto__c = 'Graduação',
            Status = 'Novo',
            Ciclo__c = periodo.Id,
            Metodo_do_curso__c = 'Presencial',
            Canal_de_Origem__c = 'Email'
        );
        return lead;
    }

    public static Agendamento__c newAgendamentoEAD(Institui_o__c instituicao, Campus__c campus, Polo__c polo, Integer daysToAdd){
        Agendamento__c agendamento = new Agendamento__c(
            RecordTypeId = AGENDAMENTO_EAD_RECORD_TYPE_ID,
            Status__c = 'Aberta',
            Capacidade__c = 10,
            Institui_o__c = instituicao.Id,
            Campus__c = campus.Id,
            Polo__c = polo.Id,
            Hor_rio__c = '08:00 - 10:00;10:00 - 12:00;13:00 - 15:00',
            Dia__c = Date.today().addDays(daysToAdd)
        );
        return agendamento;
    }

    public static Polo__c newPolo(Institui_o__c instituicao, String nome){
        Polo__c polo = new Polo__c(
            Instituicao__c = instituicao.Id,
            Name = nome
        );
        return polo;
    }
}