public with sharing class ProjectOrganizerController {

	private Static final Integer INITIAL_NUMBER_OF_ITENS_FOR_THE_BULK_OPERATION = 3;
	private List<String> metadataTypes;

	public List<Metadado_do_Projeto__c> metadataToInsert {get;set;}
	public Boolean isBulkRegistration {get;set;}
	public List<SelectOption> metadataOptions {get; private set;}
	public List<SelectOption> projectOptions {get; private set;}
	public List<SelectOption> objectOptions {get; private set;}
	public String selectedProject {get;set;}
	public String selectedMetadata {get;set;}
	public String selectedObject {get;set;}
	public String metadataName {get;set;}
	public String apiName {get;set;}
	public Boolean itFailed {get;set;}
	public Boolean itSucced {get;set;}
	public Boolean isObjectNecessary {get;set;}
	public Boolean renderPackageXml {get;set;}
	public List<String> errorMessages {get;set;}
	public List<String> packageXml{get;set;}
	public Map<String, Id>  projectsUniqueNameIdMap;
	public Map<String, String>  projectsUniqueNameNameMap;
	public Map<String, String>  projectsUniqueNameApiVersionMap;
	public Boolean showRegistrationDataContainer {get;set;}

	public ProjectOrganizerController(ApexPages.StandardController stdController) {
		metadataTypes = Metadata.getMetadataTypes();
		metadataToInsert = createMetadataElementsToBulkOperation();
		projectOptions = buildProjectPickList();
		metadataOptions = buildMetadataPickList();
		objectOptions = buildObjectsPickList();
		isObjectNecessary = false;
		itSucced = false;
		renderPackageXml = false;
		isBulkRegistration = false;
		showRegistrationDataContainer = true;
		packageXml = new List<String>();
	}

	private List<Metadado_do_Projeto__c> createMetadataElementsToBulkOperation(){
		List<Metadado_do_Projeto__c> metadataToInsert = new List<Metadado_do_Projeto__c>();
		for(Integer i = 0; i < INITIAL_NUMBER_OF_ITENS_FOR_THE_BULK_OPERATION; i++){
			metadataToInsert.add(new Metadado_do_Projeto__c());
		}
		return metadataToInsert;
	}

	public void setToBulkRegistrationMode(){
		isBulkRegistration = true;
		renderPackageXml = false;
		showRegistrationDataContainer = true;
	}

	public void setToSingleRegistrationMode(){
		isBulkRegistration = false;
		renderPackageXml = false;
		showRegistrationDataContainer = true;
	}

	public void viewProjectsProjectXml(){
		packageXml = createPackageXmlText();
		renderPackageXml = true;
		showRegistrationDataContainer = false;
	}
	
	public void addLineToBulkRegistrationMode(){
		metadataToInsert.add(new Metadado_do_Projeto__c());
	}

	public void removeLineFromBulkRegistrationMode(){
		if(!metadataToInsert.isEmpty()){
			metadataToInsert.remove(metadataToInsert.size()-1);
		}
	}

	public PageReference registerMultipleMetadataElements(){
		Id projectId = projectsUniqueNameIdMap.get(projectsUniqueNameNameMap.get(selectedProject));
		for(Metadado_do_Projeto__c metadataElement: metadataToInsert){
			metadataElement.Projeto__c = projectId;

			if(metadataElement.object__c == 'true' || metadataElement.object__c == 'false'){
				metadataElement.object__c = null;
			}
		}

		try{
			insert metadataToInsert;
			itFailed = false;
			itSucced = true;
			packageXml = createPackageXmlText();
			savePackageXmlInTheProject(projectId, 
									   projectXmlListToText(packageXml));
			renderPackageXml = true;
			metadataToInsert = createMetadataElementsToBulkOperation();
		}catch(Exception ex){
			errorMessages = new List<String>();

			if(String.isBlank(selectedProject)){
				errorMessages.add('Escolha um projeto!');
			}

			Integer lineNumber = 1;
			for(Metadado_do_Projeto__c metadataElement: metadataToInsert){
				if(String.isBlank(metadataElement.metadata_type__c)){ 
					errorMessages.add('Escolha um metadado para a Linha!' + lineNumber);
				}

				if(String.isBlank(metadataElement.Name)){ 
					errorMessages.add('O nome do metadado não pode estar vazio para a Linha!' + lineNumber);
				}

				if(String.isBlank(metadataElement.api_name__c)){ 
					errorMessages.add('O nome da API não pode estar vazio para a Linha!' + lineNumber);
				}

				if(metadataElement.object__c == 'true' && String.isBlank(metadataElement.object__c)){
					errorMessages.add('É obrigatório selecionar o objeto para esse tipo de Metadado para a Linha!' + lineNumber);
				}

				if(errorMessages.isEmpty()){
					errorMessages.add(ex.getMessage());
				}
				lineNumber++;
			}
			itFailed = true;
			return null;
		}
		return null;
	}

	public PageReference registerNewMetadata(){
		Metadado_do_Projeto__c projectElement = new Metadado_do_Projeto__c();
		projectElement.Name = metadataName;
		projectElement.metadata_type__c = selectedMetadata;
		projectElement.api_name__c = apiName;
		projectElement.Projeto__c = projectsUniqueNameIdMap.get(projectsUniqueNameNameMap.get(selectedProject));
		
		if(isObjectNecessary){
			projectElement.object__c = selectedObject;
		}

		try{
			insert projectElement;
			itFailed = false;
			itSucced = true;
			packageXml = createPackageXmlText();
			savePackageXmlInTheProject(projectElement.Projeto__c, 
									   projectXmlListToText(packageXml));
			renderPackageXml = true;
		}catch(Exception ex){
			errorMessages = new List<String>();

			if(String.isBlank(selectedProject)){
				errorMessages.add('Escolha um projeto!');
			}

			if(String.isBlank(selectedMetadata)){ 
				errorMessages.add('Escolha um metadado!');
			}

			if(String.isBlank(metadataName)){ 
				errorMessages.add('O nome do metadado não pode estar vazio!');
			}

			if(String.isBlank(apiName)){ 
				errorMessages.add('O nome da API não pode estar vazio!');
			}

			if(isObjectNecessary && String.isBlank(selectedObject)){
				errorMessages.add('É obrigatório selecionar o objeto para esse tipo de Metadado!');
			}

			if(errorMessages.isEmpty()){
				errorMessages.add(ex.getMessage());
			}
			itFailed = true;
			return null;
		}
		return null;
	}

	private void savePackageXmlInTheProject(Id projectId, String packageXml){
		Project__c project = [SELECT Id,
									 Package_XML__c	
							    FROM Project__c
							   WHERE Id =:projectId];
		project.Package_XML__c = packageXml;
		update project;
	}

	private String projectXmlListToText(List<String> projectXmlList){
		String projectXml ='';
		for(String line:projectXmlList){
			projectXml+=line;
		}
		return projectXml;
	}

	public List<String> createPackageXmlText(){
		Map<String, List<String>> metadataTypesMap = separateProjectMetadataElements();
		List<String> packageXml = new List<String>();
		packageXml.add('<?xml version="1.0" encoding="UTF-8"?>');
		packageXml.add('<Package xmlns="http://soap.sforce.com/2006/04/metadata">');
		for(String metadataType: metadataTypesMap.keySet()){
			List<String> elementsForThisType = metadataTypesMap.get(metadataType);
			if(!elementsForThisType.isEmpty()){
				packageXml.add('	<types>');
				for(String element : elementsForThisType){
					packageXml.add('		' + element);
				}
				packageXml.add('		<name>'+ metadataType +'</name>');
				packageXml.add('	</types>');
			}
		}
		packageXml.add('	<version>'+ projectsUniqueNameApiVersionMap.get(projectsUniqueNameNameMap.get(selectedProject)) +'</version>');
		packageXml.add('</Package>');
		return packageXml;	
	}

	private Map<String, List<String>> separateProjectMetadataElements(){
		List<Metadado_do_Projeto__c> projectMetadataElements = [SELECT Projeto__c,
																	   package_code__c,
																       metadata_type__c
																  FROM Metadado_do_Projeto__c
																 WHERE Projeto__c=:projectsUniqueNameIdMap.get(projectsUniqueNameNameMap.get(selectedProject))];

		Map<String, List<String>> metadataTypesMap = new Map<String, List<String>>();
		for(String metadataType : metadataTypes){
			List<String> elements = new List<String>();
			for(Metadado_do_Projeto__c metadataElement : projectMetadataElements){
				if(metadataElement.metadata_type__c.equals(metadataType)){
					elements.add(metadataElement.package_code__c);
				}
			}
			metadataTypesMap.put(metadataType, elements);
		}

		return metadataTypesMap;
	}


	public void checkIfItIsAMetadataDependentOfAnObjectForBulkRegistration(){
		for(Metadado_do_Projeto__c metadataElement: metadataToInsert){
			if(metadataElement.metadata_type__c != null && itIsObjectDependent(metadataElement.metadata_type__c)){
				metadataElement.object__c = 'true';
			}else{
				metadataElement.object__c = 'false';
			}
		}
	}

	public void checkIfItIsAMetadataDependentOfAnObject(){
		if(itIsObjectDependent(selectedMetadata)){
			isObjectNecessary = true;
		}else{
			isObjectNecessary = false;
		}
	}

	private Boolean itIsObjectDependent(String metadataType){
		if(metadataType.equals('CompactLayout') ||
   		   metadataType.equals('CustomField') ||
           metadataType.equals('WebLink') ||
           metadataType.equals('BusinessProcess') ||
           metadataType.equals('Layout') ||
           metadataType.equals('RecordType')||
           metadataType.equals('WorkflowAlert')||
           metadataType.equals('ValidationRule') ||
           metadataType.equals('AssignmentRule')){
			return true;
		}else{
			return false;
		}
	}

	private List<SelectOption> buildObjectsPickList(){
       	Map<String, Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();
        List<String> options = new List<String>();
		for(String sObjectName : globalDescribe.keyset()){
           options.add(stringFirstLetterToUpperCase(sObjectName));
		}
		options.add('PersonAccount');
        SortUtil.sortStringList(options);
		return buildPickList(options);
	}

	private String stringFirstLetterToUpperCase(String stringToChange){
		return stringToChange.substring(0,1).toUpperCase() + stringToChange.substring(1,stringToChange.length());
	}

	private List<SelectOption> buildProjectPickList(){
		List<Project__c> projects =[SELECT Id,
									       Name,
									       unique_name__c,
									       api_version__c
		                              FROM Project__c];
		projectsUniqueNameIdMap = new Map<String, Id>();
		projectsUniqueNameNameMap = new Map<String, String>();
		projectsUniqueNameApiVersionMap = new Map<String, String>();
		List<String> projectNames = new List<String>();
		for(Project__c project: projects){
			projectNames.add(project.Name);
			projectsUniqueNameIdMap.put(project.unique_name__c,project.Id);
			projectsUniqueNameNameMap.put(project.Name, project.unique_name__c);
			projectsUniqueNameApiVersionMap.put(project.unique_name__c, project.api_version__c);
		}				            
		return buildPickList(projectNames);
	}

	private List<SelectOption> buildMetadataPickList(){
		return buildPickList(metadataTypes);
	}

	private List<SelectOption> buildPickList(List<String> names){
		List<SelectOption> options = new List<SelectOption>();
		for(String name: names){
			options.add(new SelectOption(name, name));
		}
		return options;
	}
}