/*
    @author Rafael Magalhães
    @class Classe controladora da pagina de pesquisa de aluno
*/

public with sharing class Checklist_ProuniFiesInscricaoController{
	public List<PesquisaTO> inscricoes { get; set; }
    public String cpf { get; set; }
    public String processo { get; set; }
    
	/*
        Construtor
    */
	public Checklist_ProuniFiesInscricaoController() {}
    
	/*
        Consulta Inscrições
        @action Consultar 
    */
    public void consultaInscricoes() {
    	if(cpf.length()==0) {
    		ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.info, 'É necessario informar o tipo de Processo Seletivo e o CPF do canditado.' );
            Apexpages.addMessage( msg );
            return;
    	}

        this.inscricoes = new List<PesquisaTO>(); 

        String queryString = '';
 
        if(queryString == ''){
            /*queryString = 'SELECT Id, Account.Name, Processo_seletivo__r.Name, ' + 
                'Processo_seletivo__r.Nome_da_IES__c, Data_Nascimento__c, StageName ' + 
                'FROM Opportunity WHERE ' + ' Account.CPF_2__c = ' + '\'' + cpf + '\' AND Tipo_de_Matr_cula__c = ' + '\'' + processo + '\' ORDER BY CreatedDate DESC LIMIT 1' ;
                */
            queryString = 'Select Id,OportunidadeID__c,AccountName__c,ProcessoSeletivoName__c,ProcessoSeletivoIES__c,CPF__c,CreatedDate__c ' +
            'from ChecklistBase__c where CPF__c = ' + '\'' + cpf + '\' and TipoMatricula__c = ' + '\'' + processo + '\' order by CreatedDate__c desc limit 1';
        }

        List<ChecklistBase__c> lista = Database.query( queryString );
        
        for( ChecklistBase__c opp : lista)  
        	this.inscricoes.add( new PesquisaTO( opp ) );

        if(inscricoes.isEmpty() ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.info, 'Nenhum registro encontrado!' );
            Apexpages.addMessage( msg );
        }
    }

    /*
		Objeto retorno de pesquisa
    */
    public class PesquisaTO {
        public String opportunityId         { get; private set; }
    	public String candidato 			{ get; private set; }
    	public String processoSeletivo 		{ get; private set; }
    	public String instituicaoEnsino 	{ get; private set; }

    	/*
        	Construtor 
    	*/
    	public PesquisaTO( ChecklistBase__c opp ) {  
    		opportunityId		= opp.Id;	 
    		candidato 			= opp.AccountName__c;
    		processoSeletivo 	= opp.ProcessoSeletivoName__c;
    		instituicaoEnsino 	= opp.ProcessoSeletivoIES__c;
    	}
    }
}