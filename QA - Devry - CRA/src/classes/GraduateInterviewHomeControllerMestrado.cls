/*******************************************************************
Author    :   Appirio
Date      :   November 2012
Purpose   :   Controller Class for page GraduateInterviewHome
*******************************************************************/

public with sharing class GraduateInterviewHomeControllerMestrado {
  /* member variables */
  public Opportunity newOpportunity{get;set;}
  public string selectedSelectionProcess {get;set;}
  public string selectedPrograms {get;set;}
  public string selectedInterviewDate {get;set;}
  public string selectedInterviewTime {get;set;}
  public Id theDean = null;
  public List<SelectOption> programsItems {get;set;}
  public List<SelectOption> avaliableDateItems {get;set;}
  public List<SelectOption> interviewTimesOptions {get;set;}
  public List<SelectOption> getCountriesSelectList() {
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('', 'Selecionar uma Opção'));        
    Map<String, Countries__c> countries = Countries__c.getAll();
    List<String> countryNames = new List<String>();
    countryNames.addAll(countries.keySet());
    countryNames.sort();
    for (String countryName : countryNames) {
      Countries__c country = countries.get(countryName);
      options.add(new SelectOption(country.Name, country.Name));
    }
    return options;
  }
  public String pageLanguage{
    get{
      String lang = ApexPages.currentPage().getParameters().get('lang');
      if( lang != null && lang.length() > 0){
        return lang;
      }
      return 'pt_BR';
    }
    set;
  }
  
  /* constructor */
  public GraduateInterviewHomeControllerMestrado (){
    newOpportunity = new Opportunity(); 
    fillPrograms();
    //fillInterviewDates();
    //fillInterviewTimes(); 
  }
  
  /* page refs */
  public Pagereference OnchangeSelectionProcess(){
    fillPrograms();
    //fillInterviewDates();
    //fillInterviewTimes();
    return null;
  }
  
  public Pagereference OnchangePrograms(){
    //fillInterviewDates();
    //fillInterviewTimes();
    return null;
  }
  
  public Pagereference OnchangeInterviewDate(){
    //fillInterviewTimes();
    return null;
  }
  
    public Pagereference save(){
    try{
      if(selectedSelectionProcess != null && selectedSelectionProcess != '')
        newOpportunity.Processo_seletivo__c = selectedSelectionProcess;
      if(selectedPrograms != null && selectedPrograms != '')
        newOpportunity.oferta__c = selectedPrograms;
      try{
        insert newOpportunity;
        System.debug('>>>>Opportunity inserted successfully.'+newOpportunity.Id+' Account:'+ +newOpportunity.AccountId);
      } catch(Exception exInner){
        System.debug('>>>>Exception in inserting opprotunity'+exInner.getMessage());
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, exInner.getMessage()));
        return null;
      }
        
      //insertInterviewTimeRecord(newOpportunity);
      newOpportunity = new Opportunity();
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  System.Label.Interview_Message));
    } catch(Exception ex){
      System.debug('>>>>Exception in Save'+ex.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
    }
    return null;
  }
    
  public PageReference resetForm() {
    PageReference newpage = new PageReference(System.currentPageReference().getURL());
    newpage.setRedirect(true);
    return newpage;
  }
  
  /* helper methods */  
  public List<SelectOption> getAllSelectionProcessItems(){
    List<SelectOption> options = new List<SelectOption>();
    options.add(new SelectOption('','Selecionar uma Opção'));
    List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Nome_da_IES__c,p.name, p.Nome_do_Campus__c, 
                                                    p.Periodo_Letivo__c,p.Sess_o_Ciclo__c,Grupo_P_s__c, ndice_do_Processo__c
                                             FROM Processo_seletivo__c p  
                                             WHERE p.Campus__c != null and 
                                                   p.Periodo_Letivo__c != null and 
                                                   p.Tipo_de_Matr_cula__c = 'Mestrado' and
                                                   p.Ativo__c = true and
                                                   p.Data_de_encerramento__c >= :system.today()
                                             ORDER BY p.Campus__c ASC];
    for (Processo_seletivo__c proc : lstProcess)  {
      String campus = proc.Nome_do_Campus__c;
      String institution = proc.Nome_da_IES__c;
      String session;
      String indice;
      String grupo;
    if(String.isNotBlank(proc.Sess_o_Ciclo__c) && String.isNotEmpty(proc.Sess_o_Ciclo__c)){
      session = proc.Sess_o_Ciclo__c;
      }else{
      session = ' ';
      }
    if(String.isNotBlank(proc.ndice_do_Processo__c) && String.isNotEmpty(proc.ndice_do_Processo__c)){
      indice = proc.ndice_do_Processo__c + ' - ';
      }else{
      indice = ' ';
      }
    if(String.isNotBlank(proc.Grupo_P_s__c) && String.isNotEmpty(proc.Grupo_P_s__c)){
      grupo = ' - ' + proc.Grupo_P_s__c;
      }else{
      grupo = ' ';
      }
      options.add(new SelectOption(proc.Id,session + indice + institution + ' - ' + campus + grupo));
    }
    return options;
  }
    
  private void fillPrograms(){
    programsItems = new List<SelectOption>();
    programsItems.add(new SelectOption('','Selecionar uma Opção'));
    String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
    if(selectedProcess != null && selectedProcess != ''){
      List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where  o.Processo_seletivo__c = :selectedProcess and o.Vagas_int__c > 0];
      for(Oferta__c o : lstOffers){
        programsItems.add(new SelectOption(o.Id, o.Name));
      }
    }
  }
/*      
  private void fillInterviewDates(){
    avaliableDateItems = new List<SelectOption>();
    avaliableDateItems.add(new SelectOption('','Selecione a Data'));
    String selectedProgram = apexpages.currentpage().getparameters().get('parmSelectedProgram');
    if (selectedProgram != null && selectedProgram !=''){
      Oferta__c theProgram = [Select ID, Location_Dean__c FROM Oferta__c WHERE id =:selectedProgram];
      theDean = theProgram.Location_Dean__c;
    
      if (theDean != null) {
        AggregateResult[] groupedResults = [select Interview_Date__c from Interview_Times__c  where Interview_Date__c > YESTERDAY and  ownerId = :theDean and opportunity__c = null GROUP BY Interview_Date__c  ORDER BY Interview_Date__c ASC];
        for (AggregateResult ar : groupedResults)  {
          Date dateObj = Date.valueOf(ar.get('Interview_Date__c'));
          Datetime dt = datetime.newInstance(dateObj.year(), dateObj.month(),dateObj.day());
          avaliableDateItems.add(new SelectOption(dt.format('MM/dd/yyyy'),dt.format('MM/dd/yyyy')));
        }
      }
    }
  }
  
  private void fillInterviewTimes(){
    interviewTimesOptions = new List<SelectOption>();
    interviewTimesOptions.add(new SelectOption('','Selecione a Hora'));
    String selectedDate = apexpages.currentpage().getparameters().get('parmSelectedDate');        
    if(theDean != null && selectedDate != null) {
      Date dateObj = GetDateByString(selectedDate);
      List<Interview_Times__c> lstInterviewTimes = [SELECT Start_Time__c
                                                    FROM Interview_Times__c 
                                                    WHERE Interview_Date__c = :dateObj and 
                                                          opportunity__c = null and
                                                          ownerId = :theDean];
            
      for(Interview_Times__c it : lstInterviewTimes){
        interviewTimesOptions.add(new SelectOption(it.Start_Time__c,it.Start_Time__c));
      }
    }
  }

  private void insertInterviewTimeRecord(Opportunity opp){
    Date dateObj = GetDateByString(selectedInterviewDate);
    List<Interview_Times__c> lstInterviewTimes = [SELECT Id 
                                                  FROM Interview_Times__c 
                                                  WHERE Interview_Date__c = :dateObj and
                                                        Start_Time__c = :selectedInterviewTime and 
                                                        opportunity__c = null and
                                                        ownerId = :theDean];
    if(lstInterviewTimes.size() > 0){
      Interview_Times__c objInterview = lstInterviewTimes[0];
      objInterview.Opportunity__c = opp.Id;
      List<Opportunity> lst = [Select o.Account.PersonContactID, o.AccountId From Opportunity o where id = :opp.Id];
      if(lst.size() >0 && lst.get(0).Account.PersonContactID != null){
        objInterview.Contact__c = lst.get(0).Account.PersonContactID;
      }
      update objInterview;
    }                                                               
  }
*/ 
  private Date GetDateByString(String s){
    String[] myDateOnly = s.split('/');
    Date dateObj;
    try{
      dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(0)),Integer.valueOf(myDateOnly.get(1)));
    } catch(exception ex) {
      dateObj = date.today();
    }
    return dateObj;
  }   
}