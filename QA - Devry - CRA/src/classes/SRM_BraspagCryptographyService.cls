/*
    @author Diego Moreira
    @class Classe de serviço Braspag de criptografia ou descriptografia de dados
*/
public class SRM_BraspagCryptographyService {
	/*
        Singleton
    */
    private static final SRM_BraspagCryptographyService instance = new SRM_BraspagCryptographyService();    
    private SRM_BraspagCryptographyService(){}
    
    public static SRM_BraspagCryptographyService getInstance() {
        return instance;
    }

    /*
		Chama serviço BrasPag para criptografia de dados
		@param merchantId Id da loja 
		@param parameters lista de parametros para criptografia 
    */ 
    public String braspagEncrypt( String merchantId, List<String> parameters ) {
        /*
    	SRM_WSBraspagCryptography.ArrayOfString arrayString = new SRM_WSBraspagCryptography.ArrayOfString();
		arrayString.string_x = parameters;

    	SRM_WSBraspagCryptography.BraspagGeneralServiceSoap request = new SRM_WSBraspagCryptography.BraspagGeneralServiceSoap();
		request.timeout_x = 60000;

		return request.EncryptRequest( merchantId, arrayString );
        */
        SRM_WSDevryCryptography.ArrayOfString arrayString = new SRM_WSDevryCryptography.ArrayOfString();
        arrayString.string_x = parameters;

        SRM_WSDevryCryptography.DecoratorBraspagGeneralServiceSoap request = new SRM_WSDevryCryptography.DecoratorBraspagGeneralServiceSoap();
        request.timeout_x = 60000;

        return request.EncryptRequest( merchantId, arrayString );        
    }

    /*
		Chama serviço BrasPag para descriptografia de dados
		@param merchantId Id da loja 
		@param cryptString String contendo os dados criptografados
		@param parameters lista de parametros para criptografia 
    */
    public String[] braspagDecrypt( String merchantId, String cryptString, List<String> parameters ) {
        /*
    	SRM_WSBraspagCryptography.ArrayOfString customFields = new SRM_WSBraspagCryptography.ArrayOfString();
		customFields.string_x = parameters;

    	SRM_WSBraspagCryptography.BraspagGeneralServiceSoap request = new SRM_WSBraspagCryptography.BraspagGeneralServiceSoap();
		request.timeout_x = 60000; 

		SRM_WSBraspagCryptography.ArrayOfString arrayResult = request.DecryptRequest( merchantId, cryptString, customFields );
		return arrayResult.string_x;
        */
        Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
        String authToken = mapToken.get( '200' ) != null ? mapToken.get( '200' ) : '';
        String apiToken = WSSetup__c.getValues( 'Token' ).API_Token__c;

        SRM_WSDevryCryptography.ArrayOfString customFields = new SRM_WSDevryCryptography.ArrayOfString();
        customFields.string_x = parameters;

        SRM_WSDevryCryptography.DecoratorBraspagGeneralServiceSoap request = new SRM_WSDevryCryptography.DecoratorBraspagGeneralServiceSoap();
        request.timeout_x = 60000; 

        SRM_WSDevryCryptography.ArrayOfString arrayResult = request.DecryptRequest( merchantId, cryptString, apiToken, authToken, customFields );
        return arrayResult.string_x;
    }

    /*
 
    */
    public SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse braspagGetBoletoData( String merchantId, String braspagTransactionId, String requestId  ) {
        SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap request = new SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap();
        request.timeout_x = 60000;
        SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse response = request.GetBoletoData( merchantId, braspagTransactionId, requestId, '1.0' );
        if(response.BankNumber == '399-9'){
            //Tratamento para boleto santander. Confirmar no arquivo de retorno do banco antes de alterar.
            integer tamanhoNN = response.BoletoNumber.length();
            
            if (tamanhoNN < 16)
            {
                integer diferencaTamanho = 16 - tamanhoNN;
                string zerosAEsquerda = '';
                
                for (integer i=0;i<diferencaTamanho; i++)
                {
                    zerosAEsquerda += '0';
                }
                
                response.BoletoNumber = zerosAEsquerda + response.BoletoNumber;
            }
        }else if(response.BankNumber == '104-0'){
            //Tratamento para boleto caixa sigcb. Confirmar no arquivo de retorno do banco antes de alterar.
            if (response.BoletoNumber.startsWith('24')){
                response.BoletoNumber = response.BoletoNumber.replace('-', '').right(11);
            }
        }
        else if(response.BankNumber == '237-2'){
            //Tratamento para boleto bradesco. Confirmar no arquivo de retorno do banco antes de alterar.
            if (response.BoletoNumber.contains('-')){
                response.BoletoNumber = response.BoletoNumber.left(8);
                
                integer tamanhoNN = response.BoletoNumber.length();
                if (tamanhoNN < 16)
                {
                    integer diferencaTamanho = 16 - tamanhoNN;
                    string zerosAEsquerda = '';
                    
                    for (integer i=0;i<diferencaTamanho; i++)
                    {
                        zerosAEsquerda += '0';
                    }
                    
                    response.BoletoNumber = zerosAEsquerda + response.BoletoNumber;
                }
            }
        }
        return response;
    }

    /*

    */
    public SRM_WSBraspagDecoratorPagadorQuery.OrderDataResponse braspagGetOrderData( String merchantId, String braspagOrderId, String requestId ) {
        SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap request = new SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap();
        request.timeout_x = 60000;

        return request.GetOrderData( merchantId, braspagOrderId, requestId, '1.0' );
    }

    /*

    */
    public SRM_WSBraspagDecoratorPagadorQuery.OrderIdDataResponse braspagGetOrderIdData( String merchantId, String orderId, String requestId ) {
        SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap request = new SRM_WSBraspagDecoratorPagadorQuery.DecoratorPagadorQuerySoap();
        request.timeout_x = 60000;

        return request.GetOrderIdData( merchantId, orderId, requestId, '1.0' );
    }

    /*
        Monta os parametros de envio para o formulario de pagamento
        @param vendaid Chave do SF para envio ao braspag
        @param valor valor a ser cobrado
        @param nome Nome do inscrito
        @param cpf Documento do inscrito
        @param codpagamento Codigo do tipo de pagamento escolhido
        @param parcelas Numero de parcelas para pagamento
        @param tipoparcelado Tipo de parcelamento
        @param transactiontype tipo de transação braspag
        @param transactioncurrency Moeda para pagamento
        @param transactioncountry País de pagamento
        @param extradynamicurl URL de retorno apos a confirmação
    */
    public List<String> createPostParameters( String vendaid, String valor, String nome, String cpf, String codpagamento, String parcelas, String tipoparcelado, 
                                                String transactiontype, String transactioncurrency, String transactioncountry, String extradynamicurl ) {
        List<String> parameters = new List<String>();
        parameters.add( 'VENDAID=' + vendaid );
        parameters.add( 'VALOR=' + valor );
        parameters.add( 'NOME=' + nome );
        parameters.add( 'CPF=' + cpf );
        parameters.add( 'CODPAGAMENTO=' + codpagamento );
        parameters.add( 'PARCELAS=' + parcelas );
        parameters.add( 'TIPOPARCELADO=' + tipoparcelado );
        parameters.add( 'TRANSACTIONTYPE=' + transactiontype );
        parameters.add( 'TRANSACTIONCURRENCY=' + transactioncurrency );
        parameters.add( 'TRANSACTIONCOUNTRY=' + transactioncountry ); 
        parameters.add( 'EXTRADYNAMICURL=' + extradynamicurl );

        return parameters;
    }

    /*
        Descriptografa as informações e retorna o mapa de dados
        @param merchantId chave da empresa autorizada
        @param crypt String com o conteudo criptografado
    */
    public Map<String, String> braspagDecryptMapResult( String merchantId,  String crypt ) {
        Map<String, String> mapResult = new Map<String, String>();
        List<String> resultDecrypt = braspagDecrypt( merchantId, crypt, new List<String>() );
        if( resultDecrypt != null ) {
            for( String result : resultDecrypt ) {
                if( result.contains('Erro') ) {
                    return null;
                } 
                String[] parameter = result.split('=');
                mapResult.put( parameter[0], parameter[1] );
            }
        }
        return mapResult;
    }
}