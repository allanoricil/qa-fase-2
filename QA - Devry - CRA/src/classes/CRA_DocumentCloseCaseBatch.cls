global class CRA_DocumentCloseCaseBatch implements Database.Batchable<sObject> {		
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator('SELECT Id FROM Case WHERE Status = \'Aguardando aluno\' AND (Sub_Status__c = \'Aguardando documentação\' OR Sub_Status__c = \'Aguardando documentação CC\') AND LastModifiedDate < LAST_N_DAYS:2');

	}

   	global void execute(Database.BatchableContext BC, List<Case> scope) {

		for(Case c : scope){
		    
		    c.Apex_context__c = true;
		    c.Status = 'Encerrado';
		    c.Sub_Status__c = 'Encerrado falta de documentação';
		}

		Database.update(scope, false);
		
	}

	global void finish(Database.BatchableContext BC) {
		
	}
	
}