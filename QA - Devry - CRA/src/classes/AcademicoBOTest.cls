@isTest
private class AcademicoBOTest {
	
	@isTest static void test_method_one() {
		Account account = InstanceToClassCoverage.createAccount();
		insert account;
        
        Institui_o__c instituicao = InstanceToClassCoverage.createInstituicao();
        insert instituicao;
        
        Campus__c campus = InstanceToClassCoverage.createCampus(instituicao);
        insert campus;
        
        Per_odo_Letivo__c periodoLetivo = InstanceToClassCoverage.createPeriodoLetivo();
        insert periodoLetivo;
        
        Processo_seletivo__c processoSeletivo = InstanceToClassCoverage.createProcessoSeletivo(instituicao, campus, periodoLetivo);
		insert processoSeletivo;
        
 		Opportunity opportunity = InstanceToClassCoverage.createOpportunity(processoSeletivo);
 		opportunity.AccountId = account.Id;
 		insert opportunity;
                
        Processo_seletivo__c processoSeletivoAgendado = InstanceToClassCoverage.createProcessoSeletivo(instituicao, campus, periodoLetivo);
        processoSeletivoAgendado.Tipo_de_Matr_cula__c = 'Vestibular Especial';
		insert processoSeletivoAgendado;
        
        Opportunity opportunityAgendado = InstanceToClassCoverage.createOpportunity(processoSeletivoAgendado);
        opportunityAgendado.AccountId = account.Id;
 		insert opportunityAgendado;
        
 		Academico__c academico = InstanceToClassCoverage.getAcademico( account.Id, opportunity.Id );
 		insert academico;       
        
        Agendamento__c agendamento = InstanceToClassCoverage.createAgendamento(instituicao, campus); 		
 		insert agendamento;
	}
	
}