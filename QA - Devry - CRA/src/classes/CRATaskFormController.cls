public with sharing class CRATaskFormController {

	@AuraEnabled
	public static Boolean createTask(Id opportunityId,
									 String assunto, 
									 String canalDeContato,
									 String statusDoContato, 
									 String descricaoDaAtividade){
		try{
			Opportunity opp = [SELECT Id,
									Data_do_ultimo_contato__c,
									Account.Person_Contact_Id__c
							   FROM Opportunity
							  WHERE Id =: opportunityId
							  LIMIT 1];

			opp.Data_do_ultimo_contato__c = Date.today();
			Task atividade = new Task();

			atividade.whatid = opp.Id;
			atividade.whoid = opp.Account.Person_Contact_Id__c;
			atividade.OwnerId = UserInfo.getUserId();
			atividade.Subject = assunto;
			atividade.Status = 'Concluída';
			atividade.Canal_de_contato__c = canalDeContato;
			atividade.Conseguiu_Contato__c = statusDoContato;
			atividade.Description = descricaoDaAtividade;

			insert atividade;
			update opp;
		  	return true;
		}catch(Exception ex){
			return false;
		}
	}

	@AuraEnabled
	public static List<String>	getSubjects(){
		List<String> subjects = new List<String>();
		Schema.DescribeFieldResult fieldResult = Task.Subject.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPickListValues();
		for(Schema.PicklistEntry f : ple){
			subjects.add(f.getValue());
		}
		return subjects;
	}
}