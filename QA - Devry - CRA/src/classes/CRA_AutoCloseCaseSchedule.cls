global class CRA_AutoCloseCaseSchedule implements Schedulable{

	global void execute(SchedulableContext sc){
		
		CRA_AutoCloseCaseBatch autoCloseCaseBatch = new CRA_AutoCloseCaseBatch(); 
		Database.executeBatch(autoCloseCaseBatch, 10);

	}
	
}