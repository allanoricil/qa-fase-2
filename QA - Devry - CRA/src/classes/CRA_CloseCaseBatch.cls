global class CRA_CloseCaseBatch implements Database.Batchable<sObject> {
		
	
	global Database.QueryLocator start(Database.BatchableContext BC) {
		
		return Database.getQueryLocator('SELECT Id, Assunto__c, Aluno__r.Campus_n__c FROM Case WHERE Status IN (\'Novo\', \'Aguardando aluno\') AND Assunto__c != null');

	}

   	global void execute(Database.BatchableContext BC, List<Case> scope) {


		Set<Id> assuntosIds = new Set<Id>();
		for(Case c : scope)
			assuntosIds.add(c.Assunto__c);

		Map<Id, Assunto__c> assuntos = new Map<Id, Assunto__c>([SELECT Id, (SELECT Campus__c FROM Detalhes_do_Assunto1__r WHERE Fim_de_Vigencia__c < TODAY AND Rotina_processou__c = false) FROM Assunto__c WHERE Id IN: assuntosIds]);

		List<Case> casesUpdate = new List<Case>();

		for(Case c : scope){
			
		    Assunto__c assunto = assuntos.Get(c.Assunto__c);
		    
		    for(Detalhe_do_Assunto__c da : assunto.Detalhes_do_Assunto1__r){
		    
		        if(da.Campus__c == c.Aluno__r.Campus_n__c){
		            c.Apex_context__c = true;
		            c.Status = 'Encerrado';
		        	c.Sub_Status__c = 'Assunto expirado';
		            casesUpdate.add(c);
		        }
		    }
		}

		List<Detalhe_do_Assunto__c> detalhesDoAssunto = new List<Detalhe_do_Assunto__c>([SELECT Id FROM Detalhe_do_Assunto__c WHERE Fim_de_Vigencia__c < TODAY AND Rotina_processou__c = false]);

		for(Detalhe_do_Assunto__c da : detalhesDoAssunto){
			da.Data_do_processamento__c = System.today();
		}

		if(!casesUpdate.isEmpty())
			Database.update(casesUpdate, false);

		if(!detalhesDoAssunto.isEmpty())
			Database.update(detalhesDoAssunto, false);	
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}