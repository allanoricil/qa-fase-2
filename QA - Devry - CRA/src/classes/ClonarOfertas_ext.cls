public class ClonarOfertas_ext {

    Processo_seletivo__c ps = new Processo_seletivo__c();
    Processo_seletivo__c psx = new Processo_seletivo__c();
    List<Oferta__c> os = new List<Oferta__c>(); 
    List<Oferta__c> osx = new List<Oferta__c>();     
	String psid;
    
    public ClonarOfertas_ext() {
        psid = ApexPages.currentPage().getParameters().get('id');
        //cpuId = ApexPages.currentPage().getParameters().get('retURL');
        if (psid != null){
            ps = [Select id, Name, Data_de_Abertura__c, Instituicao_n__c, Data_de_encerramento__c, Sede_n__c, Campus__c,            
                         Ativo__c, Periodo_Letivo__c, Inscri_o__c, Data_de_Inicio_das_Aulas__c, Resultado__c, Institui_o_n__c,
                         Tipo_de_Processo_n__c, Integra_o_Candidatos__c, Tipo_de_Matr_cula__c, Niveis_de_Ensino__c, 
                         Data_do_Vestibular__c, Data_Aprova_Classificaveis__c, Data_Validade_Processo__c, Boleto__c,
                         Status_de_Uso__c, Status_Processo_Seletivo__c
                    From Processo_seletivo__c Where id =: psid limit 1];
            psx = ps.clone();
            psx.Institui_o_n__c = ps.Institui_o_n__c;
            psx.Tipo_de_Matr_cula__c = ps.Tipo_de_Matr_cula__c;
            psx.Niveis_de_Ensino__c = ps.Niveis_de_Ensino__c;
        }
    }
    public Processo_seletivo__c getpsx(){
        return psx;
    }
    public void setpsx(Processo_seletivo__c psx){
        this.psx = psx;
    }
    public Pagereference Clonar(){                    
        Savepoint sp = Database.setSavepoint();        
        try{                          
            if(psx.id == null){
                insert psx;
            }
            else{
                update psx;
            }
            try{
                
                if ([Select count() From Oferta__c Where Processo_seletivo__c =: psid AND Envia_para_Academus_RM__c =: true] > 0){
                    os = [Select id, Name, Processo_Seletivo__c, Curso__c, Filial__c, Campus__c, Institui_o__c, Institui_o_n__c, Tipo_do_Curso__c, Vagas__c, Valor_da_Inscri_o__c, Valor_da_Matr_cula__c, Link_do_curso_no_site__c 
                            From Oferta__c Where Processo_seletivo__c =: ps.id AND Envia_para_Academus_RM__c =: true];
                    for (Oferta__c o : os){
                        Oferta__c ox = new Oferta__c();
                        ox.Name = o.Name;
                        ox.Processo_Seletivo__c = psx.Id;
                        system.debug(psx.Id);
                        ox.Curso__c = o.Curso__c;
                        ox.Filial__c = o.Filial__c;
                        ox.Campus__c = o.Campus__c;
                        ox.Institui_o_n__c = o.Institui_o_n__c;
                        ox.Tipo_do_Curso__c = o.Tipo_do_Curso__c;
                        ox.Vagas__c = o.Vagas__c;
                        ox.Valor_da_Inscri_o__c = o.Valor_da_Inscri_o__c;
                        ox.Valor_da_Matr_cula__c = o.Valor_da_Matr_cula__c;
                        ox.Link_do_curso_no_site__c = o.Link_do_curso_no_site__c;
                        osx.add(ox);
                    }
                    system.debug(osx);
                    insert osx;
                }
            }
            catch(Exception e){  
                delete osx;
                return null;
            }
            finally {
              osx = new List<Oferta__c>(); 
            }             
        }
        catch(Exception e){  
            if (osx != null){
                for (Oferta__c o : osx){
                    if (o.Id != null) delete o;
                }
            }
            //Database.rollback(sp);
            //psx = ps.clone();
            //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.FATAL,'Erro ao clonar processo seletivo. Verifique as regras de validação'));
            //delete psx;
            //psx = new Processo_seletivo__c();
            //psx = ps.clone();
            
            return null;       
        }
        finally {
          osx = new List<Oferta__c>(); 
        }        
        PageReference p = new PageReference('/' + psx.Id);
        p.setRedirect(true);  
        return p;        
    }    
    public Pagereference Cancelar(){
        PageReference p = new PageReference('/' + psid);
        p.setRedirect(true);  
        return p;      
    }
    @isTest(SeeAllData=true)
    static  void UnitTest1() {

        Oferta__c oft = [Select id, Processo_seletivo__r.Id From Oferta__c Where Processo_seletivo__r.Ativo__c =: true AND Campus__c != null AND Institui_o_n__c != null AND Tipo_do_Curso__c != null AND Curso__r.Status__c =: 'Ativo' AND Curso__c != null limit 1];
        oft.Envia_para_Academus_RM__c = true;
        update oft;
        Processo_seletivo__c ps = [Select id From Processo_seletivo__c Where id =: oft.Processo_seletivo__r.Id limit 1];

        PageReference testPage =  Page.ClonarOfertas;
        testPage.getParameters().put('id',(String) ps.Id); 
        testPage.setRedirect(true);
        ApexPages.currentPage().getParameters().put('id', (String)ps.id);
        ClonarOfertas_ext ext = new ClonarOfertas_ext();
        ext.setpsx(ps);
        ext.getpsx();    
        PageReference result1 = ext.Clonar();
        PageReference result2 = ext.Cancelar();        
    }
}