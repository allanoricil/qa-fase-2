public with sharing class EvolucionalCreateUser{

	public static void processingQueue(String queueId, String payload){
		syncToServer(queueId, payload);         
	}

	@future(Callout = true)
	public Static void syncToServer(String queueId, String payload){
		try{
		    HttpResponse res = EvolucionalAPI.criaUsuario(payload);
		    Integer statusCode = res.getStatusCode();
		    if(statusCode == 200){
				processJsonBody(queueId, res.getBody());                                       
		    }else{
				if(statusCode != 409){
					updateOpportunitysPresenceFieldToFalse(queueId);
				}
				QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + res.getStatusCode() + ' / ' + res.getStatus());
			}
		}catch(CalloutException ex){
			QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
			updateOpportunitysPresenceFieldToFalse(queueId);
		}
	}

	private static void processJsonBody(String queueId, String response){
		Queue__c fila = [SELECT Oportunidade__c 
						   FROM Queue__c 
						  WHERE Id =:queueId];

		Opportunity opp = [SELECT Id,
							      Name,
							      CPF_form__c, 
		                          Account.PersonEmail,
		                          Evolucional_Username__c,
		                          Presente__c,
		                          Senha__c,
		                          Email_form__c,
		                          Evolucional_User_Id__c
		                     FROM Opportunity 
		                    WHERE Id =:fila.Oportunidade__c];

		String userId = extractUserId(response);
		updateQueuePayloadSAP(opp, userId, fila);
		opp.Evolucional_User_Id__c = userId;
		opp.Evolucional_Username__c = opp.Email_form__c;
		opp.Presente__c = true;
		update opp;
		QueueBO.getInstance().updateQueue(queueId,''); 
	}

	private Static void updateOpportunitysPresenceFieldToFalse(String queueId){
		Queue__c fila = [SELECT Oportunidade__c 
						   FROM Queue__c 
						  WHERE Id =:queueId];

		Opportunity opp = [SELECT Id,
		                          Presente__c
		                     FROM Opportunity 
		                    WHERE Id =:fila.Oportunidade__c];
		opp.Senha__c = '';
		opp.Evolucional_Username__c = '';
		opp.Presente__c = false;
		update opp;
	}

	private Static void updateQueuePayloadSAP(Opportunity opp, String userId, Queue__c fila){
	    fila.PayloadSAP__c = EvolucionalJsonBuilder.buildPayloadSAP(opp, userId);
	    update fila;    
    }

    private Static String extractUserId(String jsonBody){
        Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(jsonBody);
        String userId = JSON.serialize(objectMap.get('UserId'));
        return userId;
    }
}