/*
	@author Diego Moreira
	@class WebService para retorno dos serviços
*/
global abstract class WSResponseSf {
	global class Result {
    	WebService MessageResult message = new MessageResult();
    }
    
    global class MessageResult {
    	WebService String status;
    	WebService String msg;
    }
}