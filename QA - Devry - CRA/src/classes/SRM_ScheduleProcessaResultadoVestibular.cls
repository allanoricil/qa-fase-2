/*
    @author Diego Moreira
    @class Schedule de processamento status do condidato
*/
global class SRM_ScheduleProcessaResultadoVestibular implements Schedulable {
	
	global void execute(SchedulableContext sc) { 

		for( Queue__c queue : QueueDAO.getInstance().getQueueByEventNameAndStatus( 'SEARCH_VESTIBULAR_RESULTS', 'CREATED', 50 ) ) {
			SRM_ConsultaResultadoVestibularBO.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
		}
	}
}