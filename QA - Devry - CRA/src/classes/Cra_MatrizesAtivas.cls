global class Cra_MatrizesAtivas{
   
    public static List<MatrizesAtivas> getMatrizesAtivas( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
             System.debug( 'Entrada no metodo');    
            List<MatrizesAtivas> matrizesAtivas = new List<MatrizesAtivas>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraMatrizesAtivasList').Endpoint__c,new String[]{codcoligada,ra,codFilial});
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraMatrizesAtivasList' ).API_Token__c );
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );
                System.debug( 'Entrada no metodo');    
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                    system.debug('serverres.getBody(): ' +  res.getBody());
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraMatrizesAtivasList / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraMatrizesAtivasList/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }


    private static List <MatrizesAtivas>  processJsonBody(String jsonBody ) {
      
        Response SR = ( Response ) JSON.deserializeStrict( jsonBody, Response.class );
      
       system.debug(SR + ' destaque1 ');
        if( !SR.CraMatrizesAtivasResult.isEmpty() ){
           return  SR.CraMatrizesAtivasResult;
       }
        else{
            return null; 
        }
 
        
        
    }
    public Class Response {
        public List<MatrizesAtivas> CraMatrizesAtivasResult;
    }
   global  Class MatrizesAtivas {
         
        public String CodColigada;
        public String CodCurso;
        public String CoDGrade;
        public String CodHabilitacao;
        public String Descricao;
    }
}