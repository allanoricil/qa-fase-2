public class CCD_CriarAluno implements IProcessingQueue{


    public static void processingQueue(String queueId, String eventName, String payload) {
        syncToServer(queueId, payload);
    }

    @future(Callout = true)
    public Static void syncToServer(String queueId, String payload) {
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();

        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint('http://testeservicorm.academusportal.com.br:8083/api/aluno?Coligada=1');
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues( 'SAP-InboundSap' ).API_Token__c );
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            req.setBody(payload);

            try {
                Http h = new Http();
                HttpResponse res = h.send(req);
                if (res.getStatusCode() == 200)
                    processResponse(res.getBody(), queueId);
                else
                    QueueBO.getInstance().updateQueue(queueId, 'Integra RM / ' + res.getStatusCode() + ' / ' + res.getStatus());
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'Integra RM TOKEN/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            QueueBO.getInstance().updateQueue(queueId, 'Token  / ' + mapToken.get('401'));
        }
    }

    public static void createQueue(String idOportunidade) {
        Queue__c fila = new Queue__c();
        fila.EventName__c = QueueEventNames.CRIAR_ALUNO_CCD.name();
        fila.Status__c = 'CREATED';
        fila.Payload__c = payload(getOportunidade(idOportunidade));
        fila.Oportunidade__c = idOportunidade;
        fila.ProcessBySchedule__c = false;

        QueueDAO.getInstance().insertData(fila);
    }

    public static void processResponse(String payload, String queueId){
        String errorMessage = ''; 
        List<Queue__c> q = [Select Oportunidade__c from Queue__c where id=: queueId];
        List<Opportunity> op = [Select id, RM__c from Opportunity where id =: q[0].Oportunidade__c limit 1];


        if(payload.length() == 18 && op.size() > 0 )
        	op[0].RM__c = true;
        
        update op;

        QueueBO.getInstance().updateQueue( queueId, errorMessage, payload ); 
    }

    public static String payload(Opportunity oportunidade) {
        return '{'+
        '    \"SAluno\": {'+ '\n'+
        '      \"CODCOLIGADA\": \"'+ oportunidade.Processo_seletivo__r.C_digo_da_Coligada__c +'\",'+ '\n'+
        '      \"RA\": \"'+ '0' +'\",'+ '\n'+
        '      \"CODCOLCFO\": \"1\",'+ '\n'+
        '      \"CODCFO\": \"50719\",'+ '\n'+
        '      \"CODPESSOA\": \"54117\",'+ '\n'+
        '      \"CODTIPOCURSO\": \"1\",'+ '\n'+
        '      \"CODIGO\": \"54177\",'+ '\n'+
        '      \"RESPFINANCEIRO\": \"\",'+ '\n'+
        '      \"SEXO\": \"'+ getSexo(oportunidade.Account.Sexo__c) +'\",'+ '\n'+
        '      \"NOME\": \"'+oportunidade.Account.Name+'\",'+ '\n'+
        '      \"CPF\": \"'+oportunidade.CPF__c.replace('-','').replace('.','') +'\",'+ '\n'+
        '      \"SOBRENOME\": \"'+ oportunidade.Account.FirstName +'\",'+ '\n'+
        '      \"DTNASCIMENTO\": \"' + String.valueOf( oportunidade.DataNascimento_Form__c) + ' 16:00:00'+'\",'+ '\n'+
        '      \"ESTADOCIVIL\": \"S\",'+ '\n'+
        '      \"NACIONALIDADE\": \"10\",'+ '\n'+
        '      \"ESTADONATAL\": \"'+ oportunidade.Account.Estado__c +'\",'+ '\n'+
        '      \"NATURALIDADE\": \"'+ getEstado(oportunidade.Account.Cidade__c) +'\",'+ '\n'+
        '      \"DEFICIENTEFISICO\": \"0\",'+ '\n'+
        '      \"DEFICIENTEFALA\": \"0\",'+ '\n'+
        '      \"DEFICIENTEMENTAL\": \"0\",'+ '\n'+
        '      \"DEFICIENTEVISUAL\": \"0\",'+ '\n'+
        '      \"DEFICIENTEAUDITIVO\": \"0\",'+ '\n'+
        '      \"RUA\": \"'+ oportunidade.Account.Rua__c +'\",'+ '\n'+
        '      \"BAIRRO\": \"'+ oportunidade.Account.Bairro__c +'\",'+ '\n'+
        '      \"ESTADO\": \"'+ oportunidade.Account.Naturalidade__c +'\",'+ '\n'+
        '      \"CIDADE\": \"'+ oportunidade.Account.Cidade__c+'\",'+ '\n'+
        '      \"TELEFONE1\": \"'+ oportunidade.Celular__c +'\",'+ '\n'+
        '      \"CEP\": \"'+ oportunidade.Account.Cep__c.replace('-','') +'\",'+ '\n'+
        '      \"TITULOELEITOR\": \"\",'+ '\n'+
        '      \"CARTIDENTIDADE\": \"'+ oportunidade.Account.RG__c +'\",'+ '\n'+
        '      \"UFCARTIDENT\": \"'+ oportunidade.Account.Naturalidade__c +'\",'+ '\n'+
        '      \"ORGEMISSORIDENT\": \"SSP\",'+ '\n'+
        '      \"INDATENDESPECIALIZADO\": \"00\",'+ '\n'+
        '      \"INDATENDESPECIFICO\": \"00\",'+ '\n'+
        '      \"INDICADORDERECURSOS\": \"00\",'+ '\n'+
        '      \"ANOULTIMAINST\": null,'+ '\n'+
        '      \"AJUSTATAMANHOFOTO\": \"0\",'+ '\n'+
        '      \"CANHOTO\": \"F\",'+ '\n'+
        '      \"BRPDH\": \"0\",'+ '\n'+
        '      \"INVESTTREINANT\": \"0.00\",'+ '\n'+
        '      \"ESTADOROW\": \"0\",'+ '\n'+
        '      \"ROWVALIDA\": \"0\",'+ '\n'+
        '      \"ALUNO\": \"1\",'+ '\n'+
        '      \"PROFESSOR\": \"0\",'+ '\n'+
        '      \"CANDIDATO\": \"0\",'+ '\n'+
        '      \"USUARIOBIBLIOS\": \"0\",'+ '\n'+
        '      \"FUNCIONARIO\": \"0\",'+ '\n'+
        '      \"EXFUNCIONARIO\": \"0\",'+ '\n'+
        '      \"CODSTATUS\": \"-1\",'+ '\n'+
        '      \"DEFICIENTEMOBREDUZIDA\": \"0\",'+ '\n'+
        '      \"RECMODIFIEDBY\": \"'+ 'mestre' +'\",'+ '\n'+
        '      \"RECMODIFIEDON\": \"'+ '2018-01-16T14:46:41' +'\",'+ '\n'+
        '      \"CODIGO1\": \"54117\"'+ '\n'+
        '    },'+ '\n'+
        '    \"SAlunoCompl\": {'+ '\n'+
        '      \"CODCOLIGADA\": \"1\",'+ '\n'+
        '      \"RA\": \"'+ oportunidade.RA__r.Name +'\",'+ '\n'+
        '      \"RECCREATEDBY\": \"' + 'mestre' +'\",'+ '\n'+
        '      \"RECCREATEDON\": \"'+ '2018-01-16T14:46:41' +'\",'+ '\n'+
        '      \"RECMODIFIEDBY\": \"'+ 'mestre' +'\",'+ '\n'+
        '      \"RECMODIFIEDON\": \"'+ '2018-01-16T14:46:41' +'\"'+ '\n'+
        '    },'+ '\n'+
        '    \"SAlunoMEC\": {'+ '\n'+
        '      \"CODCOLIGADA\": \"' + oportunidade.Processo_seletivo__r.C_digo_da_Coligada__c+ '\",'+ '\n'+
        '      \"RA\": \"'+ oportunidade.RA__r.Name +'\",'+ '\n'+
        '      \"TRANSPORTEPUB\": \"0\",'+ '\n'+
        '      \"ZONARESIDENCIA\": \"1\",'+ '\n'+
        '      \"EDUEMOUTROESPACO\": \"3\",'+ '\n'+
        '      \"NECESSIDADEESPEC\": \"0\",'+ '\n'+
        '      \"CEGUEIRA\": \"0\",'+ '\n'+
        '      \"BAIXAVISAO\": \"0\",'+ '\n'+
        '      \"SURDEZ\": \"0\",'+ '\n'+
        '      \"DEFAUDITIVA\": \"0\",'+ '\n'+
        '      \"SURDOCEGUEIRA\": \"0\",'+ '\n'+
        '      \"DEFFISICA\": \"0\",'+ '\n'+
        '      \"DEFMENTAL\": \"0\",'+ '\n'+
        '      \"DEFMULTIPLA\": \"0\",'+ '\n'+
        '      \"AUTISMO\": \"0\",'+ '\n'+
        '      \"ASPERGER\": \"0\",'+ '\n'+
        '      \"RETT\": \"0\",'+ '\n'+
        '      \"PSICOSEINFANTIL\": \"0\",'+ '\n'+
        '      \"SUPERDOTADO\": \"0\",'+ '\n'+
        '      \"TRPRODVAN\": \"0\",'+ '\n'+
        '      \"TRPRODMICROBUS\": \"0\",'+ '\n'+
        '      \"TRPRODBUS\": \"0\",'+ '\n'+
        '      \"TRPRODBICICLO\": \"0\",'+ '\n'+
        '      \"TRPRODANIMAL\": \"0\",'+ '\n'+
        '      \"TRPRODOUTRO\": \"0\",'+ '\n'+
        '      \"TRPBARCOATE5\": \"0\",'+ '\n'+
        '      \"TRPBARCOATE15\": \"0\",'+ '\n'+
        '      \"TRPBARCOATE35\": \"0\",'+ '\n'+
        '      \"TRPBARCOMAIS35\": \"0\",'+ '\n'+
        '      \"TRPFERROVIARIO\": \"0\",'+ '\n'+
        '      \"PSICOSSOCIAL\": \"0\",'+ '\n'+
        '      \"PARAPLEGIA\": \"0\",'+ '\n'+
        '      \"TETRAPLEGIA\": \"0\",'+ '\n'+
        '      \"HEMIPLEGIA\": \"0\",'+ '\n'+
        '      \"AMPUTACAO\": \"0\",'+ '\n'+
        '      \"PARALCEREBRAL\": \"0\",'+ '\n'+
        '      \"RECAUXILIOLEDOR\": \"0\",'+ '\n'+
        '      \"RECAUXILIOTRANSCRICAO\": \"0\",'+ '\n'+
        '      \"RECGUIAINTERPRETE\": \"0\",'+ '\n'+
        '      \"RECINTERPRETELIBRAS\": \"0\",'+ '\n'+
        '      \"RECLEITURALABIAL\": \"0\",'+ '\n'+
        '      \"RECPROVAAMPLIADAFONTE16\": \"0\",'+ '\n'+
        '      \"RECPROVAAMPLIADAFONTE20\": \"0\",'+ '\n'+
        '      \"RECPROVAAMPLIADAFONTE24\": \"0\",'+ '\n'+
        '      \"RECPROVABRAILLE\": \"0\"'+ '\n'+
        '    },'+ '\n'+
        '    \"VFiliacao\": ['+ '\n'+
        '    ]'+ '\n'+
        '}'.toUpperCase();
    }

    public static String getSexo(String sexo){
        if(sexo == 'Masculino') return 'M';
        else if(sexo == 'Feminino') return 'F';
        else return '';
    }


    public static String getEstado(String uf){
        uf = uf.toUpperCase();
        String retorno = '';
        if(uf =='AC')retorno = 'ACRE';
        if(uf =='AL')retorno = 'ALAGOAS';
        if(uf =='AP')retorno = 'AMAPÁ';
        if(uf =='AM')retorno = 'AMAZONAS';
        if(uf =='BA')retorno = 'BAHIA';
        if(uf =='CE')retorno = 'CEARÁ';
        if(uf =='DF')retorno = 'DISTRITO FEDERAL';
        if(uf =='ES')retorno = 'ESPÍRITO SANTO';
        if(uf =='GO')retorno = 'GOIÁS';
        if(uf =='MA')retorno = 'MARANHÃO';
        if(uf =='MT')retorno = 'MATO GROSSO';
        if(uf =='MS')retorno = 'MATO GROSSO DO SUL';  
        if(uf =='MG')retorno = 'MINAS GERAIS';
        if(uf =='PA')retorno = 'PARÁ';
        if(uf =='PB')retorno = 'PARAÍBA';
        if(uf =='PR')retorno = 'PARANÁ';
        if(uf =='PE')retorno = 'PERNAMBUCO';
        if(uf =='PI')retorno = 'PIAUÍ';
        if(uf =='RJ')retorno = 'RIO DE JANEIRO';
        if(uf =='RN')retorno = 'RIO GRANDE DO NORTE';
        if(uf =='RS')retorno = 'RIO GRANDE DO SUL';
        if(uf =='RO')retorno = 'RONDÔNIA';
        if(uf =='RR')retorno = 'RORAIMA';
        if(uf =='SC')retorno = 'SANTA CATARINA';
        if(uf =='SP')retorno = 'SÃO PAULO';
        if(uf =='SE')retorno = 'SERGIPE';
        if(uf =='TO')retorno = 'TOCANTINS';

        return retorno;
    }

    public static Opportunity getOportunidade(String idOportunidade) {
        return [Select id, RA__r.Name,
        Processo_seletivo__r.C_digo_da_Coligada__c,
        Account.Sexo__c,
        Account.Name,
        CPF__c,
        Account.FirstName,
        DataNascimento_Form__c,
        Account.Estado__c,
        Celular__c,
        Account.Rua__c,
        Account.Bairro__c,
        Account.Naturalidade__c,
        Account.Cidade__c,
        Account.Cep__c,
        Account.RG__c,
        Naturalidade__c,
        LastModifiedDate,
        CreatedDate from Opportunity where id=: idOportunidade];
    }
}