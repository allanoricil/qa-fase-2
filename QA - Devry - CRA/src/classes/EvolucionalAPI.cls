public with sharing class EvolucionalAPI {
	
	private static final String LISTAR_SIMULADOS_URL = 'http://api.evolucional.com.br/simulado-online/usuario/listar-simulados';
	private static final String CRIAR_USUARIO_URL = 'http://api.evolucional.com.br/simulado-online/usuario/criar';

	public static HttpResponse criaUsuario(String requestBody){
		return chamaServico(CRIAR_USUARIO_URL, requestBody);
	}

	public static HttpResponse listaSimulados(String requestBody){
		return chamaServico(LISTAR_SIMULADOS_URL, requestBody);	
	}

	private static HttpResponse chamaServico(String endPoint,String requestBody){
		HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(endPoint);
        httpRequest.setMethod('POST');
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setTimeout(120000);
        httpRequest.setBody(requestBody);
        String response = '';
        Http h = new Http();
        HttpResponse httpResponse = h.send(httpRequest);
        return httpResponse;
	}

}