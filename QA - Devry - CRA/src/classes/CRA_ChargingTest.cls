@isTest
public class CRA_ChargingTest {
	
    public static testMethod void testCreateCharging(){
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Anexo__c = 'Obrigatório';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Status = 'Aguardando pagamento';
        caso.Status_de_Pagamento__c = 'Em Aberto';
        caso.Assunto__c = assunto.Id;
        caso.Apex_context__c = true;
        insert caso;
        
        Cobranca__c cobranca = CRA_DataFactoryTest.newCharging(caso.Id);     
        insert cobranca;
        
        String cName = [SELECT Name FROM Cobranca__c WHERE Id =: cobranca.Id LIMIT 1].Name;
        
        string jsonBody = '{"InboundSapResult":[{"bELNRField":"2800000082","bUKRSField":"1000","gJAHRField":"2017","messagesField":[{"fIELDField":"","idField":"RW","lOG_MSG_NOField":"000000","lOG_NOField":"","mESSAGEField":"Documento lançado: BKPFF 280000008210002017 ECQCLNT310","mESSAGE_V1Field":"BKPFF","mESSAGE_V2Field":"280000008210002017","mESSAGE_V3Field":"ECQCLNT310","mESSAGE_V4Field":"","nUMBERField":"605","pARAMETERField":"","rOWField":"0","sYSTEMField":"ECQCLNT310","tYPEField":"S"},{"fIELDField":"","idField":"KI","lOG_MSG_NOField":"000000","lOG_NOField":"","mESSAGEField":"Entrar um objeto efetivo de classificação contábil para a receita.","mESSAGE_V1Field":"331203","mESSAGE_V2Field":"1000","mESSAGE_V3Field":"","mESSAGE_V4Field":"","nUMBERField":"166","pARAMETERField":"","rOWField":"0","sYSTEMField":"ECQCLNT310","tYPEField":"W"}],"rEF_DOC_NOField":"' + cName + '"}]}';
       	CRA_CreateCaseChargings.processChargingJsonBody(jsonBody);
    }
    
    public static testMethod void testProcessCharging(){
    	Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Front Office'];
        User usuario = CRA_DataFactoryTest.newStandardUser(perfil.Id); 
        insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Descricao_obrigatoria__c = false;
        assunto.Anexo__c = 'Obrigatório';
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        insert detalhe;
        
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Status = 'Aguardando pagamento';
        caso.Status_de_Pagamento__c = 'Em Aberto';
        caso.Assunto__c = assunto.Id;
        caso.Apex_context__c = true;
        insert caso;
        
        Cobranca__c cobranca = CRA_DataFactoryTest.newCharging(caso.Id);
        cobranca.CodigoSAP__c = '31321312';
        cobranca.EmpresaSAP__c = '1234';
        cobranca.ExercicioSAP__c = '2017';
        insert cobranca;
        
        String cName = [SELECT Name FROM Cobranca__c WHERE Id =: cobranca.Id LIMIT 1].Name;
        
        system.debug('nome da cobrança: ' + cName);
        
		Test.setMock(HttpCalloutMock.class, new CRA_ChargingMockTest());
        
        insert new WSSetup__c(Endpoint__c = 'Authorization', Name ='Token Test', API_Token__c ='token');
        insert new WSSetup__c(Endpoint__c = 'Authorization', Name ='Token', API_Token__c ='token');
        insert new WSSetup__c(Endpoint__c = 'SAP-StatusDocumento', Name ='SAP-StatusDocumento', API_Token__c = 'token');
        
        Test.startTest();
        
        CRA_ProcessCaseCharging.execute();
        
        Test.stopTest();
        
    }    
    
}