public without sharing class CRA_CaseMilestoneTrackingController {
	private final Case myCase{get;set;}
	public List<CaseMilestone> milestones{get;set;}

	public CRA_CaseMilestoneTrackingController(ApexPages.StandardController stdController) {
		List<String> myFields = new List<String>();
        myFields.add('Id');
        stdController.addFields(myFields); 
        this.myCase = (Case)stdController.getRecord();        
        milestones = [SELECT CaseId, TargetDate, CompletionDate, IsCompleted, MilestoneType.Name FROM CaseMilestone WHERE CaseId =: myCase.Id AND MilestoneType.Name != 'Conclusão do Caso' ORDER BY CompletionDate DESC NULLS FIRST];
	}
}