/*
    @author Adílio Santos
    @class Classe DAO do objeto Meio de Pagamento Do Campus (Campus_Payment_Type__c)
*/

public with sharing class CampusPaymentTypeDAO extends SObjectDAO {
	
    //Singleton
    private static final CampusPaymentTypeDAO instance = new CampusPaymentTypeDAO();
    private CampusPaymentTypeDAO(){}
    
    public static CampusPaymentTypeDAO getInstance() {
        return instance;
    }

	//Busca os meios de pagamento do campus
    public List<Campus_Payment_Type__c> getPaymentTypesByCampusId( String campusId ) {
    	return [SELECT Id, Campus__c, Payment_Type_Braspag__c, Payment_Type_Braspag__r.Name,
                		Payment_Type_Braspag__r.Codigo__c, Payment_Type_Braspag__r.TipoMeioPagamento__c,
                		Payment_Type_Braspag__r.Bandeira__c, Payment_Type_Braspag__r.Chave_Bandeira__c,
                		Payment_Type_Braspag__r.Forma__c
                FROM Campus_Payment_Type__c
                WHERE Campus__c = :campusId];
    }
}