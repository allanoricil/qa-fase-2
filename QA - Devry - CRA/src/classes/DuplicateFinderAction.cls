global class DuplicateFinderAction {    
    
    public void go(String q){

        DuplicateFinder df = new DuplicateFinder();
        df.query = q;
        df.email='rafael@weengo.com';
        df.fromUserId = UserInfo.getUserId();
        df.toUserId = UserInfo.getUserId();
        ID batchprocessid = Database.executeBatch(df, 2000);
    }
}