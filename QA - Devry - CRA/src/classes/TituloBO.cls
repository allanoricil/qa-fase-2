/*
    @author Diego Moreira
    @class Classe de negocio de titulos de inscrição
*/
public with sharing class TituloBO {
    /*
        Singleton
    */
    private static final TituloBO instance = new TituloBO();
    private TituloBO() {}

    public static TituloBO getInstance() {
        return instance;
    }

    /*
        Cria o titulo da inscrição do aluno
        @param processoId Id do processo seletivo
        @param accountId Id do aluno
        @param tipoPagamento tipo de pagamento da inscrição
    */
    //ALTER: MERCHANTID
    /*
    public Titulos__c criaTituloInscricao( String opportunityId, String processoId, String accountId, MeioPagamentoIES__c meioPagamento ) {
        Date dateTitulo = Date.today();     
        Processo_seletivo__c pSeletivo = Processo_SeletivoDAO.getInstance().getProcessoSeletivoById( processoId )[0];
        Opportunity opportunity = OpportunityDAO.getInstance().getOpportunityById( opportunityId )[0];  
        System.debug('>>> ' + meioPagamento.MeioPagamentoBraspag__r.TipoMeioPagamento__c + ' / ' + pSeletivo.Vencimento_Boleto_dias__c);
        Titulos__c titulo = new Titulos__c();  
        titulo.Candidato_Aluno__c      = accountId;
        titulo.Inscricao__c            = opportunityId; 
        titulo.Documento__c            = 'VEST';
        titulo.Instituicao__c          = pSeletivo.Institui_o_n__c;
        titulo.ProcessoSeletivo__c     = pSeletivo.Id;
        titulo.Valor__c                = opportunity.Amount;
        titulo.Agencia__c              = pSeletivo.Institui_o_n__r.Agencia__c;
        titulo.Conta__c                = pSeletivo.Institui_o_n__r.Conta__c;
        titulo.Digito__c               = pSeletivo.Institui_o_n__r.Digito__c;
        titulo.DataEmissao__c          = dateTitulo;
        titulo.DataVencimento__c       = meioPagamento.MeioPagamentoBraspag__r.TipoMeioPagamento__c.equals('Boleto') ? validaDiaDeSemana( dateTitulo.addDays( Integer.valueOf( pSeletivo.Vencimento_Boleto_dias__c ) ) ) : dateTitulo;
        titulo.DataProcessamento__c    = Date.today();
        titulo.Instrucao__c            = pSeletivo.Instrucoes_Boleto__c;
        titulo.OpcaoPagamento__c       = meioPagamento.MeioPagamentoBraspag__r.Name;
        titulo.MeioPagamento__c        = meioPagamento.MeioPagamentoBraspag__r.TipoMeioPagamento__c;
        titulo.CodigoMeioPagamento__c  = meioPagamento.MeioPagamentoBraspag__r.Codigo__c;
        titulo.Chave_Bandeira__c       = meioPagamento.MeioPagamentoBraspag__r.Chave_Bandeira__c;
        titulo.Bandeira__c             = meioPagamento.MeioPagamentoBraspag__r.Bandeira__c;
        titulo.Forma__c                = meioPagamento.MeioPagamentoBraspag__r.Forma__c;
        insert titulo;

        return titulo;
    }
    */

    /*
        Atualiza os dados do titulo
    */
    //ALTER: MERCHANTID
    /*
    public Titulos__c atualizaTituloInscricao( String opportunityId, String processoId, String accountId, MeioPagamentoIES__c meioPagamento, 
                                                Map<String, Object> postResult, Map<String, Object> paymentResult ) {
        Date dateTitulo = Date.today();     
        Processo_seletivo__c pSeletivo = Processo_SeletivoDAO.getInstance().getProcessoSeletivoById( processoId )[0];
        Opportunity opportunity = OpportunityDAO.getInstance().getOpportunityById( opportunityId )[0];  

        Titulos__c tituloToUpdate = new Titulos__c();
        //tituloToUpdate.Id                           = (String) postResult.get( 'MerchantOrderId' );
        tituloToUpdate.Name                         = (String) paymentResult.get( 'BoletoNumber' );
        tituloToUpdate.CodigoBrasPag__c             = (String) paymentResult.get( 'PaymentId' );
        tituloToUpdate.CodigoRetornoBrasPag__c      = String.valueOf( paymentResult.get( 'ReasonCode' ) );
        tituloToUpdate.DescricaoRetornoBrasPag__c   = (String) paymentResult.get( 'ReasonMessage' );
        tituloToUpdate.LinhaDigitavelBoleto__c      = (String) paymentResult.get( 'DigitableLine' );
        tituloToUpdate.UrlBoleto__c                 = (String) paymentResult.get( 'Url' );
        tituloToUpdate.Candidato_Aluno__c      = accountId;
        tituloToUpdate.Inscricao__c            = opportunityId; 
        tituloToUpdate.Documento__c            = 'VEST';
        tituloToUpdate.Instituicao__c          = pSeletivo.Institui_o_n__c;
        tituloToUpdate.ProcessoSeletivo__c     = pSeletivo.Id;
        tituloToUpdate.Valor__c                = opportunity.Amount;
        tituloToUpdate.Agencia__c              = pSeletivo.Institui_o_n__r.Agencia__c;
        tituloToUpdate.Conta__c                = pSeletivo.Institui_o_n__r.Conta__c;
        tituloToUpdate.Digito__c               = pSeletivo.Institui_o_n__r.Digito__c;
        tituloToUpdate.DataEmissao__c          = dateTitulo;
        tituloToUpdate.DataVencimento__c       = meioPagamento.MeioPagamentoBraspag__r.TipoMeioPagamento__c.equals('Boleto') ? validaDiaDeSemana( dateTitulo.addDays( Integer.valueOf( pSeletivo.Vencimento_Boleto_dias__c ) ) ) : dateTitulo;
        tituloToUpdate.DataProcessamento__c    = Date.today();
        tituloToUpdate.Instrucao__c            = pSeletivo.Instrucoes_Boleto__c;
        tituloToUpdate.OpcaoPagamento__c       = meioPagamento.MeioPagamentoBraspag__r.Name;
        tituloToUpdate.MeioPagamento__c        = meioPagamento.MeioPagamentoBraspag__r.TipoMeioPagamento__c;
        tituloToUpdate.CodigoMeioPagamento__c  = meioPagamento.MeioPagamentoBraspag__r.Codigo__c;
        tituloToUpdate.Chave_Bandeira__c       = meioPagamento.MeioPagamentoBraspag__r.Chave_Bandeira__c;
        tituloToUpdate.Bandeira__c             = meioPagamento.MeioPagamentoBraspag__r.Bandeira__c;
        tituloToUpdate.Forma__c                = meioPagamento.MeioPagamentoBraspag__r.Forma__c;                                               

        insert tituloToUpdate;
        return tituloToUpdate;
    }
    */

    /*
        Cria o titulo da inscrição do aluno
    */
    public Titulos__c insertTitle(String opportunityId, String processoId, String accountId, Campus_Payment_Type__c paymentType) {

        Processo_seletivo__c selectiveProcess = Processo_SeletivoDAO.getInstance().getProcessoSeletivoById(processoId)[0];
        Opportunity opportunity = OpportunityDAO.getInstance().getOpportunityById(opportunityId)[0];
        Date titleDate = Date.today();
        System.debug('Insert >>> ' + paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c + ' / ' + selectiveProcess.Vencimento_Boleto_dias__c);


        //Preenche o CPF do titulo referente à oportunidade
        List < Opportunity > opps = [SELECT ID, Cpf__c from Opportunity WHERE Id =: opportunityId];
        String cpfTitulo = opps[0].Cpf__c;
        String cpf = cpfTitulo.replaceAll('[|.|-]', '');

        Titulos__c title = new Titulos__c();
        title.Candidato_Aluno__c = accountId;
        title.Inscricao__c = opportunityId;
        title.CPF__c = cpf;
        title.Documento__c = 'VEST';
        title.Instituicao__c = selectiveProcess.Institui_o_n__c;
        title.ProcessoSeletivo__c = selectiveProcess.Id;
        title.Valor__c = opportunity.Amount;
        title.Agencia__c = selectiveProcess.Institui_o_n__r.Agencia__c;
        title.Conta__c = selectiveProcess.Institui_o_n__r.Conta__c;
        title.Digito__c = selectiveProcess.Institui_o_n__r.Digito__c;
        title.DataEmissao__c = titleDate;
        title.DataVencimento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c.equals('Boleto') ? validaDiaDeSemana(titleDate.addDays(Integer.valueOf(selectiveProcess.Vencimento_Boleto_dias__c))) : titleDate;
        title.DataProcessamento__c = Date.today();
        title.Instrucao__c = selectiveProcess.Instrucoes_Boleto__c;
        title.OpcaoPagamento__c = paymentType.Payment_Type_Braspag__r.Name;
        title.MeioPagamento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c;
        title.CodigoMeioPagamento__c = paymentType.Payment_Type_Braspag__r.Codigo__c;
        title.Chave_Bandeira__c = paymentType.Payment_Type_Braspag__r.Chave_Bandeira__c;
        title.Bandeira__c = paymentType.Payment_Type_Braspag__r.Bandeira__c;
        title.Forma__c = paymentType.Payment_Type_Braspag__r.Forma__c;

        insert title;
        return title;
    }

    /*
        Atualiza os dados do titulo
    */
    public Titulos__c updateTitle(String opportunityId, String processoId, String accountId, Campus_Payment_Type__c paymentType, Map < String, Object > postResult, Map < String, Object > paymentResult) {

        Processo_seletivo__c selectiveProcess = Processo_SeletivoDAO.getInstance().getProcessoSeletivoById(processoId)[0];
        Opportunity opportunity = OpportunityDAO.getInstance().getOpportunityById(opportunityId)[0];
        Date titleDate = Date.today();

        System.debug('Update >>> ' + paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c + ' / ' + selectiveProcess.Vencimento_Boleto_dias__c);

        Titulos__c titleToUpdate = new Titulos__c();
        //titleToUpdate.Id                           = (String) postResult.get( 'MerchantOrderId' );
        titleToUpdate.Name = (String) paymentResult.get('BoletoNumber');
        titleToUpdate.CodigoBrasPag__c = (String) paymentResult.get('PaymentId');
        titleToUpdate.CodigoRetornoBrasPag__c = String.valueOf(paymentResult.get('ReasonCode'));
        titleToUpdate.DescricaoRetornoBrasPag__c = (String) paymentResult.get('ReasonMessage');
        titleToUpdate.LinhaDigitavelBoleto__c = (String) paymentResult.get('DigitableLine');
        titleToUpdate.UrlBoleto__c = (String) paymentResult.get('Url');
        titleToUpdate.Candidato_Aluno__c = accountId;
        titleToUpdate.Inscricao__c = opportunityId;
        titleToUpdate.Documento__c = 'VEST';
        titleToUpdate.Instituicao__c = selectiveProcess.Institui_o_n__c;
        titleToUpdate.ProcessoSeletivo__c = selectiveProcess.Id;
        titleToUpdate.Valor__c = opportunity.Amount;
        titleToUpdate.Agencia__c = selectiveProcess.Institui_o_n__r.Agencia__c;
        titleToUpdate.Conta__c = selectiveProcess.Institui_o_n__r.Conta__c;
        titleToUpdate.Digito__c = selectiveProcess.Institui_o_n__r.Digito__c;
        titleToUpdate.DataEmissao__c = titleDate;
        titleToUpdate.DataVencimento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c.equals('Boleto') ? validaDiaDeSemana(titleDate.addDays(Integer.valueOf(selectiveProcess.Vencimento_Boleto_dias__c))) : titleDate;
        titleToUpdate.DataProcessamento__c = Date.today();
        titleToUpdate.Instrucao__c = selectiveProcess.Instrucoes_Boleto__c;
        titleToUpdate.OpcaoPagamento__c = paymentType.Payment_Type_Braspag__r.Name;
        titleToUpdate.MeioPagamento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c;
        titleToUpdate.CodigoMeioPagamento__c = paymentType.Payment_Type_Braspag__r.Codigo__c;
        titleToUpdate.Chave_Bandeira__c = paymentType.Payment_Type_Braspag__r.Chave_Bandeira__c;
        titleToUpdate.Bandeira__c = paymentType.Payment_Type_Braspag__r.Bandeira__c;
        titleToUpdate.Forma__c = paymentType.Payment_Type_Braspag__r.Forma__c;

        insert titleToUpdate;
        return titleToUpdate;
    }


    /*
        Atualiza o status do titulo
        @param idTitulo id para atualizar
        @param Status de atualiza��o
    */
    public Titulos__c atualizaStatusTitulo(String idTitulo, String status) {
        Titulos__c titulo = new Titulos__c();
        titulo.Id = idTitulo;
        titulo.Status__c = status;
        return titulo;
    }

    /*
        Cria as filas dos titulos
        @param titulos Lista de titulos criados
    */
    public void createQueueToInsertTitles(List < Titulos__c > titulos) {
        String fila = '';
        List < String > titulosId = new List < String > ();
        List < Queue__c > queueToCreate = new List < Queue__c > ();

        for (Titulos__c titulo: titulos) {
            titulosId.add(titulo.Id);
        }

        for (Titulos__c titulo: TituloDAO.getInstance().getCreateTitulosByIds(titulosId)) {
            String jsonResultSap = SRM_CreateRegistrationTitlesBO.createClientToJson(titulo.Inscricao__c);

            String jsonResult = SRM_CreateRegistrationTitlesBO.createTitlesToJson(titulo);
            system.debug('jsonResult: ' + jsonResult);

            fila = QueueBO.getInstance().createQueueByScheduleProcess(QueueEventNames.INSERT_REGISTRATION_TITLES.name(), jsonResult, jsonResultSap, titulo.id);

            // String isMatriculaOnline = [SELECT Id, Name, ProcessoSeletivo__r.Matr_cula_Online__c FROM Titulos__c WHERE Id =: titulo.id limit 1 ].ProcessoSeletivo__r.Matr_cula_Online__c;
            /*
            if(isMatriculaOnline == 'Sim'){
               retornaClienteSAP( jsonResultSap, fila );  
            }
*/

        }
    }

    @future(Callout = true)
    public static void retornaClienteSAP(String payload) {
        system.debug('retornaCliente SAP para o payload' + payload);

        HttpRequest req = new HttpRequest();
        req.setEndpoint(WSSetup__c.getValues('ClienteSap').Endpoint__c);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('API-TOKEN', 'A58060FC5E289220BF971DFCFBD43CEF');
        req.setTimeout(120000);
        req.setBody(payload);

        Http h = new Http();
        HttpResponse res = h.send(req);

        system.debug('sapres.getStatusCode(): ' + res.getStatusCode());
        system.debug('sapres.getBody(): ' + res.getBody());
        system.debug('sapres.getBody(): ' + res.toString());

        if (res.getStatusCode() == 200) {
            SRM_CreateRegistrationTitlesBO.processJsonBodySAP(res.getBody());
        }
    }

    /*
        Cria as filas dos titulos
        @param titulos Lista de titulos criados
    */
    public void createQueueToInsertTitlesSyncrono(List < Titulos__c > titulos) {
        String fila = '';
        List < String > titulosId = new List < String > ();
        List < Queue__c > queueToCreate = new List < Queue__c > ();

        for (Titulos__c titulo: titulos) {
            titulosId.add(titulo.Id);
        }
        system.debug('titulo id = ' + titulosId);
        for (Titulos__c titulo: TituloDAO.getInstance().getCreateTitulosByIds(titulosId)) {
            String jsonResultSap = SRM_CreateRegistrationTitlesBO.createClientToJson(titulo.Inscricao__c);
            system.debug('resultado = ' + jsonResultSap);

            String jsonResult = SRM_CreateRegistrationTitlesBO.createTitlesToJson(titulo);
            system.debug('resultado = ' + jsonResult);

            fila = QueueBO.getInstance().createQueueByScheduleProcess(QueueEventNames.INSERT_REGISTRATION_TITLES.name(), jsonResult, jsonResultSap, titulo.id);
            system.debug('queueid = ' + fila);

            String isMatriculaOnline = [SELECT Id, Name, ProcessoSeletivo__r.Matr_cula_Online__c FROM Titulos__c WHERE Id =: titulo.id limit 1].ProcessoSeletivo__r.Matr_cula_Online__c;
            if (isMatriculaOnline == 'Sim') {
                retornaClienteSAPSyncrono(jsonResultSap, fila);
            }


        }
    }

    @future(callout = true)
    public static void retornaClienteSAPSyncrono(String payload, String fila) {
        system.debug('retorna clientesap syncrono');

        HttpRequest req = new HttpRequest();
        req.setEndpoint(WSSetup__c.getValues('ClienteSap').Endpoint__c);
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setHeader('API-TOKEN', 'A58060FC5E289220BF971DFCFBD43CEF');
        req.setTimeout(120000);
        req.setBody(payload);

        Http h = new Http();
        HttpResponse res = h.send(req);

        system.debug('sapres.getStatusCode(): ' + res.getStatusCode());
        system.debug('sapres.getBody(): ' + res.getBody());
        system.debug('sapres.getBody(): ' + res.toString());

        if (res.getStatusCode() == 200) {
            SRM_CreateRegistrationTitlesBO.processJsonBodySAP(fila, res.getBody());
        }
    }

    /*
        Cria as filas dos titulos
        @param titulos Lista de titulos criados
    */
    /*public void createQueueToInsertTitles( List<Titulos__c> titulos ) {
        List<String> titulosId = new List<String>();
        List<Queue__c> queueToCreate = new List<Queue__c>();

        for( Titulos__c titulo : titulos )
            titulosId.add( titulo.Id );

        for( Titulos__c titulo : TituloDAO.getInstance().getCreateTitulosByIds( titulosId ) ) {
            List <Titulos__c> opp = [ SELECT Inscricao__c FROM Titulos__c WHERE id = :titulosId];
            String jsonResult = '';
            String jsonResultSap = SRM_CreateRegistrationTitlesBO.createClientToJson( opp[0].Inscricao__c );
            
            QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.INSERT_REGISTRATION_TITLES.name(), jsonResult, jsonResultSap );



            QueueBO.getInstance().reprocessQueue();

            jsonResult = SRM_CreateRegistrationTitlesBO.createTitlesToJson( titulo );    
            QueueBO.getInstance().updateQueueTemp( QueueEventNames.INSERT_REGISTRATION_TITLES.name(), jsonResult );
            QueueBO.getInstance().reprocessQueue();
            
        }                
    }
    */
    /*
        Cria as filas de atualização de titulos
        @param titulos Lista de titulos criados
    */
    public void createQueueToUpdateTitles(List < Titulos__c > titulos) {
        List < String > titulosId = new List < String > ();
        List < Queue__c > queueToCreate = new List < Queue__c > ();

        for (Titulos__c titulo: titulos) {
            if (titulo.Status__c.equals('Pago'))
                titulosId.add(titulo.Id);
        }

        for (Titulos__c titulo: TituloDAO.getInstance().getUpdateTitulosByIds(titulosId)) {
            String jsonResult = SRM_UpdateRegistrationTitlesBO.createTitlesToJson(titulo);
            QueueBO.getInstance().createQueueByScheduleProcess(QueueEventNames.UPDATE_REGISTRATION_TITLES.name(), jsonResult);
        }
    }

    /*
        Atualiza os dados do titulos com o retorno da integra��o SAP
        @param Id do titulo a ser atualizado
        @param codSAP codigo gerado no SAP
        @param empresa dado de retorno do SAP
        @param exercicio dado de retorno do SAP
    */
    public Titulos__c updateCreateSapTitulos(String id, String codSAP, String empresa, String exercicio) {
        Titulos__c titulos = new Titulos__c();
        titulos.Id = id;
        titulos.CodigoSAP__c = codSAP;
        titulos.EmpresaSAP__c = empresa;
        titulos.ExercicioSAP__c = exercicio;

        return titulos;

    }
    public Titulos__c updateClienteSap(String id, String clientesap) {
        date dataAtual = date.today();
        String anoAtual = String.valueOf(dataAtual.year());
        Titulos__c titulos = new Titulos__c();
        titulos.Id = id;
        titulos.Cliente_Sap__c = clientesap;
        titulos.CodigoSAP__c = clientesap;
        titulos.ExercicioSAP__c = anoAtual;
        return titulos;
    }

    /*
        Atualiza a fase da oportunidade apos o pagamento do titulo
        @param titulos lista de titulos atualizados
    */
    public void atualizaFaseOportunidadeTituloPago(List < Titulos__c > titulos) {
        for (Titulos__c titulo: titulos) {
            if (titulo.FaseInscricao__c.equals('Pré-Inscrito')) {
                Opportunity opportunityToUpdate;
                if (titulo.Status__c.equals('Pago'))
                    opportunityToUpdate = new Opportunity(Id = titulo.Inscricao__c, StageName = 'Inscrito', AguardandoRetornoBraspag__c = false);
                else if (titulo.MeioPagamento__c.equals('Cartão') && titulo.Status__c.equals('Em aberto'))
                    opportunityToUpdate = new Opportunity(Id = titulo.Inscricao__c, AguardandoRetornoBraspag__c = true);
                else
                    opportunityToUpdate = new Opportunity(Id = titulo.Inscricao__c, AguardandoRetornoBraspag__c = false);

                update opportunityToUpdate;
            }
        }
    }

    /*
        Valida a data de vencimento somente dias de semana
        @param dtVencimento Data vencimento do boleto
    */
    public Date validaDiaDeSemana(Date dtVencimento) {
        Datetime dtTime = (DateTime) dtVencimento;
        String dayOfWeek = dtTime.format('EEEE');

        if (dayOfWeek.equals('Friday')) dtVencimento = dtVencimento.addDays(2);
        if (dayOfWeek.equals('Saturday')) dtVencimento = dtVencimento.addDays(1);

        return dtVencimento;
    }


}