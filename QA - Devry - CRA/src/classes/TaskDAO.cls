/*
    @author Adílio Santos
    @class Classe DAO do objeto Task
*/
public with sharing class TaskDAO extends SObjectDAO {
    /*
        Singleton
    */
    private static final TaskDAO instance = new TaskDAO();    
    private TaskDAO(){}
    
    public static TaskDAO getInstance() {
        return instance;
    }

    /*
        Busca a Task pelo Id
        @param taskId, Id da Task
    */
    public List<Task> getTaskById( String taskId ) {
        return [SELECT Id,RecordTypeId,Priority,AccountId,CreatedById,CreatedDate,LastModifiedById,LastModifiedDate,Conseguiu_Contato__c,Status,SystemModstamp,WhatId,WhoId 
                    FROM Task 
                    WHERE Id = :taskId];
    }
}