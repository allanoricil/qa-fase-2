/*
	@author Diego Moreira
    @class Classe schedule para gerar filas de consulta de pedidos no braspag
*/
global class SRM_ScheduleBraspagConsultaPedido implements Schedulable {
	global void execute( SchedulableContext SC ) {
		List<Titulos__c> tituloList = TituloDAO.getInstance().getTitulosSemRetornoBraspag();
		if( tituloList.size() > 0 ) {
			String titulosJson = JSON.serializePretty( tituloList ); 
		
			QueueBO.getInstance().createQueue( QueueEventNames.SEARCH_BRASPAG_ORDER.name(), titulosJson );
		}		
	}
}