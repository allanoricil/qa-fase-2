public without sharing class TaskUtil {

	//Método usado para espelhar o status da ultima tarefa criada para a oportunidade
	public static void atualizaOpp(List<Task> allTasks) {		
		List<Id> allOppIds = new List<Id>();
		List<Id> allLeadIds = new List<Id>();
		Map<Id,Lead> taskLead;
		Map<Id,Opportunity> taskOpp;

		for(Task oneTask: allTasks){
			if(oneTask.WhatId != null){
				allOppIds.add(oneTask.WhatId);
			}
			if(oneTask.WhoId != null){
				allLeadIds.add(oneTask.WhoId);
			}
		}

		Map<Id,Opportunity> allOpps = new Map<Id,Opportunity>();
		Map<Id,Lead> allLeads = new Map<Id,Lead>();

		if(!allOppIds.isEmpty()){
			allOpps.putAll([SELECT Id, StageName, Instituicao__r.Name, Status_Interesse_do_contato__c, (SELECT Id FROM Tasks WHERE Status = 'Concluída') FROM Opportunity WHERE Id =: allOppIds]);
		}
		system.debug('map opp '+allOpps);

		if(!allLeadIds.isEmpty()){
			allLeads.putAll([SELECT Id, Numero_de_interacoes__c, Status, (SELECT Id FROM Tasks WHERE Status = 'Concluída') FROM Lead WHERE Id =: allLeadIds]);
		}
		system.debug('map lead '+allLeads);
		for(Lead l:allLeads.values()){
			system.debug('teste: '+l.tasks);
		}
		Map<Id, Opportunity> opp2updateMap = new Map<Id, Opportunity>();
        Map<Id, Lead> lead2updateMap = new Map<Id, Lead>();
		//List<Opportunity> opp2update = new List<Opportunity>();
		//List<Lead> leads2Update = new List<Lead>();
		for(Task actualTask : allTasks){
			system.debug('ID do WhatId: '+actualTask.WhatId);
			system.debug('ID do WhoId: '+actualTask.WhoId);
			if(actualTask.WhatId != null){
				//verifica se é opp
				if(string.valueOf(actualTask.WhatId).substring(0,3) == '006'){
					if(actualTask.Status == 'Concluída'){
						Opportunity auxOpp = new Opportunity();
						auxOpp.Id = actualTask.WhatId;
						//auxOpp.Apex_Context__c = true;
						//Data do último contato
						//Status do Contato
						//Motivo
						//puxa fase para atividade
						if(allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == 'Confirmado' && (actualTask.Status_do_Contato__c == 'Desistente' || actualTask.Status_do_Contato__c == 'Desqualificado')) {
							auxOpp.Status_Interesse_do_contato__c = actualTask.Status_do_Contato__c;
							auxOpp.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional');
						} else if(allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == 'Analisando' && (actualTask.Status_do_Contato__c != 'Novo' && actualTask.Status_do_Contato__c != 'Tentativa')){
							auxOpp.Status_Interesse_do_contato__c = actualTask.Status_do_Contato__c;
							auxOpp.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional2');
						} else if(allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == 'Tentativa' && actualTask.Status_do_Contato__c != 'Novo' && actualTask.Status_do_Contato__c != null){
							auxOpp.Status_Interesse_do_contato__c = actualTask.Status_do_Contato__c;
							auxOpp.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional3');
						} else if(allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == 'Novo' || allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == '' || allOpps.get(actualTask.WhatId).Status_Interesse_do_contato__c == null){
							auxOpp.Status_Interesse_do_contato__c = actualTask.Status_do_Contato__c;
							auxOpp.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional4');
						}

						actualTask.Fase_da_Oportunidade__c = allOpps.get(actualTask.WhatId).StageName;
						//system.debug('Teste da minha condicional: ' +actualTask.Subject.contains('Ligação') );
						system.debug('vamos verificar a string: '+actualTask.Subject);
                        if(!String.isBlank(actualTask.Subject)){
                            if(actualTask.Subject.contains('Ligação')){
                                auxOpp.Data_do_ultimo_contato__c = system.now();
                            }
                        }
						actualTask.IES_do_candidato_oculto__c = allOpps.get(actualTask.WhatId).Instituicao__r.Name;
						auxOpp.Numero_de_interacoes__c = allOpps.get(actualTask.WhatId).tasks.size() + 1;
                        if(opp2updateMap.get(auxOpp.Id) == null)
							opp2updateMap.put(auxOpp.Id, auxOpp);
					}
				}
			}
			if(actualTask.WhoId != null){
				//verifica se é lead

				if(string.valueOf(actualTask.WhoId).substring(0,3) == '00Q'){
					if(actualTask.Status == 'Concluída'){
						Lead auxLead = new Lead();
						auxLead.Id = actualTask.WhoId;
						auxLead.Apex_Context__c = true;
						//PENDENTE CUSTOMIZAÇÃO EM ATIVIDADE
						if(allLeads.get(actualTask.WhoId).Status == 'Confirmado' && (actualTask.Status_do_Contato__c == 'Desistente' || actualTask.Status_do_Contato__c == 'Desqualificado')) {
							auxLead.Status = actualTask.Status_do_Contato__c;
							auxLead.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional');
						} else if(allLeads.get(actualTask.WhoId).Status == 'Analisando' && (actualTask.Status_do_Contato__c != 'Novo' && actualTask.Status_do_Contato__c != 'Tentativa')){
							auxLead.Status = actualTask.Status_do_Contato__c;
							auxLead.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional2');
						} else if(allLeads.get(actualTask.WhoId).Status == 'Tentativa' && actualTask.Status_do_Contato__c != 'Novo' && actualTask.Status_do_Contato__c != null){
							auxLead.Status = actualTask.Status_do_Contato__c;
							auxLead.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional3');
						} else if(allLeads.get(actualTask.WhoId).Status == 'Novo' && actualTask.Status_do_Contato__c != null){
							auxLead.Status = actualTask.Status_do_Contato__c;
							auxLead.Motivo__c = actualTask.Motivo__c;
							system.debug('entrou na condicional4');
						}
						if(!String.isBlank(actualTask.Subject)){
                            if(actualTask.Subject.contains('Ligação')){
                                auxLead.Data_do_ultimo_contato__c = system.now();
                            }
                        }
						auxLead.Numero_de_interacoes__c = allLeads.get(actualTask.WhoId).tasks.size() + 1;
						system.debug('tamanho da lsita: '+allLeads.get(actualTask.WhoId).tasks.size());
						system.debug('numero de interacoes: '+auxLead.Numero_de_interacoes__c);
						system.debug('status: '+allLeads.get(actualTask.WhoId).Status);
                        if(lead2updateMap.get(auxLead.Id) == null)
							lead2updateMap.put(auxLead.Id, auxLead);
					}
				}
			}
		}
		if(opp2updateMap.size() > 0){
			system.debug('lista de opp para atualizar: '+opp2updateMap);
			try{
				List<Database.SaveResult> SR = Database.update(opp2updateMap.values());
				System.debug(SR);
			} catch(DMLException e){
				System.debug(e.getMessage());
			}
		}
		if(lead2updateMap.size() > 0){
			try{
				List<Database.SaveResult> SR = Database.update(lead2updateMap.values());
				System.debug(SR);
			} catch(DMLException e){
				System.debug(e.getMessage());
			}
		}
	}
}