public class ObjectFactory {
    
    public static Account CREATE_AND_GET_ACCOUNT(Account accParam){
                                                     Account acc = CREATE_BASIC_ACCOUNT();
                                                     acc.Bairro__c= accParam.Bairro__c;
                                                     acc.CEP__c=accParam.CEP__c;
                                                     acc.Cidade__c= accParam.Cidade__c ;
                                                     acc.CPF_2__c= accParam.CPF_2__c ;
                                                     acc.DataNascimento__c = accParam.DataNascimento__c;
                                                     acc.Estado__c=  accParam.Estado__c;
                                                     acc.E_portador_de_alguma_aten_o_especial__c= accParam.E_portador_de_alguma_aten_o_especial__c ;
        											 acc.LastName= accParam.LastName;
                                                     acc.N_mero__c=accParam.N_mero__c;
                                                     acc.PersonEmail= accParam.PersonEmail ;
                                                     acc.PersonMobilePhone=accParam.PersonMobilePhone;
        											 acc.Cor_Ra_a__c = accParam.Cor_Ra_a__c;
                                                     acc.Phone=accParam.Phone;
                                                     acc.Rua__c= accParam.Rua__c ;
                                                     acc.Sexo__c= accParam.Sexo__c ;
                                                     acc.Tipo_escola_cursa_ou_cursou_ensino_medio__c= accParam.Tipo_escola_cursa_ou_cursou_ensino_medio__c ;
                                                     acc.Z1_opcao_curso__c= accParam.Z1_opcao_curso__c ;
                                                     acc.Z2_opcao_curso__c= accParam.Z2_opcao_curso__c;
                                                     return acc;
                                                 }
    
    
    
    private static Account CREATE_BASIC_ACCOUNT(){
        Account acc = new Account();
        acc.Agendou_evento_encontro__c=false ;
        acc.Candidato_Iniciado__c= false ;
        acc.Candidato_Revalidado__c= false ;
        acc.Compareceu_ao_Encontro_Entrevista__c= false ;
        acc.Data_de_Matricula_Preenchida__c= false ;
        acc.Duplicidade__c= false ;
        acc.Endere_o_confirmado__c= false ;
        acc.et4ae5__HasOptedOutOfMobile__pc= false ;
        acc.E_mail_confirmado__c= false ;
        acc.I_P__c= false ;
        acc.Lead_QuinStreet__c= false ;
        acc.Nacionalidade__c= 'Brasileira' ;
        acc.Nome_completo_confirmado__c= false ;
        acc.Participa_o_no_Processo_Sel_confirmada__c= 'Sem informação' ;
        acc.Pa_s__c= 'Brasil' ;
        acc.PersonDoNotCall= false ;
        acc.PersonHasOptedOutOfEmail= false ;
        acc.PersonHasOptedOutOfFax= false ;
        acc.PrecisaAtendimentoEspecial__c= 'NAO' ;
        acc.RecordTypeId= Schema.SObjectType.Account.getRecordTypeInfosByName().get('DNA do Aluno').getRecordTypeId();
        acc.Recusa__c= false ;
        acc.Status__c= '1. Inscrito' ;
        acc.Telefone_Invalido_ou_Errado__c= false ;
        return acc;
    }
    
    public static Opportunity CREATE_AND_GET_OPPORTUNITY(Account accParam,Opportunity oppParam){
        Opportunity opp = CREATE_BASIC_OPPORTUNITY();
        opp.Processo_seletivo__c = oppParam.Processo_seletivo__c;
        opp.AccountId= accParam.ID;
        opp.Celular__c= accParam.PersonMobilePhone;
        opp.CloseDate = system.today()+30;
        opp.Data_Nascimento__c= accParam.DataNascimento__c;
        opp.Email__c= accParam.PersonEmail;
        opp.Name= 'inicio';
        opp.Telefone__c = accParam.Phone ;
        opp.X1_OpcaoCurso__c= oppParam.X1_OpcaoCurso__c ;
        opp.X2_Op_o_de_curso__c= oppParam.X2_Op_o_de_curso__c ;
        opp.CPF__C = accParam.CPF_2__c;
        opp.Nota__c = oppParam.Nota__c;        
        opp.Unique__C = opp.Processo_seletivo__r.ID+accParam.CPF_2__c+opp.X1_OpcaoCurso__C+opp.X2_Op_o_de_curso__C;        
        return opp;
    }
        
    private static Opportunity CREATE_BASIC_OPPORTUNITY(){
        Opportunity opp = new Opportunity();        
        opp.Agendou_evento_encontro__c= false ;
        opp.AguardandoRetornoBraspag__c= false ;
        opp.Amount=0;
        opp.CanalInscricao__c= 'Site - Ficha de Inscrição' ;
        opp.Candidato_Iniciado__c= false ;
        opp.Candidato_Revalidado__c= false ;
        opp.CodigoProcessoCandidato__c= '-' ;        
        opp.Compareceu_ao_Encontro_Entrevista__c= false ;
        opp.ContadorAgendamento__c=0;
        opp.Contador_Email__c=0;
        opp.Contador__c=0;        
        opp.Data_de_Matricula_Preenchida__c= false ; 
        opp.Enviar_e_mail_de_contato_para_Candidato__c= false ;
        opp.estrangeiro__c= 'Não' ;
        opp.E_mail_do_Pai_M_e_ou_respons_vel__c= 'shirleytolentino@icloud.com' ;
        opp.FezProvaENEM__c= 'Não' ;
        opp.Foi_Interessado__c= false ;
        opp.ForecastCategoryName= 'Pipeline' ;
        opp.Historico_do_Funil_Aprovados__c= false ;
        opp.Historico_do_Funil_Ausentes__c= false ;
        opp.Historico_do_Funil_Avaliados__c= false ;
        opp.Historico_do_Funil_Confirmados__c= false ;
        opp.Historico_do_Funil_Inscritos__c= false ;
        opp.Historico_do_Funil_Reprovados__c= false ;
        opp.Historico_do_Funil__c= false ;
        opp.InteresseEnglishPro__c= 'Sim' ;
        opp.Por_que_voc_escolheu_nossa_Institui_o__c= 'IBMEC DAY' ;
        opp.Probability=10;
        opp.Quando_deseja_ingressar__c= '2018.1' ;
        opp.RecordTypeId= Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Graduação').getRecordTypeId();
        opp.StageName= 'Inscrito' ;
        opp.TaskName__c='0';
        opp.Treineiro__c= 'Não' ;
        return opp;
    }
    
}