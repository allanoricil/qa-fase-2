global class CraOferta {
    
      public static List<CraOfertasResult> getOfertas( String codcoligada, String ra, String codfilial, string tipo) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
               
            List<CraOfertasResult> matrizesAtivas = new List<CraOfertasResult>();
			
          	if (tipo == 'INCLUSÃO')
                tipo = 'INCLUSAO';
          
            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraOferta').Endpoint__c,new String[]{codcoligada,ra,codFilial});
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraOferta' ).API_Token__c );
                
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setHeader( 'TIPO', tipo == 'INCLUSÃO' ? 'INCLUSAO' : tipo);
                req.setTimeout( 120000 );
            
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraOferta / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraOferta/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }

     private static List <CraOfertasResult>  processJsonBody(String jsonBody ) {
      
        CordResponse SR = ( CordResponse ) JSON.deserializeStrict( jsonBody, CordResponse.class );
      
        if( !SR.CraOfertasResult.isEmpty() ){
           return  SR.CraOfertasResult;
       }
        else{
            return new List<CraOfertasResult>(); 
        }
       
    }
  
     public Class CordResponse {
        public List<CraOfertasResult> CraOfertasResult;
    }
  
    global Class CraOfertasResult {

        public String CodColigada;
        public String CodDisc;
        public String CodTurma;
        public String Horario;
        public String Nome;
        public String Valor;
    }

    

}