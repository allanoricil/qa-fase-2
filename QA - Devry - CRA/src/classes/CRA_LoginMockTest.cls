@isTest
global class CRA_LoginMockTest implements HttpCalloutMock{
	global HttpResponse respond(HttpRequest req){
		//System.assertEquals('login', req.getEndpoint());
		HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('"auth_token": "eeac0aecd45fb1d838e42c4e202e1614"');
        res.setStatusCode(200);
        return res;
	}
}