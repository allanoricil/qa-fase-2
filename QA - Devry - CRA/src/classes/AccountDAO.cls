/* 
    @author Diego Moreira
    @class Classe DAO do objeto Account
*/
public with sharing class AccountDAO extends SObjectDAO {
	
	public static final String RECORDTYPE_EMPRESA = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Empresa').getRecordTypeId();
    public static final String RECORDTYPE_ESCOLA = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Escola').getRecordTypeId();

	/*
        Singleton
    */
    private static final AccountDAO instance = new AccountDAO();    
    private AccountDAO(){}
    
    public static AccountDAO getInstance() {
        return instance; 
    }

    /*
        Busca as contas pelo id
    */
    public List<Account> getAccountById( String accountId ) {
        return [SELECT Id, Name
                FROM Account
                WHERE Id = :accountId];
    }

    /*
		Retorna as contas do tipo empresa
    */
    public List<Account> getAccountEnterprise() {
    	return [SELECT Id, Name
    				FROM Account
    				WHERE RecordTypeId = :RECORDTYPE_EMPRESA];
    }

    /*
        Retorna apenas contas do tipo escola
    */
    public List<Account> getAllColleges() {
        return [SELECT Id, Name 
                    FROM Account
                    WHERE RecordTypeId = :RECORDTYPE_ESCOLA];
    } 
  
    /*
        Retorna todas as contas com lead origem
    */
    public List<Account> getAllAccountsWithLeadOrigin(){
        return [SELECT Id, FirstName, LastName, Nacionalidade__c, RG__c, CPF_2__c, Passaporte__c, VoceEstrangeiro__c, VoceTreineiro__c, LeadOrigem__c,
                Codigo_Especial__c, Sexo__c, Phone, PersonMobilePhone, PersonEmail, DataNascimento__c, Rua__c, N_mero__c, Complemento__c, Bairro__c,
                CEP__c, Cidade__c, Estado__c, OutroColegio__c, Naturalidade__c, NomeCompletoResponsavel__c, CPFdoResponsavel__c, 
                Tipo_escola_cursa_ou_cursou_ensino_medio__c, E_portador_de_alguma_aten_o_especial__c, Z1_opcao_curso__c
                    FROM Account
                    WHERE LeadOrigem__c != null 
                    AND LeadOrigem__r.isConverted = false];
    }

    /*
        Retorna a conta pelo numero do CPF
        @param cpf numero do CPF para consulta
    */
    public List<Account> getAccountByCpf( String cpf ) {
        return [SELECT Id, Name 
                FROM Account
                WHERE CPF_2__c = :cpf];
    }
}