global class Batch_EmailCampanhasCASA implements Database.Batchable<sObject>,Database.Stateful
{
    List<Configura_es_do_Per_odo_Letivo__c> cpls {get; set;}
    List<Atendimento_CASA__c> acs {get; set;}
    List<ID> ids1 {get; set;}
    public String q = '';
    
    global Batch_EmailCampanhasCASA(String conf)
    {
        cpls = new List<Configura_es_do_Per_odo_Letivo__c>();    
        acs = new List<Atendimento_CASA__c>();    
        ids1 = new List<ID>();    
        Boolean aux = true;
        
        Configura_es_do_Per_odo_Letivo__c[] cpls = [Select id From Configura_es_do_Per_odo_Letivo__c Where Per_odo_Letivo__r.Status_do_Per_odo_Letivo__c =: 'Ativo' and id =: conf];
        
        for (Configura_es_do_Per_odo_Letivo__c cpl : cpls){
            ids1.add(cpl.Id);
            q = cpl.id;
        }
   }

   global Database.QueryLocator start(Database.BatchableContext BC)
   {
      String query;
      if ( !Test.isRunningTest() ) query = 'Select id, Fez_a_rematr_cula_no_semestre_seguinte__c , Grupo_de_Risco__c, Status_do_Per_odo_Letivo__c, Owner.Email, Owner.Name, Email_Aluno__c, Id_do_Contato__c, Rematr_cula_Grupos_de_Risco__c, Envio_Rematr_cula_Grupos_de_Risco__c,Rematr_cula_Geral__c, Envio_Rematr_cula_Geral__c, Acompanhamento_de_AP1_e_Faltas__c, Envio_Acompanhamento_de_AP1_e_Faltas__c, Carreiras__c, Envio_Carreiras__c, Prepara_o_para_AP2_e_AP3_com_oficinas__c, Envio_Prepara_o_AP2_e_AP3_com_oficinas__c, Programas_Internacionais__c, Envio_Programas_Internacionais__c, Suporte_Psicol_gico__c, Envio_Suporte_Psicol_gico__c, Acompanhamento_de_AP2_e_AP3__c, Envio_Acompanhamento_de_AP2_e_AP3__c, Pesquisa_de_Empregabilidade__c, Envio_Pesquisa_de_Empregabilidade__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Grupos_de_Risco__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Geral__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP1_e_Falta__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Carreiras__c, Configura_es_do_Per_odo_Letivo__r.Venc_Prepara_o_para_AP2_e_AP3_oficinas__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Programas_Internacionais__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Suporte_Psicol_gico__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP2_e_AP3__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Pesquisa_de_Empregabilidade__c From Atendimento_CASA__c Where Configura_es_do_Per_odo_Letivo__c = \'' + q + '\'';
      else query =                         'Select id, Fez_a_rematr_cula_no_semestre_seguinte__c , Grupo_de_Risco__c, Status_do_Per_odo_Letivo__c, Owner.Email, Owner.Name, Email_Aluno__c, Id_do_Contato__c, Rematr_cula_Grupos_de_Risco__c, Envio_Rematr_cula_Grupos_de_Risco__c,Rematr_cula_Geral__c, Envio_Rematr_cula_Geral__c, Acompanhamento_de_AP1_e_Faltas__c, Envio_Acompanhamento_de_AP1_e_Faltas__c, Carreiras__c, Envio_Carreiras__c, Prepara_o_para_AP2_e_AP3_com_oficinas__c, Envio_Prepara_o_AP2_e_AP3_com_oficinas__c, Programas_Internacionais__c, Envio_Programas_Internacionais__c, Suporte_Psicol_gico__c, Envio_Suporte_Psicol_gico__c, Acompanhamento_de_AP2_e_AP3__c, Envio_Acompanhamento_de_AP2_e_AP3__c, Pesquisa_de_Empregabilidade__c, Envio_Pesquisa_de_Empregabilidade__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Grupos_de_Risco__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Geral__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP1_e_Falta__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Carreiras__c, Configura_es_do_Per_odo_Letivo__r.Venc_Prepara_o_para_AP2_e_AP3_oficinas__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Programas_Internacionais__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Suporte_Psicol_gico__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP2_e_AP3__c, Configura_es_do_Per_odo_Letivo__r.Vencimento_Pesquisa_de_Empregabilidade__c From Atendimento_CASA__c limit 1'; 

      return Database.getQueryLocator(query);
   }

    global void execute(Database.BatchableContext BC, List<sObject> scope){

        List<Task> ts = new List<Task>();    
        List<Atendimento_CASA__c> acx = new List<Atendimento_CASA__c>(); 
        boolean aux = true;   
        // Ids dos modelos de emails enviados nas campanhas abaixo.
        ID emt1 = [Select id From EmailTemplate Where DeveloperName =: 'Apresentacao_CASA_Oficial' limit 1][0].ID;
        ID emt2 = [Select id From EmailTemplate Where DeveloperName =: 'Acompanhamento_de_AP1_e_Faltas_Baixo_Rendimento_new' limit 1][0].ID;
        ID emt3 = [Select id From EmailTemplate Where DeveloperName =: 'Oferta_Oficinas_AP2_new' limit 1][0].ID;
        ID emt4 = [Select id From EmailTemplate Where DeveloperName =: 'Acompanhamento_de_AP2_new' limit 1][0].ID;
        ID emt5 = [Select id From EmailTemplate Where DeveloperName =: 'Rematr_cula_Alunos_Acompanhados_pela_CASA_new' limit 1][0].ID;
               
        for(Sobject s : scope){
            Atendimento_CASA__c ac = (Atendimento_CASA__c)s ;
            system.debug(ac);
            /*if (ac.lastmodifiedbyId != '005A0000000sMPi' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Apresenta_o_Casa__c == null && 
                (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9')){
                List<String> e1 = new List<String>();
                e1.add(ac.Email_Aluno__c);
                system.debug(ac.Owner.Email);
                //EmailHelper.sendEmail(ac.Id_do_Contato__c, emt1, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, e1, null, null);
                Task t1 = new Task(Subject='Apresentacao da CASA', OwnerId=ac.OwnerId, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                t1.ActivityDate = system.today().addMonths(1);
                ts.add(t1);
                ac.Envio_Apresenta_o_Casa__c = system.now();
                
            } 
            if (ac.Acompanhamento_AP1__c == 'sim' && ac.Grupo_de_Risco__c != null && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Acompanhamento_Ap1__c == null){      
                List<String> e1 = new List<String>();
                e1.add(ac.Email_Aluno__c);
                //EmailHelper.sendEmail(ac.Id_do_Contato__c, emt2, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, e1, null, null);                                                            
                Task t2 = new Task(Subject='Acompanhamento AP1', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_AP1__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_AP1__c == null) t2.ActivityDate = system.today().addMonths(1);
                ts.add(t2);            
                ac.Envio_Acompanhamento_Ap1__c = system.now();
                //if (aux) { acx.add(ac); aux = false;}
            }
            if (ac.Oficina_AP2__c == 'sim' && ac.Grupo_de_Risco__c != null && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Oficina_Preparatoria_Ap2__c == null){      
                List<String> e1 = new List<String>();
                e1.add(ac.Email_Aluno__c);
                //EmailHelper.sendEmail(ac.Id_do_Contato__c, emt3, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, e1, null, null);                                                            
                Task t3 = new Task(Subject='Oferta Oficinas Preparatórias AP2', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Oferta_Oficina_AP2__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Oferta_Oficina_AP2__c == null) t3.ActivityDate = system.today().addMonths(1);
                ts.add(t3);            
                ac.Envio_Oficina_Preparatoria_Ap2__c = system.now();
                //if (aux) { acx.add(ac); aux = false;}
            }
            if (ac.Acompanhamento_AP2__c == 'sim' && ac.Grupo_de_Risco__c != null && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Acompanhamento_Ap2__c == null){      
                List<String> e1 = new List<String>();
                e1.add(ac.Email_Aluno__c);
                //EmailHelper.sendEmail(ac.Id_do_Contato__c, emt4, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, e1, null, null);                                                            
                Task t4 = new Task(Subject='Acompanhamento AP2', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_AP2__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_AP2__c == null) t4.ActivityDate = system.today().addMonths(1);
                ts.add(t4); 
                ac.Envio_Acompanhamento_Ap2__c = system.now();
                //if (aux) { acx.add(ac); aux = false;}
            }*/
            if (ac.Rematr_cula_Grupos_de_Risco__c == 'sim' && ac.Grupo_de_Risco__c != null && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Rematr_cula_Grupos_de_Risco__c == null){
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                    ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    //EmailHelper.sendEmail(ac.Id_do_Contato__c, emt5, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, e1, null, null);                                                            
                    Task t5 = new Task(Subject='Rematricula - Alunos do Grupo de Risco', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Grupos_de_Risco__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Grupos_de_Risco__c == null) t5.ActivityDate = system.today().addMonths(1);
                    ts.add(t5); 
                    ac.Envio_Rematr_cula_Grupos_de_Risco__c = system.now();
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Rematr_cula_Geral__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Rematr_cula_Geral__c == null && ac.Fez_a_rematr_cula_no_semestre_seguinte__c == false){      
                if (ac.Grupo_de_Risco__c == 'g10' || ac.Grupo_de_Risco__c == 'g20' || ac.Grupo_de_Risco__c == 'g30' || ac.Grupo_de_Risco__c == 'g40' || ac.Grupo_de_Risco__c == 'g50'){            
                    ac.Envio_Rematr_cula_Geral__c = system.now();
                    Task t6 = new Task(Subject='Rematrícula - Geral', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Geral__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Rematricula_Geral__c == null) t6.ActivityDate = system.today().addMonths(1);
                    ts.add(t6);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Acompanhamento_de_AP1_e_Faltas__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Acompanhamento_de_AP1_e_Faltas__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Acompanhamento_de_AP1_e_Faltas__c = system.now();
                    Task t7 = new Task(Subject='Acompanhamento de AP1 e Faltas', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP1_e_Falta__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP1_e_Falta__c == null) t7.ActivityDate = system.today().addMonths(1);
                    ts.add(t7);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Carreiras__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Carreiras__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Carreiras__c = system.now();
                    Task t8 = new Task(Subject='Carreiras', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Carreiras__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Carreiras__c == null) t8.ActivityDate = system.today().addMonths(1);
                    ts.add(t8);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Prepara_o_para_AP2_e_AP3_com_oficinas__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Prepara_o_AP2_e_AP3_com_oficinas__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Prepara_o_AP2_e_AP3_com_oficinas__c = system.now();
                    Task t9 = new Task(Subject='Preparação para AP2 e AP3 com oficinas', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Venc_Prepara_o_para_AP2_e_AP3_oficinas__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Venc_Prepara_o_para_AP2_e_AP3_oficinas__c == null) t9.ActivityDate = system.today().addMonths(1);
                    ts.add(t9);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Programas_Internacionais__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Programas_Internacionais__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Programas_Internacionais__c = system.now();
                    Task t10 = new Task(Subject='Programas Internacionais', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Programas_Internacionais__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Programas_Internacionais__c == null) t10.ActivityDate = system.today().addMonths(1);
                    ts.add(t10);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Suporte_Psicol_gico__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Suporte_Psicol_gico__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Suporte_Psicol_gico__c = system.now();
                    Task t11 = new Task(Subject='Suporte Psicológico', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Suporte_Psicol_gico__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Suporte_Psicol_gico__c == null) t11.ActivityDate = system.today().addMonths(1);
                    ts.add(t11);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Acompanhamento_de_AP2_e_AP3__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Acompanhamento_de_AP2_e_AP3__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Acompanhamento_de_AP2_e_AP3__c = system.now();
                    Task t12 = new Task(Subject='Acompanhamento de AP2 e AP3', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP2_e_AP3__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Acompanhamento_de_AP2_e_AP3__c == null) t12.ActivityDate = system.today().addMonths(1);
                    ts.add(t12);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            if (ac.Pesquisa_de_Empregabilidade__c == 'sim' && ac.Status_do_Per_odo_Letivo__c == 'Ativo' && ac.Envio_Pesquisa_de_Empregabilidade__c == null){      
                if (ac.Grupo_de_Risco__c == 'g1' || ac.Grupo_de_Risco__c == 'g2' || ac.Grupo_de_Risco__c == 'g3' || ac.Grupo_de_Risco__c == 'g4' || ac.Grupo_de_Risco__c == 'g5' ||
                 ac.Grupo_de_Risco__c == 'g6' || ac.Grupo_de_Risco__c == 'g7' || ac.Grupo_de_Risco__c == 'g8' || ac.Grupo_de_Risco__c == 'g9'){            
                    List<String> e1 = new List<String>();
                    e1.add(ac.Email_Aluno__c);
                    ac.Envio_Pesquisa_de_Empregabilidade__c = system.now();
                    Task t13 = new Task(Subject='Pesquisa de Empregabilidade', OwnerId=ac.OwnerId, ActivityDate=ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Pesquisa_de_Empregabilidade__c, Status='Ligação Pendente', Priority='Normal', WhatId=ac.Id);            
                    if (ac.Configura_es_do_Per_odo_Letivo__r.Vencimento_Pesquisa_de_Empregabilidade__c == null) t13.ActivityDate = system.today().addMonths(1);
                    ts.add(t13);                 
                    //if (aux) { acx.add(ac); aux = false;}
                }
            }
            acx.add(ac);
        }
        insert ts;
        update acx;
    }
    global void finish(Database.BatchableContext BC){
    
    }
    @isTest(SeeAllData=true)
    static void UnitTest1() {

        Configura_es_do_Per_odo_Letivo__c c = new Configura_es_do_Per_odo_Letivo__c();
        Configura_es_do_Per_odo_Letivo__c[] cpls = [Select id From Configura_es_do_Per_odo_Letivo__c Where Per_odo_Letivo__r.Status_do_Per_odo_Letivo__c =: 'Ativo'];
        for (Configura_es_do_Per_odo_Letivo__c cpl : cpls){
            Integer aux = [Select count() From Atendimento_CASA__c Where Configura_es_do_Per_odo_Letivo__c =: cpl.id];
            if (aux > 0){
                c = cpl;
            }   
        }
        //update cpls;
        String conf = c.id;
        system.debug(conf);      
        Test.StartTest();
        Batch_EmailCampanhasCASA bt = new Batch_EmailCampanhasCASA(conf);
        bt.q = ' limit 1';
        Integer batchSize = 1;  
        ID batchprocessid = database.executebatch(bt , batchSize);
        Test.StopTest();
        System.AssertEquals(database.countquery('SELECT COUNT()' +' FROM Account limit 10'), 10);  
   
    }    
}