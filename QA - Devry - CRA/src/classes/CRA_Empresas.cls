/*
@author Willian Matheus
@class Classe para CRA_Empresas
*/
global class CRA_Empresas{

        public static List<Empresas> getEmpresas( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
            List<Empresas> empresas = new List<Empresas>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                req.setEndpoint( WSSetup__c.getValues( 'CraEmpresas' ).Endpoint__c + '?CODCOLIGADA=' + codcoligada+'&RA='+ ra +'&CODFILIAL='+ codfilial);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraEmpresas' ).API_Token__c );
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );

                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                    system.debug('serverres.getBody(): ' +  res.getBody());
                    if( res.getStatusCode() == 200 ){
                        empresas = processJsonBody(res.getBody() );
                        return empresas;
                    }
                    else{
                        system.debug( 'CraEmpresas getStatusCode / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraEmpresas CalloutException / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }


                public static List<Empresas> processJsonBody( String jsonBody ) {

                    List<Empresas> empresas = new List<Empresas>();

        //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe Empresas
        //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
        ServicosResponse SR = ( ServicosResponse ) JSON.deserializeStrict( jsonBody, ServicosResponse.class );

        system.debug(SR + ' destaque1 ');
        if( !SR.CraEmpresasResult.isEmpty() ){
            for( Empresas item : SR.CraEmpresasResult ){
                empresas.add(item);
            }
            system.debug('empresas' + empresas);
            return empresas;
            }else{
                system.debug('lista de response vazia');
                return null;
            }

        }

        public Class ServicosResponse {
            public List<Empresas> CraEmpresasResult;
        }

        global Class Empresas {
            public String codcoligada{get;set;}
            public String codinterno{get;set;}
            public String descricao{get;set;}

        }

    }