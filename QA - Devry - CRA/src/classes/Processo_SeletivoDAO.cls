/*
    @author Diego Moreira
    @class Classe DAO do Processo_seletivo
*/
public with sharing class Processo_SeletivoDAO extends SObjectDAO {
	
	/*
        Singleton
    */
    private static final Processo_SeletivoDAO instance = new Processo_SeletivoDAO();    
    private Processo_SeletivoDAO(){}
    
    public static Processo_SeletivoDAO getInstance() {
        return instance;
    }

    public static final String RECORDTYPE_GRADUACAO = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get( 'Graduação' ).getRecordTypeId();
 
    /*
        Retorna os dados do processo seletivo por Id
        @param processoId Id do processo seletivo
    */
    public List<Processo_seletivo__c> getProcessoSeletivoById( String processoId ) {
        return [SELECT Id, Institui_o_n__c, Valor_Taxa_Inscricao__c, Vencimento_Boleto_dias__c, Instrucoes_Boleto__c,
                Institui_o_n__r.Agencia__c, Institui_o_n__r.Conta__c, Institui_o_n__r.Banco__c, Institui_o_n__r.Digito__c 
                    FROM Processo_Seletivo__c 
                    WHERE Id = :processoId]; 
    }

    /*
		Retorna os processos seletios ativos
		@param instituicaoId Id da instituição para filtro 
    */
    public List<Processo_seletivo__c> getProcessoSeletivosAtivosByInstituicaoId( String instituicaoId ) {
    	String statusAberto = 'Aberto';
       
        List<Processo_seletivo__c> processoSeletivo = new List<Processo_seletivo__c>();       
        processoSeletivo = [SELECT Id, Name, Nome_da_IES__c, Campus__r.Name, Periodo_Letivo__c, Data_Validade_Processo__c, Tipo_de_Processo_n__c, 
                    Data_do_Vestibular__c, Tipo_de_Matr_cula__c, CobrarTaxaInscricao__c, Nome_do_Campus__c,Verifica_Periodo_de_ingresso__c,Verifica_cidade__c,Local_de_prova__c,Quando_Deseja_ingressar__c
                        FROM Processo_seletivo__c
    				    WHERE Institui_o_n__c = :instituicaoId
                        AND Status_Processo_Seletivo__c = :statusAberto
                		AND Data_Validade_Processo__c >= TODAY
                        AND RecordTypeId = :RECORDTYPE_GRADUACAO];
        if(!processoSeletivo.isEmpty())
        {
            return processoSeletivo;
        }
        else
        {
            return null;
        }
	}

    /*
        Retorna os processos seletivos em aberto no data corrente
    */
    public List<Processo_seletivo__c> getProcessoSeletivosAbertos() {
        return [SELECT Id, Name
                FROM Processo_seletivo__c
                WHERE Data_de_encerramento__c >= TODAY
                AND RecordTypeId = :RECORDTYPE_GRADUACAO];    
    }
}