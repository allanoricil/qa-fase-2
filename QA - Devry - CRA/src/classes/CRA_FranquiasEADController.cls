global class CRA_FranquiasEADController{

  public Opportunity opp{get; set;}
  public PageReference oppReturn{get; set;}

  public String canalDeContato{get;set;}
  public String descricaoDaAtividade{get;set;}
  public String statusDoContato{get;set;}
  public List<SelectOption> opcoesDeStatusDoContato{get; private set;}
  public List<SelectOption> opcoesDeCanaisDeContato{get; private set;}

	public CRA_FranquiasEADController(ApexPages.StandardController stdController){
      opp = (Opportunity)stdController.getRecord();
      String oppId = opp.Id;
    	oppReturn = new PageReference('/' + oppId);
      opp = [SELECT Id,
                    Name,
                    Account.Id,
                    Account.Name,
                    Account.Person_Contact_Id__c,
                    Email_form__c,
                    Celular__c,
                    Presente__c,
                    Senha__c,
                    CPF_form__c,
                    TipoProcesso__c,
                    RecordType.Name, 
                    Bolsa__c, 
                    QualNotaMediaENEM__c,
                    StageName, 
                    Nota__c, 
                    Nota_1__c,
                    Nota_2__c,
                    Nota_3__c,
                    Nota_de_Corte__c,
                    Page_Auto_Refresh__c
               FROM Opportunity
              WHERE Id =:oppId];

      //SOLUCAO PROVISORIA
      //O CORRETO SERIA USAR O SCHEMA.DESCRIBE NO CAMPO E VER OS POSSIVEIS VALORES
      opcoesDeCanaisDeContato = new List<SelectOption>();
      opcoesDeCanaisDeContato.add(new SelectOption('-- Nenhum --', '-- Nenhum --'));
      opcoesDeCanaisDeContato.add(new SelectOption('Phone', 'Phone'));
      opcoesDeCanaisDeContato.add(new SelectOption('E-mail', 'E-mail'));
      opcoesDeCanaisDeContato.add(new SelectOption('Face', 'Face'));
      opcoesDeCanaisDeContato.add(new SelectOption('Skype', 'Skype'));

      opcoesDeStatusDoContato = new List<SelectOption>();
      opcoesDeStatusDoContato.add(new SelectOption('-- Nenhum --', '-- Nenhum --'));
      opcoesDeStatusDoContato.add(new SelectOption('Sim', 'Sim'));
      opcoesDeStatusDoContato.add(new SelectOption('Caixa Postal', 'Caixa Postal'));
      opcoesDeStatusDoContato.add(new SelectOption('Não Atende', 'Não Atende'));
      opcoesDeStatusDoContato.add(new SelectOption('Não se Encontra', 'Não se Encontra'));
      opcoesDeStatusDoContato.add(new SelectOption('Reagendou', 'Reagendou'));
      opcoesDeStatusDoContato.add(new SelectOption('Número Errado', 'Número Errado'));
	}

	public PageReference confirmarPresenca(){
  		if(isAbleToConfirmPresence()){
          opp.Evolucional_Username__c = opp.Email_form__c;
          opp.Senha__c = constroiSenhaUsandoCpf(opp.CPF_form__c);
          opp.Presente__c = true;
          opp.Page_Auto_Refresh__c = true;
          update opp;
          String jsonRequest = EvolucionalJsonBuilder.buildRequestForUserCreationService(opp);
          String serviceName = QueueEventNames.EVOLUCIONAL_SERVICES.name();
          Queue__c fila = EvolucionalQueueFactory.buildQueue(opp.Id,jsonRequest,serviceName);
          QueueDAO.getInstance().insertData(fila);
          EvolucionalCreateUser.processingQueue(fila.Id, fila.Payload__c);
      }
      return oppReturn;
	}

  public PageReference recuperarResultados(){
      if(isAbleToFetchResults()){
          String oppId = opp.Id;
          opp.Page_Auto_Refresh__c = true;
          update opp;
          Queue__c queue = [SELECT Id,
                                   PayloadSAP__c,
                                   CreatedDate
                              FROM Queue__c 
                             WHERE Oportunidade__c=:oppId
                                   AND Status__c = 'SUCCESS'
                          ORDER BY CreatedDate DESC
                             LIMIT 1];
          if(queue != null){
            EvolucionalListUserExamResults.processingQueue(queue.Id, queue.PayloadSAP__c);
          }
      }
      return oppReturn;
  }


  public PageReference criarAtividade(){
      opp.Data_do_ultimo_contato__c = Date.today();
      Task atividade = new Task();

      atividade.whatid = opp.Id;
      atividade.whoid = opp.Account.Person_Contact_Id__c;
      atividade.OwnerId = UserInfo.getUserId();
      atividade.Subject = 'Atividade';
      atividade.Status = 'Concluída';
      atividade.Canal_de_contato__c = canalDeContato;
      atividade.Conseguiu_Contato__c = statusDoContato;
      atividade.Description = descricaoDaAtividade;

      insert atividade;
      opp.Page_Auto_Refresh__c = true;

      update opp;
      return oppReturn;
  }

  public pageReference goBack(){
      return oppReturn;
  }

  private Boolean isAbleToConfirmPresence(){
      return !opp.Presente__c && !opp.RecordType.Name.equals('ENEM');
  }

  private Boolean isAbleToFetchResults(){
      return opp.Presente__c && !opp.RecordType.Name.equals('ENEM');
  }

  //APESAR DA CLASSE TER SIDO REFATORADA, ESSE METODO FOI MANTIDO
  //TODO REMOVER ESSE METODO!!!!!!!!
  private String constroiSenhaUsandoCpf(String cpf){
    if(cpf != null){
        return 'UNIFAVIP@' + cpf.replace('.','').substring(0,4);
    }
    return null;
  }

}