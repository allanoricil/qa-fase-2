global class CraProgramaDisc {
	public static List<ProgramaDisc> getProgramaDisc( String codcoligada, String ra, String idhabilitacaofilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
               
            //List<CraOfertaQuebraResult> inclusaoQuebraList = new List<CraOfertaQuebraResult>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraProgramaDisc').Endpoint__c,new String[]{codcoligada,ra,idhabilitacaofilial});
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraProgramaDisc' ).API_Token__c );
                
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );
            
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraProgramaDisc / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraProgramaDisc/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }

     private static List <ProgramaDisc>  processJsonBody(String jsonBody ) {
        
        system.debug('jsonbody ProgramaDisc: ' + jsonBody);
      
        Response SR = ( Response ) JSON.deserializeStrict( jsonBody, Response.class );
         
       	system.debug('SR::: ' + SR);
      
        if( !SR.CraProgramaDiscResult.isEmpty() ){
           return  SR.CraProgramaDiscResult;
       }
        else{
            return new List<ProgramaDisc>(); 
        }
       
    }
  
     public Class Response {
        public List<ProgramaDisc> CraProgramaDiscResult;
    }
  
    global Class ProgramaDisc {

        public String CodColigada{get;set;}
        public String CodDisc{get;set;}
        public String RA{get;set;}
        public String CodTurma{get;set;}
        public String IdTurmaDisc{get;set;}
        public String IdHabilitacaoFilial{get;set;}
        public String CargaHoraria{get;set;}
        public String Horario{get;set;}
        public String Disciplina{get;set;}
        public String PeriodoLetivo{get;set;}
        public String Nome{get;set;}
        public String Tipo{get;set;}
        public String Valor{get;set;}
    }
}