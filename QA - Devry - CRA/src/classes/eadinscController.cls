public class eadinscController {

	public List<Processo_Seletivo__c> processoSeletivo{
		get{
				return [Select id, Tipo_de_Matr_cula__c from Processo_Seletivo__c 
						where Campus__r.Name = 'Unifavip EAD' and Status_Processo_Seletivo__c = 'Aberto'];
			}	set;}


	public String stage { get; set;}
	public List<SelectOption> estadoPoloOptions 	  { get; set; }
	public List<SelectOption> poloOptions 			  { get; set; }
	public List<SelectOption> cursoOptions 			  { get; set; }


	public String oppid 							  { get; set; }

	public String cpf								  { get; set; }
	public String nome								  { get; set; }	
	public String sobrenome							  { get; set; }
	public String email								  { get; set; }	
	public String phone								  { get; set; }	
	public String birthdate							  { get; set; }		
	public String birth_country						  { get; set; }			
	public String state								  { get; set; }	
	public String city								  { get; set; }	
	public String disability						  { get; set; }			
	public String disability_need					  { get; set; }				
	public String enem_year							  { get; set; }		
	public String enem_score						  { get; set; }			
	public String offer								  { get; set; }
	public String gender 							  { get; set; }		

	public String estado 							  { get; set; }
	public String processo 							  { get; set; }
	public String curso 							  { get; set; }
	public String polo 								  { get; set; }

	public eadinscController() {
		this.stage = 'ficha';
		getEstadoPolos();
	}


	public void getEstadoPolos(){
		estadoPoloOptions = new List<SelectOption>();
		for(Polo__c p : [Select Estado__c from Polo__c])
			estadoPoloOptions.add(new SelectOption(p.Estado__c, p.Estado__c));
	}

	public void getPoloByEstado(){
		poloOptions = new List<SelectOption>();
		for(Polo__c p : [Select Name, Cidade__c from Polo__c where Estado__c =: estado]){
			poloOptions.add(new SelectOption(p.Id, p.Cidade__c + ' - Polo ' + p.Name));
		}
	}

	public void getCursos(){
		cursoOptions = new List<SelectOption>();
		for(Oferta__c o : [Select Id, Name from Oferta__c where Processo_seletivo__c =: processo])
			cursoOptions.add(new SelectOption(o.Id, o.Name));
	}

	public void execute(){
		Account acc = new Account ();

			try{
				acc.RecordTypeId = '012A0000000oGh1IAE';
				acc.FirstName = nome;
				acc.LastName = sobrenome;
				acc.PersonEmail  = email;
				acc.CPF_2__c = cpf;
				acc.PersonMobilePhone  = phone;
				acc.DataNascimento__c  = GetDateByString(birthdate);
				acc.Nacionalidade__c  = birth_country;
				acc.Estado__c  = state;
				acc.Cidade__c  = city;
				acc.Processo_seletivo__c = processo;
				acc.E_portador_de_alguma_aten_o_especial__c  = disability;
				acc.PrecisaAtendimentoEspecial__c  = disability_need;
				acc.Z1_opcao_curso__c  = curso;
				acc.Z2_opcao_curso__c  = curso;
				acc.Sexo__c = gender;

				insert acc;

				Processo_seletivo__c ps = [Select id,Institui_o_n__c,Campus__c from Processo_Seletivo__c where id =: processo];

				Opportunity opp = new Opportunity();
				opp.AccountId 	= acc.id;

				opp.Name  = 'EAD - INSC';
				opp.Primeiro_Nome__c = nome;
				opp.Sobrenome__c = sobrenome;
				opp.Email__c  = email;
				opp.CPF__c = cpf;
				opp.Celular__c  = phone;
				opp.Nacionalidade__c  = birth_country;
				opp.Estado__c  = state;
				opp.Cidade__c  = city;
				opp.QualNotaMediaENEM__c = enem_score != '' ? integer.ValueOf(enem_score) : 0;
				opp.AnoProvaENEM__c = enem_year != '' ? integer.ValueOf(enem_year) : 0;
				opp.Processo_seletivo__c = ps.Id;
				opp.E_portador_de_alguma_aten_o_especial__c  = disability;
				opp.X1_OpcaoCurso__c  = curso;
				opp.X2_Op_o_de_curso__c  = curso;
				opp.Closedate  = system.today();
				opp.EAD__c ='Sim';
				opp.Polo__c = polo;
				insert opp;
				this.oppid = opp.Id;
				
			}catch(Exception e){
				e.getMessage();
				e.getCause();
			}

			
			this.stage = 'finish';

	}

	public void stage2(){
		this.stage = 'finish';
	}

	public Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(0)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }
}