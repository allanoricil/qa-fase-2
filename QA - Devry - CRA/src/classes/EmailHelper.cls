Global class EmailHelper {

    public static void sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, 
                                String ReplyTo, String SenderDisplayName, String[] ToAddresses, String Subject, String PlainTextBody) {
    
        //New instance of a mass email message
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
         
        // Who you are sending the email to
        mail.setTargetObjectId(recipient);
        mail.setToAddresses(ToAddresses);
    
        // The email template ID used for the email
        if (TemplateId != null) mail.setTemplateId(TemplateId);
              
        if(WhatObjectId != null) mail.setWhatId(WhatObjectId);    
        mail.setBccSender(BccSender);
        mail.setUseSignature(UseSignature);
        mail.setReplyTo(ReplyTo);
        mail.setSenderDisplayName(SenderDisplayName);
        if(recipient != null && WhatObjectId != null) mail.setSaveAsActivity(true);
        else mail.setSaveAsActivity(false);
        if(Subject != null) mail.setSubject(Subject);
        if(PlainTextBody != null) mail.setPlainTextBody(PlainTextBody);
         
        if(recipient != null && ToAddresses != null) Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });    
    
    }  
}