global class CRA_AutoCloseCaseBatch implements Database.Batchable<sObject> {		
	
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator('SELECT Id FROM Case WHERE (Status = \'Concluído\' AND  ClosedDate < LAST_N_DAYS:2) OR (Status = \'Aguardando pagamento\' AND Data_Limite_Pagamento__c < TODAY)');

	}

   	global void execute(Database.BatchableContext BC, List<Case> scope) {

		for(Case c : scope){    
		    
		    c.Apex_context__c = true;
		    c.Status = 'Encerrado';
		    c.Sub_Status__c = 'Encerrado automaticamente';

		}
		
		if(!scope.isEmpty())
			Database.update(scope, false);
		
	}
	
	global void finish(Database.BatchableContext BC) {
		
	}
	
}