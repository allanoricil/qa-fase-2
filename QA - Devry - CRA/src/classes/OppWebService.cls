global with sharing class OppWebService
{
    webService static string CreateOpp(String pnomeOpp,
                                   String pnomeCandidato,
                                   String pSobreNomeCandidato,
                                   String pofertaID,
                                   String popcaoCurso2,
                                   String pRG,
                                   String pPassporte,
                                   String pTreineiro,
                                   String pCPF,
                                   String psexo,
                                   String ptelefone,
                                   String pemail,
                                   Date pdataNasc,
                                   String pRua,
                                   String pcasaNumero,
                                   String pcomplemeto,
                                   String pbairro,
                                   String pcep,
                                   String pcidade,
                                   String pestado,
                                   String pnaturalidade,
                                   String pnacionalidade,
                                   String pNoPai,
                                   String pNoMae,
                                   String pEmpresaParceira,
                                   String pcodigoEspecial,
                                   String pTipoEscola,
                                   String pProcessoSeletivoId,
                                   String pIdParticipacao,
                                   String Statusboleto)
    {
        try{
                Opportunity opp;
                //RecordType rt = [select id,Name from RecordType where SobjectType='Account' and Name='A' Limit 1];
                
                if(String.isNotEmpty(popcaoCurso2) && String.isNotBlank(popcaoCurso2))
                {
                opp = new Opportunity(Name = pnomeOpp, 
                                      Primeiro_Nome__c = pnomeCandidato,
                                      Sobrenome__c = pSobreNomeCandidato,
                                      RecordType  = [select id from RecordType where SobjectType='opportunity' and Name='vestibular tradicional' Limit 1],
                                      Oferta__c = pofertaID,
                                      X2_Op_o_de_curso__c = popcaoCurso2,
                                      RG__c = pRG,
                                      Passaporte__c = pPassporte,
                                      Treineiro__c = pTreineiro,
                                      CPF__c = pCPF,
                                      Sexo__c = psexo,
                                      Telefone__c = ptelefone,
                                      Email__c = pemail,
                                      Data_Nascimento__c = pdataNasc,
                                      Rua__c = prua,
                                      N_mero__c = pcasaNumero,
                                      Complemento__c = pcomplemeto,
                                      Bairro__c = pbairro,
                                      CEP__c = pcep,
                                      Cidade__c = pcidade,
                                      Estado__c = pestado,
                                      Naturalidade__c = pnaturalidade,
                                      Nacionalidade__c = pnacionalidade,
                                      Nome_do_pai__c = pnoPai,
                                      Nome_da_Mae__c = pnoMae,
                                      Empresa_Parceira__c = pEmpresaParceira,
                                      Codigo_Especial__c = pcodigoEspecial,
                                      Tipo_escola_cursa_ou_cursou_ensino_medio__c = pTipoEscola,
                                      Processo_seletivo__c = pProcessoSeletivoId,
                                      ID_Participa_o__c = pIdParticipacao,
                                      Taxa_Paga_Isento__c = Statusboleto);
                                          
            }else
            {
                 opp = new Opportunity(Name = pnomeOpp, 
                                       Primeiro_Nome__c = pnomeCandidato,
                                       Sobrenome__c = pSobreNomeCandidato,
                                       RecordType  = [select id from RecordType where SobjectType='opportunity' and Name='vestibular tradicional' Limit 1],
                                       Oferta__c = pofertaID,
                                       RG__c = pRG,
                                       Passaporte__c = pPassporte,
                                       Treineiro__c = pTreineiro,
                                       CPF__c = pCPF,
                                       Sexo__c = psexo,
                                       Telefone__c = ptelefone,
                                       Email__c = pemail,
                                       Data_Nascimento__c = pdataNasc,
                                       Rua__c = prua,
                                       N_mero__c = pcasaNumero,
                                       Complemento__c = pcomplemeto,
                                       Bairro__c = pbairro,
                                       CEP__c = pcep,
                                       Cidade__c = pcidade,
                                       Estado__c = pestado,
                                       Naturalidade__c = pnaturalidade,
                                       Nacionalidade__c = pnacionalidade,
                                       Nome_do_pai__c = pnoPai,
                                       Nome_da_Mae__c = pnoMae,
                                       Empresa_Parceira__c = pEmpresaParceira,
                                       Codigo_Especial__c = pcodigoEspecial,
                                       Tipo_escola_cursa_ou_cursou_ensino_medio__c = pTipoEscola,
                                       Processo_seletivo__c = pProcessoSeletivoId,
                                       ID_Participa_o__c = pIdParticipacao,
                                       Taxa_Paga_Isento__c = Statusboleto);
            }
                insert opp;
                return opp.Id;
        }catch(Exception ex)
        {
            Error_Log__c e = new Error_Log__c(Source_Code__c = pIdParticipacao,
                                              Details__c = String.valueOf(ex) + 
                                              ' Processo Seletivo:' + String.valueOf(pProcessoSeletivoId) +
                                              ' Oferta:' + String.valueOf(pofertaID) +
                                              ' Oferta2:' + String.valueOf(popcaoCurso2));
            insert e;
            return String.valueOf(ex);
        }
    }
    
    webService static String UpdateOppById(string pid, string pstagename)
    {
        Opportunity opp;
        
        try
        {
        	opp = [select o.id, o.stagename from Opportunity o where o.id =: pid];
        
        	opp.stagename = pstagename;
        
        	update opp;
            return 'Atualizado com Sucesso';
        }catch(Exception ex)
        {
            Error_Log__c e = new Error_Log__c(Source_Code__c = pid, Details__c = String.valueOf(ex) + 
            									                                     ' Fase: ' + String.valueOf(pstagename) ) ;
            insert e;
            return String.valueOf(ex);
        }
        
        
    }
    
    webService static String UpdateOppByIdPart(string pidPart, string pstagename)
    {
        Opportunity opp;
        try
        {
        	opp = [select o.id, o.stagename from Opportunity o where o.ID_Participa_o__c =: pidPart];
        
        	opp.stagename = pstagename;
        
        	update opp;
            return 'Atualizado com Sucesso';
        }catch(Exception ex)
        {
            Error_Log__c e = new Error_Log__c(Source_Code__c = pidPart, Details__c = String.valueOf(ex) + 
                                              ' Fase: ' + String.valueOf(pstagename) ) ;
            insert e;
            return String.valueOf(ex);
        }
    }
    
}