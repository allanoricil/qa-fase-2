public with sharing class CRA_Search4Leads {
    public static void searchLeads(List<Opportunity> allOpps) {
        //List<Lead> allLeads = new List<Lead>();
        List<Lead> leads2Convert = new List<Lead>();
        Map<Id,Id> mapLeadAcc = new Map<Id,Id>();
        Map<String, Opportunity > mapEmailOpp = new Map<String,Opportunity>();
        List<String> allEmails = new List<String>();
        List<String> allTipoDoCurso = new List<String>();
        list<Database.LeadConvert> leadConverts = new list<Database.LeadConvert>();

        for(Opportunity actualOpp : allOpps){
            if(actualOpp.Email_form__c != '' && actualOpp.Email_form__c != null) {
                allEmails.add(actualOpp.Email_form__c);
                if(mapEmailOpp.get(actualOpp.Email_form__c+actualOpp.Tipo_do_curso__c) == null){
                    mapEmailOpp.put(actualOpp.Email_form__c+actualOpp.Tipo_do_curso__c,actualOpp);
                } 
            }
        }

        if(allEmails.size() > 0){
            //Map<Id, Lead> allLeads = new Map<id, Lead>([SELECT Id, Email, Produto__c FROM Lead WHERE Email =: allEmails]);
            List<Lead> allLeads = new List<Lead>();
            system.debug('minha lista de emails: '+allEmails);
            allLeads = [SELECT Id, Email, Produto__c FROM Lead WHERE Email =: allEmails AND Status != 'Convertido'];
            for(Lead actualLead : allLeads){
                if(mapEmailOpp.get(actualLead.Email+actualLead.Produto__c) != null){
                    leads2Convert.add(actualLead);
                    if(mapLeadAcc.get(actualLead.Id) == null){
                        mapLeadAcc.put(actualLead.Id,mapEmailOpp.get(actualLead.Email+actualLead.Produto__c).AccountId);
                    }
                }
            }
        }
        system.debug('Lista de Leads para conversão: '+leads2Convert);

        if(!leads2Convert.isEmpty()){
            for(Lead myLead : leads2Convert){
                Database.LeadConvert lc = new database.LeadConvert();
                lc.setLeadId(myLead.Id);
                lc.convertedStatus = 'Convertido';
                //Database.ConvertLead(lc,true);
                lc.setDoNotCreateOpportunity(true);
                lc.setAccountId(mapLeadAcc.get(myLead.Id));
                leadConverts.add(lc);
            }
        }

        if(!leadConverts.isEmpty()){
            for(Integer i = 0; i <= leadConverts.size()/100 ; i++){
                list<Database.LeadConvert> tempList = new list<Database.LeadConvert>();
                Integer startIndex = i*100;
                Integer endIndex = ((startIndex+100) < leadConverts.size()) ? startIndex+100: leadConverts.size();
                for(Integer j=startIndex;j<endIndex;j++){
                    tempList.add(leadConverts[j]);
                }
                Database.LeadConvertResult[] lcrList = Database.convertLead(tempList, false);
                system.debug('meu resultado da conversão: '+ lcrList);
                //for(Database.LeadConvertResult lcr : lcrList)
                //  System.assert(lcr.isSuccess());
            }
        }
    }
}