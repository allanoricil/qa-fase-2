/*
    @author Diego Moreira
    @class Classe controladora da pagina de pesquisa de aluno
*/
public with sharing class SRM_PesquisaInscricaoController{
	public List<PesquisaTO> inscricoes { get; set; }
	public String tipoDocumento { get; set; }
    public String valorPesquisa { get; set; }
    
	/*
        Construtor
    */
	public SRM_PesquisaInscricaoController() {
    }
    
	/*
        Consulta inscrições ja feitas
        @action botão Pesquisa 
    */
    public void consultaInscricoes() {
    	if( valorPesquisa.equals('') ) {
    		ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.info, 'É necessario inserir um valor para pesquisa!' );
            Apexpages.addMessage( msg );
            return;
    	}

        this.inscricoes = new List<PesquisaTO>(); 
        //for( Opportunity opportunity : OpportunityDAO.getInstance().getOpportunityByWhereSting( tipoDocumento + ' = ' + '\'' + valorPesquisa + '\'' ) )
        String queryString = '';
        String instituicao = ApexPages.currentPage().getParameters().get('instId');
        if(instituicao != null && instituicao != ''){
            instituicao = instituicao.substring(0,15);
            if(instituicao.length() == 15){
                queryString = 'SELECT Id, AguardandoRetornoBraspag__c, TotalTitulos__c,	Processo_seletivo__r.Periodo_Letivo__c, Account.Name, Agendamento__r.Id, Agendamento__r.Data_Hora_Inicial__c, Processo_seletivo__r.Name, ' +
                    'Processo_seletivo__r.Nome_da_IES__c, Processo_seletivo__r.Data_do_Vestibular__c ,Oferta__r.Name, StageName, Processo_seletivo__r.CobrarTaxaInscricao__c, Amount, Processo_seletivo__r.Tipo_de_Processo_n__c,Cidade_onde_realizar_a_prova__c,Account.PersonEmail,' + 
                    'NomeCampus__c,CodigoProcessoCandidato__c,Processo_seletivo__r.Tipo_de_Matr_cula__c    FROM Opportunity WHERE ' + ' Account.CPF_2__c = ' + '\'' + valorPesquisa + '\'' +
                    ' AND IdInstituicao__c = ' + '\'' + instituicao + '\'';           
        	}
        }
            
        if(queryString == ''){
            queryString = 'SELECT Id, Account.CPF_2__c,AguardandoRetornoBraspag__c, TotalTitulos__c, Processo_seletivo__r.Periodo_Letivo__c,Account.Name, Agendamento__r.Id, Agendamento__r.Data_Hora_Inicial__c, Processo_seletivo__r.Name, ' + 
                'Processo_seletivo__r.Nome_da_IES__c, Processo_seletivo__r.Data_do_Vestibular__c, Oferta__r.Name, StageName, Processo_seletivo__r.CobrarTaxaInscricao__c, Amount, Cidade_onde_realizar_a_prova__c,Processo_seletivo__r.Tipo_de_Processo_n__c ,Account.PersonEmail,  ' + 
                'NomeCampus__c,CodigoProcessoCandidato__c,Processo_seletivo__r.Tipo_de_Matr_cula__c   FROM Opportunity WHERE ' + ' Account.CPF_2__c = ' + '\'' + valorPesquisa + '\'' ;
        }
            
        System.debug('>>> ' + queryString);                        
        List<Opportunity> lista = Database.query( queryString );
        
        for( Opportunity opportunity : lista)            
        //for( Opportunity opportunity : OpportunityDAO.getInstance().getOpportunityByWhereSting( ' Account.CPF_2__c = ' + '\'' + valorPesquisa + '\'' ) )
        
        	this.inscricoes.add( new PesquisaTO( opportunity ) );

        if( inscricoes.isEmpty() ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.info, 'Nenhum registro encontrado!' );
            Apexpages.addMessage( msg );
        }
    }

    /*
		Direciona para pagamento
		@action Link Efetuar pagamento
    */
    public PageReference efetuarPagamento() {
    	PageReference pagInscricao = Page.SRM_PagamentoInscricao;

        Map<String,String> parameters = pagInscricao.getParameters();
        parameters.put( 'id', ApexPages.currentPage().getParameters().get('opportunityId') );
        return pagInscricao;
    }

    /*
		Direciona para gerar novamente o boleto
		@action Link 2° via do boleto
    */
    public PageReference segundaViaBoleto() {
    	Titulos__c titulo = TituloDAO.getInstance().getTitulosById( ApexPages.currentPage().getParameters().get('tituloAbertoId') )[0];

    	PageReference braspag = Page.SRM_Braspag;

        Map<String,String> parameters = braspag.getParameters();
        //ALTER: MERCHANTID
		/*
        parameters.put( 'Id_Loja', 				titulo.ProcessoSeletivo__r.Merchant__c );
		*/
        parameters.put( 'Id_Loja', 				titulo.ProcessoSeletivo__r.Merchant_Id__c );
        parameters.put( 'VENDAID', 				titulo.Id );
        parameters.put( 'NOSSONUMERO',          titulo.NossoNumero__c );
        //parameters.put( 'VALOR', 				String.valueOf( titulo.Inscricao__r.Amount ) );
        parameters.put( 'VALOR', 				String.valueOf( titulo.Valor__c ) );
        parameters.put( 'NOME', 				titulo.Candidato_Aluno__r.Name );
        parameters.put( 'CPF', 					titulo.Candidato_Aluno__r.Cpf_2__c );
        parameters.put( 'CODPAGAMENTO', 		titulo.CodigoMeioPagamento__c );
        parameters.put( 'INSTRUCOESBOLETO',     titulo.Instrucao__c );
        //parameters.put( 'PARCELAS', 			titulo.Parcelado__c );
        parameters.put( 'PARCELAS', 			'1' );
        //parameters.put( 'TIPOPARCELADO', 		'0' );
        parameters.put( 'TIPOPARCELADO', 		titulo.ProcessoSeletivo__r.Matr_cula_Online__c == 'Sim' ?  Braspag__c.getValues( 'TIPOPARCELADOSEMJUROS' ).Valor__c : Braspag__c.getValues( 'TIPOPARCELADO' ).Valor__c );
        parameters.put( 'TRANSACTIONTYPE', 		'2' );
        parameters.put( 'TRANSACTIONCURRENCY', 	'BRL' );
        parameters.put( 'TRANSACTIONCOUNTRY', 	'BRA' ); 
        parameters.put( 'EXTRADYNAMICURL', 		Label.SRM_URL_INSCRICAO_ALUNO + titulo.Instituicao__c );
        return braspag;
    }

    /*
		Objeto retorno de pesquisa
    */
    public class PesquisaTO {
    	public String candidato 			{ get; private set; }
        public String cpf 			        { get; private set; }
    	public String processoSeletivo 		{ get; private set; }
        public String oferta		 		{ get; private set; }
    	public String instituicaoEnsino 	{ get; private set; }
    	public Date dataProva				{ get; private set; }
    	public String status 				{ get; private set; }
    	public Boolean isPagamento			{ get; private set; }
    	public Boolean is2ViaBoleto			{ get; private set; } 
        public Boolean isAguardandoConfirm  { get; private set; } 
    	public String tituloAbertoId 		{ get; private set; }
    	public String opportunityId 		{ get; set; }
        public String msgPagamento          { get; set; }
        public Boolean panelPayment         { get; set; }
    	private List<Titulos__c> titulosAbertos = new List<Titulos__c>();
        public Boolean isAgendamento        { get; set; }
        public String localProva            {get;set;}
        public String email                 {get;set;}
        public String Campus                {get;set;}
        public String CodigoProcessoCandidato {get;set;}
        public String tipoMatricula {get;set;}
        public String perLet{get;set;}
    	/*
        	Construtor 
    	*/
    	public PesquisaTO( Opportunity opportunity ) {
            //isAgendamento = false;

            //if(opportunity.Processo_seletivo__r.Name.contains('Agendado'))
                isAgendamento = true;
            localProva='';
          if(opportunity.Cidade_onde_realizar_a_prova__c !=null)
               localProva=opportunity.Cidade_onde_realizar_a_prova__c;
    		Boolean isBoletoAberto = existeBoletoEmAberto( opportunity.Id );
               perLet=opportunity.Processo_seletivo__r.Periodo_Letivo__c;
    		Boolean temTitulo = opportunity.TotalTitulos__c > 0 ? true : false;
            tipoMatricula=opportunity.Processo_seletivo__r.Tipo_de_Matr_cula__c;
            panelPayment = opportunity.Processo_seletivo__r.CobrarTaxaInscricao__c;

            if(temTitulo) {

            if(titulosAbertos[0].MeioPagamento__c == 'Cartão')
                isBoletoAberto = false;

            if((titulosAbertos[0].DescricaoRetornoBrasPag__c != null) && (titulosAbertos[0].Status__c != 'Pago')){
                msgPagamento = 'Pagamento não efetuado: ' + titulosAbertos[0].DescricaoRetornoBrasPag__c;
            }
            if(titulosAbertos[0].Status__c == 'Pago'){
                msgPagamento = 'Pagamento Efetuado com Sucesso';
                isBoletoAberto = false;
                panelPayment = false;
            }

            if((titulosAbertos[0].Status__c == 'Em Aberto') && (titulosAbertos[0].DescricaoRetornoBrasPag__c == null))
                msgPagamento = 'Aguardando Pagamento. Se você já o realizou aguarde a confirmação.';
            }else{
                msgPagamento = 'Aguardando Pagamento. Se você já o realizou aguarde a confirmação.';
            } 
            CodigoProcessoCandidato= opportunity.CodigoProcessoCandidato__c;
    		opportunityId		= opportunity.Id;	 
    		candidato 			= opportunity.Account.Name.remove('\'');
            oferta			 	= opportunity.Oferta__r.Name;
    		processoSeletivo 	= opportunity.Processo_seletivo__r.Name;
    		instituicaoEnsino 	= opportunity.Processo_seletivo__r.Nome_da_IES__c;
    		dataProva 			= opportunity.Agendamento__r.Id != null ? Date.valueOf(opportunity.Agendamento__r.Data_Hora_Inicial__c) : opportunity.Processo_seletivo__r.Data_do_Vestibular__c;
    		status 				= opportunity.StageName;
    		isPagamento 		= opportunity.StageName == 'Inscrito' ? false : opportunity.AguardandoRetornoBraspag__c == true ? false : temTitulo == false ? true : isBoletoAberto == false ? true : false;
    		is2ViaBoleto		= opportunity.StageName == 'Inscrito' ? false : isBoletoAberto == true ? true : false;
    		isAguardandoConfirm = opportunity.StageName == 'Inscrito' ? false : opportunity.AguardandoRetornoBraspag__c == true && isBoletoAberto == false ? true : false;
            tituloAbertoId 		= titulosAbertos.isEmpty() ? null : titulosAbertos[0].Id;
       
            cpf                 = opportunity.Account.CPF_2__c!=null?opportunity.Account.CPF_2__c:'';
            
           // system.debug('email '+opportunity.Email_form__c);
            email               = opportunity.Account.PersonEmail !=null ?String.valueOf(opportunity.Account.PersonEmail):'';
            campus=       opportunity.NomeCampus__c!=null? opportunity.NomeCampus__c:'';
    		System.debug('>>> ' + is2ViaBoleto + ' / ' + isPagamento);
    	}

    	/*
			Verifica se existe algum boleto em aberto para inscrição 
    	*/ 
    	private Boolean existeBoletoEmAberto( String opportunityId ) {
    		titulosAbertos = TituloDAO.getInstance().getTitulosAbertoByOportunidadeId( opportunityId ); 
    		if( titulosAbertos.size() > 0 ) return true;
    		return false; 
    	} 
    }
}