public without sharing class CRA_CancelCaseController {
	public Case myCase{set;get;}
    public Boolean error{set;get;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CRA_CancelCaseController(ApexPages.StandardController stdController) {
        
        if(!Test.isRunningTest()){
        	List<String> myFields = new List<String>();
            myFields.add('Id');
            myFields.add('Status');
            stdController.addFields(myFields);
        }
        
        this.myCase = (Case)stdController.getRecord();
        if(myCase.Status != 'Em andamento' && myCase.Status != 'Aguardando aluno' && myCase.Status != 'Aguardando pagamento'){
            this.error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Essa solicitação não pode mais ser cancelada neste momento.'));
        }
    }

    
    public PageReference returnToCase(){
        PageReference pageRef = new pageReference('/' + myCase.Id);
        return pageRef;
    }

    public PageReference finish(){
        if(myCase.Status == 'Aguardando pagamento'){
            estornaCobrancas(myCase.Id);
        }
        myCase.Status = 'Encerrado';
        myCase.Sub_Status__c = 'Cancelado pelo aluno';
        myCase.Apex_context__c = true;
        myCase.Solucao__c = 'Foi solicitado o cancelamento pelo aluno através do Portal.';
        update myCase;
        PageReference pageRef = new pageReference('/' + myCase.Id);
        return pageRef;
    }

    public static void estornaCobrancas(String caseId){
        system.debug(caseId);
        List<Cobranca__c> cobrancas = [select id, Name, Caso__c, Status__c from Cobranca__c where Caso__c=: caseId];
        system.debug(cobrancas);
        for(Cobranca__c cobranca: cobrancas){
            cobranca.Status__c = 'Cancelado';
        }
        system.debug(cobrancas);
        update cobrancas;
    }
}