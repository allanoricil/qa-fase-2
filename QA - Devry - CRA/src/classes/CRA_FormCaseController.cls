public class CRA_FormCaseController {
    
    private final Case oCase;

	public CRA_FormCaseController(ApexPages.StandardController stdController) {

		List<String> fields = new List<String>();
	    fields.add('Logo_do_Campus__c');
	   	stdController.addFields(fields);
		this.oCase = (Case)stdController.getRecord();

	}
	
	public Datetime getTargetDate() {

        try{

			MilestoneType milestoneType = [SELECT Id FROM MilestoneType WHERE Name = 'Conclusão do Caso' LIMIT 1];
	        CaseMilestone caseMilestone = [SELECT TargetDate FROM CaseMilestone WHERE CaseId = :oCase.id AND MilestoneTypeId = :milestoneType.Id LIMIT 1];
        	return caseMilestone.TargetDate;

        }catch(Exception e){
        	return null;
        }

    }


	public String getImageURL() {

        return 'Logos/'+oCase.Logo_do_Campus__c;

    }
    

}