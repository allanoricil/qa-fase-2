/*
    @author 
    @class Classe controladora da pagina de Reagendamento
*/
public with sharing class ReagedamentoController{
    public List<SelectOption>     horaOptions       { get; set; }
    public List<SelectOption>     diaOptions        { get; set; }
    public List<Reagendamento__c> numeroAgendamento { get; set; }
    
    public Agendamento__c age              { get; set; }
    
    public String  horaAgend               { get; set; }
    //public String  dataAgend               { get; set; }
    public String  nome                    { get; set; }
    public String  cpf                     { get; set; }
    public String  polo                    { get; set; }
    public String idpolo                   { get; set; }
    public String  nInscricao              { get; set; }
    public Boolean excedeReagendamento     { get; set; } 
    public Boolean execute                 { get; set; }   
    public Date    dia                     { get; set; }
    public String  agendId                 { get; set; }
    public string  callfunc                { get; set; }
    public String  generate                { get; set; }
    

    public ReagedamentoController(ApexPages.StandardController controller){
        diaOptions = new List<SelectOption>();
        getInformacoesCandidato();
        validaAgendamento();
        execute = false;
        getListDate();
        generate = 'function generate(type, text) {var n = noty({text: text,type: type,dismissQueue: true,progressBar: true,timeout: 3000,layout: \'center\',closeWith: [\'click\'],theme: \'relax\',maxVisible: 10,animation: {open: \'animated bounceInLeft\',close: \'animated bounceOutRight\',easing: \'swing\',speed: 500}}); return n;}';    
    }

    public void calljavascript_cls(){
        callfunc='<script> '+generate +' generate(\'error\', \'<div class="activity-item" style="text-align: left"><div class="activity">Não há vagas Disponíveis.</div></div>\');</script>';
    }

    public void getInformacoesCandidato(){
        String oportunidade = ApexPages.currentPage().getParameters().get('id');
        Opportunity op = [SELECT id, Account.Name, Account.CPF_2__c, Polo__c, Numeroinscrprocsel__c, Polo__r.Name 
                          FROM Opportunity WHERE id=:oportunidade];

        nome       = op.Account.Name;
        cpf        = op.Account.CPF_2__c;
        polo       = op.Polo__r.Name;
        nInscricao = op.Numeroinscrprocsel__c;
        idpolo     = op.Polo__c;
    }

    //Validação de apenas 3 reagendamentos
    public void validaAgendamento(){
        String oportunidadeid = ApexPages.currentPage().getParameters().get('id');
         numeroAgendamento = [SELECT Name, Reagendamento_del__c FROM Reagendamento__c WHERE Reagendamento_del__c=:oportunidadeid];

        if(numeroAgendamento.size()>=3){
            excedeReagendamento = true;
        }else {
            excedeReagendamento = false;
        }
    }

    //Retorna Data Agendamento
    public List<Agendamento__c> getDate(){
        return [SELECT Id, Name, Polo__c, Dia__c FROM Agendamento__c WHERE Polo__r.Name=:polo order by Dia__c asc];
    }

    //Retorna Hora
    public List<Agendamento__c> getHour(Date dataAction){
        return [SELECT Id, Name, Polo__c, Hor_rio__c FROM Agendamento__c WHERE Dia__c=:dataAction and Polo__c =:idpolo];
    }

    //Lista de Datas
    public void getListDate(){
        diaOptions = new List<SelectOption>();
        for(Agendamento__c agendamentoData : getDate()){
                Date datas  = agendamentoData.Dia__c;
                String data = datas.format();
                if(getDateByString(data) >= date.TODAY() && diaOptions.size() < 14){
                diaOptions.add(new SelectOption(data,data));
                }
        }
    }

    //Lista de Horários
    public void getListHour(){
        horaOptions = new List<SelectOption>();
        if(dia != null){
            for(Agendamento__c  agendamentoHora : getHour(dia)){
            List<String> hora = agendamentoHora.Hor_rio__c.split(';');
                for(integer i=0;hora.size()>i;i++)
            horaOptions.add(new SelectOption(hora[i],hora[i]));           
            }
        }else{
            horaOptions.add(new SelectOption('Dia indisponível', '')); 
        }

    }

    //Conversão de data dd/MM/AAAA para padrão Salesforce AAAA/MM/dd
    public Date getDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(0)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }
    
    //Verifica se há vagas para o agendaento
    public Boolean validacapacidade(Date sdia, String shora ){
         age = [Select Id, Dia__c, Capacidade__c,Hor_rio__c FROM Agendamento__c where Dia__c =: sdia and Polo__c =: idpolo limit 1];
        List<Reagendamento__c> re = [Select Id From Reagendamento__c where Agendamento__c =: age.id and Hora__c=: shora];
        if(age.Capacidade__c <= re.size()){
            calljavascript_cls();
            return false;
        }
        return true;
    }

    public void populaOportunidadeEad(){
        if(validacapacidade(dia,horaAgend)){

            String oportunidadeid = ApexPages.currentPage().getParameters().get('id');

            Oportunidade_EAD__c oppEad = [SELECT Id, Name,Opportunity__c, Opportunity__r.StageName, Agendamentos__c, StageName__c
                                               FROM Oportunidade_EAD__c WHERE Opportunity__c =:oportunidadeid and TipoProcesso__c = 'Vestibular Especial' limit 1];

            //--------------------------------------------------------
            //UPDATE ALLAN ORICIL
            Opportunity opp = [SELECT Id, 
                                      Dia_Prova__c, 
                                      Hora_Prova__c 
                                 FROM Opportunity 
                                WHERE id=: oportunidadeid];
            //-----------------------------------------------------------

            system.debug('Picole' + oppEad );
            
            Reagendamento__c agenda = new Reagendamento__c();

            agenda.Data__c              = dia;
            agenda.Hora__c              = horaAgend;
            agenda.Reagendamento_del__c = oportunidadeid; 
            agenda.Candidato__c         = oppEad.Id;
            agenda.Agendamento__c       = age.Id;

            //WMMB - 06/10/2017 - verifica se ja existe um reagendamento pra aquele dia
            List<Reagendamento__c> reagendamentoNoDia = [SELECT Id, Name, Data__c, Agendamento__c, Candidato__c FROM Reagendamento__c WHERE Candidato__c =:  oppEad.Id and Data__c =: dia and Reagendamento_del__c =: oportunidadeid limit 1];
            if(reagendamentoNoDia.size()>0){
                //se sim, atualiza
                agenda.id = reagendamentoNoDia[0].id;
                update agenda;
            }else{
                //senão, insere tendo um limite de até 3
                insert agenda;
                oppEad.Agendamentos__c ++;
            }
            
            oppEad.DiaProva__c              = dia;
            oppEad.HoraProva__c             = horaAgend;   
            oppEad.Presente__c              = false;

            //--------------------------------------------------------
            //UPDATE ALLAN ORICIL
            opp.Dia_Prova__c = dia;
            opp.Hora_Prova__c = horaAgend;
            //-------------------------------------------------------

            if(oppEad.StageName__c != 'Aprovado'){
                oppEad.StageName__c = 'Inscrito';
                oppEad.Opportunity__r.StageName = 'Inscrito';
            } 

            update oppEad;

            //--------------------------------------------------------
            //UPDATE ALLAN ORICIL
            update opp;
            //--------------------------------------------------------

            this.execute = true;
        }   
    } 
}