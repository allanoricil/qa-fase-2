public class OpportunityTriggerHandler implements TriggerHandlerInterface{
    private static final OpportunityTriggerHandler instance = new OpportunityTriggerHandler();

    /** Private constructor to prevent the creation of instances of this class.*/
    private OpportunityTriggerHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    */
    public static OpportunityTriggerHandler getInstance() {
        return instance;
    }    
    
    /**
    * Method to handle trigger before update operations. 
    */
    public void beforeUpdate() {
        System.debug('BEFOREUPDATE');
        for(Opportunity myOpp :(List<Opportunity>)Trigger.new ){
            if(myOpp.Oferta__c == null){
                if(myOpp.X1_OpcaoCurso__c != null){
                    myOpp.Oferta__c = myOpp.X1_OpcaoCurso__c;
                }
            }

        }
        SetInscricoesContas.setInscricoesContas((List<Opportunity>)Trigger.new, (List<Opportunity>)Trigger.old, false);
        UpdateOwnerGra.updateOwnerGra((List<Opportunity>)Trigger.new);
        VagasDoAgendamento.vagasDoAgendamento((List<Opportunity>)Trigger.new, (Map<Id,Opportunity>)Trigger.oldMap , false, true);
        CRA_PhoneUtil.fixOpportunityPhone((List<Opportunity>)Trigger.new);
    }
    
    /**
    * Method to handle trigger before insert operations. 
    */ 
    public void beforeInsert() {
        system.debug('BEFOREINSERT');
        for(Opportunity myOpp :(List<Opportunity>)Trigger.new ){
            system.debug('AQUI ESTÁ A INDICAÇÃO CASO A OPORTUNIDADE SEJA CLONADA '+myOpp.isClone__c);
            if(myOpp.isClone__c){
                myOpp.Unique__c = '';
                myOpp.Unique_Name_v2__c = '';
                myOpp.Status_Interesse_do_contato__c = 'Novo';
                myOpp.Motivo__c = '';
            }
            if(myOpp.Oferta__c == null){
                if(myOpp.X1_OpcaoCurso__c != null){
                    myOpp.Oferta__c = myOpp.X1_OpcaoCurso__c;
                }
            }
        }
        SetInscricoesContas.setInscricoesContas((List<Opportunity>)Trigger.new, (List<Opportunity>)Trigger.old, true);
        UpdateOwnerGra.updateOwnerGra((List<Opportunity>)Trigger.new);
        UpdateOwnerPos.updateOwnerPos((List<Opportunity>)Trigger.new);
        Map<Id,Opportunity> auxMap = new Map<Id,Opportunity>();
        VagasDoAgendamento.vagasDoAgendamento((List<Opportunity>)Trigger.new, auxMap, true, false);
        CRA_OwnerFranquia.setOwnerFranquiaOpp((List<Opportunity>)Trigger.new);
        CRA_SetRecordTypeOpp.setRT((List<Opportunity>)Trigger.new);
        CRA_AtualizaFaseOportunidade.atualizaFaseConversao((List<Opportunity>)Trigger.new);
        CRA_Search4Leads.searchLeads((List<Opportunity>)Trigger.new);
        CRA_PhoneUtil.fixOpportunityPhone((List<Opportunity>)Trigger.new);
    }
    
    /**
    * Method to handle trigger before delete operations. 
    */
    public void beforeDelete() {}
    
    /**
    * Method to handle trigger after update operations. 
    */
    public void afterUpdate()  {
        System.debug('AFTERUPDATE');
        if(!ProcessorControl.inFutureContext ) {
            OpportunityBO.getInstance().createQueueUpdateCandidate( (List<Opportunity>)trigger.new );
        }
        ChecklistBase.checklistBaseUpdate((List<Opportunity>)Trigger.new);
    }
    
    /**
    * Method to handle trigger after insert operations. 
    */
    public void afterInsert()  {
        System.debug('AFTERINSERT');
        OpportunityBO.getInstance().createQueueNewCandidate((List<Opportunity>) trigger.new );
        ChecklistBase.checklistBaseInsert((List<Opportunity>)Trigger.new);
        CRA_OpportunityEAD.setEADOpportunityInitialValues((List<Opportunity>)Trigger.new);
    }
    /**
    * Method to handle trigger after delete operations. 
    */
    public void afterDelete() {}
}