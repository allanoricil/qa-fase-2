public without sharing class CRA_SuccessCasePortalController {

    public final Id myCaseId{get;set;}
    public Case myCase{get;set;}
    public Boolean error{get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CRA_SuccessCasePortalController() {
        this.error = false;
        this.myCaseId = ApexPages.currentPage().getParameters().get('CaseId');
        if (this.myCaseId == null)
            this.error = true;
        this.myCase = [SELECT Id, Assunto__r.Pagamento__c, Valor__c, Previsao_de_Conclusao__c, CaseNumber FROM Case WHERE Id =: this.myCaseId];
    }

    public PageReference goToPortal(){
        PageReference portalFinanceiro = new PageReference('http://financeiro.devrybrasil.com.br');
        portalFinanceiro.setRedirect(true);
        return portalFinanceiro;
    }

    public PageReference goToDetail(){
        PageReference portalCommunity = new PageReference('/' + myCaseId);
        portalCommunity.setRedirect(true);
        return portalCommunity;
    }
}