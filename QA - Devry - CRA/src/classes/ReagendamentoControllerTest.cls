@isTest
private class ReagendamentoControllerTest {
	
	@testSetup
	static void setup(){

		ControleDeAgendamentos__c maxNumeroDeAgendamentos = new ControleDeAgendamentos__c();
		maxNumeroDeAgendamentos.Name = 'MAXNUMERODEAGENDAMTOS';
		maxNumeroDeAgendamentos.Maximo__c = 20;
		insert maxNumeroDeAgendamentos;


		Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
	    insert instituicao;
	    
	    Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
	    insert campus;

	    Per_odo_Letivo__c periodo = CRA_DataFactoryTest.newPeriodo();
	    insert periodo;
	    
	    Processo_Seletivo__c processoSeletivo = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
	    insert processoSeletivo;
	    
	    Curso__c curso = CRA_DataFactoryTest.newCurso(instituicao, campus);
	    insert curso;
	    
	    Area_Curso__c areaCurso = CRA_DataFactoryTest.newAreaCurso();
	    insert areaCurso;
	    
	    Oferta__c oferta = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo);
	   	insert oferta;
	    
	    Account conta = CRA_DataFactoryTest.newAccountAluno();
	    insert conta;

	    Polo__c polo = CRA_DataFactoryTest.newPolo(instituicao, 'Alphaville');
	    insert polo;

	    Opportunity opp = CRA_DataFactoryTest.newOpportunityVestibularAgendado(instituicao,
	                                                   campus,
	                                                   periodo,
	                                                   processoSeletivo,
	                                                   curso,
	                                                   areaCurso,
	                                                   oferta,
	                                                   conta);
	    opp.Polo__c = polo.Id;
	    insert opp;

	    

	    Agendamento__c agendamentoEAD1 = CRA_DataFactoryTest.newAgendamentoEAD(instituicao, campus, polo, 1);
	    Agendamento__c agendamentoEAD2 = CRA_DataFactoryTest.newAgendamentoEAD(instituicao, campus, polo, 2);
	    Agendamento__c agendamentoEAD3 = CRA_DataFactoryTest.newAgendamentoEAD(instituicao, campus, polo, 3);
	    Agendamento__c agendamentoEAD4 = CRA_DataFactoryTest.newAgendamentoEAD(instituicao, campus, polo, 4);
	    insert agendamentoEAD1;
	    insert agendamentoEAD2;
	    insert agendamentoEAD3;
	    insert agendamentoEAD4;
	}

	@isTest
	static void testaAgendamento(){
		Test.startTest();
		
		Date dia = Date.today().addDays(1);
		String horario = '08:00 - 10:00';
		reagendar(dia, horario);

		Test.stopTest();
	}

	@isTest
	static void testaReagendamento(){
		Test.startTest();
		
		//Agenda a primeira vez
		Date dia = Date.today().addDays(1);
		String horario = '08:00 - 10:00';
		reagendar(dia, horario);

		//Agenda a segunda vez
		dia = Date.today().addDays(2);
		horario = '10:00 - 12:00';
		reagendar(dia, horario);

		//testa o contador de reagendamentos
		Opportunity opp = [SELECT Numero_de_Agendamentos__c 
							 FROM Opportunity 
							LIMIT 1];

		System.assertEquals(2, opp.Numero_de_Agendamentos__c);

		Test.stopTest();
	}


	//ISSO MOSTRA QUE O CONTADOR DE REAGENDAMENTOS ESTÁ PROGRENDINDO
	//E NÃO PARA EM 3, COMO NO PROJETO ANTERIOR.
	//CONSIDERE QUE O NUMERO MAXIMO DE REAGENDAMENTOS É 20
	@isTest
	static void testaQuartoReagendamento(){
		Test.startTest();
		
		//Agenda a primeira vez
		Date dia = Date.today().addDays(1);
		String horario = '08:00 - 10:00';
		reagendar(dia, horario);

		//Agenda a segunda vez
		dia = Date.today().addDays(2);
		horario = '10:00 - 12:00';
		reagendar(dia, horario);

		//Agenda a terceira vez
		dia = Date.today().addDays(3);
		horario = '13:00 - 15:00';
		reagendar(dia, horario);

		//testa o contador de reagendamentos foi para 3
		Opportunity opp = [SELECT Numero_de_Agendamentos__c 
							 FROM Opportunity 
							LIMIT 1];

		System.assertEquals(3, opp.Numero_de_Agendamentos__c);

		//Agenda a Quarta vez e verifica que não mudou nada
		dia = Date.today().addDays(4);
		horario = '08:00 - 10:00';
		reagendar(dia, horario);

		//verifica que o contador não foi para 4
		opp = [SELECT Numero_de_Agendamentos__c 
				 FROM Opportunity 
				LIMIT 1];

		System.assertEquals(4, opp.Numero_de_Agendamentos__c);

		Test.stopTest();
	}

	@isTest
	static void testaDeletarReagendamento(){
		Test.startTest();

		//Agenda a primeira vez
		Date dia = Date.today().addDays(1);
		String horario = '08:00 - 10:00';
		reagendar(dia, horario);


		Opportunity opp = [SELECT Id
							 FROM Opportunity 
							LIMIT 1];

		Reagendamento__c reagendamento = [SELECT Reagendamento_del__c
										 	FROM Reagendamento__c
										   WHERE Reagendamento_del__c=:opp.Id
										   LIMIT 1];

		delete reagendamento;

	    opp = [SELECT Numero_de_Agendamentos__c
				 FROM Opportunity 
      		    LIMIT 1];

        System.assertEquals(0, opp.Numero_de_Agendamentos__c);

		Test.stopTest();
	}


	private static void reagendar(Date dia, String horario){
		Opportunity opp = [SELECT Id,
								  Numero_de_Agendamentos__c 
							 FROM Opportunity
							LIMIT 1];

		ReagendamentoController controller = instantiatePageController(opp);

		controller.diaAgendado = dia;
		controller.horarioAgendado = horario;

		controller.confirmaAgendamento();

		opp = [SELECT Id,
					  Dia_Prova__c,
					  Hora_Prova__c 
				FROM  Opportunity
				WHERE Id=:opp.Id];

		System.assertEquals(dia, opp.Dia_Prova__c);
		System.assertEquals(horario, opp.Hora_Prova__c);
		
	}

	private static ReagendamentoController instantiatePageController(Opportunity opp){
	    PageReference pageRef = Page.Reagendamento;
	    pageRef.getParameters().put('id', opp.Id);
	    Test.setCurrentPage(pageRef);
	    ApexPages.StandardController standardController = new ApexPages.StandardController(opp);
	    ReagendamentoController controller = new ReagendamentoController(standardController);
	    return controller;
	  }
}