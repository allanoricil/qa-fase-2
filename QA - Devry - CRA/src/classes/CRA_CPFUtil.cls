public class CRA_CPFUtil {
	public static Boolean isCPF(String CPF) {
		
		string regExp = '[^0-9]';
		CPF = CPF.replaceAll(regExp,'');

		if (CPF.equals('00000000000') || CPF.equals('11111111111') ||
         	CPF.equals('22222222222') || CPF.equals('33333333333') ||
        	CPF.equals('44444444444') || CPF.equals('55555555555') ||
        	CPF.equals('66666666666') || CPF.equals('77777777777') ||
        	CPF.equals('88888888888') || CPF.equals('99999999999') ||
       		(CPF.length() != 11))
       		return(false);

    	Integer dig10, dig11, sm, i, r, num, peso;
       
      	sm = 0;
      	peso = 10;
      	List<String> cpfString = cpf.split('');
        system.debug('cpfString: ' + cpfString);
      	for (i=0; i<9; i++) {
        	num = Integer.valueOf(cpfString[i]); 
        	sm = sm + (num * peso);
        	peso = peso - 1;
      	}

      	r = 11 - (math.mod(sm,11));
      	if ((r == 10) || (r == 11))
        	dig10 = 0;
      	else 
      		dig10 = r;

		// Calculo do 2o. Digito Verificador
      	sm = 0;
      	peso = 11;
      	for(i=0; i<10; i++) {
        	num = Integer.valueOf(cpfString[i]);
        	sm = sm + (num * peso);
        	peso = peso - 1;
      	}

      	r = 11 - (math.mod(sm,11));
      	if ((r == 10) || (r == 11))
         	dig11 = 0;
      	else 
      		dig11 = r;

		// Verifica se os digitos calculados conferem com os digitos informados.
      	if (dig10 == Integer.valueOf(cpfString[9]) && dig11 == Integer.ValueOf(cpfString[10]))
        	return true;
      	else 
      		return false;
  	}
}