public with sharing class CRA_SubjectsQuery {
	public CRA_SubjectsQuery() {
		
	}
	
    Public Static Map<String, Object> executeDiscEmCurso(String Usuario, String Senha, String Tipo) {
       	Map<String, Object> getData = new Map<String, Object>();
       	Map<String, String> subjects = new Map<String, String>();

       	//Método para obter o token:
    	Map<String, String> mapToken = login(Usuario, Senha);
    	system.debug('mapToken ' + mapToken);
    	if( mapToken.get('200') != null ) {
    		 try {
    			getData = getDiscEmCurso( mapToken.get('200'), Tipo);
    			//Teve pelo menos um documento retornado:
    			if( getData.size() > 0 ) 
    				subjects = processSubjects(getData);
    			System.debug( '>>> getData' + getData );
    		} catch ( CalloutException ex ) {}
    			catch ( Exception ex ) {} 
    	}
    	return getData;
   	}   	

	public Static Map<String, String> login(String Usuario, String Senha){
		Map<String, String> mapToReturn = new Map<String, String>();
		//if(test.IsRunningTest()){
		//	mapToReturn.put('200', 'sucesso');
		//}else{
			try {
				HttpRequest req = new HttpRequest();			
				req.setEndpoint( WSSetup__c.getValues('Login').Endpoint__c );
				req.setMethod( 'POST' );
				req.setHeader( 'content-type', 'application/json' );
				req.setTimeout( 120000 );
				req.setBody(getLoginJsonRequest(Usuario, Senha));
				system.debug('req body ' + req.getBody());
				system.debug('req body document ' + req.getBodyDocument());
				Http h = new Http();
				HttpResponse res = h.send( req );

				system.debug('res ' + res.getBody());
				system.debug('headers ' + res.getHeaderKeys());
				system.debug('res status ' + res.getStatusCode() );
                system.debug('res body ' + JSON.deserializeUntyped( res.getBody()));


	 
				Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
			
				if( res.getStatusCode() == 200 ) 
					mapToReturn.put( '200', (String) postData.get( 'auth_token' ) );
				else
					mapToReturn.put( '401', (String) postData.get( 'mensagem' ) );
			} catch ( CalloutException ex ) {
				mapToReturn.put( '401', ex.getMessage() );
			}
		//}
		return mapToReturn;
	}

	private Static String getLoginJsonRequest(String Usuario, String Senha) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'usuario', Usuario); 
        gen.writeStringField( 'senha', Senha);
        gen.writeEndObject();
    	return gen.getAsString(); 
    }

    
    private Static String getDiscEmCursoJsonRequest(String auth_token){
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'API-TOKEN', auth_token); 
    	gen.writeEndObject();
    	return gen.getAsString();
    }
    
	
    private Static Map<String, Object> getDiscEmCurso(String api_token, String Tipo){
    	Map<String, Object> postData = new Map<String, Object>();

    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues( 'DisciplinaEmCurso' ).Endpoint__c );
        req.setMethod( 'GET' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader('API-TOKEN', api_token);
        req.setHeader('TIPO', Tipo);
        req.setTimeout( 120000 );
        //req.setBody( DiscEmCursoJSONRequest );
        //system.debug('req disciplinas' + req.getEndpoint() + req.getBody() );
        Http h = new Http();
        HttpResponse res = h.send( req );

        if( res.getStatusCode() == 200 )
    		postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );			        							               					          

        System.debug( '>>> getStatusDocumento ' + res.getBody() );
        return postData;
    }
	

    private Static Map<String, String> processSubjects(Map<String, Object> getData){
    	Map<String, String> DisciplinaMap = new Map<String, String>();
    	List<Object> DisciplinaList = new List<Object>();
        if(getData.get( 'emcursoResult' ) != null)
        	DisciplinaList = ( List<Object> ) getData.get( 'emcursoResult' );
        if( !DisciplinaList.isEmpty() ) {
        	for( Object listResult : DisciplinaList ) {
    			Map<String, Object> documentField = ( Map<String, Object> ) listResult;
	      		String codigoDisc = String.valueOf( documentField.get( 'codigo' ) );
	      		String nomeDisc = String.valueOf( documentField.get( 'disciplina' ) );
	      		DisciplinaMap.put(codigoDisc, nomeDisc);     		
          	}
        }
        return DisciplinaMap;
    }


    Public Static Map<String, Object> executeDiscHistorico(String Usuario, String Senha, String Tipo) {
       	Map<String, Object> getData = new Map<String, Object>();
       	Map<String, String> subjects = new Map<String, String>();

       	//Método para obter o token:
    	Map<String, String> mapToken = login(Usuario, Senha);
    	system.debug('mapToken ' + mapToken);
    	if( mapToken.get('200') != null ) {
    		 try {
    			getData = getDiscHistorico( mapToken.get('200'), Tipo );
    			//Teve pelo menos um documento retornado:
    			if( getData.size() > 0 ) 
    				subjects = processSubjects(getData);
    			System.debug( '>>> getData' + getData );
    		} catch ( CalloutException ex ) {}
    			catch ( Exception ex ) {} 
    	}
    	return getData;
   	}

   	private Static Map<String, Object> getDiscHistorico(String api_token, String Tipo){
    	Map<String, Object> postData = new Map<String, Object>();

    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues('HistoricoDisciplinas').Endpoint__c );
        req.setMethod( 'GET' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader('API-TOKEN', api_token);
        req.setHeader('TIPO', Tipo);
        req.setTimeout( 120000 );
        //system.debug('req historico: ' + req.getBody());
        Http h = new Http();
        HttpResponse res = h.send( req );
        system.debug('resposta: '+res);
        system.debug('body: '+JSON.deserializeUntyped( res.getBody() ));

        if( res.getStatusCode() == 200 )
    		postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );			        							               					          

        System.debug( '>>> getStatusDocumento ' + res.getBody() );
        return postData;
    }

}