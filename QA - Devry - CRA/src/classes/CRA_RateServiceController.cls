public without sharing class CRA_RateServiceController {

	public Case myCase{set;get;}
    public String caseRating{set;get;}
    public String isAtendida{set;get;}
    public Boolean error{set;get;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CRA_RateServiceController(ApexPages.StandardController stdController) {
        
        if(!Test.isRunningTest()){
            List<String> myFields = new List<String>();
            myFields.add('Id');
            myFields.add('Avaliacao_do_atendimento__c');
            myFields.add('Status');
            stdController.addFields(myFields);
        }
        
        this.myCase = (Case)stdController.getRecord();
        if(myCase.Avaliacao_do_atendimento__c != null){
            this.error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Olá, tudo bem? Este atendimento já foi avaliado!'));
        }
        if(myCase.Status != 'Concluído' && myCase.Status != 'Encerrado'){
            this.error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Olá, tudo bem? Aguarde a conclusão da solicitação para avaliar o atendimento!'));
        }
    }

    
    public PageReference returnToCase(){
        PageReference pageRef = new pageReference('/' + myCase.Id);
        return pageRef;
    }

    public PageReference rate(){
        if(!String.isBlank(caseRating)){
            myCase.Avaliacao_do_atendimento__c = Integer.valueOf(caseRating);
        }
        if(!String.isBlank(isAtendida)){
            myCase.Atendida__c = Boolean.valueOf(isAtendida);
        }
        update myCase;
        PageReference pageRef = new pageReference('/' + myCase.Id);
        return pageRef;
    }

    public Class CaseRating{
        public Boolean Selected{get;set;}
        public Integer Order{get;set;}
        public CaseRating(Integer ord){
            this.Order = ord;
        }
    }

    public Class SolAtendida{
        public String Name {get;set;}
        public SolAtendida(String nome){
            this.Name = nome;
        }
    }

}