public with sharing class CRA_ProvaSubstitutivaAtaController {

	public String campus {get;set;}
	public String assunto {get;set;}
	public String turma {get;set;}
	public String periodo {get;set;}
	public String professor {get;set;}
	public String disciplina {get;set;}
	public String nomeIES {get;set;}
	public String imageURL {get;set;}
	public String endereco {get;set;}
	public String cnpj {get;set;}
	public List<Item_do_caso__c> itensCaso {get;set;}
	
	public CRA_ProvaSubstitutivaAtaController() {

		this.campus  = ApexPages.currentPage().getParameters().get('campus');
		this.assunto = ApexPages.currentPage().getParameters().get('assunto');
		this.turma   = ApexPages.currentPage().getParameters().get('turma');
		this.periodo = ApexPages.currentPage().getParameters().get('periodo');

		this.itensCaso = [SELECT Id,
						Name,
						Caso__r.Aluno__r.Campus_n__r.Name,
						Caso__r.Aluno__r.Institui_o_n__r.Name,
						Caso__r.Logo_do_Campus__c,
						Caso__r.Assunto__r.Name,
						Professor__c,
						Disciplina__c,
						Periodo__c,
						Turma__c,
						Caso__r.Aluno__r.R_A_do_Aluno__c,
						Caso__r.Nome_exibicao_do_aluno__c,
						Caso__r.Aluno__r.Campus_n__r.Endere_o__c,
						Caso__r.Aluno__r.Institui_o_n__r.CNPJ__c,
						Codigo_da_disciplina__c
						FROM Item_do_Caso__c
						WHERE 
						Caso__r.Aluno__r.Campus_n__r.Id = :this.campus 
						AND Caso__r.Assunto__r.Name like :'%'+this.assunto+'%'
						AND (Caso__r.Status = 'Encerrado' OR Caso__r.Status = 'Concluído')
						AND Caso__r.Status_de_Pagamento__c = 'Pago'
						AND Turma__c =: this.turma
						AND Periodo__c =: this.periodo
						];

		if(this.itensCaso.isEmpty())
        	return;
			
		this.imageURL   = 'Logos/'+ this.itensCaso.Get(0).Caso__r.Logo_do_Campus__c;
		this.professor  = this.itensCaso.Get(0).Professor__c;
		this.disciplina = this.itensCaso.Get(0).Disciplina__c;
		this.nomeIES	= this.itensCaso.Get(0).Caso__r.Aluno__r.Institui_o_n__r.Name+' - '+this.itensCaso.Get(0).Caso__r.Aluno__r.Campus_n__r.Name;
		this.endereco	= this.itensCaso.Get(0).Caso__r.Aluno__r.Campus_n__r.Endere_o__c;
		this.cnpj 		= this.itensCaso.Get(0).Caso__r.Aluno__r.Institui_o_n__r.CNPJ__c;
		
	}



}