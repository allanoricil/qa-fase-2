public without sharing class CRA_confirmCaseController {
    public Boolean error{get; set;}
	private Case myCase;

    public CRA_confirmCaseController(ApexPages.StandardController stdController) {
       
        List<String> fields = new List<String>();
        fields.add('Id');
        fields.add('Sub_Status__c');
        fields.add('Status');
        
        if(!Test.isRunningTest())
            stdController.addFields(fields);
        
        this.myCase = (Case)stdController.getRecord();
        this.myCase = [SELECT Id,
                              Sub_Status__c,
                              Status
                         FROM Case 
                        WHERE Id=:myCase.Id];
        this.error = false;
    }

    public PageReference confirm(){
        if(myCase.Sub_Status__c != 'Aguardando confirmação'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Olá, tudo bem? Essa ação não é necessária para este tipo de solicitação.'));
            error = true;
            return null;
        }else{
            if(myCase.Status == 'Em andamento'){
                myCase.Sub_Status__c = 'Pendência regularizada';
            }else{
            	myCase.Status = 'Em andamento';
            	myCase.Sub_Status__c = null; 
            }            
            try{
                update myCase;
            }catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Ocorreu um erro inesperado: '+ e.getMessage()));
                error = true;
                return null;
            }
            return GoBack();
        }
    }

    public PageReference goBack(){
        PageReference pageRef = new PageReference('/' + myCase.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }

    
}