@isTest
private class AccountDAOTest {

    static testMethod void myUnitTest() {
    	Account acc = InstanceToClassCoverage.createAccount();
    	
    	insert acc;
    	
    	String accId = acc.Id;
    	
    	AccountDAO.getInstance().getAccountById(accId);
    	AccountDAO.getInstance().getAccountEnterprise();
    	AccountDAO.getInstance().getAllColleges();
    	AccountDAO.getInstance().getAllAccountsWithLeadOrigin();
    	AccountDAO.getInstance().getAccountByCpf('41214288898');
    }
}