global class EADOpportunityScheduledTask implements Schedulable {

	global EADOpportunityScheduledTask(){
	}

	global static void execute(SchedulableContext sc) {
		updateStage();
	}

	public Static void updateStage(){
		List<Opportunity> oppsToUpdate = new List<Opportunity>();
		List<Opportunity> ausentes = [SELECT Id,
											 StageName, 
											 Prova_entrevista_matricula_vencida__c,
											 EAD__c
										FROM Opportunity 
									   WHERE Prova_entrevista_matricula_vencida__c = true
									   		 AND EAD__c = 'Sim'
									   		 AND RecordType.Name != 'ENEM'];

		for(Opportunity opp : ausentes){
			opp.StageName = 'Ausente';
			oppsToUpdate.add(opp);
		}
		update oppsToUpdate;
	}
}