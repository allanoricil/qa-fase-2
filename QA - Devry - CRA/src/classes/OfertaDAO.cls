/*
    @author Diego Moreira
    @class Classe DAO do objeto Oferta
*/
public with sharing class OfertaDAO extends SObjectDAO {
    
    /*
        Singleton
    */
    private static final OfertaDAO instance = new OfertaDAO();    
    private OfertaDAO(){}
    
    public static OfertaDAO getInstance() {
        return instance;
    }

    /*
        Retorna as ofertas relacionadas ao processo seletivo
        @param processoSeletivo valor do processo a ser filtrado
    */
    public List<Oferta__c> getOfertasByProcessoSeletivo( String processoSeletivo ) {
        return [SELECT Id, Name 
                    FROM Oferta__c
                    WHERE Processo_seletivo__c = :processoSeletivo AND Inativa__c = false
                    Order By Name];
    }
}