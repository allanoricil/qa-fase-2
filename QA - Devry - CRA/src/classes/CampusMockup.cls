public class CampusMockup {
    public static Campus__C createAndGetCampusMockup(){
        
        Institui_o__c ins = Institui_oMockup.createAndGetInstituicao();
        
        Campus__c campus = new Campus__c(Name = 'Campus Teste',
                                         Institui_o__c=ins.id,
                                         Id_Campus__c = '9999'
                                        );
        
        upsert campus Id_Campus__c;
        
        return campus;
    }

}