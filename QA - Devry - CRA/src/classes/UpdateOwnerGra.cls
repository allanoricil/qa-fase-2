public without sharing class UpdateOwnerGra {
	public static void updateOwnerGra(List<Opportunity> allOpps) {
		if (allOpps.size() <= 23){   
			for(Opportunity opp : allOpps){
				if(!String.isEmpty(opp.Tipo_de_Matr_cula__c)){
					if(opp.Tipo_de_Matr_cula__c.equals('Vestibular Tradicional')){
						List<Campus__c> Campus;
						List<Processo_seletivo__c> ProcSel;
						String CampusId2;
						String OwnerId2;

						ProcSel   = [select Campus__c from Processo_seletivo__c pro where pro.id = :opp.Processo_seletivo__c];

						for(Processo_seletivo__c p: ProcSel){
							String idSlfProcSel = p.id;
							CampusId2 = p.Campus__c;
						}

						Campus = [select Propriet_rio_Gradua_o_Inscri_es__c from Campus__c cam where cam.id = :CampusId2];
						for(Campus__c c: Campus){
							String idSlfCampus = c.id;
							OwnerId2 = c.Propriet_rio_Gradua_o_Inscri_es__c;
							if(!String.isEmpty(OwnerId2)){
								opp.OwnerId = OwnerId2;
							}
							else{
								opp.OwnerId = '005A0000000sMPi';
							}
						}
					}
				}
			}
		}
	}
}