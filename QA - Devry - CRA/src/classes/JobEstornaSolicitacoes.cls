global class JobEstornaSolicitacoes implements Schedulable {
	global void execute(SchedulableContext sc) {
		List<Cobranca__c> cobrancas = [select id, Name, Caso__c, Status__c from Cobranca__c where Data_Limite_Pagamento__c < TODAY  and Status__c ='Em Aberto' limit 2];
		Set<Id> casesIds = new Set<Id>();
		for(Cobranca__c cobranca: cobrancas){
			cobranca.Status__c = 'Cancelado';
			casesIds.add(cobranca.Caso__c);
		}

		update cobrancas;
		
		List<Case> casesToUpdate = [select id,Status_de_Pagamento__c, Status, CaseNumber, Sub_Status__c, Apex_context__c,Solucao__c from Case where id in:casesIds];
		for(Case caso: casesToUpdate){
			caso.Status = 'Encerrado';
        caso.Sub_Status__c = 'Encerrado automaticamente';
        caso.Apex_context__c = true;
        caso.Solucao__c = 'Encerrado automaticamente por decurso de prazo do pagamento.';
		caso.Status_de_Pagamento__c = 'Cancelado';
		}

		update casesToUpdate;
	}
}




//String sch0 = '0 0 * * * ?';
//JobEstornaSolicitacoes sqrb0 = new JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 00', sch0, sqrb0);

//String sch5 = '0 5 * * * ?';
//    JobEstornaSolicitacoes sqrb5 = new     JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 05 min', sch5, sqrb5);

//String sch10 = '0 10 * * * ?';
//    JobEstornaSolicitacoes sqrb10 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 10 min', sch10, sqrb10);

//String sch15 = '0 15 * * * ?';
//    JobEstornaSolicitacoes sqrb15 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 15 min', sch15, sqrb15);

//String sch20 = '0 20 * * * ?';
//    JobEstornaSolicitacoes sqrb20 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 20 min', sch20, sqrb20);

//String sch25 = '0 25 * * * ?';
//    JobEstornaSolicitacoes sqrb25 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 25 min', sch25, sqrb25);

//String sch30 = '0 30 * * * ?';
//    JobEstornaSolicitacoes sqrb30 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 30 min', sch30, sqrb30);

//String sch34 = '0 35 * * * ?';
//    JobEstornaSolicitacoes sqrb34 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 34 min', sch34, sqrb34);

//String sch40 = '0 40 * * * ?';
//    JobEstornaSolicitacoes sqrb40 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 40 min', sch40, sqrb40);

//String sch45 = '0 45 * * * ?';
//    JobEstornaSolicitacoes sqrb45 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 45 min', sch45, sqrb45);

//String sch50 = '0 50 * * * ?';
//    JobEstornaSolicitacoes sqrb50 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 50 min', sch50, sqrb50);

//String sch55 = '0 55 * * * ?';
//    JobEstornaSolicitacoes sqrb55 = new    JobEstornaSolicitacoes();
//system.schedule('Estorna Cobranças 55 min', sch55, sqrb55);