/*************************************************************************
Author   : Appirio Offshore(Sunil Gupta)
Date     : Dec 24, 2012
Usage    : Utility methods to be used in test coverage methods. This class can only be used and 
           instantiated by unit test executions and will throw an exception if used in a non test context.
**************************************************************************/
public class TestUtils
{
    //Our constructor. We throw a custom exception if this class is being instantiated from a non-test context.
    private TestUtils()
    {
        if(!Test.isRunningTest())
            throw new TestUtilsClassUsedInNonTestContextException ('This class cannot be instantiated or used from a non-Test context!');
    }
    
    //This is the custom Exception that we throw if this class is being instantiated from a non-test context.
    class TestUtilsClassUsedInNonTestContextException extends Exception {}
    
    //This method returns a test Person Account with the required fields needed to perform a proper insert.
    public static Account CreateTestPersonAccount()
    {
        return new Account(LastName = 'TEST', RecordTypeId = personAccountRecordTypeId);        
    }
    
    public static User GetUser(){
        Profile portalProfile = [select id from profile where name = 'Graduate Interview Profile'];
        
        User portalUser = new User(alias = 'standt', email='testdevrybrasil2@testorg2.com', 
                        emailencodingkey='UTF-8', lastname='Testing', 
                        languagelocalekey='en_US', 
                        localesidkey='en_US', profileid = portalProfile.Id,
                        timezonesidkey='America/Los_Angeles', 
                        username='testdevrybrasil2@testorg2.com', DBNumber__c = 'DB9999000');
        
        insert portalUser;
        return portalUser;
   }
    
    
    
    //This method returns a test Contact with the required fields needed to perform a proper insert.
    public static Contact createTestContact()
    {
        return new Contact(LastName = 'TEST');
    }

    //This method returns a test Lead with the required fields needed to perform a proper insert.
    public static Lead createTestLead()
    {
        return new Lead(LastName = 'TEST');
    }
    
    //This method returns a test Opportunity with the required fields needed to perform a proper insert.
    public static Opportunity CreateTestOpportunity()
    { 
        return new Opportunity(Name = 'TEST', StageName = 'TEST', CloseDate = Date.TODAY() + 30);
    }
    
   
    
    //This method returns the RecordType Id of Person Account
    public static Id personAccountRecordTypeId
    {
        get
        {
            if(personAccountRecordTypeId == null)
                personAccountRecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'conta pessoal' AND SObjectType = 'Account'].Id;
            
            return personAccountRecordTypeId;
        }
        private set;
    }
    
    static testmethod void mytest(){
      contact c=testutils.createTestContact();
      lead l=testutils.createtestlead();
      opportunity o=testutils.CreateTestOpportunity();
      account a=testutils.CreateTestPersonAccount();
    }
      
    
    
}