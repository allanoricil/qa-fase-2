/*
    @author Diego Moreira
    @class Classe controladora da pagina de pagamento da inscrição
*/
public with sharing class SRM_PagamentoInscricaoController {
    public Opportunity opportunity {
        get;
        private set;
    }
    private Institui_o__c instituicao;
    boolean juros;
    //public String processoSeletivo {get; private set;}
    //public String processoSeletivoCampus {get; private set;}    
    //public String processoSeletivoInstituicao {get; private set;}    

    //ALTER: MERCHANTID
    /*
    public List<SelectOption> optMeiosPagamento { get; private set; }
    public String opPagto { get; set; }     
    private Map<String, MeioPagamentoIES__c> mapDescMeioPagamento = new Map<String, MeioPagamentoIES__c>();
    */
    public List < SelectOption > enabledInstallments {
        get;
        private set;
    }
    public List < SelectOption > enabledPaymentTypes {
        get;
        private set;
    }
    public String codigoPromocional {
        get;
        set;
    }
    public String installments {
        get;
        set;
    }
    private Map < String, Oferta__c > mapInstallmentsOferta = new Map < String, Oferta__c > ();

    public String payment {
        get;
        set;
    }
    private Map < String, Campus_Payment_Type__c > mapPaymentType = new Map < String, Campus_Payment_Type__c > ();


    /*
        Construtor
    */
    public SRM_PagamentoInscricaoController() {
        juros = false;
        dadosProcessoSeletivo();
        dadosMeiosPagamento();
    }

    /*
        Metodo principal de execução e validação
    */
    public PageReference execute() {
        if (opportunity.Amount == 0) return openConfirmacaoInscricao();
        return null;
    }


    /*
        Monta os dados da inscrição e pagamento
    */
    private void dadosProcessoSeletivo() {
        //opportunity = getOpportunityById( ApexPages.currentPage().getParameters().get('id') )[0];
        opportunity = getOpportunityById(ApexPages.currentPage().getParameters().get('id'))[0];
        instituicao = getInstituicaoById(opportunity.Processo_seletivo__r.Institui_o_n__c)[0];

        // Define atributos para condição do código GTM
        //processoSeletivo = opportunity.Processo_Seletivo_Nome__c;
        //processoSeletivoInstituicao = instituicao.Name;
        //processoSeletivoCampus = opportunity.NomeCampus__c;
    }

    /*
        Monta os meios de pagamento
    */
    private void dadosMeiosPagamento() {
        //ALTER: MERCHANTID
        /*optMeiosPagamento = new List<SelectOption>();

        for( MeioPagamentoIES__c meioPagamento : getMeioPagamentoByInstituicaoId( opportunity.Processo_seletivo__r.Institui_o_n__c ) ) {
                optMeiosPagamento.add( new SelectOption( meioPagamento.MeioPagamentoBraspag__r.Codigo__c, meioPagamento.MeioPagamentoBraspag__r.Name ) );
                mapDescMeioPagamento.put( meioPagamento.MeioPagamentoBraspag__r.Codigo__c, meioPagamento );
        }
        System.debug('>>> ' + mapDescMeioPagamento);        
        */
        enabledInstallments = new List < SelectOption > ();
        enabledPaymentTypes = new List < SelectOption > ();
        //Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get( 'Extensão').getRecordTypeId();
        if (opportunity.Processo_seletivo__r.RecordTypeId == Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Extensão').getRecordTypeId() &&
            opportunity.Processo_seletivo__r.Tipo_de_Matr_cula__c == 'Outra Origem' && opportunity.Processo_seletivo__r.Matr_cula_Online__c == 'Sim') {
            for (Oferta__c installment: getInstallmentsByOfertaId(opportunity.Oferta__r.Id)) {
                List < String > parts = installment.Parcelas__c.split(';');
                for (integer i = 0; parts.size() > i; i++) {
                    enabledInstallments.add(new SelectOption(parts[i], parts[i]));
                }
                mapInstallmentsOferta.put(installment.Parcelas__c, installment);
            }

            for (Campus_Payment_Type__c paymentType: getPaymentTypesCardsByCampusId(opportunity.Processo_seletivo__r.Campus__c)) {
                enabledPaymentTypes.add(new SelectOption(paymentType.Payment_Type_Braspag__r.Codigo__c, paymentType.Payment_Type_Braspag__r.Name));
                mapPaymentType.put(paymentType.Payment_Type_Braspag__r.Codigo__c, paymentType);
            }
        } else {
            for (Campus_Payment_Type__c paymentType: getPaymentTypesByCampusId(opportunity.Processo_seletivo__r.Campus__c)) {
                enabledPaymentTypes.add(new SelectOption(paymentType.Payment_Type_Braspag__r.Codigo__c, paymentType.Payment_Type_Braspag__r.Name));
                mapPaymentType.put(paymentType.Payment_Type_Braspag__r.Codigo__c, paymentType);
            }
        }
        System.debug('Map >>> ' + mapPaymentType);
    }

    /*
        Cria o titulo de pagamento e envia para o Braspag
        @action Continuar
    */
    public PageReference formPagamento() {
        if (formularioValidado()) {
            String nossoNumero = '';
            //Boolean isBoleto = mapDescMeioPagamento.get( opPagto ).MeioPagamentoBraspag__r.TipoMeioPagamento__c.equals('Boleto') ? true : false;            

            //ALTER: MERCHANTID
            /*
            Titulos__c titulo = TituloBO.getInstance().criaTituloInscricao( opportunity.Id, opportunity.Processo_seletivo__c, opportunity.AccountId, mapDescMeioPagamento.get( opPagto ) );
            */

            Titulos__c title = insertTitle(opportunity.Id, opportunity.Processo_seletivo__c, opportunity.AccountId, mapPaymentType.get(payment));

            //if( isBoleto )
            //nossoNumero = geraNossoNumero( titulo.Id );

            //ALTER: MERCHANTID
            /*
            return formParameters( titulo.Id, nossoNumero );
            */

            return formParameters(title.Id, nossoNumero);
        }
        return null;
    }
    public Titulos__c insertTitle(String opportunityId, String processoId, String accountId, Campus_Payment_Type__c paymentType) {

        Processo_seletivo__c selectiveProcess = Processo_SeletivoDAO.getInstance().getProcessoSeletivoById(processoId)[0];
        Opportunity opportunity = getOpportunityById(opportunityId)[0];
        Date titleDate = Date.today();
        System.debug('Insert >>> ' + paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c + ' / ' + selectiveProcess.Vencimento_Boleto_dias__c);
        List < Opportunity > opps = [SELECT ID, Cpf__c, Account.Cpf_2__c from Opportunity WHERE Id =: opportunityId];
        String cpfTitulo = opps[0].Account.Cpf_2__c;
        String cpf = cpfTitulo.replaceAll('[|.|-]', '');

        Titulos__c title = new Titulos__c();
        title.Candidato_Aluno__c = accountId;
        title.Inscricao__c = opportunityId;
        title.Cpf__c = cpf;
        title.Documento__c = String.isBlank(installments) ? 'VEST' : 'ONLINE';
        title.Instituicao__c = selectiveProcess.Institui_o_n__c;
        title.ProcessoSeletivo__c = selectiveProcess.Id;
        title.Parcelado__c = String.isBlank(installments) ? 1 : Decimal.valueOf(installments);
        title.Valor__c = opportunity.Amount;
        title.Agencia__c = selectiveProcess.Institui_o_n__r.Agencia__c;
        title.Conta__c = selectiveProcess.Institui_o_n__r.Conta__c;
        title.Digito__c = selectiveProcess.Institui_o_n__r.Digito__c;
        title.DataEmissao__c = titleDate;
        title.DataVencimento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c.equals('Boleto') ? validaDiaDeSemana(titleDate.addDays(Integer.valueOf(selectiveProcess.Vencimento_Boleto_dias__c))) : titleDate;
        title.DataProcessamento__c = Date.today();
        title.Instrucao__c = selectiveProcess.Instrucoes_Boleto__c;
        title.OpcaoPagamento__c = paymentType.Payment_Type_Braspag__r.Name;
        title.MeioPagamento__c = paymentType.Payment_Type_Braspag__r.TipoMeioPagamento__c;
        title.CodigoMeioPagamento__c = paymentType.Payment_Type_Braspag__r.Codigo__c;
        title.Chave_Bandeira__c = paymentType.Payment_Type_Braspag__r.Chave_Bandeira__c;
        title.Bandeira__c = paymentType.Payment_Type_Braspag__r.Bandeira__c;
        title.Forma__c = paymentType.Payment_Type_Braspag__r.Forma__c;

        insert title;
        return title;
    }

    /*
        Atualiza titulos com o nosso numero do boleto
        @param idTitulo id para pesquisa e atualização
    */
    private String geraNossoNumero(String idTitulo) {
        Titulos__c titulo = getTitulosById(idTitulo)[0];
        String nossoNumero = SRM_BoletoBO.getInstance().getNossoNumero(titulo.CodigoMeioPagamento__c, titulo.CodigoTitulo__c);

        update new Titulos__c(Id = idTitulo, Name = nossoNumero, NossoNumero__c = nossoNumero);
        return nossoNumero;
    }
    public Date validaDiaDeSemana(Date dtVencimento) {
        Datetime dtTime = (DateTime) dtVencimento;
        String dayOfWeek = dtTime.format('EEEE');

        if (dayOfWeek.equals('Friday')) dtVencimento = dtVencimento.addDays(2);
        if (dayOfWeek.equals('Saturday')) dtVencimento = dtVencimento.addDays(1);

        return dtVencimento;
    }

    /*
        Monta os parametros da pagina
        @param tituloId id do titulo para atualização
    */
    private PageReference formParameters(String tituloId, String nossoNumero) {
        PageReference braspag = Page.SRM_Braspag;
        braspag.setRedirect(false);

        Map < String, String > parameters = braspag.getParameters();
        Titulos__c titulo = getTitulosById(tituloId)[0];

        //ALTER: MERCHANTID
        /*
        parameters.put( 'Id_Loja', opportunity.Processo_seletivo__r.Merchant__c );
        */
        parameters.put('Id_Loja', opportunity.Processo_seletivo__r.Merchant_Id__c);
        parameters.put('VENDAID', tituloId);
        parameters.put('NOSSONUMERO', nossoNumero);
        //parameters.put( 'VALOR', String.valueOf( opportunity.Amount ) );
        parameters.put('VALOR', String.valueOf(titulo.Valor__c));
        parameters.put('NOME', opportunity.Account.Name);
        parameters.put('CPF', opportunity.Account.Cpf_2__c);
        //ALTER: MERCHANTID
        /*
        parameters.put( 'CODPAGAMENTO', opPagto );
        */
        parameters.put('CODPAGAMENTO', payment);
        //parameters.put( 'PARCELAS', Braspag__c.getValues( 'PARCELAS' ).Valor__c  );
        parameters.put('PARCELAS', String.valueOf(titulo.Parcelado__c));
        //parameters.put( 'TIPOPARCELADO', Braspag__c.getValues( 'TIPOPARCELADO' ).Valor__c );

        if (titulo.Parcelado__c == 1)
            parameters.put('TIPOPARCELADO', '0');

        if (titulo.Parcelado__c == 0)
            parameters.put('TIPOPARCELADO', '0');
        if ((titulo.Parcelado__c != 0) && (titulo.Parcelado__c != 1))
            parameters.put('TIPOPARCELADO', '1');

        //parameters.put( 'TIPOPARCELADO', titulo.ProcessoSeletivo__r.Matr_cula_Online__c == 'Sim' ?  Braspag__c.getValues( 'TIPOPARCELADOSEMJUROS' ).Valor__c : Braspag__c.getValues( 'TIPOPARCELADO' ).Valor__c );
        if (titulo.Forma__c == 'D') {
            parameters.put('TRANSACTIONTYPE', '4');
        } else {
            parameters.put('TRANSACTIONTYPE', Braspag__c.getValues('TRANSACTIONTYPE').Valor__c);
        }


        parameters.put('INSTRUCOESBOLETO', opportunity.Processo_seletivo__r.Instrucoes_Boleto__c);
        parameters.put('TRANSACTIONCURRENCY', Braspag__c.getValues('TRANSACTIONCURRENCY').Valor__c);
        parameters.put('TRANSACTIONCOUNTRY', Braspag__c.getValues('TRANSACTIONCOUNTRY').Valor__c);
        //parameters.put( 'EXTRADYNAMICURL', Braspag__c.getValues( 'EXTRADYNAMICURL' ).Valor__c );
        //parameters.put( 'EXTRADYNAMICURL', String.valueOf(opportunity.Processo_seletivo__r.Niveis_de_Ensino__c) != 'Mestrado' ? Braspag__c.getValues( 'EXTRADYNAMICURL' ).Valor__c : System.URL.getSalesforceBaseURL().toExternalForm() + '/masters/SRM_BraspagRetorno' );
        if (opportunity.Processo_seletivo__r.Niveis_de_ensino__C == 'Mestrado') {
            parameters.put('EXTRADYNAMICURL', System.URL.getSalesforceBaseURL().toExternalForm() + '/masters/SRM_BraspagRetorno');
        } else if (opportunity.Processo_seletivo__r.Niveis_de_Ensino__c == 'Extensão') {
            parameters.put('EXTRADYNAMICURL', System.URL.getSalesforceBaseURL().toExternalForm() + '/matriculaonline/SRM_BraspagRetorno');
        } else {
            parameters.put('EXTRADYNAMICURL', Braspag__c.getValues('EXTRADYNAMICURL').Valor__c);
        }

        String isMatriculaOnline = [SELECT Id, Name, ProcessoSeletivo__r.Matr_cula_Online__c, Inscricao__c FROM Titulos__c WHERE Id =: titulo.id limit 1].ProcessoSeletivo__r.Matr_cula_Online__c;
        if (isMatriculaOnline == 'Sim') {
            string json = SRM_CreateRegistrationTitlesBO.createClientToJson(titulo.Inscricao__c);
            system.debug('tentativa de preencher clientSap' + json);
            TituloBO.retornaClienteSAP(json);
        }

        return braspag;
    }
    public List < Titulos__c > getTitulosById(String tituloId) {
        return [SELECT Id, Name, CodigoTitulo__c, NossoNumero__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Matr_cula_Online__c, ProcessoSeletivo__r.Merchant_Id__c, ProcessoSeletivo__r.CodigoSAPCampus__c, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Instituicao__c,
            DiasParaVencimento__c, CodigoMeioPagamento__c, MeioPagamento__c, Candidato_Aluno__c, Inscricao__r.Amount,
            Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c,
            Instituicao__r.EnderecoInstituicao__c, Instituicao__r.Name, DataVencimento__c, Instituicao__r.CNPJ__c, Instrucao__c, CodigoAutorizacao__c,
            Instituicao__r.CodigoSAP__c, Instituicao__r.CodigoBancoSAP__c, Instituicao__r.CentroLucro__c, Valor__c, Parcelado__c
            FROM Titulos__c
            WHERE Id =: tituloId
        ];
    }

    private Boolean test(String a) {

        ApexPages.Message msgInstallments = new ApexPages.Message(ApexPages.Severity.WARNING, 'Debug:::' + a);
        Apexpages.addMessage(msgInstallments);
        return false;
    }

    /*
        Metodo de validação do formulario
    */
    private Boolean formularioValidado() {
        //ALTER: MERCHANTID
        /*
        if( String.isBlank( opPagto ) ) {
        */
        if (opportunity.Processo_seletivo__r.Matr_cula_Online__c == 'Sim') {
            if (String.isBlank(installments)) {
                ApexPages.Message msgInstallments = new ApexPages.Message(ApexPages.Severity.WARNING, 'Selecione a Quantidade de Parcelas!');
                Apexpages.addMessage(msgInstallments);
                return false;
            }
        }

        if (String.isBlank(payment)) {
            ApexPages.Message msgPagto = new ApexPages.Message(ApexPages.Severity.WARNING, 'Selecione uma meio de pagamento!');
            Apexpages.addMessage(msgPagto);
            return false;
        }

        return true;
    }

    /*
        Abre o formulario de confirmação de inscrição
        @action botão inscrever
    */
    public PageReference openConfirmacaoInscricao() {
        PageReference confirmPage = Page.SRM_InscricaoConfirmada;
        Map < String, String > parameters = confirmPage.getParameters();
        parameters.put('id', instituicao.Id);
        return confirmPage;
    }

    /*
        Regatando oportunidade com dados adicionais
    */
    public List < Opportunity > getOpportunityById(String opportunityId) {
        System.debug('Map >>> ' + opportunityId);

        List < Opportunity > opp2 = [SELECT
            Id,
            Name,
            StageName,
            CreatedDate,
            AccountId,
            Amount,
            Data_Nascimento__c,
            Processo_Seletivo_Nome__c,
            IdInstituicao__c,
            IES__c,
            Tipo_de_Matr_cula__c,
            NomeCampus__c,
            Candidato_Aprovado__c,
            Account.Name,
            Account.Cpf_2__c,
            Account.RG__C,
            Account.Rua__c,
            Account.N_mero__c,
            Account.CreatedDate,
            Account.Sexo__c,
            Account.DataNascimento__c,
            Account.Cidade__c,
            Account.Estado__c,
            Account.Nacionalidade__c,
            Account.CPFdoResponsavel__c,
            Account.Nome_do_Pai__c,
            Account.Nome_da_Mae__c,
            Account.Bairro__c,
            Account.CEP__c,
            Account.Phone,
            Account.Complemento__c,
            Account.PersonEmail,
            Processo_seletivo__c,
            Processo_seletivo__r.Nome_do_Campus__c,
            Processo_seletivo__r.Valor_Taxa_Inscricao__c,
            Processo_seletivo__r.Institui_o_n__c,
            Processo_seletivo__r.Campus__c,
            Processo_seletivo__r.Vencimento_Boleto_dias__c,
            Processo_seletivo__r.Instrucoes_Boleto__c,
            //ALTER: MERCHANTID
            /*
            Processo_seletivo__r.Merchant__c,
            */
            Processo_seletivo__r.Merchant_Id__c,
            Processo_seletivo__r.MerchantKey__c,
            Processo_seletivo__r.Id_Institui_o__c,
            Processo_seletivo__r.IdPeriodoLetivo__c,
            Processo_seletivo__r.ID_Academus_ProcSel__c,
            Processo_seletivo__r.C_digo_da_Coligada__c,
            Processo_seletivo__r.Tipo_de_Matr_cula__c,
            Processo_seletivo__r.Matr_cula_Online__c,
            Processo_seletivo__r.Necessita_de_aprova_o_para_Inscri_o__c,
            Processo_seletivo__r.Niveis_de_Ensino__c,
            Oferta__r.Id,
            Oferta__r.Parcelas__c,
            X1_OpcaoCurso__r.ID_Academus_Oferta__c,
            X2_Op_o_de_curso__r.ID_Academus_Oferta__c,
            Aluno_Ex__c,
            Codigo_Utilizado__c
            FROM Opportunity
            where
            Id =: opportunityId
        ];

        System.debug('Map >>> ' + opp2);
        return opp2;
    }
    public List < Institui_o__c > getInstituicaoById(String instituicaoId) {
        return [SELECT Id, Name, EnderecoInstituicao__c, CNPJ__c
            FROM Institui_o__c
            WHERE Id =: instituicaoId
        ];
    }

    /*
        Retornas as instituições pelo nome
        @param instituicaoNameList nomes das instituições
    */
    public List < Institui_o__c > getInstituicaoByName(List < String > instituicaoNameList) {
        return [SELECT Id, Name, Lead_Map__c
            FROM Institui_o__c
            WHERE Lead_Map__c in: instituicaoNameList
        ];
    }

    /*
        Retorna todas as intituições
    */
    public List < Institui_o__c > getAllInstituicoes() {
        return [SELECT Id, Name
            FROM Institui_o__c
        ];
    }
    public List < MeioPagamentoIES__c > getMeioPagamentoByInstituicaoId(String instituicaoId) {
        return [SELECT Id, Instituicao__c, MeioPagamentoBraspag__c, MeioPagamentoBraspag__r.Name,
            MeioPagamentoBraspag__r.Codigo__c, MeioPagamentoBraspag__r.TipoMeioPagamento__c,
            MeioPagamentoBraspag__r.Bandeira__c, MeioPagamentoBraspag__r.Chave_Bandeira__c,
            MeioPagamentoBraspag__r.Forma__c
            FROM MeioPagamentoIES__c
            WHERE Instituicao__c =: instituicaoId
        ];
    }
    public List < Oferta__c > getInstallmentsByOfertaId(String ofertaId) {
        return [SELECT Id, Parcelas__c
            FROM Oferta__c
            WHERE Id =: ofertaId and Parcelas__c != ''
        ];
    }
    public List < Campus_Payment_Type__c > getPaymentTypesCardsByCampusId(String campusId) {
        return [SELECT Id, Campus__c, Payment_Type_Braspag__c, Payment_Type_Braspag__r.Name,
            Payment_Type_Braspag__r.Codigo__c, Payment_Type_Braspag__r.TipoMeioPagamento__c,
            Payment_Type_Braspag__r.Bandeira__c, Payment_Type_Braspag__r.Chave_Bandeira__c,
            Payment_Type_Braspag__r.Forma__c
            FROM Campus_Payment_Type__c
            WHERE Campus__c =: campusId AND Payment_Type_Braspag__r.TipoMeioPagamento__c =: 'Cartão'
        ];
    }
    public List < Campus_Payment_Type__c > getPaymentTypesByCampusId(String campusId) {
        return [SELECT Id, Campus__c, Payment_Type_Braspag__c, Payment_Type_Braspag__r.Name,
            Payment_Type_Braspag__r.Codigo__c, Payment_Type_Braspag__r.TipoMeioPagamento__c,
            Payment_Type_Braspag__r.Bandeira__c, Payment_Type_Braspag__r.Chave_Bandeira__c,
            Payment_Type_Braspag__r.Forma__c
            FROM Campus_Payment_Type__c
            WHERE Campus__c =: campusId
        ];
    }


    /* CODIGO PROMOCIONAL */

    public PageReference aplicarCodigo() {
        String opp = ApexPages.currentPage().getParameters().get('id');
        Opportunity oportunidade = [Select id, Name, Amount, Codigo_Utilizado__c from Opportunity where id =: opp];

        PageReference concluido;

        if (validaCodigoPromocional()) {
            CodigoPromocional__c codigo = [Select Id, Desconto__c, Disponivel__c, Expira__c, Oportunidade__c, Tipo_de_C_digo__c, TipoDesconto__c from CodigoPromocional__c where Name =: codigoPromocional];


            //Cálculo do novo valor
            oportunidade.Amount = oportunidade.Amount - (oportunidade.Amount * (Codigo.Desconto__c / 100));

            oportunidade.Codigo_Utilizado__c = true;
            codigo.Oportunidade__c = opp;
            update oportunidade;
            update codigo;

            //Recarrega a página com o novo valor 
            concluido = returnPaymentPage(opp);


        } else {
            //Não recarrega página se der errado
            concluido = null;

        }
        return concluido;

    }

    public Boolean validaCodigoPromocional() {

        List < CodigoPromocional__c > codigo = [Select Id, Desconto__c, Disponivel__c, Expira__c, Oportunidade__c, Tipo_de_C_digo__c, TipoDesconto__c from CodigoPromocional__c where Name =: codigoPromocional];

        if ((codigoPromocional != null) && (codigo.size() < 1)) {
            ApexPages.Message msgInstallments = new ApexPages.Message(ApexPages.Severity.WARNING, 'Código promocional inválido!');
            Apexpages.addMessage(msgInstallments);
            return false;
        }
        if ((codigoPromocional != null) && (codigo[0].Oportunidade__c != null)) {
            ApexPages.Message msgInstallments = new ApexPages.Message(ApexPages.Severity.WARNING, 'Código promocional já utilizado!');
            Apexpages.addMessage(msgInstallments);
            return false;
        }

        if ((codigoPromocional != null) && (codigo[0].Expira__c < Date.Today())) {
            ApexPages.Message msgInstallments = new ApexPages.Message(ApexPages.Severity.WARNING, 'Código promocional expirado!');
            Apexpages.addMessage(msgInstallments);
            return false;
        }

        return true;

    }

    //Recarrega a página
    public PageReference returnPaymentPage(String opportunityId) {

        PageReference paymentPage = Page.SRM_PagamentoInscricao;

        Map < String, String > parameters = paymentPage.getParameters();
        parameters.put('id', opportunityId);
        paymentPage.setRedirect(true);

        return paymentPage;
    }
}