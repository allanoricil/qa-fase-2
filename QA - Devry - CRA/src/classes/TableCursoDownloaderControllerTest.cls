@IsTest(SeeAllData=false)
public class TableCursoDownloaderControllerTest {
    private static TableCursoDownloaderController controller = new TableCursoDownloaderController();
    
    @IsTest
    public static void allTest(){
        basicsTest();
        shouldCreateOpportunities();
    }
    public static void shouldCreateOpportunities(){
        List<Account> accs = createAndGetTwoAccount();
        List<Opportunity> opps = createAndGetTwoOpportunities();
        TableCursoDownloaderController.createOpportunityAndAccount(accs,opps);
    }
    public static void basicsTest(){
        Processo_Seletivo__C processo = Processo_SeletivoMockup.createAndGetProcessoSeletivoWithOfertas();
    	controller.download();
        controller.getOferta();
        controller.getProcessoSeletivoCurrentYear();
        controller.idProcessoSeletivo = '0';
        controller.selectOfertaList = '0';
        controller.selectedValueProcessoSeletivo = processo.Name;
    }
    private static List<Account> createAndGetTwoAccount(){
        List<Account> accs = new List<Account>();
        
        Account acc = new Account (LastName = 'Joao Teste', CPF_2__C = '723.631.738-13' );
        Account basicAccount = ObjectFactory.CREATE_AND_GET_ACCOUNT(acc);
        accs.add(basicAccount);
        
        Account acc2 = new Account (LastName = 'Maria Teste', CPF_2__C = '668.691.756-75' );
        Account basicAccount2 = ObjectFactory.CREATE_AND_GET_ACCOUNT(acc2);
        accs.add(basicAccount2);
        
        return accs;
    }
    private static List<Opportunity> createAndGetTwoOpportunities(){
        
        Processo_Seletivo__C processo = Processo_SeletivoMockup.createAndGetProcessoSeletivoWithOfertas();
        Oferta__c oferta = [Select id from Oferta__C where Processo_Seletivo__C = :processo.id limit 1];
        
        List<Opportunity> opps = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.X1_OpcaoCurso__c = oferta.id;
        opp.Processo_seletivo__c = processo.id;
        opps.add(opp);
        
        Opportunity opp2 = new Opportunity();
        opp2.X1_OpcaoCurso__c = oferta.id;
        opp2.Processo_seletivo__c = processo.id;
        opps.add(opp2);
        
        return opps;
    }

}