public class TaskTriggerHandler implements TriggerHandlerInterface{
    private static final TaskTriggerHandler instance = new TaskTriggerHandler();

    /** Private constructor to prevent the creation of instances of this class.*/
    private TaskTriggerHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    * 
    * 
    *      for(Task a :trigger.new ){
       a.subject=a.Lista_de_A_es__c;
    }

    * 
    * 
    * 
    */
    public static TaskTriggerHandler getInstance() {
        return instance;
    }    
    
    /**
    * Method to handle trigger before update operations. 
    */
    public void beforeUpdate() {
        TaskUtil.atualizaOpp(Trigger.new);
    }
    
    /**
    * Method to handle trigger before insert operations. 
    */ 
    public void beforeInsert() {
    	TaskUtil.atualizaOpp(Trigger.new);
    }
    
    /**
    * Method to handle trigger before delete operations. 
    */
    public void beforeDelete() {}
    
    /**
    * Method to handle trigger after update operations. 
    */
    public void afterUpdate()  {

    }
    
    /**
    * Method to handle trigger after insert operations. 
    */
    public void afterInsert()  {
    }
    /**
    * Method to handle trigger after delete operations. 
    */
    public void afterDelete() {}
}