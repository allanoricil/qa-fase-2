/*
    @author Diego Moreira
    @class Classe controladora da pagina de retorno pagamento inscrição
*/
public with sharing class SRM_BraspagRetornoController { 
    private String merchantId;
    private String crypt;
    private String vendaId { get; private set; }

    /* 
        Construtor
    */
    public SRM_BraspagRetornoController() {
        merchantId = Braspag__c.getValues( 'MERCHANTID' ).Valor__c;
        crypt = ApexPages.currentPage().getParameters().get('crypt');
    } 

    /*
        Metodo principal de execução
    */
    public void execute() {
        try {
            Map<String, String> mapDecryptResult = SRM_BraspagCryptographyService.getInstance().braspagDecryptMapResult( merchantId, crypt );
            if( mapDecryptResult != null && 
                        !mapDecryptResult.isEmpty() ) {
                vendaId = mapDecryptResult.get('VENDAID');
                SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse result = consultaDadosBoleto( mapDecryptResult );
                updateTitulos( mapDecryptResult, result );
                mensagemRetorno( mapDecryptResult );
            } 
        } catch( Exception ex ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, ex.getMessage() + ' / ' + ex.getStackTraceString() ); 
            Apexpages.addMessage( msg );
        }                
    } 

    /*
        Atualiza os dados do titulo com o retorno BrasPag
        @param mapDecryptResult mapa de com todos os parametros de retorno
    */
    private void updateTitulos( Map<String, String> mapDecryptResult, SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse consultaBoleto ) {
        Titulos__c titulo = new Titulos__c();
        titulo.Id                           = mapDecryptResult.get( 'VENDAID' );
        titulo.CodigoBrasPag__c             = mapDecryptResult.get( 'BRASPAGORDERID' );
        titulo.CodigoRetornoBrasPag__c      = mapDecryptResult.get( 'CODRETORNO' );
        titulo.DescricaoRetornoBrasPag__c   = mapDecryptResult.get( 'DESRETORNO' );
        titulo.LinhaDigitavelBoleto__c      = mapDecryptResult.get( 'LINHADIGITAVEL' );
        titulo.TransacaoCartao__c           = mapDecryptResult.get( 'TRANSID' );
        titulo.TransacaoBoleto__c           = mapDecryptResult.get( 'TRANSACTIONID' );
        titulo.CodigoAutorizacao__c         = mapDecryptResult.get( 'CODAUTORIZACAO' );
        if( mapDecryptResult.get( 'TRANSID' ) != null ) titulo.Name = mapDecryptResult.get( 'TRANSID' );

        if( consultaBoleto != null ) {
            titulo.Name             = consultaBoleto.BoletoNumber;
            titulo.NossoNumero__c   = consultaBoleto.BoletoNumber;
            titulo.UrlBoleto__c     = consultaBoleto.BoletoUrl;
        }
            
        update titulo; 

    }

    /*
        Consulta os dados do Boleto no Braspag para atualização do titulo
    */
    private SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse consultaDadosBoleto( Map<String, String> mapDecryptResult ) {
        SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse response;
        Titulos__c titulo = TituloDAO.getInstance().getTitulosById( vendaId )[0];

        if( titulo.MeioPagamento__c.equals( 'Boleto' ) ) {
            //ALTER: MERCHANTID
			/*
            response = SRM_BraspagCryptographyService.getInstance().braspagGetBoletoData( titulo.ProcessoSeletivo__r.Merchant__c, mapDecryptResult.get( 'TRANSACTIONID' ), Braspag__c.getValues( 'REQUESTID' ).Valor__c );
			*/
            response = SRM_BraspagCryptographyService.getInstance().braspagGetBoletoData( titulo.ProcessoSeletivo__r.Merchant_Id__c, mapDecryptResult.get( 'TRANSACTIONID' ), Braspag__c.getValues( 'REQUESTID' ).Valor__c );
        }
        return response;
    }

    /*
        Exibe mensagem para o usuario
        @param mapDecryptResult mapa de com todos os parametros de retorno
    */
    private void mensagemRetorno( Map<String, String> mapDecryptResult ) {
        if( String.isNotBlank( mapDecryptResult.get('DESRETORNO') ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.info, 'Pagamento não foi concluído!' );
            Apexpages.addMessage( msg );
        }       

        if( String.isBlank( mapDecryptResult.get('DESRETORNO') ) ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.confirm, 'Inscrição realizada com sucesso!' );
            Apexpages.addMessage( msg );
        }       
    }

    /*
        Abre o formulario de pesquisa
        @action Link de abertura de pesquisa
    */
    public PageReference openFormPesquisa() {
        List<Titulos__c> titulos = TituloDAO.getInstance().getTitulosById( vendaId );
        PageReference page = new PageReference( Label.SRM_URL_CONSULTA );//Label.SRM_URL_INSCRICAO_ALUNO + titulos[0].Instituicao__c );
        return page;
    }

}