@IsTest
public class NewCaseControllerTest {
  	
     static NewCaseController  caseController = new NewCaseController();
    
    @testSetup
    public static void setUp(){
        Aluno__c aluno =  new Aluno__c();
        aluno.name = 'aluno test1 - 73212';
        aluno.R_A_do_Aluno__c = '0190183';
        aluno.Curso__c = 'Administração';
        aluno.Telefone__c = '8888888888';
        aluno.e_mail__c = 'alunotest1@dvb.edu.br';
        aluno.Celular__c = '99999999999';
        insert aluno;
        
        
        Motivos__c motivo = new Motivos__c();
        motivo.name = 'helpdesk';
        motivo.categoria__c =  'suport';
    	insert motivo;
    
    }
    
    @istest
    public static void  testBuscarNomeDoAlunoPorRA(){
        System.debug('call testBuscarNomedoAlunoPorRA');
        caseController.newCase = new Case();
        caseController.newCase.R_A_do_Aluno__c = '0190183';
        PageReference result = caseController.buscarNomeDoAlunoPorRA();
        PageReference expected = null;
        System.assertEquals(expected,result);
    }
    
      @istest
    public static void  testBuscarNomeDoAlunoPorRAInexistente(){
        System.debug('call testBuscarNomedoAlunoPorRA');
        caseController.newCase = new Case();
        caseController.newCase.R_A_do_Aluno__c = 'codigoinexistente';
        PageReference result = caseController.buscarNomeDoAlunoPorRA();
        PageReference expected = null;
        System.assertEquals(expected,result);
    }
    
    @IsTest
    public static void testCarregarMotivos(){
        caseController.newCase = new Case();
        caseController.newCase.Categoria_do_Atendimento__c = 'suport';
        PageReference result =  caseController.carregarMotivos();
        PageReference expected = null;
        System.assertEquals(expected,result);
    }
    
        
}