/*
	@author Diego Moreira
	@class WebService de titulos
*/
global class SRM_WSTitulosSf {
	/*
		Classe com os objetos de entrada
	*/	
	global class Titulos {
		WebService List<Rows> row = new List<Rows>();
	}
	
	/*
		Classe com os objetos de entrada
	*/	
	global class Rows {
		WebService String REF_DOC_NO;
		WebService String BELNR; 
		WebService String STATUS; 
	}

	/*
		Serviço de criação ou atualização de materiais
		@parans rows valores a serem salvos  
	*/  
	WebService static TitulosSfBO.TitulosResponse updateTitulos( Titulos rows ) {
		return TitulosSfBO.getInstance().atualizaTitulos( rows );
	}
}