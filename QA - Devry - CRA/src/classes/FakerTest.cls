@isTest
private class FakerTest {

	private static final Integer NUMBER_OF_ITEMS_TO_GENERATE = 100;

	@isTest 
	static void testNameCreation() {
		List<String> fakeNames = new List<String>();
		Map<String,String> fakeNamesMap = new Map<String,String>();
		for(Integer i = 0; i < NUMBER_OF_ITEMS_TO_GENERATE; i++){
			String name = Faker.getName();
			fakeNames.add(name);
			fakeNamesMap.put(name, name);
		}

		//testa se foram gerados 50 nomes
		System.assertEquals(NUMBER_OF_ITEMS_TO_GENERATE, fakeNames.size());
		//testa se foram gerados 50 nomes diferentes
		System.assertEquals(NUMBER_OF_ITEMS_TO_GENERATE, fakeNamesMap.size());
	}
	
	@isTest 
	static void testEmailCreation() {
		List<String> fakeEmails = new List<String>();
		Map<String,String> fakeEmailsMap = new Map<String, String>();
		for(Integer i = 0; i < NUMBER_OF_ITEMS_TO_GENERATE; i++){
			String email = Faker.getEmail();
			fakeEmails.add(email);
			fakeEmailsMap.put(email, email);
		}

		//testa se foram gerados 50 nomes
		System.assertEquals(NUMBER_OF_ITEMS_TO_GENERATE, fakeEmails.size());
		//testa se foram gerados 50 nomes diferentes
		System.assertEquals(NUMBER_OF_ITEMS_TO_GENERATE, fakeEmailsMap.size());
	}
	
}