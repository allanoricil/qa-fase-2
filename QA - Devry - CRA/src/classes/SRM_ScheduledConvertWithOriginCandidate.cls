global class SRM_ScheduledConvertWithOriginCandidate implements Schedulable {
	
	global void execute(SchedulableContext sc) { 
		LeadBO.getInstance().convertWithOriginCandidate();
	}
}