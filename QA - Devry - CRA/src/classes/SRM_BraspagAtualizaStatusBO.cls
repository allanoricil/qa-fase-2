/*
    @author Diego Moreira
    @class Classe de negocio para atualização de status de titulos pelo braspag
*/
public class SRM_BraspagAtualizaStatusBO implements IProcessingQueue {
    private static Map<String, String> mapStatus;
    Static{
        mapStatus = new Map<String, String>();
        mapStatus.put('0', 'Pago');
        mapStatus.put('1', 'Autorizado');
        mapStatus.put('2', 'Não Autorizado');
    }

	/* 
        Processa a fila de atualização de criação de titulos no SAP
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
   		atualizaStatusTitulo( queueId, payload );
    }

    /*
		Atualiza o status do titulo braspag
    */
    private static void atualizaStatusTitulo( String queueId, String payload ) {        
        Map<String, String> mapDecryptResult = new Map<String, String>();
        try{
            payload = payload.replace('{', '');
            payload = payload.replace('}', '');
            payload = payload.deleteWhitespace();
            
            for( String result : payload.split(',') ) {
                String[] parameter = result.split('=');
                mapDecryptResult.put( parameter[0], parameter[1] );
            }

	        TituloDAO.getInstance().updateData( TituloBO.getInstance().atualizaStatusTitulo( 
                                                                                    mapDecryptResult.get( 'NUMPEDIDO' ), 
                                                                                    mapStatus.get( mapDecryptResult.get('STATUS') ) ) );

            QueueBO.getInstance().updateQueue( queueId, '' );	
		} catch( DmlException ex ) {
			QueueBO.getInstance().updateQueue( queueId, 'StatusBraspag / ' + ex.getMessage() );
		} catch ( Exception ex ) {
            QueueBO.getInstance().updateQueue( queueId, 'StatusBraspag / ' + ex.getMessage() );
        }     
    }
}