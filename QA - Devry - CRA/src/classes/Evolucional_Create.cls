public class Evolucional_Create {

    public static void processingQueue( String queueId, String payload ) {
        syncToServer( queueId, payload );         
    }

    
    @future(Callout = true)
    public Static void syncToServer(String queueId, String payload){

            HttpRequest req = new HttpRequest();
            
            req.setEndpoint('http://api.evolucional.com.br/simulado-online/usuario/criar');
            
            req.setMethod( 'POST' );
            
            req.setHeader( 'Content-Type', 'application/json' );
            
            req.setTimeout( 120000 );
            
            req.setBody( payload );
            
            try {
                Http h = new Http();
                HttpResponse res = h.send( req );
                if( res.getStatusCode() == 200 )
                    processJsonBody( queueId, res.getBody() );                                                                                        
                else
                   QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + res.getStatusCode() + ' / ' + res.getStatus() );
            } catch ( CalloutException ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_SERVICES / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
   }

    private static void processJsonBody( String queueId, String jsonBody) {
        
        String errorMessage = '';

        Queue__c fila = [Select Oportunidade__c From Queue__c where Id =: queueId];

        Opportunity opp = [SELECT Name, Account.PersonEmail FROM Opportunity where Id =: fila.Oportunidade__c];

        Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(jsonBody);
        String userid = JSON.serialize(cObjMap.get('UserId'));

        updateOpp(opp, userid, fila);
   

        QueueBO.getInstance().updateQueue( queueId, errorMessage ); 

    }
    public Static void updateOpp(Opportunity oportunidade, String cadastro, Queue__c fila){
        fila.PayloadSAP__c = createLink(oportunidade, cadastro);
        update fila;    
    }

    public Static String createLink(Opportunity opp, String cadastro) {
    
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'token','18B257B8-9280-4B3C-B14E-674AD4EB0148');
        if((opp.Account.PersonEmail == null) && (cadastro != null)){
            gen.writeNullField('email');
            gen.writeStringField( 'userId', cadastro);
        }
        if((opp.Account.PersonEmail != null) && (cadastro == null)){
            gen.writeStringField( 'email', opp.Email__c);
            gen.writeNullField('userId');

        }
        if((opp.Account.PersonEmail != null) && (cadastro != null)){
            gen.writeNullField('email');
            gen.writeStringField( 'userId', cadastro);
        }

        gen.writeEndObject();
        return gen.getAsString();

    }
}