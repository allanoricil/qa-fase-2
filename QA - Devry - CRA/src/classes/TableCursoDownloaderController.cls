public class TableCursoDownloaderController {
    public String selectedValueProcessoSeletivo{get;set;}
    public String selectOfertaList{get;set;}
    public List<String> listIDOferta {get;set;}
    public String idProcessoSeletivo {get;set;}
    public List<SelectOption> getProcessoSeletivoCurrentYear(){
        
        List<SelectOption> options = new List<SelectOption>();
        
        for (Processo_seletivo__c processo : [SELECT Name FROM Processo_seletivo__c where Data_de_Abertura__c = THIS_YEAR]){
            options.add(new SelectOption(processo.Name,processo.Name));
        }
        
        return options;
    }
    public List<Oferta__C> getOferta(){
        if(selectedValueProcessoSeletivo!=null)
            idProcessoSeletivo = [Select ID FROM Processo_seletivo__c where Processo_Seletivo__c.Name = :selectedValueProcessoSeletivo][0].id;
        List<Oferta__c> ofertas = [SELECT ID,Name FROM Oferta__C where Processo_Seletivo__r.Name = :selectedValueProcessoSeletivo];
        for(Oferta__c ofer : ofertas){
            listIDOferta.add(ofer.id);
        }
        return ofertas;
    }
    
    public TableCursoDownloaderController(){
        listIDOferta = new List<String>();
    }
    public PageReference download(){
        PageReference pageRef = new PageReference('/resource/GenericSpreadsheetOferta');
        pageRef.setRedirect(true);
        return pageRef;
    }
    @RemoteAction
    public static List<Opportunity> createOpportunityAndAccount(List<Account> accounts,List<Opportunity> opportunities){
        List<Account> accountsCreated = createAccounts(accounts);
        upsert accountsCreated CPF_2__c;
        
        List<Opportunity> opportunitiesCreated = createOpportunity(accountsCreated,opportunities);
        System.debug(opportunitiesCreated);
        upsert opportunitiesCreated Unique__C;
        
        Database.UpsertResult[] srList = Database.upsert(opportunitiesCreated, false);
        
        return opportunitiesCreated;
    }
    private static List<Opportunity> createOpportunity(List<Account> accounts,List<Opportunity> opportunities){
        if(accounts.size() != opportunities.size()) throw new SerializationException ('Account and opportunities needs same size');
        List<Opportunity> opportunitiesNormaliezed = new List<Opportunity>();
        for (Integer i = 0; i<accounts.size();i++){
            Opportunity opp = ObjectFactory.CREATE_AND_GET_OPPORTUNITY(accounts[i],opportunities[i]);
            opportunitiesNormaliezed.add(opp);
        }
        return opportunitiesNormaliezed;
    }
    private static List<Account> createAccounts(List<Account> accounts){
       List<Account> accountsNormalized = new List<Account>();
        for(Account acc : accounts){
            Account accN = ObjectFactory.CREATE_AND_GET_ACCOUNT(acc);
            accountsNormalized.add(accN);
        }
        return accountsNormalized;
    }
}