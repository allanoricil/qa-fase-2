global class CRA_ScheduleChargingUpdate implements Schedulable {
	global void execute(SchedulableContext sc) {
		CRA_ProcessCaseCharging.execute();		
	}
}