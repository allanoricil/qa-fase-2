/*
    @author Diego Moreira
    @class Classe controladora da pagina de retorno da inscrição
*/
public with sharing class SRM_InscricaoConfirmadaController {
	/* 
        Construtor
    */
	public SRM_InscricaoConfirmadaController() {}

	/*
        Abre o formulario de pesquisa
        @action Link de abertura de pesquisa
    */
    public PageReference openFormPesquisa() {
        PageReference page = new PageReference(Label.SRM_URL_CONSULTA);
        return page;
    }

    public PageReference openFormPesquisaIbmec() {
        PageReference page = new PageReference(Label.SRM_URL_CONSULTA_IBMEC);
        return page;
    }
 
    //PageReference page = new PageReference( Label.SRM_URL_INSCRICAO_ALUNO + ApexPages.currentPage().getParameters().get('id') );
}