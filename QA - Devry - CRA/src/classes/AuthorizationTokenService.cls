/*
    @author Diego Moreira
    @class Classe de serviço Devry captura token de autorização
*/
public class AuthorizationTokenService { 
   	/*
		Faz chamada para geração da chave de autorização
    */  
    public Static Map<String, String> getAuthorizationToken() {
    	Map<String, String> mapToReturn = new Map<String, String>();
		if(test.IsRunningTest()){
			
		}else{
			
		
		try {
			HttpRequest req = new HttpRequest();			
			req.setEndpoint( WSSetup__c.getValues('Token').Endpoint__c );
			req.setMethod( 'GET' );
			req.setHeader( 'API-TOKEN', WSSetup__c.getValues('Token').API_Token__c );
			req.setTimeout( 120000 );
			
			Http h = new Http();
			HttpResponse res = h.send( req );
 
			Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
		
			if( res.getStatusCode() == 200 ) 
				mapToReturn.put( '200', (String) postData.get( 'auth_token' ) );
			else
				mapToReturn.put( '401', (String) postData.get( 'mensagem' ) );
		} catch ( CalloutException ex ) {
			mapToReturn.put( '401', ex.getMessage() );
		}
		}
		return mapToReturn;	
    }
}