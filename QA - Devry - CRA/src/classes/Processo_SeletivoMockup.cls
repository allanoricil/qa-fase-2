public class Processo_SeletivoMockup {
    public static Processo_Seletivo__C createAndGetProcessoSeletivoWithOfertas(){
        
        Institui_o__c ins = Institui_oMockup.createAndGetInstituicao();
        
        Campus__c campus = CampusMockup.createAndGetCampusMockup();
        
        Curso__c curso = CursoMockup.createAndGetCursoMockup();
        
        Area_Curso__c area = Area_CursoMockup.createAndGetCurso();
        
        Processo_Seletivo__C processo = new Processo_Seletivo__C(
            Name = 'Processo Teste', 
            Data_Validade_Processo__c= System.today()+60,
            Status_Processo_Seletivo__c = 'Planejado',
            Institui_o_n__c = ins.id,
            Data_de_Abertura__c = System.today(),
            Campus__C = campus.id
        );
        insert processo;
               
        Oferta__C oferta1 = new Oferta__C(Name = 'Oferta 1',
                                          Campus__C = campus.id,
                                          Institui_o_n__c = ins.id,
                                          Processo_seletivo__c = processo.id,
                                          Curso__c = curso.id,
                                          rea_Curso__C = area.id,
                                          Tipo_do_Curso__c = 'Tipo Teste'
                                         );
        insert oferta1;
        
        Oferta__C oferta2 = new Oferta__C(Name = 'Oferta 2',
                                          Campus__C = campus.id,
                                          Institui_o_n__c = ins.id,
                                          Processo_seletivo__c = processo.id,
                                          Curso__c = curso.id,
                                          rea_Curso__C = area.id,
                                          Tipo_do_Curso__c = 'Tipo Teste'
                                         );
        insert oferta2;
        
        return processo;
        
    }
}