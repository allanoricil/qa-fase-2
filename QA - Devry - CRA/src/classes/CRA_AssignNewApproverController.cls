public without sharing class CRA_AssignNewApproverController {
	public Boolean error{get; set;}
	public Case myCase;
    public PageReference pageRef{get; set;}
    
    public CRA_AssignNewApproverController(ApexPages.StandardController stdController) {
    	List<String> fields = new List<String>();
        fields.add('Id');
        fields.add('Sub_Status__c');
        fields.add('Status');
        fields.add('Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c');
        fields.add('Aluno__r.R_A_do_Aluno__c');
        fields.add('Aluno__r.Campus_n__r.C_digo_do_Campus__c');
        fields.add('Assunto__r.Aprovacao_Diretor__c');
        fields.add('Assunto__r.Aprovacao_Coordenador__c');
        fields.add('Diretor__c');
        fields.add('Coordenador__c');
        if(!Test.isRunningTest())
            stdController.addFields(fields);        
        this.myCase = (Case)stdController.getRecord();
        this.error = false;
        this.pageRef = new PageReference('/' + myCase.Id);
        this.pageRef.setRedirect(true);
    }
    
    public pageReference submit(){
        if(myCase.Status == 'Em aprovação'){
            List<CraDirectorCords.DirectorCord> directorCords = CraDirectorCords.getDirectorCords(
				myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c,
				myCase.Aluno__r.R_A_do_Aluno__c,
				myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c
			);
            Id oldDiretorId = myCase.Diretor__c;
            Id oldCoordId = myCase.Coordenador__c;
            List<User> aprovadores = [SELECT Id, DBNumber__c FROM User WHERE DBNumber__c != null AND IsActive = true AND (Profile.Name = 'CRA - Aprovador Acadêmico' OR Profile.Name = 'CRA - Aprovador Diretor')];
			Map<String, Id> codigoIdAprovadores = new Map<String, Id>();
            for(User aprovador : aprovadores){
                codigoIdAprovadores.put(aprovador.DBNumber__c, aprovador.Id);
            }
            if(directorCords != null){
                Boolean updateCase = false;
                
                if(String.isBlank(codigoIdAprovadores.get(directorCords.get(0).DiretorCodPessoa))){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'O diretor ' + directorCords.get(0).DiretorNome + ' não está cadastrado no Salesforce.'));
            		error = true;
                }
                
                if(String.isBlank(codigoIdAprovadores.get(directorCords.get(0).CoodCodigo))){
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'O coordenador ' + directorCords.get(0).Nome + ' não está cadastrado no Salesforce.'));
            		error = true;
                }
                
                if(error)
                    return null;
                
        		//if(myCase.Assunto__r.Aprovacao_Diretor__c){
                    if(myCase.Diretor__c != codigoIdAprovadores.get(directorCords.get(0).DiretorCodPessoa) && !String.isBlank(String.valueOf(codigoIdAprovadores.get(directorCords.get(0).DiretorCodPessoa))) ){
                        myCase.Diretor__c = codigoIdAprovadores.get(directorCords.get(0).DiretorCodPessoa);
                        updateCase = true;
                    }
        		//}
                
        		//if(myCase.Assunto__r.Aprovacao_Coordenador__c){
                    if(myCase.Coordenador__c != codigoIdAprovadores.get(directorCords.get(0).CoodCodigo) && !String.isBlank(String.valueOf(codigoIdAprovadores.get(directorCords.get(0).CoodCodigo))) ){
                        myCase.Coordenador__c = codigoIdAprovadores.get(directorCords.get(0).CoodCodigo);
                        updateCase = true;
                    }        			
        		//}
                
                if(updateCase){
                    List<ProcessInstanceWorkItem> workItemList = [SELECT p.ProcessInstance.Status, p.ProcessInstance.TargetObjectId,p.ProcessInstanceId,p.OriginalActorId,p.Id,p.ActorId FROM ProcessInstanceWorkitem p WHERE p.ProcessInstance.TargetObjectId =: myCase.Id];
                    List<ProcessInstanceNode> nodeList = [SELECT ProcessNode.Name FROM ProcessInstanceNode WHERE ProcessInstanceId =: workItemList.get(0).ProcessInstanceId AND CompletedDate = null LIMIT 1];
                    system.debug('nodeList: ' + nodeList.get(0).ProcessNode.Name + ' ' + nodeList );
                    if(nodeList.get(0).ProcessNode.Name == 'Aprovação Diretor' ){
                        workItemList.get(0).ActorId  = myCase.Diretor__c;
                        try{
                            update myCase;
                            update workItemList;
                        }
                        catch(Exception e){
                            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Não foi possível atualizar a solicitação. Verifique se você possui permissão para editá-la.'));
            				error = true;
            				return null; 
                        }
                        return pageRef;
                    }                    	
                    else if((nodeList.get(0).ProcessNode.Name == 'Aprovação Coordenador' || nodeList.get(0).ProcessNode.Name == 'Fila de aprovação acadêmica') && myCase.Assunto__r.Aprovacao_Coordenador__c){
                        workItemList.get(0).ActorId  = myCase.Coordenador__c;
                        try{
                            update myCase;
                            update workItemList;
                        }
                        catch(Exception e){
                            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Não foi possível atualizar a solicitação. Verifique se você possui acesso para editá-la.'));
            				error = true;
            				return null; 
                        }
                        return pageRef;
                    }
                    else{
                        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'O aprovador não pode ser substituído pois não é o coordenador nem o diretor dessa solicitação.'));
            			error = true;
            			return null; 
                    }
                       
                }  
                else{
                    if(String.isBlank(String.valueOf(codigoIdAprovadores.get(directorCords.get(0).CoodCodigo))) || String.isBlank(String.valueOf(codigoIdAprovadores.get(directorCords.get(0).DiretorCodPessoa))))
                    	ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Não foi encontrado o usuário aprovador no Salesforce, por favor entre em contato com o administrador do sistema.'));
                    else
                    	ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Recebidos aprovadores ' + directorCords.get(0).DiretorNome + ' (Diretor) e ' + directorCords.get(0).Nome + ' (Coordenador), os quais já são aprovadores deste aluno.'));
            		error = true;
            		return null; 
                }                          
        	}
            else{
				ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Não foi encontrado coordenador ou diretor para esse aluno.'));
            	error = true;
            	return null;                
            }
            
        }
        else{
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Não é permitido mudar o aprovador neste estágio'));
            error = true;
            return null;
        }
    }
    
    public PageReference goBack(){
        return pageRef;
    }
}