public class CRA_CobrancaUtil {

	public static void AfterInsertCobranca(List<Cobranca__c> newCobrancasList){
		Set<Id> cobrancaIds = new Set<Id>();
		Set<Id> caseIds = new Set<Id>();
		for(Cobranca__c cobranca : newCobrancasList){
			caseIds.add(cobranca.Caso__c);
			cobrancaIds.add(cobranca.Id);
		}
		createCharging(cobrancaIds, caseIds);
	}

	@Future(callout=true)
	public static void  createCharging(Set<Id>cobrancaIds, Set<Id> caseIds){
		List<Aluno__c> alunoList = new List<Aluno__c>();
		Map<Id, Case> casesMap = new Map<Id, Case>([SELECT  Id, Aluno__r.Institui_o_n__r.CodigoSAP__c, Assunto__r.Servico__r.ProductCode, Account.Name,
													Assunto__r.Servico__r.Name, Aluno__r.Campus_n__r.CodigoSAP__c, Aluno__r.Institui_o_n__r.Banco__c,
                                                    Aluno__r.Institui_o_n__r.Banco_CRA__c, Aluno__r.Institui_o_n__r.CentroLucro_CRA__c, Aluno__r.Institui_o_n__r.Conta_CRA__c,
													Assunto__r.Name, Aluno__r.Institui_o_n__r.Conta_Caixa__c, Aluno__r.R_A_do_Aluno__c, Aluno__r.Rua__c, 
													Aluno__r.Institui_o_n__r.CentroLucro__c, Aluno__r.Name, Aluno__r.Sexo__c, Aluno__r.Cidade__c, Aluno__c,
													Aluno__r.Numero__c, Aluno__r.Complemento__c, Aluno__r.Bairro__c, Aluno__r.CEP__c, Aluno__r.Codigo_SAP__c,
													Aluno__r.Estado__c, Aluno__r.Telefone__c, Aluno__r.CPF__c, Aluno__r.Institui_o_n__r.C_digo_da_Coligada__c,
													Aluno__r.Celular__c, Aluno__r.e_mail__c, Aluno__r.Conta__r.VoceEstrangeiro__c FROM Case WHERE Id IN: caseIds]);
		List<Cobranca__c> cobrancaList = new List<Cobranca__c>([SELECT Id, Name, Caso__c, DataVencimento__c, Valor__c FROM Cobranca__c WHERE Id IN: cobrancaIds]);
		
		for(Cobranca__c cobranca: cobrancaList){
			String findClienteId;
			String findEmpresaId;
			if(String.isBlank(casesMap.get(cobranca.Caso__c).Aluno__r.Codigo_SAP__c)){
				Map<String, String> mapSAPId = CRA_CreateCaseChargings.findClienteSAPcode(casesMap.get(cobranca.Caso__c));
                findClienteId = mapSAPId.get('id_cliente');
                findEmpresaId = mapSAPId.get('empresasap');
                if(!String.isBlank(findClienteId) &&!String.isBlank(findEmpresaId) ){
                    findClienteId = mapSAPId.get('id_cliente').leftPad(10, '0');
                    Aluno__c aluno = new Aluno__c(
                        Id = casesMap.get(cobranca.Caso__c).Aluno__c, 
                        Codigo_SAP__c = findClienteId
                    );
                    alunoList.add(aluno);
                }                
			}
			else{
				findClienteId = casesMap.get(cobranca.Caso__c).Aluno__r.Codigo_SAP__c;
				findEmpresaId = casesMap.get(cobranca.Caso__c).Aluno__r.Institui_o_n__r.CodigoSAP__c;
			}			
    		String cobranca_json = CRA_CreateCaseChargings.createChargingToJson(casesMap.get(cobranca.Caso__c), cobranca, findClienteId, findEmpresaId);
    		system.debug('cobranca_json: ' + cobranca_json);
    		CRA_CreateCaseChargings.syncToServer(cobranca_json);
		}

		Database.update(alunoList);
	}

	public static List<Case> CreateChargingfromCase(Set<Id> casosId){
		
		List<Case> casos = [SELECT Id, Status, Valor__c, Aluno__r.Campus_n__r.Horario_Comercial__c, 
							Assunto__r.Pagamento__c, Assunto__r.Cobranca_manual__c, Assunto__r.Aprovacao_Imediata__c, 
							Assunto__r.Aprovacao_Coordenador__c, Assunto__r.Aprovacao_Diretor__c, 
							Assunto__r.Aprovacao_Supervisor__c, Assunto__r.Aprovacao_manual__c, Vencimento_do_boleto__c
							FROM Case 
							WHERE Id IN: casosId AND Valor__c > 0 AND Valor__c != null];

		Map<Id, Cobranca__c> caseChargingsMap = new Map<Id,Cobranca__c>();

		for(Case caso : casos){
			
			if((caso.Assunto__r.Pagamento__c || caso.Assunto__r.Cobranca_manual__c) && caso.Valor__c != null && caso.Valor__c > 0 ){
				
				Datetime dataVencimento = BusinessHours.add(caso.Aluno__r.Campus_n__r.Horario_Comercial__c, system.now(), 24*60*60*1000);
				Datetime dataLimite = BusinessHours.add(caso.Aluno__r.Campus_n__r.Horario_Comercial__c, system.now(), 4*24*60*60*1000);
				
				Cobranca__c charging = new Cobranca__c();
				charging.Valor__c = caso.Valor__c;
				charging.Caso__c = caso.Id;
				charging.Status__c = 'Em Aberto';
				charging.DataVencimento__c = Date.newInstance(dataVencimento.year(), dataVencimento.month(), dataVencimento.day());
				charging.Data_Limite_Pagamento__c = Date.newInstance(dataLimite.year(), dataLimite.month(), dataLimite.day());
				
				caso.Vencimento_do_boleto__c = charging.DataVencimento__c;
                caso.Data_Limite_Pagamento__c = charging.Data_Limite_Pagamento__c;
				caso.Status = 'Aguardando pagamento';
				caso.Apex_context__c = true;
			
				caseChargingsMap.put(caso.Id, charging);

			}
		}

		if(caseChargingsMap.size() > 0)
			Database.insert(caseChargingsMap.values(), false);

		return casos;

	}

	public static void AfterUpdateCobranca(Map<Id, Cobranca__c> newCobrancasMap, Map<Id, Cobranca__c> oldCobrancasMap){
		Set<Id> casesStatusIdSet = new Set<Id>();
		for(Cobranca__c cobranca : newCobrancasMap.values()){
			if(cobranca.Status__c == 'Pago' && oldCobrancasMap.get(cobranca.Id).Status__c == 'Em aberto'){
				casesStatusIdSet.add(cobranca.Caso__c);
			}
		}


		if(!casesStatusIdSet.isEmpty()){
			List<Case> casesStatusList = [SELECT Id, Status_de_Pagamento__c, Status FROM Case WHERE Id IN: casesStatusIdSet AND Status = 'Aguardando pagamento'];
			if(!casesStatusList.isEmpty()){
				for(Case caso : casesStatusList){
					caso.Status_de_Pagamento__c = 'Pago';
					caso.Status = 'Em andamento';
					caso.Apex_context__c = true;
				}
				Database.update(casesStatusList);
			}
		}
	}

	public static void criaClienteSap(List<Cobranca__c> newCobrancas, List<Cobranca__c> oldCobrancas){
		Case caso = [select id, CaseNumber, Aluno__c from Case where id=: newCobrancas[0].Caso__c];
		
		if(newCobrancas[0].Cliente_SAP__c == null){

			string jsonBodySap = createClientToJson(caso.Aluno__c);
			String clienteSapQueue = QueueBO.getInstance().createQueueWithObjectId(QueueEventNames.RETORNA_CLIENTE_SAP.name(), jsonBodySap, newCobrancas[0].Id);
	    			newCobrancas[0].Cliente_SAP_Queue__c = clienteSapQueue;
		}
		
		
	}

	public static void BeforeUpdateCobranca(List<Cobranca__c> newCobrancas, List<Cobranca__c> oldCobrancas){
		//Case caso = [select id, CaseNumber, Aluno__c from Case where id=: newCobrancas[0].Caso__c];
		
		if(newCobrancas[0].Status__c == 'Cancelado' && oldCobrancas[0].Status__c != 'Cancelado'){
			
			String queueEstorno = QueueBO.getInstance().createQueueWithObjectId(QueueEventNames.ESTORNO_TAXAS_REQUERIMENTO.name(), '', newCobrancas[0].Id);
	    			newCobrancas[0].Estorno_Integra_o__c = queueEstorno;	

		}
		
		
	}

	public static String createClientToJson(String alunoId) {
        Aluno__c aluno = [select id, Name, Conta__c, Institui_o_n__r.C_digo_da_Coligada__c from Aluno__c where id =: alunoId];

        Account conta = [Select id, Rua__c, Name, Nacionalidade__c, Sexo__c, Complemento__c,
            N_mero__c, CEP__c, Cidade__c, Estado__c, Phone, PersonMobilePhone,
            PersonEmail, CPF_2__c from Account where id =: aluno.Conta__c
        ];

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('cliente');
        //gen.writeStartArray();
        gen.writeStartObject();
        //CAMPOS DO JSON PARA O CLIENTE SAP
        gen.writeStringField('nome', conta.Name);
        gen.writeStringField('kunnr', 'SALESFORCE'); //FIXO

        /* VALORES PARA KTOKD - DEFINIR IF PARA CADA CASO
        DB01 – Brasileiro Pessoa
        Física
        DB02 – Estrangeiro Pessoa
        Física
        DB03 – Pessoa Jurídica;
        */

        if (conta.nacionalidade__c == 'Brasileiro') {
            gen.writeStringField('ktokd', 'DB01');
        } else {
            gen.writeStringField('ktokd', 'DB02');
        }

        //SENHOR OU SENHORA
        if (conta.Sexo__c == 'Masculino') {
            gen.writeStringField('anred', 'SENHOR');
        } else {
            gen.writeStringField('anred', 'SENHORA');
        }
        gen.writeStringField('street', conta.Rua__c == null ? '' : conta.Rua__c);
        gen.writeStringField('houseno', conta.N_mero__c == null ? '' : conta.N_mero__c);
        gen.writeStringField('strsuppl1', conta.Complemento__c == null ? '' : conta.Complemento__c);
        gen.writeStringField('ort02', ''); //CAMPO FALTANDO  
        gen.writeStringField('pstlz', conta.CEP__c == null ? '' : conta.CEP__c);
        gen.writeStringField('ort01', conta.Cidade__c == null ? '' : conta.Cidade__c);
        gen.writeStringField('regio', conta.Estado__c == null ? '' : conta.Estado__c);
        gen.writeStringField('tel1numbr', conta.Phone == null ? '' : conta.Phone);
        gen.writeStringField('tel1text', ''); //CAMPO FALTANDO
        gen.writeStringField('mobnumber', conta.PersonMobilePhone == null ? '' : conta.PersonMobilePhone);
        gen.writeStringField('faxnumber', ''); //CAMPO FALTANDO
        gen.writeStringField('faxextens', ''); //CAMPO FALTANDO
        gen.writeStringField('email', conta.PersonEmail == null ? '' : conta.PersonEmail);
        gen.writeStringField('stcd1', ''); //PARA ALUNO NÃO É NECESSÁRIO CNPJ
        gen.writeStringField('stcd2', conta.CPF_2__c == null ? '' : conta.CPF_2__c.replaceAll('[|.|-]', ''));
        gen.writeStringField('aluno', '1'); // 1- ALUNO 0- FUNCIONÁRIO
        gen.writeStringField('codcoligada', aluno.Institui_o_n__r.C_digo_da_Coligada__c == null ? '' : aluno.Institui_o_n__r.C_digo_da_Coligada__c);

        gen.writeEndObject();
        //gen.writeEndArray();
        gen.writeEndObject();

        system.debug('gen.getAsString(): ' + gen.getAsString());

        return gen.getAsString();
    }


}