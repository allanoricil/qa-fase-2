global class FixEmailBatch implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator('SELECT PersonEmail, Id FROM Account WHERE PersonEmail != null AND (NOT(PersonEmail LIKE \'%.dev\'))');

	}
    
    global void execute(Database.BatchableContext BC, List<Account> accountList) {       
        for(Account conta: accountList){
            String currentEmail = conta.PersonEmail;
            conta.PersonEmail = currentEmail + '.dev';
        }
        Database.update(accountList, false);
    }
    
    global void finish(Database.BatchableContext BC) {
		
	}
}