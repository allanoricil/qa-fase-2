/*
    @author Diego Moreira
    @class Classe DAO do objeto Titulo
*/
public with sharing class TituloDAO extends SObjectDAO {
    /*
        Singleton
    */
    private static final TituloDAO instance = new TituloDAO();    
    private TituloDAO(){}
    
    public static TituloDAO getInstance() {
        return instance;
    }

    /*
        Busca o titulo pelo id 
        @param tituloId id do titulo
    */
    //ALTER: MERCHANTID
    /*
    public List<Titulos__c> getTitulosById( String tituloId ) {
        return [SELECT Id, Name, CodigoTitulo__c, NossoNumero__c, ProcessoSeletivo__r.Merchant__c, ProcessoSeletivo__r.CodigoSAPCampus__c, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Instituicao__c,
                    DiasParaVencimento__c, Candidato_Aluno__r.Name, Candidato_Aluno__r.Cpf_2__c, CodigoMeioPagamento__c, MeioPagamento__c, Candidato_Aluno__c, Inscricao__r.Amount, 
                    Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c,
                    Instituicao__r.EnderecoInstituicao__c, Instituicao__r.Name, DataVencimento__c, Instituicao__r.CNPJ__c, Instrucao__c, CodigoAutorizacao__c,
                    Instituicao__r.CodigoSAP__c, Instituicao__r.CodigoBancoSAP__c, ProcessoSeletivo__r.Name, Instituicao__r.CentroLucro__c, Valor__c
                    FROM Titulos__c 
                    WHERE Id = :tituloId];
    }
    */
    
    /*
        Busca o titulo pelo id 
        @param tituloId id do titulo
    */
    public List<Titulos__c> getTitulosById( String tituloId ) {
        return [SELECT Id, Name, CodigoTitulo__c, NossoNumero__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Matr_cula_Online__c, ProcessoSeletivo__r.Merchant_Id__c, ProcessoSeletivo__r.CodigoSAPCampus__c, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Instituicao__c,
                    DiasParaVencimento__c, Candidato_Aluno__r.Name, Candidato_Aluno__r.Cpf_2__c, CodigoMeioPagamento__c, MeioPagamento__c, Candidato_Aluno__c, Inscricao__r.Amount, 
                    Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c,
                    Instituicao__r.EnderecoInstituicao__c, Instituicao__r.Name, DataVencimento__c, Instituicao__r.CNPJ__c, Instrucao__c, CodigoAutorizacao__c,
                    Instituicao__r.CodigoSAP__c, Instituicao__r.CodigoBancoSAP__c, Instituicao__r.CentroLucro__c, Valor__c, Cliente_Sap__c
                    FROM Titulos__c 
                    WHERE Id = :tituloId];
    }
 
    /*
        Busca o titulo pelo id 
        @param tituloId ids dos titulos
    */
    public List<Titulos__c> getTitulosByIds( List<String> titulosId ) {
        return [SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c,
                        DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                        Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Cliente_Sap__c
                        FROM Titulos__c
                        WHERE Id in :titulosId];
    }


    /*
        Busca o titulo pelo id para criar título no SAP
        @param tituloId ids dos titulos
    */
    public List<Titulos__c> getCreateTitulosByIds( List<String> titulosId ) {
        return [SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c, Cliente_Sap__c,
                        DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                        Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Parcelado__c,
                        Inscricao__c, Cpf__c, MatriculaOnline__c
                        FROM Titulos__c
                        WHERE Id in :titulosId
                        AND CodigoSAP__c = null
                        AND (NossoNumero__c != null
                        OR TransacaoCartao__c != null)];
    }


    /*
        Busca o titulo pelo id para atualizar título no SAP
        @param tituloId ids dos titulos
    */
    public List<Titulos__c> getUpdateTitulosByIds( List<String> titulosId ) {
        String statusTitulo = 'Em Aberto';
        String meioPgto     = 'Cartão';
        String codErro      = '$';

        return [SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c, Cliente_Sap__c,
                        DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                        Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Parcelado__c
                        FROM Titulos__c
                        WHERE Id in :titulosId
                        AND (CodigoSAP__c != :codErro
                        OR  CodigoSAP__c != null)
                        AND Status__c != :statusTitulo
                        AND MeioPagamento__c = :meioPgto];
    }
    
    /* 
	Busca os titulos em aberto da oportunidade
	@param opportunityId   
	*/
    public List<Titulos__c> getTitulosAbertoByOportunidadeId( String opportunityId ) {
        return [SELECT Id, Name, Candidato_Aluno__c, Inscricao__c, Documento__c, Instituicao__c, ProcessoSeletivo__c, Valor__c, Agencia__c, Cliente_Sap__c,
                Conta__c, Digito__c, DataEmissao__c, DataVencimento__c, DataProcessamento__c, Instrucao__c, OpcaoPagamento__c, MeioPagamento__c, Inscricao__r.Amount, DescricaoRetornoBrasPag__c,
                Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, Instituicao__r.Conta_Caixa__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Status__c
                    FROM Titulos__c
                    WHERE Inscricao__c = :opportunityId order by CreatedDate desc
                    //AND TituloVencido__c = false
                    //AND MeioPagamento__c = 'Boleto'
                    ];
    }

    /*
        Busca o titulo pelo codigo do SAP 
        @param codigoSAP codigo SAP do titulo
    */
    public List<Titulos__c> getTitulosByCodigoSAP( String codigoSAP, String empresaSAP ) {
        return [SELECT Id, CodigoSAP__c, EmpresaSAP__c, ExercicioSAP__c, Status__c, Inscricao__c, Instituicao__r.CodigoBancoSAP__c, Instituicao__r.Conta_Caixa__c, Cliente_Sap__c
                    FROM Titulos__c 
                    WHERE CodigoSAP__c = :codigoSAP
                    AND EmpresaSAP__c = :empresaSAP];
    }

    /*
        Busca titulos que não tiveram a primeira atualização do Braspag
    */
    //ALTER: MERCHANTID
    /*
    public List<Titulos__c> getTitulosSemRetornoBraspag() {
        return [SELECT Id, Name, ProcessoSeletivo__r.Merchant__c, CodigoBrasPag__c, SequencialConsulta__c, NossoNumero__c, UrlBoleto__c,
                TransacaoBoleto__c, MeioPagamento__c, Status__c
                    FROM Titulos__c
                    WHERE NossoNumero__c = null 
                    AND TransacaoCartao__c = null
                    AND SequencialConsulta__c < 3                    
                    AND CreatedDate < :System.now().addMinutes(-40)
                	AND (Status__c = 'Em aberto' OR Status__c = 'Inexistente')
                	AND Valor__c != null
                    ORDER BY CreatedDate desc Limit 16];
    }
    */
    
    /*
        Busca titulos que não tiveram a primeira atualização do Braspag
    */
    public List<Titulos__c> getTitulosSemRetornoBraspag() {
        return [SELECT Id, Name, ProcessoSeletivo__r.Merchant_Id__c, CodigoBrasPag__c, SequencialConsulta__c, NossoNumero__c, UrlBoleto__c,
                TransacaoBoleto__c, MeioPagamento__c, Status__c, Cliente_Sap__c
                    FROM Titulos__c
                    WHERE NossoNumero__c = null 
                    AND TransacaoCartao__c = null
                    AND SequencialConsulta__c < 3                    
                    AND CreatedDate < :System.now().addMinutes(-40)
                	AND (Status__c = 'Em aberto' OR Status__c = 'Inexistente')
                	AND Valor__c != null
                    ORDER BY CreatedDate desc Limit 16];
    }

    /*
        Busca titulos para atualizar o status do Braspag
    */
    //ALTER: MERCHANTID
    /*
    public List<Titulos__c> getTitulosAtualizarStatusBraspag() {
        return [SELECT Id, Name, ProcessoSeletivo__r.Merchant__c, CodigoBrasPag__c, SequencialConsulta__c, NossoNumero__c, UrlBoleto__c,
                TransacaoBoleto__c, MeioPagamento__c, Status__c
                    FROM Titulos__c
                    WHERE CodigoBrasPag__c != null 
                    AND TransacaoBoleto__c != null
                    AND Status__c = 'Em aberto'
                    AND MeioPagamento__c != 'Boleto'
                    AND CodigoSAP__c != null
                    Limit 25];
    }
    */
    
    /*
        Busca titulos para atualizar o status do Braspag
    */
    public List<Titulos__c> getTitulosAtualizarStatusBraspag() {
        return [SELECT Id, Name, ProcessoSeletivo__r.Merchant_Id__c, CodigoBrasPag__c, SequencialConsulta__c, NossoNumero__c, UrlBoleto__c,
                TransacaoBoleto__c, MeioPagamento__c, Status__c, Cliente_Sap__c
                    FROM Titulos__c
                    WHERE CodigoBrasPag__c != null 
                    AND TransacaoBoleto__c != null
                    AND Status__c = 'Em aberto'
                    AND MeioPagamento__c != 'Boleto'
                    AND CodigoSAP__c != null
                    Limit 25];
    }
}