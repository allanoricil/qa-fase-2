global class AcademicoWebService {
    webService static String CreateAcademico(String pName, String pRA, String pCurso, String pSemestre, String pSituacaoMatricula,
    String pDataSituacao, String pTurno, String pIdProcAcademus, String pNumeroInscProcSel)
    {
        if (pRA == '' || pCurso == '' || pDataSituacao == '' ||
            pSemestre == '' || pSituacaoMatricula == '' || pTurno == '' ||
            pIdProcAcademus == '' || pNumeroInscProcSel == '')
        {            
            return 'ERROR - NÃO SÃO PERMITIDOS VALORES NULOS.';
        }
        else{
        
            String result;
            String idpart = pIdProcAcademus + pNumeroInscProcSel;
            try
            {
                List<Opportunity> lst = [Select o.AccountId, o.Id, o.Processo_seletivo__r.Institui_o_n__c, o.Processo_seletivo__r.Campus__c
                                     From Opportunity o where (o.Id_processo_Academus__c = :pIdProcAcademus and o.CodigoInscricao__c = :pNumeroInscProcSel) or o.ID_Participa_o__c = :idpart];
                       
                Academico__c academico = new Academico__c();
                academico.Aluno__c = lst.get(0).AccountId;
                academico.Oportunidade__c = lst.get(0).Id;
                academico.IES__c = lst.get(0).Processo_seletivo__r.Institui_o_n__c;
                academico.Campus__c = lst.get(0).Processo_seletivo__r.Campus__c;
                
                Academico__c a = new Academico__c(Name = pRA, RA__c = pRA, Curso__c = pCurso, DataSituacao__c = Date.valueOf(pDataSituacao),       
                Semestre__c = pSemestre, SituacaoMatricula__c = pSituacaoMatricula, Turno__c = pTurno, Aluno__c = academico.Aluno__c,
                Oportunidade__c = academico.Oportunidade__c, IES__c = academico.IES__c, Campus__c = academico.Campus__c);
            
                upsert a Academico__c.Fields.RA__c;
                result = 'SUCCESS - ' + a.Id;
            }
            catch (Exception e)
            {
                result = 'ERROR - ' + e.getMessage();
            }
            
            return result;
        }
    }
}