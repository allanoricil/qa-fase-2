/*
    @author Diego Moreira
    @class Classe de negocio para criação candidato no Academus
*/
public class SRM_CadastroCandidatoBO implements IProcessingQueue {
    /* 
        Map de execução dos eventos
    */
    private static map<String, String> mapPagamento;     
    static {
        mapPagamento = new map<String, String>();
        mapPagamento.put( 'Pré-Inscrito', '0' );
        mapPagamento.put( 'Inscrito', '1' );
    }
    
	/* 
        Processamento de fila
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, payload );     
    }

    /*
		Processa a fila de atualização de criação de candidato no academus
		@param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {        
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null ) {
    		HttpRequest req = new HttpRequest();
	        req.setEndpoint( WSSetup__c.getValues( 'Academus-NovoCandidato' ).Endpoint__c );
	        req.setMethod( 'POST' );
	        req.setHeader( 'content-type', 'application/json' );
	        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'Academus-NovoCandidato' ).API_Token__c );
	        req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
	        req.setBody( payload );
	 		
	        try {
	            Http h = new Http();
	            HttpResponse res = h.send( req );

	            if( res.getStatusCode() == 200 ){
                    system.debug('aqui temos outro debug '+res.getBody());
                    processJsonResult( res.getBody() );
	            	QueueBO.getInstance().updateQueue( queueId, '' );	        							               					          
	            } else
	                QueueBO.getInstance().updateQueue( queueId, 'CadastroCandidato / ' + res.getStatusCode() + ' / ' + res.getStatus() );
	        } catch ( CalloutException ex ) {
	            QueueBO.getInstance().updateQueue( queueId, 'CadastroCandidato / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
	        } catch ( Exception ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'CadastroCandidato / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
        } else {
        	QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }
    }

    /*
        Processa o json de retorno 
        @param jsonResult JSON de retorno do serviço
    */
    private static void processJsonResult( String jsonResult ) {
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( jsonResult );
        System.debug('>>> ' + jsonResult );
        system.debug('map do result '+ mapResult);
        Opportunity opportunityToUpdate         = new Opportunity();
        opportunityToUpdate.Id                  = ( String ) mapResult.get( 'idCadastro' );
        system.debug('ID da oportunidade a ser atualizada: '+opportunityToUpdate.Id);
        opportunityToUpdate.CodigoInscricao__c  = ( String ) mapResult.get( 'codCandidato' );
        system.debug('Cod Candidato! '+opportunityToUpdate.CodigoInscricao__c);
        ProcessorControl.inFutureContext = true;
        OpportunityDAO.getInstance().updateData( opportunityToUpdate );
    }

    /*
		Retorna o Json 
    */
    public Static String getJsonRequest( Opportunity opportunity ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        //gen.writeStringField( 'idformulario',   '1454' ); 
        gen.writeStringField( 'idInstituicao',  opportunity.Processo_seletivo__r.Id_Institui_o__c != null ? opportunity.Processo_seletivo__r.Id_Institui_o__c : '' ); 
        gen.writeStringField( 'idPeriodo',      opportunity.Processo_seletivo__r.IdPeriodoLetivo__c != null ? opportunity.Processo_seletivo__r.IdPeriodoLetivo__c : '' ); 
        gen.writeStringField( 'idProcesso',     opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c != null ? opportunity.Processo_seletivo__r.ID_Academus_ProcSel__c : '' ); 
        gen.writeStringField( 'idCadastro',     opportunity.Id ); 
        gen.writeStringField( 'idOrigem',       '1' ); 
        
        //if(!test.IsRunningTest()){       	
       
        
        gen.writeStringField( 'codColigada',    opportunity.Processo_seletivo__r.C_digo_da_Coligada__c );
        gen.writeStringField( 'pagamento',      mapPagamento.get( opportunity.StageName ) != null ? mapPagamento.get( opportunity.StageName ) : '0' ); 
        gen.writeStringField( 'dtCadastro',     String.valueOf( Date.valueOf( opportunity.CreatedDate ) ) ); 
        gen.writeStringField( 'nome',           opportunity.Account.Name ); 
        gen.writeStringField( 'sexo',           opportunity.Account.Sexo__c != null ? opportunity.Account.Sexo__c : '' );
        if(opportunity.Account.DataNascimento__c != null)
            gen.writeStringField( 'nascimento',     opportunity.Account.DataNascimento__c != null ? String.valueOf( opportunity.Account.DataNascimento__c ) : '' );
        else 
            gen.writeStringField( 'nascimento',     opportunity.Data_Nascimento__c != null ? String.valueOf( opportunity.Data_Nascimento__c ) : '' );
        gen.writeStringField( 'cidade',         opportunity.Account.Cidade__c != null ? opportunity.Account.Cidade__c : '' ); 
        gen.writeStringField( 'uf',             opportunity.Account.Estado__c != null ? opportunity.Account.Estado__c : '' );
        //gen.writeStringField( 'uf',             opportunity.Account.Estado__c != null ? opportunity.Estado_Form__c : '' ); 
        gen.writeStringField( 'nacionalidade',  opportunity.Account.Nacionalidade__c != null ? opportunity.Account.Nacionalidade__c : '' ); 
        gen.writeStringField( 'nomePai',        opportunity.Account.Nome_do_Pai__c != null ? opportunity.Account.Nome_do_Pai__c : '' ); 
        gen.writeStringField( 'nomeMae',        opportunity.Account.Nome_da_Mae__c != null ? opportunity.Account.Nome_da_Mae__c : '' ); 
        gen.writeStringField( 'cpfCandidato',   opportunity.Account.CPF_2__c.replace( '.', '').replace( '-', '' )  ); 
        gen.writeStringField( 'rgCandidato',    opportunity.Account.RG__c != null ? opportunity.Account.RG__c : '' ); 
        gen.writeStringField( 'endereco',       opportunity.Account.Rua__c != null ? opportunity.Account.Rua__c : '' ); 
        gen.writeStringField( 'numero',         opportunity.Account.N_mero__c != null ? opportunity.Account.N_mero__c : '' ); 
        gen.writeStringField( 'bairro',         opportunity.Account.Bairro__c != null ? opportunity.Account.Bairro__c : '' ); 
        gen.writeStringField( 'cep',            opportunity.Account.CEP__c != null ? opportunity.Account.CEP__c : '' ); 
        gen.writeStringField( 'telefone',       opportunity.Account.Phone != null ? opportunity.Account.Phone : '' ); 
        gen.writeStringField( 'complemento',    opportunity.Account.Complemento__c != null ? opportunity.Account.Complemento__c : '' ); 
        gen.writeStringField( 'email',          opportunity.Account.PersonEmail != null ? opportunity.Account.PersonEmail : '' ); 
        gen.writeStringField( 'cpfMae',         opportunity.Account.CPFdoResponsavel__c != null ? opportunity.Account.CPFdoResponsavel__c : '' ); 
        gen.writeStringField( 'oferta',         opportunity.X1_OpcaoCurso__r.ID_Academus_Oferta__c != null ? opportunity.X1_OpcaoCurso__r.ID_Academus_Oferta__c : '' );
        gen.writeStringField( 'oferta2',        opportunity.X2_Op_o_de_curso__r.ID_Academus_Oferta__c != null ? opportunity.X2_Op_o_de_curso__r.ID_Academus_Oferta__c : '0' );


        //gen.writeStartObject();
        ////gen.writeStringField( 'idformulario',   '1454' ); 
        //gen.writeStringField( 'idInstituicao',  '26'); 
        //gen.writeStringField( 'idPeriodo',      '37'); 
        //gen.writeStringField( 'idProcesso',     '3205' ); 
        //gen.writeStringField( 'idCadastro',     opportunity.Id ); 
        //gen.writeStringField( 'idOrigem',       '1' ); 
        
        //if(!test.IsRunningTest()){          
       
        
        //gen.writeStringField( 'codColigada',    '16' );
        //gen.writeStringField( 'pagamento',      '1'); 
        //gen.writeStringField( 'dtCadastro',     '2017-06-23'); 
        //gen.writeStringField( 'nome',           'André Batista' ); 
        //gen.writeStringField( 'sexo',           'Masculino'); 
        //gen.writeStringField( 'nascimento',     '1994-04-30'); 
        //gen.writeStringField( 'cidade',         'Imperatriz'); 
        //gen.writeStringField( 'uf',             'MA'); 
        //gen.writeStringField( 'nacionalidade',  'Brasileira'); 
        //gen.writeStringField( 'nomePai',        ''); 
        //gen.writeStringField( 'nomeMae',        ''); 
        //gen.writeStringField( 'cpfCandidato',   opportunity.Account.CPF_2__c.replace( '.', '').replace( '-', '' )  ); 
        //gen.writeStringField( 'rgCandidato',    ''); 
        //gen.writeStringField( 'endereco',       'Rua Aimorés'); 
        //gen.writeStringField( 'numero',         '8'); 
        //gen.writeStringField( 'bairro',         'Parque das Estrelas'); 
        //gen.writeStringField( 'cep',            '65911-321'); 
        //gen.writeStringField( 'telefone',       ''); 
        //gen.writeStringField( 'complemento',    ''); 
        //gen.writeStringField( 'email',          'andre.ferreira2@email.com'); 
        //gen.writeStringField( 'cpfMae',         ''); 
        //gen.writeStringField( 'oferta',         '52459');
        //gen.writeStringField( 'oferta2',        '52461');
         //}else{
         	
         //}
        gen.writeEndObject(); 
		
        return gen.getAsString(); 
    }
}