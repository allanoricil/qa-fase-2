public class knowledgeController{
	public List<SelectOption> campusOptions  	{ get; set; }
	public List<SelectOption> iesOptions 	  	{ get; set; }
	public List<SelectOption> cursoOptions    	{ get; set; }
	public List<SelectOption> cursoPosOptions   { get; set; }
	public List<SelectOption> metoEntradaOptions{ get; set; }
	public List<SelectOption> produtoOptions	{ get; set; }
	public List<Dados> 	      dados				{ get; set; }
	public List<DadosFixa> 	  dados1			{ get; set; }
	public String 			  ies 				{ get; set; }
	public string 			  curso 	 		{ get; set; }
	public String 			  categoria			{ get; set; }
	public String 			  campus			{ get; set; }
	public String 			  tipo 				{ get; set; }
	public String 			  metoEntrada 		{ get; set; }
	public String 			  produto 			{ get; set; }
	
	public knowledgeController(){

		tipo = getTipoProcesso();
		if(tipo != 'Vestibular') {
			getListIes();
		}
		if(tipo == 'Vestibular') {
			getListProduto();
			}	
	 	
	 	}

	public List<Knowledge__kav> get_ies(){
		String query;
		if(tipo == '') {	
			query = 'SELECT IES_del__c From Knowledge__kav where categoria__c = \'pos\'';
		}
		if(tipo == 'Graduação'){
			query = 'SELECT IES_del__c From Knowledge__kav where Categoria__c = \'Graduação\' or Categoria__c = \'Graduação Tecnológica\''+'ORDER BY IES_del__c ASC';
		}
		if(tipo == 'Pós Graduação'){
			query  = 'SELECT IES_del__c From Knowledge__kav where Categoria__c = \'Especialização\' OR Categoria__c = \'MBA\' OR Categoria__c = \'GLOBAL\' or Categoria__c = \'GLOBAL MBA\' or Categoria__c = \'LL.M.\''+'ORDER BY IES_del__c ASC';
		}
		if(tipo == 'Vestibular'){
			query = 'SELECT IES_del__c From Knowledge__kav where Categoria__c = \'Vestibular\''+'and Produto__c ='+ '\''+ produto +'\''+'ORDER BY IES_del__c ASC' ;
		}	
				

		List<Knowledge__kav> know = Database.query(query);
		
		return know;
		
	}
	
	public List<Knowledge__kav> get_campus(String instituicao){

		return [SELECT id,Campus__c From Knowledge__kav where IES_del__c =: instituicao and (Categoria__c = 'Graduação' or Categoria__c = 'Graduação Tecnológica') ORDER BY Campus__c ASC ];

	}

	public String getTipoProcesso(){
		String retorno = '';
        String processo = ApexPages.currentPage().getParameters().get('v');
        	
	        
	        if(processo == 'g'){
	            retorno = 'Graduação';
	        }
	        if(processo == 'e'){
	        	retorno = 'Pós Graduação';
	        }
	        if(processo =='p'){
	            retorno = 'Vestibular';
	        }
	        return retorno;
	}



	public List<Knowledge__kav> getcurso(){
		String query;

	
		if(tipo == 'Graduação'){

			query = 'SELECT Nome_do_Curso__c From Knowledge__kav where IES_del__c = '+ '\'' +  ies + '\'' +' and Campus__c = '+ '\'' +  campus + '\'' +' and (Categoria__c = \'Graduação\' or Categoria__c = \'Graduação Tecnológica\')'+'ORDER BY Nome_do_Curso__c ASC';

		}
		if(tipo == 'Pós Graduação'){

			query = 'SELECT Nome_do_Curso__c From Knowledge__kav WHERE IES_del__c = '+ '\'' +  ies + '\'' +'  and (Categoria__c =\'Especialização\' OR Categoria__c=\'MBA\' OR Categoria__c=\'GLOBAL\' or Categoria__c =\'GLOBAL MBA\' or Categoria__c=\'LL.M.\')'+'ORDER BY Nome_do_Curso__c ASC';
			
		}
		if(tipo == 'Vestibular'){

					query = 'SELECT Nome_do_Curso__c From Knowledge__kav where IES_del__c = '+ '\'' +  ies + '\''+'and Produto__c ='+ '\''+produto+'\'' +' and Categoria__c = \'Vestibular\''+'ORDER BY Nome_do_Curso__c ASC';

				}

		List<Knowledge__kav> know = Database.query(query);
		
		return know;
		
	}
	//public List<Knowledge__kav> get_metoEntrada(){
	//		String query;
	//		if(tipo == 'Graduação'){
	//		query = 'SELECT FORMA_DE_INGRESSO__c FROM Knowledge__kav WHERE (Categoria__c = \'Graduação\' or Categoria__c = \'Graduação Tecnológica\')' + 'and IES_del__c ='+ '\'' +  ies + '\'' +'and Campus__c ='+ '\'' +  campus + '\'' + ' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+'ORDER BY FORMA_DE_INGRESSO__c ASC';
	//		}

	//		if(tipo == 'Pós Graduação'){
	//			query = 'SELECT FORMA_DE_INGRESSO__c FROM Knowledge__kav WHERE (Categoria__c =\'Especialização\' OR Categoria__c=\'MBA\' OR Categoria__c=\'GLOBAL\' or Categoria__c =\'GLOBAL MBA\' or Categoria__c=\'LL.M.\') and IES_del__c ='+ '\'' +  ies + '\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+'ORDER BY FORMA_DE_INGRESSO__c ASC';
	//		}
	//		if(tipo == 'Vestibular'){
	//			query = 'SELECT FORMA_DE_INGRESSO__c FROM Knowledge__kav WHERE Categoria__c =' + '\'' +  tipo + '\'' + 'and IES_del__c ='+ '\'' +  ies + '\'' + 'and Produto__c ='+ '\''+ produto +'\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+'ORDER BY FORMA_DE_INGRESSO__c ASC';
	//		}

	//		List<Knowledge__kav> know = Database.query(query);

	//		return know;

	//}

	public List<Knowledge__kav> get_produto(){
			String query;
			/*if(tipo == 'Graduação'){
			query = 'SELECT FORMA_DE_INGRESSO__c FROM Knowledge__kav WHERE (Categoria__c = \'Graduação\' or Categoria__c = \'Graduação Tecnológica\')' + 'and IES_del__c ='+ '\'' +  ies + '\'' +'and Campus__c ='+ '\'' +  campus + '\'' + ' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+'ORDER BY FORMA_DE_INGRESSO__c ASC';
			}

			if(tipo == 'Pós Graduação'){
				query = 'SELECT FORMA_DE_INGRESSO__c FROM Knowledge__kav WHERE (Categoria__c =\'Especialização\' OR Categoria__c=\'MBA\' OR Categoria__c=\'GLOBAL\' or Categoria__c =\'GLOBAL MBA\' or Categoria__c=\'LL.M.\') and IES_del__c ='+ '\'' +  ies + '\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+'ORDER BY FORMA_DE_INGRESSO__c ASC';
			}*/
			if(tipo == 'Vestibular'){
				query = 'SELECT Produto__c FROM Knowledge__kav ORDER BY Produto__c ASC';
	}

			List<Knowledge__kav> know = Database.query(query);

			return know;

			}
		public void getDadosFixa(){
			String query;
			if(tipo == 'Vestibular'){
				query='SELECT Atualizado_em__c,Condi_es_de_Pagamento__c,Docs_para_reserva_pr_xima_entrada_IBMEC__c,Documentos_para_a_matr_cula__c,Documentos_para_ENEM_colegial_conclu_do__c,'+
				'Documentos_para_ENEM_cursando_colegial__c,Documentos_para_Portador_de_Diploma__c,Documentos_para_Transfer_ncia_Externa__c,Hor_rio_Aula_Turno_Integral__c,'+
				'Hor_rio_Aula_Turno_Manh__c,Hor_rio_Aula_Turno_Noite__c,Hor_rio_Aula_Turno_Tarde__c,Hor_rio_de_Atendimento_na_IES__c,Horario_de_Atendimento_no_Contact_Center__c,Inicio_das_Aulas_2018_1__c,'+
				'Inicio_das_Aulas_2018_2__c,Planilha_G_Docs_Pegue_Informa_es__c,Site__c,Tipos_de_Financiamento__c,WhatsApp__c FROM Base_fixa__c WHERE IES__c ='+ '\'' +  ies + '\'' + 'and Produto__c ='+'\''+produto+'\'';
			}	
			List<Base_fixa__c> bf = Database.query(query);
			dados1 = new List<DadosFixa>();
			for(Base_fixa__c base : bf){
			this.dados1.add(new DadosFixa(base));
		}
		

		}


	public void getDados(){
		String query;

		if(tipo == 'Graduação'){

			query = 'SELECT IES_del__c,Campus__c,Quantidade_de_turmas__c,FinanciamentoGRAD__c,Nota_MEC__c,Condi_es_de_Pag__c,Nome_do_Curso__c,Porque_podemos_afirmar_que_o_curso_em_qu__c,Nota_Enade__c,Premio__c,Projetos__c,'+
			'Diferenciais_do_curso__c,Cases_de_Sucesso__c,Cases_n_o_relacionados__c,Bolsa_M_rito_Ibmec__c,Bolsa_v_lida_para_2018_1__c,Bolsa_v_lida_para_2018_2__c,'+
			'Data_da_prova__c,Data_da_entrevista__c,Pre_requisito__c,Dias_de_aula__c,Periodicidade_das_aulas__c,In_cio_das_aulas__c,Desconto_Ex_aluno_at_30_agendar_admis__c,Desconto_ex_aluno_35__c,Grupo_de_alunos_admiss_es_agendadas__c,Parceria_empresas_Corpore_at_30__c,Parceria_empresas_Corpore_at_10__c,Parentesco_30_parentes_de_1_grau__c,Parceria_Corpore_at_20__c,Parentesco_15_parentes_de_1_grau__c,Parceria_Corpore_15__c,Antecipa_o_de_matr_10_25_mensalidade__c,Principal_questionamento__c,Per_odo_m_dio_de_in_cio_de_est_gio__c,Bolsa_Corpore__c,Regras_para_Concess_o__c,'+
			'Valor_m_dio_maximo_de_bolsa__c,Valor_com_desconto__c,Dura_o_do_CursoGRAD__c,HOR_RIO_DE_ATENDIMENTO_NO_CONTACT_CENTER__c,FORMA_DE_INGRESSO__c,'+
			'DATA_DE_INSCRI_O__c,Processo_decis_rio__c,Principal_questionamento_Pos__c,Taxa_de_inscri_o__c,Semestre_requisitado_para_uso_do_lab__c,Prova__c,Local_da_prova__c,Resultado__c,Per_odo_de_Matr_cula__c,Pr_Requisito_para_Matr_cula__c,Bolsa_Isen_o_de_Matr_cula_Transfer_ncia__c,'+
			'PRINCIPAIS_EVENTOS__c,O_que_o_evento__c,Quando_ser__c,Se_estende_para_P_s__c,M_todo_de_pagamento__c,Processo_de_inscri_o__c,Organizador_do_Evento__c,IN_CIO_DAS_AULAS_2018_1__c,Como_contra_argumentamos_POS__c,IN_CIO_DAS_AULAS_2018_2__c,Documentos_matr_cula__c,'+
			'Pre_o_Mensal__c,Regras_para_Concess_o1__c,Regras_para_Concess_o2__c,Regras_para_Concess_o3__c,Regras_para_Concess_o4__c,Regras_para_Concess_o5__c,Regras_para_Concess_o6__c,Regras_para_Concess_o7__c,Regras_para_Concess_o8__c,Regras_para_Concess_o9__c,'+
			'Desconto__c,Dura_o_do_Curso__c,Turno__c,Financiamento__c,Cumulatividade_de_Bolsa__c,Quantidade_de_turmas_ofertadas__c,'+
			'Documentos_para_ENEM_ensino_m_dio_em_cu__c,Bolsa_Transferido_e_Portador__c,Valor_do_curso__c,Carga_hor_riaVest__c,'+
			'Bolsa_v_lida_para_2018_1_1__c,Bolsa_v_lida_para_2018_1_2__c,Bolsa_v_lida_para_2018_1_3__c,Bolsa_v_lida_para_2018_1_4__c,Bolsa_v_lida_para_2018_1_5__c,'+
			'Bolsa_v_lida_para_2018_2_1__c,Bolsa_v_lida_para_2018_2_2__c,Bolsa_v_lida_para_2018_2_3__c,Bolsa_v_lida_para_2018_2_4__c,Bolsa_v_lida_para_2018_2_5__c,'+
			'Regras_para_Concess_o10__c,Regras_para_Concess_o11__c,Regras_para_Concess_o12__c,Regras_para_Concess_o13__c,Regras_para_Concess_o14__c,Regras_para_Concess_o15__c,'+
			'Documentos_para_ENEM__c,Categoria__c,Processo_Seletivo__c,Limite_para_matriculas__c,Corpo_acad_mico__c,Documentos_para_reserva_de_vaga_pr_xima__c,Carga_hor_ria__c,Grade_do_curso__c,Labs__c,'+
			'Pol_tica_de_Faltas__c,Conclus_o_de_Curso__c,O_porque_de_ser_DeVry__c,Condi_es_de_PagamentoGRAD__c,Laborat_rios_do_curso__c,Bolsa_Antecipa_o_de_Vagas__c,Bolsa_Grupo__c,'+
			'Como_contra_argumentamos__c,Formas_de_Pagamento__c,Diferenciais__c,Diferenciais_do_cursoPos__c,Observa_o_1__c,TurnoGRAD__c,Observa_o_2__c,Observa_o_3__c,Observa_o_4__c,Observa_o_5__c From Knowledge__kav'+
										 ' where (Categoria__c = \'Graduação\' or Categoria__c = \'Graduação Tecnológica\')' + 'and IES_del__c ='+ '\'' +  ies + '\'' +'and Campus__c ='+ '\'' +  campus + '\'' + ' and Nome_do_Curso__c = ' + '\'' +  curso + '\'' + ' limit 1';

		}

		if(tipo == 'Pós Graduação'){

			query = 'SELECT IES_del__c,Processo_decis_rio__c,Campus__c,Como_contra_argumentamos_POS__c,FinanciamentoGRAD__c,Nota_MEC__c,Condi_es_de_Pag__c,Condi_es_de_PagamentoGRAD__c,Porque_podemos_afirmar_que_o_curso_em_qu__c,Nome_do_Curso__c,Nota_Enade__c,Premio__c,Projetos__c,'+
			'Diferenciais_do_curso__c,Cases_de_Sucesso__c,Principal_questionamento_Pos__c,Cases_n_o_relacionados__c,Bolsa_M_rito_Ibmec__c,Bolsa_v_lida_para_2018_1__c,Bolsa_v_lida_para_2018_2__c,'+
			'Principal_questionamento__c,Per_odo_m_dio_de_in_cio_de_est_gio__c,Semestre_requisitado_para_uso_do_lab__c,Dura_o_do_CursoGRAD__c,Documentos_para_reserva_de_vaga_pr_xima__c,Bolsa_Corpore__c,Regras_para_Concess_o__c,'+
			'Data_da_prova__c,Quantidade_de_turmas__c,Data_da_entrevista__c,Pre_requisito__c,Dias_de_aula__c,Periodicidade_das_aulas__c,In_cio_das_aulas__c,Desconto_Ex_aluno_at_30_agendar_admis__c,Desconto_ex_aluno_35__c,Grupo_de_alunos_admiss_es_agendadas__c,Parceria_empresas_Corpore_at_30__c,Parceria_empresas_Corpore_at_10__c,Parentesco_30_parentes_de_1_grau__c,Parceria_Corpore_at_20__c,Parentesco_15_parentes_de_1_grau__c,Parceria_Corpore_15__c,Antecipa_o_de_matr_10_25_mensalidade__c,Documentos_matr_cula__c,Diferenciais__c,Valor_m_dio_maximo_de_bolsa__c,HOR_RIO_DE_ATENDIMENTO_NO_CONTACT_CENTER__c,FORMA_DE_INGRESSO__c,'+
			'DATA_DE_INSCRI_O__c,Valor_com_desconto__c,PRINCIPAIS_EVENTOS__c,O_que_o_evento__c,Quando_ser__c,Se_estende_para_P_s__c,M_todo_de_pagamento__c,Processo_de_inscri_o__c,Organizador_do_Evento__c,Taxa_de_inscri_o__c,Prova__c,Local_da_prova__c,Resultado__c,Per_odo_de_Matr_cula__c,Pr_Requisito_para_Matr_cula__c,Bolsa_Isen_o_de_Matr_cula_Transfer_ncia__c,'+
			'Diferenciais_do_cursoPos__c,Pre_o_Mensal__c,Bolsa_Transferido_e_Portador__c,Valor_do_curso__c,Carga_hor_riaVest__c,'+
			'Desconto__c,Dura_o_do_Curso__c,Turno__c,Laborat_rios_do_curso__c,Financiamento__c,Quantidade_de_turmas_ofertadas__c,'+
			'Bolsa_v_lida_para_2018_1_1__c,Bolsa_v_lida_para_2018_1_2__c,Bolsa_v_lida_para_2018_1_3__c,Bolsa_v_lida_para_2018_1_4__c,Bolsa_v_lida_para_2018_1_5__c,'+
			'Bolsa_v_lida_para_2018_2_1__c,Bolsa_v_lida_para_2018_2_2__c,Bolsa_v_lida_para_2018_2_3__c,Bolsa_v_lida_para_2018_2_4__c,Bolsa_v_lida_para_2018_2_5__c,'+
			'Regras_para_Concess_o10__c,Regras_para_Concess_o11__c,Regras_para_Concess_o12__c,Regras_para_Concess_o13__c,Regras_para_Concess_o14__c,Regras_para_Concess_o15__c,'+
			'Documentos_para_ENEM_ensino_m_dio_em_cu__c,Regras_para_Concess_o1__c,Regras_para_Concess_o2__c,Regras_para_Concess_o3__c,Regras_para_Concess_o4__c,Regras_para_Concess_o5__c,Regras_para_Concess_o6__c,Regras_para_Concess_o7__c,Regras_para_Concess_o8__c,Regras_para_Concess_o9__c,'+
			'Documentos_para_ENEM__c,Categoria__c,Processo_Seletivo__c,Corpo_acad_mico__c,Carga_hor_ria__c,Grade_do_curso__c,Labs__c,Cumulatividade_de_Bolsa__c,'+
			'Pol_tica_de_Faltas__c,Conclus_o_de_Curso__c,Limite_para_matriculas__c,O_porque_de_ser_DeVry__c,Bolsa_Antecipa_o_de_Vagas__c,Bolsa_Grupo__c,'+
			'Como_contra_argumentamos__c,Formas_de_Pagamento__c,Observa_o_1__c,Observa_o_2__c,TurnoGRAD__c,Observa_o_3__c,Observa_o_4__c,Observa_o_5__c From Knowledge__kav'+
			' where (Categoria__c =\'Especialização\' OR Categoria__c=\'MBA\' OR Categoria__c=\'GLOBAL\' or Categoria__c =\'GLOBAL MBA\' or Categoria__c=\'LL.M.\' or Categoria__c=\'Pós Graduação\') and IES_del__c ='+ '\'' +  ies + '\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\''+' limit 1';
		}

		if(tipo == 'Vestibular'){
			if(produto == 'Graduação'){
			query = 'SELECT IES_del__c,Processo_decis_rio__c,Campus__c,Semestre_requisitado_para_uso_do_lab__c,Como_contra_argumentamos_POS__c,FinanciamentoGRAD__c,Nota_MEC__c,Condi_es_de_Pag__c,Porque_podemos_afirmar_que_o_curso_em_qu__c,Nome_do_Curso__c,Nota_Enade__c,Premio__c,Projetos__c,'+
			'Diferenciais_do_curso__c,Dura_o_do_CursoGRAD__c,Limite_para_matriculas__c,Principal_questionamento_Pos__c,Cases_de_Sucesso__c,Cases_n_o_relacionados__c,Bolsa_M_rito_Ibmec__c,TurnoGRAD__c,Bolsa_v_lida_para_2018_1__c,Bolsa_v_lida_para_2018_2__c,'+
			'Principal_questionamento__c,Per_odo_m_dio_de_in_cio_de_est_gio__c,Documentos_para_reserva_de_vaga_pr_xima__c,Bolsa_Corpore__c,Regras_para_Concess_o__c,'+
			'Valor_m_dio_maximo_de_bolsa__c,HOR_RIO_DE_ATENDIMENTO_NO_CONTACT_CENTER__c,FORMA_DE_INGRESSO__c,Carga_hor_riaVest__c,'+
			'Data_da_prova__c,Quantidade_de_turmas__c,Data_da_entrevista__c,Pre_requisito__c,Dias_de_aula__c,Periodicidade_das_aulas__c,In_cio_das_aulas__c,Desconto_Ex_aluno_at_30_agendar_admis__c,Desconto_ex_aluno_35__c,Grupo_de_alunos_admiss_es_agendadas__c,Parceria_empresas_Corpore_at_30__c,Parceria_empresas_Corpore_at_10__c,Parentesco_30_parentes_de_1_grau__c,Parceria_Corpore_at_20__c,Parentesco_15_parentes_de_1_grau__c,Parceria_Corpore_15__c,Antecipa_o_de_matr_10_25_mensalidade__c,Documentos_matr_cula__c,DATA_DE_INSCRI_O__c,Taxa_de_inscri_o__c,Prova__c,Local_da_prova__c,Resultado__c,Per_odo_de_Matr_cula__c,Pr_Requisito_para_Matr_cula__c,Bolsa_Isen_o_de_Matr_cula_Transfer_ncia__c,'+
			'Condi_es_de_PagamentoGRAD__c,Valor_com_desconto__c,PRINCIPAIS_EVENTOS__c,O_que_o_evento__c,Quando_ser__c,Se_estende_para_P_s__c,M_todo_de_pagamento__c,Processo_de_inscri_o__c,Organizador_do_Evento__c,'+
			'Diferenciais_do_cursoPos__c,Laborat_rios_do_curso__c,Pre_o_Mensal__c,Bolsa_Transferido_e_Portador__c,'+
			'Desconto__c,Dura_o_do_Curso__c,Turno__c,Financiamento__c,Valor_do_curso__c,Quantidade_de_turmas_ofertadas__c,'+
			'Bolsa_v_lida_para_2018_1_1__c,Bolsa_v_lida_para_2018_1_2__c,Bolsa_v_lida_para_2018_1_3__c,Bolsa_v_lida_para_2018_1_4__c,Bolsa_v_lida_para_2018_1_5__c,'+
			'Bolsa_v_lida_para_2018_2_1__c,Bolsa_v_lida_para_2018_2_2__c,Bolsa_v_lida_para_2018_2_3__c,Bolsa_v_lida_para_2018_2_4__c,Bolsa_v_lida_para_2018_2_5__c,'+
			'Regras_para_Concess_o10__c,Regras_para_Concess_o11__c,Regras_para_Concess_o12__c,Regras_para_Concess_o13__c,Regras_para_Concess_o14__c,Regras_para_Concess_o15__c,'+
			'Documentos_para_ENEM_ensino_m_dio_em_cu__c,Regras_para_Concess_o1__c,Regras_para_Concess_o2__c,Regras_para_Concess_o3__c,Regras_para_Concess_o4__c,Regras_para_Concess_o5__c,Regras_para_Concess_o6__c,Regras_para_Concess_o7__c,Regras_para_Concess_o8__c,Regras_para_Concess_o9__c,'+
			'Documentos_para_ENEM__c,Categoria__c,Processo_Seletivo__c,Corpo_acad_mico__c,Carga_hor_ria__c,Grade_do_curso__c,Labs__c,Cumulatividade_de_Bolsa__c,'+
			'Pol_tica_de_Faltas__c,Conclus_o_de_Curso__c,O_porque_de_ser_DeVry__c,Bolsa_Antecipa_o_de_Vagas__c,Bolsa_Grupo__c,'+
			'Como_contra_argumentamos__c,Diferenciais__c,Formas_de_Pagamento__c,Observa_o_1__c,Observa_o_2__c,Observa_o_3__c,Observa_o_4__c,Observa_o_5__c From Knowledge__kav'+
			' where Categoria__c =' + '\'' +  tipo + '\'' + 'and IES_del__c ='+ '\'' +  ies + '\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\'' + 'and Produto__c ='+ '\''+ produto +'\'' + ' limit 1';
			getDadosFixa();
		}else{
			query = 'SELECT IES_del__c,Processo_decis_rio__c,Campus__c,Semestre_requisitado_para_uso_do_lab__c,Como_contra_argumentamos_POS__c,FinanciamentoGRAD__c,Nota_MEC__c,Condi_es_de_Pag__c,Porque_podemos_afirmar_que_o_curso_em_qu__c,Nome_do_Curso__c,Nota_Enade__c,Premio__c,Projetos__c,'+
			'Diferenciais_do_curso__c,Dura_o_do_CursoGRAD__c,Limite_para_matriculas__c,Principal_questionamento_Pos__c,Cases_de_Sucesso__c,Cases_n_o_relacionados__c,Bolsa_M_rito_Ibmec__c,TurnoGRAD__c,Bolsa_v_lida_para_2018_1__c,Bolsa_v_lida_para_2018_2__c,'+
			'Principal_questionamento__c,Per_odo_m_dio_de_in_cio_de_est_gio__c,Documentos_para_reserva_de_vaga_pr_xima__c,Bolsa_Corpore__c,Regras_para_Concess_o__c,'+
			'Valor_m_dio_maximo_de_bolsa__c,HOR_RIO_DE_ATENDIMENTO_NO_CONTACT_CENTER__c,FORMA_DE_INGRESSO__c,Carga_hor_riaVest__c,'+
			'Data_da_prova__c,Quantidade_de_turmas__c,Data_da_entrevista__c,Pre_requisito__c,Dias_de_aula__c,Periodicidade_das_aulas__c,In_cio_das_aulas__c,Desconto_Ex_aluno_at_30_agendar_admis__c,Desconto_ex_aluno_35__c,Grupo_de_alunos_admiss_es_agendadas__c,Parceria_empresas_Corpore_at_30__c,Parceria_empresas_Corpore_at_10__c,Parentesco_30_parentes_de_1_grau__c,Parceria_Corpore_at_20__c,Parentesco_15_parentes_de_1_grau__c,Parceria_Corpore_15__c,Antecipa_o_de_matr_10_25_mensalidade__c,Documentos_matr_cula__c,DATA_DE_INSCRI_O__c,Taxa_de_inscri_o__c,Prova__c,Local_da_prova__c,Resultado__c,Per_odo_de_Matr_cula__c,Pr_Requisito_para_Matr_cula__c,Bolsa_Isen_o_de_Matr_cula_Transfer_ncia__c,'+
			'Condi_es_de_PagamentoGRAD__c,PRINCIPAIS_EVENTOS__c,O_que_o_evento__c,Quando_ser__c,Se_estende_para_P_s__c,M_todo_de_pagamento__c,Processo_de_inscri_o__c,Organizador_do_Evento__c,'+
			'Diferenciais_do_cursoPos__c,Laborat_rios_do_curso__c,Pre_o_Mensal__c,Bolsa_Transferido_e_Portador__c,'+
			'Desconto__c,Valor_com_desconto__c,Dura_o_do_Curso__c,Turno__c,Financiamento__c,Valor_do_curso__c,Quantidade_de_turmas_ofertadas__c,'+
			'Bolsa_v_lida_para_2018_1_1__c,Bolsa_v_lida_para_2018_1_2__c,Bolsa_v_lida_para_2018_1_3__c,Bolsa_v_lida_para_2018_1_4__c,Bolsa_v_lida_para_2018_1_5__c,'+
			'Bolsa_v_lida_para_2018_2_1__c,Bolsa_v_lida_para_2018_2_2__c,Bolsa_v_lida_para_2018_2_3__c,Bolsa_v_lida_para_2018_2_4__c,Bolsa_v_lida_para_2018_2_5__c,'+
			'Regras_para_Concess_o10__c,Regras_para_Concess_o11__c,Regras_para_Concess_o12__c,Regras_para_Concess_o13__c,Regras_para_Concess_o14__c,Regras_para_Concess_o15__c,'+
			'Documentos_para_ENEM_ensino_m_dio_em_cu__c,Regras_para_Concess_o1__c,Regras_para_Concess_o2__c,Regras_para_Concess_o3__c,Regras_para_Concess_o4__c,Regras_para_Concess_o5__c,Regras_para_Concess_o6__c,Regras_para_Concess_o7__c,Regras_para_Concess_o8__c,Regras_para_Concess_o9__c,'+
			'Documentos_para_ENEM__c,Categoria__c,Processo_Seletivo__c,Corpo_acad_mico__c,Carga_hor_ria__c,Grade_do_curso__c,Labs__c,Cumulatividade_de_Bolsa__c,'+
			'Pol_tica_de_Faltas__c,Conclus_o_de_Curso__c,O_porque_de_ser_DeVry__c,Bolsa_Antecipa_o_de_Vagas__c,Bolsa_Grupo__c,'+
			'Como_contra_argumentamos__c,Diferenciais__c,Formas_de_Pagamento__c,Observa_o_1__c,Observa_o_2__c,Observa_o_3__c,Observa_o_4__c,Observa_o_5__c From Knowledge__kav'+
			' where Categoria__c =' + '\'' +  tipo + '\'' + 'and IES_del__c ='+ '\'' +  ies + '\'' +' and Nome_do_Curso__c = ' + '\'' +  curso + '\'' + 'and Produto__c ='+ '\''+ produto +'\''+' limit 1';	
			getDadosFixa();

					
		}
	}
		
		List<Knowledge__kav> know = Database.query(query);
		dados = new List<Dados>();
		for(Knowledge__kav base : know){
			this.dados.add(new Dados(base));
		}

	}

	public void getListIes(){
		iesOptions = new List<SelectOption>();
            Set<String> filtro = new Set<String>();
		for(Knowledge__kav selecIes : get_ies()){
            if(filtro.add(selecIes.IES_del__c))
			iesOptions.add(new SelectOption(selecIes.IES_del__c,selecIes.IES_del__c));
		}
	}
	

	public void getListcampus(){
		campusOptions = new List<SelectOption>();
		String iesReturn = ies;
		  Set<string> filtro = new set<string>();
        for(Knowledge__kav selecCampus : get_campus(iesReturn)){
              if(filtro.add(selecCampus.Campus__c))
       		campusOptions.add(new SelectOption(selecCampus.Campus__c,selecCampus.Campus__c));
        }
    }
     
  	  public void getListCurso(){
  	  	cursoOptions  = new List<SelectOption>();
  	  	Set<String> filtro = new Set<String>();
        for(Knowledge__kav selecCurso : getcurso() ){
        	if(filtro.add(selecCurso.Nome_do_Curso__c))
  			cursoOptions.add(new SelectOption(selecCurso.Nome_do_Curso__c,selecCurso.Nome_do_Curso__c));
    	}
	}
	//public void getListmetoEntrada(){
 // 	  	metoEntradaOptions  = new List<SelectOption>();
 // 	  	Set<String> filtro = new Set<String>();
 // 	  	List<Knowledge__kav> x = get_metoEntrada();
 // 	  	if(x[0].FORMA_DE_INGRESSO__c != null){
 // 	  	for(Knowledge__kav selecmetoEntrada : x)
 // 	  		if(filtro.add(selecmetoEntrada.FORMA_DE_INGRESSO__c))
 // 	  		if(selecmetoEntrada.FORMA_DE_INGRESSO__c != null)
 // 				metoEntradaOptions.add(new SelectOption(selecmetoEntrada.FORMA_DE_INGRESSO__c,selecmetoEntrada.FORMA_DE_INGRESSO__c));
 //   	}else{
	//  		metoEntradaOptions.add(new SelectOption('Para este Produto não há método de entrada.','Para este Produto não há método de entrada.'));
	//   	}
	//}

	public void getListProduto(){
  	  	produtoOptions  = new List<SelectOption>();
  	  	Set<String> filtro = new Set<String>();
        for(Knowledge__kav selecProduto : get_produto() ){
        	if(filtro.add(selecProduto.Produto__c)){
        		if(selecProduto.Produto__c != null)
  				produtoOptions.add(new SelectOption(selecProduto.Produto__c,selecProduto.Produto__c));
  			}
    	}
	}
	public class DadosFixa{
		public String atualizadoEm {get; private set;}
		public String condicaoPagBF {get; private set;}
		public String documentosReservaDeVagaIBMEC{get; private set;}
		public String documentosMatricula {get; private set;}
		public String docEnemColegial {get; private set;}
		public String docEnemEnsMedConcluido {get; private set;}
		public String docParaPortDiploma {get; private set;}
		public String docParaTransfExterna {get; private set;}
		public String horarioManha {get; private set;}
		public String horariotarde {get; private set;}
		public String horariointegral {get; private set;}
		public String horarioNoite {get; private set;}
		public String horaAtendimentoIes {get; private set;}
		public String horaAtendContactCenter {get; private set;}
		public String site {get; private set;}
		public String whatsapp {get; private set;}
		public String planilhaPegueInformacoes {get; private set;}
		public String tiposFinanciamento {get; private set;}
		public String produto 	{get; private set;}
		public String aulas2018_1 {get; private set;}
		public String aulas2018_2 {get; private set;}
	
	public DadosFixa(Base_fixa__c base){
		atualizadoEm = base.Atualizado_em__c;
		condicaoPagBF = base.Condi_es_de_Pagamento__c;
		documentosReservaDeVagaIBMEC = base.Docs_para_reserva_pr_xima_entrada_IBMEC__c;
		documentosMatricula = base.Documentos_para_a_matr_cula__c;
		docEnemColegial = base.Documentos_para_ENEM_cursando_colegial__c;
		docEnemEnsMedConcluido = base.Documentos_para_ENEM_colegial_conclu_do__c;
		docParaPortDiploma = base.Documentos_para_Portador_de_Diploma__c;
		docParaTransfExterna = base.Documentos_para_Transfer_ncia_Externa__c;
		horarioManha = base.Hor_rio_Aula_Turno_Manh__c;
		horariotarde = base.Hor_rio_Aula_Turno_Tarde__c;
		horariointegral = base.Hor_rio_Aula_Turno_Integral__c;
		horarioNoite = base.Hor_rio_Aula_Turno_Noite__c;
		horaAtendimentoIes = base.Hor_rio_de_Atendimento_na_IES__c;
		horaAtendContactCenter = base.Horario_de_Atendimento_no_Contact_Center__c;
		site = base.Site__c;
		whatsapp = base.WhatsApp__c;
		planilhaPegueInformacoes = base.Planilha_G_Docs_Pegue_Informa_es__c;
		tiposFinanciamento = base.Tipos_de_Financiamento__c;
		//produto	 = base.Produto__c;
		aulas2018_1 =base.Inicio_das_Aulas_2018_1__c;
		aulas2018_2 =base.Inicio_das_Aulas_2018_2__c;

	}
}
	public class Dados{

		public String nota_mec {get; private set;}
		public String nota_enade {get; private set;}
		public String premioReco {get; private set;}
		public String projetos {get; private set;}
		public String laboratorioGrad {get; private set;}
		public String diferenciais {get; private set;}
		public String garantiaMelhor {get; private set;}
		public String sucessos {get; private set;}
		public String naoRelacionados {get; private set;}
		public String questionIes {get; private set;}
		public String contraArgumentos {get; private set;}
		public String periodoEstagio {get; private set;}
		public String valorBolsa {get; private set;}
		//public String produto 	{get; private set;}
		public String condicaoPagamentoGrad {get; private set;}
		public String laboratorioPos {get; private set;}
		public String principaisQuestionamentoPos {get; private set;}
		public String diferenciaisdoCursoPos {get; private set;}
		//public String precoMensalGrad {get; private set;}
		public String duracaoCursoGrad {get; private Set;}
		public String financiamentoGrad {get; private Set;}
		public String semestreRequisitadoParaUsoDoLab {get; private Set;}
		public String precessoDecisorio {get; private Set;}
		public String observacao1 	{get; private set;}
		public String observacao2 	{get; private set;}
		public String observacao3 	{get; private set;}
		public String observacao4 	{get; private set;}
		public String observacao5 	{get; private set;}

		//DADOS VESTIBULAR
		public String formaIngreco {get; private set;}
		public String pricipaisEventos {get; private set;}
		public String dataInscricao {get; private set;}
		public String taxaInscricao {get; private set;}
		public String prova {get; private set;}
		public String resultado {get; private set;}
		public String periodoMatricula {get; private set;}
		public String preRequesitoMatricula {get; private set;}
		public String condicaoPagamento {get; private set;}
		public String precoMensal {get; private set;}
		public String precoMensalObservacao {get; private set;}
		public String duracaoCurso {get; private set;}
		public String turno {get; private set;}
		public String finaciamento {get; private set;}
		public String docParaMatricula {get; private set;}
		public String localProva {get; private set;}
		public String bolsaGrupo {get; private set;}
		public String transferidoPortador {get; private set;}
		public String isencaoTransfPortador {get; private set;}
		public String bolsaCorpore {get; private set;}
		public String bolsaMeritoIbmec {get; private set;}
		public String regrasConcessao {get; private set;}
		public String regrasConcessao1 {get; private set;}
		public String regrasConcessao2 {get; private set;}
		public String regrasConcessao3 {get; private set;}
		public String regrasConcessao4 {get; private set;}
		public String regrasConcessao5 {get; private set;}
		public String regrasConcessao6 {get; private set;}
		public String regrasConcessao7 {get; private set;}
		public String regrasConcessao8 {get; private set;}
		public String regrasConcessao9 {get; private set;}
		public String regrasConcessao10 {get; private set;}
		public String regrasConcessao11 {get; private set;}
		public String regrasConcessao12 {get; private set;}
		public String regrasConcessao13 {get; private set;}
		public String regrasConcessao14 {get; private set;}
		public String regrasConcessao15 {get; private set;}

		public String bolsaValidada2018_1 {get; private set;}
		public String bolsaValidada2018_1_1 {get; private set;}
		public String bolsaValidada2018_1_2 {get; private set;}
		public String bolsaValidada2018_1_3 {get; private set;}
		public String bolsaValidada2018_1_4 {get; private set;}
		public String bolsaValidada2018_1_5 {get; private set;}

		public String bolsaValidada2018_2 {get; private set;}
		public String bolsaValidada2018_2_1 {get; private set;}
		public String bolsaValidada2018_2_2 {get; private set;}
		public String bolsaValidada2018_2_3 {get; private set;}
		public String bolsaValidada2018_2_4 {get; private set;}
		public String bolsaValidada2018_2_5 {get; private set;}

		public String cumulatividadeBolsa {get; private set;}
		public String turnoGrad {get; private set;}
		public String limiteMatricula {get; private set;}
		public String desconto {get; private set;}

		public String quantidadeTurmas {get; private set;}
		public String dataEntrevista {get; private set;}
		public String preRequisito {get; private set;}
		public String diaAula	 {get; private set;}
		public String periodicidadeAulas {get; private set;}
		public String inicioAulas	 {get; private set;}
		public String descontoExaluno30agendar {get; private set;}
		public String descontoexaluno35	 {get; private set;}
		public String grupoDeAlunosAdmissoesAgendadas {get; private set;}
		public String parceriaEmpresasCorpore30 {get; private set;}
		public String parceriaEmpresasCorpore10 {get; private set;}

		public String parentesco30 {get; private set;}
		public String parceriaCorpore20 {get; private set;}
		public String parentesco15 {get; private set;}
		public String parceriaCorpore15 {get; private set;}
		public String antecipacaoMatr_10_25Mensalidade {get; private set;}
		//public String diferenciais   {get; private set;}
		public String dataProva     {get; private set;}
		public String valorComDesconto {get;private set;}
		public String quandoSera {get;private set;}
		public String seEstendeParaPos {get;private set;}
		public String metodoDePagamento {get;private set;}
		public String processoDeInscricao {get;private set;}
		public String organizadorDoEvento{get;private set;}
		public String oQueEvento {get;private set;}
		public String condicaoPagVest {get; private set;}
		public String valorDoCurso {get; private set;}
		public String quantidadeDeTurmasOfertadas {get; private set;}
		public String cargaHorarioVest {get; private set;}
		public String diferenciaisVest {get; private set;}
		

		//DADOS PÓS GRADUÇÃO

		public String proceSeletivo {get; private set;}
		public String corpoAcademico {get; private set;}
		public String cargaHoraria {get; private set;}
		public String gradeCurso {get; private set;}
		public String politicaFaltas {get; private set;}
		public String conclusaoCurso {get; private set;}
		public String porqueSerDevry {get; private set;}
		public String pricipaisQuestions {get; private set;}
		public String contraArgumento {get; private set;}
		public String formaPagamento {get; private set;}
		public String comparativos   {get; private set;}
		public String bolsaAntecipadas {get; private set;}
		public String contraArgumentoPos{get; private set;}
		

    	public Dados( Knowledge__kav know ) {


    	nota_mec         = know.Nota_MEC__c;
		nota_enade       = know.Nota_Enade__c;
		premioReco       = know.Premio__c;
		projetos         = know.Projetos__c;
		laboratorioGrad  = know.Labs__c;
		diferenciais     = know.Diferenciais_do_curso__c;
		sucessos         = know.Cases_de_Sucesso__c;
		naoRelacionados  = know.Cases_n_o_relacionados__c;
		questionIes      = know.Principal_questionamento__c;
		contraArgumentos = know.Como_contra_argumentamos__c;
		periodoEstagio   = know.Per_odo_m_dio_de_in_cio_de_est_gio__c;
		valorBolsa       = know.Valor_m_dio_maximo_de_bolsa__c;
		comparativos     = know.Porque_podemos_afirmar_que_o_curso_em_qu__c;
		//precoMensalGrad  = know.Pre_o_Mensal_Grad__c;
		duracaoCursoGrad = know.Dura_o_do_CursoGRAD__c;
		condicaoPagamentoGrad =know.Condi_es_de_PagamentoGRAD__c;
		financiamentoGrad 	=know.FinanciamentoGRAD__c;
		precessoDecisorio =know.Processo_decis_rio__c;
		semestreRequisitadoParaUsoDoLab = know.Semestre_requisitado_para_uso_do_lab__c;
		observacao1		 = know.Observa_o_1__c;
		observacao2		 = know.Observa_o_2__c;
		observacao3		 = know.Observa_o_3__c;
		observacao4		 = know.Observa_o_4__c;
		observacao5		 = know.Observa_o_5__c;

       //VARIAVEIS VESTIBULAR

		formaIngreco            =know.FORMA_DE_INGRESSO__c;
		pricipaisEventos = know.Principais_Eventos__c;
		dataInscricao           =know.DATA_DE_INSCRI_O__c;
		taxaInscricao           =know.Taxa_de_inscri_o__c;
		prova                   =know.Prova__c;
		resultado               =know.Resultado__c;
		periodoMatricula        =know.Per_odo_de_Matr_cula__c;
		preRequesitoMatricula   =know.Pr_Requisito_para_Matr_cula__c;
		//condicaoPagamento       =know.Condi_es_de_Pagamento__c;
		precoMensal             =know.Pre_o_Mensal__c;
		desconto	     =know.Desconto__c;
		duracaoCurso            =know.Dura_o_do_Curso__c;
		turno                   =know.Turno__c;
		finaciamento            =know.Financiamento__c;
		//docParaMatricula        =know.Documentos_para_a_matr_cula__c;
		//produto 				=know.Produto__c;
		localProva              =know.Local_da_prova__c;
		bolsaAntecipadas        =know.Bolsa_Antecipa_o_de_Vagas__c;
		bolsaGrupo 				=know.Bolsa_Grupo__c;
		transferidoPortador 	=know.Bolsa_Transferido_e_Portador__c;
		isencaoTransfPortador   =know.Bolsa_Isen_o_de_Matr_cula_Transfer_ncia__c;
		bolsaCorpore 			=know.Bolsa_Corpore__c;
		bolsaMeritoIbmec        =know.Bolsa_M_rito_Ibmec__c;
		regrasConcessao         =know.Regras_para_Concess_o__c;
		bolsaValidada2018_1		=know.Bolsa_v_lida_para_2018_1__c;
		bolsaValidada2018_1_1	=know.Bolsa_v_lida_para_2018_1_1__c;
		bolsaValidada2018_1_2	=know.Bolsa_v_lida_para_2018_1_2__c;
		bolsaValidada2018_1_3	=know.Bolsa_v_lida_para_2018_1_3__c;
		bolsaValidada2018_1_4	=know.Bolsa_v_lida_para_2018_1_4__c;
		bolsaValidada2018_1_5	=know.Bolsa_v_lida_para_2018_1_5__c;

		bolsaValidada2018_2     =know.Bolsa_v_lida_para_2018_2__c;
		bolsaValidada2018_2_1	=know.Bolsa_v_lida_para_2018_2_1__c;
		bolsaValidada2018_2_2	=know.Bolsa_v_lida_para_2018_2_2__c;
		bolsaValidada2018_2_3	=know.Bolsa_v_lida_para_2018_2_3__c;
		bolsaValidada2018_2_4	=know.Bolsa_v_lida_para_2018_2_4__c;
		bolsaValidada2018_2_5	=know.Bolsa_v_lida_para_2018_2_5__c;

		cumulatividadeBolsa=know.Cumulatividade_de_Bolsa__c;
		turnoGrad               =know.TurnoGRAD__c;
		regrasConcessao1		=know.Regras_para_Concess_o1__c;
		regrasConcessao2		=know.Regras_para_Concess_o2__c;
		regrasConcessao3		=know.Regras_para_Concess_o3__c;
		regrasConcessao4		=know.Regras_para_Concess_o4__c;
		regrasConcessao5		=know.Regras_para_Concess_o5__c;
		regrasConcessao6		=know.Regras_para_Concess_o6__c;
		regrasConcessao7		=know.Regras_para_Concess_o7__c;
		regrasConcessao8		=know.Regras_para_Concess_o8__c;
		regrasConcessao9		=know.Regras_para_Concess_o9__c;
		regrasConcessao10		=know.Regras_para_Concess_o10__c;
		regrasConcessao11		=know.Regras_para_Concess_o11__c;
		regrasConcessao12		=know.Regras_para_Concess_o12__c;
		regrasConcessao13		=know.Regras_para_Concess_o13__c;
		regrasConcessao14		=know.Regras_para_Concess_o14__c;
		regrasConcessao15		=know.Regras_para_Concess_o15__c;

		valorComDesconto        =know.Valor_com_desconto__c;
		

		limiteMatricula					 =know.Limite_para_matriculas__c;
		dataProva						 =know.Data_da_prova__c;
		quantidadeTurmas 				 =know.Quantidade_de_turmas__c;
		dataEntrevista					 =know.Data_da_entrevista__c;
		preRequisito   					 =know.Pre_requisito__c;
		diaAula    					     =know.Dias_de_aula__c;
		periodicidadeAulas    			 =know.Periodicidade_das_aulas__c;
		inicioAulas    					 =know.In_cio_das_aulas__c;
		descontoExaluno30agendar    	 =know.Desconto_Ex_aluno_at_30_agendar_admis__c;
		descontoexaluno35    			 =know.Desconto_ex_aluno_35__c;
		grupoDeAlunosAdmissoesAgendadas  =know.Grupo_de_alunos_admiss_es_agendadas__c;
		parceriaEmpresasCorpore30    	 =know.Parceria_empresas_Corpore_at_30__c;
		parceriaEmpresasCorpore10    	 =know.Parceria_empresas_Corpore_at_10__c;
		parentesco30    				 =know.Parentesco_30_parentes_de_1_grau__c;
		parceriaCorpore20    			 =know.Parceria_Corpore_at_20__c;
		parentesco15   					 =know.Parentesco_15_parentes_de_1_grau__c;
		parceriaCorpore15   			 =know.Parceria_Corpore_15__c;
		antecipacaoMatr_10_25Mensalidade =know.Antecipa_o_de_matr_10_25_mensalidade__c;
		//diferenciais 					= know.Diferenciais__c;
		oQueEvento     =know.O_que_o_evento__c;
		quandoSera     =know.Quando_ser__c;
		seEstendeParaPos=know.Se_estende_para_P_s__c;
		metodoDePagamento=know.M_todo_de_pagamento__c;
		processoDeInscricao=know.Processo_de_inscri_o__c;
		organizadorDoEvento=know.Organizador_do_Evento__c;
		condicaoPagVest    =know.Condi_es_de_Pag__c;
		valorDoCurso       = know.Valor_do_curso__c;
		quantidadeDeTurmasOfertadas =know.Quantidade_de_turmas_ofertadas__c;
		cargaHorarioVest			=know.Carga_hor_riaVest__c;
		diferenciaisVest         =know.Diferenciais__c;

		//VARIAVEIS DE PÓSGRADUAÇÃO

		proceSeletivo       =know.Processo_Seletivo__c;
		corpoAcademico      =know.Corpo_acad_mico__c;
		cargaHoraria        =know.Carga_hor_ria__c;
		gradeCurso          =know.Grade_do_curso__c;
		politicaFaltas		=know.Pol_tica_de_Faltas__c;
		conclusaoCurso		=know.Conclus_o_de_Curso__c;
		porqueSerDevry		=know.O_porque_de_ser_DeVry__c;
		pricipaisQuestions	=know.Principal_questionamento__c;
		contraArgumento		=know.Como_contra_argumentamos__c;
		formaPagamento      =know.Formas_de_Pagamento__c;
		laboratorioPos		=know.Laborat_rios_do_curso__c;
		principaisQuestionamentoPos= know.Principal_questionamento_Pos__c;
		diferenciaisdoCursoPos = know.Diferenciais_do_cursoPos__c;
		contraArgumentoPos     = know.Como_contra_argumentamos_POS__c;





    	}
    }
}