public with sharing class TaskBO {
    
    /*
        Singleton
    */
    private static final TaskBO instance = new TaskBO();    
    private TaskBO(){}
    
    public static TaskBO getInstance() {
        return instance;
    }

    public void taskToLead(List<Task> task){
    
        //filtrar tipo de registro captação em task + tasks concluídas
        RecordType rt = [select Id, Name from RecordType where SobjectType = 'Task' and Name = 'Captação' Limit 1];

        if(task[0].RecordTypeId == rt.Id && task[0].Status == 'Concluída'){

            List<Lead> listLead = [Select Conseguiu_Contato__c, Tem_Interesse__c, QuerInscrever__c, Como__c, DataReagendamento__c, Contador__c, ContadorAgendamento__c 
                            From Lead 
                                Where Id = :task[0].whoId];
            System.debug('>>>' + listLead);

            For(Lead lead : listLead){

                lead.Conseguiu_Contato__c = task[0].Conseguiu_Contato__c;
                lead.Tem_Interesse__c = task[0].Tem_Interesse__c;
                lead.QuerInscrever__c = task[0].QuerInscrever__c;
                lead.Como__c = task[0].Como__c;
                if(lead.Conseguiu_Contato__c == 'Caixa Postal' || lead.Conseguiu_Contato__c == 'Não Atende' || lead.Conseguiu_Contato__c == 'Não se encontra'){
                    if(lead.Contador__c == null)
                        lead.Contador__c = 1;
                    else
                        lead.Contador__c = lead.Contador__c + 1;
                }else{
                    if(lead.Contador__c == null)
                        lead.Contador__c = 1;

                    if(lead.ContadorAgendamento__c == null)
                        lead.ContadorAgendamento__c = 1;
                    else
                        lead.ContadorAgendamento__c = lead.ContadorAgendamento__c + 1;
                    
                }
                lead.DataReagendamento__c = task[0].DataReagendamento__c;
                update lead;
            }
        }
    }

    public void taskToOpportunity(List<Task> task){
    
        //filtrar tipo de registro captação em task + tasks concluídas
        List<RecordType> rt = [select Id, Name from RecordType where SobjectType = 'Task' and Id = :task[0].RecordTypeId];

        if((rt[0].Name == 'Captação' || rt[0].Name == 'Presencial - Ausente' || rt[0].Name == 'Presencial - Pré-Inscrito' || rt[0].Name == 'Presencial - Reprovado' || rt[0].Name == 'Presencial - Aprovado' || rt[0].Name == 'Presencial - Inscrito') && task[0].Status == 'Concluída'){

            Opportunity opportunity = [Select DataMatricula__c, ConseguiuContato__c, TemInteresse__c, QuerInscrever__c, Como__c, DataReagendamento__c, Contador__c, ContadorAgendamento__c, Motivo_nao_Interesse__c, PresencaConfirmada__c
                            From Opportunity 
                                Where Id = :task[0].whatId Limit 1];

            opportunity.ConseguiuContato__c = task[0].Conseguiu_Contato__c;
            opportunity.TemInteresse__c = task[0].Tem_Interesse__c;
            opportunity.QuerInscrever__c = task[0].QuerInscrever__c;
            opportunity.Como__c = task[0].Como__c;
            if(task[0].DataMatricula__c != null)
                opportunity.DataMatricula__c = task[0].DataMatricula__c;
            
            opportunity.DataReagendamento__c = task[0].DataReagendamento__c;
            opportunity.Motivo_nao_Interesse__c = task[0].MotivoNaoInteresseInscrever__c;
            
            if(task[0].Motivo_de_Ausencia__c != null)
                opportunity.MotivoAusencia__c = task[0].Motivo_de_Ausencia__c;

            if(task[0].ConfirmaPresencaVestibular__c != null)
                opportunity.PresencaConfirmada__c = task[0].ConfirmaPresencaVestibular__c;

            if(opportunity.ConseguiuContato__c == 'Caixa Postal' || opportunity.ConseguiuContato__c == 'Não Atende' || opportunity.ConseguiuContato__c == 'Não se encontra'){
            		opportunity.TaskName__c = task[0].Subject;
                    if(opportunity.Contador__c == null)
                        opportunity.Contador__c = 1;
                    else
                        opportunity.Contador__c = opportunity.Contador__c + 1;
            }else{
                if(opportunity.Contador__c == null)
                        opportunity.Contador__c = 1;

                if(opportunity.ContadorAgendamento__c == null)
                    opportunity.ContadorAgendamento__c = 1;
                else
                    opportunity.ContadorAgendamento__c = opportunity.ContadorAgendamento__c + 1;
                    
                }

            update opportunity;
        }
    }
}