public class Area_CursoMockup {
    public static Area_Curso__C createAndGetCurso(){
        Area_Curso__c area = new Area_Curso__C(Name = 'Area Teste');
        insert area;
        return area;
    }

}