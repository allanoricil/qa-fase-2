public with sharing class EvolucionalListUserExamResults {
	
	public static void processingQueue(String queueId, String payload) {
	    syncToServer(queueId, payload);         
	}

	@future(Callout = true)
	public Static void syncToServer(String queueId, String payload){
	    try {
	        HttpResponse res = EvolucionalAPI.listaSimulados(payload);
	        if(res.getStatusCode() == 200){
	        	processJsonBody(queueId, res.getBody());  
	        }else{
	        	QueueBO.getInstance().updateQueue(queueId, 'EVOLUCIONAL_SERVICES / ' + res.getStatusCode() + ' / ' + res.getStatus());
        	}
        }catch(CalloutException ex) {
        	QueueBO.getInstance().updateQueue(queueId, 'EVOLUCIONAL_SERVICES / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
        }
    }	

    private static void processJsonBody( String queueId, String response){
  	    Queue__c fila = [SELECT Oportunidade__c  
  	                       FROM Queue__c 
  	                      WHERE Id =:queueId 
  	                      LIMIT 1];

  	    Opportunity opp = [SELECT Id
  	                         FROM Opportunity 
  	                        WHERE Id =:fila.Oportunidade__c 
  	                        LIMIT 1];

  	    AssessmentList assessment = (AssessmentList)JSON.deserialize(response, AssessmentList.class);
  	    updateOpportunityWithGrades(opp.Id, assessment);
  	    QueueBO.getInstance().updateQueue(queueId, ''); 
    }


    private static void updateOpportunityWithGrades(Id oportunityId, AssessmentList assessmentList){
        Opportunity opp = getOpportunitysNecessaryData(oportunityId);
        Decimal passingGrade = opp.Processo_Seletivo__r.Nota_M_nima__c;      
        
        List<Integer> notas = extractGrades(assessmentList);
        Integer nota1 = notas.get(0);
        Integer nota2 = notas.get(1);
        Integer nota3 = notas.get(2);
        Integer highestGrade = computeHighestGrade(nota1, nota2, nota3);

        //boolean oppWithScholarship = isOppWithScholarship(opp.Bolsa__c);

        if(highestGrade >= passingGrade){
          registerJsonRequest(opp);
        }

        /*
        if(highestGrade >= passingGrade){
            if(oppWithScholarship){
                opp.StageName = 'Aprovado';
            }else{
                opp.StageName = 'Inscrito';
            }
            registerJsonRequest(opp);
        }else{
            if(oppWithScholarship){
                opp.StageName = 'Reprovado';
            }else{
                opp.StageName = 'Inscrito';
            }
        }*/

        opp.Nota_1__c = nota1;
        opp.Nota_2__c = nota2;
        opp.Nota_3__c = nota3;

        update opp;
    }

    private static Integer computeHighestGrade(Integer grade1, Integer grade2, Integer grade3){
        Integer highestGrade;
        if (grade1 >= grade2){
            highestGrade  = grade1;
        }else{
            highestGrade  = grade2;
        }
        if (highestGrade <= grade3){
            highestGrade = grade3;
        }
        return highestGrade;
    }

    private static List<Integer> extractGrades(AssessmentList assessmentList){
        List<Integer> notas = new List<Integer>();
        Integer nota1, nota2, nota3;
        Integer notaLC1, notaLC2, notaLC3;
        Integer notaMT1, notaMT2, notaMT3;
        List<Assessment> assessments = assessmentList.getAssessmentList(); 

        Assessment assessment1 = assessments.get(0);
        Assessment assessment2 = assessments.get(1);
        Assessment assessment3 = assessments.get(2);

        Evaluation evaluation1 = assessment1.getEvaluations().get(0);
        Evaluation evaluation2 = assessment2.getEvaluations().get(0);
        Evaluation evaluation3 = assessment3.getEvaluations().get(0);

        List<Area> areas1 = evaluation1.getAreas();
        Area nota1area1 = areas1.get(0);
        Area nota2area1 = areas1.get(1);

        List<Area> areas2 = evaluation2.getAreas();
        Area nota1area2 = areas2.get(0);
        Area nota2area2 = areas2.get(1);

        List<Area> areas3 = evaluation3.getAreas();
        Area nota1area3 = areas3.get(0);
        Area nota2area3 = areas3.get(1);

        notaLC1 = nota1area1.getValue() != null ? Integer.valueOf(nota1area1.getValue()) : 0;
        notaMT1 = nota2area1.getValue() != null ? Integer.valueOf(nota2area1.getValue()) : 0;
        notaLC2 = nota1area2.getValue() != null ? Integer.valueOf(nota1area2.getValue()) : 0;
        notaMT2 = nota2area2.getValue() != null ? Integer.valueOf(nota2area2.getValue()) : 0;
        notaLC3 = nota1area3.getValue() != null ? Integer.valueOf(nota1area3.getValue()) : 0;
        notaMT3 = nota2area3.getValue() != null ? Integer.valueOf(nota2area3.getValue()) : 0;

        nota1 = (notaLC1 + notaMT1)/2;
        nota2 = (notaLC2 + notaMT2)/2;
        nota3 = (notaLC3 + notaMT3)/2;        

        notas.add(nota1);
        notas.add(nota2);
        notas.add(nota3);
        
        return notas;
    }

    private static Opportunity getOpportunitysNecessaryData(Id opportunityId){
        Opportunity opp = [SELECT Id,
                                  Processo_Seletivo__r.Nota_M_nima__c,
                                  Bolsa__c,
                                  StageName,   
                                  Processo_seletivo__r.C_digo_da_Coligada__c,
                                  Processo_seletivo__r.C_digo_Processo_Seletivo__c,
                                  Numeroinscrprocsel__c, 
                                  Processo_seletivo__r.Sequencial_Processo_Seletivo__c,
                                  Account.Name,
                                  Account.Sexo__c,
                                  Data_Nascimento__c,
                                  Account.Nacionalidade__c,
                                  Estado_Civil__c,
                                  Account.Nome_do_Pai__c,
                                  Account.Nome_da_Mae__c,
                                  Account.RG__c,
                                  Account.CPF_2__c,
                                  Account.Rua__c,
                                  Account.N_mero__c,
                                  Account.Bairro__c,
                                  Account.Cidade__c,
                                  Account.Estado__c,
                                  Account.CEP__c,
                                  Account.Phone,
                                  Account.Complemento__c,
                                  Account.PersonEmail,
                                  Processo_seletivo__r.IdPeriodoLetivo__c,
                                  Nota__c,
                                  ClassificaProcessoSeletivo__c,
                                  X1_OpcaoCurso__r.C_digo_da_Oferta__c ,
                                  Polo__r.Name,
                                  Processo_seletivo__r.ID_Academus_ProcSel__c,
                                  Polo__r.CodPolo__c
                             FROM Opportunity 
                            WHERE Id =:opportunityId];
        return opp;
    }

    private static boolean isOppWithScholarship(String bolsa){
        return bolsa != null && !String.isEmpty(bolsa);
    }

    private static void registerJsonRequest(Opportunity opp){
        String jsonRequest = EvolucionalJsonBuilder.buildRequestForListExamsService(opp);
        String serviceName = QueueEventNames.APROVA_CANDIDATO_2.name();
        Queue__c fila = EvolucionalQueueFactory.buildQueue(opp.Id, jsonRequest, serviceName);
        ProcessorControl.inFutureContext = true;
        QueueDAO.getInstance().insertData(fila); 
    }

    
}