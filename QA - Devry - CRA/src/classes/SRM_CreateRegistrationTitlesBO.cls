/*
@author Diego Moreira
@class Classe de negocio para criação de titulos na integração
*/
public class SRM_CreateRegistrationTitlesBO implements IProcessingQueue {
    /* 
Processa a fila de atualização de criação de titulos no SAP
@param queueId Id da fila de processamento 
@param eventName nome do evento de processamento
@param payload JSON com o item da fila para processamento
*/
    public String titulo_id;
    public static void processingQueue(String queueId, String eventName, String payload) {
        //syncToServerSap( queueId, eventName, payload );
        syncToServer(queueId, eventName, payload);
    }

    public static void processingQueueSap(String queueId, String eventName, String payload, String payLoadSap) {
        system.debug('queueId: ' + queueId);
        system.debug('eventName: ' + eventName);
        system.debug('payLoadSap: ' + payLoadSap);
        //syncToServerSap( queueId, eventName, payLoadSap );

        syncToServer(queueId, eventName, payload);
    }

    /*
Processa a fila de atualização de criação de titulos no SAP
*/
    @future(Callout = true)
    private Static void syncToServer(String queueId, String eventName, String payload) {
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();

        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('SAP-InboundSap').Endpoint__c);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('SAP-InboundSap').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            req.setBody(payload);

            try {
                Http h = new Http();
                HttpResponse res = h.send(req);
                system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                system.debug('serverres.getBody(): ' + res.getBody());
                if (res.getStatusCode() == 200)
                    processJsonBody(queueId, res.getBody());
                else
                    QueueBO.getInstance().updateQueue(queueId, 'InboundSap / ' + res.getStatusCode() + ' / ' + res.getStatus());
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'InboundSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            QueueBO.getInstance().updateQueue(queueId, 'Token  / ' + mapToken.get('401'));
        }
    }
    /*
        SAP CUSTOMER 
    */

    private Static void syncToServerSap(String queueId, String eventName, String payload) {
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();

        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('ClienteSap').Endpoint__c);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', 'A58060FC5E289220BF971DFCFBD43CEF');
            req.setTimeout(120000);
            req.setBody(payload);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                system.debug('sapres.getStatusCode(): ' + res.getStatusCode());
                system.debug('sapres.getBody(): ' + res.getBody());
                system.debug('sapres.getBody(): ' + res.toString());

                if (res.getStatusCode() == 200) {
                    //System.debug( '>>> postDatabody ' + res.getBody() );
                    processJsonBodySAP(queueId, res.getBody());
                    //QueueBO.getInstance().updateQueue( queueId, '' );                                                                                 
                } else
                    QueueBO.getInstance().updateQueue(queueId, 'clientesap / ' + res.getStatusCode() + ' / ' + res.getStatus());
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'clientesap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'clientesap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }

        } else {
            QueueBO.getInstance().updateQueue(queueId, 'Token Cliente / ' + mapToken.get('401'));
        }

    }

    /*
Processa o retorno do serviço
@param queueId Id da fila que sera atualizada
@param jsonBody JSON de retorno do serviço
*/
    /* public static void processJsonBodySAP(String queueId, String jsonBodySap){
         
       
         // INICIO
         Map<String,Object> mapResult = ( Map<String,Object> ) JSON.deserializeUntyped( jsonBodySap );

         System.debug('debug1>>> ' + jsonBodySap );

         System.debug('debug2>>> ' + mapResult.get('id_cliente'));

         System.debug('debug2>>> ' + mapResult);

         System.debug(mapResult);
         String codigosap = String.valueOf(mapResult.get('id_cliente')) ;
         System.debug('debug-codigo sap' + codigosap);

         Titulos__c titulosap = TituloBO.getInstance().updateClienteSap('a0i2F000000971q', String.valueOf(mapResult.get('id_cliente')) );

         ProcessorControl.inFutureContext = true;        
         TituloDAO.getInstance().updateData( titulosap ); 
         // FIM 

     }*/

    public static void processJsonBodySAP(String jsonBodySap) {

        String empresaSap = '';
        String cpf = '';
        String idCliente = '';

        //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe ResponseItems
        //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
        jsonBodySap = jsonBodySap.substring(0, 20) + '[' + jsonBodySap.substring(20, jsonBodySap.length() - 1) + ']}';
        ServicosResponse SR = (ServicosResponse) JSON.deserializeStrict(jsonBodySap, ServicosResponse.class);

        system.debug(SR + ' destaque1 ');
        if (!SR.ClienteSapResult.isEmpty()) {
            for (ResponseItems item: SR.ClienteSAPResult) {
                cpf = item.cpf;
                idCliente = item.id_cliente;
                empresaSap = item.empresaSap;
            }

            cpf = cpf.replaceAll('[|.|-]', '');

            system.debug(cpf + ' destaque ');

            list < Titulos__c > titulo_id = [SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c, Cliente_Sap__c,
                DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, Instituicao__r.Raz_o__c, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Parcelado__c
                FROM Titulos__c
                WHERE CPF__c =: CPF order by CreatedDate desc
            ];
            system.debug('titulo_id: ' + titulo_id);
            if (!titulo_id.isEmpty()) {
                /*
                Titulos__c titulosap = TituloBO.getInstance().updateClienteSap( titulo_id[0].Id, idCliente );
                system.debug('iddestaque'+ titulosap);
                ProcessorControl.inFutureContext = true;        
                TituloDAO.getInstance().updateData( titulosap ); 
                */

                //Atualiza o clienteSap em todos os títulos com o mesmo CPF
                List < Titulos__c > titulosParaAtualizar = new List < Titulos__c > ();
                for (Titulos__c titulo: titulo_id) {
                    idCliente = idCliente.leftPad(10, '0');
                    titulo.Cliente_Sap__c = idCliente;
                    //titulo.EmpresaSAP__c = empresaSap;
                    titulosParaAtualizar.add(titulo);
                }

                ProcessorControl.inFutureContext = true;
                TituloDAO.getInstance().updateData(titulosParaAtualizar);

                //update titulo_id;

            }
        }
    }
    //Criado por Carlos Filho Em: 03/08/2017 ************
    public static void processJsonBodySAP(String queueId, String jsonBodySap) {

        String empresaSap = '';
        String cpf = '';
        String idCliente = '';

        //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe ResponseItems
        //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
        jsonBodySap = jsonBodySap.substring(0, 20) + '[' + jsonBodySap.substring(20, jsonBodySap.length() - 1) + ']}';
        ServicosResponse SR = (ServicosResponse) JSON.deserializeStrict(jsonBodySap, ServicosResponse.class);

        system.debug(SR + ' destaque1 ');
        if (!SR.ClienteSapResult.isEmpty()) {
            for (ResponseItems item: SR.ClienteSAPResult) {
                cpf = item.cpf;
                idCliente = item.id_cliente;
                empresaSap = item.empresaSap;
            }

            cpf = cpf.replaceAll('[|.|-]', '');

            system.debug(cpf + ' destaque ');

            list < Titulos__c > titulo_id = [SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c, Cliente_Sap__c,
                DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, Instituicao__r.Raz_o__c, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Parcelado__c
                FROM Titulos__c
                WHERE CPF__c =: CPF order by CreatedDate desc
            ];
            system.debug('titulo_id: ' + titulo_id);
            if (!titulo_id.isEmpty()) {
                /*
                Titulos__c titulosap = TituloBO.getInstance().updateClienteSap( titulo_id[0].Id, idCliente );
                system.debug('iddestaque'+ titulosap);
                ProcessorControl.inFutureContext = true;        
                TituloDAO.getInstance().updateData( titulosap ); 
                */

                //Atualiza o clienteSap em todos os títulos com o mesmo CPF
                List < Titulos__c > titulosParaAtualizar = new List < Titulos__c > ();
                for (Titulos__c titulo: titulo_id) {
                    idCliente = idCliente.leftPad(10, '0');
                    titulo.Cliente_Sap__c = idCliente;
                    //titulo.EmpresaSAP__c = empresaSap;
                    titulosParaAtualizar.add(titulo);
                }

                ProcessorControl.inFutureContext = true;
                TituloDAO.getInstance().updateData(titulosParaAtualizar);

                //update titulo_id;
                Queue__c fila = [SELECT Id, Payload__c FROM Queue__c WHERE Id =: queueId limit 1];
                fila.payload__c = createTitlesToJson(titulo_id[0]);
                update fila;
            }
        }
    }

    public Class ServicosResponse {
        public List < responseItems > ClienteSAPResult;
    }

    public Class ResponseItems {
        public String cpf;
        public String empresaSap;
        public String id_cliente;
    }

    /* 
        SAP CUSTOMER 
    */
    private static void processJsonBody(String queueId, String jsonBody) {
        Map < String, Object > postData = (Map < String, Object > ) JSON.deserializeUntyped(jsonBody);
        System.debug('>>> postData ' + postData);

        for (Object objResult: (List < Object > ) postData.get('InboundSapResult')) {
            String errorMessage = '';
            Map < String, Object > result = (Map < String, Object > ) objResult;
            system.debug('aqui' + result);
            Titulos__c titulos = TituloBO.getInstance().updateCreateSapTitulos(String.valueOf(result.get('rEF_DOC_NOField')).left(15), (String) result.get('bELNRField'),
                (String) result.get('bUKRSField'), (String) result.get('gJAHRField'));
            if (String.valueOf(result.get('bELNRField')).equals('$')) {
                for (Object objMessage: (List < Object > ) result.get('messagesField')) {
                    Map < String, Object > resultMessage = (Map < String, Object > ) objMessage;
                    errorMessage = (String) resultMessage.get('mESSAGEField');
                }
            }
            ProcessorControl.inFutureContext = true;
            TituloDAO.getInstance().updateData(titulos);
            update titulos;
            QueueBO.getInstance().updateQueue(queueId, errorMessage);
        }

    }

    /*
Cria o JSON do titulo criado no Salesforce 
*/
    public static String createTitlesToJson(Titulos__c titulo) {
        /*Titulos__c titulo = [ SELECT Id, Name, DataVencimento__c, Instituicao__r.CodigoSAP__c, ProcessoSeletivo__r.CodigoSAPCampus__c, Instituicao__r.CentroLucro__c, Cliente_Sap__c,
                                     DiasParaVencimento__c, Instituicao__r.Banco__c, Instituicao__r.Conta_Caixa__c, ProcessoSeletivo__r.Name, ProcessoSeletivo__r.Valor_Taxa_Inscricao__c, Inscricao__r.Amount,
                                     Chave_Bandeira__c, Bandeira__c, Forma__c, EmpresaSAP__c, Instituicao__r.CodigoBancoSAP__c, CodigoSAP__c, ExercicioSAP__c, TransacaoCartao__c, CodigoAutorizacao__c, Valor__c, Parcelado__c
                                FROM Titulos__c
                               WHERE Id = :tituloNew.Id ];*/

        String isMatriculaOnline = [SELECT Id, Name, ProcessoSeletivo__r.Matr_cula_Online__c FROM Titulos__c WHERE Id =: titulo.id limit 1].ProcessoSeletivo__r.Matr_cula_Online__c;
        string razao = [select id, Name, Instituicao__r.Raz_o__c from Titulos__c WHERE Id =: titulo.id limit 1].Instituicao__r.Raz_o__c;

        List < Titulos__c > tit = [select id, Name, Instituicao__r.CentroLucro__c, Instituicao__r.Centro_de_Lucro_Vestibular__c from Titulos__c WHERE Id =: titulo.id limit 1];
        String centroLucro = tit[0].Instituicao__r.CentroLucro__c;
        string centroLucroVest = tit[0].Instituicao__r.Centro_de_Lucro_Vestibular__c != null ? tit[0].Instituicao__r.Centro_de_Lucro_Vestibular__c : '';
        system.debug('TituloJson: ' + titulo);
        system.debug('Cliente_Sap__c: ' + titulo.Cliente_Sap__c);
        Date dtHoje = Date.today();
        String year = String.valueOf(dtHoje.year());
        String month = String.valueOf(dtHoje.month()).length() == 2 ? String.valueOf(dtHoje.month()) : '0' + String.valueOf(dtHoje.month());
        String day = String.valueOf(dtHoje.day()).length() == 2 ? String.valueOf(dtHoje.day()) : '0' + String.valueOf(dtHoje.day());
        String mesVencimento = String.valueOf(titulo.DataVencimento__c.month()).length() == 2 ? String.valueOf(titulo.DataVencimento__c.month()) : '0' + String.valueOf(titulo.DataVencimento__c.month());

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('AccountingDataAcc_Document');
        gen.writeStartArray();
        gen.writeStartObject();


        gen.writeStringField('REF_DOC_NO', titulo.Id);
        if (Test.isRunningTest()) {
            gen.writeStringField('COMP_CODE', '4');
        } else {
            gen.writeStringField('COMP_CODE', titulo.Instituicao__r.CodigoSAP__c);
        }

        gen.writeStringField('DOC_DATE', year + month + day);
        gen.writeStringField('DOC_TYPE', 'DN');
        gen.writeDateField('PSTNG_DATE', titulo.DataVencimento__c);
        gen.writeStringField('FIS_PERIOD', mesVencimento);
        gen.writeStringField('CURRENCY', 'BRL');

        gen.writeFieldName('CustomerItem');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField('REF_DOC_NO', titulo.Id);
        gen.writeStringField('ITEMNO_ACC', '0000000001');

        if (isMatriculaOnline == 'Sim') {
            gen.writeStringField('ALLOC_NMBR', 'Curso de extensão');
            gen.writeStringField('CUSTOMER', titulo.Cliente_Sap__c == null ? '' : titulo.Cliente_Sap__c);
            gen.writeStringField('HEADER_TXT', 'Curso de extensão');
        } else {
            gen.writeStringField('ALLOC_NMBR', 'VESTIBULAR');
            gen.writeStringField('HEADER_TXT', 'VESTIBULAR');
            gen.writeStringField('CUSTOMER', '9900000004');
        }
        if (Test.isRunningTest()) {
            gen.writeStringField('BUSINESSPLACE', '4');
        } else {
            gen.writeStringField('BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c);
        }

        // gen.writeStringField( 'BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c ); 

        gen.writeStringField('REF_KEY_3', titulo.Name);
        gen.writeStringField('BLINE_DATE', year + month + day);

        if (Test.isRunningTest()) {
            gen.writeStringField('DSCT_DAYS1', String.valueOf(5));
        } else {
            gen.writeStringField('DSCT_DAYS1', String.valueOf(titulo.DiasParaVencimento__c));
        }


        gen.writeStringField('DSCT_PCT1', '00000');
        gen.writeStringField('DSCT_DAYS2', '0');
        gen.writeStringField('DSCT_PCT2', '00000');
        gen.writeStringField('ZINKZ', 'A');
        gen.writeStringField('PYMT_METH', 'D');

        if (Test.isRunningTest()) {
            gen.writeStringField('BANK_ID', '4');
        } else {
            gen.writeStringField('BANK_ID', titulo.Instituicao__r.Banco__c);
        }


        //gen.writeStringField( 'BANK_ID', titulo.Instituicao__r.Banco__c );
        if (!String.isBlank(titulo.Instituicao__r.Conta_Caixa__c)) {
            gen.writeStringField('HOUSEBANKACCTID', titulo.Instituicao__r.Conta_Caixa__c);
        } else {
            gen.writeStringField('HOUSEBANKACCTID', 'CC001');
        }


        gen.writeStringField('MATRICULA', ' ');

        if (Test.isRunningTest()) {
            gen.writeStringField('ITEM_TEXT', 'teste');
        } else {
            gen.writeStringField('ITEM_TEXT', titulo.ProcessoSeletivo__r.Name);
        }


        gen.writeStringField('SHKZG', 'D');

        if (Test.isRunningTest()) {
            gen.writeStringField('PYMT_AMT', String.valueOf(4));
        } else {
            //gen.writeStringField( 'PYMT_AMT', String.valueOf( titulo.Inscricao__r.Amount ) );
            gen.writeStringField('PYMT_AMT', String.valueOf(titulo.Valor__c));
        }

        // gen.writeStringField( 'PYMT_AMT', String.valueOf( titulo.Inscricao__r.Amount ) );
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeFieldName('AccountItem');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField('REF_DOC_NO', titulo.Id);
        gen.writeStringField('ITEMNO_ACC', '0000000002');
        //gen.writeStringField( 'GL_ACCOUNT', '0000112022' );

        if (isMatriculaOnline == 'Sim') {
            gen.writeStringField('GL_ACCOUNT', razao);
        } else {
            gen.writeStringField('GL_ACCOUNT', '0000112022');
        }

        if (Test.isRunningTest()) {
            gen.writeStringField('BUSINESSPLACE', '43');
        } else {
            gen.writeStringField('BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c);
        }


        //gen.writeStringField( 'BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c ); 
        if (Test.isRunningTest()) {
            gen.writeStringField('PROFIT_CTR', '54');
        } else {
            if (isMatriculaOnline == 'Sim') {
                gen.writeStringField('PROFIT_CTR', centroLucroVest);
            } else {
                gen.writeStringField('PROFIT_CTR', '1010102002');
            }
        }


        //gen.writeStringField( 'PROFIT_CTR', titulo.Instituicao__r.CentroLucro__c );
        gen.writeStringField('ALLOC_NMBR', ' ');

        if (Test.isRunningTest()) {
            gen.writeStringField('ITEM_TEXT', 'teste');
        } else {
            gen.writeStringField('ITEM_TEXT', titulo.ProcessoSeletivo__r.Name);
        }

        // gen.writeStringField( 'ITEM_TEXT', titulo.ProcessoSeletivo__r.Name );
        gen.writeStringField('SHKZG', 'C');

        if (Test.isRunningTest()) {
            gen.writeStringField('AMT_DOCCUR', String.valueOf(5));
        } else {
            //gen.writeStringField( 'AMT_DOCCUR', String.valueOf( titulo.Inscricao__r.Amount ) );
            gen.writeStringField('AMT_DOCCUR', String.valueOf(titulo.Valor__c));
        }
        //gen.writeStringField( 'AMT_DOCCUR', String.valueOf( titulo.Inscricao__r.Amount ) );
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeEndObject();

        return gen.getAsString();
    }

    /*
Cria o JSON do cliente criado no Salesforce 
*/
    /*public static String createClientToJson(String  opp){
        //Retorna a conta pessoal referente à inscrição
        Opportunity oppWithAcct = [SELECT Id, Account.id, Account.Rua__c, Account.name, Account.nacionalidade__c, Account.Sexo__c, Account.Complemento__c, Account.N_mero__c,
                                    Account.CEP__c, Account.Cidade__c, Account.Estado__c, Account.Phone, Account.PersonMobilePhone, Account.PersonEmail, C_digo_da_Coligada__c, CPF__c
                                    FROM Opportunity where Opportunity.id =: opp];

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('ClienteSAP');
        gen.writeStartArray();          
        gen.writeStartObject();          
        
        //CAMPOS DO JSON PARA O CLIENTE SAP
        gen.writeStringField( 'NOME', oppWithAcct.Account.Name);
        gen.writeStringField( 'KUNNR', 'PERGAMUM'); //FIXO

            /* VALORES PARA KTOKD - DEFINIR IF PARA CADA CASO
            DB01 – Brasileiro Pessoa
            Física
            DB02 – Estrangeiro Pessoa
            Física
            DB03 – Pessoa Jurídica;
            

        gen.writeStringField( 'KTOKD', 'DB01');

        //SENHOR OU SENHORA
        if(oppWithAcct.Account.Sexo__c == 'Masculino'){
            gen.writeStringField( 'ANRED', 'SENHOR'); 
        }else{
             gen.writeStringField( 'ANRED', 'SENHORA'); 
        }

        gen.writeStringField( 'STREET', oppWithAcct.Account.Rua__c); 
        gen.writeStringField( 'HOUSENO', oppWithAcct.Account.N_mero__c); 
        gen.writeStringField( 'STRSUPPL1', oppWithAcct.Account.Complemento__c);
        gen.writeStringField( 'ORT02',''); //CAMPO FALTANDO  
        gen.writeStringField( 'PSTLZ', oppWithAcct.Account.CEP__c); 
        gen.writeStringField( 'ORT01', oppWithAcct.Account.Cidade__c);
        gen.writeStringField( 'REGIO', oppWithAcct.Account.Estado__c); 
        gen.writeStringField( 'TEL1NUMBR',oppWithAcct.Account.Phone); 
        gen.writeStringField( 'TEL1TEXT',''); //CAMPO FALTANDO
        gen.writeStringField( 'MOBNUMBER', oppWithAcct.Account.PersonMobilePhone); 
        gen.writeStringField( 'FAXNUMBER',''); //CAMPO FALTANDO
        gen.writeStringField( 'FAXEXTENS',''); //CAMPO FALTANDO
        gen.writeStringField( 'EMAIL', oppWithAcct.Account.PersonEmail); 
        gen.writeStringField( 'STCD1', '');//PARA ALUNO NÃO É NECESSÁRIO CNPJ
        gen.writeStringField( 'STCD2', oppWithAcct.CPF__c);   
        gen.writeStringField( 'ALUNO', '1');// 1- ALUNO 0- FUNCIONÁRIO
        gen.writeStringField( 'CODCOLIGADA', oppWithAcct.C_digo_da_Coligada__c);


        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeEndArray();
        gen.writeEndObject(); 
        return gen.getAsString();
    }*/

    public static String createClientToJson(String opp) {
        //Retorna a conta pessoal referente à inscrição
        Opportunity oppWithAcct = [SELECT Id, Account.id, Account.Rua__c, Account.Name, Account.Nacionalidade__c,
            Account.Sexo__c, Account.Complemento__c, Account.N_mero__c, Account.CEP__c,
            Account.Cidade__c, Account.Estado__c, Account.Phone, Account.PersonMobilePhone,
            Account.PersonEmail, C_digo_da_Coligada__c, CPF__c
            FROM Opportunity
            WHERE Opportunity.id =: opp
        ];

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('cliente');
        //gen.writeStartArray();
        gen.writeStartObject();
        //CAMPOS DO JSON PARA O CLIENTE SAP
        gen.writeStringField('nome', oppWithAcct.Account.Name);
        gen.writeStringField('kunnr', 'SALESFORCE'); //FIXO

        /* VALORES PARA KTOKD - DEFINIR IF PARA CADA CASO
        DB01 – Brasileiro Pessoa
        Física
        DB02 – Estrangeiro Pessoa
        Física
        DB03 – Pessoa Jurídica;
        */

        if (oppWithAcct.Account.nacionalidade__c == 'Brasileiro') {
            gen.writeStringField('ktokd', 'DB01');
        } else {
            gen.writeStringField('ktokd', 'DB02');
        }

        //SENHOR OU SENHORA
        if (oppWithAcct.Account.Sexo__c == 'Masculino') {
            gen.writeStringField('anred', 'SENHOR');
        } else {
            gen.writeStringField('anred', 'SENHORA');
        }
        gen.writeStringField('street', oppWithAcct.Account.Rua__c == null ? '' : oppWithAcct.Account.Rua__c);
        gen.writeStringField('houseno', oppWithAcct.Account.N_mero__c == null ? '' : oppWithAcct.Account.N_mero__c);
        gen.writeStringField('strsuppl1', oppWithAcct.Account.Complemento__c == null ? '' : oppWithAcct.Account.Complemento__c);
        gen.writeStringField('ort02', ''); //CAMPO FALTANDO  
        gen.writeStringField('pstlz', oppWithAcct.Account.CEP__c == null ? '' : oppWithAcct.Account.CEP__c);
        gen.writeStringField('ort01', oppWithAcct.Account.Cidade__c == null ? '' : oppWithAcct.Account.Cidade__c);
        gen.writeStringField('regio', oppWithAcct.Account.Estado__c == null ? '' : oppWithAcct.Account.Estado__c);
        gen.writeStringField('tel1numbr', oppWithAcct.Account.Phone == null ? '' : oppWithAcct.Account.Phone);
        gen.writeStringField('tel1text', ''); //CAMPO FALTANDO
        gen.writeStringField('mobnumber', oppWithAcct.Account.PersonMobilePhone == null ? '' : oppWithAcct.Account.PersonMobilePhone);
        gen.writeStringField('faxnumber', ''); //CAMPO FALTANDO
        gen.writeStringField('faxextens', ''); //CAMPO FALTANDO
        gen.writeStringField('email', oppWithAcct.Account.PersonEmail == null ? '' : oppWithAcct.Account.PersonEmail);
        gen.writeStringField('stcd1', ''); //PARA ALUNO NÃO É NECESSÁRIO CNPJ
        gen.writeStringField('stcd2', oppWithAcct.CPF__c == null ? '' : oppWithAcct.CPF__c.replaceAll('[|.|-]', ''));
        gen.writeStringField('aluno', '1'); // 1- ALUNO 0- FUNCIONÁRIO
        gen.writeStringField('codcoligada', oppWithAcct.C_digo_da_Coligada__c == null ? '' : oppWithAcct.C_digo_da_Coligada__c);

        gen.writeEndObject();
        //gen.writeEndArray();
        gen.writeEndObject();

        system.debug('gen.getAsString(): ' + gen.getAsString());

        return gen.getAsString();
    }



}