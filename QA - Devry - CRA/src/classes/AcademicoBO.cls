/*
    @author Diego Moreira
    @class Classe de negocio do objeto informações academicas
*/
public with sharing class AcademicoBO {
	/*
        Singleton
    */
    private static final AcademicoBO instance = new AcademicoBO();    
    private AcademicoBO(){}
    
    public static AcademicoBO getInstance() {
        return instance;
    } 

    /*
		Atualiza fase de inscrição para matriculado na criação de informações academicas 
		@param academicosList Lista de registros criados
    */
    public void atualizaFaseInscricao( List<Academico__c> academicosList ) {
    	List<Opportunity> oportunityToUpdate = new List<Opportunity>();

    	for( Academico__c academico : academicosList )
    		oportunityToUpdate.add( new Opportunity( Id = academico.Oportunidade__c, StageName = 'Matriculado' ) );

    	OpportunityDAO.getInstance().updateData( oportunityToUpdate );	
    }
}