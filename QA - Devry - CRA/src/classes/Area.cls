public with sharing class Area {
	
	private String Name{get;private set;}
	private Decimal Value{get;private set;}

	public String getName(){
		return Name;
	}

	public Decimal getValue(){
		return Value;
	}
}