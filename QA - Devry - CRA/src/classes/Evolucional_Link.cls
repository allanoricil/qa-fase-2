public class Evolucional_Link {

    public static void processingQueue( String queueId, String payload ) {
        syncToServer( queueId, payload );
    }

    
    @future(Callout = true)
    public Static void syncToServer(String queueId, String payload){

            HttpRequest req = new HttpRequest();
            
            req.setEndpoint('http://api.evolucional.com.br/simulado-online/usuario/single-sign-on');
            
            req.setMethod( 'POST' );
            
            req.setHeader( 'Content-Type', 'application/json' );
            
            req.setTimeout( 120000 );
            
            req.setBody( payload );
            
            try {
                Http h = new Http();
                HttpResponse res = h.send( req );
                if( res.getStatusCode() == 200 )
                    processJsonBodyLink( queueId, res.getBody() );                                                                                        
                else
                   QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_LINK / ' + res.getStatusCode() + ' / ' + res.getStatus() );
            } catch ( CalloutException ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'EVOLUCIONAL_LINK / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
   }


    public Static void updateOpp(Opportunity opp, String link){
    	opp.Link__c = link;
    	OpportunityDAO.getInstance().updateData(opp);
    }

    //
   private static void processJsonBodyLink(String queueId, String jsonBody){

   		String errorMessage = '';

        Queue__c fila = [Select Oportunidade__c From Queue__c where Id =: queueId];

        Opportunity opp = [SELECT Name FROM Opportunity where Id =: fila.Oportunidade__c];

        Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(jsonBody);
        String acesso = JSON.serialize(cObjMap.get('ResultAccessUrl'));
        /*
        Queue__c fila = [Select id, PayloadSAP__c from Queue__c where id='a0h2F0000000B3G'];
Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(fila.PayloadSAP__c);
String acesso = JSON.serialize(cObjMap.get('AssessmentList'));
system.debug(acesso);
        */


        updateOpp(opp, acesso);     

        QueueBO.getInstance().updateQueue( queueId, errorMessage );
       
   }


}