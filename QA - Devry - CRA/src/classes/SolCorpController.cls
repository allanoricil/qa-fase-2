public with sharing class SolCorpController {

    /* Listas */
    
    public List<SelectOption> processoSeletivoOptions   { get; set; }
    public List<SelectOption> ofertaOptions             { get; set; }
    public List<SelectOption> estadoOptions             { get; set; }
    public List<SelectOption> paisOptions               { get; set; }

    public String opportunityId                         { get; set; }
    
     private static String RECORDTYPE_FICHA_SONHOS = Schema.SObjectType.Account.getRecordTypeInfosByName().get('DNA do Aluno').getRecordTypeId();
    public String processo                              { get; set; }
    public String curso                                 { get; set; }
    
    /*Informações Pessoais */

    public String nome                                  { get; set; }
    public String sobrenome                             { get; set; }
    public String dataNascimento                        { get; set; }
    public String sexo                                  { get; set; }
    public String naturalidade                          { get; set; }
    public String nacionalidade                         { get; set; }
    public String estadoCivil                           { get; set; } 
    public String cpf                                   { get; set; }
    public String rg                                    { get; set; }
    public String orgaoEmissor                          { get; set; }
    public String uf                                    { get; set; }
    public String dataEmissao                           { get; set; }
   // public String nomeMae                               { get; set; }
    
    public String dtNasc                                {get;set;}
    /* Informações de Contato */

    public String email                                 { get; set; }
    public String celular                               { get; set; }
    public String telefone                              { get; set; }
    public String cep                                   { get; set; }
    public String cidade                                { get; set; }
    public String estado                                { get; set; }
    public String bairro                                { get; set; }
    public String rua                                   { get; set; }
    public String numero                                { get; set; }
    public String complemento                           { get; set; }
    public String SitProf                                {get;set;}
    /* Profissão */

    public String profissao                              { get; set; }
    public String cargo                                  { get; set; }
    public String empresa                                { get; set; }
    public String tempoExperiencia                       { get; set; }

    /* Formação Acadêmica */

    public String escolaridade                           { get; set; }
    public String escolaridade_dois                      { get; set; }
    public String nomeIES                                { get; set; }
    public String nomeIES_dois                           { get; set; }
    public String curso_um                               { get; set; }
    public String curso_dois                             { get; set; }
    public String ano_conclusao                          { get; set; }
    public String ano_conclusao_dois                     { get; set; }
    public String pageLanguage{
        get{
            String lang = ApexPages.currentPage().getParameters().get('lang');
            if( lang != null && lang.length() > 0){
                return lang;
            }
            return 'pt_BR';
        }
        set;
    }

        public SolCorpController() {
        getNaturalidade();
        getNacionalidade();
        getProcessoSeletivo();
    }

   
    /*
    public SolCorpController(ApexPages.StandardController controller) {
        getNaturalidade();
        getNacionalidade();
        getProcessoSeletivo();
    }
*/
    public List<SelectOption> getNaturalidade(){
        List<Estados__c> estados = [Select Id,Name from Estados__c order by Name];
        estadoOptions = new List<SelectOption>();
        for(Estados__c es : estados)
        estadoOptions.add(new SelectOption(es.Id, es.Name));

        return estadoOptions;
    }
    public List<SelectOption> getNacionalidade(){
        List<Countries__c> paises = [Select Id,Name  from Countries__c  Where (NOT Name = 'Brasil') order by Name];
        paisOptions = new List<SelectOption>();
        for(Countries__c co : paises)
        paisOptions.add( new SelectOption (co.Id, co.Name));

        return paisOptions;
    }

    public List<SelectOption> getProcessoSeletivo(){
        List<Processo_seletivo__C> processos = [SELECT Id, Name FROM Processo_seletivo__c 
        WHERE Periodo_Letivo__c != null and Formul_rio_Salesforce__c = true and Ativo__c = true and
        Periodo_Letivo__c IN ('SOLCORP','EXT') and
        Inscri_o__c = true and Data_de_encerramento__c >= :system.today()];
        
        processoSeletivoOptions = new List<SelectOption>();

        for (Processo_seletivo__c processo : processos)
        processoSeletivoOptions.add(new SelectOption( processo.Id, processo.Name));
        
        return processoSeletivoOptions;
    }
    public PageReference getOferta(){
        String processoId = apexpages.currentpage().getparameters().get('processoSeletivo');
        List<Oferta__c> ofertas = [Select Id, Name from Oferta__C Where Processo_seletivo__c=:processoId];
        
        ofertaOptions = new List<SelectOption>();
        for(Oferta__c oferta : ofertas){
            ofertaOptions.add(new SelectOption( oferta.Id, oferta.Name));
        }

        return null;
    }

   
    public Opportunity CreateOpp()
    {   
        try{  

            Processo_seletivo__c processoSeletivo = [Select Id, Name,Valor_Taxa_Inscricao__c, CobrarTaxaInscricao__c, Data_Validade_Processo__c, Campus__c, Institui_o_n__c, Tipo_do_curso_oferecido__c,Institui_o_n__r.Name from Processo_seletivo__c where id=: processo];
            Opportunity  mOpp  = new Opportunity();
		    mOpp.Amount =  getOfertaValor(curso);
            mOpp.name  = processoSeletivo.Name + ' - ' + processoSeletivo.Data_Validade_Processo__c;
            mOpp.Primeiro_Nome__c = nome;
			mOpp.CPF__c = cpf;
			mOpp.RG__c=rg;
			mOpp.Nacionalidade__c= nacionalidade;
			mOpp.Naturalidade__c=naturalidade;
            mOpp.Sobrenome__c = sobrenome;
            mOpp.X1_OpcaoCurso__c = curso;
            mOpp.Estado_Civil__c = estadoCivil;
            mOpp.Origem_RG__c = orgaoEmissor;
            mOpp.Cidade_UF__c = uf;
            mOpp.Data_Nascimento__c= Date.parse(dtNasc); 
            mOpp.RecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Soluções Corporativas' ).getRecordTypeId();
           //mOpp.RecordTypeId =OpportunityDAO.RECORDTYPE_GRADUACAO;
             System.debug(' mOpp.RecordTypeId ' + mOpp.RecordTypeId);
            mOpp.Data_de_Emiss_o_do_RG__c = Date.parse(dataEmissao);
            System.debug('data ' + dataEmissao+'  dt parse'+Date.parse(dataEmissao));
           // mOpp.Nome_da_Mae__c =nomeMae ;
            mOpp.Celular__c = celular;
            mOpp.CEP__c =cep ;
            mOpp.Email__c = email;
            mOpp.Telefone__c = telefone;
            mOpp.Cidade__c =cidade ;
            mOpp.Estado__c = estado;
            mOpp.Bairro__c = bairro;
            mOpp.Rua__c =rua ;
            mOpp.N_mero__c = numero;
            mOpp.Complemento__c =complemento ;
            mOpp.Confirma_o_Encontro_com_a_Profiss_o__c =profissao ;
            mOpp.Cargo__c = cargo;
            mOpp.Empresa__c =empresa ;
            mOpp.Tempo_no_cargo__c = tempoExperiencia;
            mOpp.Escolaridade__c = escolaridade;
            mOpp.Institui_o_de_P_s_Gradua_o__c = nomeIES;
            mOpp.Curso_p_s_gradua_o__c = curso_um;
            mOpp.Ano_de_conclus_o_de_p_s_gradua_o__c=ano_conclusao;
            mOpp.OwnerId  = userinfo.getUserId();
            mOpp.CloseDate = system.today();
            mOpp.Processo_seletivo__c  = processo;
            mOpp.StageName='Inscrito';
            
        return mOpp;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
            throw e;
        } 
        
    }
     private Account contaExiste(String cpf) {
        system.debug('CPF:::'+ cpf);
        List<Account> listAccount = AccountDAO.getInstance().getAccountByCpf( cpf );
        system.debug('Lista de Contas:::'+listAccount);
        if( listAccount.size() > 0 ) 
            return listAccount[0];
        
        return null;  
    }
    private Boolean oportunidadeDuplicada() {
        List<Opportunity> listOpportunity = OpportunityDAO.getInstance().getDuplicateOpportunityByCPF( processo, curso, cpf);
        system.debug('Lista de Oportunidades:::'+ listOpportunity);
        if( listOpportunity.size() > 0 ) {
            return true;
        }
        return false;    
    }
    public PageReference SaveAll(){
        
          Account acnt=contaExiste(cpf);
          Opportunity newOpportunity=CreateOpp();
        system.debug('Valor da opp:::'+newOpportunity);
        if(acnt==null)
           return createAccount(newOpportunity) ;
        else if( !oportunidadeDuplicada() ) {
             newOpportunity.AccountId= acnt.id;
             insert newOpportunity;
              return mensagemFinal();            
        }
        else {
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Já existe a inscrição para este processo seletivo!' );
                Apexpages.addMessage( msg );
            }
            
            
        return null;
        
    }

     private PageReference createAccount(Opportunity newOpportunity) {
      
        Account accountToInsert = new Account(
            RecordTypeId = RECORDTYPE_FICHA_SONHOS,
            FirstName =newOpportunity.Primeiro_Nome__c,
           LastName = newOpportunity.Sobrenome__c==''?newOpportunity.Primeiro_Nome__c:newOpportunity.Sobrenome__c,
            //LastName = newOpportunity.Name,
            DataNascimento__c = newOpportunity.Data_Nascimento__c,
            Sexo__c = newOpportunity.Sexo__c,
            Nacionalidade__c = newOpportunity.Nacionalidade__c,
            Naturalidade__c = newOpportunity.Naturalidade__c,
            Estado_Civil__c = newOpportunity.Estado_Civil__c,
            CPF_2__c = newOpportunity.CPF__c,
            RG__c = newOpportunity.RG__c,
            RG_Data_de_Emiss_o__c = newOpportunity.Data_de_Emiss_o_do_RG__c,
            Origem_RG__c = newOpportunity.Origem_RG__c,
            Origem_RG_UF__c = newOpportunity.Cidade_UF__c,
            Nome_da_Mae__c = newOpportunity.Nome_da_Mae__c,
            PersonEmail = newOpportunity.Email__c,
            PersonMobilePhone = newOpportunity.Celular__c,
            Phone = newOpportunity.Telefone__c,
            CEP__c = newOpportunity.CEP__c,
            Cidade__c = newOpportunity.Cidade__c,
            Estado__c = newOpportunity.Estado__c,
            Bairro__c = newOpportunity.Bairro__c,
            Rua__c = newOpportunity.Rua__c,
            N_mero__c = newOpportunity.N_mero__c,
            Complemento__c = newOpportunity.Complemento__c,
            OwnerId = userinfo.getUserId()
        );        
        system.debug('Conta a Ser Inserida - Preenchida:::'+accountToInsert);
        Savepoint sp = Database.setSavepoint();
        try {
            insert accountToInsert;
            system.debug('Conta a Ser Inserida - Inserida:::'+accountToInsert);
            system.debug('ID da Conta inserida:::'+accountToInsert.Id);
            newOpportunity.AccountId= accountToInsert.id;
            system.debug('oportunidade:::'+newOpportunity);
            insert newOpportunity;
            return mensagemFinal();
           // return createOpportunity( accountToInsert.Id );            
        } catch ( DmlException ex ) {
              system.debug('except :::'+ex);
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, ex.getDmlMessage(0) ); 
            Apexpages.addMessage( msg );
            Database.rollback(sp);            
            ApexPages.Message msge = new ApexPages.Message( ApexPages.Severity.error,ex.getMessage() );
            Apexpages.addMessage( msge );
            throw ex;
        }
    }

    private Boolean contaDuplicada(String cpf) {
        List<Account> accountDuplicate = AccountDAO.getInstance().getAccountByCpf(cpf);
        if( accountDuplicate.size() > 0 ) {
            return true;
        }
        return false;  
    }



    public Decimal getOfertaValor(String ofertaId) {
        Decimal retorno;
        if (ofertaId != null) {
            retorno = [Select Id, Valor_da_Matr_cula__c from Oferta__c where Id = :ofertaId limit 1].Valor_da_Matr_cula__c;          
        } 
        return retorno;
    }
    public void atualizaOportunidade(String opportunityId) {
        try {
            Opportunity mOpp = [Select Id, CPF__c, Amount, Account.FirstName, Account.LastName, Oferta__c,Data_Nascimento__c from Opportunity Where Id = : opportunityId ];
            mOpp.Amount =  getOfertaValor(curso);
            mOpp.Primeiro_Nome__c = mOpp.Account.FirstName;
            mOpp.Sobrenome__c = mOpp.Account.LastName;
            mOpp.X1_OpcaoCurso__c = mOpp.Oferta__c;
            mOpp.Estado_Civil__c = estadoCivil;
            mOpp.Origem_RG__c = orgaoEmissor;
            mOpp.Cidade_UF__c = uf;
            mOpp.Data_Nascimento__c= Date.parse(dtNasc); 
             System.debug('data ' + dtNasc+'  dt parse'+Date.parse(dtNasc));
            mOpp.Data_de_Emiss_o_do_RG__c = Date.parse(dataEmissao);
            System.debug('data ' + dataEmissao+'  dt parse'+Date.parse(dataEmissao));
           // mOpp.Nome_da_Mae__c =nomeMae ;
            mOpp.Celular__c = celular;
            mOpp.CEP__c =cep ;
            mOpp.Email__c = email;
            mOpp.Telefone__c = telefone;
            mOpp.Cidade__c =celular ;
            mOpp.Estado__c = estado;
            mOpp.Bairro__c = bairro;
            mOpp.Rua__c =rua ;
            mOpp.N_mero__c = numero;
            mOpp.Complemento__c =complemento ;
            mOpp.Confirma_o_Encontro_com_a_Profiss_o__c =profissao ;
            mOpp.Cargo__c = cargo;
            mOpp.Empresa__c =empresa ;
            mOpp.Tempo_no_cargo__c = tempoExperiencia;
            mOpp.Escolaridade__c = escolaridade;
            mOpp.Institui_o_de_P_s_Gradua_o__c = nomeIES;
            mOpp.Curso_p_s_gradua_o__c = curso_um;
            mOpp.Ano_de_conclus_o_de_p_s_gradua_o__c=ano_conclusao;
            update mOpp;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
    }
    public PageReference mensagemFinal(){
         PageReference  pageRef = new PageReference('/apex/FinishPage');
         pageRef.setRedirect(true);
         return pageRef;
    } 

}