/*
    @author Diego Moreira
    @class Classe de negocio de atualização de baixa de titulos
*/
public class SRM_StatusDocumentoBO { 
	/*
		Metodo principal de execução
    */
    @future( Callout=true )
    public static void execute() {
    	ProcessorControl.inFutureContext = true;
       	Map<String, Object> postData = new Map<String, Object>();
       	List<Titulos__c> titulos;
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null ) {
    		 try {
    			postData = getStatusDocumento( mapToken.get('200'), getJsonRequest() );
    			if( postData.size() > 0 ) titulos = processaDocumento( postData );
    			System.debug( '>>> ' + postData );
    			if( !titulos.isEmpty() ) retornaStatusDocumento( mapToken.get('200'), getJsonRequest( titulos ) );
    			System.debug( '>>> ' + titulos );
    		} catch ( CalloutException ex ) {}
    			catch ( Exception ex ) {} 
    	}
    }
  
    /*
		Faz a pesquisa nos status do titulo
		@param token chave de acesso ao serviço
    */
    private static Map<String, Object> getStatusDocumento( String token, String jsonRequest ) {
    	Map<String, Object> postData = new Map<String, Object>();

    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues( 'SAP-StatusDocumento' ).Endpoint__c );
        req.setMethod( 'POST' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-StatusDocumento' ).API_Token__c );
        req.setHeader( 'AUTH-TOKEN', token );
        req.setTimeout( 120000 );
        req.setBody( jsonRequest );

        Http h = new Http();
        HttpResponse res = h.send( req );

        if( res.getStatusCode() == 200 )
    		postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );			        							               					          

        System.debug( '>>> getStatusDocumento ' + res.getBody() );
        return postData;
    } 

    /*
		Processa os dados de retorno
		@postData retorno do serviço
    */
    private static List<Titulos__c> processaDocumento( Map<String, Object> postData ) {
    	List<Titulos__c> titulosToUpdate = new List<Titulos__c>();
        Map<String, Object> statusDocumento = ( Map<String, Object> ) postData.get( 'GetStatusDocumentoResult' );
        List<Object> objMessage = ( List<Object> ) statusDocumento.get( 'messagesField' );
        Map<String, Object> message = ( Map<String, Object> ) objMessage[0];

        if( String.valueOf( message.get('tIPOField') ).equals( 'S' ) ) {
        	for( Object listResult : ( List<Object> ) statusDocumento.get( 'acc_DocumentField' ) ) {
    			Map<String, Object> documentField = ( Map<String, Object> ) listResult;
    			
                System.debug( 'documentField>>> ' + documentField );

	      		String codigoSAP = String.valueOf( documentField.get( 'bELNRField' ) );
                String empresaSAP = String.valueOf( documentField.get( 'cOMP_CODEField' ) );
	      		String status = String.valueOf( documentField.get('aCTIONField') );
	      		List<Titulos__c> titulos = TituloDAO.getInstance().getTitulosByCodigoSAP( codigoSAP, empresaSAP );
	      		if( titulos.size() > 0 ) {
	      			Titulos__c titulo = titulos[0];
	      			titulo.Status__c = status.equals( 'L' ) ? 'Pago' : 'Cancelado';
	      			titulosToUpdate.add( titulo );
	      		}
          	}
        }
      	update titulosToUpdate;
      	return titulosToUpdate;
    }

    /*
		Retorna o dado processado
		@param token chave de acesso ao serviço
    */
    private static void retornaStatusDocumento( String token, String jsonRequest ) {
    	System.debug('>>> jsonRequest ' + jsonRequest);
    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues( 'SAP-RetornaStatusDocumento' ).Endpoint__c );
        req.setMethod( 'POST' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-RetornaStatusDocumento' ).API_Token__c );
        req.setHeader( 'AUTH-TOKEN', token );
        req.setTimeout( 120000 );
        req.setBody( jsonRequest );
 		
        Http h = new Http();
        HttpResponse res = h.send( req );
    }

    /*
		Retorna o Json 
    */
    private Static String getJsonRequest() {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'IdSistema', '002' ); 
        gen.writeStringField( 'QtdeActions', '20' ); 
        gen.writeEndObject(); 

    	return gen.getAsString(); 
    }

    /*
		Retorna o Json 
		@param titulo objeto atualizado para retorno
    */
    private Static String getJsonRequest( List<Titulos__c> titulos ) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('LDocumentoRetorno');
            gen.writeStartArray();
            	for( Titulos__c titulo : titulos ) {
            		gen.writeStartObject();
         			gen.writeStringField( 'ACTION', 'L' ); 
        			gen.writeStringField( 'BUKRS', titulo.EmpresaSAP__c ); 
        			gen.writeStringField( 'BELNR', titulo.CodigoSAP__c ); 
        			gen.writeStringField( 'GJAHR', titulo.ExercicioSAP__c ); 
        			gen.writeFieldName('ITENS_DOC');
			            gen.writeStartArray();          
			                gen.writeStartObject();
			                	gen.writeStringField( 'BUZEI', '001' ); 
			        			gen.writeStringField( 'ID_MSG', 'S' ); 
			        			gen.writeStringField( 'MSG', 'Processado com sucesso' );
			                gen.writeEndObject();  
            			gen.writeEndArray(); 
        			gen.writeEndObject();	
            	}            
            gen.writeEndArray();
        gen.writeEndObject();  

    	return gen.getAsString(); 
    }
}