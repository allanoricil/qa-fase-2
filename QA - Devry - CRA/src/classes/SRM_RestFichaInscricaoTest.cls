@isTest
private class SRM_RestFichaInscricaoTest {

  static {
  }


    static testMethod void testDoGetTrue(){

      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/fichainscricao/';  

      req.httpMethod = 'GET';
      req.addParameter('object', 'Account');  
      req.addParameter('fields','Id,Name,Phone');


      RestContext.request = req;
      RestContext.response = res;

      String jsonResult = SRM_RestFichaInscricao.doGet();

      System.debug(jsonResult);
  }



  static testMethod void testDoGetFalse(){

        //do request
      RestRequest req = new RestRequest(); 
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/v1/fichainscricao/';  


      req.httpMethod = 'GET';
      req.addParameter('object', 'Account__c');


      RestContext.request = req;
      RestContext.response = res;

      String jsonResult = SRM_RestFichaInscricao.doGet();

  }
    
  
  
  static testMethod void testDoPostFalse(){


    Lead l = new Lead();

        l.firstname='Wain';
        l.lastname='Rolen';
        l.company='Test Company';
        l.email='Wain@test.com';
        
        String jsonResult= JSON.serialize(l);

        system.debug('------->' + jsonResult);

        RestRequest req = new RestRequest();
        req.addHeader('Content-Type', 'application/json'); // Add a JSON Header as it is validated
        req.requestURI = '/services/apexrest/v1/fichainscricao/?event=FICHA_INSCRICAO';
        req.httpMethod = 'POST';
        
        req.requestBody = Blob.valueof(jsonResult); 

        system.debug('------->' + req.requestBody);

        RestResponse res = new RestResponse();

      RestContext.request = req;
      RestContext.response = res;

      String Result = SRM_RestFichaInscricao.doPost();

      system.debug('------->' + Result);
                   
  }


  static testMethod void testDoPostTrue(){

   JSONGenerator gen = JSON.createGenerator(true);
    gen.writeStartObject();
       gen.writeFieldName('FichaInscricao');
       gen.writeStartArray();      
       gen.writeStartObject();
         gen.writeStringField( 'IdConta', '' );
         gen.writeStringField( 'IdInscricao', '' );
         gen.writeStringField( 'IdProcessoSeletivo', 'a06J000000DbJAg' );
         gen.writeStringField( 'DataValidadeProcesso', '2015-01-01' );
         gen.writeStringField( 'IdOpcaoCurso1', 'a0LJ000000KFWkW' ); 
         gen.writeStringField( 'IdOpcaoCurso2', '' );
         gen.writeStringField( 'FezProvaENEM', 'Não' );
         gen.writeStringField( 'QualNotaMediaENEM', '' );
         gen.writeStringField( 'AnoProvaENEM', '' );
         gen.writeStringField( 'LastName', 'teste' );
         gen.writeStringField( 'Nacionalidade', 'Brasileira' );
         gen.writeStringField( 'CPF', '830.922.409-52' );
         gen.writeStringField( 'Treineiro', 'Não' );
         gen.writeStringField( 'Sexo', 'Masculino' );
         gen.writeStringField( 'Telefone', '13 4123-1234' );
         gen.writeStringField( 'Celular', '13 4123-1234' );
         gen.writeStringField( 'Email', 'etste@teste.com' );
         gen.writeStringField( 'DataNascimento', '1983-03-28' );
         gen.writeStringField( 'Cep', '13800000' );
         gen.writeStringField( 'Rua', '' );
         gen.writeStringField( 'Numero', '' );
         gen.writeStringField( 'Complemento', '' );
         gen.writeStringField( 'Bairro', '' );
         gen.writeStringField( 'Cidade', '' );
         gen.writeStringField( 'Estado', '' );
         gen.writeStringField( 'TipoEscolaCursou', 'Estabelecimento privado' );
         gen.writeStringField( 'IdColegio', '' );
         gen.writeStringField( 'PortadorAtencaoEspecial', 'Não' );
         gen.writeStringField( 'PrecisaAtendimentoEspecial', '' );
         gen.writeStringField( 'IdCodigoPromocional', '' );
         gen.writeStringField( 'CanalInscricao', 'Rest' );
       gen.writeEndObject();                 
      gen.writeEndArray();
    gen.writeEndObject();  

    RestRequest req = new RestRequest();
    RestResponse res = new RestResponse();

    System.debug( '>>> ' + gen.getAsString() );
        
    String jsonResult = gen.getAsString();

        req.requestURI = '/services/apexrest/v1/fichainscricao/?event=FICHA_INSCRICAO';
        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(jsonResult);

        System.debug( '>>> ' + req.requestBody );

        String queueId = QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.FICHA_INSCRICAO.name(), jsonResult );
        Queue__c queue = QueueDAO.getInstance().getQueueById( queueId )[0];

        SRM_FichaInscricaoBO.processaFichaInscricao( queue.Id, queue.Payload__c );
        SRM_FichaInscricaoBO.processaFichaCaptacao( queue.Id, queue.Payload__c );           
  }

  
}