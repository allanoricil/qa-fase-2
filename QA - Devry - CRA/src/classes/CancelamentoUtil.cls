public with sharing class CancelamentoUtil {
	
	public static String createClientToJson(String alunoId) {
        Aluno__c aluno = [select id, Name, Conta__c, Institui_o_n__r.C_digo_da_Coligada__c from Aluno__c where id =: alunoId];

        Account conta = [Select id, Rua__c, Name, Nacionalidade__c, Sexo__c, Complemento__c,
            N_mero__c, CEP__c, Cidade__c, Estado__c, Phone, PersonMobilePhone,
            PersonEmail, CPF_2__c from Account where id =: aluno.Conta__c
        ];

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('cliente');
        //gen.writeStartArray();
        gen.writeStartObject();
        //CAMPOS DO JSON PARA O CLIENTE SAP
        gen.writeStringField('nome', conta.Name);
        gen.writeStringField('kunnr', 'SALESFORCE'); //FIXO

        /* VALORES PARA KTOKD - DEFINIR IF PARA CADA CASO
        DB01 – Brasileiro Pessoa
        Física
        DB02 – Estrangeiro Pessoa
        Física
        DB03 – Pessoa Jurídica;
        */

        if (conta.nacionalidade__c == 'Brasileiro') {
            gen.writeStringField('ktokd', 'DB01');
        } else {
            gen.writeStringField('ktokd', 'DB02');
        }

        //SENHOR OU SENHORA
        if (conta.Sexo__c == 'Masculino') {
            gen.writeStringField('anred', 'SENHOR');
        } else {
            gen.writeStringField('anred', 'SENHORA');
        }
        gen.writeStringField('street', conta.Rua__c == null ? '' : conta.Rua__c);
        gen.writeStringField('houseno', conta.N_mero__c == null ? '' : conta.N_mero__c);
        gen.writeStringField('strsuppl1', conta.Complemento__c == null ? '' : conta.Complemento__c);
        gen.writeStringField('ort02', ''); //CAMPO FALTANDO  
        gen.writeStringField('pstlz', conta.CEP__c == null ? '' : conta.CEP__c);
        gen.writeStringField('ort01', conta.Cidade__c == null ? '' : conta.Cidade__c);
        gen.writeStringField('regio', conta.Estado__c == null ? '' : conta.Estado__c);
        gen.writeStringField('tel1numbr', conta.Phone == null ? '' : conta.Phone);
        gen.writeStringField('tel1text', ''); //CAMPO FALTANDO
        gen.writeStringField('mobnumber', conta.PersonMobilePhone == null ? '' : conta.PersonMobilePhone);
        gen.writeStringField('faxnumber', ''); //CAMPO FALTANDO
        gen.writeStringField('faxextens', ''); //CAMPO FALTANDO
        gen.writeStringField('email', conta.PersonEmail == null ? '' : conta.PersonEmail);
        gen.writeStringField('stcd1', ''); //PARA ALUNO NÃO É NECESSÁRIO CNPJ
        gen.writeStringField('stcd2', conta.CPF_2__c == null ? '' : conta.CPF_2__c.replaceAll('[|.|-]', ''));
        gen.writeStringField('aluno', '1'); // 1- ALUNO 0- FUNCIONÁRIO
        gen.writeStringField('codcoligada', aluno.Institui_o_n__r.C_digo_da_Coligada__c == null ? '' : aluno.Institui_o_n__r.C_digo_da_Coligada__c);

        gen.writeEndObject();
        //gen.writeEndArray();
        gen.writeEndObject();

        system.debug('gen.getAsString(): ' + gen.getAsString());

        return gen.getAsString();
    }

	@future(Callout = true)
    public Static void retiraFaturamentoRM(String cancId) {
        Cancelamento__c canc = [select id, Finalizado__c, StageQueue__c, StageQueueRM__c, Per_odo_Letivo__c, Coligada__c, Usu_rio_em_Atendimento__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c, Queue__c from Cancelamento__c where id =: cancId limit 1];
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string codigoColigada = canc.Coligada__c;
        string idHabilitacaoFilial = canc.Id_Habilita_o__c;
        //string idPerlet = canc.Id_Per_odo_Letivo__c;
        string codperlet = canc.Per_odo_Letivo__c;
        string ra = canc.RA__c;
        string codOperacao = canc.Cod_Operac_o__c;
        string recCreatedBy = canc.RecCreatedBy__c;
        string params = ('?CODCOLIGADA=' + codigoColigada + '&IDHABILITACAOFILIAL=' + idHabilitacaoFilial + '&CODPERLET=' + codperlet + '&RA=' + ra + '&CODOPERACAO=' + codOperacao + '&RECCREATEDBY=' + recCreatedBy);
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
            //req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/ZCANC_RETIRAFATURAMENTO?CODCOLIGADA=1&IDHABILITACAOFILIAL=554&CODPERLET=2017.2&RA=05200323&CODOPERACAO=1&RECCREATEDBY=TOTVS');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('RetiraFaturamentoRM').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    RmRetorno response = parse(res.getBody());
                    system.debug('xanaina ' + res.getBody());
                    if (response.ZcancRetiraFaturamentoResult == 0) {
                        canc.Retirado_do_Faturamento_RM__c = false;
                    } else if (response.ZcancRetiraFaturamentoResult == 1) {
                        canc.Retirado_do_Faturamento_RM__c = true;
                    }

                    system.debug('xanaina ' + canc.Retirado_do_Faturamento_RM__c);
                    update canc;

                } else
                    system.debug('ZCANC_RETIRAFATURAMENTO  / ' + res.getStatusCode() + ' / ' + res.getStatus());
            } catch (CalloutException ex) {
                system.debug('ZCANC_RETIRAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                system.debug('ZCANC_RETIRAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }

    }

    public class RmRetorno {
        public Integer ZcancRetiraFaturamentoResult;
    }

    public static RmRetorno parse(String json) {
        return (RmRetorno) System.JSON.deserialize(json, RmRetorno.class);
    }

    public static Cancelamento__c preencheDadosRm(Cancelamento__c canc) {
        Aluno__c aluno = [select id, name, Conta__c, R_A_do_Aluno__c, Institui_o_n__c, Campus_n__c, Institui_o_n__r.C_digo_da_Coligada__c from Aluno__c where id =: canc.Aluno__c];

        // Academico__c academico = [select Id, Name, RA__c, Aluno__c, IES__c, Campus__c, Oportunidade__c from Academico__c where RA__c =: aluno.R_A_do_Aluno__c and IES__c =: aluno.Institui_o_n__c and Campus__c =: aluno.Campus_n__c limit 1];

        Oferta__c oferta = new Oferta__c();
//        Opportunity opp = new Opportunity();
//        Academico__c academico = new Academico__c();

//        try {
//            academico = [select Id, Name, RA__c, Aluno__c, IES__c, Campus__c, Oportunidade__c from Academico__c where RA__c =: aluno.R_A_do_Aluno__c limit 1];
//            opp = [select id, name, Oferta__c, X1_OpcaoCurso__c, Processo_seletivo__c from Opportunity where id =: academico.Oportunidade__c limit 1];
//            try {
   
//                oferta = [select id, name, ID_Habilitacao_Filial__c, C_digo_da_Coligada__c from Oferta__c where id =: opp.Oferta__c OR id =: opp.X1_OpcaoCurso__c limit 1];


//            } catch (Exception e) {
   
//            }
//        } catch (Exception e) {

////            delete canc;
//            return null;
//        }


        User usuario = [select id, name, DBNumber__c from User where id =: UserInfo.getUserId() limit 1];
        Per_odo_Letivo__c per = [select id, Name, CodPeriodo__c, Status_do_Per_odo_Letivo__c from Per_odo_Letivo__c where Status_do_Per_odo_Letivo__c = 'Ativo'
            and CodPeriodo__c like '2%'
            limit 1
        ];

        //if (oferta != null) {
            //canc.Id_Habilita_o__c = oferta.ID_Habilitacao_Filial__c;
            canc.Id_Habilita_o__c = '0';
            canc.Coligada__c = aluno.Institui_o_n__r.C_digo_da_Coligada__c;
        //}


        if (per != null) {
            canc.Per_odo_Letivo__c = per.CodPeriodo__c;
        }

        canc.Conta__c = aluno.Conta__c;
        //canc.Oportunidade__c = opp.Id;
        canc.RecCreatedBy__c = usuario.DBNumber__c;
        return canc;
    }
}