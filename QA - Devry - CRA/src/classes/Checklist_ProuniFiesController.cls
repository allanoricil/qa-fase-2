/*******************************************************************
Author    :   Rafael Magalhães
Date      :   January 2017
Purpose   :   Controller Class for page Checklist_ProuniFies
*******************************************************************/
public with sharing class Checklist_ProuniFiesController {
    public Checklist_ProuniFiesInscricaoController pesquisa { get; set; }
    /*
        Construtor
    */
    public Checklist_ProuniFiesController() {
        pesquisa = new Checklist_ProuniFiesInscricaoController();
    }
}