/*
	Serviço de consulta de dados do Aluno
*/
public class SRM_ValidaNumeroTelefoneBO implements IProcessingQueue {
	/* 
        Processa a fila de atualização de criação de titulos no SAP
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, payload );     
    }

    /*
		Processa chamada para atualização dos status de inscrição do aluno
		@param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {
    	//Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	//if( mapToken.get('200') != null ) {
    		if(Test.isRunningTest()){
    			
    		}else{
    			
    		
    		HttpRequest req = new HttpRequest(); 
	        req.setEndpoint( WSSetup__c.getValues( 'ASTERIX' ).Endpoint__c );
	        req.setMethod( 'POST' );
	        req.setHeader( 'content-type', 'application/json' );
	        req.setTimeout( 120000 );
	        //req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'ASTERIX' ).API_Token__c );
	        //req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
	        req.setBody( payload );
	 		
	        try {  
	            Http h = new Http();
	            HttpResponse res = h.send( req );

	            if( res.getStatusCode() == 200 ) {
	            	processJsonResult( res.getBody() );
                    QueueBO.getInstance().updateQueue( queueId, '' );		        							               					          
	            } else    
	                QueueBO.getInstance().updateQueue( queueId, 'ASTERIX / ' + res.getStatusCode() + ' / ' + res.getStatus() );
	        } catch ( CalloutException ex ) {
	            QueueBO.getInstance().updateQueue( queueId, 'ASTERIX / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
	        } catch ( Exception ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'ASTERIX / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
        /*    
        } else {
        	QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }*/	
    	}
    }

    /*
        Processa o json de retorno 
        @param jsonResult JSON de retorno do serviço
    */
    private static void processJsonResult( String jsonResult ) {
        Map<String, Object> mapResult = ( Map<String, Object> ) JSON.deserializeUntyped( jsonResult );
        System.debug( '>>> ' + jsonResult );
        Database.DMLOptions options = new Database.DMLOptions();
        options.assignmentRuleHeader.useDefaultRule= true;
        
        Lead leadToUpdate         		= new Lead();
        leadToUpdate.Id                 = ( String ) mapResult.get( 'id' );
        leadToUpdate.setOptions( options );
        leadToUpdate.Telefone_Ok__c  	= String.valueOf( mapResult.get( 'check' ) ).equals( 'yes' ) ? 'Sim' : 'Não';

        ProcessorControl.inFutureContext = true;
        LeadDAO.getInstance().updateData( leadToUpdate );
    }

    /*
		Retorna o Json 
    */
    public Static String getJsonRequest( String recordId, String phoneNumber ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'id',  recordId ); 
        gen.writeStringField( 'phoneNumber', phoneNumber ); 
        gen.writeEndObject(); 

        return gen.getAsString(); 
    }
}