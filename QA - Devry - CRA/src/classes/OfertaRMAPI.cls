public abstract class OfertaRMAPI {
	
	private static final String END_POINT_URL = '';

	public static HttpResponse sendOffers(String requestBody){
		return chamaServico(END_POINT_URL, requestBody, 'POST');
	}

	private static HttpResponse chamaServico(String endPoint,String requestBody, String requestType){
		HttpRequest httpRequest = new HttpRequest();
        httpRequest.setEndpoint(endPoint);
        httpRequest.setMethod(requestType);
        httpRequest.setHeader('Content-Type', 'application/json');
        httpRequest.setTimeout(120000);
        httpRequest.setBody(requestBody);
        Http h = new Http();
        HttpResponse httpResponse = h.send(httpRequest);
        return httpResponse;
	}
}