/*******************************************************************
Author    :   Adílio Santos
Date      :   Agosto 2016
Purpose   :   Controller Class for Page NewGraduateInterviewHome
*******************************************************************/
public with sharing class MastersController {
    
    public Opportunity newOpportunity{get;set;}
    public Account accountToInsert { get; set; }
    public string formStep {get;set;}    
    public string selectedEstado {get;set;}
    
    public string selectedCampus {get;set;}
    public string selectedSelectionProcess {get;set;}
    public string selectedPrograms {get;set;}
    public string selectedScheduling {get;set;}
    public string selectedCountrie {get;set;}
    public string selectedStateOrigemRG {get;set;}
    public List<SelectOption> campusItems {get;set;}
    public List<SelectOption> processItems {get;set;}
    public List<SelectOption> programsItems {get;set;}
    public List<SelectOption> schedulingItems {get;set;}
    public List<SelectOption> placeItems {get;set;}
    public List<SelectOption> areaInterest{get;set;}
    public List<SelectOption> crtitAval{get;set;}
    private String contaExistenteId;
    private String recordTypeId = ApexPages.currentPage().getParameters().get('recordTypeId');    
    private Map<String, Processo_seletivo__c> mapProcessosSeletivos = new Map<String, Processo_seletivo__c>();
    public boolean isRio{get;set;}
    public String pageLanguage{
        get{
            String lang = ApexPages.currentPage().getParameters().get('lang');
            if( lang != null && lang.length() > 0){
                return lang;
            }
            return 'pt_BR';
        }
        set;
    }
    
    /* Constructor */
    public MastersController(){
        placeItems=new List<SelectOption>();
        this.formStep = 'form';
        newOpportunity  = new Opportunity();
        isrio=ApexPages.currentPage().getParameters().get('isRio')!=null?Boolean.valueOf( ApexPages.currentPage().getParameters().get('isRio')):false;   
        String name = ApexPages.currentPage().getParameters().get('name');
        String email = ApexPages.currentPage().getParameters().get('email');
        String phone = ApexPages.currentPage().getParameters().get('phone');
        
        if(name != '' || name != null)
            newOpportunity.Name = name;
        
        if(email != '' || email != null)
            newOpportunity.Email__c = email;
        
        if(phone != '' || phone != null)
            newOpportunity.Celular__c = phone;
        
        fillCampus();
        fillProcess();
        fillPrograms();
        //fillInterviewDates();
        //fillInterviewTimes();
        getAllSelectionCountriesItems();
        getAllSelectionStateItems();
        placeItems=   new List<SelectOption>();
        areaInterest= new List<SelectOption>();
        crtitAval=  new List<SelectOption>();

        
    }
    
    public Pagereference OnchangeSelectionEstado(){
        fillCampus();
        return null;
    }
    public void DuoPass(){
         system.debug('chegou aqui');
         this.formstep='duo';
    }
    public Pagereference OnchangeCampus(){
        fillProcess();
        return null;
    }
    
    public Pagereference OnchangeSelectionProcess(){
        fillPrograms();
        fillScheduling();
        fillSelectsProces();
        //
        //
        //fillInterviewDates();
        //fillInterviewTimes();
        return null;
    }
    
    public List<SelectOption> getAllSelectionCountriesItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Nenhum --'));        
        Map<String, Countries__c> countries = Countries__c.getAll();
        List<String> countryNames = new List<String>();
        countryNames.addAll(countries.keySet());
        countryNames.sort();
        for (String countryName : countryNames) {
            Countries__c country = countries.get(countryName);
            options.add(new SelectOption(country.Name, country.Name));
        }
        return options;
    }
    
    public List<SelectOption> getAllSelectionStateItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- Nenhum --'));        
        Map<String, Estados__c> states = Estados__c.getAll();
        List<String> stateNames = new List<String>();
        stateNames.addAll(states.keySet());
        stateNames.sort();
        for (String stateName : stateNames) {
            Estados__c state = states.get(stateName);
            options.add(new SelectOption(state.Name, state.Name));
        }
        return options;
    }
   
    public List<SelectOption> getAllSelectionEstadoItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Nenhum --'));
        Map<String, Estados__c> estados = Estados__c.getAll();
        List<String> estadosNome = new List<String>();
        estadosNome.addAll(estados.keySet());
        estadosNome.sort();
        for (String e : estadosNome) {
            Estados__c estado = estados.get(e);
                if(estado.Name == 'RJ'&& isRio){
                   options.add(new SelectOption(estado.Name, estado.Name));
                    break;
                }
                 if(estado.Name == 'PE'&& !isRio){
                    options.add(new SelectOption(estado.Name, estado.Name));
                    break;
                }
        }
        return options;
    }
    /*
     public List<SelectOption> getAllSelectionEstadoItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Nenhum --'));
        Map<String, Estados__c> estados = Estados__c.getAll();
        List<String> estadosNome = new List<String>();
        estadosNome.addAll(estados.keySet());
        estadosNome.sort();
        for (String e : estadosNome) {
            Estados__c estado = estados.get(e);
            if(estado.Name == 'PE' || estado.Name == 'RJ') {
                options.add(new SelectOption(estado.Name, estado.Name));
            }
        }
        return options;
    }
    */
    public List<SelectOption> getAllSelectionCampusItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Nenhum --'));
        List<Campus__c> lstCampus = [Select c.Id, c.Name From Campus__c c where c.Estado__c = :selectedEstado];
        for (Campus__c c : lstCampus){
            String campus = c.Name;
            options.add(new SelectOption('',campus));
        }
        return options;
    } 
    
    public List<SelectOption> getAllSelectionProcessItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','-- Nenhum --'));
        List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Nome_da_IES__c,p.name, p.Nome_do_Campus__c, 
                                                 p.Periodo_Letivo__c,p.Sess_o_Ciclo__c,Grupo_P_s__c, ndice_do_Processo__c, Nome_Instituicao_Pos__c
                                                 FROM Processo_seletivo__c p  
                                                 WHERE p.Campus__c != null and 
                                                 p.Periodo_Letivo__c != null and 
                                                 (p.Tipo_de_Matr_cula__c = 'Pós-Graduação IBMEC' or (p.Tipo_de_Matr_cula__c = 'Pós-Graduação' and p.Campus__c = 'a0PA000000FiNao')) and
                                                 p.Ativo__c = true and
                                                 p.Data_de_encerramento__c >= :system.today()
                                                 ORDER BY p.Campus__c ASC];
        for (Processo_seletivo__c proc : lstProcess)  {
            String campus = proc.Nome_do_Campus__c;
            String institution = proc.Nome_da_IES__c;            
            if(proc.Nome_Instituicao_Pos__c != null && proc.Nome_Instituicao_Pos__c.length() > 0){
                institution = proc.Nome_Instituicao_Pos__c;
            }      
            String session;
            String indice;
            String grupo;
            if(String.isNotBlank(proc.Sess_o_Ciclo__c) && String.isNotEmpty(proc.Sess_o_Ciclo__c)){
                session = proc.Sess_o_Ciclo__c;
            }else{
                session = ' ';
            }
            if(String.isNotBlank(proc.ndice_do_Processo__c) && String.isNotEmpty(proc.ndice_do_Processo__c)){
                indice = proc.ndice_do_Processo__c + ' - ';
            }else{
                indice = ' ';
            }
            if(String.isNotBlank(proc.Grupo_P_s__c) && String.isNotEmpty(proc.Grupo_P_s__c)){
                grupo = ' - ' + proc.Grupo_P_s__c;
            }else{
                grupo = ' ';
            }
            options.add(new SelectOption(proc.Id,session + indice + institution + ' - ' + campus + grupo));
        }
        return options;
    }
    
    private void fillCampus(){
        campusItems = new List<SelectOption>();
        campusItems.add(new SelectOption('','-- Nenhum --'));
        String selectedEstado = apexpages.currentpage().getparameters().get('parmSelectedSelectiionEstado');
        if(selectedEstado != null && selectedEstado != ''){
            List<Campus__c> lstCampus = [Select c.Id, c.Name, c.Institui_o__c From Campus__c c where 
                                         c.Estado__c = :selectedEstado and
                                         c.Vis_vel_na_Ficha_de_Inscri_o__c = true];
            for(Campus__c c : lstCampus){
                    Institui_o__c i = [Select Name From Institui_o__c where Id =: c.Institui_o__c limit 1];
                    //string instituicao = i.Name;
                    c.Name = c.Name == 'DeVry São Luis' ? c.Name : i.Name + ' - ' + c.Name;
                    //if(c.Name == 'Faculdade Boa Viagem - Imbiribeira' || c.Name == 'Ibmec Educacional - RJ Centro')
                    if(c.Name == 'Faculdade Boa Viagem - Imbiribeira' || c.Name == 'Ibmec Educacional - RJ Centro' || c.Name == 'Ibmec Educacional - RJ Barra')
                    {
                        campusItems.add(new SelectOption(c.Id, c.Name));
                    }
                }
                //if(c.Name.contains('Dunas')||c.Name.contains('Imbiribeira')||c.Name.contains('Rio Vermelho')||c.Name.contains('Brasília')||c.Name.contains('BH')||c.Name.contains('RJ Centro')||c.Name.contains('RJ Barra')||c.Name.contains('Ibmec Educacional(Filial SP)'))
                    
            //}
        }
    }
    
    private void fillProcess(){
        processItems = new List<SelectOption>();
        processItems.add(new SelectOption('','-- Nenhum --'));
        String selectedCampus = apexpages.currentpage().getparameters().get('parmSelectedCampus');        
        String idTipoDeRegistroDoProcessoSeletivo = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Mestrado').getRecordTypeId();
        if(selectedCampus != null && selectedCampus != ''){
            List<Processo_seletivo__c> lstProcess = [Select p.Id, p.Name, p.CobrarTaxaInscricao__c, Valor_Taxa_Inscricao__c,Cobrar_Taxa_de_Inscri_o__c From Processo_seletivo__c p where
                                                     p.RecordTypeId = :idTipoDeRegistroDoProcessoSeletivo and
                                                     p.Campus__c = :selectedCampus and
                                                     p.Campus__c != null and 
                                                     p.Periodo_Letivo__c = :'MES' and 
                                                     p.Periodo_Letivo__c != null and 
                                                     p.Tipo_de_Matr_cula__c = :'Outra Origem' and
                                                     p.Ativo__c = :true and
                                                     p.Status_Processo_Seletivo__c = :'Aberto' and
                                                     p.Data_de_encerramento__c >= :system.today() and
                                                     p.ID_Academus_ProcSel__c != null and
                                                     p.ID_Academus_ProcSel__c != '' and
                                                     p.C_digo_Processo_Seletivo__c != null and
                                                     p.C_digo_Processo_Seletivo__c != ''
                                                     ORDER BY p.Campus__c ASC];
            if(!lstProcess.isEmpty()){
                for(Processo_seletivo__c p : lstProcess){
                    processItems.add(new SelectOption(p.Id, p.Name));
                    mapProcessosSeletivos.put(p.Id, p);   
                }
            }
        }
    }
  
     private List<SelectOption> ConvertTOSelectOption(string bas)
    {   
         System.debug ('RA=>>>'+ bas);
        if(bas==null)
            return new List<SelectOption>();
        
        String[] arrTest=bas.split(';');
        List<SelectOption> lt= new List<SelectOption>();
        if(arrTest.size()>0){
           for(integer i=0;i<=arrTest.size()-1;i++)
               lt.add(new SelectOption(arrTest[i], arrTest[i]));  
        }

       
       return  lt;
    }
    private void fillSelectsProces(){
        placeItems=   new List<SelectOption>();
        areaInterest= new List<SelectOption>();
        crtitAval=  new List<SelectOption>();
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
        if(selectedProcess != null && selectedProcess != ''){
           Processo_seletivo__c process=[select id,Verifica_cidade__c,Local_de_prova__c,Principal_rea_de_interesse__c,Crit_rios_de_Avalia_o__c from Processo_seletivo__c where id=:selectedProcess Limit 1];
            crtitAval= ConvertTOSelectOption(process.Crit_rios_de_Avalia_o__c); 
            areaInterest=ConvertTOSelectOption(process.Principal_rea_de_interesse__c);
            if(process.Verifica_cidade__c)
               placeItems=  ConvertTOSelectOption(process.Local_de_prova__c);
            
        }
    }

    private void fillPrograms(){
        programsItems = new List<SelectOption>();
        programsItems.add(new SelectOption('','-- Nenhum --'));
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
        if(selectedProcess != null && selectedProcess != ''){
            List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where
                                         o.Processo_seletivo__c = :selectedProcess and
                                         o.ID_Academus_Oferta__c != null and                                         
                                         o.ID_Academus_Oferta__c != '' and
                                         o.C_digo_da_Oferta__c != null and
                                         o.C_digo_da_Oferta__c != '' and
                                         o.Inativa__c != true and
                                         o.Vagas_int__c > 0];
            for(Oferta__c o : lstOffers){
                programsItems.add(new SelectOption(o.Id, o.Name));
            }
        }
    }
    
    private void fillScheduling(){
        schedulingItems = new List<SelectOption>();
        schedulingItems.add(new SelectOption('','-- Nenhum --'));
        String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedSelectiionProcess');
        
        String idTipoDeRegistroDoAgendamento = Schema.SObjectType.Agendamento__c.getRecordTypeInfosByName().get('Mestrado').getRecordTypeId();
        
        List<Processo_seletivo__c> lstProcess = [Select p.Id, p.Name, p.Tipo_de_Matr_cula__c, p.Institui_o_n__c, p.Campus__c From Processo_seletivo__c p where  
                                                 p.Id = :selectedProcess limit 1];      
        
        if(!lstProcess.isEmpty() && selectedProcess != null && selectedProcess != ''){
            List<Agendamento__c> lstAgendamentos = [Select a.Id, a.Name, a.Data_Hora_Inicial__c, a.Data_Hora_T_rmino__c From Agendamento__c a where
                                                    a.RecordTypeId = :idTipoDeRegistroDoAgendamento and                                                     
                                                    a.Institui_o__c =: lstProcess[0].Institui_o_n__c and
                                                    a.Campus__c =: lstProcess[0].Campus__c and
                                                    a.Status__c = 'Aberta' and
                                                    a.Vagas__c > 0 and
                                                    a.Data_Hora_Inicial__c > :system.today()];
            
            for(Agendamento__c a : lstAgendamentos){
                String diaHora = a.Data_Hora_Inicial__c.format('dd/MM/YYYY','America/Sao_Paulo') + ' - ' + a.Data_Hora_Inicial__c.format('HH:mm','America/Sao_Paulo') + ' às ' + a.Data_Hora_T_rmino__c.format('HH:mm','America/Sao_Paulo');
                schedulingItems.add(new SelectOption(a.Id, diaHora));
            }
        }
    }
    
    private Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(0)),Integer.valueOf(myDateOnly.get(1)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }
    
    public Pagereference save(){
        try{ 
           
            system.debug('Email:::'+newOpportunity.Email__c);
            system.debug('Confirmar Email:::'+newOpportunity.Confirmar_E_mail__c);            
            if(newOpportunity.Email__c != newOpportunity.Confirmar_E_mail__c)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'E-mails não correpondentes teste!');
                Apexpages.addMessage(msg);
                return null;
            }
            system.debug('Conta Existente ID - Antes da Atualização:::'+contaExistenteId);
            if( !contaExiste() ) {
                system.debug('Conta Existente ID - Depois da Atualização:::'+contaExistenteId);
                return createAccount();
            } else if( !oportunidadeDuplicada() ) {
                return createOpportunity( contaExistenteId );        
            } else {
                ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.error, 'Já existe a inscrição para este processo seletivo!' );
                Apexpages.addMessage( msg );
            }
            
            //newOpportunity = new Opportunity();
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  System.Label.Interview_Message));
            //this.formStep = 'finish';      
        } catch(Exception ex){
            System.debug('>>>>Exception in Save'+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
            this.formStep = 'form';
        }
        return null;
    }
    
    private Boolean contaExiste() {
        system.debug('CPF:::'+ newOpportunity.CPF__c);
        List<Account> listAccount = AccountDAO.getInstance().getAccountByCpf( newOpportunity.CPF__c );
        system.debug('Lista de Contas:::'+listAccount);
        if( listAccount.size() > 0 ) {
            this.contaExistenteId = listAccount[0].Id;
            return true;
        }
        return false;  
    }
    
    private Boolean oportunidadeDuplicada() {
        List<Opportunity> listOpportunity = OpportunityDAO.getInstance().getDuplicateOpportunityByCPF( selectedSelectionProcess, selectedPrograms, newOpportunity.CPF__c );
        system.debug('Lista de Oportunidades:::'+ listOpportunity);
        if( listOpportunity.size() > 0 ) {
            return true;
        }
        return false;    
    }
    
    private PageReference createAccount() {
        system.debug('Conta a Ser Inserida - Vazia:::'+accountToInsert);
        this.accountToInsert = new Account(
            //FirstName = (newOpportunity.Name).subString(0, (newOpportunity.Name).indexOf(' ')),
            //LastName = (newOpportunity.Name).subString((newOpportunity.Name).indexOf(' '), (newOpportunity.Name).length()),
            LastName = newOpportunity.Name,
            DataNascimento__c = newOpportunity.Data_Nascimento__c,
            Sexo__c = newOpportunity.Sexo__c,
            Nacionalidade__c = selectedCountrie,
            Naturalidade__c = newOpportunity.Naturalidade__c,
            Estado_Civil__c = newOpportunity.Estado_Civil__c,
            CPF_2__c = newOpportunity.CPF__c,
            RG__c = newOpportunity.RG__c,
            RG_Data_de_Emiss_o__c = newOpportunity.Data_de_Emiss_o_do_RG__c,
            Origem_RG__c = newOpportunity.Origem_RG__c,
            Origem_RG_UF__c = selectedStateOrigemRG,
            Nome_da_Mae__c = newOpportunity.Nome_da_Mae__c,
            PersonEmail = newOpportunity.Email__c,
            PersonMobilePhone = newOpportunity.Celular__c,
            Phone = newOpportunity.Telefone__c,
            CEP__c = newOpportunity.CEP__c,
            Cidade__c = newOpportunity.Cidade__c,
            Estado__c = newOpportunity.Estado__c,
            Bairro__c = newOpportunity.Bairro__c,
            Rua__c = newOpportunity.Rua__c,
            N_mero__c = newOpportunity.N_mero__c,
            Complemento__c = newOpportunity.Complemento__c,
            OwnerId = userinfo.getUserId()
        );        
        system.debug('Conta a Ser Inserida - Preenchida:::'+accountToInsert);
        Savepoint sp = Database.setSavepoint();
        try {
            insert accountToInsert;
            system.debug('Conta a Ser Inserida - Inserida:::'+accountToInsert);
            system.debug('ID da Conta inserida:::'+accountToInsert.Id);
            return createOpportunity( accountToInsert.Id );            
        } catch ( DmlException ex ) {
            ApexPages.Message msg = new ApexPages.Message( ApexPages.Severity.ERROR, ex.getDmlMessage(0) ); 
            Apexpages.addMessage( msg );
            Database.rollback(sp);            
            newOpportunity.Id = null;
            accountToInsert.Id = null;
            return null;   
        }
    }
    
    private PageReference createOpportunity( String accountId ) {
        
            if(selectedSelectionProcess != null && selectedSelectionProcess != '')
                newOpportunity.Processo_seletivo__c = selectedSelectionProcess;
            if(selectedPrograms != null && selectedPrograms != '')
                newOpportunity.oferta__c = selectedPrograms;
            if(selectedScheduling != null && selectedScheduling != '')
                newOpportunity.Agendamento__c = selectedScheduling;
            if (recordTypeId != null && recordTypeId != '')
                newOpportunity.RecordTypeId = recordTypeId;  
        
        newOpportunity.StageName               = mapProcessosSeletivos.get( selectedSelectionProcess ).CobrarTaxaInscricao__c == true ? 'Pré-Inscrito' : 'Inscrito';
        newOpportunity.Amount                  = mapProcessosSeletivos.get( selectedSelectionProcess ).CobrarTaxaInscricao__c == true ? mapProcessosSeletivos.get( selectedSelectionProcess ).Valor_Taxa_Inscricao__c : null;
        newOpportunity.AccountId               = accountId;
        newOpportunity.OwnerId                 = userinfo.getUserId();
        newOpportunity.CloseDate               = system.today();
        newOpportunity.Sobrenome__c            = newOpportunity.Name;
        
        system.debug('Fase / Valor / Id da Conta / Proprietário:::'+newOpportunity.StageName+' - '+newOpportunity.Amount+' - '+newOpportunity.AccountId+' - '+newOpportunity.OwnerId);
        
        /* Mapeamento Marketing */
        if (System.currentPageReference().getParameters().get('utm_source') != null) {
            newOpportunity.M_dia_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('utm_source'));
        } else {
            if (System.currentPageReference().getParameters().get('origem_midia') != null) {
                newOpportunity.M_dia_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('origem_midia'));
            }
        }
        if (System.currentPageReference().getParameters().get('utm_campaign') != null) {
            newOpportunity.Campanha_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('utm_campaign'));
        } else {
            if (System.currentPageReference().getParameters().get('origem_campanha') != null) {
                newOpportunity.Campanha_Digital__c = String.valueOf(System.currentPageReference().getParameters().get('origem_campanha'));
            }
        }
        
              
                  List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Niveis_de_Ensino__c, Institui_o_n__c, Campus__c, Tipo_do_curso_oferecido__c
                                                 FROM Processo_seletivo__c p
                                                      WHERE p.Id = :selectedSelectionProcess LIMIT 1];
            
                newOpportunity.Instituicao__c          = lstProcess[0].Institui_o_n__c;
                newOpportunity.Campus_Unidade__c       = lstProcess[0].Campus__c;
                newOpportunity.Tipo_do_curso__c        = lstProcess[0].Tipo_do_curso_oferecido__c;
                
        
        insert newOpportunity;
         String Estado =[Select c.Id, c.Estado__c From Campus__c c where 
                                          c.id = :newOpportunity.Campus_Unidade__c limit 1].Estado__c;
         
        //essa regra foi pedida
         if(Estado=='RJ' && newOpportunity.Crit_rios_de_Avalia_o__c.contains('prova'))
             return paymentRegistration( newOpportunity.Id );
         return mapProcessosSeletivos.get( selectedSelectionProcess ).Cobrar_Taxa_de_Inscri_o__c == true ? paymentRegistration( newOpportunity.Id ) : openConfirmacaoInscricao();         
        //return mapProcessosSeletivos.get( this.processoSeletivoId ).CobrarTaxaInscricao__c == true ? paymentRegistration( opportunityToInsert.Id ) : openConfirmacaoInscricao(); */
        //return null;
    }
    
    private PageReference paymentRegistration( String opportunityId ) {
        PageReference paymentPage = Page.SRM_PagamentoInscricao;
         
        Map<String,String> parameters = paymentPage.getParameters();
        parameters.put( 'id', opportunityId );

        return paymentPage;
    }
    
    public PageReference openConfirmacaoInscricao() {
        PageReference confirmPage = Page.SRM_InscricaoConfirmada;
        Map<String,String> parameters = confirmPage.getParameters();
        parameters.put( 'id', ApexPages.currentPage().getParameters().get('instId') );
        return confirmPage;
    }
}