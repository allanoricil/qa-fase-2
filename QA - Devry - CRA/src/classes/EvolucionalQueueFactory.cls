public with sharing class EvolucionalQueueFactory{

  public static Queue__c buildQueue(String opportunityId, String payload, String serviceName){
    Queue__c fila = new Queue__c();
    fila.EventName__c = serviceName;
    fila.Status__c ='CREATED';
    fila.Payload__c    = payload;
    fila.Oportunidade__c = opportunityId;
    fila.ProcessBySchedule__c = false;
    return fila;
  }

}