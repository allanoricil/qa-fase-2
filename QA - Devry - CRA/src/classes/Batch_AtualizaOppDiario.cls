global class Batch_AtualizaOppDiario implements Schedulable
{
    List<Opportunity> opps {get; set;}
    
        global void execute(SchedulableContext sc) 
        {
            opps = [Select Id FROM Opportunity WHERE Processo_Seletivo_Ativo__c = 'sim' AND Processo_Seletivo_Ativo__c = 'sim' AND RecordTypeId <> '012A0000000nhar'];
            
            for(Opportunity opp : opps)
            {
                opp.Lead_Id__c = '';
            }
                update opps;
        }
        
        
        public static testMethod void testBatch()
        {
            Test.StartTest();

            String time_test = '0 0 0 3 9 ? 2022';

            String jobId = System.schedule('testBasicScheduledApex',
                                            time_test, 
                                            new Batch_AtualizaOppDiario());
            Test.StopTest();
        }
}