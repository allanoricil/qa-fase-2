public without sharing class CRA_ProcessCaseCharging {
	public CRA_ProcessCaseCharging() {
		
	}

	//Método principal para consultar e processar as cobranças do SAP
	@future( Callout=true )
    public static void execute() {
       	Map<String, Object> postData = new Map<String, Object>();
       	List<Cobranca__c> cobrancas = new List<Cobranca__c>();

       	//Método para obter o token:
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null || Test.isRunningTest()) {
    		 try {
    			postData = getStatusDocumento( mapToken.get('200'), getJsonRequestAll() );
    			//Teve pelo menos um documento retornado:
    			if( postData.size() > 0 ) 
    				cobrancas = processaDocumento( postData );
    			System.debug( '>>> postData' + postData );
    			//Documentos tinham cobranças criadas no sistema:
    			if( !cobrancas.isEmpty() ) 
    				retornaStatusDocumento( mapToken.get('200'), getJsonRequest( cobrancas ) );
    			System.debug( '>>> cobrancas' + cobrancas );
    		} catch ( CalloutException ex ) {}
    			catch ( Exception ex ) {
                    system.debug('erro: ' + ex.getMessage());
                } 
    	}

    	//Agendamento de nova consulta para a próxima hora:
    	/*
    	Datetime sysTime = System.now();
        sysTime = sysTime.addMinutes(5) ;
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        System.schedule('NewChargingSchedule' + sysTime.getTime(),chron_exp, new CRA_ScheduleChargingUpdate());
		*/
    }

    //Método para consultar documentos da base do SAP - retorna em JSON desserializado
    private static Map<String, Object> getStatusDocumento( String token, String jsonRequest ) {
    	Map<String, Object> postData = new Map<String, Object>();

    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues( 'SAP-StatusDocumento' ).Endpoint__c );
        req.setMethod( 'POST' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-StatusDocumento' ).API_Token__c );
        req.setHeader( 'AUTH-TOKEN', Test.isRunningTest() ? 'token' : token);
        req.setTimeout( 120000 );
        req.setBody( jsonRequest );

        Http h = new Http();
        HttpResponse res = h.send( req );

        if( res.getStatusCode() == 200 )
    		postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );			        							               					          

        System.debug( '>>> getStatusDocumento ' + res.getBody() );
        return postData;
    }

    //Monta JSON para enviar para SAP 
    private Static String getJsonRequestAll() {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'IdSistema', '005' ); 
        gen.writeStringField( 'QtdeActions', '20' ); 
        gen.writeEndObject(); 

    	return gen.getAsString(); 
    }

    //Processa documentos que foram recebidos - se forem encontradas cobranças referentes no Salesforce, atualiza-as
    private static List<Cobranca__c> processaDocumento( Map<String, Object> postData ) {
        List<Cobranca__c> cobrancasToReturn = new List<Cobranca__c>();
    	Map<Id, Cobranca__c> cobrancasToUpdate = new Map<Id, Cobranca__c>();
        Map<String, Object> statusDocumento = ( Map<String, Object> ) postData.get( 'GetStatusDocumentoResult' );
        List<Object> objMessage = ( List<Object> ) statusDocumento.get( 'messagesField' );
        Map<String, Object> message = ( Map<String, Object> ) objMessage[0];
        Set<String> codigosSF = new Set<String>();
        List<Cobranca__c> allChargings = new List<Cobranca__c>();
        Map<String, Cobranca__c> mapChargings = new Map<String, Cobranca__c>();

        if( String.valueOf( message.get('tIPOField') ).equals( 'S' ) ) {
        	for( Object listResult : ( List<Object> ) statusDocumento.get( 'acc_DocumentField' ) ) {
    			Map<String, Object> documentField = ( Map<String, Object> ) listResult;
                if(String.valueOf( documentField.get( 'dOC_TYPEField' ) ) == 'SF'){
                    String codigoSF = String.valueOf( documentField.get( 'rEF_DOC_NOField' ) );
	      			codigosSF.add(codigoSF);
                }	      			      		
          	}

          	//Busca no Salesforce os registros referentes às cobranças
          	if(codigosSF.size() > 0){
          		allChargings = [SELECT Id, Name, Status__c, CodigoSAP__c, EmpresaSAP__c, ExercicioSAP__c FROM Cobranca__c WHERE Name IN: codigosSF AND Status__c = 'Em Aberto'];
          		for(Cobranca__c cobranca: allChargings){
          			mapChargings.put(cobranca.Name, cobranca);
          		}
          	}

          	for( Object listResult : ( List<Object> ) statusDocumento.get( 'acc_DocumentField' ) ) {
    			Map<String, Object> documentField = ( Map<String, Object> ) listResult;                
                if(String.valueOf( documentField.get( 'dOC_TYPEField' ) ) == 'SF'){
                    System.debug( 'documentField>>> ' + documentField );

                    String codigoSF = String.valueOf( documentField.get( 'rEF_DOC_NOField' ) );
                    String empresaSAP = String.valueOf( documentField.get( 'cOMP_CODEField' ) );
                    String status = String.valueOf( documentField.get('aCTIONField') );
    
                    //Se a cobrança foi encontrada:
                    if( mapChargings.get(codigoSF) != null) {
                        Cobranca__c cobranca = new Cobranca__c();
                        cobranca = mapChargings.get(codigoSF);
                        //Se a cobrança foi liquidada:
                        if(status.equals( 'L' ) ){
                            cobranca.Status__c= 'Pago';
                            cobrancasToUpdate.put(cobranca.Id, cobranca );
                        }
                        else if(cobranca.Data_Limite_Pagamento__c < system.today()){
                            cobranca.Status__c= 'Cancelado';
                            cobrancasToUpdate.put( cobranca.Id, cobranca );
                        }	      			
                    }
                }               
          	}

        }
      	Database.SaveResult[] saveResult = Database.update(cobrancasToUpdate.values(), false) ;
        for(Database.SaveResult sr : saveResult){
            if(sr.isSuccess())
                cobrancasToReturn.add(cobrancasToUpdate.get(sr.getId()));
        }
        return cobrancasToReturn;
    }

    //Atualiza status de documento no SAP, para processado
    private static void retornaStatusDocumento( String token, String jsonRequest ) {
    	System.debug('>>> jsonRequest ' + jsonRequest);
    	HttpRequest req = new HttpRequest();
        req.setEndpoint( WSSetup__c.getValues( 'SAP-RetornaStatusDocumento' ).Endpoint__c );
        req.setMethod( 'POST' );
        req.setHeader( 'content-type', 'application/json' );
        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-RetornaStatusDocumento' ).API_Token__c );
        req.setHeader( 'AUTH-TOKEN', token );
        req.setTimeout( 120000 );
        req.setBody( jsonRequest );
 		
        Http h = new Http();
        HttpResponse res = h.send( req );
    }

    //Monta JSON de envio ao SAP, método ReturnStatusDocumento
    private Static String getJsonRequest( List<Cobranca__c> cobrancas ) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('LDocumentoRetorno');
            gen.writeStartArray();
            	for( Cobranca__c cobranca : cobrancas ) {
            		gen.writeStartObject();
         			gen.writeStringField( 'ACTION', 'L' ); 
        			gen.writeStringField( 'BUKRS', cobranca.EmpresaSAP__c ); 
        			gen.writeStringField( 'BELNR', cobranca.CodigoSAP__c ); 
        			gen.writeStringField( 'GJAHR', cobranca.ExercicioSAP__c ); 
        			gen.writeFieldName('ITENS_DOC');
			            gen.writeStartArray();          
			                gen.writeStartObject();
			                	gen.writeStringField( 'BUZEI', '001' ); 
			        			gen.writeStringField( 'ID_MSG', 'S' ); 
			        			gen.writeStringField( 'MSG', 'Processado com sucesso' );
			                gen.writeEndObject();  
            			gen.writeEndArray(); 
        			gen.writeEndObject();	
            	}            
            gen.writeEndArray();
        gen.writeEndObject();  

    	return gen.getAsString(); 
    }


}