global class CRA_CloseCaseSchedule implements Schedulable{

	global void execute(SchedulableContext sc){
		
		CRA_CloseCaseBatch closeCaseBatch = new CRA_CloseCaseBatch(); 
		Database.executeBatch(closeCaseBatch, 10);

	}
	
}