/*
    @author Diego Moreira
    @class Classe controller para reprocessar a fila
*/
public with sharing class ReprocessarQueueController {
	private String queueId;

	/*
		Construtor
	*/
	public ReprocessarQueueController( ApexPages.StandardController stdController ) {
		queueId = stdController.getId();
	}
    
    public ReprocessarQueueController() {
	}

	/*
		Metodo principal de execução
	*/
	public void execute() {
		Queue__c queue = QueueDAO.getInstance().getQueueById( queueId )[0];
		reprocessaFila( queue );
	}

	/*
    	Executa o reprocessamento da fila
    */
    public static void reprocessaFila( Queue__c queue ) {
    	QueueBO.getInstance().executeProcessingQueue( new List<Queue__c>{ queue } );
    }
}