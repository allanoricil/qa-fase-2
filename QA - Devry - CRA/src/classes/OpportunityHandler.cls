public class OpportunityHandler extends SObjectDAO{
    
    @future
    public static void updateOwnerOpportunity(Id IdOpp, String OwnerId) {        
        List<Opportunity> opp = [SELECT o.Id,o.OwnerId FROM Opportunity o WHERE o.Id =: IdOpp LIMIT 1];
        opp[0].OwnerId = OwnerId;
        update opp;
        
    }
}