@isTest
public class EvolucionalAPITest {

@isTest
public static void testUserCreationServiceCalloutException(){
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPICalloutExceptionMock evolucionalHttpMock = new EvolucionalAPICalloutExceptionMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
 
	try{
		String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	}catch(CalloutException e){
		Boolean expectedExceptionThrown =  e.getMessage().contains('Callout Exception!') ? true : false;
		System.assertEquals(true, expectedExceptionThrown);
	}
}

@isTest 
public static void testListExamResultsServiceCalloutException(){
	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"","userId":"1207349"}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPICalloutExceptionMock evolucionalHttpMock = new EvolucionalAPICalloutExceptionMock(200, 'OK', '{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":439.20},{"Name":"MT","Value":439.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":128.10},{"Name":"MT","Value":128.10}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}', headerMap);

	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
 
	try{
		String response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
	}catch(CalloutException e){
		Boolean expectedExceptionThrown =  e.getMessage().contains('Callout Exception!') ? true : false;
		System.assertEquals(true, expectedExceptionThrown);
	}
}


 
//TESTES PARA O SERVICO DE CRIACAO DE USUARIO
@isTest 
public static void testUserCreationServiceWithACorrectJsonBody(){
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	 
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
}

@isTest 
public static void testUserCreationServiceWithTheSameEmail(){
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	 
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);

	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
	 

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(409, 'Conflict', '{"Message":"O email em causa já está em uso por outro usuário.","Error":"EmailInUse"}', headerMap);
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);

	response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O email em causa já está em uso por outro usuário.","Error":"EmailInUse"}', response);
}

@isTest 
public static void testUserCreationServiceWithoutToken(){
	String randomEmail = Faker.getEmail();
	String requestBodyWithoutToken = '{"token":"","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	 
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(403, 'Forbidden', '{"Message":"Você não tem acesso a API.","Error":"InvalidToken"}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);

	String response = EvolucionalAPI.criaUsuario(requestBodyWithoutToken).getBody();
	System.assertEquals('{"Message":"Você não tem acesso a API.","Error":"InvalidToken"}', response);
}

@isTest 
public static void testUserCreationServiceWithInvalidJsonStructure(){
	String randomEmail = Faker.getEmail();
	String requestBodyWithWrongJsonStructure = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1"}';
	 
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(400, 'Bad Request', '{"Message":"Estrutura de json incorreta.","Error":"InvalidJsonFormat"}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);

	String response = EvolucionalAPI.criaUsuario(requestBodyWithWrongJsonStructure).getBody();
	System.assertEquals('{"Message":"Estrutura de json incorreta.","Error":"InvalidJsonFormat"}', response);
}

@isTest
public static void testUserCreationServiceWithoutPassword(){
	String randomEmail = Faker.getEmail();
	String requestBodyWithNullPassword = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":null,"degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	 
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(400, 'Bad Request', '{"Message":"Estrutura de json incorreta.","Error":"InvalidJsonFormat"}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);

	String response = EvolucionalAPI.criaUsuario(requestBodyWithNullPassword).getBody();
	System.assertEquals('{"Message":"Estrutura de json incorreta.","Error":"InvalidJsonFormat"}', response);
}
 
//TESTES PARA O SERVICO DE LISTAR OS RESULTADOS DO USUARIO
@isTest
public static void testListExamServiceWithACorrectJsonBody(){
	//Primeiro requisita o servico de criar usuario 
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
	 
	//Agora requisita o servico de listar resultados com o usuario criado anteriormente
	Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(response);
	        String userId = JSON.serialize(objectMap.get('UserId'));

	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"'+ randomEmail +'","userId":"'+ userId +'"}';

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}', headerMap);
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();

	System.assertEquals('{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":390.02},{"Name":"MT","Value":390.02}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}',response);
}

@isTest
public static void testListExamServiceUserIdDIffersFromEmail(){
	//Primeiro requisita o servico de criar usuario 
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
	 
	//Agora requisita o servico de listar resultados com o usuario criado anteriormente
	Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(response);
        String userId = JSON.serialize(objectMap.get('UserId'));

	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"lalalala@blablabla.com.br","userId":"'+ userId +'"}';

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(400, 'Bad Request', '{"Message":"Não existe nenhum usuário associado com os dados informados.","Error":"NoContent"}', headerMap);

	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
	System.assertEquals('{"Message":"Não existe nenhum usuário associado com os dados informados.","Error":"NoContent"}',response);
}


@isTest
public static void testListExamServiceWithoutEmail(){
	//Primeiro requisita o servico de criar usuario 
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
 
	//Agora requisita o servico de listar resultados com o usuario criado anteriormente
	Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(response);
        String userId = JSON.serialize(objectMap.get('UserId'));

	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"","userId":"'+ userId +'"}';

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":439.20},{"Name":"MT","Value":439.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":128.10},{"Name":"MT","Value":128.10}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}', headerMap);

	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
	System.assertEquals('{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":439.20},{"Name":"MT","Value":439.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":128.10},{"Name":"MT","Value":128.10}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}',response);
}
 

@isTest
public static void testListExamServiceWithoutUserId(){
	//Primeiro requisita o servico de criar usuario 
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
	 
	//Agora requisita o servico de listar resultados com o usuario criado anteriormente
	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"'+ randomEmail +'","userId":""}';

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}', headerMap);

	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
	System.assertEquals('{"AssessmentList":[{"AssessmentId":490,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.a","Evaluations":[{"OpenEvaluationId":226,"Id":874,"Name":"Vestibular Damásio | UNIFAVIP 2018.1","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/226"}]},{"AssessmentId":491,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.b","Evaluations":[{"OpenEvaluationId":227,"Id":875,"Name":"Vestibular Damásio | UNIFAVIP 2018.2","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/227"}]},{"AssessmentId":492,"AssessmentName":"Vestibular Damásio | UNIFAVIP 2018.c","Evaluations":[{"OpenEvaluationId":228,"Id":876,"Name":"Vestibular Damásio | UNIFAVIP 2018.3","StartDate":"2017-10-10T11:32:35","EndDate":"2018-09-15T09:38:58","Areas":[{"Name":"LC","Value":329.20},{"Name":"MT","Value":329.20}],"TimeDuration":120,"ResultAvailable":false,"Finalized":false,"TimeUp":false,"InProgress":false,"Initiated":false,"Url":"http://testedevry.evolucional.com.br/prova/228"}]}]}',response);
}

@isTest
public static void testListExamServiceWithoutUserIdAndEmail(){
	//Primeiro requisita o servico de criar usuario 
	String randomEmail = Faker.getEmail();
	String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
	Map<String,String> headerMap = new Map<String, String>();
	headerMap.put('Content-Type', 'application/json');
	EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
	 
	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
	System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
	 
	//Agora requisita o servico de listar resultados com o usuario criado anteriormente
	Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(response);
	        String userId = JSON.serialize(objectMap.get('UserId'));

	String correctListExamsRequestBody = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":"","userId":""}';

	evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(400, 'Bad Request', '{"Message":"O campo email ou id deve conter um valor.","Error":"MissingParams"}', headerMap);

	Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
	response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
	System.assertEquals('{"Message":"O campo email ou id deve conter um valor.","Error":"MissingParams"}',response);
}


@isTest
public static void testListExamServiceWithoutToken(){
		//Primeiro requisita o servico de criar usuario 
		String randomEmail = Faker.getEmail();
		String correctUserCreationServiceRequestBody = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"' + randomEmail + '","cityId":null,"name":"José de Paulo Araújo Neto","mobile":"8888888888","password":"UNIFAVIP@4575","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}';
		Map<String,String> headerMap = new Map<String, String>();
		headerMap.put('Content-Type', 'application/json');
		EvolucionalAPIHttpCalloutMock evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(200, 'OK', '{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', headerMap);
		 
		Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
		String response = EvolucionalAPI.criaUsuario(correctUserCreationServiceRequestBody).getBody();
		System.assertEquals('{"Message":"O usuário foi cadastrado com sucesso.","UserId":1207349}', response);
		 
		//Agora requisita o servico de listar resultados com o usuario criado anteriormente
		Map<String, Object> objectMap = (Map<String, Object>) JSON.deserializeUntyped(response);
		        String userId = JSON.serialize(objectMap.get('UserId'));

		String correctListExamsRequestBody = '{"token":"","email":"","userId":"'+ userId +'"}';

		evolucionalHttpMock = new EvolucionalAPIHttpCalloutMock(403, 'Forbidden', '{"Message":"Você não tem acesso a API.","Error":"InvalidToken"}', headerMap);

		Test.setMock(HttpCalloutMock.class, evolucionalHttpMock);
		response = EvolucionalAPI.listaSimulados(correctListExamsRequestBody).getBody();
		System.assertEquals('{"Message":"Você não tem acesso a API.","Error":"InvalidToken"}',response);
	}
}