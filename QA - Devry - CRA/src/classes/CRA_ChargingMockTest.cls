@isTest
global class CRA_ChargingMockTest implements HttpCalloutMock{
	global HttpResponse respond(HttpRequest req){
		//System.assertEquals('disciplina', req.getEndpoint());
		system.debug('respond: '+req.getBody());
		system.debug('endpointmock: '+req.getEndpoint());
		HttpResponse res = new HttpResponse();
		if(req.getEndpoint() == 'Authorization'){
			res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"auth_token": "eeac0aecd45fb1d838e42c4e202e1614"}');
	        res.setStatusCode(200);
		} else if(req.getEndpoint() == 'SAP-StatusDocumento'){
            String cName = [SELECT Name FROM Cobranca__c LIMIT 1].Name;
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody('{ "GetStatusDocumentoResult": { "acc_DocumentField": [ { "aCTIONField": "L", "bELNRField": "' + cName + '", "cOMP_CODEField": "1010", "cURRENCYField": "BRL", "customerItemField": [ { "aGBUZField": "", "aLLOC_NMBRField": "3 parc smt 2017.2", "aUGBLField": "", "aUGDTField": "", "aUGGJField": "", "bANK_IDField": "CAIX1", "bLINE_DATEField": "2017023", "bUSINESSPLACEField": "0002", "cUSTOMERField": "0000131955", "dSCT_DAYS1Field": "15 ", "dSCT_DAYS2Field": "0 ", "dSCT_PCT1Field": "5.000 ", "dSCT_PCT2Field": "0.000 ", "hOUSEBANKACCTIDField": "CC001", "iTEMNO_ACCField": "001", "iTEM_TEXTField": "1013222026 LARISSA FERNANDES MENDES LIMA DIREITO", "mATRICULAField": "1013222026", "pYMT_AMTField": "820.80 ", "pYMT_METHField": "D", "rEF_KEY_3Field": "240000010002548802", "sHKZGField": "D", "sP_GL_INDField": "" } ], "dOC_DATEField": "20150223", "dOC_TYPEField": "SF", "fIS_PERIODField": "03", "gJAHRField": "2015", "hEADER_TXTField": "GRAD - MENSALIDADE", "pSTNG_DATEField": "20150301", "rEF_DOC_NOField": "' + cName + '" } ], "messagesField": [ { "idField": "003", "mSGField": "Action enviada com sucesso", "tIPOField": "S" } ] } }');
            res.setStatusCode(200);
    	}
    	else res.setStatusCode(401);
        return res;	
	}
}