/*
    @author Lincoln Soares
    @class Classe controladora da pagina de processo seletivo
*/
public with sharing class SRM_InscricaoController {
    public List<SelectOption> instituicaoOptions { get; private set; }
    public List<SelectOption> processoSeletivoOptions { get; private set; }
    public String instituicaoId { get; set; }
    public String processoSeletivoId { get; set; }


    /*
        Construtor
    */
    public SRM_InscricaoController(ApexPages.StandardController controller) {
        processoSeletivoOptions = new List<SelectOption>();
        getInstituicaoEnsino();
    }

    /*

    */
    public void getInstituicaoEnsino() {
        instituicaoOptions = new List<SelectOption>();
        for( Institui_o__c instituicao : InstituicaoDAO.getInstance().getAllInstituicoes() ) {
            instituicaoOptions.add( new SelectOption( instituicao.Id, instituicao.Name ) );
        }
    }

    /*

    */
    public void getProcessoSeletivoByIntituicaoId() {
        processoSeletivoOptions = new List<SelectOption>();
        for( Processo_seletivo__c processoSeletivo : Processo_SeletivoDAO.getInstance().getProcessoSeletivosAtivosByInstituicaoId( instituicaoId ) ) {
            processoSeletivoOptions.add( new SelectOption( processoSeletivo.Id, processoSeletivo.Name ) );
        }
    }
    
    public PageReference openProcessoSeletivo() {
        if( instituicaoId != null && processoSeletivoId != null ) {
            
            if(instituicaoId =='a0SA000000OsejfMAB'){
               PageReference pageInsc = Page.SRM_InscricaoAlunoIbmec;
               Map<String,String> parameters = pageInsc.getParameters();
               parameters.put( 'instId', instituicaoId );
               parameters.put( 'processoSeletivoId', processoSeletivoId );
               return pageInsc;   
            }
            
            PageReference pageInsc = Page.SRM_InscricaoAluno;
            Map<String,String> parameters = pageInsc.getParameters();
            parameters.put( 'instId', instituicaoId );
            parameters.put( 'processoSeletivoId', processoSeletivoId );
            return pageInsc;
        } else {
            ApexPages.Message msgPagto = new ApexPages.Message( ApexPages.Severity.ERROR, 'Selecione a IES e o Processo seletivo!' );
            Apexpages.addMessage( msgPagto );
            return null;
        }        
    }
    
}