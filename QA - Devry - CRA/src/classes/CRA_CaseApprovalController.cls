public without sharing class CRA_CaseApprovalController {
    public Case myCase {
        set;
        get;
    }
    public Boolean error {
        set;
        get;
    }
    public Boolean emNAAF {
        set;
        get;
    }
    public String comment {
        set;
        get;
    }
    public String proxFilaName {
        set;
        get;
    }
    private List < ProcessInstance > pi {
        set;
        get;
    }
    private List < ProcessInstanceWorkitem > piwi {
        set;
        get;
    }

    public CRA_CaseApprovalController(ApexPages.StandardController stdController) {
        this.error = false;

        if (!Test.isRunningTest()) {
            List < String > myFields = new List < String > ();
            myFields.add('Id');
            myFields.add('CaseNumber');
            myFields.add('Contador_de_Marcos__c');
            myFields.add('Tipo_de_Registro__c');
            myFields.add('C_d_Opera_o__c');
            stdController.addFields(myFields);
        }

        system.debug('aqui---');

        this.myCase = (Case) stdController.getRecord();

        system.debug('aqui' + this.myCase);

        this.pi = [Select ID, Status From ProcessInstance Where TargetObjectID =: myCase.Id AND Status = 'Pending'
            LIMIT 1
        ];
        system.debug('aqui2');
        if (this.pi.isEmpty()) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Esse registro não pode ser aprovado agora'));
            this.error = true;
            system.debug('aqui3');
        } else {
            system.debug('aqui4');
            this.piwi = [select Id, OriginalActorId, ActorId from ProcessInstanceWorkitem where ProcessInstanceId =: pi.get(0).Id LIMIT 1];
            system.debug('piwi: ' + piwi);
            if (!this.piwi.isEmpty()) {
                system.debug('aqui5');
                        	
                if (this.piwi.get(0).OriginalActorId != UserInfo.getUserId() && this.piwi.get(0).ActorId != UserInfo.getUserId()) {
                    system.debug('aqui6');
                    if (this.myCase.Tipo_de_Registro__c == 'Cancelamento de Contrato') {
                        //impor regra de aprovação

                        Group fila = new Group();
                        
                        if (this.myCase.Contador_de_Marcos__c == 1 && this.myCase.C_d_Opera_o__c == '4') {
                            fila = [Select Id from Group where type = 'Queue'
                                and Name = 'Cancelamento de contrato - Admissões'
                            ];
                            this.proxFilaName = 'Coordenação';
                        } else if (this.myCase.Contador_de_Marcos__c == 1 && this.myCase.C_d_Opera_o__c != '4') {
                            fila = [Select Id from Group where type = 'Queue'
                                and Name = 'Cancelamento de contrato - Casa'
                            ];
                            this.proxFilaName = 'Coordenação';
                        } else if (this.myCase.Contador_de_Marcos__c == 2) {
                            fila = [Select Id from Group where type = 'Queue'
                                and Name = 'Cancelamento de contrato - Coordenação'
                            ];
                            this.proxFilaName = 'Biblioteca';
                        } else if (this.myCase.Contador_de_Marcos__c == 3) {
                            fila = [Select Id from Group where type = 'Queue'
                                and Name = 'Cancelamento de contrato - Biblioteca'
                            ];
                            this.proxFilaName = 'NAAF';
                        } else if (this.myCase.Contador_de_Marcos__c == 5) {
                            fila = [Select Id from Group where type = 'Queue'
                                and Name = 'Cancelamento de contrato - Faturamento'
                            ];

                        }

                        List < Id > idMembros = new List < Id > ();
                        List < GroupMember > membros = [Select UserOrGroupId From GroupMember where GroupId =: fila.Id];
                        for (GroupMember membro: membros) {
                            idMembros.add(membro.UserOrGroupId);
                        }

                        Boolean podeAprovar = false;
                        List < User > usuarios = [select id, Email from User where id in: idMembros];
                        for (User usuario: usuarios) {
                            if (usuario.Id == UserInfo.getUserId()) {
                                podeAprovar = true;
                            }
                        }

                        if (!podeAprovar) {
                            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Você não pertence a fila de aprovação desse registro.'));
                            this.error = true;
                        }

                    } else {
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Você não é o aprovador atual desse registro.'));
                        this.error = true;
                    }

                }
                
                if (this.myCase.Contador_de_Marcos__c == 4){
	                this.proxFilaName = 'Faturamento';
	            }
            }

            if (this.myCase.Tipo_de_Registro__c == 'Cancelamento de Contrato' && this.myCase.Contador_de_Marcos__c == 4) {
                this.emNAAF = true;
                string mensagem = CRA_CaseUtil.checkCaseCancAttachment(this.myCase.Id);
                system.debug('xanaina ' + mensagem);
                if (mensagem != null) {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, mensagem));
                    this.error = true;
                }
            }

        }
    }

    public pageReference approve() {
        if (String.isBlank(comment)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Por favor, deixe um comentário sobre a aprovação.'));
            return null;
        } else {
            PageReference pageRef = new PageReference('/' + myCase.Id);
            pageRef.setRedirect(true);
            Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
            prWkItem.setWorkItemID(piwi.get(0).id);
            prWkItem.setComments(comment);
            prWkItem.setAction('Approve');
            try {
                Approval.ProcessResult appResult = Approval.process(prWkItem);
            } catch (Exception e) {
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Ocorreu um erro inesperado, entre em contato com o administrador do sistema'));
                return null;
            }
            return pageRef;
        }
        return null;
    }

    public pageReference reject() {
        if (String.isBlank(comment)) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Por favor, deixe um comentário sobre a aprovação.'));
            return null;
        } else {
            PageReference pageRef = new PageReference('/' + myCase.Id);
            pageRef.setRedirect(true);
            Approval.ProcessWorkitemRequest prWkItem = new Approval.ProcessWorkitemRequest();
            prWkItem.setWorkItemID(piwi.get(0).id);
            prWkItem.setComments(comment);
            prWkItem.setAction('Reject');
            try {
                Approval.ProcessResult appResult = Approval.process(prWkItem);
            } catch (Exception e) {
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO, 'Ocorreu um erro inesperado, entre em contato com o administrador do sistema'));
                return null;
            }
            return pageRef;
        }
        return null;
    }
}