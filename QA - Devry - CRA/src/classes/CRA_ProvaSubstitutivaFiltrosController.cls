public without sharing class CRA_ProvaSubstitutivaFiltrosController {
	public List<SelectOption> campusList     {get;set;}
	public List<SelectOption> assuntoList    {get;set;}
	public String campusValue                {get;set;}
	public String turma                      {get;set;}
	public String assuntoValue               {get;set;}
	public String periodo_Letivo             {get;set;}
	public List<Item_do_caso__c> itensCaso   {get;set;}
	public boolean allowPDF                  {get;set;}

	
	public CRA_ProvaSubstitutivaFiltrosController() {

		//preparacao valores de assunto
        assuntoList = new List<SelectOption>();
        assuntoList.add( new SelectOption('AP1','AP1'));
        assuntoList.add( new SelectOption('AP2','AP2'));
        assuntoList.add( new SelectOption('AP3','AP3'));

        //preparacao valore de campus
        List<Campus__c> allCampi = new List<Campus__c>();
        allCampi = [SELECT Id, Name FROM Campus__c ORDER BY Name ASC];
        campusList = new List<SelectOption>();
        for(Campus__c c: allCampi)
        	campusList.add(new SelectOption (c.Id,c.Name));

        //informações do usuário
        Id userId = UserInfo.getUserId();
        User usuario = [SELECT Id, DBNumber__c FROM User WHERE Id =: userId ];

        //preparação valores de periodo letivo
        system.debug('allowpdf '+allowPDF);

    }


    public PageReference gerarPDF(){
		
    	if(campusValue != null && turma != null && assuntoValue !=null && periodo_Letivo != null){
	    	PageReference listaPDF = new PageReference('/apex/CRA_ProvaSubstitutivaAta?campus='+campusValue+'&assunto='+assuntoValue+'&turma='+turma+'&periodo='+periodo_Letivo);
	    	listaPDF.setRedirect(true);
        	return listaPDF;
	    }

	    return null;
    }
    
}