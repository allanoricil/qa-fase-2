public with sharing class Assessment {

	private Integer AssessmentId{get;private set;}
	private String AssessmentName{get;private set;}
	List<Evaluation> Evaluations{get;private set;}

	public Integer getAssessmentId(){
		return AssessmentId;
	}

	public String getAssessmentName(){
		return AssessmentName;
	}

	public List<Evaluation> getEvaluations(){
		return Evaluations;
	}
}