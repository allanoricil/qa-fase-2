public class CursoMockup {
    public static Curso__C createAndGetCursoMockup(){
        Institui_o__c ins = Institui_oMockup.createAndGetInstituicao();
        
        Campus__C campus = CampusMockup.createAndGetCampusMockup();
        
         Curso__c curso = new Curso__c(Name='Curso teste',
                                      Status__c='Ativo',
                                      Institui_o_n__c = ins.id,
                                      Campus__C = campus.id,
                                      Tipo_do_Curso__c = 'Tipo Teste',
                                      Id_Habilitacao_Filial__c = 'id teste'
                                     );
        insert curso;
        return curso;
    }

}