global class CRA_OpportunityEADRemoteAction {
	

	public CRA_OpportunityEADRemoteAction(ApexPages.StandardController stdController){}

	@RemoteAction
	public static void setPageAutoRefreshToFalse(Id oppId){
		Opportunity opp = new Opportunity();
		opp.Id = oppId;
		opp.Page_Auto_Refresh__c = false;
		update opp;
	}
}