public without sharing class CRA_CreateCaseChargings {
    /*        
        Aluno__c aluno = [SELECT Name, Id, Conta__r.VoceEstrangeiro__c, Sexo__c, Rua__c, Numero__c, Complemento__c, Bairro__c, CEP__c, Cidade__c, Estado__c, Telefone__c, Celular__c, CPF__c, Institui_o_n__r.C_digo_da_Coligada__c, e_mail__c FROM Aluno__c WHERE Id = 'a0B2F000000AHvT'];

        Map<String, String> mapSAPId = CRA_CreateCaseChargings.findClienteSAPcode(aluno);
        String findClienteId = '000' + mapSAPId.get('id_cliente');
        String findEmpresaId = mapSAPId.get('empresasap');
        Cobranca__c cobranca = [SELECT Id, Caso__r.Aluno__r.Institui_o_n__r.CodigoSAP__c, Caso__r.Assunto__r.Tipo_de_Documento__c, DataVencimento__c, Caso__r.Assunto__r.Tipo_de_Servico_Gerado__c, Caso__r.Aluno__r.Campus_n__r.CodigoSAP__c, Name, Caso__r.Aluno__r.Institui_o_n__r.Banco__c, Caso__r.Aluno__r.Institui_o_n__r.Conta_Caixa__c, Caso__r.Aluno__r.R_A_do_Aluno__c, Caso__r.Assunto__r.Name, Valor__c, Caso__r.Aluno__r.Institui_o_n__r.CentroLucro__c FROM Cobranca__c WHERE ID = 'a1a2F00000002cv'];

        String cobranca_json = CRA_CreateCaseChargings.createChargingToJson(cobranca, findClienteId, findEmpresaId);
        system.debug('JSON cobrança: ' + cobranca_json);
        CRA_CreateCaseChargings.syncToServer(cobranca_json);

        //CRA_ProcessCaseCharging.execute();
    */

	public Static void syncToServer(String payload) {
        Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	system.debug('mapToken' + mapToken);
	    if( mapToken.get('200') != null ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( WSSetup__c.getValues( 'SAP-InboundSap' ).Endpoint__c );
            req.setMethod( 'POST' );
            req.setHeader( 'content-type', 'application/json' );
            req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'SAP-InboundSap' ).API_Token__c );
            req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
            req.setBody( payload );
            
            try {
                Http h = new Http();
                HttpResponse res = h.send( req );
                system.debug('res body'+ res.getBody());
                system.debug('res status'+ res.getStatusCode() );
                if( res.getStatusCode() == 200 ){
                	system.debug('status = 200');
                    system.debug('response ' + res.getBody());
                    processChargingJsonBody( res.getBody() );			        							               					          
                }
                else{
                	system.debug('status != 200');
                    //QueueBO.getInstance().updateQueue( queueId, 'InboundSap / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                }
            } catch ( CalloutException ex ) {
            	system.debug('callout exception' + ex);
                //QueueBO.getInstance().updateQueue( queueId, 'InboundSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }	       
	    } else {
	    	system.debug('200 = null');
	            //QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
	    }
	}

    //Geração do JSON para enviar para serviço ClienteSAP
    public static Map<String, String> findClienteSAPcode(Case caso){

        JSONGenerator gen = JSON.createGenerator(true);

        gen.writeStartObject();
        gen.writeFieldName('cliente');        
        gen.writeStartObject();

        gen.writeStringField( 'nome', caso.Account.Name );

        //De onde vem essa informação?
        gen.writeStringField( 'kunnr', 'SALESFORCE' );

        //Aluno pode ser estrangeiro?
        if(caso.Aluno__r.Conta__r.VoceEstrangeiro__c == 'Sim')
            gen.writeStringField( 'ktokd', 'DB02' );
        else
            gen.writeStringField( 'ktokd', 'DB01' );
        //Sexo = Genero?
        if(caso.Aluno__r.Sexo__c == 'Masculino' || caso.Aluno__r.Sexo__c == 'M')        
            gen.writeStringField( 'anred', 'Senhor' );
        else if(caso.Aluno__r.Sexo__c == 'Feminino' || caso.Aluno__r.Sexo__c == 'F')
            gen.writeStringField( 'anred', 'Senhora' );
        else
            gen.writeStringField( 'anred', '' );

        //Endereço:
        gen.writeStringField( 'street', String.isBlank(caso.Aluno__r.Rua__c)?'':caso.Aluno__r.Rua__c );
        gen.writeStringField( 'Houseno', String.isBlank(caso.Aluno__r.Numero__c)?'':caso.Aluno__r.Numero__c );
        gen.writeStringField( 'strsuppl1', String.isBlank(caso.Aluno__r.Complemento__c)?'':caso.Aluno__r.Complemento__c );
        gen.writeStringField( 'ort02', String.isBlank(caso.Aluno__r.Bairro__c)?'':caso.Aluno__r.Bairro__c );
        gen.writeStringField( 'pstlz', String.isBlank(caso.Aluno__r.CEP__c)?'':caso.Aluno__r.CEP__c );
        gen.writeStringField( 'ort01', String.isBlank(caso.Aluno__r.Cidade__c)?'':caso.Aluno__r.Cidade__c );
        gen.writeStringField( 'regio', String.isBlank(caso.Aluno__r.Estado__c)?'':caso.Aluno__r.Estado__c );

        //Comunicação:
        gen.writeStringField( 'tel1numbr', String.isBlank(caso.Aluno__r.Telefone__c)?'':caso.Aluno__r.Telefone__c );
        gen.writeStringField( 'tel1ext', '' );
        gen.writeStringField( 'mobnumber', String.isBlank(caso.Aluno__r.Celular__c)?'':caso.Aluno__r.Celular__c );
        gen.writeStringField( 'faxnumber', '' );
        gen.writeStringField( 'faxextens', '' );
        gen.writeStringField( 'email', String.isBlank(caso.Aluno__r.e_mail__c)?'':caso.Aluno__r.e_mail__c );

        //CPF/coligada
        gen.writeStringField( 'stcd1', '');
        gen.writeStringField( 'stcd2', String.isBlank(caso.Aluno__r.CPF__c)?'':caso.Aluno__r.CPF__c );
        gen.writeStringField( 'aluno', '1' );
        gen.writeStringField( 'codcoligada', String.isBlank(caso.Aluno__r.Institui_o_n__r.C_digo_da_Coligada__c)?'':caso.Aluno__r.Institui_o_n__r.C_digo_da_Coligada__c );

        gen.writeEndObject();

        gen.writeEndObject();

        String payload = gen.getAsString();
        system.debug('payload '+ payload);
        
        Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
        system.debug('mapToken: ' + mapToken);
        Map<String, String> id_SAP = new Map<String, String>();

        if( mapToken.get('200') != null ) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint( WSSetup__c.getValues( 'ClienteSap' ).Endpoint__c );
            req.setMethod( 'POST' );
            req.setHeader( 'content-type', 'application/json' );
            req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'ClienteSap' ).API_Token__c );
            req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
            req.setBody( payload );        
            try {
                Http h = new Http();
                HttpResponse res = h.send( req );
                system.debug('response ' + res + ' body: ' + res.getBody() + 'header_keys: ' + res.getHeaderKeys());
                if( res.getStatusCode() == 200 ){
                    system.debug('status = 200');                
                    Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
                    System.debug( '>>> postData ' + postData );     
                    Object objResult = postData.get( 'ClienteSapResult' );
                    Map<String, Object> result = ( Map<String, Object> ) objResult;
                    id_SAP.put('id_cliente', String.valueOf(result.get('id_cliente')));
                    id_SAP.put('empresasap', String.valueOf(result.get('empresasap')));                                                     
                }
                else{
                    system.debug('status != 200');
                    //QueueBO.getInstance().updateQueue( queueId, 'InboundSap / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                }
            } catch ( CalloutException ex ) {
                system.debug('callout exception' + ex);
                //QueueBO.getInstance().updateQueue( queueId, 'InboundSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }          
        } else {
            system.debug('200 = null');
                //QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }

        return id_SAP;        
    }

	 public static String createChargingToJson( Case caso, Cobranca__c cobranca, String idClienteSAP, String idEmpresaSAP ) {
        List<Per_odo_Letivo__c> periodoLetivoList = [SELECT Name FROM Per_odo_Letivo__c WHERE Status_CRA__c =: 'Ativo' LIMIT 1];
        String periodoLetivo = periodoLetivoList.isEmpty()? '' : periodoLetivoList.get(0).Name;
	 	Date dtHoje = Date.today();
        String year = String.valueOf( dtHoje.year() );
        String month = String.valueOf( dtHoje.month() ).length() == 2 ? String.valueOf( dtHoje.month() ) : '0' + String.valueOf( dtHoje.month() );
        String day = String.valueOf( dtHoje.day() ).length() == 2 ? String.valueOf( dtHoje.day() ) : '0' + String.valueOf( dtHoje.day() );
        String postingYear = String.valueOf( cobranca.DataVencimento__c.year() );
        String postingMonth = String.valueOf( cobranca.DataVencimento__c.month() ).length() == 2 ? String.valueOf( cobranca.DataVencimento__c.month() ) : '0' + String.valueOf( cobranca.DataVencimento__c.month() );
        String postingDay = String.valueOf( cobranca.DataVencimento__c.day() ).length() == 2 ? String.valueOf( cobranca.DataVencimento__c.day() ) : '0' + String.valueOf( cobranca.DataVencimento__c.day() ); 
        
        String mesVencimento = String.valueOf( cobranca.DataVencimento__c.month() ).length() == 2 ? String.valueOf( cobranca.DataVencimento__c.month() ) : '0' + String.valueOf( cobranca.DataVencimento__c.month() );
        system.debug('mes vencimento ' + mesVencimento);
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('AccountingDataAcc_Document');
        gen.writeStartArray();          
        gen.writeStartObject();
        
        
        gen.writeStringField( 'REF_DOC_NO', cobranca.Name );   
        if(Test.isRunningTest()){
            gen.writeStringField( 'COMP_CODE', '4' );
        }else{
            gen.writeStringField( 'COMP_CODE', idEmpresaSAP );
        }        
        //cobranca.Caso__r.Aluno__r.Institui_o_n__r.CodigoSAP__c
        gen.writeStringField( 'DOC_DATE', year + month + day );
        gen.writeStringField( 'DOC_TYPE', String.isBlank(caso.Assunto__r.Servico__r.ProductCode)?'':caso.Assunto__r.Servico__r.ProductCode );        
        gen.writeStringField( 'PSTNG_DATE', postingYear + postingMonth + postingDay); 
        gen.writeStringField( 'FIS_PERIOD', String.isBlank(mesVencimento)?'':mesVencimento );
        gen.writeStringField( 'CURRENCY', 'BRL' );
        gen.writeStringField( 'HEADER_TXT', String.isBlank(caso.Assunto__r.Servico__r.Name)?'':caso.Assunto__r.Servico__r.Name);            
        gen.writeFieldName('CustomerItem');
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField( 'REF_DOC_NO', cobranca.Name );
        gen.writeStringField( 'ITEMNO_ACC', '0000000001' );
        gen.writeStringField( 'CUSTOMER', String.isBlank(idClienteSAP)?'':idClienteSAP);
        if(Test.isRunningTest()){
            gen.writeStringField( 'BUSINESSPLACE', '4' ); 
        }else{
            gen.writeStringField( 'BUSINESSPLACE', String.isBlank(caso.Aluno__r.Campus_n__r.CodigoSAP__c)?'':caso.Aluno__r.Campus_n__r.CodigoSAP__c ); 
        }     
        
        // gen.writeStringField( 'BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c ); 
        
        gen.writeStringField( 'REF_KEY_3', '' );  
        gen.writeDateField( 'BLINE_DATE', cobranca.DataVencimento__c );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'DSCT_DAYS1', String.valueOf( 5 ) ); 
        }else{
            gen.writeStringField( 'DSCT_DAYS1', '' ); 
        }  
        
        
        gen.writeStringField( 'DSCT_PCT1', '00000' ); 
        gen.writeStringField( 'DSCT_DAYS2', '0' );
        gen.writeStringField( 'DSCT_PCT2', '00000' );
        gen.writeStringField( 'ZINKZ', 'A' );
        gen.writeStringField( 'PYMT_METH', 'D' );
        gen.writeStringField( 'SP_GL_IND', ' ' );

        if(Test.isRunningTest()){
            gen.writeStringField( 'BANK_ID', '4' );
        }else{
            gen.writeStringField( 'BANK_ID', String.isBlank(caso.Aluno__r.Institui_o_n__r.Banco_CRA__c) ? 'BRAD1' : caso.Aluno__r.Institui_o_n__r.Banco_CRA__c );
        }  
        
        

        //gen.writeStringField( 'BANK_ID', titulo.Instituicao__r.Banco__c );
        if(!String.isBlank(caso.Aluno__r.Institui_o_n__r.Conta_Caixa__c)){
            gen.writeStringField( 'HOUSEBANKACCTID', String.isBlank(caso.Aluno__r.Institui_o_n__r.Conta_Caixa__c)?'':caso.Aluno__r.Institui_o_n__r.Conta_Caixa__c );
        }
        else{
            gen.writeStringField( 'HOUSEBANKACCTID', 'CC001' );
        }
        
        //gen.writeStringField( 'ALLOC_NMBR', String.isBlank(caso.Assunto__r.Servico__r.Name)?'':caso.Assunto__r.Servico__r.Name );
        gen.writeStringField( 'ALLOC_NMBR', '0 PARC SMT ' + periodoLetivo );
        gen.writeStringField( 'MATRICULA', String.isBlank(caso.Aluno__r.R_A_do_Aluno__c)?'':caso.Aluno__r.R_A_do_Aluno__c );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'ITEM_TEXT', 'teste' );
        }else{
            gen.writeStringField( 'ITEM_TEXT', 'Cobrança de serviço referente a ' + caso.Assunto__r.Name);
        }  
        
        //gen.writeStringField('ZUONR', '0 PARC SMT ' + periodoLetivo);
        
        gen.writeStringField( 'SHKZG', 'D' );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'PYMT_AMT', String.valueOf(4 ) );
        }else{
            //gen.writeStringField( 'PYMT_AMT', String.valueOf( titulo.Inscricao__r.Amount ) );
            gen.writeStringField( 'PYMT_AMT', String.isBlank(String.valueOf(cobranca.Valor__c))?'':String.valueOf(cobranca.Valor__c));
        }  
        
        // gen.writeStringField( 'PYMT_AMT', String.valueOf( titulo.Inscricao__r.Amount ) );
        gen.writeEndObject();  
        gen.writeEndArray();            
        gen.writeFieldName('AccountItem'); 
        gen.writeStartArray();
        gen.writeStartObject();
        gen.writeStringField( 'REF_DOC_NO', cobranca.Name );
        gen.writeStringField( 'ITEMNO_ACC', '0000000002' );
        gen.writeStringField( 'GL_ACCOUNT', String.isBlank(caso.Aluno__r.Institui_o_n__r.Conta_CRA__c)?'':caso.Aluno__r.Institui_o_n__r.Conta_CRA__c );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'BUSINESSPLACE', '43' ); 
        }else{
            gen.writeStringField( 'BUSINESSPLACE', String.isBlank(caso.Aluno__r.Campus_n__r.CodigoSAP__c)?'':caso.Aluno__r.Campus_n__r.CodigoSAP__c ); 
        } 
        
        gen.writeStringField( 'COSTCENTER', ' ' );
        
        //gen.writeStringField( 'BUSINESSPLACE', titulo.ProcessoSeletivo__r.CodigoSAPCampus__c ); 
        if(Test.isRunningTest()){
            gen.writeStringField( 'PROFIT_CTR', '54' );
        }else{
            gen.writeStringField( 'PROFIT_CTR', String.isBlank(caso.Aluno__r.Institui_o_n__r.CentroLucro_CRA__c)?'':caso.Aluno__r.Institui_o_n__r.CentroLucro_CRA__c );
        } 
        
        
        //gen.writeStringField( 'PROFIT_CTR', titulo.Instituicao__r.CentroLucro__c );
        gen.writeStringField( 'ALLOC_NMBR', ' ' );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'ITEM_TEXT', 'teste' );
        }else{
            gen.writeStringField( 'ITEM_TEXT', 'Cobrança de serviço referente a ' + caso.Assunto__r.Name );
        } 
        
        // gen.writeStringField( 'ITEM_TEXT', titulo.ProcessoSeletivo__r.Name );
        gen.writeStringField( 'SHKZG', 'C' );
        
        if(Test.isRunningTest()){
            gen.writeStringField( 'AMT_DOCCUR', String.valueOf( 5 ) );
        }else{
            //gen.writeStringField( 'AMT_DOCCUR', String.valueOf( titulo.Inscricao__r.Amount ) );
            gen.writeStringField( 'AMT_DOCCUR', String.isBlank(String.valueOf(cobranca.Valor__c))?'':String.valueOf(cobranca.Valor__c) );
        }  
        //gen.writeStringField( 'AMT_DOCCUR', String.valueOf( titulo.Inscricao__r.Amount ) );
        gen.writeEndObject();
        gen.writeEndArray();
        gen.writeEndObject();  
        gen.writeEndArray();
        gen.writeEndObject(); 
        
        return gen.getAsString();
	 }
	 
	 public static void processChargingJsonBody( String jsonBody ) {
        Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( jsonBody );
        System.debug( '>>> postData ' + postData );
        Set<String> NamesSF = new Set<String>();
        
        for( Object objResult : ( List<Object> ) postData.get( 'InboundSapResult' ) ) {
            String errorMessage = '';
            Map<String, Object> result = ( Map<String, Object> ) objResult;
            String NameSF = String.valueOf( result.get( 'rEF_DOC_NOField' ));
            NamesSF.add(NameSF);
        }
        
        List<Cobranca__c> cobrancaList = [SELECT Name, Id FROM Cobranca__c WHERE Name IN: NamesSF];
        Map<String, Cobranca__c> cobrancaMap = new Map<String, Cobranca__c>();
        for(Cobranca__c cobranca : cobrancaList){
        	cobrancaMap.put(cobranca.Name, cobranca);
        }
        
        List<Cobranca__c> cobrancaToUpdate = new List<Cobranca__c>();
        for( Object objResult : ( List<Object> ) postData.get( 'InboundSapResult' ) ) {
            String errorMessage = '';
            Map<String, Object> result = ( Map<String, Object> ) objResult;
            system.debug('debug parameters: ' + (String) result.get( 'bELNRField' ) + (String) result.get( 'bUKRSField' ) + (String) result.get( 'gJAHRField' ) + '' + cobrancaMap.get(String.valueOf( result.get( 'rEF_DOC_NOField' ) ) ).Id);
            Cobranca__c cobranca = ChargingBO.getInstance().updateCreateSapCharging( cobrancaMap.get(String.valueOf( result.get( 'rEF_DOC_NOField' ) ) ).Id, (String) result.get( 'bELNRField' ), 
                                                                               (String) result.get( 'bUKRSField' ), (String) result.get( 'gJAHRField' ), String.valueOf(objResult)  );
            
            if( String.valueOf( result.get( 'bELNRField' ) ).equals( '$' ) ) {
                for( Object objMessage : ( List<Object> ) result.get( 'messagesField' ) ) {
                    Map<String, Object> resultMessage = ( Map<String, Object> ) objMessage;
                    errorMessage = (String) resultMessage.get( 'mESSAGEField' );
                }	
            }
            //ProcessorControl.inFutureContext = true;		
            cobrancaToUpdate.add(cobranca);
            system.debug('Erro: '+ errorMessage);
            //QueueBO.getInstance().updateQueue( queueId, errorMessage ); 
        }
        if(!cobrancaToUpdate.isEmpty())
            update cobrancaToUpdate;
    }
    
}