/*
    @author Diego Moreira
    @class Classe de negocio para consulta do resultado do vestibular
*/
public class SRM_ConsultaResultadoVestibularBO implements IProcessingQueue {
    /*
        Metodo de consulta e criação das filas
    */
    public static void execute() {
        Map<String, List<Opportunity>> mapOpportunity = new Map<String, List<Opportunity>>(); 
        List<String> processoSeletivoId = new List<String>();
        List<Processo_seletivo__c> processoSeletivoList = Processo_SeletivoDAO.getInstance().getProcessoSeletivosAbertos();

        for( Processo_seletivo__c processoSeletivo : processoSeletivoList ) {
            processoSeletivoId.add( processoSeletivo.Id );
        }

        for( Opportunity opportunity : OpportunityDAO.getInstance().getOpportunityByProcessoSeletivoId( processoSeletivoId ) ) {
            if ( mapOpportunity.containsKey( opportunity.Processo_Seletivo__c ) )
                mapOpportunity.get( opportunity.Processo_Seletivo__c ).add( opportunity );
            else
                mapOpportunity.put( opportunity.Processo_Seletivo__c, new List<Opportunity>{ opportunity } );    
        }


        for( String processoSeletivoKey : mapOpportunity.keySet() ) {
            Integer count = 0;
            List<Opportunity> opportunitiesToQueue = new List<Opportunity>();

            for( Opportunity opportunity : mapOpportunity.get( processoSeletivoKey ) ) {
                count++;
                opportunitiesToQueue.add( opportunity );

                if ( count >= 1000 ) {    
                    String jsonResult = SRM_ConsultaResultadoVestibularBO.getJsonRequest( opportunitiesToQueue );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.SEARCH_VESTIBULAR_RESULTS.name(), jsonResult );
                    count = 0;
                    opportunitiesToQueue.clear();
                } 
            }
            if( opportunitiesToQueue.size() > 0 ) {
                String jsonResult = SRM_ConsultaResultadoVestibularBO.getJsonRequest( opportunitiesToQueue );
                QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.SEARCH_VESTIBULAR_RESULTS.name(), jsonResult );
            }              
        }

    }

	/* 
        Processamento de fila 
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, payload );     
    }

    /*
		Processa a fila de atualização de criação de candidato no academus
		@param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null ) {
    		HttpRequest req = new HttpRequest();
	        req.setEndpoint( WSSetup__c.getValues( 'Academus-ConsultaResultadoVestibular' ).Endpoint__c );
	        req.setMethod( 'POST' );
	        req.setHeader( 'content-type', 'application/json' );
	        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'Academus-ConsultaResultadoVestibular' ).API_Token__c );
	        req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
	        req.setBody( payload );
	 		
	        try {
	            Http h = new Http();
	            HttpResponse res = h.send( req );

	            if( res.getStatusCode() == 200 ){
	            	processJsonResult( res.getBody() );
	            	QueueBO.getInstance().updateQueue( queueId, '' );	        							               					          
	            } else
	                QueueBO.getInstance().updateQueue( queueId, 'ConsultaResultadoVestibular / ' + res.getStatusCode() + ' / ' + res.getStatus() );
	        } catch ( CalloutException ex ) {
	            QueueBO.getInstance().updateQueue( queueId, 'ConsultaResultadoVestibular / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
	        } catch ( Exception ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'ConsultaResultadoVestibular / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
        } else {
        	QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }
    }

    /*
        Processa o json de retorno 
        @param jsonResult JSON de retorno do serviço
    */
    private static void processJsonResult( String jsonResult ) {
        List<Opportunity> opportunityToUpsert = new List<Opportunity>();

        for( Object result : ( List<Object> ) JSON.deserializeUntyped( jsonResult ) ) {
            Map<String, Object> mapResult = ( Map<String, Object> ) result;

            if( !String.valueOf( mapResult.get( 'Resultado' ) ).equals( '' ) ) {
                Opportunity opportunity                 = new Opportunity();
                opportunity.CodigoInscricao__c          = ( String ) mapResult.get( 'Candidato' );
                opportunity.StageName                   = ( String ) mapResult.get( 'Resultado' );
                opportunity.CodigoProcessoCandidato__c  = mapResult.get( 'Processo' ) + '-' + mapResult.get( 'Candidato' );
                opportunity.Curso_Aprovado__c           = ( String ) mapResult.get( 'Curso' );

                opportunityToUpsert.add( opportunity );
            }
        }
        ProcessorControl.inFutureContext = true;
        OpportunityDAO.getInstance().upsertData( opportunityToUpsert, Opportunity.CodigoProcessoCandidato__c );
    }

    /*
		Retorna o Json 
    */
    public Static String getJsonRequest( List<Opportunity> opportunities ) {
    	JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartArray();
            for( Opportunity opportunity : opportunities ) {
                gen.writeStartObject();
                gen.writeStringField( 'idInstituicao', opportunity.Processo_Seletivo__r.Id_Institui_o__c );           
                gen.writeStringField( 'idPeriodo', opportunity.Processo_Seletivo__r.IdPeriodo__c );
                gen.writeStringField( 'idProcesso', opportunity.Processo_Seletivo__r.ID_Academus_ProcSel__c );
                gen.writeStringField( 'codCadastro', opportunity.CodigoInscricao__c );
                gen.writeEndObject(); 
            }           
        gen.writeEndArray();

    	return gen.getAsString(); 
    }
}