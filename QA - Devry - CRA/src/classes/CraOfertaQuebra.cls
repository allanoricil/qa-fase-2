global class CraOfertaQuebra {
	public static List<OfertaQuebra> getInclusaoQuebra( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
               
            //List<CraOfertaQuebraResult> inclusaoQuebraList = new List<CraOfertaQuebraResult>();

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraOfertaQuebra').Endpoint__c,new String[]{codcoligada,ra,codFilial});
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraOfertaQuebra' ).API_Token__c );
                
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );
            
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraOfertaQuebra / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraOfertaQuebra/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }

     private static List <OfertaQuebra>  processJsonBody(String jsonBody ) {
        
        system.debug('jsonbody quebra: ' + jsonBody);
      
        Response SR = ( Response ) JSON.deserializeStrict( jsonBody, Response.class );
         
       	system.debug('SR::: ' + SR);
      
        if( !SR.CraOfertaQuebraResult.isEmpty() ){
           return  SR.CraOfertaQuebraResult;
       }
        else{
            return new List<OfertaQuebra>(); 
        }
       
    }
  
     public Class Response {
        public List<OfertaQuebra> CraOfertaQuebraResult;
    }
  
    global Class OfertaQuebra {

        public String CodColigada{get;set;}
        public String CodDisc{get;set;}
        public String CodTurma{get;set;}
        public String Horario{get;set;}
        public String Nome{get;set;}
        public String Valor{get;set;}
    }

}