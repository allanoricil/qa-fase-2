/*
    @author Diego Moreira
    @class Classe de negocio para consulta do status da inscrição
*/
public class SRM_ConsultaStatusInscricaoBO implements IProcessingQueue {
    /*
        Metodo de consulta e criação das filas
    */
    public static void execute() {
        List<Opportunity> opportunities = OpportunityDAO.getInstance().getInscricaoAprovadas();
        List<Opportunity> opportunitiesToQueue = new List<Opportunity>();
        Integer count = 0;
                  
        for ( Opportunity opp : opportunities ) {
            count++;
            opportunitiesToQueue.add(opp);

            if (count >= 1000){    
                String jsonResult = SRM_ConsultaStatusInscricaoBO.createRequestJson( opportunitiesToQueue );
                QueueBO.getInstance().createQueue( QueueEventNames.SEARCH_STATUS_INSCRIPTION.name(), jsonResult );
                count = 0;
                opportunitiesToQueue.clear();
            } 
        }
        if(opportunitiesToQueue.size() > 0){
            String jsonResult = SRM_ConsultaStatusInscricaoBO.createRequestJson( opportunitiesToQueue );
            QueueBO.getInstance().createQueue( QueueEventNames.SEARCH_STATUS_INSCRIPTION.name(), jsonResult );
        }               
        
        opportunitiesToQueue.clear();                 
    }

    /*
        Cria o request Json
        @param opportunities Oportunidades de pesquisa
    */
    public static String createRequestJson( List<Opportunity> opportunities ) {
        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeFieldName('pConsulta');
            gen.writeStartArray();                  
                
            for( Opportunity opportunity : opportunities ) {
                gen.writeStartObject();
                    gen.writeStringField( 'IDPROCSEL', opportunity.Processo_seletivo__r.C_digo_processo_seletivo__c ); 
                    gen.writeStringField( 'NUMEROINSCPROCSEL', opportunity.CodigoInscricao__c ); 
                    gen.writeStringField( 'CODCOLIGADA', opportunity.Processo_seletivo__r.C_digo_da_Coligada__c ); 
                gen.writeEndObject();  
            }                
                            
            gen.writeEndArray();
        gen.writeEndObject(); 

        return gen.getAsString();
    }    

    /* 
        Processa a fila de atualização
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, payload );     
    }

    /*
		Processa chamada para atualização dos status de inscrição do aluno
		@param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {
    	Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
    	
    	if( mapToken.get('200') != null ) {
    		HttpRequest req = new HttpRequest(); 
	        req.setEndpoint( WSSetup__c.getValues( 'ConsultaStatusInscricao' ).Endpoint__c );
	        req.setMethod( 'POST' );
	        req.setHeader( 'content-type', 'application/json' );
	        req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'ConsultaStatusInscricao' ).API_Token__c );
	        req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
            req.setTimeout( 120000 );
	        req.setBody( payload );
	 		
	        try {  
	            Http h = new Http();
	            HttpResponse res = h.send( req );

	            if( res.getStatusCode() == 200 ) {
	            	processJsonResult( res.getBody() );
                    QueueBO.getInstance().updateQueue( queueId, '' );		        							               					          
	            } else    
	                QueueBO.getInstance().updateQueue( queueId, 'ConsultaStatusInscricao / ' + res.getStatusCode() + ' / ' + res.getStatus() );
	        } catch ( CalloutException ex ) {
	            QueueBO.getInstance().updateQueue( queueId, 'ConsultaStatusInscricao / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
	        } catch ( Exception ex ) {
                QueueBO.getInstance().updateQueue( queueId, 'ConsultaStatusInscricao / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
            }
        } else {
        	QueueBO.getInstance().updateQueue( queueId, 'Token / ' + mapToken.get('401') );
        }	
    }

    /*
        Processa o json de retorno 
        @param jsonResult JSON de retorno do serviço
    */
    private static void processJsonResult( String jsonResult ) {
        List<Academico__c> academicoToUpsert = new List<Academico__c>();

        System.debug( '>>> jsonResult ' + jsonResult );
        Map<String, Object> postData = ( Map<String, Object> ) JSON.deserializeUntyped( jsonResult );
        List<Object> statusInscricao = ( List<Object> ) postData.get( 'ConsultaStatusInscricaoResult' );        
        List<String> keyList = new List<String>();

        for( Object objResult : statusInscricao  ) {
            Map<String, Object> result = ( Map<String, Object> ) objResult;
            keyList.add( String.valueOf( result.get( 'IDPROCACADEMUS' ) ) + '-' + String.valueOf( result.get( 'NUMEROINSCPROCSEL' ) ) );
        }

        Map<String, Opportunity> mapOpportunity = new Map<String, Opportunity>();

        for( Opportunity opportunity : OpportunityDAO.getInstance().getOpportunityByCodigoInscricao( keyList ) ) {
            mapOpportunity.put( opportunity.CodigoProcessoCandidato__c, opportunity );
        }
        
        for( Object objResult : statusInscricao  ) {
            Map<String, Object> result = ( Map<String, Object> ) objResult;  

            Academico__c academico = new Academico__c();
            academico.Name                  = String.valueOf( result.get( 'RA' ) );
            academico.RA__c                 = String.valueOf( result.get( 'RA' ) );
            academico.Curso__c              = String.valueOf( result.get( 'CURSO' ) );
            if( !String.valueOf( result.get( 'DTSITUACAO' ) ).equals( '' ) ) 
                academico.DataSituacao__c   = createDateTimeInstance( String.valueOf( result.get( 'DTSITUACAO' ) ) );
            academico.Semestre__c           = String.valueOf( result.get( 'SEMESTRE' ) );
            academico.SituacaoMatricula__c  = String.valueOf( result.get( 'STATUS' ) );
            academico.Turno__c              = String.valueOf( result.get( 'TURNO' ) );

            String key = String.valueOf( result.get( 'IDPROCACADEMUS' ) ) + '-' + String.valueOf( result.get( 'NUMEROINSCPROCSEL' ) );
            System.debug( '>>>key ' + key );
            if( mapOpportunity.get( key ) != null ) {
                academico.Aluno__c          = mapOpportunity.get( key ).AccountId;
                academico.Oportunidade__c   = mapOpportunity.get( key ).Id;
                academico.IES__c            = mapOpportunity.get( key ).Processo_seletivo__r.Institui_o_n__c;
                academico.Campus__c         = mapOpportunity.get( key ).Processo_seletivo__r.Campus__c;
                System.debug( '>>>mapOpportunity ' + mapOpportunity.get( key ) );
            }

            academicoToUpsert.add( academico );
        }
        ProcessorControl.inFutureContext = true;
        QueueDAO.getInstance().upsertData( academicoToUpsert, Academico__c.RA__c );
    }

    /*
        Monta a data e hora 
    */
    private static Datetime createDateTimeInstance( String dtSituacao ) {
        List<String> dtTimeToSplit = dtSituacao.split(' ');
        List<String> dtResult = dtTimeToSplit[0].split( '/' );
        List<String> timeResult = dtTimeToSplit[1].split( ':' );
        return Datetime.newInstance( Integer.valueOf( dtResult[2] ), Integer.valueOf( dtResult[1] ), Integer.valueOf( dtResult[0] ), Integer.valueOf( timeResult[0] ), Integer.valueOf( timeResult[1] ), Integer.valueOf( timeResult[2] ));
    }


	
}