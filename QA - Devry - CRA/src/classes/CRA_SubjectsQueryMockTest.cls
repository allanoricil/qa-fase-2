@isTest
global class CRA_SubjectsQueryMockTest implements HttpCalloutMock{
	global HttpResponse respond(HttpRequest req){
		//System.assertEquals('disciplina', req.getEndpoint());
		system.debug('respond: '+req.getBody());
		system.debug('endpointmock: '+req.getEndpoint());
		HttpResponse res = new HttpResponse();
		if(req.getEndpoint() == 'login'){
			res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"auth_token": "eeac0aecd45fb1d838e42c4e202e1614"}');
	        res.setStatusCode(200);
		} else if(req.getEndpoint() == 'disciplina'){
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"historicoResult":[{"ch": 60,"coddisc": "5ALCM","codperlet": "2017.2","disciplina": "Algoritmos Computacionais","faltafinal": null,"idturmadisc": 28401,"notafinal": null,"periodo": "1","qtdalunos": 24,"raking": 0,"tipo": "CURSANDO"},{"ch":60.0000,"coddisc":"5CANL","codperlet":"2017.2","disciplina":"Cálculo Instrumental","faltafinal":null,"idturmadisc":28403,"notafinal":null,"periodo":"1","qtdalunos":25,"raking":0,"tipo":"CURSANDO"}]}');
	        res.setStatusCode(200);
    	} else if(req.getEndpoint() == 'token'){
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"auth_token":"468ee7b1cda02d3de9590d45fc889022"}');
	        res.setStatusCode(200);
    	} else if(String.valueOf(req.getEndpoint()).contains('english')){
	        res.setHeader('Content-Type', 'application/json');
	        res.setBody('{"CraTurmasEPROResult":[]}');
	        res.setStatusCode(200);
    	}
    	else res.setStatusCode(401);
        //res.setStatus(String status);
        system.debug('res aqui: '+res.getBody());
        return res;	
	}
}