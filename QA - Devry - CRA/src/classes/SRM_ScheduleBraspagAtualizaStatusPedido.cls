/*
	@author Diego Moreira
    @class Classe schedule para gerar filas de atualização de status pedidos no braspag
*/
global class SRM_ScheduleBraspagAtualizaStatusPedido implements Schedulable {
	global void execute( SchedulableContext SC ) {
		List<Titulos__c> tituloList = TituloDAO.getInstance().getTitulosAtualizarStatusBraspag();

		if( tituloList.size() > 0 ) {
			String titulosJson = JSON.serializePretty( tituloList ); 
		
			QueueBO.getInstance().createQueue( QueueEventNames.UPDATE_BRASPAG_ORDER_STATUS.name(), titulosJson );
		}		
	}
}