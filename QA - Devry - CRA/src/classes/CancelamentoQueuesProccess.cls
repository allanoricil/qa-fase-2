public with sharing class CancelamentoQueuesProccess implements IProcessingQueue {
    public static void processingQueue(String queueId, String eventName, String payload) {
        if (eventName == 'CANCELAMENTO_CONTRATO_SAP') {
            cancelaNoSap(queueId, payload);
        } else if (eventName == 'CANCELAMENTO_CONTRATO_RM') {
            cancelaNoRm(queueId, payload);
        } else if (eventName == 'RETORNA_FATURAMENTO_RM') {
            retornaFaturamentoRm(queueId, payload);
        } else if (eventName == 'CRIA_CLIENTE_SAP') {
            criaClienteNoSap(queueId, payload);
        } else if (eventName == 'RETIRA_FATURAMENTO_RM') {
            retiraFaturamentoRm(queueId, payload);
        }
    }

    @future(Callout = true)
    public static void criaClienteNoSap(String queueId, String payload) {
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        Cancelamento__c canc = [select id, Coligada__c, Usu_rio_em_Atendimento__c, CPF__c, Cliente_SAP__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c from Cancelamento__c where Queue_ClienteSap__c =: queueId limit 1];

        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('ClienteSap').Endpoint__c);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('ClienteSap').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setBody(payload);
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {

                    String empresaSap = '';
                    String cpf = '';
                    String idCliente = '';
                    string response = res.getBody();
                    system.debug('xanaina ' + payload);
                    //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe ResponseItems
                    //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
                    system.debug('xanaina ' + response);
                    response = response.substring(0, 20) + '[' + response.substring(20, response.length() - 1) + ']}';
                    ServicosResponse SR = (ServicosResponse) JSON.deserializeStrict(response, ServicosResponse.class);

                    system.debug(SR + ' destaque1 ');
                    if (!SR.ClienteSapResult.isEmpty()) {
                        for (ResponseItems item: SR.ClienteSAPResult) {
                            cpf = item.cpf;
                            idCliente = item.id_cliente;
                            empresaSap = item.empresaSap;
                        }

                        cpf = cpf.replaceAll('[|.|-]', '');
                        //idCliente = idCliente.leftPad(10, '0');
                        canc.Cliente_SAP__c = idCliente;
                        canc.CPF__c = cpf;
                        update canc;
                        QueueBO.getInstance().updateQueue(queueId, '');
                    }

                } else {
                    QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                    system.debug('ClienteSap  / ' + res.getStatusCode() + ' / ' + res.getStatus());
                }
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                system.debug('ClienteSap  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                system.debug('ClienteSap  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    public Class ServicosResponse {
        public List < responseItems > ClienteSAPResult;
    }

    public Class ResponseItems {
        public String cpf;
        public String empresaSap;
        public String id_cliente;

    }

    @future(Callout = true)
    public Static void cancelaNoSap(String queueId, String payload) {
        Cancelamento__c canc = [select id, N_documento_SAP__c, Cliente_SAP__c, CPF__c, Empresa_SAP__c, CreatedDate, Filial_SAP__c, Finalizado__c, StageQueue__c, StageQueueRM__c, Per_odo_Letivo__c, Coligada__c, Usu_rio_em_Atendimento__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c, Queue__c from Cancelamento__c where Queue__c =: queueId limit 1];
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string filial;
        // if (canc.Filial_SAP__c != null) {
        //filial = canc.Filial_SAP__c;
        // } else {
        filial = '';
        //}
        //string ra = canc.RA__c;
        string ra = '';
        string exercicio = '';
        string referencias = '';
        string empresa = canc.Empresa_SAP__c;
        string cpf = canc.CPF__c;
        string clientesap = canc.Cliente_SAP__c;
        string nome = '';

        String dataCancelamento = canc.CreatedDate.format('dd/MM/yyyy');
        dataCancelamento = dataCancelamento.replace('/', '');
        string params = ('?NOME=' + nome + '&CPF=' + cpf + '&RA=' + ra + '&CLIENTE_SAP=' + clientesap + '&EMPRESA=' + empresa + '&FILIAL=' + filial + '&DATA_CANCEL=' + dataCancelamento + '&EXERCICIO=' + exercicio + '&REFERENCIAS=' + referencias);
        system.debug('xaniana ' + params);
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('CancelaContratoSap').Endpoint__c + params);
            //          req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/ZcancCancelaContratoSAP?NOME=NULL&CPF=04794787324&RA=NULL&CLIENTE_SAP=9399&EMPRESA=1000&FILIAL=NULL&DATA_CANCEL=05.02.2018&EXERCICIO=NULL&REFERENCIAS=NULL');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('CancelaContratoSap').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    system.debug('xaniana ' + res.getBody());
                    SapRetorno response = parseSap(res.getBody());
                    if (response.ZcancCancelaContratoSAPResult.cANCELADOField == 'X') {
                        QueueBO.getInstance().updateQueue(queueId, '');
                        canc.Finalizado__c = true;
                        canc.N_documento_SAP__c = response.ZcancCancelaContratoSAPResult.mENSAGEMField;
                    } else if (response.ZcancCancelaContratoSAPResult.cANCELADOField == '') {
                        QueueBO.getInstance().updateQueue(queueId, response.ZcancCancelaContratoSAPResult.mENSAGEMField + WSSetup__c.getValues('CancelaContratoSap').Endpoint__c + params);
                        canc.Finalizado__c = false;
                    }
                    update canc;
                } else {
                    QueueBO.getInstance().updateQueue(queueId, 'ZcancCancelaContratoSAP / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('CancelaContratoSap').Endpoint__c + params);
                    system.debug('ZcancCancelaContratoSAP / ' + res.getStatusCode() + ' / ' + res.getStatus());
                }
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ZcancCancelaContratoSAP / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('CancelaContratoSap').Endpoint__c + params);
                system.debug('ZcancCancelaContratoSAP / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ZcancCancelaContratoSAP / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('CancelaContratoSap').Endpoint__c + params);
                system.debug('ZcancCancelaContratoSAP / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    @future(Callout = true)
    public Static void cancelaNoRm(String queueId, String payload) {
        Cancelamento__c canc = [select id, Finalizado__c,Solicita_o__c, StageQueue__c, StageQueueRM__c, Per_odo_Letivo__c, Coligada__c, Usu_rio_em_Atendimento__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c, Queue_RM__c from Cancelamento__c where Queue_RM__c =: queueId limit 1];
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string codigoColigada = canc.Coligada__c;
        string idHabilitacaoFilial = canc.Id_Habilita_o__c;
        //string idPerlet = canc.Id_Per_odo_Letivo__c;
        string codperlet = canc.Per_odo_Letivo__c;
        string ra = canc.RA__c;
        string codOperacao = canc.Cod_Operac_o__c;
        string recCreatedBy = canc.RecCreatedBy__c;
        if (codperlet == null) {
            codperlet = '2017.2';
        }
        string params = ('?CODCOLIGADA=' + codigoColigada + '&IDHABILITACAOFILIAL=' + idHabilitacaoFilial + '&CODPERLET=' + codperlet + '&RA=' + ra + '&CODOPERACAO=' + codOperacao + '&RECCREATEDBY=' + recCreatedBy);
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('CancelaContratoRm').Endpoint__c + params);
            //req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/ZCANC_CANCELACONTRATO?CODCOLIGADA=1&IDHABILITACAOFILIAL=586&CODPERLET=2017.2&RA=13110267&CODOPERACAO=1&RECCREATEDBY=DB1004833');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('CancelaContratoRm').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    RmRetorno response = parse(res.getBody());
                    if (response.ZcancCancelaContratoResult == 0) {
                        QueueBO.getInstance().updateQueue(queueId, 'Não cancelou o contrato no RM - ' + WSSetup__c.getValues('CancelaContratoRm').Endpoint__c + params);
                        canc.Finalizado__c = true;
                        Case solict = [select id, CaseNumber, Status,Erro_nas_Procedures__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Erro_nas_Procedures__c = true;
                        update solict;
                    } else if (response.ZcancCancelaContratoResult == 1) {
                        QueueBO.getInstance().updateQueue(queueId, '');
                        canc.Finalizado__c = true;
                    } else if (response.ZcancCancelaContratoResult == 7) {
                        QueueBO.getInstance().updateQueue(queueId, 'Falha em retirar Faturamento do RM, encontrado mais de um  Id idHabilitacaoFilial encontrado ' + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
                        canc.Finalizado__c = true;
                        canc.Status_do_Atendimento__c = 'Erro - Varios Habilitacao Filiais';
                        Case solict = [select id, CaseNumber, Status,Varios_Id_Habilitacao__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Varios_Id_Habilitacao__c = true;
                        update solict;
                    }
                    update canc;
                } else {
                    QueueBO.getInstance().updateQueue(queueId, 'ZCANC_CANCELACONTRATO / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('CancelaContratoRm').Endpoint__c + params);
                    system.debug('ZCANC_CANCELACONTRATO / ' + res.getStatusCode() + ' / ' + res.getStatus());
                }
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_CANCELACONTRATO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('CancelaContratoRm').Endpoint__c + params);
                system.debug('ZCANC_CANCELACONTRATO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_CANCELACONTRATO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('CancelaContratoRm').Endpoint__c + params);
                system.debug('ZCANC_CANCELACONTRATO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    @future(Callout = true)
    public Static void retiraFaturamentoRm(String queueId, String payload) {
        Cancelamento__c canc = [select id, Finalizado__c,Solicita_o__c, StageQueue__c, Aluno__c, StageQueueRM__c, Per_odo_Letivo__c, Coligada__c, Usu_rio_em_Atendimento__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c, Queue__c from Cancelamento__c where Queue_RetiraFaturamentoRM__c =: queueId limit 1];
        //CancelamentoUtil.preencheDadosRm(canc);
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string codigoColigada = canc.Coligada__c;
        string idHabilitacaoFilial = canc.Id_Habilita_o__c;
        //string idPerlet = canc.Id_Per_odo_Letivo__c;
        string codperlet = canc.Per_odo_Letivo__c;
        string ra = canc.RA__c;
        string codOperacao = canc.Cod_Operac_o__c;
        string recCreatedBy = canc.RecCreatedBy__c;
        string params = ('?CODCOLIGADA=' + codigoColigada + '&IDHABILITACAOFILIAL=' + idHabilitacaoFilial + '&CODPERLET=' + codperlet + '&RA=' + ra + '&CODOPERACAO=' + codOperacao + '&RECCREATEDBY=' + recCreatedBy);
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
            //req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/ZCANC_RETIRAFATURAMENTO?CODCOLIGADA=1&IDHABILITACAOFILIAL=554&CODPERLET=2017.2&RA=05200323&CODOPERACAO=1&RECCREATEDBY=TOTVS');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('RetiraFaturamentoRM').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    RmRetorno response = parse(res.getBody());
                    system.debug('xanaina ' + res.getBody());
                    if (response.ZcancRetiraFaturamentoResult == 0) {
                        QueueBO.getInstance().updateQueue(queueId, 'Falha em retirar Faturamento do RM' + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
                        canc.Retirado_do_Faturamento_RM__c = false;
                        canc.Finalizado__c = true;
                        Case solict = [select id, CaseNumber, Status,Erro_nas_Procedures__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Erro_nas_Procedures__c = true;
                        update solict;
                    } else if (response.ZcancRetiraFaturamentoResult == 1) {
                        QueueBO.getInstance().updateQueue(queueId, '');
                        canc.Retirado_do_Faturamento_RM__c = true;
                    } else if (response.ZcancRetiraFaturamentoResult == 7) {
                        QueueBO.getInstance().updateQueue(queueId, 'Falha em retirar Faturamento do RM, encontrado mais de um  Id idHabilitacaoFilial encontrado ' + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
                        canc.Retirado_do_Faturamento_RM__c = false;
                        canc.Finalizado__c = true;
                        canc.Status_do_Atendimento__c = 'Erro - Varios Habilitacao Filiais';
                        Case solict = [select id, CaseNumber, Status,Varios_Id_Habilitacao__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Varios_Id_Habilitacao__c = true;
                        update solict;
                    }

                    system.debug('xanaina ' + canc.Retirado_do_Faturamento_RM__c);
                    update canc;

                } else {
                    system.debug('ZCANC_RETIRAFATURAMENTO  / ' + res.getStatusCode() + ' / ' + res.getStatus());
                    QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETIRAFATURAMENTO  / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
                }
            } catch (CalloutException ex) {
                system.debug('ZCANC_RETIRAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETIRAFATURAMENTO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
            } catch (Exception ex) {
                system.debug('ZCANC_RETIRAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETIRAFATURAMENTO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    @future(Callout = true)
    public Static void retornaFaturamentoRm(String queueId, String payload) {
        Cancelamento__c canc = [select id, Finalizado__c,Solicita_o__c, StageQueue__c, StageQueueRM__c, Per_odo_Letivo__c, Coligada__c, Usu_rio_em_Atendimento__c, Retirado_do_Faturamento_RM__c, Id_Habilita_o__c, RA__c, Cod_Operac_o__c, RecCreatedBy__c, Queue_RM__c from Cancelamento__c where Queue_RM__c =: queueId limit 1];
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string codigoColigada = canc.Coligada__c;
        string idHabilitacaoFilial = canc.Id_Habilita_o__c;
        //string idPerlet = canc.Id_Per_odo_Letivo__c;
        string codperlet = canc.Per_odo_Letivo__c;
        string ra = canc.RA__c;
        string codOperacao = canc.Cod_Operac_o__c;
        string recCreatedBy = canc.RecCreatedBy__c;
        string params = ('?CODCOLIGADA=' + codigoColigada + '&IDHABILITACAOFILIAL=' + idHabilitacaoFilial + '&CODPERLET=' + codperlet + '&RA=' + ra + '&CODOPERACAO=' + codOperacao + '&RECCREATEDBY=' + recCreatedBy);
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('RetornaFaturamentoRm').Endpoint__c + params);
            //req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/ZCANC_RETORNAFATURAMENTO?CODCOLIGADA=1&IDHABILITACAOFILIAL=554&CODPERLET=2017.2&RA=05200323&CODOPERACAO=1&RECCREATEDBY=TOTVS');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('RetornaFaturamentoRm').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    RmRetorno response = parse(res.getBody());
                    if (response.ZcancRetornaFaturamentoResult == 0) {
                        QueueBO.getInstance().updateQueue(queueId, 'Falha em retornar ao Faturamento do RM' + WSSetup__c.getValues('RetornaFaturamentoRm').Endpoint__c + params);
                        canc.Finalizado__c = true;
                        Case solict = [select id, CaseNumber, Status,Erro_nas_Procedures__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Erro_nas_Procedures__c = true;
                        update solict;
                    } else if (response.ZcancRetornaFaturamentoResult == 1) {
                        QueueBO.getInstance().updateQueue(queueId, '');
                        canc.Retirado_do_Faturamento_RM__c = false;
                        canc.Finalizado__c = true;
                        Case solict = [select id, CaseNumber, Status,Erro_nas_Procedures__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Erro_nas_Procedures__c = true;
                        update solict;
                    }else if (response.ZcancRetornaFaturamentoResult == 7) {
                        QueueBO.getInstance().updateQueue(queueId, 'Falha em retirar Faturamento do RM, encontrado mais de um  Id idHabilitacaoFilial encontrado ' + WSSetup__c.getValues('RetiraFaturamentoRM').Endpoint__c + params);
                        canc.Retirado_do_Faturamento_RM__c = false;
                        canc.Finalizado__c = true;
                        canc.Status_do_Atendimento__c = 'Erro - Varios Habilitacao Filiais';
                        Case solict = [select id, CaseNumber, Status,Varios_Id_Habilitacao__c from Case where id=:canc.Solicita_o__c];
                        solict.Status = 'Erro';
                        solict.Varios_Id_Habilitacao__c = true;
                        update solict;
                    }
                    update canc;
                } else {
                    system.debug('ZCANC_RETORNAFATURAMENTO  / ' + res.getStatusCode() + ' / ' + res.getStatus());
                    QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETORNAFATURAMENTO  / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('RetornaFaturamentoRm').Endpoint__c + params);
                }
            } catch (CalloutException ex) {
                system.debug('ZCANC_RETORNAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETORNAFATURAMENTO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('RetornaFaturamentoRm').Endpoint__c + params);
            } catch (Exception ex) {
                system.debug('ZCANC_RETORNAFATURAMENTO  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
                QueueBO.getInstance().updateQueue(queueId, 'ZCANC_RETORNAFATURAMENTO / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('RetornaFaturamentoRm').Endpoint__c + params);
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    public class RmRetorno {
        public Integer ZcancRetornaFaturamentoResult;
        public Integer ZcancCancelaContratoResult;
        public Integer ZcancRetiraFaturamentoResult;
    }


    public static RmRetorno parse(String json) {
        return (RmRetorno) System.JSON.deserialize(json, RmRetorno.class);
    }

    public class SapRetorno {
        public ZcancCancelaContratoSAPResult ZcancCancelaContratoSAPResult;
    }

    public class ZcancCancelaContratoSAPResult {
        public String cANCELADOField;
        public String mENSAGEMField;
    }


    public static SapRetorno parseSap(String json) {
        return (SapRetorno) System.JSON.deserialize(json, SapRetorno.class);
    }
}