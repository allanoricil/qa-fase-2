/*
	@author Diego Moreira
	@class Classe DAO do objeto queue
*/
public with sharing class QueueDAO extends SObjectDAO {
	/*
		Singleton
    */  
	private static final QueueDAO instance = new QueueDAO();    
    private QueueDAO(){}
    
    public static QueueDAO getInstance() {
        return instance;
    }

    /*
		Retorna a fila pelo Id
		@param queueId id da fila para reprocessamento
    */
    public List<Queue__c> getQueueById( String queueId ) {
    	return [SELECT Id, Name, EventName__c, Status__c, Payload__c, ProcessBySchedule__c, PayloadSap__c 
					FROM Queue__c
					WHERE Id = :queueId];
    } 

    /*
		Retorna filas por status
		@param queueStatus Status da fila para filtro 
	*/
	public List<Queue__c> getQueueByStatus( String queueStatus ) {
		return [SELECT Id, Name, EventName__c, Status__c, Payload__c, createdDate, ProcessBySchedule__c, PayloadSAP__c 
					FROM Queue__c
					WHERE Status__c = :queueStatus
					order by Name desc Limit 50];
	}

	/*
		Retorna filas por status
		@param queueStatus Status da fila para filtro
	*/
	public List<Queue__c> getQueueByStatus( List<String> queueStatus ) {
		return [SELECT Id, Name, EventName__c, Status__c, Payload__c, createdDate, ProcessBySchedule__c, PayloadSAP__c
					FROM Queue__c
					WHERE Status__c in :queueStatus
					order by Name desc
					Limit 50];
	}

	/*
		Retorna as filas pelo evento e status
		@param eventName nome do evento criado
		@param queueStatus status da fila
	*/
	public List<Queue__c> getQueueByEventNameAndStatus( String eventName, String queueStatus, Integer numLimit ) {
		return [SELECT Id, Name, EventName__c, Status__c, Payload__c, createdDate, ProcessBySchedule__c, PayloadSAP__c,ObjectId__c
					FROM Queue__c
					WHERE EventName__c = :eventName
					AND Status__c = :queueStatus
					ORDER BY createdDate ASC
					Limit :numLimit];
	}

}