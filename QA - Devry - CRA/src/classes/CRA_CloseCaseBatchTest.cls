@isTest
public class CRA_CloseCaseBatchTest {
	public static testMethod void CRA_CloseCaseBatchTest() {
		Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;
        
        //Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Contact Center'];
        //Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        //User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id); 
        //insert usuario;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;
        
        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        List<Assunto__c> allAssuntos = new List<Assunto__c>();
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.SLA__c = direito.Id;
        assunto.Restringe_duplicidade__c = true;
        assunto.Tipo_Disciplinas__c = 'CURSANDO';
        assunto.Pagamento__c = false;
        assunto.Anexo__c = 'Sem anexo';
        assunto.Contact_Center__c = true;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;
        detalhe.Fim_de_Vigencia__c = Date.newInstance(2017, 12, 4);
        insert detalhe;
        
        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;

        List<Case> allCases = new List<Case>();
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Assunto__c = assunto.Id;
        caso.Status = 'Novo';
        caso.Description = 'Testanto classe teste';
        insert caso;

        Test.startTest();

            CRA_CloseCaseBatch obj = new CRA_CloseCaseBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();

	}
}