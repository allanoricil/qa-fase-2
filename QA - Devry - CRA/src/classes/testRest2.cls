@RestResource(urlMapping='/listaContas/*')
global class testRest2 {
	
	@HttpGet
	webservice static List<Account> getAccountList(){
		String cpf = RestContext.request.params.get('cpf');
		List<Account> contas = [Select id, Name,CPF_2__c from Account where CPF_2__c = :cpf ];
		return contas;
	}
}