/*
	@author Diego Moreira
	@interface interface responsável pelo processamento da fila
*/
public interface IProcessingQueue {
	void processingQueue( String queueId, String eventName, String payload );
}