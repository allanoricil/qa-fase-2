public without sharing class CRA_ReopenCaseController {

	public Boolean error{get; set;}
    public Case myCase {get;set;}

    public CRA_ReopenCaseController(ApexPages.StandardController stdController) {            
        List<String> fields = new List<String>();
        fields.add('Id');
        fields.add('Status');
        fields.add('Motivo_de_reabertura__c');
        
        if(!Test.isRunningTest()){
            stdController.addFields(fields);    
        }
        
        this.myCase = (Case)stdController.getRecord();
        
        if(this.myCase.Status != 'Concluído'){
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Olá, tudo bem? \r Você poderá reabrir sua solicitação quando receber a resposta e o status for concluído.'));
            error = true;            
        }
        
        this.error = false;
    }

    public PageReference reOpenCase(){
        if(String.isBlank(myCase.Motivo_de_reabertura__c)){
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Por favor, preencha um motivo para a reabertura.'));
        }
        else if( myCase.Motivo_de_reabertura__c.length() > 255){
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO, 'Texto muito longo, utilize no máximo 255 caracteres.'));
        }
        if(!error){
            try{
                myCase.Status = 'Em andamento';
                myCase.Sub_Status__c = 'Aluno contestou conclusão';
                update myCase;
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'Ocorreu um erro inesperado: '+ e.getMessage()));
                error = true;
            }
            if(!error)
                return goBack();
        }        
        return null;
    }

    public PageReference goBack(){
        PageReference pageRef = new PageReference('/' + myCase.Id);
        pageRef.setRedirect(true);
        return pageRef;
    }
}