global class Batch_AtualizaProprietarioOpp implements Schedulable {
	String ownerid;

	global Batch_AtualizaProprietarioOpp(String userId){
		ownerid = userId;
	}

	global void execute(SchedulableContext SC)  {                 
		List<Opportunity> opps = [SELECT o.Id FROM Opportunity o WHERE o.OwnerId =: ownerid];
		if(opps.size() > 0){
			List<Id> Ids = new List<Id>();
			for(integer i = 0; i<10; i++)
			{
				//Lista 20 oportunidades por vez pertencentes ao usuário que está sendo inativado e que ainda não foi atribuido ao Data Loader
				List<Opportunity> ownerOppLimit = [SELECT o.Id FROM Opportunity o WHERE o.OwnerId =: ownerid AND o.Id NOT IN: Ids LIMIT 20];

				for(Opportunity opp : ownerOppLimit)
				{
				//Id da oportunidade que será atribuída ao Data Loader
				Ids.add(opp.Id);
			}
			    //Chamada do Batch, passando as oportunidades que seram atribuídaa ao Data Loader.
			    Batch_UpdateOwnerOpportunity up = new Batch_UpdateOwnerOpportunity(ownerOppLimit);				
			    Database.executeBatch(up,20);
			}

			
			}else{
				system.debug('Usuário não tem oportunidades associadas.');
			}


		}   
	}