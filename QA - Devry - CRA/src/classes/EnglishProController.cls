public with sharing class EnglishProController{

    public string selectedOferta { get; set; }
    public string selectedCampus { get; set; }
    public string selectedSelectionProcess { get; set; }
    public List<SelectOption> options { get; set; }
    public List<SelectOption> programsItems { get; set; }
    // the soql without the order and limit
    private String soql {get;set;}
    private Boolean dadosCand {get;set;}
    public  Opportunity newOpportunity{get;set;}

    // the collection of Accounts to display
    public List<Account> accounts {get;set;}
    public List<opportunity> opp {get;set;}

    public EnglishProController(){
        newOpportunity = new Opportunity();
        getprocessosSeletivos();
        getOfertas();
    }

   /* public Boolean getDadosCand() {
        return dadosCand;
      }
    */
     public PageReference runSearch() {
    
        String cpf = Apexpages.currentPage().getParameters().get('cpf');
        soql = 'select id, Name,CPF_2__c, Cidade__c, Estado__c, PersonBirthdate, PersonMobilePhone, PersonEmail from account where account.CPF_2__c = \'' + cpf + '\' limit 1';
        runQuery();
    
        return null;
    }
     /*   
        public PageReference createLad() {
        
        if(String.valueOf(accounts[0].CPF_2__c) != null) 
        {
        insert New Lead(lastname = String.valueOf(accounts[0].name),
                        IES_de_interesse__c = ' ',
                        Status = 'Não inscrito', 
                        Phone = String.valueOf(accounts[0].PersonMobilePhone),
                        Cidade__c = String.valueOf(accounts[0].Cidade__c),
                        Estado__c = String.valueOf(accounts[0].Estado__c),
                        CPF__c = String.valueOf(accounts[0].CPF_2__c));

        }
        return null;
    }
     */   
        public PageReference limparForm() {
            
              PageReference newpage = new PageReference(System.currentPageReference().getURL());
              newpage.setRedirect(true);
              return newpage;
        
        }

            public void getprocessosSeletivos(){
                options = new List<SelectOption>();
                options.add(new SelectOption('','Selecionar uma Opção'));
                String selectedCampus = apexpages.currentpage().getparameters().get('parmSelectedCampus');
                List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Institui_o_n__c,p.name, p.Campus__c, 
                                                    p.Periodo_Letivo__c,p.Data_de_encerramento__c, Nome_do_Campus__c
                                             FROM Processo_seletivo__c p  
                                             WHERE p.Institui_o_n__c = :selectedCampus and 
                                                   p.Periodo_Letivo__c != null and 
                                                   p.Formul_rio_Salesforce__c = true and
                                                   p.Ativo__c = true and
                                                   p.Inscri_o__c = true and
                                                   p.Data_de_encerramento__c >= :system.today() and                                                
                                                   p.Name like '%english%'
                                             ORDER BY p.Campus__c ASC];
                for (Processo_seletivo__c proc : lstProcess)  {
                String nome = proc.Name;
                String campus = proc.Nome_do_Campus__c;
                String institution = proc.Institui_o_n__c;
                String session = proc.Data_de_encerramento__c.format();
                options.add(new SelectOption(proc.Id,nome+' - '+ campus));
                }
                }


            public List<SelectOption> getCampus(){
                List<SelectOption> Campus = new List<SelectOption>();
                Campus.add(new SelectOption('','Selecionar uma Opção'));
                List<Institui_o__c> lstCampus = [SELECT c.Id,c.Name
                                             FROM Institui_o__c c
                                             ORDER BY c.Name ASC];
                for (Institui_o__c ies : lstCampus)  {
                String nome = ies.Name;
                Campus.add(new SelectOption(ies.id,nome));
                }
                return Campus;
                }
    

            public void getOfertas(){
                programsItems = new List<SelectOption>();
                programsItems.add(new SelectOption('','Selecionar uma Opção'));
                String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedProcessoSeletivo');
                if(selectedProcess != null && selectedProcess != ''){
                    List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where  o.Processo_seletivo__c = :selectedProcess and Formul_rio_Salesforce__c = true];
                        for(Oferta__c o : lstOffers){
                        programsItems.add(new SelectOption(o.Id, o.Name));
                        }
                    }
            }
            
            public Pagereference OnchangeCampus(){
                getprocessosSeletivos();
                return null;
            }
            
            public Pagereference OnchangeSelectionProcess(){
                getOfertas();
                return null;
            }
            
            public PageReference OnchangeOferta() {
                return null;
            }
            
            public PageReference salvar() {
         try{
//             newOpportunity.Processo_seletivo__c = 'a06K0000004IzQp';
//           newOpportunity.oferta__c = 'a0LK0000003e0Bt';

             if(selectedSelectionProcess != null && selectedSelectionProcess != '')
             {
             newOpportunity.Processo_seletivo__c = selectedSelectionProcess;
             }
             if(selectedOferta != null && selectedOferta != '')
             {
             newOpportunity.oferta__c = selectedOferta;
             newOpportunity.Name = 'opp_Name';
             newOpportunity.StageName = 'Inscrito';
             newOpportunity.CloseDate = date.today();
             }
             
             
             insert newOpportunity;
               
               Integer counter = [ Select count() from opportunity Where Processo_seletivo__c = :selectedSelectionProcess And oferta__c = :selectedOferta];
               Integer counter2 = [ Select count() from opportunity Where Processo_seletivo__c = :selectedSelectionProcess];
//               String Oppname = [ Select Nome_do_Candidato__c from opportunity Where id = newOpportunity.id];
               
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Dados Salvos com Sucesso!'));
               //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Seu número de inscrição é ' + counter2 + '.' ));
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,  'Parabéns! Você é o ' + counter + '° inscrito para esse curso.'));

               newOpportunity.Posi_o_de_Inscri_o_Curso__c = String.valueOf(counter);
//             newOpportunity.Name = Oppname;
                
               upsert newOpportunity;
             newOpportunity = new Opportunity();
             getCampus();
             getprocessosSeletivos();
             getOfertas();
             
            } catch (Exception ex) {
               ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
             }       
             return null;
        }

        public void runQuery() {
    
        try {
              opp = Database.query(soql);

            } catch (Exception e) {
              soql = '1';
              ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Dados Não Encontrados'));
              
            }
 
            }
    
    public String debugSoql {
    get { return soql;}
    set;
    }

}