public with sharing class curriculoController {

    public string selectedCampus { get; set; }
    public string selectedSelectionProcess { get; set; }
    public string selectedOferta { get; set; }
    public string selectedStageName { get; set; }
    public boolean disable { get; set; }
    public List<SelectOption> options { get; set; }
    public List<SelectOption> programsItems { get; set; }
    public List<opportunity> opps { get; set; }
    
    public curriculoController(){
        disable = true;
        opps = new List<opportunity>();
        getprocessosSeletivos();
        getOfertas();
    }
    
    public void getprocessosSeletivos(){
                options = new List<SelectOption>();
                options.add(new SelectOption('','Selecionar uma Opção'));
                selectedCampus = apexpages.currentpage().getparameters().get('parmSelectedCampus');
                List<Processo_seletivo__c> lstProcess = [SELECT p.Id,p.Institui_o_n__c,p.name, p.Campus__c, 
                                                    p.Periodo_Letivo__c,p.Data_de_encerramento__c, Nome_do_Campus__c
                                             FROM Processo_seletivo__c p
                                             WHERE p.Institui_o_n__c = :selectedCampus and
                                                   p.Periodo_Letivo__c != null and 
                                                   p.Tipo_de_Matr_cula__c = 'Pós-Graduação' and
                                                   p.Ativo__c = true
                                             ORDER BY p.Campus__c ASC];
                for (Processo_seletivo__c proc : lstProcess)  {
                String nome = proc.Name;
                String campus = proc.Nome_do_Campus__c;
                String institution = proc.Institui_o_n__c;
                options.add(new SelectOption(proc.Id,nome+' - '+ campus));
                }
                if(!opps.isEmpty()) 
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Relatório gerado com sucesso'));
                    disable = false;
                }else
                {
                    disable = true;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Os filtros não retornaram nenhum valor.'));
                }
                
                }

            public void getOfertas(){
                programsItems = new List<SelectOption>();
                programsItems.add(new SelectOption('','Selecionar uma Opção'));
                String selectedProcess = apexpages.currentpage().getparameters().get('parmSelectedProcessoSeletivo');
                if(selectedProcess != null && selectedProcess != ''){
                    List<Oferta__c> lstOffers = [Select o.Id, o.Name From Oferta__c o where  o.Processo_seletivo__c = :selectedProcess];
                        for(Oferta__c o : lstOffers){
                        programsItems.add(new SelectOption(o.Id, o.Name));
                        }
                    }
                    
                if(!opps.isEmpty()) 
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Relatório gerado com sucesso'));
                    disable = false;
                }else
                {
                    disable = true;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Os filtros não retornaram nenhum valor.'));
                }
                    
            }

            public List<SelectOption> getCampus(){
                List<SelectOption> Campus = new List<SelectOption>();
                Campus.add(new SelectOption('','Selecionar uma Opção'));
                List<Institui_o__c> lstCampus = [SELECT c.Id,c.Name
                                             FROM Institui_o__c c
                                             ORDER BY c.Name ASC];
                for (Institui_o__c ies : lstCampus)  {
                String nome = ies.Name;
                Campus.add(new SelectOption(ies.id,nome));
                }
                
                if(!opps.isEmpty()) 
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Relatório gerado com sucesso'));
                    disable = false;
                }else
                {
                    disable = true;
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Os filtros não retornaram nenhum valor.'));
                }
                
                return Campus;
                }
                
            public List<opportunity> getInscricoes(){
                String selectedSelectionProcess = apexpages.currentpage().getparameters().get('parmSelectedProcessoSeletivo');
                String selectedProgram = apexpages.currentpage().getparameters().get('parmSelectedOferta');
                //String selectedStageName = apexpages.currentpage().getparameters().get('parmselectedStageName');
                opps = new List<opportunity>();
                opps = [SELECT o.Id,o.Nome_do_Candidato__c, o.Processo_Seletivo_Nome__c, o.Graduado_no_curso_de__c,
                                                      o.Na_institui_o__c, o.Curso_p_s_gradua_o__c, o.Caso_sim_Qual_Empresa__c, o.Cargo__c,
                                                      o.Tempo_no_cargo__c, o.Experi_ncia_principal_em__c, o.Outra_Experi_ncia__c, 
                                                      o.Possui_experi_ncia_internacional__c, o.Habilidade_em_ingl_s__c, o.Id_processo_RM__c,
                                                      o.CPF__c, o.Processo_seletivo__r.Institui_o_n__r.C_digo_da_Coligada__c, o.stagename
                                             FROM opportunity o
                                             where o.Processo_seletivo__c = :selectedSelectionProcess and
                                                   o.Processo_seletivo__c <> null and
                                                   o.Oferta__c = :selectedProgram and
                                                   o.stagename = 'MATRICULADO'
                                             ORDER BY o.Nome_do_Candidato__c ASC limit 999];

                if(!opps.isEmpty()) 
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Relatório gerado com sucesso'));
                    disable = false;
                }else
                {
                    disable = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Os filtros não retornaram nenhum valor.'));
                }
                
                
                return opps;
                }
                
            public Pagereference OnchangeCampus(){
                getprocessosSeletivos();
                getOfertas();
                return null;
            }
            
            public Pagereference OnchangeSelectionProcess(){
                getOfertas();
                //getInscricoes();
                return null;
            }
            
            public PageReference OnchangeOferta() {
                getInscricoes();
                return null;
            }

}