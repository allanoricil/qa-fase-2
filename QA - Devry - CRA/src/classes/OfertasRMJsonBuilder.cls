public abstract class OfertasRMJsonBuilder {

	public static String offers2Json(List<Oferta__c> offers){
		JSONGenerator gen = JSON.createGenerator(true);
		gen.writeStartObject();
		gen.writeFieldName('Ofertas');
		gen.writeStartArray();
        for(Oferta__c offer:offers){  
        	gen.writeStartObject();	
        	gen.writeFieldName('Oferta');
        	gen.writeObject(offer);
        	gen.writeEndObject();
        }
        gen.writeEndArray();
        gen.writeEndObject();
        return gen.getAsString(); 
	}
}