public with sharing class EstornaTaxasRequerimentoBO implements IProcessingQueue {
    public static void processingQueue(String queueId, String eventName, String payload) {
        if(eventName == 'RETORNA_CLIENTE_SAP'){
        	criaClienteNoSap(queueId, payload);
        }else if(eventName == 'ESTORNO_TAXAS_REQUERIMENTO'){
        	cancelaNoSap(queueId, payload);
        }
    }

    @future(Callout = true)
    public Static void cancelaNoSap(String queueId, String payload) {
    	String cobrancaId = [select ObjectId__c from Queue__c where id=: queueId].ObjectId__c;
        Cobranca__c cobr = [select id, name,CPF__c,CreatedDate,Cliente_SAP__c,EmpresaSAP__c, Log_Estorno__c from Cobranca__c where id=:cobrancaId limit 1];
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        string filial = '';
        string empresa = cobr.EmpresaSAP__c;
        string cpf = cobr.CPF__c;
        string clientesap = cobr.Cliente_SAP__c;
        String dataEstorno = cobr.CreatedDate.format('dd/MM/yyyy');
        dataEstorno = dataEstorno.replace('/', '');
        
        String params = ('?CPF=' + cpf + '&CLIENTE_SAP=' + clientesap + '&EMPRESA=' + empresa + '&FILIAL=' + filial + '&DATA_CANCEL=' + dataEstorno);
        system.debug('xaniana ' + params);
        
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('EstornaTaxaReqSAP').Endpoint__c + params);
            //          req.setEndpoint('http://testedevry1.academusportal.com.br/RestServiceImpl.svc/v1/EstornaTaxaReqSap?NOME=NULL&CPF=04794787324&RA=NULL&CLIENTE_SAP=9399&EMPRESA=1000&FILIAL=NULL&DATA_CANCEL=05.02.2018&EXERCICIO=NULL&REFERENCIAS=NULL');
            req.setMethod('GET');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('EstornaTaxaReqSAP').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {
                    system.debug('xaniana ' + res.getBody());
                    SapRetorno response = parseSap(res.getBody());
                    if (response.EstornaTaxaReqSAPResult.cANCELADOField == 'X') {
                        QueueBO.getInstance().updateQueue(queueId, '');
                        cobr.Log_Estorno__c = response.EstornaTaxaReqSAPResult.mENSAGEMField;
                    } else if (response.EstornaTaxaReqSAPResult.cANCELADOField == '') {
                        QueueBO.getInstance().updateQueue(queueId, response.EstornaTaxaReqSAPResult.mENSAGEMField + WSSetup__c.getValues('EstornaTaxaReqSAP').Endpoint__c + params);
                    	cobr.Log_Estorno__c = response.EstornaTaxaReqSAPResult.mENSAGEMField;
                    }
                    update cobr;
                } else {
                    QueueBO.getInstance().updateQueue(queueId, 'EstornaTaxaReqSap / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('EstornaTaxaReqSAP').Endpoint__c + params);
                    system.debug('EstornaTaxaReqSap / ' + res.getStatusCode() + ' / ' + res.getStatus());
                }
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'EstornaTaxaReqSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('EstornaTaxaReqSAP').Endpoint__c + params);
                system.debug('EstornaTaxaReqSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'EstornaTaxaReqSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('EstornaTaxaReqSAP').Endpoint__c + params);
                system.debug('EstornaTaxaReqSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

   	@future(Callout = true)
    public static void criaClienteNoSap(String queueId, String payload) {
        Map < String, String > mapToken = AuthorizationTokenService.getAuthorizationToken();
        Cobranca__c cobr = [select id, name, CPF__c, Cliente_SAP__c from Cobranca__c where Cliente_SAP_Queue__c =: queueId limit 1];
        if (mapToken.get('200') != null) {
            HttpRequest req = new HttpRequest();
            req.setEndpoint(WSSetup__c.getValues('ClienteSap').Endpoint__c);
            req.setMethod('POST');
            req.setHeader('Content-Type', 'application/json');
            req.setHeader('API-TOKEN', WSSetup__c.getValues('ClienteSap').API_Token__c);
            req.setHeader('AUTH-TOKEN', mapToken.get('200'));
            req.setBody(payload);
            req.setTimeout(120000);
            try {
                Http h = new Http();
                HttpResponse res = h.send(req);

                if (res.getStatusCode() == 200) {

                    String empresaSap = '';
                    String cpf = '';
                    String idCliente = '';
                    string response = res.getBody();
                    //A variavel SR do tipo ServicosResponse recebe a estrutura do Json ja formatando para uma lista do tipo da classe ResponseItems
                    //Desta forma vc pode tratar o retorno como uma lista e realizar a interação normalmente acessado todos os campos vindos no json.
                    response = response.substring(0, 20) + '[' + response.substring(20, response.length() - 1) + ']}';
                    ServicosResponse SR = (ServicosResponse) JSON.deserializeStrict(response, ServicosResponse.class);

                    system.debug(SR + ' destaque1 ');
                    if (!SR.ClienteSapResult.isEmpty()) {
                        for (ResponseItems item: SR.ClienteSAPResult) {
                            cpf = item.cpf;
                            idCliente = item.id_cliente;
                            empresaSap = item.empresaSap;
                        }

                        cpf = cpf.replaceAll('[|.|-]', '');
                        idCliente = idCliente.leftPad(10, '0');
                        cobr.Cliente_SAP__c = idCliente;
                        cobr.CPF__c = cpf;
                        update cobr;
                        QueueBO.getInstance().updateQueue(queueId, '');
                    }

                } else {
                    QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + res.getStatusCode() + ' / ' + res.getStatus() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                    system.debug('ClienteSap  / ' + res.getStatusCode() + ' / ' + res.getStatus());
                }
            } catch (CalloutException ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                system.debug('ClienteSap  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            } catch (Exception ex) {
                QueueBO.getInstance().updateQueue(queueId, 'ClienteSap / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() + WSSetup__c.getValues('ClienteSap').Endpoint__c );
                system.debug('ClienteSap  / ' + ex.getMessage() + ' / ' + ex.getStackTraceString());
            }
        } else {
            system.debug('Token Cliente / ' + mapToken.get('401'));
        }
    }

    public Class ServicosResponse {
        public List < responseItems > ClienteSAPResult;
    }

    public Class ResponseItems {
        public String cpf;
        public String empresaSap;
        public String id_cliente;

    }

    public class SapRetorno {
        public EstornaTaxaReqSAPResult EstornaTaxaReqSAPResult;
    }

    public class EstornaTaxaReqSAPResult {
        public String cANCELADOField;
        public String mENSAGEMField;
    }


    public static SapRetorno parseSap(String json) {
        return (SapRetorno) System.JSON.deserialize(json, SapRetorno.class);
    }

}