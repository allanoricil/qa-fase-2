global class LeadWebService {
    webService static Id CreateLead(String pLastName, String pTelefone, String pTelefoneConfirm, String pEmail, String pEmailConfirm, String pIes, String pCelular,
    String p1Curso, String p2Curso, String p3Curso, String pRua, String pCidade, String pEstado, String pCep, String pPais,
    String pEnsinoMedio, String pAnoConclusao, String pCanalOrigem, String pIdQS) 
    {
        Lead l = new Lead(lastName = pLastName, Phone = pTelefone, Confirmar_Telefone__c = pTelefoneConfirm, Email = pEmail, Confirmar_e_mail__c = pEmailConfirm, IES_de_interesse__c = pIes, MobilePhone = pCelular,
        X1a_Opcao_de_Curso__c = p1Curso, X2a_Opcao_de_Curso__c  = p2Curso, X3a_Opcao_de_Curso__c = p3Curso,
        Street = pRua, City = pCidade, State = pEstado, PostalCode = pCep, Country = pPais, 
        Local_que_cursa_ou_cursou_ensino_medio__c = pEnsinoMedio, Em_caso_de_ja_ter_concluido_qual_ano__c =pAnoConclusao, 
        Canal_de_Origem__c=pCanalOrigem , Id_QuinStreet__c=pIdQS, Lead_QuinStreet__c = true);
        insert l;
        return l.id;
        
    }
}