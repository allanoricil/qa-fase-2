public class ItemDoCasoTriggerHandler{

    public static void handleBeforeInsert(List<Item_do_Caso__c> itensNew){

        //Busca os ids dos Casos, catálogo de precos e produtos relacionados aos Itens
        Set<Id> caseIds = ItemDoCasoTriggerHandler.getCasesIds(itensNew);
        Set<Id> priceBookIds = ItemDoCasoTriggerHandler.getPriceBooksIds(itensNew, caseIds);
        Set<Id> productIds = ItemDoCasoTriggerHandler.getProductsIds(itensNew, caseIds);
        Map<String,PricebookEntry> prices = ItemDoCasoTriggerHandler.getPriceBookEntries(priceBookIds, productIds);

        //Atualiza o serviço e o preço no registro do Item
        ItemDoCasoTriggerHandler.setItemProduct(itensNew, caseIds);
        ItemDoCasoTriggerHandler.setItemPrice(itensNew, prices, caseIds);

    }
    
    public static void handleBeforeUpdate(List<Item_do_Caso__c> itensNew, List<Item_do_Caso__c> itensOld){}

    public static void handleBeforeDelete(List<Item_do_Caso__c> itensOld){}

    public static void handleAfterInsert(List<Item_do_Caso__c> itensNew){}

    public static void handleAfterUpdate(List<Item_do_Caso__c> itensNew, List<Item_do_Caso__c> itensOld){}

    public static void handleAfterDelete(List<Item_do_Caso__c> itensOld){}

    private static Set<Id> getCasesIds(List<Item_do_Caso__c> itensNew){
        
        Set<Id> caseIds = new Set<Id>();
        
        for (Item_do_Caso__c item : itensNew)
            caseIds.add(item.Caso__c);
        
        return caseIds;

    }

    private static Set<Id> getProductsIds(List<Item_do_Caso__c> itensNew, Set<Id> caseIds){
        
        //Map dos Casos
        Map<id,Case> casos = new Map<id,Case>([SELECT id, Aluno__r.Campus_n__r.Catalogo_de_precos__c, Assunto__r.Servico__c FROM Case WHERE Id IN: caseIds]);
        Set<Id> productIds = new Set<Id>();
        
        for (Item_do_Caso__c item : itensNew){
            Case caso = casos.get(item.Caso__c);
            productIds.add(caso.Assunto__r.Servico__c);
        }
        
        return productIds;

    }

    private static Set<Id> getPriceBooksIds(List<Item_do_Caso__c> itensNew, Set<Id> caseIds){

        //Map dos Casos
        Map<id,Case> casos = new Map<id,Case>([SELECT id, Aluno__r.Campus_n__r.Catalogo_de_precos__c, Assunto__r.Servico__c FROM Case WHERE Id IN: caseIds]);
        Set<Id> priceBooksIds = new Set<Id>();
        
        for (Item_do_Caso__c item : itensNew){
            Case caso = casos.get(item.Caso__c);
            priceBooksIds.add(caso.Aluno__r.Campus_n__r.Catalogo_de_precos__c);
        }

        return priceBooksIds;

    }

    private static Map<String,PricebookEntry> getPriceBookEntries(Set<Id> priceBookIds, Set<Id> productIds){

        //Map dos Preços
        Map<String,PricebookEntry> prices = new Map<String,PricebookEntry>();      
        for(PricebookEntry p : [SELECT id, pricebook2id, name, Product2Id, UnitPrice FROM PricebookEntry WHERE pricebook2id IN: priceBookIds AND Product2Id IN: productIds])
            prices.put(p.pricebook2id+''+p.Product2Id, p);

        return prices;

    }

    private static void setItemProduct(List<Item_do_Caso__c> itensNew, Set<Id> caseIds){

        //Map dos Casos
        Map<id,Case> casos = new Map<id,Case>([SELECT id, Aluno__r.Campus_n__r.Catalogo_de_precos__c, Assunto__r.Servico__c FROM Case WHERE Id IN: caseIds]);
        
        for (Item_do_Caso__c item : itensNew){
            Case caso = casos.get(item.Caso__c);
            item.Servico__c = caso.Assunto__r.Servico__c;
        }

    }

    private static void setItemPrice(List<Item_do_Caso__c> itensNew, Map<String,PricebookEntry> prices, Set<Id> caseIds){

        //Map dos Casos
        Map<id,Case> casos = new Map<id,Case>([SELECT id, Aluno__r.Campus_n__r.Catalogo_de_precos__c, Assunto__r.Servico__c FROM Case WHERE Id IN: caseIds]);

        //Map dos Preços
        for (Item_do_Caso__c item : itensNew){
            Case caso = casos.get(item.Caso__c);
            PricebookEntry p = prices.get(caso.Aluno__r.Campus_n__r.Catalogo_de_precos__c+''+caso.Assunto__r.Servico__c);
            if(p==null)
                item.Valor__c = 0;
            else    
                item.Valor__c = p.UnitPrice;
        }

    }

}