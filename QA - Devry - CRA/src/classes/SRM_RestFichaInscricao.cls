/*
	@author Diego Moreira
	@class Classe de serviço para fica de inscrição
*/
@RestResource (urlMapping='/v1/fichainscricao/*')
global class SRM_RestFichaInscricao {
	/*
		Serviço de consulta de dados para fica de inscrição
    */
	@HttpGet
	global static String doGet() {
		String jsonResult;

		try {
			jsonResult = SRM_FichaInscricaoBO.getRecords( RestContext.request.params );
		} catch( Exception ex ) {
			jsonResult = SRM_FichaInscricaoBO.jsonResponse( 'NOK', ex.getMessage() );
		}		

		return jsonResult;
	}

	/*
		Serviço de recebimento dos dados de inscrição
	*/
	@HttpPost
	global static String doPost() {
		String status = 'OK';
		String mensagem = '';		
		String jsonResult = RestContext.request.requestBody.toString();

		try {
			String queueId = QueueBO.getInstance().createQueueByScheduleProcess( RestContext.request.params.get('event'), jsonResult );
			Queue__c queue = QueueDAO.getInstance().getQueueById( queueId )[0];

			if( queue.EventName__c.equals( QueueEventNames.FICHA_INSCRICAO.name() ) )
	   			return SRM_FichaInscricaoBO.processaFichaInscricao( queue.Id, queue.Payload__c );
	   		else
	   			return SRM_FichaInscricaoBO.processaFichaCaptacao( queue.Id, queue.Payload__c );

		} catch( DmlException ex ) {
			status = 'NOK';
			mensagem = ex.getMessage();
		} catch ( Exception ex ) {
			status = 'NOK';
			mensagem = ex.getMessage();
		}

		return SRM_FichaInscricaoBO.jsonResponse( status, mensagem );		
	}
}