public class CRA_EmailUtil {

	@Future
	public static void sendCaseEmail(Set<Id> caseIds, String templateName){

		List<Case> cases = [SELECT Id, Account.PersonContactId FROM Case WHERE Id IN :caseIds];

		//Grava a data de previsão do marco "Conclusão do Caso" para utilização no template de email
		/*
		Map<Id,CaseMilestone> milestones = getCaseMilestone(caseIds);
		for(Case caso: cases)
			caso.CRA_Data_Hora_de_Previsao_para_SLA__c = (milestones.get(caso.Id)!=null ? milestones.get(caso.Id).TargetDate : null);

		Database.update(cases);
		*/
		
		Id templateId = [SELECT Id FROM EmailTemplate where Name = :templateName].Id;
		
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'salesforce.service@adtalembrasil.com.br'];
        
		for(Case c : cases){

			try{
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setOrgWideEmailAddressId(owea.get(0) != null ? owea.get(0).Id: '');
				mail.setTargetObjectId(c.Account.PersonContactId);
				mail.setWhatId(c.Id);
				mail.setTemplateId(templateId);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}catch(Exception e){ 
				//Aguardando definição de negócio quando não email da conta é inválido
			}

		}
	}

	public static void sendCaseEmailNow(Set<Id> caseIds, String templateName){

		List<Case> cases = [SELECT Id, Account.PersonContactId FROM Case WHERE Id IN :caseIds];

		//Grava a data de previsão do marco "Conclusão do Caso" para utilização no template de email
		/*
		Map<Id,CaseMilestone> milestones = getCaseMilestone(caseIds);
		for(Case caso: cases)
			caso.CRA_Data_Hora_de_Previsao_para_SLA__c = (milestones.get(caso.Id)!=null ? milestones.get(caso.Id).TargetDate : null);

		Database.update(cases);
		*/
		
		Id templateId = [SELECT Id FROM EmailTemplate where Name = :templateName].Id;
		
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address = 'salesforce.service@adtalembrasil.com.br'];
        
		for(Case c : cases){
			
			try{
				Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setOrgWideEmailAddressId(owea.get(0) != null ? owea.get(0).Id: '');
				mail.setTargetObjectId(c.Account.PersonContactId);
				mail.setWhatId(c.Id);
				mail.setTemplateId(templateId);
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
			}catch(Exception e){ 
				//Aguardando definição de negócio quando não email da conta é inválido
			}
		}
	}
	/*
	private static Map<Id,CaseMilestone> getCaseMilestone(Set<Id> caseIds){

		MilestoneType milestoneType = [SELECT Id FROM MilestoneType WHERE Name = 'Conclusão do Caso' LIMIT 1];
		List<CaseMilestone> caseMilestones = [SELECT CaseId, TargetDate FROM CaseMilestone WHERE CaseId IN :caseIds AND MilestoneTypeId = :milestoneType.Id];

		Map<Id,CaseMilestone> milestones = new Map<Id,CaseMilestone>();
		for(CaseMilestone m : caseMilestones)
			milestones.put(m.CaseId, m);

		return milestones;
	}
	*/
}