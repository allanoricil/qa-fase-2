/*
    @author Rogério Oliveira
    @class Classe DAO do objeto Opprtunity
*/
public with sharing class OpportunityDAO extends SObjectDAO {   
    /*
        Singleton
    */
    private static final OpportunityDAO instance = new OpportunityDAO();    
    private OpportunityDAO(){}
    
    public static OpportunityDAO getInstance() {
        return instance;
    }

    public static final String RECORDTYPE_GRADUACAO = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Graduação' ).getRecordTypeId();
    public static final String RECORDTYPE_MESTRADO = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Mestrado' ).getRecordTypeId();
    public static final String RECORDTYPE_EXTENSAO = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Cursos de Extensão' ).getRecordTypeId();
    //novas mudanças CRA
    public static final String RECORDTYPE_ENEM_ENTREVISTA = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'ENEM c/ entrevista' ).getRecordTypeId();
    public static final String RECORDTYPE_PROUNIFIES = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'PROUNI/FIES' ).getRecordTypeId();
    public static final String RECORDTYPE_POS_PROVA_ENTREVISTA = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Pós-Graduação c/ Prova + Entrevista' ).getRecordTypeId();
    public static final String RECORDTYPE_POS_ENTREVISTA = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Pós-Graduação c/ entrevista' ).getRecordTypeId();
    public static final String RECORDTYPE_ENEM = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'ENEM' ).getRecordTypeId();
    public static final String RECORDTYPE_PROUNI = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'PROUNI' ).getRecordTypeId();
    public static final String RECORDTYPE_POS = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Pós Graduação' ).getRecordTypeId();
    public static final String RECORDTYPE_VESTRADICIONAL = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Vestibular Tradicional' ).getRecordTypeId();
    public static final String RECORDTYPE_VESTAGENDADO = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Vestibular Agendado' ).getRecordTypeId();
    public static final String RECORDTYPE_PORTDIPLOMA = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Portador de diploma' ).getRecordTypeId();
    public static final String RECORDTYPE_TRANSFERENCIA = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Transferidos e Graduados' ).getRecordTypeId();
    public static final String RECORDTYPE_IB = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'IB' ).getRecordTypeId();
    public static final String RECORDTYPE_REABERTURAMAT = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get( 'Reabertura de Matrícula' ).getRecordTypeId();



    /*
        Consulta os leads via whereString
        @param whereString String de condição de filtro
    */
    public List<Opportunity> getOpportunityByWhereSting( String whereString ) {
        String queryString = 'SELECT Id, AguardandoRetornoBraspag__c, TotalTitulos__c, Account.Name, Processo_seletivo__r.Name, ' + 
                                'Processo_seletivo__r.Nome_da_IES__c, Processo_seletivo__r.Data_do_Vestibular__c, StageName ' + 
                                'FROM Opportunity WHERE ' + whereString +
                                ' AND RecordTypeId = ' + '\'' + RECORDTYPE_GRADUACAO + '\'' +
                                ' AND Processo_seletivo__r.Data_de_encerramento__c >= ' +String.valueOf( Date.today() );

        System.debug('>>> ' + queryString);                        
        return Database.query( queryString ); 
    }

    /*
        Retorna os dados da oportunidade 
        @param opportunityId id da oportunidade 
    */
    //ALTER: MERCHANTID
    /*
    public List<Opportunity> getOpportunityById( String opportunityId ) {
        return [SELECT Id, Name, StageName, Account.Name, Account.Cpf_2__c, Account.RG__C, Account.Rua__c, Account.N_mero__c, Processo_seletivo__c, CreatedDate,
                Account.CreatedDate, Account.Sexo__c, Account.DataNascimento__c, Account.Cidade__c, Account.Estado__c, Account.Nacionalidade__c, Account.CPFdoResponsavel__c,
                Account.Nome_do_Pai__c, Account.Nome_da_Mae__c, Account.Bairro__c, Account.CEP__c, Account.Phone, Account.Complemento__c, Account.PersonEmail,
                Processo_seletivo__r.Valor_Taxa_Inscricao__c, Processo_seletivo__r.Institui_o_n__c, Processo_seletivo__r.Vencimento_Boleto_dias__c,
                Processo_seletivo__r.Instrucoes_Boleto__c, Processo_seletivo__r.Merchant__c, Processo_seletivo__r.MerchantKey__c, Processo_seletivo__r.Id_Institui_o__c,
                Processo_seletivo__r.IdPeriodoLetivo__c, Processo_seletivo__r.ID_Academus_ProcSel__c, Processo_seletivo__r.C_digo_da_Coligada__c, Amount,
                X1_OpcaoCurso__r.ID_Academus_Oferta__c, X2_Op_o_de_curso__r.ID_Academus_Oferta__c, Data_Nascimento__c, Processo_Seletivo_Nome__c, NomeCampus__c, Account.Tipo_escola_cursa_ou_cursou_ensino_medio__c, Account.Colegio__r.Name
                    FROM Opportunity
                    WHERE Id = :opportunityId];
    }
    */
    
    /*
        Retorna os dados da oportunidade 
        @param opportunityId id da oportunidade 
    */
    public List<Opportunity> getOpportunityById( String opportunityId ) {
        return [SELECT Id, Name, StageName, Account.Name, Account.Cpf_2__c, Account.RG__C, Account.Rua__c, Account.N_mero__c, Processo_seletivo__c, CreatedDate,
                Account.CreatedDate, Account.Sexo__c, Account.DataNascimento__c, Account.Cidade__c, Account.Estado__c, Account.Nacionalidade__c, Account.CPFdoResponsavel__c,
                Account.Nome_do_Pai__c, Account.Nome_da_Mae__c, Account.Bairro__c, Account.CEP__c, Account.Phone, Account.Complemento__c, Account.PersonEmail,
                Processo_seletivo__r.Valor_Taxa_Inscricao__c, Processo_seletivo__r.Institui_o_n__c, Processo_seletivo__r.Campus__c, Processo_seletivo__r.Vencimento_Boleto_dias__c,
                Processo_seletivo__r.Instrucoes_Boleto__c, Processo_seletivo__r.Merchant_Id__c, Processo_seletivo__r.MerchantKey__c, Processo_seletivo__r.Id_Institui_o__c,
                Processo_seletivo__r.IdPeriodoLetivo__c, Processo_seletivo__r.ID_Academus_ProcSel__c, Processo_seletivo__r.C_digo_da_Coligada__c, Amount,
                X1_OpcaoCurso__r.ID_Academus_Oferta__c, X2_Op_o_de_curso__r.ID_Academus_Oferta__c, Data_Nascimento__c, Processo_Seletivo_Nome__c, NomeCampus__c, Account.Tipo_escola_cursa_ou_cursou_ensino_medio__c, Account.Colegio__r.Name
                    FROM Opportunity
                    WHERE Id = :opportunityId];
    }

    /*
        Retorna as oportunidades da conta e processo seletivo 
        @param accountId id da conta para pesquisa
        @param processoSeletivoId Id do processo seletido da oportunidade
    */
    public List<Opportunity> getOpportunityByAccountIdAndProcessoSeletivo( String accountId, String processoSeletivoId ) {
        return [SELECT Id, Name
                FROM Opportunity
                WHERE AccountId = :accountId
                AND Processo_seletivo__c = :processoSeletivoId
               	AND StageName != 'Cancelada'];
    }

    /*
        Retorna as oportunidades da conta e processo seletivo 
        @param accountId id da conta para pesquisa
        @param processoSeletivoId Id do processo seletido da oportunidade
    */
    public List<Opportunity> getOpportunityByCPFAndProcessoSeletivo( String cpf, String processoSeletivoId ) {
        return [SELECT Id, Name
                FROM Opportunity
                WHERE Account.CPF_2__c = :cpf
                AND Processo_seletivo__c = :processoSeletivoId];
    }
    
    /*
        Retorna as oportunidades de acordo com o Processo Seletivo, Oferta e CPF passados por parâmetro
        @param accountId id da conta para pesquisa
        @param processoSeletivoId Id do processo seletido da oportunidade
		@param accountId id da conta para pesquisa
    */
    public List<Opportunity> getDuplicateOpportunityByCPF( String processoSeletivoId, String ofertaId, String CPF ) {
        return [SELECT Id, Name
                FROM Opportunity
                WHERE Processo_seletivo__c = :processoSeletivoId
               	AND X1_OpcaoCurso__c = :ofertaId
                AND CPF__c = :cpf];
    }

    /*
        Retorna as inscrições do aluno aprovadas
    */
    public List<Opportunity> getInscricaoAprovadas() { 
        return [SELECT Id, Processo_seletivo__r.ID_Academus_ProcSel__c, CodigoInscricao__c, 
                    Processo_seletivo__r.C_digo_da_Coligada__c, Processo_seletivo__r.C_digo_processo_seletivo__c
                        FROM Opportunity
                        WHERE StageName = 'Aprovado'
                            AND CodigoInscricao__c != null
                            AND Processo_seletivo__r.Data_de_encerramento__c >= :Date.today()
                            AND RecordTypeId = :RECORDTYPE_GRADUACAO];
    }

    /*
        Retorna as inscrições do aluno aprovadas por processo seletivo
    */
    public List<Opportunity> getInscricaoAprovadasByProcessoSeletivoId(List<String> processoSeletivoId ) { 
        return [SELECT Id, Processo_seletivo__r.ID_Academus_ProcSel__c, CodigoInscricao__c, 
                    Processo_seletivo__r.C_digo_da_Coligada__c, Processo_seletivo__r.C_digo_processo_seletivo__c
                        FROM Opportunity
                        WHERE StageName = 'Aprovado'
                            AND CodigoInscricao__c != null
                            AND Processo_seletivo__r.Data_de_encerramento__c >= :Date.today()
                            AND RecordTypeId = :RECORDTYPE_GRADUACAO
                            AND Processo_seletivo__c IN :processoSeletivoId];
    }

    /*
        Retornas as oportunidades pelo codigo da inscrição
        @param codInscricao codigos de inscrição para consulta
    */
    public List<Opportunity> getOpportunityByCodigoInscricao( List<String> keyList ) {
        return [SELECT Id, Name, AccountId, CodigoInscricao__c, Processo_seletivo__r.Institui_o_n__c, CodigoProcessoCandidato__c,
                Processo_seletivo__r.Campus__c, Processo_seletivo__r.C_digo_Processo_Seletivo__c
                    FROM Opportunity
                    WHERE CodigoProcessoCandidato__c IN :keyList];
    }

    /*
        Retorna todas as oportunidades onde esteja em aberto o aluno no ACADEMUS
        @param processoSeletivoId Id do processo seletivo
    */
    public List<Opportunity> getOpportunityByProcessoSeletivoId( List<String> processoSeletivoId ) {
        return [SELECT Id, CodigoInscricao__c, Processo_Seletivo__r.Id_Institui_o__c, Processo_Seletivo__r.IdPeriodo__c, 
                    Processo_Seletivo__r.ID_Academus_ProcSel__c, Processo_Seletivo__c
                    FROM Opportunity
                    WHERE StageName IN ('Pré-Inscrito', 'Inscrito', 'Classificado', 'Ausente', 'Reprovado')
                    AND CodigoInscricao__c != null
                    AND Processo_Seletivo__c IN :processoSeletivoId];
    }

    /*
        Consulta a oportunidade que usa o codigo promocional
        @param idCodigoPromocional id do codigo promocional de pesquisa
    */
    public List<Opportunity> getOpportunityByPromotionalCode( String idCodigoPromocional ) {
        return [SELECT Id, Name
                FROM Opportunity
                WHERE CodigoPromocional__c = :idCodigoPromocional];
    }
    
    public List<Opportunity> getOpportunityByOwner(String owner, Integer maxResult) {
        return [SELECT Id, OwnerId FROM Opportunity WHERE OwnerId = :owner LIMIT :maxResult];
    }

}