public with sharing class Randomizer {
	
	public static Integer getNextInt(Integer size){
        Double d = math.random() * size;
    	return d.intValue();
    }
}