public without sharing class CRA_ItemUtil {
	public static Map<Id, Case> findDuplicates(List<Case> newCasesList) {
		Map<Id, Case> casesToReturn = new Map<Id, Case>();
        Set<Id> accountIds = new Set<Id>();
        Set<Id> newCasesIds = new Set<Id>();
        Map<Id, List<Case>> accountCases = new Map<Id, List<Case>>();
        
        for(Case caso: newCasesList){
            accountIds.add(caso.AccountId);
            newCasesIds.add(caso.Id);
        }
        
        Map<Id, Case> newCasesMap = new Map<Id, Case>([SELECT Id, Assunto__c, AccountId, Assunto__r.Restringe_duplicidade__c, (SELECT Name, Servico__c, Codigo_da_disciplina__c FROM Itens_do_caso__r) FROM Case WHERE Id IN: newCasesIds AND Assunto__r.Restringe_duplicidade__c = true ORDER BY AccountId]);
        
        Map<Id, Case> oldCasesMap = new Map<Id, Case>([SELECT Id, Assunto__c, AccountId, Assunto__r.Restringe_duplicidade__c, (SELECT Name, Servico__c, Codigo_da_disciplina__c FROM Itens_do_caso__r) FROM Case WHERE AccountId IN: accountIds AND Id NOT IN: newCasesIds AND (Status = 'Aguardando aluno' OR Status = 'Em andamento' OR Status = 'Em aprovação' OR Status = 'Aguardando pagamento') AND Assunto__r.Restringe_duplicidade__c = true ORDER BY AccountId]);
        
        system.debug('oldCasesMap duplicidade: ' + oldCasesMap);
        
        system.debug('newCasesMap duplicidade: ' + newCasesMap);
        
        for(Case oldCase : oldCasesMap.values()){
            if(accountCases.get(oldCase.AccountId) == null){
                accountCases.put(oldCase.AccountId, new List<Case>{oldCase});
            }
            else{
                accountCases.get(oldCase.AccountId).add(oldCase);
            }
        }
        
        for(Case newCase : newCasesMap.values()){
            if(accountCases.get(newCase.AccountId) != null){
                for(Case oldCase : accountCases.get(newCase.AccountId)){              
                    if(oldCase.Assunto__c == newCase.Assunto__c && casesToReturn.get(newCase.Id) == null){
                        if(!oldCase.Itens_do_caso__r.isEmpty() && !newCase.Itens_do_caso__r.isEmpty()){
                            for(Item_do_caso__c newItem : newCase.Itens_do_caso__r){
                                for(Item_do_caso__c oldItem : oldCase.Itens_do_caso__r){
                                    if(oldItem.Servico__c == newItem.Servico__c && oldItem.Codigo_da_disciplina__c == newItem.Codigo_da_disciplina__c){
                                    	casesToReturn.put(newCase.Id, newCase); 
                                    }
                                }                                
                            }
                        }
                        else{
                        	casesToReturn.put(newCase.Id, newCase);  
                        }                        
                    }
                }
            }
        }        
        return casesToReturn;
	}
}