/*
    @author Diego Moreira
    @class Classe controladora da pagina de geração de codigos promocionais
*/
public with sharing class SRM_GeradorCodigoPromocionalController {
    public CodigoPromocional__c formCodigoPromo                 { get; set; }
    public Integer qtdadeCodigos                                { get; set; }
    public List<CodigoPromocional__c> codigosToInsert           { get; set; }

    /*
        Construtor
    */
    public SRM_GeradorCodigoPromocionalController() {
        codigosToInsert = new List<CodigoPromocional__c>();
        formCodigoPromo = new CodigoPromocional__c();
    }

    /*
        Cria os codigos promocionais
        @action Botão gerar
    */
    public void geraCodigosPromocionais() { 
        if( validaInformacoes() ) {
            final Integer len = 6;
            final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
            codigosToInsert = new List<CodigoPromocional__c>();

            for( Integer i = 0; i < qtdadeCodigos; i++ ) {
              String randStr = '';
              while (randStr.length() < len) {
                 Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
                 randStr += chars.substring(idx, idx+1);
              }
              randStr  = randStr.toUpperCase();
              codigosToInsert.add( criaCodigoPromocional( randStr ) );
            }
        }       
    }

    /*
        Monta o objeto de codigo promocional
        @param codigo codigo promocional gerado
    */
    public CodigoPromocional__c criaCodigoPromocional( String codigo ) {
        CodigoPromocional__c codigoPromo = new CodigoPromocional__c();
        codigoPromo.Name                            = codigo;       
        codigoPromo.Expira__c                       = formCodigoPromo.Expira__c;
        codigoPromo.Desconto__c                     = formCodigoPromo.Desconto__c;
        codigoPromo.TipoDesconto__c                 = formCodigoPromo.TipoDesconto__c;
        codigoPromo.DescricaoCodigoPromocional__c   = formCodigoPromo.DescricaoCodigoPromocional__c;
        return codigoPromo;
    }

    /*
        Salva os codigos 
        @action botão salvar
    */
    public void save() {
        try {
            CodigoPromocionalDAO.getInstance().insertData( codigosToInsert );
            codigosToInsert = new List<CodigoPromocional__c>();
            Apexpages.addMessage( new ApexPages.Message( ApexPages.Severity.confirm, 'Códigos gerados com sucesso!' ) );
        } catch( DmlException ex ) {
            Apexpages.addMessage( new ApexPages.Message( ApexPages.Severity.error, ex.getMessage() ) );
        }
    }

    /*

    */
    private Boolean validaInformacoes() {
        Boolean isValid = true;
        if( qtdadeCodigos == null || qtdadeCodigos == 0 ) {
            Apexpages.addMessage( new ApexPages.Message( ApexPages.Severity.error, 'É necessário informar a quantidade de códigos a gerar!' ) );
            isValid = false;
        }

        return isValid;
    }
}