/*
    @author Diego Moreira
    @class Classe DAO do objeto Lead
*/
public with sharing class LeadDAO extends SObjectDAO {
	
	/*
        Singleton 
    */
    private static final LeadDAO instance = new LeadDAO();    
    private LeadDAO(){}
    
    public static LeadDAO getInstance() {
        return instance;
    }

    public static String RECORDTYPE_CAPTACAO = Schema.SObjectType.Lead.getRecordTypeInfosByName().get('Captação').getRecordTypeId();

    /*
        Consulta os dados pelo Id do Lead
        @param leadId Id de consulta do resgistro 
    */
    public List<Lead> getLeadById( String leadId ) {
        return [SELECT FirstName, LastName, Nacionalidade__c, RG__c, CPF__c, Passaporte__c, VoceEstrangeiro__c, VoceTreineiro__c, C_digo_Especial__c,
                Sexo__c, Phone, MobilePhone, Email, Data_de_Nascimento__c, Rua__c, N_mero__c, Complemento__c, Bairro__c, CEP__c, Cidade__c, Estado__c, OutroColegio__c, 
                Naturalidade__c, NomeCompletoResponsavel__c, CPFdoResponsavel__c, Tipo_de_Escola_cursa_cursou_ensino_medio__c, portador_de_necessidades_especiais__c,
                X1a_Op_o_de_Curso__c, X2a_Op_o_de_Curso__c, LeadOrigem__c, DataFimProcessoSeletivo__c, NomeProcessoSeletivo__c, CanalInscricao__c
                    FROM Lead
                    WHERE Id = :leadId];
    }

    /*
		Consulta os leads via whereString
		@param whereString String de condição de filtro
    */
    public List<Lead> getLeadByWhereSting( String whereString ) {
    	return Database.query( 'SELECT Id, Name, Processo_seletivo__r.Name, Processo_seletivo__r.Periodo_Letivo__c FROM Lead WHERE ' + whereString ); 
    }

    /*
        Consulta os dados do Lead que não tem o telefone valido
    */
    public List<Lead> getLeadCheckPhone() {
        String statusNovo = 'Novo';

        return [SELECT FirstName, LastName, Nacionalidade__c, RG__c, CPF__c, Passaporte__c, VoceEstrangeiro__c, VoceTreineiro__c, C_digo_Especial__c,
                Sexo__c, Phone, MobilePhone, Email, Data_de_Nascimento__c, Rua__c, N_mero__c, Complemento__c, Bairro__c, CEP__c, Cidade__c, Estado__c, OutroColegio__c, 
                Naturalidade__c, NomeCompletoResponsavel__c, CPFdoResponsavel__c, Tipo_de_Escola_cursa_cursou_ensino_medio__c, portador_de_necessidades_especiais__c,
                X1a_Op_o_de_Curso__c, X2a_Op_o_de_Curso__c, LeadOrigem__c, DataFimProcessoSeletivo__c, NomeProcessoSeletivo__c
                    FROM Lead
                    WHERE Status = :statusNovo
                    AND Telefone_Ok__c = null
                    AND RecordTypeId = :RECORDTYPE_CAPTACAO];
    }
}