public with sharing class Evaluation {

	private Integer OpenEvaluationId{get;private set;}
	private Integer Id{get;private set;}
	private String Name{get;private set;}
	private String StartDate{get;private set;}
	private String EndDate{get;private set;}
	List<Area> Areas{get;private set;}
	private Integer TimeDuration{get;private set;}
	private Boolean ResultAvailable{get;private set;}
	private Boolean Finalized{get;private set;}
	private Boolean TimeUp{get;private set;}
	private Boolean InProgress{get;private set;}
	private Boolean Initiated{get;private set;}
	private String Url{get;private set;}


	public Integer getOpenEvaluationId(){
		return OpenEvaluationId;
	}

	public Integer getId(){
		return Id;
	}

	public String getName(){
		return Name;
	}

	public String getStartDate(){
		return StartDate;
	}

	public String getEndDate(){
		return EndDate;
	}

	public List<Area> getAreas(){
		return Areas;
	}

	public Integer getTimeDuration(){
		return TimeDuration;
	}

	public Boolean getResultAvailable(){
		return ResultAvailable;
	}

	public Boolean getFinalized(){
		return Finalized;
	}

	public Boolean getTimeUp(){
		return TimeUp;
	}

	public Boolean getInProgress(){
		return InProgress;
	}

	public Boolean getInitiated(){
		return Initiated;
	}

	public String getUrl(){
		return Url;
	}


}