public without sharing class FinishExtensionController{
        public String oportunidadeId;
        public String nome { get; set;}
        public String sobrenome { get; set;}

        public FinishExtensionController(ApexPages.StandardController controller){
            getDados();
        }
        public void getDados(){
            oportunidadeId  = apexpages.currentpage().getparameters().get('id');
            Opportunity dados = [Select Id, Account.FirstName, Account.LastName from Opportunity where id=: oportunidadeId];
            nome = dados.Account.FirstName;
            sobrenome = dados.Account.LastName;
        }
    }