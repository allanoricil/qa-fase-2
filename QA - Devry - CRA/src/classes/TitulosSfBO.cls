/*
	@author Diego Moreira
	@class classe de serviço para objeto Titulos
*/
global class TitulosSfBO extends WSResponseSf {
	/*
		Singleton
    */
	private static final TitulosSfBO instance = new TitulosSfBO();    
    private TitulosSfBO(){}
    
    public static TitulosSfBO getInstance() {
        return instance;
    }

    /*
		Classe com os objetos de retorno
	*/	
	global class TitulosResponse {
		WebService Result response = new Result();
	}

	/* 
		Atualiza titulos pelo Web Service
		@param titulos Contexto do Web Service 
	*/
	public TitulosResponse atualizaTitulos( SRM_WSTitulosSf.Titulos titulos ) {
		List<Titulos__c> titulosToUpdate = new List<Titulos__c>();

 		for( SRM_WSTitulosSf.Rows row : titulos.row ) {
			Titulos__c titulo = new Titulos__c();
			titulo.Id 			= row.REF_DOC_NO; 
			titulo.CodigoSAP__c = row.BELNR; 
			titulo.Status__c	= row.STATUS;

			titulosToUpdate.add( titulo );
		}

		try {
			List<Database.SaveResult> updateDMLResult = TituloDAO.getInstance().updateData( titulosToUpdate );
			return successResponse();
		} catch ( DmlException ex ) {
			return errorResponse( ex );
		}
	}

	/*
		Retorno do serviço
	*/
	private TitulosResponse successResponse() {
		TitulosResponse responseResult = new TitulosResponse();
		responseResult.response.message.status 	= 'S';
		responseResult.response.message.msg 	= 'Titulos atualizados com sucesso!';
			
		return responseResult;
	}

	/*
		Retorno de DML exception
	*/
	private TitulosResponse errorResponse ( DmlException ex ) {
		TitulosResponse responseResult = new TitulosResponse();
		responseResult.response.message.status 	= 'E';
		responseResult.response.message.msg 	= ex.getMessage();
			
		return responseResult;
	} 
}