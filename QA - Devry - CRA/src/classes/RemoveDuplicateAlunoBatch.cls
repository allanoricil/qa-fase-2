global class RemoveDuplicateAlunoBatch implements Database.Batchable<sObject>{
	global Database.QueryLocator start(Database.BatchableContext BC) {

		return Database.getQueryLocator('SELECT R_A_do_Aluno__c, Aluno_de_destino__c, (SELECT Id, Aluno__c FROM Casos__r), (SELECT Id, Aluno_Atendido__c FROM Alunos_Atendidos_Monitoria__r), (SELECT Id, Aluno__c FROM Acompanhamentos_CASA__r), (SELECT Id, Aluno__c FROM Aluno_Eventos__r), (SELECT Id, Aluno__c FROM Atendimentos_Psicol_gicos__r), (SELECT Id, Aluno__c FROM Cadastros_de_Monitores__r), (SELECT Id, Aluno__c FROM Cancelamentos__r), (SELECT Id, Aluno__c FROM Desligamentos__r) FROM Aluno__c WHERE R_A_do_Aluno__c != null AND Conta__c != null AND IsDeleted = false AND Aluno_de_destino__c != null LIMIT 1000');

	}
    
    global void execute(Database.BatchableContext BC, List<Aluno__c> alunoList) {
        List<Case> caseList = new List<Case>();
        List<Aluno_Atendido_Monitoria__c> alunoAtendidoMonitoriaList = new List<Aluno_Atendido_Monitoria__c>();
        List<Aluno_Evento__c> alunoEventoList = new List<Aluno_Evento__c>();
        List<Atendimento_CASA__c> AcompanhamentoCASAList = new List<Atendimento_CASA__c>();
        List<Cancelamento__c> cancelamentoList = new List<Cancelamento__c>();
        List<Atendimento_Psicol_gico__c> atendimentoPsicoList = new List<Atendimento_Psicol_gico__c>();
        List<Cadastro_de_Monitor__c> cadastroMonitorList = new List<Cadastro_de_Monitor__c>();
        List<Desligamento__c> desligamentoList = new List<Desligamento__c>();
        for(Aluno__c aluno : alunoList){
            if(aluno.Casos__r.size() > 0){
                for(Case caso : aluno.Casos__r){
                    caso.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    caseList.add(caso);
                }
            }
            if(aluno.Alunos_Atendidos_Monitoria__r.size() > 0){
                for(Aluno_Atendido_Monitoria__c alunoAtendido : aluno.Alunos_Atendidos_Monitoria__r){
                    alunoAtendido.Aluno_Atendido__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    alunoAtendidoMonitoriaList.add(alunoAtendido);
                }
            }
            if(aluno.Acompanhamentos_CASA__r.size() > 0){
                for(Atendimento_CASA__c atendimento : aluno.Acompanhamentos_CASA__r){
                    atendimento.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    AcompanhamentoCASAList.add(atendimento);
                }
          }
            if(aluno.Aluno_Eventos__r.size() > 0){
                for(Aluno_Evento__c alunoEvento : aluno.Aluno_Eventos__r){
                    alunoEvento.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    alunoEventoList.add(alunoEvento);
                }
            }
            if(aluno.Atendimentos_Psicol_gicos__r.size() > 0){
                for(Atendimento_Psicol_gico__c atendimentoPsico : aluno.Atendimentos_Psicol_gicos__r){
                    atendimentoPsico.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    atendimentoPsicoList.add(atendimentoPsico);
                }
            }
            if(aluno.Cadastros_de_Monitores__r.size() > 0){
                for(Cadastro_de_Monitor__c cadastro : aluno.Cadastros_de_Monitores__r){
                    cadastro.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    cadastroMonitorList.add(cadastro);
                }
            }
            if(aluno.Cancelamentos__r.size() > 0){
                for(Cancelamento__c cancelamento : aluno.Cancelamentos__r){
                    cancelamento.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    cancelamentoList.add(cancelamento);
                }
            }
            if(aluno.Desligamentos__r.size() > 0){
                for(Desligamento__c desligamento : aluno.Desligamentos__r){
                    desligamento.Aluno__c = Id.valueOf(aluno.Aluno_de_Destino__c);
                    desligamentoList.add(desligamento);
                }
            }          
        }
        
        if(!caseList.isEmpty())
            update caseList;
        
        if(!alunoEventoList.isEmpty())
            update alunoEventoList;
        
        if(!alunoAtendidoMonitoriaList.isEmpty())
            update alunoAtendidoMonitoriaList;
        
        if(!atendimentoPsicoList.isEmpty())
            update atendimentoPsicoList;
        
        if(!AcompanhamentoCASAList.isEmpty())
            update AcompanhamentoCASAList;
        
        if(!cancelamentoList.isEmpty())
            update cancelamentoList;
        
        if(!cadastroMonitorList.isEmpty())
            update cadastroMonitorList;
        
        if(!desligamentoList.isEmpty())
            update desligamentoList;
        
    }
    
    global void finish(Database.BatchableContext BC) {
		
	}
}