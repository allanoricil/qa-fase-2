public class CalendarioBO{
    
    private String periodoLetivo;
    private Integer anoDoPeriodo;
    private Integer ultimoMes;
    private List<Evento_Calend_rio_Acad_mico__c> eventos;
    private boolean show = true;
    
    public CalendarioBO(String periodoLetivo, List<Evento_Calend_rio_Acad_mico__c> eventos){
        this.periodoLetivo = periodoLetivo;
        this.eventos = eventos;
    }
    
    public Map<Integer,List<DiaDoMes>> gerarCalendarioAcademico(){    
        Map<Integer,List<DiaDoMes>> calendarioMap = new Map<Integer,List<DiaDoMes>>();
        Date dataInicial = constroiDataInicial();
        system.debug('dataInicial ' + dataInicial.format());
        for(Integer i = dataInicial.month(); i <= ultimoMes; i++){
             Date clone = Date.newInstance(dataInicial.year(), i, dataInicial.day());
             Integer mes = i;
            List<DiaDoMes> diasDoMes = new List<DiaDoMes>();
           
            Integer lastDayOfMonth = clone.addMonths(1).toStartofMonth().addDays(-1).day();
            
            system.debug('lastDayOfMonth ' + lastDayOfMonth + ' mes ' + mes + ' ano ' + anoDoPeriodo);
            for(Integer indiceDia = 1; indiceDia <=  lastDayOfMonth; indiceDia++){
                Date data = Date.newInstance(anoDoPeriodo, mes,indiceDia);
                String nome = extrairDataNome(data);                
                DiaDoMes dia =  new DiaDoMes(indiceDia,nome);               
                dia.aplicacaoAp = this.isAplicacaoAp(data);
                dia.feriado = this.isFeriado(data);
                if(dia.nome == 'Sun'|| dia.nome == 'Sat'){
                    dia.aplicacaoAp  = false;
                }
                diasDoMes.add(dia);
            }
            calendarioMap.put(mes,diasDoMes);
        }
        return calendarioMap;
    }
    @TestVisible
    private Date constroiDataInicial(){
        String[] split = this.periodoLetivo.split('\\.');
        anoDoPeriodo = Integer.valueOf(split[0]);
        Integer numeroDoMes =  Integer.valueOf(split[1]) == 1 ? 1: 7;
        if(numeroDoMes == 1){
            ultimoMes = 6;
        }else{
            ultimoMes = 12;
        }
        Date dataBase = Date.newInstance(anoDoPeriodo, numeroDoMes, 1);   
        return dataBase;
    }
    @TestVisible
    private boolean isAplicacaoAp(Date data){
        boolean aplicacaoAp = false;
        if(this.eventos != null){
            Integer size = this.eventos.size();
            for(Integer index = 0; index < size; index++){
                Evento_Calend_rio_Acad_mico__c ev = eventos.get(index);     
                aplicacaoAp = (ev.Aplica_o_AP__c && (data  >= ev.Data_de_nicio__c && data <= ev.Data_de_T_rmino__c));
                if(aplicacaoAp){
                    break;
                } 
            }
        }
        return aplicacaoAp;
    }
    
    @TestVisible
    private  String extrairDataNome(Date data){
        DateTime DateTimeConvertida =  DateTime.newInstance(data.year(), data.month(),data.day() , 1, 0, 0);
        return DateTimeConvertida.format('E');
    }
    
    
    @TestVisible
    private boolean isFeriado(Date data){
        boolean feriado = false;
        if(eventos != null){
            for(Evento_Calend_rio_Acad_mico__c ev : eventos){
                feriado =  (ev.feriado__c && (data >= ev.Data_de_nicio__c && data <= ev.Data_de_T_rmino__c ));
                if(feriado){
                    break;
                }
            }
        }
        return feriado;
    }
    
    public class DiaDoMes {
        public integer index {set;get;}
        public boolean aplicacaoAp {set;get;}
        public  boolean feriado {set;get;}
        public String nome {set;get;}
        
        public DiaDoMes(integer index, boolean aplicacaoAp, boolean feriado){
            this.index = index;
            this.aplicacaoAp = aplicacaoAp;
            this.feriado = feriado;   
        }
        public DiaDoMes(integer index, String nome){
            this.index = index;
            this.nome = nome;
        }
        
    }
    
    
}