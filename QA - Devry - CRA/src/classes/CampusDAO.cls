/*
    @author Adílio Santos
    @class Classe DAO do objeto Campus
*/
public with sharing class CampusDAO extends SObjectDAO {
	/*
        Singleton
    */
    private static final CampusDAO instance = new CampusDAO();    
    private CampusDAO(){}    
    public static CampusDAO getInstance() {
        return instance;
    }

    /*
        Retornas o Campus pelo Id
        @param campusId id do Campus
    */
    public List<Campus__c> getCampusById( String campusId ) {
    	return [SELECT Id, Name
    			FROM Campus__c
    			WHERE Id = :campusId];
    }

}