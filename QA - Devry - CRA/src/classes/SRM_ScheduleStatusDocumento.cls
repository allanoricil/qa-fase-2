/*
    @author Diego Moreira
    @class Schedule de atualização de status documento
*/
global class SRM_ScheduleStatusDocumento implements Schedulable {
	global void execute(SchedulableContext sc) { 
		SRM_StatusDocumentoBO.execute();
	}
}