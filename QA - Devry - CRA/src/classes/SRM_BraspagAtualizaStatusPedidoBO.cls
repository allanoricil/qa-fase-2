/*
	@author Diego Moreira
    @class Classe de negocio para consulta e atualização do status de pedidos do braspag
    que não foram enviados no primeiro post de envio
*/
public class SRM_BraspagAtualizaStatusPedidoBO implements IProcessingQueue {
	private static Map<String, String> mapStatus;
    Static{
        mapStatus = new Map<String, String>();
        mapStatus.put('0', 'Indefinida');
        mapStatus.put('1', 'Pago');
        mapStatus.put('2', 'Autorizada');
        mapStatus.put('3', 'Não Autorizado');
        mapStatus.put('4', 'Cancelada');
        mapStatus.put('5', 'Estornada');
        mapStatus.put('6', 'Aguardando');
        mapStatus.put('7', 'Desqualificada');
    }
	/* 
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */     
    public static void processingQueue( String queueId, String eventName, String payload ) {
    	syncToServer( queueId, payload );     
    }

    /*
		@param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {
    	SRM_WSBraspagDecoratorPagadorQuery.OrderIdDataResponse responseOrderId;
    	SRM_WSBraspagDecoratorPagadorQuery.OrderDataResponse responseOrderData;
    	List<Titulos__c> titulosToUpdate = (List<Titulos__c>) JSON.deserialize( payload, List<Titulos__c>.class );

    	try {
    		for( Titulos__c titulo : titulosToUpdate ) {
                //ALTER: MERCHANTID
                /*
    			responseOrderData = SRM_BraspagCryptographyService.getInstance().braspagGetOrderData( titulo.ProcessoSeletivo__r.Merchant__c, titulo.CodigoBrasPag__c, GuidUtil.NewGuid() );
                */
                responseOrderData = SRM_BraspagCryptographyService.getInstance().braspagGetOrderData( titulo.ProcessoSeletivo__r.Merchant_Id__c, titulo.CodigoBrasPag__c, GuidUtil.NewGuid() );
	    		if( responseOrderData.TransactionDataCollection != null ) {
                    SRM_WSBraspagDecoratorPagadorQuery.OrderTransactionDataResponse orderDataResult = responseOrderData.TransactionDataCollection.OrderTransactionDataResponse[0];

                    System.debug('>>> orderDataResult ' + orderDataResult);
                    //titulo.CodigoBrasPag__c             = braspagOrderId;
                    titulo.TransacaoBoleto__c           = orderDataResult.BraspagTransactionId;
                    titulo.Status__c                    = mapStatus.get( orderDataResult.Status );
                    titulo.CodigoAutorizacao__c         = orderDataResult.AuthorizationCode;
                    
                    System.debug('>>> titulosToUpdate ' + titulo);
                }                
	    	}

	    	update titulosToUpdate;
            QueueBO.getInstance().updateQueue( queueId, '' );
    	} catch ( CalloutException ex ) {
            QueueBO.getInstance().updateQueue( queueId, 'UPDATE_BRASPAG_ORDER_STATUS / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        } catch ( Exception ex ) {
            QueueBO.getInstance().updateQueue( queueId, 'UPDATE_BRASPAG_ORDER_STATUS / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }    	
    }
}