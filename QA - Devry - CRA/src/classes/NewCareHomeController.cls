/*******************************************************************
Author    :   Adílio Santos
Date      :   Outubro 2016
Purpose   :   Controller Class for Page NewCareHome
*******************************************************************/

public with sharing class NewCareHomeController {

    public Case newCase {get;set;}
    public String formStep {get;set;}
	public String selectedEstado {get;set;}
	public String selectedCampus {get;set;}
	public String selectedTipoForm {get;set;}
	public String selectedMotivo {get;set;}
	public String selectedArea {get;set;}
	public String selectedTipoUser {get;set;}
    public List<SelectOption> campusItems {get;set;}
    public List<SelectOption> formsItems {get;set;}
    public List<SelectOption> motivosItems {get;set;}
    public List<SelectOption> areasItems {get;set;}
    public List<SelectOption> TipoUsersItems {get;set;}
    
    public String pageLanguage{
        get{
            String lang = ApexPages.currentPage().getParameters().get('lang');
            if( lang != null && lang.length() > 0){
                return lang;
            }
            return 'pt_BR';
        }
        set;
    }
    
    /* constructor */
    public NewCareHomeController(){
        this.formStep = 'form';
        newCase = new Case();
                        
        fillCampus();
        fillTipoForm();
        fillArea();
        fillTipoUser();
        fillMotivo();
    }
    
    public List<SelectOption> getAllSelectionEstadoItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        Map<String, Estados__c> estados = Estados__c.getAll();
        List<String> estadosNome = new List<String>();
        estadosNome.addAll(estados.keySet());
        estadosNome.sort();
        for (String e : estadosNome) {
            Estados__c estado = estados.get(e);
            //if(estado.Name == 'BA' || estado.Name == 'CE' || estado.Name == 'PE' || estado.Name == 'RJ' || estado.Name == 'DF' || estado.Name == 'MG') {
            options.add(new SelectOption(estado.Name, estado.Name));
            //}
        }
        return options;
    }
    
    /*
    public List<SelectOption> getAllSelectionCampusItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','Selecionar uma Opção'));
        List<Campus__c> lstCampus = [Select c.Id, c.Name From Campus__c c where c.Estado__c = :selectedEstado];
        for (Campus__c c : lstCampus){
            String campus = c.Name;
            options.add(new SelectOption('',campus));
        }
        return options;
    }  
    */
    
    public Pagereference OnchangeSelectionEstado(){
		fillCampus();
		return null;
    }
    
    public Pagereference OnchangeSelectionTipoForm(){
		fillMotivo();
		return null;
    }
    
    public Pagereference save(){
        try{
            newCase.Instituicao_sede__c = selectedCampus;
            newCase.DeVry_Care_rea__c = selectedArea;
            
            List<DeVry_Care__c> lstUserType = [Select d.Id, d.Name From DeVry_Care__c d where d.ID =: selectedTipoUser limit 1];
            newCase.Tipo_de_usuario__c = lstUserType[0].Name;
            
            if(selectedMotivo == '' || selectedMotivo == null){
                List<DeVry_Care__c> lstCare = [Select c.Id, c.Name From DeVry_Care__c c where 
                                               c.Status__c = 'Ativo' and c.Name = 'N/A'];
                
                for(DeVry_Care__c c : lstCare){
                    if(c.Name == 'N/A'){
                        selectedMotivo = c.Id;
                        break;
                    }
                }
            }
            newCase.DeVry_Care_Motivo__c = selectedMotivo;
            newCase.Data_do_contato__c = System.today();
            newcase.Origin = 'Form. DeVry Care';
            insert newCase;
            this.formStep = 'finish';
        }
        catch(Exception ex){
            System.debug('>>>>Exception in Save'+ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
        return null;
    }
    
    /*
    private Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(0)),Integer.valueOf(myDateOnly.get(1)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }
	*/
    
    private void fillCampus(){
        campusItems = new List<SelectOption>();
        campusItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedEstado = apexpages.currentpage().getparameters().get('parmSelectedSelectiionEstado');
        if(selectedEstado != null && selectedEstado != ''){
            List<Campus__c> lstCampus = [Select c.Id, c.Name, c.Institui_o__c From Campus__c c where 
                                         c.Estado__c = :selectedEstado];
            for(Campus__c c : lstCampus){
                campusItems.add(new SelectOption(c.Id, c.Name));
            }
        }
    }
    
    private void fillMotivo(){
        motivosItems = new List<SelectOption>();
        motivosItems.add(new SelectOption('','Selecionar uma Opção'));
        String selectedTipoForm = apexpages.currentpage().getparameters().get('parmSelectedTipoForm');
        if(selectedTipoForm != null && selectedTipoForm != ''){
            List<DeVry_Care__c> lstCare = [Select c.Id, c.Name From DeVry_Care__c c where 
                                           c.Status__c = 'Ativo' and c.Tipo_registro__c = :selectedTipoForm];
            
            for(DeVry_Care__c c : lstCare){
                if(c.Name != 'N/A')
                	motivosItems.add(new SelectOption(c.Id, c.Name));
            }
        }
    }
    
    private void fillTipoForm(){
        formsItems = new List<SelectOption>();
        formsItems.add(new SelectOption('','Selecionar uma Opção'));
        
        List<DeVry_Care__c> lstCare = [Select c.Tipo_registro__c From DeVry_Care__c c
                                       where c.Tipo_registro__c not in ('Área','Tipo usuário')
                                         and c.Status__c = 'Ativo'
                                       ORDER BY c.Tipo_registro__c DESC];
        
        String lastInsert = '';
        
        for(DeVry_Care__c c : lstCare){
            if(c.Tipo_registro__c != lastInsert){
                formsItems.add(new SelectOption(c.Tipo_registro__c,c.Tipo_registro__c));
                lastInsert = c.Tipo_registro__c;
            }
        }
    }
    
    private void fillArea(){
    	areasItems = new List<SelectOption>();
        areasItems.add(new SelectOption('','Selecionar uma Opção'));
        List<DeVry_Care__c> lstCare = [Select c.Id, c.Name From DeVry_Care__c c where c.Tipo_registro__c = 'Área' and c.Status__c = 'Ativo'];
        for(DeVry_Care__c c : lstCare){
            areasItems.add(new SelectOption(c.Id, c.Name));
        } 
    }
    
    private void fillTipoUser(){
    	TipoUsersItems = new List<SelectOption>();
        TipoUsersItems.add(new SelectOption('','Selecionar uma Opção'));
        List<DeVry_Care__c> lstCare = [Select c.Id, c.Name From DeVry_Care__c c where c.Tipo_registro__c = 'Tipo usuário' and c.Status__c = 'Ativo'];
        for(DeVry_Care__c c : lstCare){
            TipoUsersItems.add(new SelectOption(c.Id, c.Name));
        }
    }
    
    public void atualizaDadosAluno(){
        List<Aluno__c> lstAluno = [Select a.Id, a.Name, a.Celular__c, a.e_mail__c From Aluno__c a where a.R_A_do_Aluno__c =: newCase.R_A_do_Aluno__c limit 1];
        if(!lstAluno.isEmpty())
        {
            newCase.Nome_do_Aluno__c = lstAluno[0].Name.substring(0,lstAluno[0].Name.IndexOf('-'));            
            newCase.Telefone_do_Aluno__c = lstAluno[0].Celular__c;
            newCase.Email_do_Aluno__c = lstAluno[0].e_mail__c;
        }
        else
        {
            newCase.Nome_do_Aluno__c = '';
            newCase.Telefone_do_Aluno__c = '';
            newCase.Email_do_Aluno__c = '';
        }
    }
}