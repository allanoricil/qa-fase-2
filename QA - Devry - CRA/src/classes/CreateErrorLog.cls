global class CreateErrorLog {
     @future
     public static void createErrorRecord(string exceptionMessage, string sourceCode){
         Error_Log__c newErrorRecord = new Error_Log__c();
         newErrorRecord.Details__c = exceptionMessage;
         newErrorRecord.Source_Code__c = sourceCode;
         Database.insert(newErrorRecord,false);
     }
}