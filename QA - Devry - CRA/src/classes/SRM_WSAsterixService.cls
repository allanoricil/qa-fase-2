public class SRM_WSAsterixService {

	/*
        Singleton
    */
    private static final SRM_WSAsterixService instance = new SRM_WSAsterixService();    
    private SRM_WSAsterixService(){}
    
    public static SRM_WSAsterixService getInstance() {
        return instance;
    }

    /*
		Chama o serviço Asterix para validar o telefone
		@param objeto
		@param id
		@param phone
		@param mobile 
    */ 
    public void asterixCheckPhone( List<Lead> listLead ) {
    	
		List<Lead> updateLead = new List<Lead>();

        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;


        for(Lead lead : listLead){
            lead.Telefone_Ok__c = 'Sim';
            lead.setOptions(dmo);
            updateLead.add(lead);
        }

        update updateLead;

    }

    
}