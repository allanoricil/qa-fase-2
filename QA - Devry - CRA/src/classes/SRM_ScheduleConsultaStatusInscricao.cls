/*
	@author Diego Moreira
	@class Classe schedule de consulta do status da inscrição
*/
global class SRM_ScheduleConsultaStatusInscricao implements Schedulable {
	global void execute( SchedulableContext SC ) {
		//List<Opportunity> opportunities = OpportunityDAO.getInstance().getInscricaoAprovadas();

		//String json = SRM_ConsultaStatusInscricaoBO.createRequestJson( opportunities );
		//QueueBO.getInstance().createQueue( QueueEventNames.SEARCH_STATUS_INSCRIPTION.name(), json );

		SRM_ConsultaStatusInscricaoBO.execute();
	}
}