public without sharing class CRA_PhoneUtil {
	public static void fixOpportunityPhone(List<Opportunity> newOppList){
		List<Account> AccountToUpdate = new List<Account>();
		for(Opportunity newOpp : newOppList){
			if(newOpp.Celular_form__c != '' && newOpp.Celular_form__c != null && newOpp.Celular_form__c != '0'){
				String telefonenumero = newOpp.Celular_form__c.replaceAll('[^0-9]', '');
				if (telefonenumero.length()<12)
					telefonenumero = '55'+telefonenumero;
				Account acc = new Account(
					Id = newOpp.AccountId,
					telefonenumero__c = telefonenumero
				);
				AccountToUpdate.add(acc);
			}
		}

		if(!AccountToUpdate.isEmpty())
			update AccountToUpdate;

	}

	public static void fixLeadPhone(List<Lead> leadNew){
		for(Lead newLead : leadNew){
			if(newLead.MobilePhone != null && newLead.MobilePhone != ''){
                system.debug('tel: '+newLead.MobilePhone);
				String telefonenumero = newLead.MobilePhone;
                telefonenumero = telefonenumero.replaceAll('[^0-9]', '');
                system.debug(telefonenumero);
				if (telefonenumero.length()<12)
					telefonenumero = '55'+telefonenumero;
				newLead.telefonenumero__c = telefonenumero;
			}
		}
	}

	public static void fixAccountPhone(Map<Id, Account> mapAccNew, Map<Id, Account> mapAccOld){

	}

}