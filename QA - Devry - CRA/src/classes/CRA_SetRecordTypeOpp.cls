public with sharing class CRA_SetRecordTypeOpp {
    public static void setRT(List<Opportunity> allOpps) {
        for(Opportunity myOpp : allOpps){
            Id id2Return;
            system.debug('metodo de ingresso: '+myOpp.Processo_Metodo_de_ingresso__c);
            system.debug('tipo do proesso seletivo: '+myOpp.Processo_Tipo_do_processo_seletivo__c);
            system.debug('niveis de ensino: '+myOpp.Processo_Niveis_de_ensino__c);
            system.debug('precisa de prova? '+myOpp.Processo_precisa_de_prova__c);
            system.debug('precisa de entrevista? '+myOpp.Processo_precisa_de_entrevista__c);
             system.debug('valor do recordtype '+myOpp.RecordTypeId);
            //List<Processo_seletivo__c> allProcess = new List<Processo_seletivo__c>();
            //allProcess = [SELECT Id, Data_do_Vestibular__c, Cobrar_Taxa_de_Inscri_o__c, Precisa_de_prova__c, Precisa_de_entrevista__c FROM Processo_seletivo__c WHERE Id = :myOpp.Processo_Seletivo__c];
            if(myOpp.Processo_Metodo_de_ingresso__c == 'Vestibular'){
                //tipo de registro de vetibular tradiciona
                id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Vestibular Tradicional').getRecordTypeId();
            } else if(myOpp.Processo_Metodo_de_ingresso__c == 'Seleção'){
                if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Vestibular Especial'){
                    //tipo de registro de vestibular Agendado
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Vestibular Agendado').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'ENEM'){
                    if(myOpp.Processo_precisa_de_entrevista__c){
                        //enem com entrevista
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ENEM c/ entrevista').getRecordTypeId();
                    } else{
                        //enem
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('ENEM').getRecordTypeId();
                    }
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Portador de Diploma'){
                    //portador de diploma
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Portador de diploma').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Transferência Externa'){
                    //tranferencia externa
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Transferidos e Graduados').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Retorno MI'){
                    //Reabertura de matrícula
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Reabertura de Matrícula').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'PROUNI' || myOpp.Processo_Tipo_do_processo_seletivo__c == 'FIES'){
                    //prouni/fies
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('PROUNI/FIES').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'IB'){
                    //IB
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('IB').getRecordTypeId();
                }
                else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Pós-Graduação IBMEC'){
                    //pos ibmec
                    id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pós Graduação IBMEC').getRecordTypeId();
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Pós-Graduação'){
                    if(myOpp.Processo_precisa_de_entrevista__c && myOpp.Processo_precisa_de_prova__c){
                        //pos com entrevista e prova
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pós-Graduação c/ Prova + Entrevista').getRecordTypeId();
                    } else if(myOpp.Processo_precisa_de_entrevista__c){
                        // pos com entrevista
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pós-Graduação c/ entrevista').getRecordTypeId();
                    } else {
                        //pos
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Pós Graduação').getRecordTypeId();
                    }
                } else if(myOpp.Processo_Tipo_do_processo_seletivo__c == 'Outra Origem'){
                    if(myOpp.Processo_Niveis_de_ensino__c == 'Mestrado'){
                        //Mestrado
                        id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Mestrado').getRecordTypeId();
                    }
                } else{
                    //RT padrão
                    //id2Return = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Graduação').getRecordTypeId();
                }
            } 

            if(id2Return != null){
                myOpp.RecordTypeId = id2Return;
            }
        }       
    }
}