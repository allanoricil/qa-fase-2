/*
    @author Diego Moreira
    @class Classe controladora da pagina de pagamento inscrição
*/
public with sharing class SRM_BraspagController {	
	public String Id_Loja 				{ get; private set; }
	public String VENDAID 				{ get; private set; }
	public String NOSSONUMERO 			{ get; private set; }
	public String VALOR 				{ get; private set; }
	public String NOME 					{ get; private set; }
	public String CPF 					{ get; private set; }
	public String CODPAGAMENTO 			{ get; private set; }
	public String PARCELAS 				{ get; private set; }
	public String TIPOPARCELADO 		{ get; private set; }
	public String TRANSACTIONTYPE 		{ get; private set; }
	public String TRANSACTIONCURRENCY 	{ get; private set; }
	public String TRANSACTIONCOUNTRY 	{ get; private set; }
	public String EXTRADYNAMICURL 		{ get; private set; }
	public String APITOKEN 	  			{ get; private set; }
	public String AUTHTOKEN 			{ get; private set; }
	public String INSTRUCOESBOLETO 			{ get; private set; }

	/*
		Construtor
	*/
	public SRM_BraspagController() {
		Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
		Id_Loja 			= ApexPages.currentPage().getParameters().get('Id_Loja');
		VENDAID 			= ApexPages.currentPage().getParameters().get('VENDAID'); 
		NOSSONUMERO      	= ApexPages.currentPage().getParameters().get('NOSSONUMERO'); 
		VALOR 				= ApexPages.currentPage().getParameters().get('VALOR'); 
		NOME 				= ApexPages.currentPage().getParameters().get('NOME');
		CPF 				= ApexPages.currentPage().getParameters().get('CPF');
		CODPAGAMENTO 		= ApexPages.currentPage().getParameters().get('CODPAGAMENTO');
		PARCELAS 			= ApexPages.currentPage().getParameters().get('PARCELAS');
		TIPOPARCELADO 		= ApexPages.currentPage().getParameters().get('TIPOPARCELADO');
		INSTRUCOESBOLETO 	= ApexPages.currentPage().getParameters().get('INSTRUCOESBOLETO');
		TRANSACTIONTYPE 	= ApexPages.currentPage().getParameters().get('TRANSACTIONTYPE');
		TRANSACTIONCURRENCY = ApexPages.currentPage().getParameters().get('TRANSACTIONCURRENCY');
		TRANSACTIONCOUNTRY 	= ApexPages.currentPage().getParameters().get('TRANSACTIONCOUNTRY');
		EXTRADYNAMICURL 	= ApexPages.currentPage().getParameters().get('EXTRADYNAMICURL');
		APITOKEN 			= WSSetup__c.getValues( 'Token' ).API_Token__c;
		AUTHTOKEN           = mapToken.get( '200' ) != null ? mapToken.get( '200' ) : '';
	}

	/*
		SAP 
	
	public static void connectSAP(){
		Academus.getInstance().processingQueue();
	}
*/

}