public class CRA_checkRecursiveTrigger {
    private static boolean run = true;
    private static boolean runConfirm = true;
    private static boolean runClose = true;
    
    public static boolean runOnce(){
	    if(run){
	    	run = false;
	     	return true;
	    }else{
	    	return run;
	    }
    }
    
    public static boolean runConfirmOnce(){
	    if(runConfirm){
	    	runConfirm = false;
	     	return true;
	    }else{
	    	return runConfirm;
	    }
    }
	
    public static boolean runCloseOnce(){
	    if(runClose){
	    	runClose = false;
	     	return true;
	    }else{
	    	return runClose;
	    }
    }
}