/*
    @author Rogério Oliveira
    @class Classe DAO do objeto Priodo letivo
*/
public with sharing class Periodo_LetivoDAO extends SObjectDAO {
	/*
        Singleton
    */
    private static final Periodo_LetivoDAO instance = new Periodo_LetivoDAO();    
    private Periodo_LetivoDAO(){}
    
    public static Periodo_LetivoDAO getInstance() {
        return instance;
    }

    /*
		Retorna os periodos letivos pelo Id
		@param periodosLetivos Ids dos periodos letivos 
    */
    public List<Per_odo_Letivo__c> getPeriodoLetivoById( List<String> periodosLetivos ) {
    	return [SELECT Id, Name 
    			FROM Per_odo_Letivo__c
    			WHERE Id in :periodosLetivos];
    }
}