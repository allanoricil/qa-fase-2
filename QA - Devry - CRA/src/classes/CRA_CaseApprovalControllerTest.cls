@isTest
public class CRA_CaseApprovalControllerTest {
	public static testMethod void test1(){
		List<User> testUser = new List<User>();
        List<Case> testCases = new List<Case>();
        
        Id standardPBID = Test.getStandardPricebookId(); 
        
        Product2 produto = CRA_DataFactoryTest.newProduct();
        insert produto;
        
        PricebookEntry standardPBEntry = CRA_DataFactoryTest.newPBEntry(produto, standardPBID);
        insert standardPBEntry;
        
        Pricebook2 pricebook = CRA_DataFactoryTest.newPricebook();
        insert pricebook;
        
        PricebookEntry pbentry = CRA_DataFactoryTest.newPBEntry(produto, pricebook.Id);
        insert pbentry;
        
        Account conta = CRA_DataFactoryTest.newPersonAccount();
        insert conta;

        Account conta2 = CRA_DataFactoryTest.newPersonAccount();
        insert conta2;
        
        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Aprovador Acadêmico'];
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id =: conta.Id LIMIT 1].PersonContactId;
        User usuario = CRA_DataFactoryTest.newCustomerUser(contactId, perfil.Id);
        testUser.add(usuario);
        //insert usuario;

        Id contactId2 = [SELECT PersonContactId FROM Account WHERE Id =: conta2.Id LIMIT 1].PersonContactId;
        User usuario2 = new User(
            LastName = 'Outro Aprovador',
            Alias = 'oaprov',
            Username = 'oaprov@adtalem.com.br.qa',
            Email = 'outro.aprovador@harpiacloud.com.br.qa',
            CommunityNickname = 'oaprov',
            ProfileId = perfil.Id,
            DBNumber__c = '13243237',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR',
            ContactId = contactId2
        );
        testUser.add(usuario2);
        //insert usuario2;

        Profile perfil2 = [SELECT Id FROM Profile WHERE Name = 'CRA - Contact Center'];
        User usuario3 = new User(
            LastName = 'Contac Center',
            Alias = 'ccenter',
            Username = 'ccenter@adtalem.com.br.qa',
            Email = 'contact.center@harpiacloud.com.br.qa',
            ProfileId = perfil2.Id,
            DBNumber__c = '13243238',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR'
        );
        testUser.add(usuario3);
        //insert usuario3;

        insert testUser;
        
        Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;
        
        BusinessHours stdBusinessHours = [select id from BusinessHours where Name = 'Default'];
        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        campus.Catalogo_de_precos__c = pricebook.Id;
        campus.Horario_comercial__c = stdBusinessHours.Id;
        insert campus;
        
        Aluno__c aluno = CRA_DataFactoryTest.newAluno(campus, conta);
        insert aluno;

        Entitlement direito = CRA_DataFactoryTest.newEntitlement();
        direito.AccountId = conta.Id;
        insert direito;
        
        Assunto__c assunto = CRA_DataFactoryTest.newAssunto(produto);
        assunto.Aprovacao_Coordenador__c = true;
        assunto.Aprovacao_Imediata__c = true;
        assunto.SLA__c = direito.Id;
        assunto.Anexo__c = 'Sem anexo';
        assunto.Contact_Center__c = true;
        assunto.pagamento__c = false;
        insert assunto;
        
        Detalhe_do_assunto__c detalhe = CRA_DataFactoryTest.newDetalhe(assunto);
        detalhe.Campus__c = campus.Id;       
        insert detalhe;

        Motivo_de_abertura__c motivo = CRA_DataFactoryTest.newMotivo(assunto);
        insert motivo;
    
        Case caso = CRA_DataFactoryTest.newCase(aluno);
        caso.Assunto__c = assunto.Id;
        caso.Coordenador__c = usuario.Id;
        caso.Status = 'Novo';
        caso.Description = 'Testanto classe teste';
        caso.OwnerId = usuario3.Id;

        Case caso2 = CRA_DataFactoryTest.newCase(aluno);
        caso2.Assunto__c = assunto.Id;
        caso2.Coordenador__c = usuario.Id;
        caso2.Status = 'Novo';
        caso2.Description = 'Testanto classe teste';
        caso2.OwnerId = usuario3.Id;

        testCases.add(caso);
        testCases.add(caso2);
        insert testCases;       

        Item_do_caso__c itemdocaso = new Item_do_caso__c();
        itemdocaso.Caso__c = caso.Id;
        insert itemdocaso;       
        caso.Status = 'Em andamento';

        Item_do_caso__c itemdocaso2 = new Item_do_caso__c();
        itemdocaso2.Caso__c = caso2.Id;
        insert itemdocaso2;       
        caso2.Status = 'Em andamento';

        update testCases;

        system.runAs(usuario2){
        	ApexPages.StandardController sc = new ApexPages.StandardController(caso); 
	        PageReference pageRef = Page.CRA_CaseApproval ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseApprovalController controller = new CRA_CaseApprovalController(sc);

        } 

        //Set<Id> ids = new Set<Id>();
        //ids.add(caso.Id);
        //Test.startTest();
        //	CRA_CaseUtil.sendToApproval(ids);
        //Test.stopTest(); 
        Test.startTest();
        Test.stopTest();

        system.runAs(usuario2){
        	ApexPages.StandardController sc = new ApexPages.StandardController(caso); 
	        PageReference pageRef = Page.CRA_CaseApproval ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseApprovalController controller = new CRA_CaseApprovalController(sc);

        }         

        system.runAs(usuario){
           	ApexPages.StandardController sc = new ApexPages.StandardController(caso); 
	        PageReference pageRef = Page.CRA_CaseApproval ;
	        Test.setCurrentPage(pageRef);
	        CRA_CaseApprovalController controller = new CRA_CaseApprovalController(sc); 
            controller.approve();

           	controller.comment = 'aprova aí!';
           	controller.approve();

           	//controller.reject();
        }

        system.runAs(usuario){
           	ApexPages.StandardController sc2 = new ApexPages.StandardController(caso2); 
	        PageReference pageRef2 = Page.CRA_CaseApproval ;
	        Test.setCurrentPage(pageRef2);
	        CRA_CaseApprovalController controller2 = new CRA_CaseApprovalController(sc2);
	        controller2.reject();

           	controller2.comment = 'aprova aí!';
           	controller2.reject();

           	//controller.reject();
        }
    }

}