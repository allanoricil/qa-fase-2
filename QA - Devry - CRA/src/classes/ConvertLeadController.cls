public without sharing class ConvertLeadController {
        public Lead lead2Convert{get;set;}
    public List<Processo_seletivo__c> allProcess {get;set;}    public Account accountToInsert                          { get; set; }
    public Opportunity opportunityToInsert                  { get; set; }    public List<Lead> allLeads {get;set;}
    public Id oppId {get;set;}

        public ConvertLeadController(ApexPages.StandardController controller) {
                lead2Convert = new Lead();
        allLeads = new List<Lead>();
        String myQuery = '';
        integer i=0;

        accountToInsert = new Account();
        opportunityToInsert = new Opportunity();

                List<String> fields = new List<String>();

        Schema.DescribeSObjectResult r = Lead.sObjectType.getDescribe();
        for(string apiName : r.fields.getMap().keySet()){
            fields.add(apiName);
            //if(i==0){
            //    myQuery = apiName + ' ';
            //    i++;
            //} else{
            //    myQuery = myQuery +',' + apiName + ' ';
            //}
        }

        System.debug(fields);

        if(!Test.isRunningTest())
            controller.addFields(fields);

        lead2Convert = (lead)controller.getRecord();
        lead2Convert.Apex_Context__c = true;
        lead2Convert.Conversao__c = true;
        system.debug('apex context01: '+lead2Convert.Apex_Context__c);
        }

        public PageReference redirect2Opp(){
        system.debug('apex context02: '+lead2Convert.Apex_Context__c);
        lead2Convert.WaitResponse__c = 'NAO';
        try{
            update lead2Convert;
        
            system.debug('apex context03: '+lead2Convert.Apex_Context__c);
            allProcess = new List<Processo_seletivo__c>();
            allProcess = [SELECT Id, Data_do_Vestibular__c, Cobrar_Taxa_de_Inscri_o__c, Precisa_de_prova__c, Precisa_de_entrevista__c FROM Processo_seletivo__c WHERE Id = :lead2Convert.Processo_Seletivo__c];
                Savepoint sp = Database.setSavepoint();
            Database.LeadConvertResult lcr = null;
            Database.LeadConvert lc = null;
            List<Account> allAcc = new List<Account>();
            allAcc = [SELECT Id FROM Account Where CPF_2__c =: lead2Convert.CPF__c LIMIT 1];
            system.debug('LISTA DAS CONTAS COM O MESMO CPF AQUI: '+allAcc);
            if(allAcc.size() > 0){

                try{
                    lc = new Database.LeadConvert();
                    lc.setLeadId(lead2Convert.Id);
                    lc.setDoNotCreateOpportunity(false);
                    lc.setAccountId(allAcc[0].Id);
                    LeadStatus convertStatus = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted = true AND MasterLabel = 'Convertido' LIMIT 1];
                    system.debug('OLHA MEU STATUS AQUI: '+convertStatus);
                    lc.setConvertedStatus(convertStatus.MasterLabel);
                    system.debug('status para conversão: '+ConvertStatus.Masterlabel);

                    system.debug('OLHA MEU MASTERLABELFINAL: '+lc);
                    lcr = Database.convertLead(lc);
                    system.debug('ISDONOTCREATEOPP AQUI: '+lc.isDoNotCreateOpportunity());
                    system.debug('status do lead: '+lead2Convert.Status);

                
                    Id accId = lcr.getAccountId();
                    oppId = lcr.getOpportunityId();

                    PageReference oppPage = new PageReference('/'+oppId);
                    oppPage.setRedirect(true);

                    return oppPage;
                } catch(System.DmlException e){
                    if(string.valueof(e.getMessage()).contains('DUPLICATE_VALUE')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Não é possível criar outra inscrição para o mesmo processo!'));
                    } else if(string.valueof(e.getMessage()).contains('CANNOT_UPDATE_CONVERTED_LEAD')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Este registro de interessado já foi Inscrito! Não é possível editá-lo ou inscrevê-lo novamente!'));
                    } else{
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'CPF já existe na base, porém não foi possível criar uma inscrição a partir dos dados.'));
                }   }
            } else{
                system.debug('apex context04: '+lead2Convert.Apex_Context__c);
                try {
                    system.debug('apex context05: '+lead2Convert.Apex_Context__c);
                    lc = new Database.LeadConvert();
                    lc.setLeadId(lead2Convert.Id);
                    lc.setDoNotCreateOpportunity(false);
                    system.debug('apex context06: '+lead2Convert.Apex_Context__c);
                    LeadStatus convertStatus = [SELECT MasterLabel FROM LeadStatus WHERE IsConverted = true AND MasterLabel = 'Convertido' LIMIT 1];
                    system.debug('OLHA MEU STATUS AQUI: '+convertStatus);
                    lc.setConvertedStatus(convertStatus.MasterLabel);

                    system.debug('OLHA MEU MASTERLABELFINAL: '+lc);
                    lcr = Database.convertLead(lc);
                    system.debug('ISDONOTCREATEOPP AQUI: '+lc.isDoNotCreateOpportunity());

                
                        Id accId = lcr.getAccountId();
                        Id oppId = lcr.getOpportunityId();

                    system.debug('apex context07: '+lead2Convert.Apex_Context__c);

                    PageReference oppPage = new PageReference('/'+oppId);
                    oppPage.setRedirect(true);
                    //update lead2Convert;
                    system.debug('EstadoLead: '+lead2Convert.Estado__c);
                    return oppPage;
                } catch (System.DmlException e) {
                        if(string.valueof(e.getMessage()).contains('duplicate value found')){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Já existe uma conta com esse CPF!'));
                        }
                        if(string.valueof(e.getMessage()).contains('CANNOT_UPDATE_CONVERTED_LEAD')){
                                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Este interessado já foi Inscrito! Não é possível editá-lo!'));
                        }
                    if(string.valueof(e.getMessage()).contains('DUPLICATE_VALUE')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Não é possível criar outra inscrição para o mesmo processo!'));
                    }
                    if(string.valueof(e.getMessage()).contains('Data de Nascimento')){
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Data de nascimento inválida!'));
                    }
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'A inscrição do candidato apresentou um erro. Entre em contato com um administrador!'));
                    Database.rollback(sp);
                    system.debug(e.getMessage());
                    //try{
                    //    update lead2Convert;
                    //} catch(DmlException er){
                    //    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Não foi possível atualizar o Lead. Entre em contato com um admnistrador'));
                    //}

                        }
            }
        } catch(DmlException e){
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Não foi possível atualizar o Lead. Entre em contato com um admnistrador'));
        }
                return null;
        }

    public void atualizaEndereco() {
        Map<String, Object> mapResult = SRM_BuscaCEP.getInstance().getCepByNumber( lead2Convert.CEP__c );

        if( mapResult != null ) { 
            lead2Convert.Rua__c      = (String) mapResult.get( 'logradouro' );
            lead2Convert.Bairro__c   = (String) mapResult.get( 'bairro' );
            lead2Convert.Cidade__c   = (String) mapResult.get( 'cidade' );
            lead2Convert.Estado__c   = (String) mapResult.get( 'estado' );
        }
    }
}