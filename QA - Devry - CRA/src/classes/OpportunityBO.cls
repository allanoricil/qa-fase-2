/*
    @author Diego Moreira
    @class Classe de negocio de oportunidade
*/
public with sharing class OpportunityBO {
	/*
        Singleton
    */
    private static final OpportunityBO instance = new OpportunityBO();    
    private OpportunityBO(){}
    
    public static OpportunityBO getInstance() {
        return instance;
    }

    /*
		Cria fila de integração ACADEMUS na criação da oportunidade
		@param opportunities lista de oportunidades criadas
        @project SRM
    */
    public void createQueueNewCandidate( List<Opportunity> opportunities ) {
    	for( Opportunity opportunity : opportunities ) {
            if(String.isBlank(String.valueOf(opportunity.Polo__c))){
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_GRADUACAO )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                } else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_VESTRADICIONAL )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_ENEM_ENTREVISTA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PROUNIFIES )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_ENEM )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PROUNI )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_VESTAGENDADO )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PORTDIPLOMA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_TRANSFERENCIA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_IB )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_REABERTURAMAT )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_CadastroCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueue( QueueEventNames.NEW_CANDIDATE.name(), jsonResult );
                }
            }
    	}
    }

    /*
		Cria fila de integração ACADEMUS na atualização da oportunidade
		@param opportunities lista de oportunidades atualizadas
        @project SRM
    */
    public void createQueueUpdateCandidate( List<Opportunity> opportunities ) {
    	for( Opportunity opportunity : opportunities ) {
            if(String.isBlank(String.valueOf(opportunity.Polo__c))){
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_GRADUACAO )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                } else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_VESTRADICIONAL )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_ENEM_ENTREVISTA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PROUNIFIES )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_ENEM )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PROUNI )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_VESTAGENDADO )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_PORTDIPLOMA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_TRANSFERENCIA )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_IB )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }else
                if( opportunity.RecordTypeId.equals( OpportunityDAO.RECORDTYPE_REABERTURAMAT )) {
                    List<Opportunity> opportunityResult = OpportunityDAO.getInstance().getOpportunityById( opportunity.Id );
                    String jsonResult = SRM_AtualizaCandidatoBO.getJsonRequest( opportunityResult[0] );
                    QueueBO.getInstance().createQueueByScheduleProcess( QueueEventNames.UPDATE_CANDIDATE.name(), jsonResult );
                }
            }                
    	}
    }

}