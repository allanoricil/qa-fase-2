@isTest
private class RelacionaPeriodoLetivo_test {
	@isTest(SeeAllData=true)
    static void UnitTest1() {
        Per_odo_Letivo__c pl = new Per_odo_Letivo__c(CodPeriodo__c = '2020.2', Status_do_Per_odo_Letivo__c = 'Ativo');
        insert pl;
         Configura_es_do_Per_odo_Letivo__c cpl = new Configura_es_do_Per_odo_Letivo__c(Per_odo_Letivo__c = pl.id);
        insert cpl;

        Atendimento_CASA__c ac = new Atendimento_CASA__c(Per_odo_Letivo__c = pl.id, CodPeriodo__c = '2020.2', CodAcompCasa__c = '577902012.1', IdUsuario__c = '57790');
        insert ac;
        
        Aluno__c alx = new Aluno__c(Name='teste',CPF__c='00622366106', E_mail__c='teste@teste.com');
        insert alx;
        alx.Conta__c = null;
        update alx;
        
        Profile up = [Select id From Profile Where Name =: 'Orientador CASA' limit 1];
        User u = [Select id From User Where isActive =: true AND ProfileId =: up.Id limit 1];
        ac.OwnerId = u.id;
        ac.Grupo_de_Risco__c = 'g10';
        //Aluno__c al = [Select id, E_mail__c, Id_do_Contato__c From Aluno__c Where E_mail__c != null AND Conta__c != null limit 1];
        //ac.Aluno__c = al.Id;
        ac.Data_de_Envio_de_Email_de_Apresenta_o__c = null;
        update ac;
        update cpl;
                
        EmailTemplate emt = [Select id From EmailTemplate Where DeveloperName =: 'Apresentacao_CASA_Oficial' limit 1];
        List<String> emails = new List<String>();
        //emails.add(al.e_mail__c);
        

        //emailHelper.sendEmail(ID recipient, ID TemplateId, ID WhatObjectId, Boolean BccSender, Boolean UseSignature, String ReplyTo, String SenderDisplayName, String[] setToAddresses, String Subject, String PlainTextBody) {            
        //EmailHelper.sendEmail(al.Id_do_Contato__c, emt.Id, ac.Id, false, false, ac.Owner.Email, ac.Owner.Name, emails, null, null);
        EmailHelper.sendEmail(null, emt.Id, null, false, false, ac.Owner.Email, ac.Owner.Name, null, null, null);        
      
    }    
}