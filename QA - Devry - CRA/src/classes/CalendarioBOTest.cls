@IsTest
public class CalendarioBOTest{
    
    /*
    Verifica se constroi a data inicial do primeiro periodo
  */
    
    @IsTest
    public static void constroiCalendarioInicialPrimeiroPeriodoTest(){
        String periodoLetivo = '2015.1';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        
        Date dataInicial = cp.constroiDataInicial();
        system.assertEquals('01/01/2015', dataInicial.format());
    }
    
     /*
    Verifica se constroi a data inicial do segundo periodo
  */
    
    @IsTest
    public static void constroiCalendarioInicialSegundoPeriodoTest(){
        String periodoLetivo = '2015.2';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        
        Date dataInicial = cp.constroiDataInicial();
        system.assertEquals('01/07/2015', dataInicial.format());
    }
    
     /*
    Verifica se em uma determinada semana os nomes dos dias correspondem
  */
    
    @IsTest
    public static void validaNomeDosDiasTest(){
        String periodoLetivo = '2015.1';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        Map<Integer,List<CalendarioBO.DiaDoMes>> calendario = cp.gerarCalendarioAcademico(); 
       
        System.assertEquals('Sun',calendario.get(1).get(10).nome);
        System.assertEquals('Mon',calendario.get(1).get(11).nome);
        System.assertEquals('Tue',calendario.get(1).get(12).nome);
        System.assertEquals('Wed',calendario.get(1).get(13).nome);
        System.assertEquals('Thu',calendario.get(1).get(14).nome);
        System.assertEquals('Fri',calendario.get(1).get(15).nome);
        System.assertEquals('Sat',calendario.get(1).get(16).nome);
        
    }
    
     /*
    Gera o calendário do primeiro periodo, e verifica se cada mês tem o número de dias correto 
  */
    
    @IsTest
    public static void gerarCalendarioAcademicoPrimeiroPeriodoTest(){
        String periodoLetivo = '2015.1';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        Map<Integer,List<CalendarioBO.DiaDoMes>> calendarioAcademico = cp.gerarCalendarioAcademico();   
        system.assertEquals(6,calendarioAcademico.size());
        system.assertEquals(31,calendarioAcademico.get(1).size());
        system.assertEquals(28,calendarioAcademico.get(2).size());
        system.assertEquals(31,calendarioAcademico.get(3).size());
        system.assertEquals(30,calendarioAcademico.get(4).size());
        system.assertEquals(31,calendarioAcademico.get(5).size());
        system.assertEquals(30,calendarioAcademico.get(6).size());
    }
    
      /*
    Gera o calendário do segundo periodo, e verifica se cada mês tem o número de dias correto 
   */
    
    @IsTest
    public static void gerarCalendarioAcademicoSegundoPeriodoTest(){
        String periodoLetivo = '2015.2';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        Map<Integer,List<CalendarioBO.DiaDoMes>> calendarioAcademico = cp.gerarCalendarioAcademico();   
        
        system.assertEquals(6,calendarioAcademico.size());
        system.assertEquals(31,calendarioAcademico.get(7).size());
        system.assertEquals(31,calendarioAcademico.get(8).size());
        system.assertEquals(30,calendarioAcademico.get(9).size());
        system.assertEquals(31,calendarioAcademico.get(10).size());
        system.assertEquals(30,calendarioAcademico.get(11).size());
        system.assertEquals(31,calendarioAcademico.get(12).size());
    }
    
      /*
    Configura no calendário uma data para aplicação de AP e valida se é verdadeiro através do método
    : isAplicacaoAp passando como parametro uma data correspondente
  */
    
    @IsTest
    public static void validaIsAplicacaoApTest(){
        String periodoLetivo = '2015.1';
        List<Evento_Calend_rio_Acad_mico__c> eventos = new List<Evento_Calend_rio_Acad_mico__c>();
        Evento_Calend_rio_Acad_mico__c e1 = new Evento_Calend_rio_Acad_mico__c();
        e1.Data_de_nicio__c = Date.newInstance(2015, 2, 5);
        e1.Data_de_T_rmino__c = Date.newInstance(2015, 2, 10);
        e1.Aplica_o_AP__c = true;
        
        eventos.add(e1);
        CalendarioBO cp = new CalendarioBO(periodoLetivo,eventos);
        
        Date aplicacaoAp1 = Date.newInstance(2015, 2, 5);
        Date aplicacaoAp2 = Date.newInstance(2015, 2, 8);
        Date aplicacaoAp3 = Date.newInstance(2015, 2, 10);
        
        system.assert( cp.isAplicacaoAp(aplicacaoAp1) );
        system.assert( cp.isAplicacaoAp(aplicacaoAp2) );
        system.assert( cp.isAplicacaoAp(aplicacaoAp3) );
    }
    
     /*
    Configura no calendário uma data para aplicação de AP e valida se é false através do método
    : isAplicacaoAp passando como parametro uma data diferente
  */
    
    @IsTest
    public static void validaNaoEAplicacaoApTest(){
        String periodoLetivo = '2015.1';
        List<Evento_Calend_rio_Acad_mico__c> eventos = new List<Evento_Calend_rio_Acad_mico__c>();
        Evento_Calend_rio_Acad_mico__c e1 = new Evento_Calend_rio_Acad_mico__c();
        e1.Data_de_nicio__c = Date.newInstance(2015, 3, 20);
        e1.Data_de_T_rmino__c = Date.newInstance(2015, 3, 30);
        eventos.add(e1);
        
        CalendarioBO cp = new CalendarioBO(periodoLetivo,eventos);
        Date naoaplicacao1 = Date.newInstance(2015, 4, 1);
        Date naoAplicacao2 = Date.newInstance(2015, 3, 19);
       
        system.assert(!cp.isAplicacaoAp(naoaplicacao1));
        system.assert(!cp.isAplicacaoAp(naoAplicacao2));
    }
    
    
    @IsTest
    public static void naoEAplicacaoApTest(){
        String periodoLetivo = '2015.1';
        
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        Date aplicacaoAp1 = Date.newInstance(2015, 4, 1);
        Date aplicacaoAp2 = Date.newInstance(2015, 3, 19);
        
        system.assert(!cp.isAplicacaoAp(aplicacaoAp1));
        system.assert(!cp.isAplicacaoAp(aplicacaoAp2));
    }
    
    
    @IsTest
    public static void gerarCalendarioAcademicoCompleto(){
        String periodoLetivo = '2015.1';
        List<Evento_Calend_rio_Acad_mico__c> eventos = new List<Evento_Calend_rio_Acad_mico__c>();
        Evento_Calend_rio_Acad_mico__c e1 = new Evento_Calend_rio_Acad_mico__c();
        e1.Data_de_nicio__c = Date.newInstance(2015, 3, 23);
        e1.Data_de_T_rmino__c = Date.newInstance(2015, 3, 25);
        e1.Aplica_o_AP__c = true;
        
        Evento_Calend_rio_Acad_mico__c e2 = new Evento_Calend_rio_Acad_mico__c();
        e2.Data_de_nicio__c = Date.newInstance(2015, 4, 5);
        e2.Data_de_T_rmino__c = Date.newInstance(2015, 4, 12);
        
        
        Evento_Calend_rio_Acad_mico__c e3 = new Evento_Calend_rio_Acad_mico__c();
        e3.Data_de_nicio__c = Date.newInstance(2015, 5, 1);
        e3.Data_de_T_rmino__c = Date.newInstance(2015, 5,1);
        e3.Feriado__c = true;
        
        Evento_Calend_rio_Acad_mico__c e4 = new Evento_Calend_rio_Acad_mico__c();
        e4.Data_de_nicio__c = Date.newInstance(2015, 2, 4);
        e4.Data_de_T_rmino__c = Date.newInstance(2015, 2, 6);
        e4.Aplica_o_AP__c = true;
        
        eventos.add(e4);
        eventos.add(e1);
        eventos.add(e2);
        eventos.add(e3);
       
        CalendarioBO cp = new CalendarioBO(periodoLetivo,eventos);
        Map<Integer,List<CalendarioBO.DiaDoMes>> calendarioAcademico = cp.gerarCalendarioAcademico();       
        
        system.assertEquals(false, calendarioAcademico.get(3).get(21).aplicacaoAp);
        system.assertEquals(22, calendarioAcademico.get(3).get(21).index);        
        system.assertEquals(true, calendarioAcademico.get(3).get(22).aplicacaoAp);
        system.assertEquals(23, calendarioAcademico.get(3).get(22).index);
        system.assertEquals(true, calendarioAcademico.get(3).get(23).aplicacaoAp);
        system.assertEquals(24, calendarioAcademico.get(3).get(23).index);
        system.assertEquals(true, calendarioAcademico.get(3).get(24).aplicacaoAp);
        system.assertEquals(25, calendarioAcademico.get(3).get(24).index);
        system.assertEquals(false, calendarioAcademico.get(3).get(25).aplicacaoAp);
        system.assertEquals(26, calendarioAcademico.get(3).get(25).index);
        
        system.assertEquals(true,calendarioAcademico.get(4).get(4).nome == 'Sun');
        system.assertEquals(true,calendarioAcademico.get(4).get(11).nome == 'Sun');
        
        system.assertEquals(1,calendarioAcademico.get(5).get(0).index);
        system.assertEquals(true,calendarioAcademico.get(5).get(0).feriado);
        
        system.assertEquals(true,calendarioAcademico.get(2).get(3).aplicacaoAp);
        system.assertEquals(true,calendarioAcademico.get(2).get(4).aplicacaoAp);
        system.assertEquals(true,calendarioAcademico.get(2).get(5).aplicacaoAp);
        
    }
    
    
    
    @IsTest
    public static void testIsNotSatORSun(){
        String periodoLetivo = '2015.1';
        CalendarioBO cp = new CalendarioBO(periodoLetivo,null);
        Date f1 = Date.newInstance(2015, 11, 2);
        Date f2 = Date.newInstance(2015, 11, 6);
        
        system.assert(!cp.isFeriado(f1));
        system.assert(!cp.isFeriado(f2));
    }
     
}