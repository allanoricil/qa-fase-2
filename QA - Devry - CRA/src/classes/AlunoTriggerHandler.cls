public class AlunoTriggerHandler implements TriggerHandlerInterface{
    private static final AlunoTriggerHandler instance = new AlunoTriggerHandler();

    /** Private constructor to prevent the creation of instances of this class.*/
    private AlunoTriggerHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    */
    public static AlunoTriggerHandler getInstance() {
        return instance;
    }    
    
    public void beforeUpdate() {
    }
    
    public void beforeInsert() {
        CRA_OwnerFranquia.setOwnerFranquiaAluno((List<Aluno__c>)Trigger.new);
    }
    
    public void beforeDelete() {
    }
    
    public void afterUpdate() {
    }
    
    public void afterInsert() {
    }
    
    public void afterDelete() {
    }
}