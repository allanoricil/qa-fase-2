/*
    @author Diego Moreira
    @class Schedule de processamento de integração com SAP
*/
global class SRM_ScheduleIntegraTitulos implements Schedulable {

	global void execute(SchedulableContext sc) { 

		for( Queue__c queue : QueueDAO.getInstance().getQueueByEventNameAndStatus( QueueEventNames.INSERT_REGISTRATION_TITLES.name(), 'CREATED', 25 ) ) {
			
               SRM_CreateRegistrationTitlesBO.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c ); 
 
		}

		for( Queue__c queue : QueueDAO.getInstance().getQueueByEventNameAndStatus( QueueEventNames.UPDATE_REGISTRATION_TITLES.name(), 'CREATED', 25 ) ) {
			SRM_UpdateRegistrationTitlesBO.processingQueue( queue.Id, queue.EventName__c, queue.Payload__c );
		}
	}
}