@isTest 
private class UsuarioProprietarioDaOportunidadeTest {
    static testMethod void testok(){
        
        User u = [SELECT Id FROM User WHERE DBNumber__c = 'db1003160'];
        
        u.FirstName = 'test';
        u.IsActive = true;
        
        update u;
        
        u.FirstName = 'test';
        u.IsActive = false;
        
        update u;
    }
}