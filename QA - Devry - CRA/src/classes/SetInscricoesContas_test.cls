@istest
private class SetInscricoesContas_test {

    static testMethod void UnitTest1() {
        Institui_o__c instituicao = InstanceToClassCoverage.createInstituicao();
        insert instituicao;
        
        Campus__c campus = InstanceToClassCoverage.createCampus(instituicao);
        insert campus;
        
        Per_odo_Letivo__c periodoLetivo = InstanceToClassCoverage.createPeriodoLetivo();
        insert periodoLetivo;
        
        Processo_seletivo__c processoSeletivo = InstanceToClassCoverage.createProcessoSeletivo(instituicao, campus, periodoLetivo);
		insert processoSeletivo;
        
        Opportunity opp1 = new Opportunity();
        opp1.CPF__c = '006.223.661-06';
        opp1.Sobrenome__c = 'teste';
        opp1.ID_Participa_o__c = '987654321';
        opp1.Name = 'teste';
        opp1.StageName	= 'Aberto';
        opp1.CloseDate = System.today();
        opp1.Processo_seletivo__c = processoSeletivo.Id;
        
        insert opp1;
        update opp1;
    }
    @isTest(SeeAllData=true)
    static void UnitTest2() {
        Test.StartTest();
        DuplicateFinder df = new DuplicateFinder();
        df.query = 'Select COUNT(Id), Name, CPF__c From Account GROUP BY Name, CPF__c limit 10';
        df.email='rafael@weengo.com';
        df.fromUserId = UserInfo.getUserId();
        df.toUserId = UserInfo.getUserId();
        ID batchprocessid = Database.executeBatch(df, 20);
        System.debug('Returned batch process ID: ' + batchProcessId);
        CreateErrorLog.createErrorRecord('teste', 'SetInscricoesContas');
        
        Test.StopTest();
    }
}