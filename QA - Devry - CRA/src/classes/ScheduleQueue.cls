/*
	@author Diego Moreira
	@class Classe schedule do objeto Queue
*/
global with sharing class ScheduleQueue implements Schedulable {

	global void execute( SchedulableContext SC ) {
		QueueBO.getInstance().reprocessQueue();
	}

}