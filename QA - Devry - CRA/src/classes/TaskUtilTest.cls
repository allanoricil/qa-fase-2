@isTest
public class TaskUtilTest {	    
    public static testMethod void testPadrao(){
    	Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;

        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        insert campus;

        Area_Curso__c area = CRA_DataFactoryTest.newAreaCurso();
        insert area;

        Per_odo_Letivo__c periodo =  CRA_DataFactoryTest.newPeriodo();
        insert periodo;

        Curso__c curso = CRA_DataFactoryTest.newCurso(instituicao, campus);
        insert curso;

        Processo_Seletivo__c processo = CRA_DataFactoryTest.newProcesso(instituicao,campus,periodo);
        Id devRecordTypeId = Schema.SObjectType.Processo_Seletivo__c.getRecordTypeInfosByName().get('Graduação').getRecordTypeId();
        processo.RecordTypeId = devRecordTypeId;
        insert processo;

        Oferta__c oferta = CRA_DataFactoryTest.newOferta(instituicao,campus,periodo,curso,area,processo);
        insert oferta;

        processo.Status_Processo_Seletivo__c = 'Aberto';
        update processo;

        Lead lead = CRA_DataFactoryTest.newLead(instituicao,campus,periodo,processo);
        insert lead;

        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Operador'];
        User usuario = new User(
            LastName = 'Admissões',
            Alias = 'admis',
            Username = 'admissoes@adtalem.com.br.qa',
            Email = 'admissoes@harpiacloud.com.br.qa',
            ProfileId = perfil.Id,
            DBNumber__c = '13243238',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR'
        );
        insert usuario;

    	Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('CRA - Tarefa').getRecordTypeId();
    	Task task = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Tentativa',
    		Motivo__c = 'Caixa Postal/ Não atende',
    		WhoId = lead.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task;

		Task task2 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Tentativa',
    		Motivo__c = 'Número inválido',
    		WhoId = lead.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task2;

		Task task3 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Analisando',
    		Motivo__c = 'Aguardando Enem',
    		WhoId = lead.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task3;

		Task task4 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Confirmado',
    		Motivo__c = '',
    		WhoId = lead.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task4;

		Task task5 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Desistente',
    		Motivo__c = 'Não ligue mais',
    		WhoId = lead.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task5;

		ApexPages.StandardController sc = new ApexPages.StandardController(lead); 
        PageReference pageRef = Page.ConvertLeadPage ;
        Test.setCurrentPage(pageRef);
        ConvertLeadController controller = new ConvertLeadController(sc);
        controller.lead2convert.Nacionalidade__c = 'Brasileira';
        controller.lead2convert.Sexo__c = 'Feminino';
        controller.lead2convert.Cor_Raca__c = 'Branca';
        controller.lead2convert.X1a_Op_o_de_Curso__c = oferta.Id;
        controller.lead2convert.CEP__c = '03355-000';
        controller.lead2Convert.Rua__c = 'Rua almirante lobo';
        controller.lead2Convert.Bairro__c = 'Vila Mariana';
        controller.lead2Convert.Cidade__c = 'São Paulo';
        controller.lead2Convert.Estado__c = 'SP';
        controller.lead2Convert.N_mero__c = '123';
        //controller.redirect2Opp();

        Id oppId = controller.oppId;
        system.debug('Id opp: '+oppId);

        Opportunity opp1 = new Opportunity();
        opp1.CPF__c = '006.223.661-06';
        opp1.Sobrenome__c = 'teste';
        opp1.ID_Participa_o__c = '987654321';
        opp1.Name = 'teste';
        opp1.StageName	= 'Aberto';
        opp1.CloseDate = System.today();
        opp1.Instituicao__c = instituicao.Id;
        opp1.Campus_Unidade__c = campus.Id;
        opp1.Tipo_do_curso__c = 'Graduação';
        opp1.X1_OpcaoCurso__c = oferta.Id;
        opp1.Processo_seletivo__c = processo.Id;
        
        insert opp1;

        Task task6 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Tentativa',
    		Motivo__c = 'Caixa Postal/ Não atende',
    		WhatId = opp1.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task6;

		Task task7 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Tentativa',
    		Motivo__c = 'Número inválido',
    		WhatId = opp1.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task7;

		Task task8 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Analisando',
    		Motivo__c = 'Aguardando Enem',
    		WhatId = opp1.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task8;

		Task task9 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Confirmado',
    		Motivo__c = '',
    		WhatId = opp1.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task9;

		Task task10 = new Task(
    		Subject = 'Ligação Ativa',
    		Status = 'Concluída',
    		RecordTypeId = taskRecordTypeId,
    		Status_do_Contato__c = 'Desistente',
    		Motivo__c = 'Não ligue mais',
    		WhatId = opp1.Id,
    		Data_Hora_do_Agendamento_da_Atividade__c = Datetime.newInstance(2017,12, 12, 10, 10,0)
		);
		insert task10;

		delete task;


		//system.debug('vai começar o teste com usuário');
  //      system.runAs(usuario){
  //          Test.startTest();

  //         	ApexPages.StandardController sc = new ApexPages.StandardController(lead); 
	 //       PageReference pageRef = Page.ConvertLeadPage ;
	 //       Test.setCurrentPage(pageRef);
	 //       ConvertLeadController controller = new ConvertLeadController(sc);
	 //       controller.lead2convert.Nacionalidade__c = 'Brasileira';
	 //       controller.lead2convert.Sexo__c = 'Feminino';
	 //       controller.lead2convert.Cor_Raca__c = 'Branca';
	 //       controller.lead2convert.X1a_Op_o_de_Curso__c = oferta.Id;
	 //       controller.lead2convert.CEP__c = '03355-000';
	 //       controller.lead2Convert.Rua__c = 'Rua almirante lobo';
  //          controller.lead2Convert.Bairro__c = 'Vila Mariana';
  //          controller.lead2Convert.Cidade__c = 'São Paulo';
  //          controller.lead2Convert.Estado__c = 'SP';
  //          controller.lead2Convert.N_mero__c = '123';

	 //       controller.redirect2Opp();
	 //       system.debug('depois da conversão de vest tradicional');

  //          Test.stopTest();
  //      }
  //      Opportunity myOpp = new Opportunity();
  //      myOpp = [SELECT Id FROM Opportunity WHERE CreatedDate = today LIMIT 1];
  //      system.debug('minha lista: '+myOpp);
    }

    public static testMethod void testOppDAO(){
    	Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
        insert instituicao;

        Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
        insert campus;

        Area_Curso__c area = CRA_DataFactoryTest.newAreaCurso();
        insert area;

        Per_odo_Letivo__c periodo =  CRA_DataFactoryTest.newPeriodo();
        insert periodo;

        Curso__c curso = CRA_DataFactoryTest.newCurso(instituicao, campus);
        insert curso;

        Processo_Seletivo__c processo = CRA_DataFactoryTest.newProcesso(instituicao,campus,periodo);
        Id devRecordTypeId = Schema.SObjectType.Processo_Seletivo__c.getRecordTypeInfosByName().get('Graduação').getRecordTypeId();
        processo.RecordTypeId = devRecordTypeId;
        insert processo;

        Oferta__c oferta = CRA_DataFactoryTest.newOferta(instituicao,campus,periodo,curso,area,processo);
        insert oferta;

        processo.Status_Processo_Seletivo__c = 'Aberto';
        update processo;

        Lead lead = CRA_DataFactoryTest.newLead(instituicao,campus,periodo,processo);
        insert lead;

        Profile perfil = [SELECT Id FROM Profile WHERE Name = 'CRA - Operador'];
        User usuario = new User(
            LastName = 'Admissões',
            Alias = 'admis',
            Username = 'admissoes@adtalem.com.br.qa',
            Email = 'admissoes@harpiacloud.com.br.qa',
            ProfileId = perfil.Id,
            DBNumber__c = '13243238',
            TimeZoneSidKey = 'America/Sao_Paulo',
            LocaleSidKey = 'pt_BR',
            EmailEncodingKey = 'ISO-8859-1',
            LanguageLocaleKey = 'pt_BR'
        );
        insert usuario;

    	Id taskRecordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('CRA - Tarefa').getRecordTypeId();

		ApexPages.StandardController sc = new ApexPages.StandardController(lead); 
        PageReference pageRef = Page.ConvertLeadPage ;
        Test.setCurrentPage(pageRef);
        ConvertLeadController controller = new ConvertLeadController(sc);
        controller.lead2convert.Nacionalidade__c = 'Brasileira';
        controller.lead2convert.Sexo__c = 'Feminino';
        controller.lead2convert.Cor_Raca__c = 'Branca';
        controller.lead2convert.X1a_Op_o_de_Curso__c = oferta.Id;
        controller.lead2convert.CEP__c = '03355-000';
        controller.lead2Convert.Rua__c = 'Rua almirante lobo';
        controller.lead2Convert.Bairro__c = 'Vila Mariana';
        controller.lead2Convert.Cidade__c = 'São Paulo';
        controller.lead2Convert.Estado__c = 'SP';
        controller.lead2Convert.N_mero__c = '123';
        //controller.redirect2Opp();

        Id oppId = controller.oppId;
        system.debug('Id opp: '+oppId);

        Opportunity opp1 = new Opportunity();
        opp1.CPF__c = '006.223.661-06';
        opp1.Sobrenome__c = 'teste';
        opp1.ID_Participa_o__c = '987654321';
        opp1.Name = 'teste';
        opp1.StageName	= 'Aberto';
        opp1.CloseDate = System.today();
        opp1.Instituicao__c = instituicao.Id;
        opp1.Campus_Unidade__c = campus.Id;
        opp1.Tipo_do_curso__c = 'Graduação';
        opp1.X1_OpcaoCurso__c = oferta.Id;
        opp1.Processo_seletivo__c = processo.Id;

        insert opp1;

        Opportunity opp2 = [SELECT Id, AccountId FROM Opportunity WHERE Id =: opp1.Id LIMIT 1];

        Id accId = opp2.AccountId;

        List<Opportunity> oportunidades = new List<Opportunity>();
        String whereString = 'AccountId = \''+accId+'\'';
        List<String> listaString = new List<String>();
        listaString.add(String.valueOf(processo.Id));

        oportunidades = OpportunityDAO.getInstance().getOpportunityByWhereSting(whereString);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByAccountIdAndProcessoSeletivo(accId,processo.Id);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByCPFAndProcessoSeletivo(opp1.CPF__c,processo.Id);
        oportunidades = OpportunityDAO.getInstance().getDuplicateOpportunityByCPF(processo.Id,oferta.Id,opp1.CPF__c);
        oportunidades = OpportunityDAO.getInstance().getInscricaoAprovadas();
        oportunidades = OpportunityDAO.getInstance().getInscricaoAprovadasByProcessoSeletivoId(listaString);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByCodigoInscricao(listaString);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByProcessoSeletivoId(listaString);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByPromotionalCode(listaString[0]);
        oportunidades = OpportunityDAO.getInstance().getOpportunityByOwner(accId,1);

    }
}