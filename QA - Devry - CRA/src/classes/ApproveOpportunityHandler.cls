public with sharing class ApproveOpportunityHandler {
	private ApproveOpportunityHandler() { /* Aprovar Candidato */ }
   	private static final ApproveOpportunityHandler instance = new ApproveOpportunityHandler();    
    
    public static ApproveOpportunityHandler getInstance() {
        return instance;
    }

    private static boolean run = true;

	public static boolean oneTime(){
	    if(run){
	     run=false;
	     return true;
	    }else{
	        return run;
	    }
	}
    public void beforeUpdate() {
        for(Opportunity oportunidade :(List<Opportunity>)Trigger.new ){
        	if( ( oportunidade.Tipo_do_curso__c != null) && ( oportunidade.Matr_cula_Online__c != null) ){
        		if( (oportunidade.Tipo_do_curso__c == 'Extensão')
        			&& (oportunidade.Matr_cula_Online__c == 'Sim') 
        			&& (oportunidade.RM__c == false) 
        			&& (oportunidade.RA__c != null)  
        			&& (oportunidade.StageName == 'Aprovado') )
        		{
        			CCD_CriarAluno.createQueue(oportunidade.Id);
        		}else{
        			system.debug('Não Integrar');
        		}
        	}else{
        		system.debug('Campos nulos');
        	}
        }
    }

}