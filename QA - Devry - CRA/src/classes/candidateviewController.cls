public with sharing class candidateviewController {
    // Polo do usuário
    public String idpolo   { get{ return [Select Polo__c From User Where Id =:UserInfo.getUserId() limit 1].Polo__c ;} set; }
    public String getPolo(){
        return [Select Name from Polo__c where id=: idpolo limit 1].Name;
    }
    // Paginação
    
    public EadIterator obj {get; set;}
    
    public List<EadWrapper> lstWrapper {get;set;}
    public List<EadWrapper> lstSetController{get;set;}
    
    //Listas de Filtro

    public List<String> fases {get; set;}
    public List<String> processos {get; set;} 
    public List<String> contatados {get; set;}

    //Campos de Filtro
    public String valorPesquisa { get; set; }
    public String dataInicial   { get; set; }
    public String dataFinal     { get; set; }
    public String data          { get; set; }
    public String processo      { get; set; }


    //Alerta Pendentes
    public String alert { get; private set; }
    public Integer pendentes { get; private set; }

    public candidateviewController() {
        consulta();
    }



    public void consulta(){

        dataInicial = '';
        dataFinal = '';

        pendentes = 0 ;


        fases = new List<String>();

        processos = new List<String>();
        
        contatados = new List<String>();

        data = getDate();

        processo = getProceso();

        lstWrapper =  new List<EadWrapper>();
        lstSetController = new List<EadWrapper>();

        String query = 'SELECT Celular__c, CPF__c, DiaProva__c, StageName__c,'+
                ' Name, StatusContato__c, Bolsa__c,' +
                ' TipoProcesso__c, Agendamentos__c,' +
                ' SituacaoMatricula__c, Opportunity__c' +
                ' FROM Oportunidade_EAD__c'+
                ' Where Polo__c = ' + '\'' + idpolo + '\''+
                  processo + data +
                ' Order by CreatedDate desc limit 10000';

        List<Oportunidade_ead__c> x = Database.query(query);
        
        for(Oportunidade_ead__c cont : x ){
            lstWrapper.add(new EadWrapper(cont));
            if(cont.StageName__c == 'Inscrito' && cont.StatusContato__c == 'Não')
                pendentes++;
        }

        alert = rtnPendentes(pendentes);

        obj = new eadIterator (lstWrapper); 
        obj.setPageSize = 30;
        next();
    }

    // Métodos de Paginação

    public Boolean hasNext {
        get {
            return !obj.hasNext();
        }
        set;
    }
        
    public Boolean hasPrevious {
        get {
            return !obj.hasPrevious();
        }
        set;
    }
        
    public void next() 
    {
        lstSetController = obj.next();
    }
    
    public void previous() 
    {
        lstSetController = obj.previous();
    }

    // Métodos de Filtragem por Parâmetro

    public String getProceso(){
            String retorno = '';
            String processo = ApexPages.currentPage().getParameters().get('processo');
            if(processo == 'ENEM'){
                retorno = ' and TipoProcesso__c = \'ENEM\'';
            }
            if(processo =='agendado'){
                retorno = ' and TipoProcesso__c = \'Vestibular Especial\'';
            }
            return retorno;
    }
    public String getDate(){
            String retorno = '';
            String num = ApexPages.currentPage().getParameters().get('date');

            if(num != null){
                Date dtft = system.today().addDays(Integer.valueOf(num));
                String dtat = String.valueOf(system.today());
                retorno = 'and DiaProva__c >=' + dtat  +' and DiaProva__c <= ' + String.valueOf(dtft);
            }else{
                retorno = '';
            }
            return retorno;
    }

    //Métodos de auxílio

    public Date GetDateByString(String s){
        String[] myDateOnly = s.split('/');
        Date dateObj;
        try{
            dateObj = date.newInstance(Integer.valueOf(myDateOnly.get(2)),Integer.valueOf(myDateOnly.get(1)),Integer.valueOf(myDateOnly.get(0)));
        } catch(exception ex) {
            dateObj = date.today();
        }
        return dateObj;
    }

    public String rtnPendentes(Integer pendetes){
        if(pendetes > 0) return '<script>var $toastContent = $(\'<span>Você possui ' + pendetes + ' inscrições pendentes!' +'</span>\').add($(\'<a class=\"btn-flat toast-action popup modal-trigger\" href=\"#modal1"\" onclick=\"pe();return false\">ver</a>\')); Materialize.toast($toastContent, 4800);</script>';
        else return '';
    }


    //Aplica filtro Personalizado

    public void aplicarFiltroPerson(){

        String fase = getFase();
        String pro = getProcessos();
        String contacts = getContatados();
        String dataz;

        if(dataInicial == '' && dataFinal == '')
            dataz = '';
        
        if(dataInicial == dataFinal)
            dataz = '';
        
        if(dataInicial != '' && dataFinal != '')
            dataz = retornadata();

        lstWrapper =  new List<EadWrapper>();
        lstSetController = new List<EadWrapper>();

        String query = 'SELECT Celular__c, CPF__c, DiaProva__c, StageName__c,'+
                ' Name, StatusContato__c, Bolsa__c,' +
                ' TipoProcesso__c, Agendamentos__c,' +
                ' SituacaoMatricula__c, Opportunity__c' +
                ' FROM Oportunidade_EAD__c Where Polo__c =: idpolo ' + fase + pro + dataz + contacts;


        List<Oportunidade_ead__c> x = Database.query(query);

        for(Oportunidade_ead__c cont : x )
            lstWrapper.add(new EadWrapper(cont));
            
        obj = new eadIterator (lstWrapper); 
        obj.setPageSize = x.size();

        next();
    }

     public void consultaInscricoes() {

        lstWrapper =  new List<EadWrapper>();
        lstSetController = new List<EadWrapper>();

        String query = 'SELECT Celular__c, CPF__c, DiaProva__c, StageName__c,'+
                ' Name, StatusContato__c, Bolsa__c,' +
                ' TipoProcesso__c, Agendamentos__c,' +
                ' SituacaoMatricula__c, Opportunity__c' +
                ' FROM Oportunidade_EAD__c'+
                ' Where Polo__c =: idpolo and (CPF__c = ' + '\'' + valorPesquisa + '\''+
                ' or Name like ' + '\'%' + valorPesquisa + '%\'' +
                ' or Email__c like ' + '\'%' + valorPesquisa + '%\')';

        List<Oportunidade_ead__c> x = Database.query(query);

        for(Oportunidade_ead__c cont : x ){
            lstWrapper.add(new EadWrapper(cont));
            if(cont.StageName__c == 'Inscrito' && cont.StatusContato__c == 'Não')
            pendentes++;
        }
        obj = new eadIterator (lstWrapper); 
        obj.setPageSize = x.size();

        next();
        
    }

    //Filtros Personalizados

    public String getProcessos(){
        String retorno;
        List<String> processosGet = new List<String>();

        if(processos.size() == 1 && processos[0] != null){
            String processo1 = ' and TipoProcesso__c =' + '\'' +processos[0]+ '\'';
            processosGet.add(processo1);
        }else{
        if(processos.size()>0 && processos[0] != null){
            String processo1 = 'TipoProcesso__c =' + '\'' +processos[0]+ '\'';
            processosGet.add(processo1);
        }
        if(processos.size()>1 && processos[1] != null){
            String processo2 = ' or TipoProcesso__c =' + '\'' +processos[1]+ '\'';
            processosGet.add(processo2);
        }
        }

        if(processos.size() < 1)
            retorno = '';
        else{
        if(processos.size() == 1)
            retorno = String.join(processosGet,',').replace(',','');
        else{
            retorno = ' and (' + String.join(processosGet,',').replace(',','')+')';
        }
        }

        return retorno;
    }

    public String getContatados(){
        String retorno;
        List<String> contatadosGet = new List<String>();

        if(contatados.size() == 1 && contatados[0] != null){
            String processo1 = ' and StatusContato__c =' + '\'' +contatados[0]+ '\'';
            contatadosGet.add(processo1);
        }else{
        if(contatados.size()>0 && contatados[0] != null){
            String processo1 = 'StatusContato__c =' + '\'' +contatados[0]+ '\'';
            contatadosGet.add(processo1);
        }
        if(contatados.size()>1 && contatados[1] != null){
            String processo2 = ' or StatusContato__c =' + '\'' +contatados[1]+ '\'';
            contatadosGet.add(processo2);
        }
        }

        if(contatados.size() < 1)
            retorno = '';
        else{
        if(contatados.size() == 1)
            retorno = String.join(contatadosGet,',').replace(',','');
        else{
            retorno = ' and (' + String.join(contatadosGet,',').replace(',','')+')';
        }
        }

        return retorno;
    }

    public String getFase(){

        String retorno;

        List<String> fasesGet = new List<String>();

        if(fases.size() == 1 && fases[0] != null){
            String fase1 = ' and StageName__c =' + '\'' +fases[0]+ '\'';
            fasesGet.add(fase1);
        }else{
        if(fases.size()>0 && fases[0] != null){
            String fase1 = 'StageName__c =' + '\'' +fases[0]+ '\'';
            fasesGet.add(fase1);
        }
        if(fases.size()>1 && fases[1] != null){
            String fase2 = ' or StageName__c =' + '\'' +fases[1]+ '\'';
            fasesGet.add(fase2);
        }
        if(fases.size()>2 && fases[2] != null){
            String fase3 = ' or StageName__c =' + '\'' +fases[2]+ '\'';
            fasesGet.add(fase3);
        }
        if(fases.size()>3 && fases[3] != null){
            String fase4 = ' or StageName__c =' + '\'' +fases[3]+ '\'';
            fasesGet.add(fase4);
        }
        if(fases.size()>4 && fases[4] != null){
            String fase5 = ' or StageName__c =' + '\'' +fases[4]+ '\'';
            fasesGet.add(fase5);
        }
        if(fases.size()>5 && fases[5] != null){
            String fase6 = ' or StageName__c =' + '\'' +fases[5]+ '\'';
            fasesGet.add(fase6);
        }
        }

        if(fases.size() < 1)
            retorno = '';
        else{
        if(fases.size() == 1)
            retorno = String.join(fasesGet,',').replace(',','');
        else{
            retorno = ' and (' + String.join(fasesGet,',').replace(',','')+')';
        }
        }

        return retorno;
    }


    public String retornadata(){
        String retorno;
        if(dataInicial == '' && dataFinal == ''){
            retorno = '';
        }
        if(dataInicial == dataFinal){
            retorno = '';
        }else{
        if(dataInicial != '' && dataFinal != ''){
            Date ini = GetDateByString(dataInicial);
            Date fin = GetDateByString(dataFinal);
            retorno = 'and Data_de_Inscri_o__c >='+ String.valueOf(ini) +' and Data_de_Inscri_o__c <= '+ String.valueOf(fin);
        }else{
            retorno = '';
        }
    }
        return retorno;
    }
}