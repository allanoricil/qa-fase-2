public class CobrancaTriggerHandler implements TriggerHandlerInterface{
    private static final CobrancaTriggerHandler instance = new CobrancaTriggerHandler();
    /** Private constructor to prevent the creation of instances of this class.*/
    private CobrancaTriggerHandler() {}
    
    /**
    * @description Method responsible for providing the instance of this class.
    * @return GeneralSourceTriggerHandler instance.
    **/

    public static CobrancaTriggerHandler getInstance() {
        return instance;
    }    
    
    /**
    * Method to handle trigger before update operations. 
    */
    public void beforeUpdate() {
        CRA_CobrancaUtil.BeforeUpdateCobranca(Trigger.new, Trigger.old);
    }
    
    /**
    * Method to handle trigger before insert operations. 
    */ 
    public void beforeInsert() {
        CRA_CobrancaUtil.criaClienteSap(Trigger.new, Trigger.old);
    }
    
    /**
    * Method to handle trigger before delete operations. 
    */
    public void beforeDelete() {

    }
    
    /**
    * Method to handle trigger after update operations. 
    */
    public void afterUpdate()  {
        CRA_CobrancaUtil.AfterUpdateCobranca((Map<Id,Cobranca__c>)Trigger.newMap, (Map<Id, Cobranca__c>)Trigger.oldMap);
    }
    
    /**
    * Method to handle trigger after insert operations. 
    */
    public void afterInsert()  {
        CRA_CobrancaUtil.AfterInsertCobranca((List<Cobranca__c>)Trigger.new);

    }
    /**
    * Method to handle trigger after delete operations. 
    */
    public void afterDelete() {

    }

}