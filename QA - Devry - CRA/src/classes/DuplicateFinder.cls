global class DuplicateFinder implements Database.Batchable<AggregateResult> {    

    public String query;
    public String email;
    public Id toUserId;
    public Id fromUserId;
    public Integer i = 0;
    public Integer k = 0;
    public String csv = 'Duplicados, Nome, CPF \n';

    global Iterable<AggregateResult> start(Database.batchableContext info){
        // just instantiate the new iterable here and return
        return new AggregateResultIterable(query);
    }
 
    global void execute(Database.BatchableContext BC, List<Sobject> scope){
        List<String> cpfs = new List<String>();
        List<Account> acs = new List<Account>();
        List<Account> acx = new List<Account>();
        for(sObject s : scope){
            AggregateResult ar = (AggregateResult) s;
            Integer aux = (Integer)(ar.get('expr0'));
            if (aux > 1){
                system.debug(ar.get('Name'));    
                system.debug(ar.get('expr0'));
                system.debug(ar.get('cpf__c'));
                cpfs.add((String) ar.get('cpf__c'));
                i++;
                k = k + (Integer)(ar.get('expr0'));
                csv = csv + aux + ',' + (String) ar.get('Name') + ',' + (String) ar.get('cpf__c') + '\n';
            }
        }
        //system.debug(cpfs);
        acs = [Select Id, Duplicidade__c From Account Where CPF__c IN : cpfs limit 10000];
        for (Account a : acs){
            a.Duplicidade__c = true;
            acx.add(a);
        }
        //update acx;
        
        Document d = new Document();
        d.Name = 'Contas Duplicadas pelo CPF';
        d.Body = Blob.valueOf(csv);
        d.ContentType = 'text/plain';
        d.Type = 'csv';
        d.FolderId = '00lA0000000w1hP';
        //insert d;    
    	/*
        Messaging.EmailFileAttachment att = new Messaging.EmailFileAttachment();        
        att.setBody(Blob.valueOf(csv));
        att.setContentType('text/plain');
        att.setFileName('Contas duplicadas.csv');    
    
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(new String[] {email});
        mail.setReplyTo('rafael@weengo.com');
        mail.setSenderDisplayName('Suporte Weengo');
        mail.setSubject('Batch Process Completed');
        mail.setPlainTextBody('Contas únicas: ' + i + '. Contas no total: ' + k);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {att});       
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        */
    }
    global void finish(Database.BatchableContext BC){
    

    }
}