global class CraEstudoDirigido {
        public static List<EstudoDirigido> getEstudoDirigidos( String codcoligada, String ra, String codfilial) {
            Map<String, String> mapToken = AuthorizationTokenService.getAuthorizationToken();
             System.debug( 'Entrada no metodo');    

            if( mapToken.get('200') != null ) {
                HttpRequest req = new HttpRequest();
                String url=String.format(WSSetup__c.getValues( 'CraEstudoDirigido').Endpoint__c,new String[]{codcoligada,ra,codFilial});
                req.setEndpoint(url);
                req.setMethod( 'GET' );
                req.setHeader( 'Content-Type', 'application/json' );
                req.setHeader( 'API-TOKEN', WSSetup__c.getValues( 'CraEstudoDirigido' ).API_Token__c );
                req.setHeader( 'AUTH-TOKEN', mapToken.get('200') );
                req.setTimeout( 120000 );
                System.debug( 'Entrada no metodo');    
                try {
                    Http h = new Http();
                    HttpResponse res = h.send( req );
                    system.debug('serverres.getStatusCode(): ' + res.getStatusCode());
                    system.debug('serverres.getBody(): ' +  res.getBody());
                    if( res.getStatusCode() == 200 ){
                         return processJsonBody(res.getBody());
                    }
                    else{
                        system.debug( 'CraEstudoDirigido / ' + res.getStatusCode() + ' / ' + res.getStatus() );
                        return null;
                    }
                    

                } catch ( CalloutException ex ) {
                        system.debug( 'CraEstudoDirigido/ ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
                        return null;
                } 
            } else {
                        system.debug( 'Token  / ' + mapToken.get('401') );
                        return null;
            }  
        }


    private static List <EstudoDirigido>  processJsonBody(String jsonBody ) {
      try
      {  
        Response SR = ( Response ) JSON.deserializeStrict( jsonBody, Response.class );
        if( !SR.CraEstudoDirigidoResult.isEmpty() ){
           return  SR.CraEstudoDirigidoResult;
       }
        else{
            return null; 
        }
       }
        catch(Exception e)
        {   
             system.debug( 'erro ao desserializar ' + e.getMessage());
            return null;            
        }
        
        
    }
    public Class Response {
        public List<EstudoDirigido> CraEstudoDirigidoResult;
    }
   global  Class EstudoDirigido {
         
        public String CodColigada;
        public String CodDisc;
        public String Nome;
        public String ValorDisc;
    }

}