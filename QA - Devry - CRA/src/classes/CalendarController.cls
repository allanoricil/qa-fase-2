public class CalendarController {
    
    
    
    public String  nomeDoCalendario {set;get;}
    public String IES_ID {get;set;}
    public String Campus_Id {get;set;}
    public String[] indiceDiasDaSemana   {set;get;}
    public Calendario_Academico__c calAcademico {set;get;}
    public List<Evento_Calend_rio_Acad_mico__c> eventos {set;get;}
    public Map<Integer,List<CalendarioBO.DiaDoMes>> calendarioAcademico {set;get;}    
  
    
    public CalendarController(){
        indiceDiasDaSemana =   new String[]{'Sun','Mon','Tue','Wed','Thu','Fri','Sat'};
            String   id = ApexPages.currentPage().getParameters().get('id');
        if(id != null){
            calAcademico = [select name, Per_odo_Letivo__r.name,Campus__r.Id_Campus__c, Institui_o__r.id, Institui_o__r.Id_Institui_o__c, (select name, Data_de_nicio__c, Data_de_T_rmino__c, Feriado__c, Aplica_o_AP__c  
                                                                  from Eventos_do_Calend_rio_Acad_mico__r order by Data_de_nicio__c) from Calendario_Academico__c where id =: id  limit 1];
            String periodoLetivo = calAcademico.Per_odo_Letivo__r.name;
            nomeDoCalendario = calAcademico.name;
            IES_ID = calAcademico.Institui_o__r.Id_Institui_o__c;
            Campus_Id =  calAcademico.Campus__r.Id_Campus__c;
            eventos = calAcademico.Eventos_do_Calend_rio_Acad_mico__r;
            CalendarioBO calBO = new CalendarioBO(periodoLetivo,eventos);
            calendarioAcademico = calBO.gerarCalendarioAcademico();
            
        }        
    }
}