public without sharing class OnlineStoreController
{
    public List<SelectOption> campusEstadoOptions { get; set; }
    public List<SelectOption> ofertaProcessoSeletivoOptions { get; set; }
    public String campusEstadoId { get; set; }
    public String nomeIES { get; set; }
    public String cpf { get; set; }
    public String nomeCurso { get; set; }
    public String processoSeletivoID { get; set; }
    public String qtdOfertas { get; set; }
    public String ofertaProcessoSeletivoId { get; set; }
    public integer existeLeadCPF { get; set; }
    public String opportunityId { get; set; }
    public Opportunity opp { get; set; }
    public String formInsc { get; set; }
    public String leadNome { get; set; }
    public String leadSobreNome { get; set; }
    public String leadEmail { get; set; }
    public String leadCpf { get; set; }
    public String leadCelular { get; set; }
    public String leadCampus { get; set; }
    public String leadOferta { get; set; }
    public String oppNomeCandidato { get; set; }
    public String oppCpf { get; set; }
    public String oppEmail { get; set; }
    public String oppCelular { get; set; }
    public String oppIES { get; set; }
    public String oppCampus { get; set; }
    public String oppCurso { get; set; }
    public String cRegistro { get; set; }
    public String cSexo { get; set; }
    public String cDataNascimento { get; set; }
    public String cComoChegou { get; set; }
    public String cNacionalidade { get; set; }
    public String cNaturalidade { get; set; }
    public String cCep { get; set; }
    public String cNumero { get; set; }
    public String cEndereco { get; set; }
    public String cBairro { get; set; }
    public String cCidade { get; set; }
    public String cEstado { get; set; }
    public String cComplemento { get; set; }
    public String cExpProfissional { get; set; }
    public String cFormacaoAcademica { get; set; }
    public String cUltInstituicao { get; set; }      
    public Account accountToInsert { get; set; }
    private Map<String, Oferta__c> mapOferta = new Map<String, Oferta__c>();
    
    public OnlineStoreController(ApexPages.StandardController controller) {
        this.formInsc = 'IbmecStep1';
        campusEstadoOptions = new List<SelectOption>();
        getCampus();
    }
     
    public void getCampus() {
        campusEstadoOptions = new List<SelectOption>();
        for( Campus__c campus : getCampusLista() ) {
            if(campus.Name == 'RJ Centro'  )
            {
                campus.Name = 'Ibmec Online';
            }
            campusEstadoOptions.add( new SelectOption( campus.Id_Campus__c, campus.Name ) );
        }
    }
    
    public List<Campus__c> getCampusLista() {
        return [Select Id, Id_Campus__c, Name from Campus__c where Estado__c != null and Estado__c=: 'RJ' order by Name];
    }

    public void getOfertaProcessoSeletivo() {
        nomeIES = getNomeIES(campusEstadoId);
        String campusEstadoId = apexpages.currentpage().getparameters().get('campusEstado');

        ofertaProcessoSeletivoOptions = new List<SelectOption>();
        for( Oferta__c os : getOfertaProcessoSeletivoLista(campusEstadoId) ) {
            ofertaProcessoSeletivoOptions.add( new SelectOption( os.Id, os.Name ) );
        }

        qtdOfertas = '10';
    }

    public void getInformacoesAdicionais() {
        nomeCurso = getOfertaCurso(ofertaProcessoSeletivoId);
        processoSeletivoID = getOfertaProcessoSeletivoId(ofertaProcessoSeletivoId);
        this.ofertaProcessoSeletivoId = ofertaProcessoSeletivoId;
    }

    public void getLeadExistencia() {
        List<Lead> leads = [SELECT Id, FirstName, LastName, CPF__c, MobilePhone, Email
                    FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];
    
        existeLeadCPF = leads.size();
            
        leadCpf = leads[0].CPF__c;
        leadNome = leads[0].FirstName;
        leadSobreNome = leads[0].LastName;
        leadEmail = leads[0].Email;
        leadCelular = leads[0].MobilePhone;
        this.formInsc = 'IbmecStep2';
    }

    public void getLeadDetalhes() {
        this.formInsc = 'IbmecStep2';
    }
    
	public String getLeadIdByCPF( String cpf ) {
        String retorno = '';
        if (cpf != null) {
            retorno = [SELECT Id, Name FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1].Id;
            
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;                    
    }

    public void setConfirmarDados() {
        // Esta atualização do lead é fundamental, pois armazenamos as informações do segundo step.
        atualizaLead(getLeadIdByCPF(cpf));
      
        if (!contaDuplicada(cpf)) {
            Database.LeadConvert lc = new Database.LeadConvert();
            lc.setLeadId(getLeadIdByCPF(cpf));

            LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
            lc.setConvertedStatus(convertStatus.MasterLabel);
            Database.LeadConvertResult lcr = Database.convertLead(lc);
            if(lcr.isSuccess()){
                opportunityId = lcr.getOpportunityId();
                atualizaOportunidade(opportunityId);
            }
            System.assert(lcr.isSuccess());
        } else {
            // Coleta informações do lead
            Lead leadPopulado = [SELECT Id, Name, FirstName, LastName, CPF__c, Phone, MobilePhone, Email, RG__c, Sexo__c, Data_de_Nascimento__c, Como__c, Nacionalidade__c,
                    Naturalidade__c, CEP__c, N_mero__c, Rua__c, Bairro__c, Cidade__c, Estado__c, Complemento__c, Oferta__c, Nome_do_Curso__c, Processo_seletivo__c
                    FROM Lead
                    WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];

            // Atualiza a conta
            Account contaAtualiza = [SELECT Id 
                FROM Account
                WHERE CPF_2__c = :cpf];
            
            // Informações coletadas no step1
            contaAtualiza.FirstName = leadPopulado.FirstName;
            contaAtualiza.LastName = leadPopulado.LastName;
            contaAtualiza.PersonEmail = leadPopulado.Email;
            contaAtualiza.Phone = leadPopulado.Phone;
            contaAtualiza.PersonMobilePhone = leadPopulado.MobilePhone;

            // Informações coletadas no step2
            contaAtualiza.RG__c = leadPopulado.RG__c;
            contaAtualiza.Sexo__c = leadPopulado.Sexo__c;
            contaAtualiza.DataNascimento__c = leadPopulado.Data_de_Nascimento__c;
            contaAtualiza.Como__c = leadPopulado.Como__c;
            contaAtualiza.Nacionalidade__c = leadPopulado.Nacionalidade__c;
            contaAtualiza.Naturalidade__c = leadPopulado.Naturalidade__c;
            contaAtualiza.CEP__c = leadPopulado.CEP__c;
            contaAtualiza.N_mero__c = leadPopulado.N_mero__c;
            contaAtualiza.Rua__c = leadPopulado.Rua__c;
            contaAtualiza.Bairro__c = leadPopulado.Bairro__c;
            contaAtualiza.Cidade__c = leadPopulado.Cidade__c;
            contaAtualiza.Estado__c = leadPopulado.Estado__c;
            contaAtualiza.Complemento__c = leadPopulado.Complemento__c;

            update contaAtualiza;
            
            // Cria oportunidade
            String tipoProcessoSeletivo = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Cursos de Extensão').getRecordTypeId();

            Opportunity opp          = new Opportunity (
                AccountId            = contaAtualiza.Id,
                Name                 = leadPopulado.Name,
                RecordTypeId         = tipoProcessoSeletivo,
                StageName            = 'Inscrito',
                CloseDate            = System.Today(),
                CPF__c               = leadPopulado.CPF__c,
                FezProvaENEM__c      = 'Não',
                Telefone__c          = leadPopulado.Phone,
                Celular__c           = leadPopulado.MobilePhone,
                Oferta__c            = leadPopulado.Oferta__c,
                //Curso__c             = leadPopulado.Nome_do_Curso__c,
                Processo_seletivo__c = leadPopulado.Processo_seletivo__c
            );

            // Insere a oportunidade e coleta seu ID
            insert opp;

            // Atualiza a oportunidade
            opportunityId = opp.Id;
            atualizaOportunidade(opportunityId);

            // Muda status do Lead para Convertido e deixa uma observação no mesmo informando que está convertido, pois já existia uma conta criada para o mesmo.
            leadPopulado.Status = 'Convertido';
            update leadPopulado;


        }
        this.formInsc = 'IbmecStep3';
        getOpportunidadeCampos(opportunityId);
    }
    
    public PageReference efetuarPagamento(){        
        return paymentRegistration( opportunityId);
    }

    /*
        Valida se a conta ja existe com o mesmo CPF
    */
    private Boolean contaDuplicada(String cpf) {
        List<Account> accountDuplicate = AccountDAO.getInstance().getAccountByCpf(cpf);
        if( accountDuplicate.size() > 0 ) {
            return true;
        }
        return false;  
    }
    
    public void atualizaLead(String leadId) {
        try {
            Lead mLead = [SELECT Id, Name, RG__c, Data_de_Nascimento__c
                                      FROM Lead 
                                      WHERE Id = :leadId
                                      LIMIT 1];

            mLead.RG__c = cRegistro;
            mLead.Sexo__c = cSexo;
            mLead.Data_de_Nascimento__c = Date.valueOf(cDataNascimento);
            mLead.Como__c = cComoChegou;
            mLead.Nacionalidade__c = cNacionalidade;
            mLead.Naturalidade__c = cNaturalidade;
            mLead.CEP__c = cCep;
            mLead.N_mero__c = cNumero;
            mLead.Rua__c = cEndereco;
            mLead.Bairro__c = cBairro;
            mLead.Cidade__c = cCidade;
            mLead.Estado__c = cEstado;
            mLead.Complemento__c = cComplemento;
           
            update mLead;
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
    }
    
    /*
        Redireciona para pagina de escolha do tipo de pagamento
        @action botão inscrever
    */
    private PageReference paymentRegistration( String opportunityId ) {
        PageReference paymentPage = Page.SRM_PagamentoInscricao;
         
        Map<String,String> parameters = paymentPage.getParameters();
        parameters.put( 'id', opportunityId );

        return paymentPage;
    }
    
    public Opportunity getOpportunityById( String id ) {
        Opportunity opp;

        try {
            opp = [SELECT Id, Nome_do_Candidato__c, CPF__c, Account.PersonMobilePhone, Account.PersonEmail, NomeCampus__c, Oferta__r.Curso__r.Name, Processo_seletivo__c, IES__c, Sede__c
                    FROM Opportunity
                    WHERE Id = :id];
        } catch (Exception e) {
            opp = null;
        }

        return opp;
    } 
    
    public void getOpportunidadeCampos(String oppId) {
        Opportunity opp =  getOpportunityById(oppId);
        oppNomeCandidato = (opp == null) ? null : opp.Nome_do_Candidato__c;
        oppCpf = (opp == null) ? null : opp.CPF__c;
        oppEmail = (opp == null) ? null : opp.Account.PersonEmail;
        oppCelular = (opp == null) ? null : opp.Account.PersonMobilePhone;
        oppIES = (opp == null) ? null : opp.IES__c;
        oppCampus = (opp == null) ? null : opp.NomeCampus__c == 'RJ Centro' ? 'Ibmec Online' : opp.NomeCampus__c;
        oppCurso = (opp == null) ? null : opp.Oferta__r.Curso__r.Name;
    }

    public List<Oferta__c> getOfertaProcessoSeletivoLista(String campusId) {
        String tipoProcessoSeletivo = Schema.SObjectType.Processo_seletivo__c.getRecordTypeInfosByName().get('Extensão').getRecordTypeId();
        return [Select Id, Name from Oferta__c where Processo_seletivo__r.Campus__c != null and Processo_seletivo__r.Status_Processo_Seletivo__c = 'Aberto'
                and Processo_seletivo__r.Data_Validade_Processo__c >= TODAY and Inativa__c=false and Envia_para_Academus_RM__c=true and Processo_seletivo__r.RecordTypeId = :tipoProcessoSeletivo
                and ID_Academus_Oferta__c != null and C_digo_da_Oferta__c != null and Processo_seletivo__r.Matr_cula_Online__c = 'Sim' and Processo_seletivo__r.Ativo__c = true
                and Processo_seletivo__r.ID_Academus_ProcSel__c != null and Processo_seletivo__r.C_digo_Processo_Seletivo__c != null and Id_Campus__c = : campusId];
    }

    public String getNomeIES(String campusId) {
        String retorno = '';
        if (campusId != null) {
            retorno = [Select Id, Institui_o__r.Name from Campus__c where Id_Campus__c = :campusId limit 1].Institui_o__r.Name;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;
    }

    public String getOfertaCurso(String ofertaId) {
        String retorno = '';
        if (ofertaId != null) {
            retorno = [Select Id,Name,Curso__r.Name,Inativa__c from Oferta__c where Id = :ofertaId limit 1].Curso__r.Name;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        }
        return retorno;
    }
    
    public Decimal getOfertaValor(String ofertaId) {
        Decimal retorno;
        if (ofertaId != null) {
            retorno = [Select Id, Valor_da_Matr_cula__c from Oferta__c where Id = :ofertaId limit 1].Valor_da_Matr_cula__c;          
        } 
        return retorno;
    } 
    
    /*public void getDescontos(String ofertaId, Opportunity opportunity) {
        system.debug('DEGUG do Valor');
        if (ofertaId != null && opportunity.ID != null) {
            WS_RMAluno.syncToServer(ofertaId, opportunity.ID, opportunity.CPF__c.Replace('.','').Replace('-','').Replace(' ',''));
        }      
    }*/

    public String getOfertaProcessoSeletivoId(String ofertaId) {
        String retorno = '';
        if (ofertaId != null) {
            retorno = [Select Id,Processo_seletivo__c from Oferta__c where Id = :ofertaId limit 1].Processo_seletivo__c;
            if (retorno.length() == 0) {
                retorno = 'Não definido.';
            }
        } else {
            retorno = 'Não definido.';
        } 
        return retorno;
    }

    public static List<Lead> getLeadByCPF( String cpf ) {
        return [SELECT Id, FirstName, LastName, CPF__c, MobilePhone, Email
                FROM Lead
                WHERE CPF__c = :cpf And LeadSource='Site Ibmec' and IsConverted=false and CreatedDate in (today, yesterday) Order by CreatedDate Desc Limit 1];
    }
    
    public void atualizaOportunidade(String opportunityId) {
        try {
            Opportunity mOpp = [Select Id, CPF__c, Amount, Account.FirstName, Account.LastName from Opportunity Where Id = : opportunityId ];
            mOpp.Amount =  getOfertaValor(ofertaProcessoSeletivoId);
            mOpp.Primeiro_Nome__c = mOpp.Account.FirstName;
            mOpp.Sobrenome__c = mOpp.Account.LastName;
            update mOpp;
            
			//Aplica DESCONTO se for 'COLABORADOR', 'ALUNO' e 'EX-ALUNO'
            //getDescontos(ofertaProcessoSeletivoId, mOpp);
            
        } catch(Exception e) {
            System.debug('An unexpected error has occurred: ' + e.getMessage());
        }
    }
}