public class CRA_OpportunityEAD {
	public static void setEADOpportunityInitialValues(List<Opportunity> newOpportunities) {
		for(Opportunity opp: newOpportunities){
			Opportunity oppAux = new Opportunity();
			oppAux.Id = opp.Id;
	   	    if(!String.isBlank(opp.EAD__c) && opp.EAD__c.equals('Sim')){
	            if(opp.Processo_Tipo_do_processo_seletivo__c.equals('ENEM')){
	                if(opp.Nota_de_Corte__c != null){
	                    if(opp.QualNotaMediaENEM__c >= opp.Nota_de_Corte__c){
	                        SRM_CadastroCandidato.insertQueue(oppAux.Id);          
	                    } 
	                }
	            	oppAux.StageName = 'Inscrito';
	            }
	            oppAux.Status_Interesse_do_contato__c = 'Novo';
	            oppAux.Situacao_Matricula__c = '-';
	            update oppAux;
	        }
	    }
	}
}