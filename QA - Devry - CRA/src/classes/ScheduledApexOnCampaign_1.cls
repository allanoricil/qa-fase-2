global class ScheduledApexOnCampaign_1 implements Schedulable {    
 
    global void execute(SchedulableContext sc) {    
         
        List<Contact> conts = new List<Contact>();        
        List<Account> acs = new List<Account>();        
        List<CampaignMember> cms = new List<CampaignMember>();
        List<Campaign> cs = new List<Campaign>();
        List<ID> ids = new List<ID>();
        if ([Select count() From Campaign Where Processo_seletivo__c != null AND IsActive =: true AND Empresa_Parceira__c != null AND Tipo_de_Matr_cula__c != null] > 0){
            cs = [Select id, Name, Processo_seletivo__c, Empresa_Parceira__c, Tipo_de_Matr_cula__c From Campaign Where Processo_seletivo__c != null AND IsActive =: true AND Empresa_Parceira__c != null AND Tipo_de_Matr_cula__c != null];
            for (Campaign c : cs){
                if ([Select count() From CampaignMember Where CampaignId =: c.Id] > 0){
                    CampaignMember[] cmx = [Select id, ContactId From CampaignMember Where CampaignId =: c.Id];
                    for (CampaignMember cx : cmx){
                        ids.add(cx.ContactId);
                    }
                }
                if ([Select count() From Contact Where Processo_seletivo_de_Interesse__c =: c.Processo_seletivo__c AND AccountId =: c.Empresa_Parceira__c AND Tipo_de_Matr_cula__c =: c.Tipo_de_Matr_cula__c AND Id NOT IN : ids] > 0){
                    conts = [Select id From Contact Where Processo_seletivo_de_Interesse__c =: c.Processo_seletivo__c AND AccountId =: c.Empresa_Parceira__c AND Tipo_de_Matr_cula__c =: c.Tipo_de_Matr_cula__c AND Id NOT IN : ids];
                    for (Contact cont : conts){
                        CampaignMember cm = new CampaignMember(CampaignId = c.Id, ContactId = cont.Id, Status = 'Enviado');
                        cms.add(cm);
                    }
                }
                if ([Select count() From Account Where Processo_seletivo_de_Interesse__pc =: c.Processo_seletivo__c AND Empresa_Escola__c =: c.Empresa_Parceira__c AND Tipo_de_Matr_cula__pc =: c.Tipo_de_Matr_cula__c AND PersonContactId NOT IN : ids] > 0){
                    acs = [Select PersonContactId From Account Where Processo_seletivo_de_Interesse__pc =: c.Processo_seletivo__c AND Empresa_Escola__c =: c.Empresa_Parceira__c AND Tipo_de_Matr_cula__pc =: c.Tipo_de_Matr_cula__c AND PersonContactId NOT IN : ids];
                    for (Account ac : acs){
                        CampaignMember cm = new CampaignMember(CampaignId = c.Id, ContactId = ac.PersonContactId, Status = 'Enviado');
                        cms.add(cm);
                    }
                }
            }
        }
        insert cms;
    }
    @isTest(SeeAllData=true)
    static  void UnitTest1() {

        Processo_seletivo__c ps = [Select id From Processo_seletivo__c limit 1];
        Recordtype rec1 = [Select id From Recordtype Where Name =: 'Empresa' limit 1];
        Recordtype rec2 = [Select id From Recordtype Where Name =: 'Conta Pessoal' limit 1];
        Account ep = new Account(recordtypeid = rec1.id, name = 'teste');
        ep.CPF_2__c = '119.301.688-61';
        insert ep;
        Campaign cp = new Campaign(name = 'teste', Processo_seletivo__c = ps.id, IsActive = true, Empresa_Parceira__c = ep.id, Tipo_de_Matr_cula__c = 'Vestibular Tradicional');
        insert cp;
        Contact cont = new Contact(Lastname = 'teste', Processo_seletivo_de_Interesse__c = ps.id, AccountId = ep.id, Tipo_de_Matr_cula__c = 'Vestibular Tradicional');
        insert cont;
        Account ac = new Account(Lastname = 'teste', Processo_seletivo_de_Interesse__pc = ps.id, Empresa_Escola__c = ep.id, Tipo_de_Matr_cula__pc = 'Vestibular Tradicional');
        ac.CPF_2__c = '634.313.627-45';
        insert ac;

        Test.startTest();
    
        // Schedule the test job 
        ScheduledApexOnCampaign_1 skedule = new ScheduledApexOnCampaign_1();
        String timeOfRun = '0 00 * * * ?'; 
        String jobId = system.schedule('Relacioamento Contatos com Campanhas', timeOfRun, skedule);
    
        // Get the information from the CronTrigger API object 
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];
    
        // Verify the job has not run 
        System.assertEquals(0, ct.TimesTriggered);
    
       Test.stopTest();
   }    
}