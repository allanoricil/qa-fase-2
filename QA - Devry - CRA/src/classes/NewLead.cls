public class NewLead {

    public String lid;
    public Lead ld = new Lead();    
    public RecordType rec1 = new RecordType();
    
    public NewLead(ApexPages.StandardController controller) {
        lid = ApexPages.currentPage().getParameters().get('id');
        rec1 = [Select id From RecordType Where Name =: 'Corporativo'];
    }
    public Pagereference Enviar(){
        try{
            ld.RecordTypeId = rec1.id;
            insert ld;
        }catch(DMLException e){  
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Erro ao enviar formulário.' + e.getMessage()));
            //ApexPages.addMessage(e.getMessage());
            return null;  
        }
        PageReference p = new PageReference('/apex/LeadSent');
        p.setRedirect(true);  
        return p;    
    }
    public Lead getld(){
        return ld;
    }
    public void setld(Lead ld){
        this.ld = ld;
    }    

}