/*
    @author Diego Moreira
    @class Classe de serviços Braspag usando API REST 
*/
public class SRM_BraspagServicesRest {
	/*
        Singleton
    */
    private static final SRM_BraspagServicesRest instance = new SRM_BraspagServicesRest();    
    private SRM_BraspagServicesRest(){}
    
    public static SRM_BraspagServicesRest getInstance() {
        return instance;
    }

    /*
		Faz a chamada de pagamento via boleto
		@param payload JSON de envio com as instruções do boleto
    */
    public Map<String, Object> pagamentoBoleto( String merchantId, String merchantKey, String payload ) {
    	HttpRequest req = new HttpRequest();
		req.setEndpoint( 'https://apisandbox.braspag.com.br/v1/sales/' );
		req.setMethod( 'POST' );
		req.setHeader( 'content-type', 'application/json' );
        req.setHeader( 'MerchantId', merchantId );
        req.setHeader( 'MerchantKey', merchantKey );
        req.setBody( payload );

		try {
			Http h = new Http();
			HttpResponse res = h.send( req );
            System.debug('>>>res ' + res.getStatusCode());
            System.debug('>>>res ' + res.getBody());

			if( res.getStatusCode() == 404 ) return null;
			return ( Map<String, Object> ) JSON.deserializeUntyped( res.getBody() );
		} catch( CalloutException ex ) {
            System.debug('>>>res ' + ex.getMessage());
			return null;
		} catch( Exception ex ) {
            System.debug('>>>res ' + ex.getMessage());
            return null;
        }
    }

    /*
        Monsta o Json do boleto
        @param titulos Dados a seram gerados
    */
    public String criaJsonBoleto( String chave, Opportunity opportunity, Institui_o__c instituicao ) {
        Date dateTitulo = Date.today();
        Integer valorTotal = Integer.valueOf( String.valueOf(opportunity.Amount).replace('.', '') );
        Date vencimento = TituloBO.getInstance().validaDiaDeSemana( dateTitulo.addDays( Integer.valueOf( opportunity.Processo_seletivo__r.Vencimento_Boleto_dias__c ) ) );

        JSONGenerator gen = JSON.createGenerator(true);
        gen.writeStartObject();
        gen.writeStringField( 'MerchantOrderId', chave );
        gen.writeFieldName( 'Customer' );
            gen.writeStartObject();
            gen.writeStringField( 'Name', opportunity.Account.Name );
            gen.writeEndObject();
        gen.writeFieldName( 'Payment' );
            gen.writeStartObject();
            gen.writeStringField( 'Type', 'Boleto' );
            gen.writeNumberField( 'Amount', valorTotal );
            gen.writeStringField( 'Carrier', 'Simulado' );
            gen.writeStringField( 'Address', instituicao.EnderecoInstituicao__c );
            //gen.writeStringField( 'BoletoNumber', titulos.NumeroBoleto__c ); Nosso numero
            gen.writeStringField( 'Assignor', instituicao.Name );
            //gen.writeStringField( 'Demonstrative', '' ); Precisa saber o que é
            gen.writeDateField( 'ExpirationDate', vencimento );
            gen.writeStringField( 'Identification', instituicao.CNPJ__c );
            gen.writeStringField( 'Instructions', opportunity.Processo_seletivo__r.Instrucoes_Boleto__c );
            //precisa do CPF do cliente
            //preciso saber se agencia e codigo beneficiario serão passados?
            gen.writeEndObject();      
        gen.writeEndObject(); 

        return gen.getAsString(); 
    }
}