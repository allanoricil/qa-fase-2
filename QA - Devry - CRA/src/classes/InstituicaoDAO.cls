/*
    @author Rogério Oliveira
    @class Classe DAO do objeto Instiruicao
*/
public with sharing class InstituicaoDAO extends SObjectDAO {
	/*
        Singleton
    */
    private static final InstituicaoDAO instance = new InstituicaoDAO();    
    private InstituicaoDAO(){}
    
    public static InstituicaoDAO getInstance() {
        return instance;
    }

    /*
        Retornas as instituições pelo id
        @param instituicaoId id da instituição
    */
    public List<Institui_o__c> getInstituicaoById( String instituicaoId ) {
    	return [SELECT Id, Name, EnderecoInstituicao__c, CNPJ__c
    			FROM Institui_o__c
    			WHERE Id = :instituicaoId];
    }

    /*
        Retornas as instituições pelo nome
        @param instituicaoNameList nomes das instituições
    */
    public List<Institui_o__c> getInstituicaoByName( List<String> instituicaoNameList ) {
        return [SELECT Id, Name, Lead_Map__c
                    FROM Institui_o__c
                    WHERE Lead_Map__c in :instituicaoNameList];
    }

    /*
        Retorna todas as intituições
    */
    public List<Institui_o__c> getAllInstituicoes() {
        return [SELECT Id, Name 
                FROM Institui_o__c];
    }
}