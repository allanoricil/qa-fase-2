public without sharing class CRA_CaseItemConsoleController {

	public final Case myCase{get;set;}
    public Boolean error{get;set;}
    public List<SubjectWrapper> SubjectWrapperList{get;set;}
    public List<CRA_Empresas.Empresas> EmpresasList{get;set;}
    public List<CRA_Empresas.Empresas> EmpresasListFilter{get;set;}
    public String empresaCodigo{get;set;}
    public String empresaDescricao{get;set;}
    public List<CRA_TurmasEproList.TurmasEPro> TurmasEPro{get;set;}
    public List<SelectOption> TurmasEProSelect{set;}
    public String TurmaEPro{get;set;}
    public PageReference redirectTo{get;set;}

    // The extension constructor initializes the private member
    // variable mysObject by using the getRecord method from the standard
    // controller.
    public CRA_CaseItemConsoleController(ApexPages.StandardController stdController) {
        List<String> myFields = new List<String>();
        
            myFields.add('Id');
            myFields.add('Status');
            myFields.add('Assunto__c');
            myFields.add('Assunto__r.Tipo_Disciplinas__c');
            myFields.add('Assunto__r.Name');
            myFields.add('Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c');
            myFields.add('Aluno__r.Campus_n__r.C_digo_do_Campus__c');
            myFields.add('Aluno__r.R_A_do_Aluno__c');
            myFields.add('Assunto__r.Tipo_de_Registro_do_Caso__c');
        if(!Test.isRunningTest()){
            stdController.addFields(myFields);
        }
        this.myCase = (Case)stdController.getRecord();
        redirectTo = new pageReference('/' + myCase.Id);
        if(myCase.Status == 'Novo'){
            if(!String.isBlank(myCase.Assunto__r.Tipo_Disciplinas__c) || myCase.Assunto__r.Name == 'Inclusão de Disciplina não Ofertada em Semestre'){
                SubjectWrapperList = new List<SubjectWrapper>();
                getSubjects();
            }
            else if(myCase.Assunto__r.Name == 'Assinatura do Termo de Estágio'){
                //codigo coligada, RA, codigo filial
                EmpresasList = CRA_Empresas.getEmpresas(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c);            
                //EmpresasList = CRA_Empresas.getEmpresas('1','162I010903','1');
                system.debug('EmpresasList: ' + EmpresasList);
                if(EmpresasList == null){
                    EmpresasList = new List<CRA_Empresas.Empresas>();
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não foi possível encontrar empresas conveniadas. Preencha as informações cadastrais nos campos de detalhe para prosseguir com a solicitação.'));
                }
                EmpresasListFilter = EmpresasList;
            }
            else if(myCase.Assunto__r.Tipo_de_Registro_do_Caso__c == 'CRA - Turma - EnglishPro'){
                TurmasEPro = CRA_TurmasEproList.getTurmasEpro(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c);
                if(TurmasEPro == null){
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não foi possível encontrar turmas do EnglishPro para prosseguir com a solicitação.'));
                }
                else if(TurmasEPro.isEmpty()){
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não foi possível encontrar turmas do EnglishPro para prosseguir com a solicitação.'));
                }
            }
            else{
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Esse tipo de serviço não precisa dessa seleção.'));
            }
        }
        else{
            error = true;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'O caso deve estar com Status igual a Novo para realizar esta ação.'));
            
        }

    }

    public void getSubjects(){
        Map<String, Object> SubjectsMap = new Map<String, String>();
        if(!String.isBlank(myCase.Assunto__r.Tipo_Disciplinas__c)){
            if(myCase.Assunto__r.Tipo_Disciplinas__c == 'INCLUSÃO' || myCase.Assunto__r.Tipo_Disciplinas__c == 'GRADECHEIA'){
                List<CRAOferta.CraOfertasResult> IncSubjects = CRAOferta.getOfertas(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c, myCase.Assunto__r.Tipo_Disciplinas__c);
                System.debug(IncSubjects);
                if(!IncSubjects.isEmpty()){
                    for(CRAOferta.CraOfertasResult subject : IncSubjects){
                        SubjectWrapper subWrapper = new SubjectWrapper();
                        subWrapper.Name = subject.Nome;
                        subWrapper.Codigo = subject.codDisc;
                        subWrapper.Tipo = myCase.Assunto__r.Tipo_Disciplinas__c;
                        subWrapper.Valor = subject.Valor;
                        subWrapper.Turma = subject.CodTurma;
                        subWrapper.Horario = subject.Horario;
                        SubjectWrapperList.add(subWrapper);
                    }
                }
                else{
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não existem disciplinas compatíveis com essa solicitação.'));
                    error = true;
                }
            }
            
            else if(myCase.Assunto__r.Tipo_Disciplinas__c == 'INCLUSÃO COM QUEBRA'){
                List<CraOfertaQuebra.OfertaQuebra> IncSubjects = CraOfertaQuebra.getInclusaoQuebra(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c);
                if(!IncSubjects.isEmpty()){
                    for(CraOfertaQuebra.OfertaQuebra subject : IncSubjects){
                        SubjectWrapper subWrapper = new SubjectWrapper();
                        subWrapper.Name = subject.Nome;
                        subWrapper.Codigo = subject.codDisc;
                        subWrapper.Tipo = 'INCLUSÃO COM QUEBRA';
                        subWrapper.Valor = subject.Valor;
                        subWrapper.Turma = subject.CodTurma;
                        subWrapper.Horario = subject.Horario;
                        SubjectWrapperList.add(subWrapper);
                    }
                }
                else{
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não existem disciplinas compatíveis com essa solicitação.'));
                }
            }
			else if(myCase.Assunto__r.Tipo_Disciplinas__c == 'PROGRAMA DE DISCIPLINAS'){
				List<CraProgramaDisc.ProgramaDisc> progSubjects = CraProgramaDisc.getProgramaDisc(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c);
                system.debug('progSubjects:: ' + progSubjects);
                if(!progSubjects.isEmpty()){
                    for(CraProgramaDisc.ProgramaDisc subject : progSubjects){
                        SubjectWrapper subWrapper = new SubjectWrapper();
                        subWrapper.Name = subject.Disciplina;
                        subWrapper.Codigo = subject.codDisc;
                        subWrapper.Tipo = subject.Tipo;
                        subWrapper.Valor = subject.Valor;
                        subWrapper.Turma = subject.CodTurma;
                        subWrapper.Horario = subject.Horario;
                        SubjectWrapperList.add(subWrapper);
                    }
                }
                else{
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não existem disciplinas compatíveis com essa solicitação.'));
                }
			}
            else {
                //SubjectsMap = CRA_SubjectsQuery.executeDiscHistorico(myCase.Aluno__r.R_A_do_Aluno__c, 'R2VyYXJTZW5oYU1hc3Rlcg==', myCase.Assunto__r.Tipo_Disciplinas__c);
                List<Object> Subjects = new List<Object>();
                system.debug('SubjectsMap ' + SubjectsMap);
                if(myCase.Assunto__r.Tipo_Disciplinas__c == 'PERÍODO LETIVO ATUAL'){                    
                    SubjectsMap = CRA_SubjectsQuery.executeDiscEmCurso(myCase.Aluno__r.R_A_do_Aluno__c, 'R2VyYXJTZW5oYU1hc3Rlcg==', 'TODAS');
                    Subjects = (List<Object>)SubjectsMap.get('CraDisciplinasEmCursoResult');                    
                }
                else if(myCase.Assunto__r.Tipo_Disciplinas__c == 'DISCIPLINAS EM CURSO'){                    
                    SubjectsMap = CRA_SubjectsQuery.executeDiscEmCurso(myCase.Aluno__r.R_A_do_Aluno__c, 'R2VyYXJTZW5oYU1hc3Rlcg==', '');
                    Subjects = (List<Object>)SubjectsMap.get('CraDisciplinasEmCursoResult');                    
                }
                else{
                    SubjectsMap = CRA_SubjectsQuery.executeDiscHistorico(myCase.Aluno__r.R_A_do_Aluno__c , 'R2VyYXJTZW5oYU1hc3Rlcg==', myCase.Assunto__r.Tipo_Disciplinas__c); 
                    Subjects = (List<Object>)SubjectsMap.get('historicoResult');
                }
                system.debug('Subjects ' + Subjects);
                if(Subjects != null && Subjects.size() > 0){
                    for(Object disciplina : Subjects){
                        Map<String, Object> objDisciplina = (Map<String, Object>) disciplina;
                        SubjectWrapper subWrapper = new SubjectWrapper();                        
                        if(myCase.Assunto__r.Tipo_Disciplinas__c == 'DISCIPLINAS EM CURSO'){
                            subWrapper.Name = (String) objDisciplina.get('NOMEDISC');
                            subWrapper.Turma = (String) objDisciplina.get('CODTURMA');
                            subWrapper.Codigo = (String) objDisciplina.get('CODDISC');
                            subWrapper.Professor = (String) objDisciplina.get('NOMEPROFESSOR');
                            //subWrapper.Periodo = (String) objDisciplina.get('PERIODOLETIVO');
                            subWrapper.Tipo = 'DISCIPLINAS EM CURSO';
                        }                                                
                        else{
                        	subWrapper.Periodo = (String) objDisciplina.get('codperlet'); 
                            subWrapper.Name = (String) objDisciplina.get('disciplina');
                            subWrapper.Codigo = (String) objDisciplina.get('coddisc');
                            subWrapper.Tipo = (String) objDisciplina.get('tipo');
                        }
                        SubjectWrapperList.add(subWrapper);
                    }
                }
                else{
                    error = true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não existem disciplinas compatíveis com essa solicitação.'));
                }      
            }
            
        }
        if(myCase.Assunto__r.Name == 'Inclusão de Disciplina não Ofertada em Semestre'){
            List<CRAEstudoDirigido.EstudoDirigido> estudoDirList = CraEstudoDirigido.getEstudoDirigidos(myCase.Aluno__r.Campus_n__r.Institui_o__r.C_digo_da_Coligada__c, myCase.Aluno__r.R_A_do_Aluno__c, myCase.Aluno__r.Campus_n__r.C_digo_do_Campus__c);
            if(estudoDirList != null && !estudoDirList.isEmpty()){
                for(CRAEstudoDirigido.EstudoDirigido estudoDir: estudoDirList){
                    SubjectWrapper subWrapper = new SubjectWrapper();
                    subWrapper.Name = estudoDir.Nome;
                    subWrapper.Codigo = estudoDir.CodDisc;
                    subWrapper.Tipo = 'Estudo Dirigido';
                    SubjectWrapperList.add(subWrapper);
                }
            }
            else{
                error = true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.WARNING,'Não existem disciplinas compatíveis com essa solicitação.'));
            }
            system.debug('SubjectWrapperList ' + SubjectWrapperList);
        }         
    }

    public List<SelectOption> getTurmasEProSelect(){
        List<SelectOption> turmasEProSelect = new List<SelectOption>();
        if(TurmasEPro != null){
            for(CRA_TurmasEproList.TurmasEPro turma : TurmasEPro){
                system.debug('turma Epro' + turma);
                turmasEProSelect.add( new SelectOption(turma.coddisc, turma.nome));
            }
    	}
        return turmasEProSelect;
    }

    public pageReference selectSubjects(){
        List<Item_do_Caso__c> items = new List<Item_do_Caso__c>();
        for(SubjectWrapper subject : SubjectWrapperList){
            if (subject.Selected){
                Item_do_Caso__c item = new Item_do_Caso__c();
                item.Caso__c = myCase.Id;
                item.Codigo_da_disciplina__c = subject.Codigo;
                item.Disciplina__c = subject.Name;
                item.Tipo_da_Disciplina__c = subject.Tipo;
                item.Periodo__c = subject.Periodo;
                item.Horario__c = subject.Horario;
                item.Turma__c = subject.Turma;
                item.Professor__c = subject.Professor;
                items.add(item);
            }
        }
        if(!items.isEmpty())
            insert items;
        return redirectTo;
    }

    public pageReference selectEmpresa(){
        myCase.Nome_da_Empresa_Estagio__c = empresaDescricao;
        myCase.Codigo_Empresa_Estagio__c = empresaCodigo; 
        update myCase;
        return redirectTo;
    }

    public pageReference selectTurma(){
        myCase.Turma__c = TurmaEPro;
        update myCase;
        return redirectTo; 
    }

    public PageReference runSearch() {
        EmpresasListFilter = new List<CRA_Empresas.Empresas>();
        String empresaName = Apexpages.currentPage().getParameters().get('empresaInput');
        if(empresaName != null && empresaName != ''){
            for(CRA_Empresas.Empresas emp : EmpresasList){
                if(emp.descricao.containsIgnoreCase(empresaName)){
                   EmpresasListFilter.add(emp);                
                }
            } 
        }
        else 
            EmpresasListFilter = EmpresasList;
        return null;
  }

    public Class SubjectWrapper{
        public String Codigo{get; set;}
        public String Name{get; set;}
        public String Tipo{get; set;}
        public Boolean Selected{get; set;}
        public String Periodo{get; set;}
        public String Turma{get; set;}
        public String Horario{get; set;}
        public String Valor{get;set;}
        public String Professor{get; set;}
        public SubjectWrapper(){
            this.Selected = false;
        }
    }

    
}