global class Batch_UpdateOwnerOpportunity implements Database.Batchable<sObject>{
    List<Opportunity> ownerOpp = new List<Opportunity>();

    global Batch_UpdateOwnerOpportunity(List<Opportunity> ownerOppLimit){
        ownerOpp = ownerOppLimit;
    }
    
    // Start Method
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, OwnerId FROM Opportunity WHERE Id IN: ownerOpp LIMIT 20]);
    }
    
    // Execute Logic
    global void execute(Database.BatchableContext BC, List<Opportunity>scope){        
        // Logic to be Executed batch wise
        for(Opportunity opp : scope)
		{            
            opp.OwnerId = Label.DATA_LOADER;            
            update opp;
		}
    }
    
    global void finish(Database.BatchableContext BC){
        // Logic to be Executed at finish
    }
}