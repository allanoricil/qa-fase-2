global class SRM_ScheduledCheckPhone implements Schedulable{


	global void execute(SchedulableContext sc) { 

		List<Lead> listLead = LeadDAO.getInstance().getLeadCheckPhone();
		SRM_WSAsterixService.getInstance().asterixCheckPhone( listLead );
	}
}