public with sharing class AssessmentList {
	
	private List<Assessment> AssessmentList{get;private set;}

	public List<Assessment> getAssessmentList(){
		return AssessmentList;
	}
}