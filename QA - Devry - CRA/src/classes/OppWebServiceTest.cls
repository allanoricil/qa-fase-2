@isTest

private class OppWebServiceTest 
{

    @isTest static void callOut()
    {
        String pnomeOpp = 'opp' + String.valueOf(System.now()), 
               pnomeCandidato = 'cand' + String.valueOf(System.now()),
               pSobreNomeCandidato = 'sbr' + String.valueOf(System.now()),
               pofertaID =  'a0LZ0000002bb3O', //getOfertaID(),
               popcaoCurso2 = 'a0LZ0000002bb3O', //getOfertaID(),
               pRG = '1245',
               pPassporte = '1245',
               pTreineiro = '',
               pCPF = '051.039.806.48',
               psexo = 'Masculino',
               ptelefone = '8596325478',
               pemail = 'mmc.maaairton@gmail.com',
               pRua = '7',
               pcasaNumero = '1',
               pcomplemeto = '',
               pbairro = '',
               pcep = '',
               pcidade = 'Fortaleza',
               pestado = 'ce',
               pnaturalidade = '',
               pnacionalidade = 'brasileiro',
               pNoPai   = 'pai',
               PNoMae = 'mae',
               pEmpresaParceira = 'parceira',
               pcodigoEspecial = '',
               pTipoEscola = '',
               pProcessoSeletivoId = 'a06Z000000FH74x', //getProcID(),
               pIdParticipacao = '185910010',
               Statusboleto = 'pago';
        date pdataNasc = Date.valueOf(System.now());
        
        
        //OppWebService oppW = new OppWebService();        
        OppWebService.CreateOpp(pnomeOpp
                       , pnomeCandidato
                       , pSobreNomeCandidato
                       , pofertaID
                       , popcaoCurso2
                       , pRG
                       , pPassporte
                       , pTreineiro
                       , pCPF
                       , psexo
                       , ptelefone
                       , pemail
                       , pdataNasc
                       , pRua
                       , pcasaNumero
                       , pcomplemeto
                       , pbairro
                       , pcep
                       , pcidade
                       , pestado
                       , pnaturalidade
                       , pnacionalidade
                       , pNoPai
                       , pNoMae
                       , pEmpresaParceira
                       , pcodigoEspecial
                       , pTipoEscola
                       , pProcessoSeletivoId
                       , pIdParticipacao
                       , Statusboleto);
        
    }
    
    
    @isTest static void callOut2()
    {
        String pnomeOpp = 'opp' + String.valueOf(System.now()), 
               pnomeCandidato = 'cand' + String.valueOf(System.now()),
               pSobreNomeCandidato = 'sbr' + String.valueOf(System.now()),
               pofertaID =  'a0LZ0000002bb3O', //getOfertaID(),
               popcaoCurso2 = '', //getOfertaID(),
               pRG = '1245',
               pPassporte = '1245',
               pTreineiro = '',
               pCPF = '051.039.806.48',
               psexo = 'Masculino',
               ptelefone = '8596325478',
               pemail = 'mmc.maaairton@gmail.com',
               pRua = '7',
               pcasaNumero = '1',
               pcomplemeto = '',
               pbairro = '',
               pcep = '',
               pcidade = 'Fortaleza',
               pestado = 'ce',
               pnaturalidade = '',
               pnacionalidade = 'brasileiro',
               pNoPai   = 'pai',
               PNoMae = 'mae',
               pEmpresaParceira = 'parceira',
               pcodigoEspecial = '',
               pTipoEscola = '',
               pProcessoSeletivoId = 'a06Z000000FH74x', //getProcID(),
               pIdParticipacao = '185910010',
               Statusboleto = 'pago';
        date pdataNasc = Date.valueOf(System.now());
        
        
        //OppWebService oppW = new OppWebService();        
        OppWebService.CreateOpp(pnomeOpp
                       , pnomeCandidato
                       , pSobreNomeCandidato
                       , pofertaID
                       , popcaoCurso2
                       , pRG
                       , pPassporte
                       , pTreineiro
                       , pCPF
                       , psexo
                       , ptelefone
                       , pemail
                       , pdataNasc
                       , pRua
                       , pcasaNumero
                       , pcomplemeto
                       , pbairro
                       , pcep
                       , pcidade
                       , pestado
                       , pnaturalidade
                       , pnacionalidade
                       , pNoPai
                       , pNoMae
                       , pEmpresaParceira
                       , pcodigoEspecial
                       , pTipoEscola
                       , pProcessoSeletivoId
                       , pIdParticipacao
                       , Statusboleto);
        
    }
    
    @isTest static void callOut3()
    {
        String pnomeOpp = 'opp' + String.valueOf(System.now()), 
               pnomeCandidato = 'cand' + String.valueOf(System.now()),
               pSobreNomeCandidato = 'sbr' + String.valueOf(System.now()),
               pofertaID =  '', //getOfertaID(),
               popcaoCurso2 = '', //getOfertaID(),
               pRG = '1245',
               pPassporte = '1245',
               pTreineiro = '',
               pCPF = '051.039.806.48',
               psexo = 'Masculino',
               ptelefone = '8596325478',
               pemail = 'mmc.maaairton@gmail.com',
               pRua = '7',
               pcasaNumero = '1',
               pcomplemeto = '',
               pbairro = '',
               pcep = '',
               pcidade = 'Fortaleza',
               pestado = 'ce',
               pnaturalidade = '',
               pnacionalidade = 'brasileiro',
               pNoPai   = 'pai',
               PNoMae = 'mae',
               pEmpresaParceira = 'parceira',
               pcodigoEspecial = '',
               pTipoEscola = '',
               pProcessoSeletivoId = 'a06Z000000FH74x', //getProcID(),
               pIdParticipacao = '185910010',
               Statusboleto = 'pago';
        date pdataNasc = Date.valueOf(System.now());
        
        
        //OppWebService oppW = new OppWebService();        
        OppWebService.CreateOpp(pnomeOpp
                       , pnomeCandidato
                       , pSobreNomeCandidato
                       , pofertaID
                       , popcaoCurso2
                       , pRG
                       , pPassporte
                       , pTreineiro
                       , pCPF
                       , psexo
                       , ptelefone
                       , pemail
                       , pdataNasc
                       , pRua
                       , pcasaNumero
                       , pcomplemeto
                       , pbairro
                       , pcep
                       , pcidade
                       , pestado
                       , pnaturalidade
                       , pnacionalidade
                       , pNoPai
                       , pNoMae
                       , pEmpresaParceira
                       , pcodigoEspecial
                       , pTipoEscola
                       , pProcessoSeletivoId
                       , pIdParticipacao
                       , Statusboleto);
        
    }
    
    @isTest static void callOut4()
    {
        string pidPart = '006Z000000DSq1XIAT',
               pstagename = 'Matriculado',
               pid = '006Z000000DSq1XIAT';
        
        OppWebService.UpdateOppById(pid, pstagename);
        OppWebService.UpdateOppByIdPart(pidPart, pstagename) ;
    }
    
    public static String getOfertaID()
    {
        Oferta__c Oid;
        Oid = [select o.Id from Oferta__c o limit 1];
        
        return String.valueOf(Oid.Id);
    }
    
    
    public static String getProcID()
    {
        Processo_seletivo__c Pid;
        Pid = [select o.Id from Processo_seletivo__c o limit 1];
        
        return String.valueOf(Pid.Id);
    }
    
}