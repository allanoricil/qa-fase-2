@isTest
private class CRA_FranquiasEADControllerTest {

  @testSetup
  static void setup(){
    Institui_o__c instituicao = CRA_DataFactoryTest.newInstituicao();
    insert instituicao;
    
    Campus__c campus = CRA_DataFactoryTest.newCampus(instituicao);
    insert campus;

    Per_odo_Letivo__c periodo = CRA_DataFactoryTest.newPeriodo();
    insert periodo;
    
    Processo_Seletivo__c processoSeletivo1 = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
    processoSeletivo1.nota_m_nima__c = 500.00;
    Processo_Seletivo__c processoSeletivo2 = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
    processoSeletivo2.nota_m_nima__c = 200.00;
    Processo_Seletivo__c processoSeletivo3 = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
    processoSeletivo3.nota_m_nima__c = 200.00;
    Processo_Seletivo__c processoSeletivo4 = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
    processoSeletivo4.Tipo_de_Matr_cula__c = 'ENEM';
    Processo_Seletivo__c processoSeletivo5 = CRA_DataFactoryTest.newProcesso(instituicao, campus, periodo);
    processoSeletivo5.nota_m_nima__c = 500.00;
    insert processoSeletivo1;
    insert processoSeletivo2;
    insert processoSeletivo3;
    insert processoSeletivo4;
    insert processoSeletivo5;
    
    Curso__c curso = CRA_DataFactoryTest.newCurso(instituicao, campus);
    insert curso;
    
    Area_Curso__c areaCurso = CRA_DataFactoryTest.newAreaCurso();
    insert areaCurso;
    
    Oferta__c oferta1 = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo1);
    Oferta__c oferta2 = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo2);
    Oferta__c oferta3 = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo3);
    Oferta__c oferta4 = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo4);
    Oferta__c oferta5 = CRA_DataFactoryTest.newOferta(instituicao, campus, periodo, curso, areaCurso, processoSeletivo5);
    insert oferta1;
    insert oferta2;
    insert oferta3;
    insert oferta4;
    insert oferta5;
    
    Account conta = CRA_DataFactoryTest.newAccountAluno();
    insert conta;

    Opportunity opp = CRA_DataFactoryTest.newOpportunityVestibularAgendado(instituicao,
                                                   campus,
                                                   periodo,
                                                   processoSeletivo1,
                                                   curso,
                                                   areaCurso,
                                                   oferta1,
                                                   conta);
    opp.Bolsa__c = 'Parente AdTalem - EAD Unifavip';

    Opportunity oppComPresencaTrue = CRA_DataFactoryTest.newOpportunityVestibularAgendado(instituicao,
                                                  campus,
                                                  periodo,
                                                  processoSeletivo2,
                                                  curso,
                                                  areaCurso,
                                                  oferta2,
                                                  conta);
    oppComPresencaTrue.Presente__c = true;


    Opportunity oppComBolsa = CRA_DataFactoryTest.newOpportunityVestibularAgendado(instituicao,
                                                   campus,
                                                   periodo,
                                                   processoSeletivo3,
                                                   curso,
                                                   areaCurso,
                                                   oferta3,
                                                   conta);
    oppComBolsa.Presente__c = true;
    oppComBolsa.Bolsa__c = 'Parente AdTalem - EAD Unifavip';

    Opportunity oppEnem = CRA_DataFactoryTest.newOpportunityEnem(instituicao,
                                                     campus,
                                                     periodo,
                                                     processoSeletivo4,
                                                     curso,
                                                     areaCurso,
                                                     oferta4,
                                                     conta);
    oppEnem.Presente__c = true;

    Opportunity oppSemBolsaEComMaiorNotaMenorQueNotaMinimaDoProcesso = CRA_DataFactoryTest.newOpportunityEnem(instituicao,
                                                      campus,
                                                      periodo,
                                                      processoSeletivo5,
                                                      curso,
                                                      areaCurso,
                                                      oferta5,
                                                      conta);
    oppSemBolsaEComMaiorNotaMenorQueNotaMinimaDoProcesso.Presente__c = true;
    insert opp;
    insert oppComPresencaTrue;
    insert oppComBolsa;
    insert oppEnem;
    insert oppSemBolsaEComMaiorNotaMenorQueNotaMinimaDoProcesso;

    Queue__c filaParaOppComPresencaConfirmada = new Queue__c(oportunidade__c = oppComPresencaTrue.Id,
       Payload__c = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"andersonn.gomesss@metrocamp.edu.br","cityId":null,"name":"2017.2 - E - Ibmec Educacional - Metrocamp - PROUNI - ANDERSON CID VIEGAS GOMES","mobile":null,"password":"UNIFAVIP@6537","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}',
       PayloadSAP__c = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":null,"userId":"1207349"}',
       Status__c = 'SUCCESS');


    Queue__c filaParaOppComEnemEPresencaConfirmada = new Queue__c(oportunidade__c = oppEnem.Id,
                              Payload__c = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"andersonn.gomesss@metrocamp.edu.br","cityId":null,"name":"2017.2 - E - Ibmec Educacional - Metrocamp - PROUNI - ANDERSON CID VIEGAS GOMES","mobile":null,"password":"UNIFAVIP@6537","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}',
                          PayloadSAP__c = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":null,"userId":"1207349"}',
                              Status__c = 'SUCCESS');

    Queue__c filaParaOppComBolsaEPresencaConfirmada = new Queue__c(oportunidade__c = oppComBolsa.Id,
                              Payload__c = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"andersonn.gomesss@metrocamp.edu.br","cityId":null,"name":"2017.2 - E - Ibmec Educacional - Metrocamp - PROUNI - ANDERSON CID VIEGAS GOMES","mobile":null,"password":"UNIFAVIP@6537","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}',
                          PayloadSAP__c = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":null,"userId":"1207349"}',
                              Status__c = 'SUCCESS');

    Queue__c filaParaOppSemBolsaEPresencaConfirmadaEComMaiorNotaMenorQueNotaMinimaDoProcesso = new Queue__c(oportunidade__c = oppSemBolsaEComMaiorNotaMenorQueNotaMinimaDoProcesso.Id,
                              Payload__c = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"andersonn.gomesss@metrocamp.edu.br","cityId":null,"name":"2017.2 - E - Ibmec Educacional - Metrocamp - PROUNI - ANDERSON CID VIEGAS GOMES","mobile":null,"password":"UNIFAVIP@6537","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}',
                          PayloadSAP__c = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":null,"userId":"1207349"}',
                              Status__c = 'SUCCESS');


    insert filaParaOppComPresencaConfirmada;
    insert filaParaOppComEnemEPresencaConfirmada;
    insert filaParaOppComBolsaEPresencaConfirmada;
    insert filaParaOppSemBolsaEPresencaConfirmadaEComMaiorNotaMenorQueNotaMinimaDoProcesso;
  }

  @isTest 
  static void testaConfirmarPresencaComServicoDeCriacaoDeUsuarioRetornando200(){
    Test.StartTest();
    Opportunity opp = [SELECT Id,
                              Presente__c
                         FROM Opportunity 
                        LIMIT 1];

    EvolucionalAPITest.testUserCreationServiceWithACorrectJsonBody();
    confirmaPresenca(opp);
    Test.StopTest();

    System.assertEquals(false, opp.Presente__c);

    opp = [SELECT Id,
                  Presente__c,
                  Senha__c,
                  Evolucional_Username__c,
                  Email_form__c,
                  CPF_form__c
             FROM Opportunity 
            LIMIT 1];

    Queue__c fila = [SELECT Oportunidade__c,
                            payloadsap__c
                       FROM Queue__c
                      WHERE Oportunidade__c =:opp.Id
                      LIMIT 1];

    String senhaEsperada = constroiSenhaUsandoCpf(opp.CPF_form__c);
    System.assertNotEquals(null, fila);
    System.assertEquals(senhaEsperada, opp.Senha__c);
    System.assertEquals(opp.Email_form__c, opp.Evolucional_Username__c);
    System.assertEquals(opp.Id, fila.Oportunidade__c);
    System.assertNotEquals('', fila.payloadsap__c);
    System.assertEquals(true, opp.Presente__c);
  }

  @isTest 
  static void testaConfirmarPresencaQuandoServicoDeCriacaoDeUsuarioRetorna409(){
    Test.StartTest();
    Opportunity opp = [SELECT Id,
                              Presente__c
                         FROM Opportunity 
                        LIMIT 1];

    EvolucionalAPITest.testUserCreationServiceWithTheSameEmail();
    confirmaPresenca(opp);
    Test.StopTest();

    System.assertEquals(false, opp.Presente__c);

    opp = [SELECT Id,
                  Presente__c
             FROM Opportunity 
            LIMIT 1];

    Queue__c fila = [SELECT Oportunidade__c,
                            payloadsap__c,
                            exceptionstacktrace__c
                       FROM Queue__c
                      WHERE Oportunidade__c =:opp.Id
                      LIMIT 1];

    System.assertNotEquals(null, fila);
    System.assertEquals(true, opp.Presente__c);
    System.assertEquals('EVOLUCIONAL_SERVICES / 409 / Conflict', fila.exceptionstacktrace__c);
  }

  @isTest 
  static void testaConfirmarPresencaQuandoServicoCaiNumaCalloutException(){
    Test.StartTest();
    Opportunity opp = [SELECT Id,
                              Presente__c
                         FROM Opportunity 
                        LIMIT 1];

    EvolucionalAPITest.testUserCreationServiceCalloutException();
    confirmaPresenca(opp);
    Test.StopTest();

    System.assertEquals(false, opp.Presente__c);

    opp = [SELECT Id,
                  Presente__c
             FROM Opportunity 
            LIMIT 1];

    Queue__c fila = [SELECT Oportunidade__c,
                            exceptionstacktrace__c
                       FROM Queue__c
                      WHERE Oportunidade__c =:opp.Id
                      LIMIT 1];

    System.assertNotEquals(null, fila);
    System.assertEquals(false, opp.Presente__c);
    Boolean exceptionOccured = fila.exceptionstacktrace__c.contains('Callout Exception!');
    System.assertEquals(true, exceptionOccured);
  }

  @isTest 
  static void testaConfirmarPresencaDeOportunidadeQueJaTemPresencaConfirmada() {
    Test.StartTest();
    List<Opportunity> opps = [SELECT Id,
                                     Presente__c
                                FROM Opportunity];
        Opportunity opp = opps.get(1);
        EvolucionalAPITest.testUserCreationServiceWithACorrectJsonBody();
    confirmaPresenca(opp);
    Test.StopTest();

    System.assertEquals(true, opp.Presente__c);

    opps = [SELECT Id,
                   Presente__c
              FROM Opportunity];
            opp = opps.get(1);
    List<Queue__c> filas = [SELECT Oportunidade__c
                              FROM Queue__c
                             WHERE Oportunidade__c =:opp.Id];
    System.assertNotEquals(null, filas);
    System.assertEquals(true, opp.Presente__c);
  }

  @isTest
  static void testaRecuperarResultadosParaUmAlunoComPresencaConfirmadaEComTipoDeProcessoDiferenteDeEnem(){
    Test.StartTest();
    List<Opportunity> opps = [SELECT Id,
                                     nota_1__c,
                                     nota_2__c,
                                     nota_3__c,
                                     maior_nota__c
                                FROM Opportunity];
    Opportunity opp = opps.get(1);
    EvolucionalAPITest.testListExamServiceWithoutEmail();
    recuperaResultados(opp);
    System.assertEquals(null, opp.nota_1__c);
    System.assertEquals(null, opp.nota_2__c);
    System.assertEquals(null, opp.nota_3__c);
    System.assertEquals(0, opp.maior_nota__c);
    Test.StopTest();


    opps =[SELECT Id,
                  nota_1__c,
                  nota_2__c,
                  nota_3__c,
                  maior_nota__c
             FROM Opportunity];

    opp = opps.get(1);
    System.assertEquals(329.00, opp.nota_1__c);
    System.assertEquals(439.00, opp.nota_2__c);
    System.assertEquals(128.00, opp.nota_3__c);
    System.assertEquals(439.00, opp.maior_nota__c);
  }

  @isTest
  static void testaRecuperarResultadosQuandoServicoCaiNumaCalloutException(){
    Test.StartTest();
    List<Opportunity> opps = [SELECT Id,
                                     nota_1__c,
                                     nota_2__c,
                                     nota_3__c,
                                     maior_nota__c
                                FROM Opportunity];
    Opportunity opp = opps.get(1);
    EvolucionalAPITest.testListExamResultsServiceCalloutException();
    recuperaResultados(opp);
    Test.StopTest();

    opps =[SELECT Id,
                  nota_1__c,
                  nota_2__c,
                  nota_3__c,
                  maior_nota__c
             FROM Opportunity];

    opp = opps.get(1);

    Queue__c fila = [SELECT Oportunidade__c,
                            exceptionstacktrace__c
                       FROM Queue__c
                      WHERE Oportunidade__c =:opp.Id
                      LIMIT 1];
        System.assertEquals(opp.Id, fila.Oportunidade__c);
    System.assertNotEquals(null, fila);
    Boolean exceptionOccured = fila.exceptionstacktrace__c.contains('Callout Exception!');
    System.assertEquals(true, exceptionOccured);
  }

  @isTest
  static void testaRecuperarResultadosVerificaAtualizacaoDaMaiorNota(){
    Test.StartTest();
      List<Opportunity> opps = [SELECT Id,
                                       nota_1__c,
                                       nota_2__c,
                                       nota_3__c,
                                       maior_nota__c
                                  FROM Opportunity];
      Opportunity opp = opps.get(1);
      EvolucionalAPITest.testListExamServiceWithoutEmail();
      recuperaResultados(opp);
      System.assertEquals(null, opp.nota_1__c);
      System.assertEquals(null, opp.nota_2__c);
      System.assertEquals(null, opp.nota_3__c);
      System.assertEquals(0, opp.maior_nota__c);
    Test.StopTest();

    opps =[SELECT Id,
      nota_1__c,
      nota_2__c,
      nota_3__c,
      maior_nota__c
    FROM Opportunity];

    opp = opps.get(1);
    System.assertEquals(329.00, opp.nota_1__c);
    System.assertEquals(439.00, opp.nota_2__c);
    System.assertEquals(128.00, opp.nota_3__c);
    System.assertEquals(439.00, opp.maior_nota__c);

    opp.nota_3__c = 500.00;
    update opp;

    opps =[SELECT Id,
      nota_1__c,
      nota_2__c,
      nota_3__c,
      maior_nota__c
    FROM Opportunity];

    opp = opps.get(1);

    System.assertEquals(500.00, opp.nota_3__c);
    System.assertEquals(500.00, opp.maior_nota__c);
  }

  /*
  @isTest
  static void testaRecuperarResultadosVerificaAtualizacaoDaFaseQuandoNotaFinalMaiorQueNotaMinimaProcessoSeletivoEComBolsa(){
    Test.StartTest();
    List<Opportunity> opps = [SELECT Id,
                                     StageName
                                FROM Opportunity];
    Opportunity opp = opps.get(2);
    EvolucionalAPITest.testListExamServiceWithoutEmail();
    recuperaResultados(opp);
    System.assertEquals('Inscrito', opp.StageName);
    Test.StopTest();


    opps =[SELECT Id,
                  StageName
             FROM Opportunity];

    opp = opps.get(2);
    System.assertEquals('Aprovado', opp.StageName);
  }*/

  @isTest
  static void testaRecuperarResultadosVerificaAtualizacaoDaFaseQuandoNotaFinalMaiorQueNotaMinimaProcessoSeletivoESemBolsa(){
    Test.StartTest();
      List<Opportunity> opps = [SELECT Id,
                                       StageName
                                  FROM Opportunity];
      Opportunity opp = opps.get(1);
      EvolucionalAPITest.testListExamServiceWithoutEmail();
      recuperaResultados(opp);
      System.assertEquals('Inscrito', opp.StageName);
    Test.StopTest();


    opps =[SELECT Id,
                  StageName
            FROM Opportunity];

    opp = opps.get(1);
    System.assertEquals('Inscrito', opp.StageName);
  }

  /*
  @isTest
  static void testaRecuperarResultadosVerificaAtualizacaoDaFaseQuandoNotaFinalMenorQueNotaMinimaProcessoSeletivoEComBolsa(){
    List<Opportunity> opps = [SELECT Id,
                                     StageName
                                FROM Opportunity];
    Opportunity opp = opps.get(0);
    opp.Presente__c = true;
    update opp;

    Queue__c fila = new Queue__c(oportunidade__c = opp.Id,
                             Payload__c = '{"token":"756C85B5-71AC-469A-979D-D8AD020D3154","email":"andersonn.gomesss@metrocamp.edu.br","cityId":null,"name":"2017.2 - E - Ibmec Educacional - Metrocamp - PROUNI - ANDERSON CID VIEGAS GOMES","mobile":null,"password":"UNIFAVIP@6537","degreeId":"7","schoolId":null,"schoolCityId":null,"schoolName":null,"courseId":"1","universityResponse":null}',
                         PayloadSAP__c = '{"token":"18B257B8-9280-4B3C-B14E-674AD4EB0148","email":null,"userId":"1207349"}',
                             Status__c = 'SUCCESS');
    insert fila;

    Test.StartTest();
      EvolucionalAPITest.testListExamServiceWithoutEmail();
      recuperaResultados(opp);
      System.assertEquals('Inscrito', opp.StageName);
    Test.StopTest();


    opps =[SELECT Id,
                  StageName
             FROM Opportunity];

    opp = opps.get(0);
    System.assertEquals('Reprovado', opp.StageName);
  }*/

  @isTest
  static void testaRecuperarResultadosVerificaAtualizacaoDaFaseQuandoNotaFinalMenorQueNotaMinimaProcessoSeletivoESemBolsa(){
      Test.StartTest();
        List<Opportunity> opps = [SELECT Id,
                                         StageName
                                    FROM Opportunity];
        Opportunity opp = opps.get(4);
        EvolucionalAPITest.testListExamServiceWithoutEmail();
        recuperaResultados(opp);
        System.assertEquals('Inscrito', opp.StageName);
      Test.StopTest();


      opps =[SELECT Id,
                    StageName
               FROM Opportunity];

      opp = opps.get(4);
      System.assertEquals('Inscrito', opp.StageName);
  } 

  private static void confirmaPresenca(Opportunity opp){
      CRA_FranquiasEADController controller = instantiatePageController(opp);
      controller.confirmarPresenca();
  }

  private static void recuperaResultados(Opportunity opp){
      CRA_FranquiasEADController controller = instantiatePageController(opp);
      controller.recuperarResultados();
  }

  private static CRA_FranquiasEADController instantiatePageController(Opportunity opp){
    PageReference pageRef = Page.CRA_Franquias_EAD_Presenca;
    pageRef.getParameters().put('id', opp.Id);
    Test.setCurrentPage(pageRef);
    ApexPages.StandardController standardController = new ApexPages.StandardController(opp);
    CRA_FranquiasEADController controller = new CRA_FranquiasEADController(standardController);
    return controller;
  }

  private static void verificaMensagemDeErroNaPagina(String mensagem){
    List<Apexpages.Message> msgs = ApexPages.getMessages();
    boolean isThereNewApexPageMessage = false;
    for(Apexpages.Message msg:msgs){
        if(msg.getDetail().contains(mensagem)){
        isThereNewApexPageMessage = true;
        }
    }
    System.assert(isThereNewApexPageMessage);
  }


  private static String constroiSenhaUsandoCpf(String cpf){
    if(cpf != null){
      return 'UNIFAVIP@' + cpf.replace('.','').substring(0,4);
    }
    return null;
  }

}