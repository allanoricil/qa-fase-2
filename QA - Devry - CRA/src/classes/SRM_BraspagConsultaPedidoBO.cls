/*
    @author Diego Moreira
    @class Classe de negocio para consulta de pedidos do braspag
    que não foram enviados no primeiro post de envio
*/
public class SRM_BraspagConsultaPedidoBO implements IProcessingQueue {
    /* 
        @param queueId Id da fila de processamento 
        @param eventName nome do evento de processamento
        @param payload JSON com o item da fila para processamento
    */   
    

    public static void processingQueue( String queueId, String eventName, String payload ) {
        ProcessorControl.sincronoContext = true;
        syncToServer( queueId, payload );
        ProcessorControl.inFutureContext = true;       
        List<Titulos__c> titulosToConsume = (List<Titulos__c>) JSON.deserialize( payload, List<Titulos__c>.class );
        TituloBO.getInstance().createQueueToInsertTitlesSyncrono( titulosToConsume );   
    }

    /*
        @param queueId Id da fila de processamento 
        @param payload JSON com o item da fila para processamento
    */
    @future( Callout=true )
    private Static void syncToServer( String queueId, String payload ) {
        SRM_WSBraspagDecoratorPagadorQuery.OrderIdDataResponse responseOrderId;
        SRM_WSBraspagDecoratorPagadorQuery.OrderDataResponse responseOrderData;
        SRM_WSBraspagDecoratorPagadorQuery.BoletoDataResponse responseBoletoData;
        List<Titulos__c> titulosToUpdate = (List<Titulos__c>) JSON.deserialize( payload, List<Titulos__c>.class );

        try {
            for( Titulos__c titulo : titulosToUpdate ) {
                //ALTER: MERCHANTID
                /*
                responseOrderId = SRM_BraspagCryptographyService.getInstance().braspagGetOrderIdData( titulo.ProcessoSeletivo__r.Merchant__c, titulo.Id, GuidUtil.NewGuid() );
                */
                responseOrderId = SRM_BraspagCryptographyService.getInstance().braspagGetOrderIdData( titulo.ProcessoSeletivo__r.Merchant_Id__c, titulo.Id, GuidUtil.NewGuid() );
                System.debug('>>> responseOrderId ' + responseOrderId.Success + ' / ' + responseOrderId);

                if( responseOrderId.Success ) {
                    String braspagOrderId = responseOrderId.OrderIdDataCollection.OrderIdTransactionResponse[0].BraspagOrderId;
                    //ALTER: MERCHANTID
                    /*
                    responseOrderData = SRM_BraspagCryptographyService.getInstance().braspagGetOrderData( titulo.ProcessoSeletivo__r.Merchant__c, braspagOrderId, GuidUtil.NewGuid() );
                    */
                    responseOrderData = SRM_BraspagCryptographyService.getInstance().braspagGetOrderData( titulo.ProcessoSeletivo__r.Merchant_Id__c, braspagOrderId, GuidUtil.NewGuid() );
                    SRM_WSBraspagDecoratorPagadorQuery.OrderTransactionDataResponse orderDataResult = responseOrderData.TransactionDataCollection.OrderTransactionDataResponse[0];

                    System.debug('>>> orderDataResult ' + orderDataResult);
                    titulo.CodigoBrasPag__c             = braspagOrderId;
                    titulo.TransacaoBoleto__c           = orderDataResult.BraspagTransactionId;            

                    if( titulo.MeioPagamento__c.equals( 'Boleto' ) && titulo.TransacaoBoleto__c != null ) {
                        //ALTER: MERCHANTID
                        /*
                        responseBoletoData = SRM_BraspagCryptographyService.getInstance().braspagGetBoletoData( titulo.ProcessoSeletivo__r.Merchant__c, titulo.TransacaoBoleto__c, Braspag__c.getValues( 'REQUESTID' ).Valor__c );
                        */
                        responseBoletoData = SRM_BraspagCryptographyService.getInstance().braspagGetBoletoData( titulo.ProcessoSeletivo__r.Merchant_Id__c, titulo.TransacaoBoleto__c, Braspag__c.getValues( 'REQUESTID' ).Valor__c );
                        if( responseBoletoData != null ) {
                            titulo.Name             = responseBoletoData.BoletoNumber;
                            titulo.NossoNumero__c   = responseBoletoData.BoletoNumber;
                            titulo.UrlBoleto__c     = responseBoletoData.BoletoUrl;
                        }
                    }                    
                    System.debug('>>> titulosToUpdate ' + titulo);
                } else if( responseOrderId.ErrorReportDataCollection.ErrorReportDataResponse[0].ErrorCode.equals( '140' ) ) {
                    titulo.Status__c = 'Inexistente';
                }
                titulo.SequencialConsulta__c = titulo.SequencialConsulta__c + 1;
            }
            ProcessorControl.inFutureContext = true;
            update titulosToUpdate;
            QueueBO.getInstance().updateQueue( queueId, '' );

        } catch ( CalloutException ex ) {
            QueueBO.getInstance().updateQueue( queueId, 'SEARCH_BRASPAG_ORDER / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        } catch ( Exception ex ) {
            QueueBO.getInstance().updateQueue( queueId, 'SEARCH_BRASPAG_ORDER / ' + ex.getMessage() + ' / ' + ex.getStackTraceString() );
        }       
    }

}