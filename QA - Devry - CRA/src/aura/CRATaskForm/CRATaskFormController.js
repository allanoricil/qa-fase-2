({
	init : function(component, event, helper) {
		var action = component.get('c.getSubjects');
        action.setCallback(this, function(response){
            var recuperouAssuntos = response.getState();
            if(recuperouAssuntos === "SUCCESS"){
                var subjects = response.getReturnValue();
                if(subjects.length > 0){
               		component.set('v.subjectValues', subjects);
                }
            }else if(recuperouAssuntos === "ERROR"){
                var errors = response.getError();
                if(errors){
                    console.log(errors);
                }
            }
        });
        $A.enqueueAction(action);
	},
    saveTask: function(component, event, helper){
        var action = component.get('c.createTask');
        var opportunityId = component.get('v.recordId');
        var assunto = 'WhatsApp';
        var canalDeContato = 'Phone';
        var statusDoContato = 'Novo';
        var descricaoDaAtividade = 'Teste';
        action.setParams({opportunityId	: opportunityId, 
                          assunto : assunto,
                          canalDeContato : canalDeContato,
                          statusDoContato : statusDoContato, 
                          descricaoDaAtividade : descricaoDaAtividade});
        action.setCallback(this, function(response){
           var state = response.getState();
            if(state === "SUCCESS"){
            	var taskCriadaComSucesso = response.getReturnValue();
                if(criouComSucesso){
                    alert('A Tarefa foi criada!');
                }else{
                    alert('A Tarefa não foi criada!');
                }
            }else if(state === "ERROR"){
            	var errors = response.getError();
                if(errors){
                    console.log(errors);
                }
            }
        });
        $A.enqueueAction(action);
    }
})